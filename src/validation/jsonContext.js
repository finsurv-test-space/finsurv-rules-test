function getTestDataByField(scope, obj, field) {
    var fieldParts = field.split('.');
    var localObj = obj;
    for (var i=0; i<fieldParts.length; i++) {
        var fieldPart = fieldParts[i];
        if (fieldPart in localObj) {
            localObj = localObj[fieldPart];
        }
        else {
            localObj = null;
        }
        if (!localObj) {
            if (i >= fieldParts.length-1)
                return null;
            else {
                if (scope === 'transaction' &&
                    (field.indexOf('Resident.') == 0 || field.indexOf('NonResident.') == 0) &&
                    i < 2)
                    return undefined;
                else
                    return null;
            }
        }
    }
    return localObj;
}

function produceJsonContext(jsonString) {
    var obj = eval(jsonString);
    obj.raisedEvent = [];
    obj.displayEvent = [];

    obj.getTransactionField = function(field) {
        return getTestDataByField('transaction', obj, field);
    };

    obj.getMoneySize = function() {
        var moneyList = obj.MonetaryAmount;
        if (moneyList)
            return moneyList.length;
        return 0;
    };

    obj.getMoneyField = function(instance, field) {
        var moneyList = obj.MonetaryAmount;
        if (moneyList)
            return getTestDataByField('money', moneyList[instance], field);
        return null;
    };

    obj.getImportExportSize = function(moneyInstance) {
        var moneyList = obj.MonetaryAmount;
        if (moneyList) {
            var money = moneyList[moneyInstance];
            var importList = money.ImportExport;
            if (importList) {
                return importList.length;
            }
        }
        return 0;
    };

    obj.getImportExportField = function(moneyInstance, instance, field) {
        var moneyList = obj.MonetaryAmount;
        if (moneyList) {
            var money = moneyList[moneyInstance];
            var importList = money.ImportExport;
            if (importList) {
                return getTestDataByField('importexport', importList[instance], field);
            }
        }
        return null;
    };

    obj.logTransactionEvent = function(type, field, code, msg) {
        this.raisedEvent.push({event: 'transaction', type: type, field: field, code: code, msg: msg });
    };

    obj.logMoneyEvent = function(type, instance, field, code, msg) {
        this.raisedEvent.push({event: 'money', instance: instance, type: type, field: field, code: code, msg: msg });
    };

    obj.logImportExportEvent = function(type, moneyInstance, instance, field, code, msg) {
        this.raisedEvent.push({event: 'importexport', moneyInstance: moneyInstance, instance: instance, type: type, field: field, code: code, msg: msg });
    };

    obj.logTransactionDisplayEvent = function(type, field, value) {
        this.displayEvent.push({event: 'transaction', type: type, field: field, value: value });
    };

    obj.logMoneyDisplayEvent = function(type, instance, field, value) {
        this.displayEvent.push({event: 'money', instance: instance, type: type, field: field, value: value });
    };

    obj.logImportExportDisplayEvent = function(type, moneyInstance, instance, field, value) {
        this.displayEvent.push({event: 'importexport', moneyInstance: moneyInstance, instance: instance, type: type, field: field, value: value });
    };

    obj.getErrorList = function() {
        return JSON.stringify(this.raisedEvent);
    }

    return obj;
}
