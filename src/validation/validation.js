define(['require', './predef'], function (require, predef) {


  function mustRunRule(scope, field, value) {
    return typeof value !== 'undefined';
  }


  function validationrule(scope, field, type, code, message, validation, categories, notCategories, assertion) {
    if (field.indexOf("{{") != -1) field = predef.getMap(field);
    if (message.indexOf("{{") != -1) message = predef.getMap(message);
    return function (context) {
      var matchedRule = false;
      var runRule;
      var value;
      var moneySize;
      var moneyCategory;
      var moneyMainCategory;
      var i;

      if (scope === "transaction") {
        value = context.getTransactionField(field);
        matchedRule = true;
        if (mustRunRule(scope, field, value)) {
          context.currentField = field;
          context.currentMoneyInstance = -1;
          context.currentImportExportInstance = -1;
          if (assertion(context, value)) {
            if (type === "VALIDATE") {
              context.callbacks.validate(validation, value, scope, context, function (status, validationCode, validationMsg) {
                var validationType;
                if (status === "pass") {
                  validationType = "SUCCESS";
                }
                else if (status === "fail") {
                  validationType = "ERROR";
                }
                else if (status === "error" || status === "warning") {
                  validationType = "WARNING";
                }
                else { // busy
                  validationType = "BUSY";
                }
                context.logTransactionEvent(validationType, field, validationCode, validationMsg);
              });
            }
            else
              context.logTransactionEvent(type, field, code.toString(), message);
          }

          //Code to perform mandatory detection
          if (type === "ERROR") {
            value = null;
            if (assertion(context, value)) {

              context.logTransactionEvent("MANDATORY", field, code.toString(), message);
            }
          }

        }
      } else if (scope === "money") {
        moneySize = context.getMoneySize();
        for (i = 0; i < moneySize; i++) {
          runRule = true;
          moneyCategory = context.categories[i];
          moneyMainCategory = context.mainCategories[i];
          if (categories && categories.indexOf(moneyCategory) == -1 && categories.indexOf(moneyMainCategory) == -1)
            runRule = false;
          if (notCategories && !predef.categoryNotInList(moneyCategory, moneyMainCategory, notCategories))
            runRule = false;
          if (runRule) {
            matchedRule = true;
            value = context.getMoneyField(i, field);
            if (mustRunRule(scope, field, value)) {
              context.currentField = field;
              context.currentMoneyInstance = i;
              context.currentImportExportInstance = -1;
              if (assertion(context, value)) {
                if (type === "VALIDATE") {
                  (function (moneyInd) {
                    context.callbacks.validate(validation, value, scope, context, function (status, validationCode, validationMsg) {
                      var validationType;
                      if (status === "pass") {
                        validationType = "SUCCESS";
                      } else if (status === "fail") {
                        validationType = "ERROR";
                      }
                      else if (status === "error" || status === "warning") {
                        validationType = "WARNING";
                      }
                      else if (status === "failbusy") {
                        validationType = "FAILBUSY";
                      }
                      else { // busy
                        validationType = "BUSY";
                      }
                      context.logMoneyEvent(validationType, moneyInd, field, validationCode, validationMsg);
                    });
                  })(i)
                }
                else
                  context.logMoneyEvent(type, i, field, code.toString(), message);
              }

              //Code to perform mandatory detection
              if (type === "ERROR") {
                value = null;
                if (assertion(context, value)) {

                  context.logMoneyEvent("MANDATORY", i, field, code.toString(), message);
                }
              }
            }
          }

        }
      } else if (scope === "importexport") {
        moneySize = context.getMoneySize();
        for (i = 0; i < moneySize; i++) {
          runRule = true;
          moneyCategory = context.categories[i];
          moneyMainCategory = context.mainCategories[i];
          if (categories && categories.indexOf(moneyCategory) == -1 && categories.indexOf(moneyMainCategory) == -1)
            runRule = false;
          if (notCategories && !predef.categoryNotInList(moneyCategory, moneyMainCategory, notCategories))
            runRule = false;

          if (runRule) {
            var importSize = context.getImportExportSize(i, j);
            for (var j = 0; j < importSize; j++) {
              value = context.getImportExportField(i, j, field);
              matchedRule = true;
              if (mustRunRule(scope, field, value)) {
                context.currentField = field;
                context.currentMoneyInstance = i;
                context.currentImportExportInstance = j;
                if (assertion(context, value)) {
                  if (type === "VALIDATE") {
                    (function (moneyInd, ieInd) {
                      context.callbacks.validate(validation, value, scope, context, function (status, validationCode, validationMsg) {
                        var validationType;
                        if (status === "pass") {
                          validationType = "SUCCESS";
                        } else if (status === "fail") {
                          validationType = "ERROR";
                        }
                        else if (status === "error" || status === "warning") {
                          validationType = "WARNING";
                        }
                        else { // busy
                          validationType = "BUSY";
                        }
                        context.logImportExportEvent(validationType, moneyInd, ieInd, field, validationCode, validationMsg);
                      });

                    })(i, j)
                  }
                  else
                    context.logImportExportEvent(type, i, j, field, code.toString(), message);
                }

                //Code to perform mandatory detection
                if (type === "ERROR") {
                  value = null;
                  if (assertion(context, value)) {

                    context.logImportExportEvent("MANDATORY", i, j, field, code.toString(), message);
                  }
                }

              }
            }
          }
        }
      }
      return matchedRule;
    };
  }

  function resolveMessageAndSection(rulename, field, msgRuleList, msgFieldLenList, defCode, defMessage, defSection) {
    if (rulename) {
      var name = rulename;

      var isFieldRuleName
        = rulename.length > 6 && rulename.slice(0, 6) === "field.";

      if (isFieldRuleName)
        name = field + rulename.slice(5);

      var isFieldRule = isFieldRuleName
        || rulename.lastIndexOf(".minLen") > 0
        || rulename.lastIndexOf(".len") > 0
        || rulename.lastIndexOf(".maxLen") > 0;

      var messages = isFieldRule
        ? msgFieldLenList
        : msgRuleList;

      for (var msg, i = 0; msg = messages[i]; i++) {
        if (msg.name === name) {
          return {
            code: (msg.code) ? msg.code : defCode,
            message: (msg.message) ? msg.message : defMessage,
            section: (msg.section) ? msg.section : defSection
          };
        }
      }
    }

    return {
      code: defCode,
      message: defMessage,
      section: defSection
    };
  }

  /*
   * Helper function to expand the rules with an Array of fields...(multi-field-rules)
   */
  function makeRule(rulelist, rulename, validation, ruleset, rule, msgRuleList, msgFieldLenList, ruleFn, section) {
    ruleFn = ruleFn ? ruleFn : validationrule;
    var m;
    var valField = validation.field;
    if (Array.isArray(valField) && valField.length == 1)
      valField = valField[0];

    if (Array.isArray(valField)) {
      for (var field, j = 0; field = valField[j]; j++) {

        m = resolveMessageAndSection(rule.name, field, msgRuleList, msgFieldLenList, rule.code, rule.message, rule.section);

        if ((section && m.section && m.section.indexOf(section) != -1) || (!section && !m.section) || (section && !m.section)) {

          if (rulename) {
            var indexedRuleName = rule.name + ':' + (j + 1);
            if (rulename === indexedRuleName)
              rulelist.push(ruleFn(ruleset.scope, field, rule.type, m.code, m.message, rule.validate, rule.categories, rule.notCategories, rule.assertion));
          } else {
            rulelist.push(ruleFn(ruleset.scope, field, rule.type, m.code, m.message, rule.validate, rule.categories, rule.notCategories, rule.assertion));
          }

        }
      }
    } else {

      m = resolveMessageAndSection(rule.name, valField, msgRuleList, msgFieldLenList, rule.code, rule.message, rule.section);

      if ((section && m.section && m.section.indexOf(section) != -1) || (!section && !m.section) || (section && !m.section)) {
        rulelist.push(ruleFn(ruleset.scope, valField, rule.type, m.code, m.message, rule.validate, rule.categories, rule.notCategories, rule.assertion));
      }
    }
  }

  /**
   * Internal helper to create named length rules so message display rules can be applied to them also ;)
   */
  function setLenRule(field, result, rulename, validation, ruleset, msgRuleList, msgFieldLenList, nameList) {
    if (validation.len) {
      if (
        (nameList.indexOf(field + ".len") === -1)
        && (!rulename || rulename == field + ".len")
      ) {
        makeRule(result, null, { field: field }, ruleset, {
          name: field + ".len",
          type: (predef.mappings && predef.mappings._lenErrorType) ? predef.mappings._lenErrorType : "ERROR",
          code: "L01",
          message: "The field " + field + " length is incorrect and must be " + validation.len + " characters in length",
          assertion: predef.isWrongLength(validation.len)
        }, msgRuleList, msgFieldLenList);
        nameList.push(field + ".len");
      }
    }
    if (validation.minLen) {
      if (
        (nameList.indexOf(field + ".minLen") === -1)
        && (!rulename || rulename == field + ".minLen")
      ) {
        makeRule(result, null, { field: field }, ruleset, {
          name: field + ".minLen",
          type: (predef.mappings && predef.mappings._minLenErrorType) ? predef.mappings._minLenErrorType : "ERROR",
          code: "L02",
          message: "The field " + field + " is too short, it must be at least " + validation.minLen + " characters in length",
          assertion: predef.isTooShort(validation.minLen)
        }, msgRuleList, msgFieldLenList);
        nameList.push(field + ".minLen");
      }
    }
    if (validation.hasOwnProperty('maxLen')) {
      if (
        (nameList.indexOf(field + ".maxLen") === -1)
        && (!rulename || rulename == field + ".maxLen")
      ) {
        makeRule(result, null, { field: field }, ruleset, {
          name: field + ".maxLen",
          type: (predef.mappings && predef.mappings._maxLenErrorType) ? predef.mappings._maxLenErrorType : "WARNING",
          code: "L03",
          message: "The field " + field + " is too long" + ((predef.mappings && predef.mappings._maxLenErrorType == "WARNING") ? " and will be shortened to " + validation.maxLen + " characters" : ""),
          assertion: validation.maxLen ? predef.isTooLong(validation.maxLen) : function () {
            return false;
          }
        }, msgRuleList, msgFieldLenList);
        
        nameList.push(field + ".maxLen");
      }
    }
  }

  /**
   * 
   * @param {*} rulesets The ruleset
   * @param {*} flow The flow to filter by
   * @param {*} section the section ("A","BF","ACD",etc. From Finsruv Ops Manual B.2)
   * @param {*} categories the category filters
   * @param {*} mainCategories the main categories
   * @param {*} rulename the rule name to extract
   * @param {*} ruleFn - the function to run on a rule - Signature should be: 
   * fn(scope, field, type, m.code, message, validate, categories, notCategories, assertion):void
   */
  function validationRulesFromRuleSets(rulesets, flow, section, categories, mainCategories, rulename, ruleFn) {
    var result = [];
    var nameList = [];
    var msgRuleList = [];
    var msgFieldLenList = [];
    for (var rs = 0; rs < rulesets.length; rs++) {
      var ruleset = rulesets[rs];
      for (var vs = 0; vs < ruleset.validations.length; vs++) {
        var validation = ruleset.validations[vs];
        if (validation.rules) {
          for (var r = 0; r < validation.rules.length; r++) {
            var i;
            var rule = validation.rules[r];

            if (nameList.indexOf(rule.name) === -1) {
              if (rule.type === "MESSAGE") {
                if (flow && rule.flow && rule.flow !== flow)
                  continue;

                if (rule.name.slice(rule.name.length - 4) === ".len" ||
                  rule.name.slice(rule.name.length - 7) === ".minLen" ||
                  rule.name.slice(rule.name.length - 7) === ".maxLen") {
                  msgFieldLenList.push(rule);
                }
                else {
                  msgRuleList.push(rule);
                }
                continue;
              }
              else
                nameList.push(rule.name);

              // if there is a rulename, match it with rule.name, or validation.field.
              if (rulename && rule.name) {
                if (rulename.indexOf(':') >= 0) {// compound rules...
                  var cleanRuleName = rulename.substr(0, rulename.indexOf(':'));
                  if (rule.name != cleanRuleName)
                    continue;
                } else {
                  if (rule.name != rulename) {
                    continue;
                  }
                }
              }

              if (flow && rule.flow && !(rule.flow === flow))
                continue;

              // TODO: REMOVE
              // if (section && rule.section && rule.section.indexOf(section) == -1)
              //   continue;

              if (categories && rule.categories) {
                var hasCategory = false;
                for (i = 0; i < categories.length; i++) {
                  if (rule.categories.indexOf(categories[i]) >= 0) {
                    hasCategory = true;
                    break;
                  }
                }
                if (!hasCategory) {
                  for (i = 0; i < mainCategories.length; i++) {
                    if (rule.categories.indexOf(mainCategories[i]) >= 0) {
                      hasCategory = true;
                      break;
                    }
                  }
                }
                if (!hasCategory)
                  continue;
              }
              if (categories && rule.notCategories) {
                if (!predef.categoriesNotInList(categories, mainCategories, rule.notCategories))
                  continue;
              }

              makeRule(result, rulename, validation, ruleset, rule, msgRuleList, msgFieldLenList, ruleFn, section);
            }
          }
        }

        if (Array.isArray(validation.field)) {
          validation.field.forEach(function (field) {
            setLenRule(field, result, rulename, validation, ruleset, msgRuleList, msgFieldLenList, nameList)
          })
        } else {
          setLenRule(validation.field, result, rulename, validation, ruleset, msgRuleList, msgFieldLenList, nameList)
        }

      }
    }
    return result;
  }

  function runValidation(context, rulesets, rulename) {
    // predefs are here, adding this here as easiets solution,
    // Actual solution, import new 'internalUtils' and use through jsonContext
    context.internalUtils = this.predef.internalUtils;

    var rulesRun = 0;
    var filteredRules = validationRulesFromRuleSets(rulesets, context.flow, context.section, context.categories, context.mainCategories, rulename);
    for (var r = 0; r < filteredRules.length; r++) {
      var rule = filteredRules[r];
      if (rule(context))
        rulesRun++;
    }
    return rulesRun > 0;
  }

  return {
    runValidation: runValidation,
    getRule: function (context, rulesets, rulename) {
      // should only be one
      return validationRulesFromRuleSets(rulesets, context.flow, context.section, context.categories, context.mainCategories, rulename,
        function (scope, field, type, code, message, validate, categories, notCategories, assertion) {
          return {
            scope: scope,
            field: field,
            type: type,
            code: code,
            message: message,
            validate: validate,
            categories: categories,
            notCategories: notCategories,
            assertion: assertion
          }
        });
    }
  }


});
