define(function () {

  function getObjDataByField(scope, obj, field) {
    var fieldParts = field.split('.');
    var localObj = obj;
    for (var i=0; i<fieldParts.length; i++) {
      var fieldPart = fieldParts[i];
      if (fieldPart in localObj) {
        localObj = localObj[fieldPart];
      }
      else {
        localObj = null;
      }
      if (!localObj) {
        if (i >= fieldParts.length-1)
          return null;
        else {
          if (scope === 'transaction' &&
              (field.indexOf('Resident.') == 0 || field.indexOf('NonResident.') == 0) &&
              i < 2)
            return undefined;
          else
            return null;
        }
      }
    }
    return localObj;
  }

  function produceObjContext(obj) {
    obj.validationEvent = [];
    obj.displayEvent = [];

    obj.getTransactionField = function(field) {
      return getObjDataByField('transaction', obj, field);
    };

    obj.getMoneySize = function() {
      var moneyList = obj.MonetaryAmount;
      if (moneyList)
        return moneyList.length;
      return 0;
    };

    obj.getMoneyField = function(instance, field) {
      var moneyList = obj.MonetaryAmount;
      if (moneyList)
        return getObjDataByField('money', moneyList[instance], field);
      return null;
    };

    obj.getImportExportSize = function(moneyInstance) {
      var moneyList = obj.MonetaryAmount;
      if (moneyList) {
        var money = moneyList[moneyInstance];
        var importList = money.ImportExport;
        if (importList) {
          return importList.length;
        }
      }
      return 0;
    };

    obj.getImportExportField = function(moneyInstance, instance, field) {
      var moneyList = obj.MonetaryAmount;
      if (moneyList) {
        var money = moneyList[moneyInstance];
        var importList = money.ImportExport;
        if (importList) {
          return getObjDataByField('importexport', importList[instance], field);
        }
      }
      return null;
    };

    obj.logTransactionEvent = function(type, field, code, msg) {
      this.validationEvent.push({event: 'transaction', type: type, field: field, code: code, msg: msg });
    };

    obj.logMoneyEvent = function(type, instance, field, code, msg) {
      this.validationEvent.push({event: 'money', instance: instance, type: type, field: field, code: code, msg: msg });
    };

    obj.logImportExportEvent = function(type, moneyInstance, instance, field, code, msg) {
      this.validationEvent.push({event: 'importexport', moneyInstance: moneyInstance, instance: instance, type: type, field: field, code: code, msg: msg });
    };

    obj.logTransactionDisplayEvent = function(type, field, value) {
      this.displayEvent.push({event: 'transaction', type: type, field: field, value: value });
    };

    obj.logMoneyDisplayEvent = function(type, instance, field, value) {
      this.displayEvent.push({event: 'money', instance: instance, type: type, field: field, value: value });
    };

    obj.logImportExportDisplayEvent = function(type, moneyInstance, instance, field, value) {
      this.displayEvent.push({event: 'importexport', moneyInstance: moneyInstance, instance: instance, type: type, field: field, value: value });
    };

    return obj;
  }

  return {
    produceObjContext: produceObjContext
  }
});
