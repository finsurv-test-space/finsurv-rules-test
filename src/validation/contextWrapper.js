define(function() {

//*************************************************************************************************************
// Validation Functionality
//*************************************************************************************************************

  // Java signature: private boolean areObjectsTheSame(Object obj1, Object obj2)
  function areObjectsTheSame(obj1, obj2) {
    // if both x and y are null or undefined and exactly the same
    if (obj1 === obj2)
      return true;

    // if they are not strictly equal, they both need to be Objects
    if ( !(obj1 instanceof Object ) || !(obj2 instanceof Object) )
      return false;

    // they must have the exact same prototype chain, the closest we can do is
    // test there constructor.
    if (obj1.constructor !== obj2.constructor)
      return false;

    for (var p in obj1) {
      // other properties were tested using x.constructor === y.constructor
      if (!obj1.hasOwnProperty( p ))
        continue;

      // allows to compare x[ p ] and y[ p ] when set to undefined
      if (!obj2.hasOwnProperty( p ))
        return false;

      // if they have the same strict value or identity then they are equal
      if (obj1[p] === obj2[p])
        continue;

      // Numbers, Strings, Functions, Booleans must be strictly equal
      if (typeof( obj1[p] ) !== "object" )
        return false;

      // Objects and Arrays must be tested recursively
      if ( !areObjectsTheSame( obj1[ p ],  obj2[ p ] ) )
        return false;
    }

    for (p in obj2) {
      // allows x[ p ] to be set to undefined
      if ( obj2.hasOwnProperty( p ) && ! obj1.hasOwnProperty( p ) ) {
        return false;
      }
    }
    return true;
  }

  function hashString(str){
  	var hash = 0;
  	for (var i = 0; i < str.length; i++) {
  		hash += Math.pow(str.charCodeAt(i) * 31, str.length - i);
  		hash = hash & hash; // Convert to 32bit integer
  	}
  	return hash;
  }

  // Java signature: private int hashInputs (Object value, Object[] inputs)
  function hashInputs(value, inputs) {
    var result = 0;
    if (value) {
      var strValue = String(value)
      result = hashString(strValue);
    }
    if (inputs && Array.isArray(inputs)) {
      for (var i = 0; i < inputs.length; i++) {
        if (inputs[i]) {
          var strInput = String(inputs[i])
          result = result ^ hashString(strInput);
        }
      }
    }
    return result;
  }

  // Java signature: private int hashCacheEntry (String  validateName, ValidationInputs : validateInputs)
  function hashCacheEntry(validateName, validateInputs) {
    return hashString(validateName) ^ hashInputs(validateInputs.value, validateInputs.inputs);
  }

  var StatusType = {
      PASS: 'pass',
      FAIL: 'fail',
      ERROR: 'error',
      BUSY: 'busy',
      WARNING: 'warning'
  }

  var CachePeriod = {
      FOREVER: 'forever', // Keep the cached entry forever
      LONG: 'long', // Keep the cached entry for a long period of time (default is 1 week)
      MEDIUM: 'medium', // Keep the cached entry for a medium period of time (default is 1 hour)
      SHORT: 'short', // Keep the cached entry for a short period of time (default is 1 minute)
      NEVER: 'never' // Don't cache the entry at all
  }

  //*****************
  //*** CacheStrategy
  //*****************
  // Java signature: StatusType status, CachePeriod cachePeriod
  function new_CacheStrategy (status, cachePeriod) {
    // Java signature: private final List<StatusCacheStrategy> strategy = new ArrayList<>();
    var strategy = {
      statusStrategy : []
    };

    // Java signature: public StatusCacheStrategy getCacheStrategyByStatus(StatusType status)
    strategy.getCacheStrategyByStatus = function(status) {
      for (var i = 0; i < this.statusStrategy.length; i++) {
        if (this.statusStrategy[i].status == status) {
          return this.statusStrategy[i];
        }
      }
      return null;
    }

    // Java signature: public CacheStrategy set(StatusType status, CachePeriod cachePeriod)
    strategy.set = function(status, cachePeriod) {
      if (status) {
        var entry = this.getCacheStrategyByStatus(status);
        if (entry) {
          entry.cachePeriod = cachePeriod;
        }
        else {
          entry = {
            status: status,
            cachePeriod: cachePeriod
          };
          this.statusStrategy.push(entry);
        }
      }
      return this;
    }

    // Java signature: public CacheStrategy addIfMissing(StatusType status, CachePeriod cachePeriod)
    strategy.addIfMissing = function(status, cachePeriod) {
      var entry = this.getCacheStrategyByStatus(status);
      if (!entry && status) {
        entry = {
          status: status,
          cachePeriod: cachePeriod
        };
        this.statusStrategy.push(entry);
      }
      return this;
    }

    strategy.set(status, cachePeriod).
      addIfMissing(StatusType.PASS, CachePeriod.LONG).
      addIfMissing(StatusType.FAIL, CachePeriod.MEDIUM).
      addIfMissing(StatusType.ERROR, CachePeriod.SHORT).
      addIfMissing(StatusType.WARNING, CachePeriod.SHORT);

    return strategy;
  }

  //********************
  //*** ValidationInputs
  //********************
  function new_ValidationInputs (value, inputs) {
    // Java signature: Object value, Object[] inputs
    var entry = {
      value: value,
      inputs : inputs
    };

    return entry;
  }

  //********************
  //*** ValidationResult
  //********************
  function new_ValidationResult (status, code, message) {
    // Java signature: StatusType status, String code, String message
    var entry = {
      status: status,
      code : code,
      message : message
    };

    if (!entry.code && entry.status !== StatusType.PASS && entry.status !== StatusType.BUSY) {
      entry.code = "ERR";
      entry.message = "Message and code needs to be set for validation results that do not pass";
    }
    return entry;
  }

  function new_DataRetrievalResult (status, message, data) {
    var entry = {
      status: status,
      message : message,
      data : data
    };

    return entry;
  }

  //************************
  //*** ValidationCacheEntry
  //************************
  function new_ValidationCacheEntry (validateName, validateInputs, result, responseCache, expiryMillis) {
    // Java signature: String validateName, ValidationInputs validateInputs,
    //                 ValidationResult result, HashMap<String, Object> responseCache, long expiryMillis
    var entry = {
      validateName: validateName,
      validateInputs : validateInputs,
      result : result,
      responseCache : responseCache,
      expiryMillis : expiryMillis
    };

    // Java signature: public boolean equals(ValidationCacheEntry other)
    entry.equals = function(other) {
      if (!areObjectsTheSame(this.validateName, other.validateName)) {
        return false;
      }
      if (validateInputs) {
        if (!areObjectsTheSame(this.validateInputs, other.validateInputs)) {
          return false;
        }
      }
      else {
        if (other.validateInputs) {
          return false;
        }
      }
      return true;
    }

    // Java signature: public boolean equalsNameAndInputs(String validateName, ValidationInputs validateInputs)
    entry.equalsNameAndInputs = function(validateName, validateInputs) {
      if (!areObjectsTheSame(this.validateName, validateName)) {
        return false;
      }
      if (validateInputs) {
        if (!areObjectsTheSame(this.validateInputs, validateInputs)) {
          return false;
        }
      }
      else {
        if (other.validateInputs) {
          return false;
        }
      }
      return true;
    }

    // Java signature: public int hashCode()
    entry.hashCode = function() {
      return hashCacheEntry(this.validateName, this.validateInputs);
    }

    return entry;
  }

  //***********************
  //*** CustomValidateCache
  //***********************
  function new_CustomValidateCache (validateRegistry) {
    // Java Signature: CustomValidateRegistry validateRegistry (wrapCallbacks()); Map<Integer, List<ValidationCacheEntry>> cacheMap
    var entry = {
      validateRegistry : validateRegistry,
      keepLongMillis : 1000 * 60 * 60 * 24 * 7, // 1 week
      keepMediumMillis : 1000 * 60 * 60, // 1 hour
      keepShortMillis : 1000 * 60, // 1 minute
      cacheMap : {}
    };

    // Java signature: public synchronized void clear()
    entry.clear = function() {
      this.cacheMap = {};
    }

    // Java signature: public long getKeepLongMillis()
    entry.getKeepLongMillis = function() {
      return this.keepLongMillis;
    }

    // Java signature: public void setKeepLongMillis(long keepLongMillis)
    entry.setKeepLongMillis = function(keepLongMillis) {
      this.keepLongMillis = keepLongMillis;
    }

    // Java signature: public long getKeepMediumMillis()
    entry.getKeepMediumMillis = function() {
      return this.keepMediumMillis;
    }

    // Java signature: public void setKeepMediumMillis(long keepMediumMillis)
    entry.setKeepMediumMillis = function(keepMediumMillis) {
      this.keepMediumMillis = keepMediumMillis;
    }

    // Java signature: public long getKeepShortMillis()
    entry.getKeepShortMillis = function() {
      return this.keepShortMillis;
    }

    // Java signature: public void setKeepShortMillis(long keepShortMillis)
    entry.setKeepShortMillis = function(keepShortMillis) {
      this.keepShortMillis = keepShortMillis;
    }

    // Java signature: private ValidationCacheEntry findMatch(List<ValidationCacheEntry> entryList, String validationName, ValidateInputs inputs)
    function findMatch(entryList, validationName, inputs) {
      var result = null;
      for (var i = 0; i < entryList.length; i++) {
        var vce = entryList[i];
        if (vce.equalsNameAndInputs(validationName, inputs)) {
          result = vce;
          break;
        }
      }
      return result;
    }

    // Java signature: private void removeCacheEntry(Map<Integer, List<ValidationCacheEntry>> cacheMap, ValidationCacheEntry ce)
    function removeCacheEntry(cacheMap, ce) {
      var ceStrHash = String(ce.hashCode());
      if (ceStrHash in cacheMap) {
        var entryList = cacheMap[ceStrHash];
        if (entryList.length > 1) {
          var removeIndex = -1;
          for (var i = 0; i < entryList.length; i++) {
            if (entryList[i].equals(ce)) {
              removeIndex = i;
              break;
            }
          }
          if (removeIndex >= 0) {
            cacheMap[ceStrHash] = entryList.splice(removeIndex, 1);
          }
        } else {
          delete cacheMap[ceStrHash];
        }
      }
    }

    // Java signature: public synchronized void cacheResult(String name, ValidateInputs inputs, CustomValidateResult result, ResponseCache responseCache)
    entry.cacheResult = function(name, inputs, result, responseCache, expiryMillis) {
      if (!expiryMillis) {
        var cp = this.validateRegistry.getCachePeriodByCallAndStatus(name, result.status);
        if (cp == CachePeriod.NEVER)
            return;

        var currentDate = new Date();
        var currentMillis = currentDate.getTime();
        expiryMillis = currentMillis;
        if (cp == CachePeriod.LONG) {
          expiryMillis += this.keepLongMillis;
        }
        else
        if (cp == CachePeriod.MEDIUM) {
          expiryMillis += this.keepMediumMillis;
        }
        else
        if (cp == CachePeriod.SHORT) {
          expiryMillis += this.keepShortMillis;
        }
        else {
          expiryMillis = -1;
        }
      }

      var ceStrHash = String(hashCacheEntry(name, inputs));
      var validationEntry = null;

      if (ceStrHash in this.cacheMap) {
        var entryList = this.cacheMap[ceStrHash];

        validationEntry = findMatch(entryList, name, inputs);
        if (validationEntry) {
          validationEntry.result = result;
          validationEntry.responseCache = responseCache;
          validationEntry.expiryMillis = expiryMillis;
        }
        else {
          validationEntry = new_ValidationCacheEntry(name,
                                                     inputs,
                                                     result,
                                                     responseCache,
                                                     expiryMillis);
          entryList.push(validationEntry);
        }
      }
      else {
        validationEntry = new_ValidationCacheEntry(name,
                                                   inputs,
                                                   result,
                                                   responseCache,
                                                   expiryMillis);

        var entryList = [];
        entryList.push(validationEntry);
        this.cacheMap[ceStrHash] = entryList;
      }
    }

    // Java signature: public synchronized CustomValidateResult getResult(String name, ValidateInputs inputs)
    entry.getResult = function(name, inputs) {
      var ceStrHash = String(hashCacheEntry(name, inputs));
      if (ceStrHash in this.cacheMap) {
        var entryList = this.cacheMap[ceStrHash];

        var matchedEntry = findMatch(entryList, name, inputs);
        if (matchedEntry) {
          var currentDate = new Date();
          var currentMillis = currentDate.getTime();

          if (currentMillis < matchedEntry.expiryMillis || matchedEntry.expiryMillis == -1) {
            return matchedEntry.result;
          }
          removeCacheEntry(this.cacheMap, matchedEntry);
          return null;
        }
      }
      return null;
    }

    // Java signature: public synchronized CustomValidateResult getResult(String name, Object value, Object[] otherInputs)
    entry.getResultOldStyle = function(name, value, otherInputs) {
      return entry.getResult(name, new_ValidateInputs(value, otherInputs));
    }

    // Java signature: public synchronized ResponseCache getResponse(String name, ValidateInputs inputs)
    entry.getResponse = function(name, inputs) {
      var ceStrHash = String(hashCacheEntry(name, inputs));
      if (ceStrHash in this.cacheMap) {
        var entryList = this.cacheMap[ceStrHash];

        var matchedEntry = findMatch(entryList, name, inputs);
        if (matchedEntry) {
          var currentDate = new Date();
          var currentMillis = currentDate.getTime();

          if (currentMillis < matchedEntry.expiryMillis || matchedEntry.expiryMillis == -1) {
            return matchedEntry.responseCache;
          }
          removeCacheEntry(this.cacheMap, matchedEntry);
          return null;
        }
      }
      return null;
    }

    // Java signature: public synchronized List<ValidationCacheEntry> getValidationCacheList()
    entry.getValidationCacheList = function() {
      var entryList = [];
      var currentDate = new Date();
      var currentMillis = currentDate.getTime();

      for (var i = 0, keys = Object.keys(this.cacheMap), ii = keys.length; i < ii; i++) {
        var entries = this.cacheMap[keys[i]];

        for (var j = 0; j < entries.length; j++) {
          var entry = entries[j];

          if (currentMillis < entry.expiryMillis || entry.expiryMillis == -1) {
            entryList.push(entry);
          }
        }
      }

      return entryList;
    }

    // Java signature: public synchronized void expiredCleanup()
    entry.expiredCleanup = function() {
      var currentDate = new Date();
      var currentMillis = currentDate.getTime();
      var removeEntries = [];
      
      for (var i = 0, keys = Object.keys(this.cacheMap), ii = keys.length; i < ii; i++) {
        var entries = this.cacheMap[keys[i]];

        for (var j = 0; j < entries.length; j++) {
          var entry = entries[j];
         
          if (currentMillis >= entry.expiryMillis && entry.expiryMillis != -1) {
            removeEntries.push(entry);
          }
        }
      }

      for (var i = 0; i < removeEntries.length; i++) {
        removeCacheEntry(this.cacheMap, removeEntries[i]);
      }
    }

    // Java signature: public int size()
    entry.size = function() {
      var size = 0;
      for (var i = 0, keys = Object.keys(this.cacheMap), ii = keys.length; i < ii; i++) {
        size = size + this.cacheMap[keys[i]].length;
      }

      return size;
    }

    // Java signature: public float sparsenessMetric()
    entry.sparsenessMetric = function() {
        var size = 0;
        for (var i = 0, keys = Object.keys(this.cacheMap), ii = keys.length; i < ii; i++) {
          size += this.cacheMap[keys[i]].length;
        }

        return size/Object.keys(this.cacheMap).length;
    }

    return entry;
  }

  //*******************
  //*** PersistentCache
  //*******************
  /*
   The following is the format to be used by this component to store validations in customData
   "ValidationCache": [
     {
       "Name": "",
       "Expiry": "",
       "Inputs": {
         "Value": "",
         "Other": [ "", "", "" ]
       },
       "Result": {
         "Status": "", //Pass, Fail, Error
         "Code": "",
         "Message": ""
       },
       "Response": {...}
     }
   ]
   */
  function new_PersistentCache (customData, customValidateCache) {
    // Java signature: Map<String, Object> customData, CustomValidateCache customValidateCache
    var wrapper = {
      customData : customData,
      customValidateCache : customValidateCache
    };

    // java signature: void clearCache();
    wrapper.clearCache = function() {
      delete this.customData['ValidationCache'];
    };

    // Java signature: private void getInternalStatus(String status)
    function getInternalStatus(status) {
      if (status == "Pass")
        return "pass";
      if (status == "Error")
        return "error";
      if (status == "Fail")
        return "fail";
      if (status == "Busy")
        return "busy";
      return status;
    }

    // Java signature: private void getPersistStatus(String status)
    function getPersistStatus(status) {
      if (status == "pass")
        return "Pass";
      if (status == "error")
        return "Error";
      if (status == "fail")
        return "Fail";
      if (status == "busy")
        return "Busy";
      return status;
    }

    // Java signature: private void marshalEntry(ValidationCacheEntry entry)
    function marshalEntry(entry) {
      var jsVal = {};
      jsVal["Name"] = entry.validateName;
      if (entry.expiryMillis > -1) {
        var expiryDate = new Date(entry.expiryMillis);
        jsVal["Expiry"] = expiryDate.toISOString();
      }
      var jsInputs = {};
      jsVal["Inputs"] = jsInputs;
      jsInputs["Value"] = entry.validateInputs.value;
      var jsOther = [];
      jsInputs["Other"] = jsOther;

      var inputs = entry.validateInputs.inputs;
      if (inputs && Array.isArray(inputs)) {
        for (var i = 0; i < inputs.length; i++) {
          if (inputs[i]) {
            jsOther.push(String(inputs[i]))
          }
          else {
            jsOther.push(null);
          }
        }
      }

      var jsResult = {};
      jsVal["Result"] = jsResult;
      jsResult["Status"] = getPersistStatus(entry.result.status);
      if (entry.result.code) {
        jsResult["Code"] = entry.result.code;
      }
      if (entry.result.message) {
        jsResult["Message"] = entry.result.message;
      }

      if (entry.responseCache) {
        jsVal["Response"] = entry.responseCache;
      }

      return jsVal;
    }

    // Java signature: void marshalToData();
    wrapper.marshalToData = function() {
      var validationCache = [];
      var entryList = this.customValidateCache.getValidationCacheList();

      if (entryList && entryList.length > 0) {
        wrapper.customData['ValidationCache'] = validationCache;
        for (var i = 0; i < entryList.length; i++) {
          var entry = entryList[i];
          validationCache.push(marshalEntry(entry));
        }
      }
    };

    // Java signature: private CustomValidateCache.ValidateInputs readInputs(Object objInputs)
    function readInputs(jsInputs) {
      if (jsInputs) {
        var value = jsInputs["Value"];
        var otherInputs = jsInputs["Other"];

        var arrayInputs = [];
        if (Array.isArray(otherInputs)) {
          for (var i = 0; i < otherInputs.length; i++) {
            if (otherInputs[i]) {
              arrayInputs.push(otherInputs[i]);
            }
            else {
              arrayInputs.push(null);
            }
          }
        }
        return new_ValidationInputs(value, arrayInputs);
      }
      return null;
    }

    // Java signature: private CustomValidateResult readResult(Object objResult)
    function readResult(jsResult) {
      if (jsResult) {
        var status = getInternalStatus(String(jsResult["Status"]));
        var code = (jsResult["Code"]) ? String(jsResult["Code"]) : undefined;
        var message = (jsResult["Message"]) ? String(jsResult["Message"]) : undefined;

        if (status) {
          return new_ValidationResult(status, code, message);
        }
      }
      return null;
    }

    // Java signature: void unmarshalFromData();
    wrapper.unmarshalFromData = function() {
      var currentDatetime = new Date();
      var currentMillis = currentDatetime.getTime();

      var cacheList = wrapper.customData?wrapper.customData['ValidationCache']:null;
      if (Array.isArray(cacheList)) {

        for (var i = 0; i < cacheList.length; i++) {
          var jsVal = cacheList[i];
          var validateName = jsVal["Name"];
          var expiryDateStr = jsVal["Expiry"];
          var expiryMillis = -1;
          if (expiryDateStr) {
            var expiryDate = new Date (expiryDateStr);
            expiryMillis = expiryDate.getTime();
          }

          if (expiryMillis == -1 || currentMillis < expiryMillis) {
            var validateInputs = readInputs(jsVal["Inputs"]);
            var result = readResult(jsVal["Result"]);
            if (validateName && validateInputs && result) {
              this.customValidateCache.cacheResult(validateName,
                               validateInputs,
                               result,
                               jsVal["Response"], expiryMillis);
            }
          }
        }
      }
      return result;
    };

    return wrapper;
  }


//*************************************************************************************************************
// Context Functionality
//*************************************************************************************************************

  // Function used to convert Java types to JavaScript
  function marshalToJS(value) {
    if (value && typeof value == "object" && "getClass" in value) {
      if (value.getClass().getName() == "com.synthesis.orwell.businesslogic.FinsurvJSContext$Undefined") {
        return undefined;
      } else if (value.getClass().getName() == "java.lang.String") {
        return String(value);
      } else if (value.getClass().getName() == "java.math.BigDecimal") {
        var num = value.doubleValue();
        if (!num)
          num = "0";
        return num;
      }
    }
    return value;
  }




  function wrapLookup(lookups) {
    var wrapLookup = {
      lookups : lookups
    };

    wrapLookup.isValidCountryCode = function(code) {
      for ( var i = 0; i < this.lookups.countries.length; i++) {
        if (this.lookups.countries[i].code === code) {
          return true;
        }
      }
      return false;
    };
    wrapLookup.isValidCurrencyCode = function(code) {
      for ( var i = 0; i < this.lookups.currencies.length; i++) {
        if (this.lookups.currencies[i].code === code) {
          return true;
        }
      }
      return false;
    };
    wrapLookup.isValidCategory = function(flow, code) {
      for ( var i = 0; i < this.lookups.categories.length; i++) {
        var category = this.lookups.categories[i];
        if (category.flow === flow && category.code.indexOf(code) == 0) {
          return true;
        }
      }
      return false;
    };
    wrapLookup.isValidSubCategory = function(flow, code, subcode) {
      var categories = [];
      var i;
      var category;
      for (i = 0; i < this.lookups.categories.length; i++) {
        category = this.lookups.categories[i];
        if (category.flow === flow && category.code.indexOf(code) == 0) {
          categories.push(category);
        }
      }
      var hasValidSubCode = false;
      var canHaveNoSubCode = false;
      if (categories.length > 0) {
        for (i = 0; i < categories.length; i++) {
          var pos = categories[i].code.indexOf("/");
          var scode = undefined;
          if (pos > -1)
            scode = categories[i].code.substr(pos + 1);
          if (scode) {
            if (scode === subcode) {
              hasValidSubCode = true;
              break;
            }
          } else {
            canHaveNoSubCode = true;
          }
        }
      }
      if (!hasValidSubCode && !subcode && canHaveNoSubCode)
        hasValidSubCode = true;
      return hasValidSubCode;
    };
    wrapLookup.isReportingQualifier = function(value) {
      return this.lookups.ReportingQualifier.indexOf(value) > -1;
    };
    wrapLookup.isNonResExceptionName = function(value) {
      return this.lookups.nonResidentExceptions.indexOf(value) > -1;
    };
    wrapLookup.isResExceptionName = function(value) {
      return this.lookups.residentExceptions.indexOf(value) > -1;
    };
    wrapLookup.isMoneyTransferAgent = function(value) {
      return this.lookups.moneyTransferAgents.indexOf(value) > -1;
    };
    wrapLookup.isValidProvince = function(value) {
      return this.lookups.provinces.indexOf(value) > -1;
    };
    wrapLookup.isValidCardType = function(value) {
      return this.lookups.cardTypes.indexOf(value) > -1;
    };
    wrapLookup.isValidInstitutionalSector = function(value) {
      for ( var co, i = 0; co = this.lookups.institutionalSectors[i]; i++) {
        if (co.code === value)
          return true;
      }
      return false;
    };
    wrapLookup.isValidIndustrialClassification = function(value) {
      for ( var co, i = 0; co = this.lookups.industrialClassifications[i]; i++) {
        if (co.code === value)
          return true;
      }
      return false;
    };
    wrapLookup.isValidCustomsOfficeCode = function(value) {
      for ( var co, i = 0; co = this.lookups.customsOffices[i]; i++) {
        if (co.code === value)
          return true;
      }
      return false;
    };

    wrapLookup.getCategory = function(flow, code, subcode) {
      var categories = [];
      var i;
      var category;
      for (i = 0; i < this.lookups.categories.length; i++) {
        category = this.lookups.categories[i];
        if (category.flow === flow && category.code.indexOf(code) === 0) {
          categories.push(category);
        }
      }
      if (categories.length > 0) {
        for (i = 0; i < categories.length; i++) {
          var pos = categories[i].code.indexOf("/");
          var scode = undefined;
          if (pos > -1) {
            scode = categories[i].code.substr(pos + 1);
          }
          if (scode) {
            if (scode === subcode) {
              return categories[i];
            }
          } else {
            if (!subcode) {
              return categories[i];
            }
          }
        }
      }
    };

    return wrapLookup;
  };


  function wrapCallbacks() {
    /*
    customValidation : [
      IVS_Validation: {
        func : function() {}
      },
      CCN_Validation: {
        func : function() {}
      }
    ]
    */
    var callbacks = {};
    var customValidateCache = null;
    var cacheHits = 0;
    var cachePuts = 0;

    function lookupScopedFieldValue(context, scope, field, moneyInd, ieInd, value) {
      var lookupScope = scope;
      var lookupField = field;
      var moneyInstance = (!(moneyInd==null))?moneyInd:context.currentMoneyInstance;
      var ieInstance = (!(ieInd==null))?ieInd:context.currentImportExportInstance;
      var lookupValue;
      var posScope = lookupField.indexOf("::");
      if (posScope > -1) {
        lookupScope = lookupField.substr(0, posScope);
        lookupField = lookupField.substr(posScope+2);
      }
      if (lookupScope === "function") {
        lookupValue = context.internalUtils[lookupField](context, value);
      }
      if (lookupScope === "transaction") {
        lookupValue = context.getTransactionField(lookupField);
      }
      if (lookupScope === "money") {
        if (!moneyInstance) {
          moneyInstance = 0;
        }
        lookupValue = context.getMoneyField(moneyInstance, lookupField);
      }
      if (lookupScope === "importexport") {
        if (!moneyInstance) {
          moneyInstance = 0;
        }
        if (!ieInstance) {
          ieInstance = 0;
        }
        lookupValue = context.getImportExportField(moneyInstance, ieInstance, lookupField);
      }
      return lookupValue;
    }

    function lookupScopedFieldValues(context, scope, fieldList, model, value) {
      var inds=[undefined,undefined],result = [];
      if(!(model==null)){
        inds = model.getIndeces();
      }
      for ( var i = 0; i < fieldList.length; i++) {
        result.push(lookupScopedFieldValue(context, scope, fieldList[i], inds[0], inds[1], value));
      }
      return result;
    }

    function processCall(name, value, scope, context, resultCallback, model) {
        if (!(name in callbacks)) {
            resultCallback('fail', '', 'The validation callback ' + name + ' has not been defined');
        } else {
            var callback = callbacks[name];

            var inputArray = lookupScopedFieldValues(context, scope, callback.inputs, model, value);

            // check to see if we have cached value
            var inputs = new_ValidationInputs(value, inputArray);
            var result = customValidateCache.getResult(name, inputs);
            if (result) {
              cacheHits = cacheHits + 1;
              resultCallback(result.status, result.code, result.message);
              return;
            }

            result = new_ValidationResult('busy', '', 'awaiting validation');
            
            if(callback.failOnBusy==true || callback.failOnBusy == "true"){
              result.status = "failbusy";
            }

            resultCallback(result.status, result.code, result.message);

            var handleResult = function(status, code, message, responseCache) {
                result = new_ValidationResult(status, code, message);
                cachePuts = cachePuts + 1;
                customValidateCache.cacheResult(name, inputs, result, responseCache);
                resultCallback(result.status, result.code, result.message);
            };

            // make call (function is of form -- function(value,arg1,...,argN,resultCallback) -- )
            var args = [value].concat(inputArray).concat([handleResult, scope, model]);
            callback.func.apply(context, args);
        }
    }

    function processDataCall(name, value, scope, context, resultCallback, model) {
      if (!(name in callbacks)) {
          resultCallback('fail', '', 'The validation callback ' + name + ' has not been defined');
      } else {
          var callback = callbacks[name];

          var inputArray = lookupScopedFieldValues(context, scope, callback.inputs, model, value);

          // check to see if we have cached value
          var inputs = new_ValidationInputs(value, inputArray);
          var result = customValidateCache.getResult(name, inputs);
          if (result) {
            cacheHits = cacheHits + 1;
            resultCallback(result.status, result.code, result.message);
            return;
          }

          // result = new_ValidationResult('busy', '', 'awaiting validation');
          result = new_DataRetrievalResult('busy', 'awaiting validation', {});

          if(callback.failOnBusy==true || callback.failOnBusy == "true"){
            result.status = "failbusy";
          }

          resultCallback(result.status, result.message, result.data);

          var handleResult = function(status, message, data, responseCache) {
              result = new_DataRetrievalResult(status, message, data);
              cachePuts = cachePuts + 1;
              customValidateCache.cacheResult(name, inputs, result, responseCache);
              resultCallback(result.status, result.message, result.data);
          };

          // make call (function is of form -- function(value,arg1,...,argN,resultCallback) -- )
          var args = [value].concat(inputArray).concat([handleResult, scope, model]);
          callback.func.apply(context, args);
      }
  }

    return {
      setCustomValidateCache: function (validationCache) {
        customValidateCache = validationCache;
      },
      registerValidationCallback: function (name, callback) {
        callbacks[name] = {};
        callbacks[name].func = callback;

        if (typeof arguments[2] === "object") {
          callbacks[name].failOnBusy = arguments[2].failOnBusy;
          [].splice.call(arguments,2,1);
        }
        // The rest of the arguments are the 'inputs'
        callbacks[name].inputs = [].splice.call(arguments,2);
        callbacks[name].cacheStrategy = new_CacheStrategy();
      },
      registerValidationCallbackEx: function (name, cacheStrategy, callback) {
        callbacks[name] = {};
        callbacks[name].cacheStrategy = cacheStrategy;
        callbacks[name].func = callback;
        // The rest of the arguments are the 'inputs'
        callbacks[name].inputs = [].splice.call(arguments,3);
      },
      // Java signature: public CachePeriod getCachePeriodByCallAndStatus(String name, StatusType statusType) {
      getCachePeriodByCallAndStatus: function (name, statusType) {
        if (name in callbacks) {
          return callbacks[name].cacheStrategy.getCacheStrategyByStatus(statusType).cachePeriod;
        }
        return CachePeriod.FOREVER;
      },
      validate: function (name, value, scope, context, resultCallback) {
        processCall(name, value, scope, context, resultCallback);
      },
      validateButton: function (model, name, value, scope, context, resultCallback) {
        processCall(name, value, scope, context, resultCallback, model);
      },
      externalDataRetrieval: function (model, name, value, scope, context, resultCallback) {
        processDataCall(name, value, scope, context, resultCallback, model);
      },
      cacheHitCount: function() {
        return cacheHits;
      },
      cachePutCount: function() {
        return cachePuts;
      },
      getValidationCallbackEx: function (name) {
        return callbacks[name];
      }
    };
  }


  function wrapContext(obj, lookups, customData) {
    var validationRegistry = wrapCallbacks();
    var customValidateCache = new_CustomValidateCache(validationRegistry);

    validationRegistry.setCustomValidateCache(customValidateCache);

    var wrapper = {
      section : "",
      flow : "",
      currency : "",
      domesticCurrency : "",
      currentField: "",
      currentMoneyInstance : 0,
      currentImportExportInstance : 0,
      categories : [],
      mainCategories : [],
      cache : new_PersistentCache (customData, customValidateCache),
      callbacks : validationRegistry,
      internalUtils : {}
    };

    wrapper.buildCategories = function() {
      wrapper.mainCategories = [];
      wrapper.categories = [];
      for ( var i = 0; i < obj.getMoneySize(); i++) {
        var cat = marshalToJS(obj.getMoneyField(i, "CategoryCode"));
        wrapper.mainCategories.push(cat);

        var fullcat = cat;
        var subcat = marshalToJS(obj.getMoneyField(i, "CategorySubCode"));
        if (subcat)
          fullcat += "/" + subcat;
        wrapper.categories.push(fullcat);
      }
    };

    wrapper.getCategories = function(){
      wrapper.buildCategories();
      return wrapper.categories;
    }

    var qualifier = marshalToJS(obj.getTransactionField("ReportingQualifier"));

    if (qualifier === "BOPCUS")
      wrapper.section = "A";
    else if (qualifier === "NON RESIDENT RAND")
      wrapper.section = "B";
    else if (qualifier === "NON REPORTABLE")
      wrapper.section = "C";
    else if (qualifier === "INTERBANK")
      wrapper.section = "D";
    else if (qualifier === "BOPCARD RESIDENT")
      wrapper.section = "E";
    else if (qualifier === "BOPCARD NON RESIDENT")
      wrapper.section = "F";
    else if (qualifier === "BOPDIR")
      wrapper.section = "G";

    wrapper.flow = marshalToJS(obj.getTransactionField("Flow"));
    wrapper.currency = marshalToJS(obj.getTransactionField("FlowCurrency"));

    var localCurr = obj.getTransactionField("DomesticCurrency");
    if (localCurr)
      wrapper.domesticCurrency = localCurr;
    else
      wrapper.domesticCurrency = "ZAR";

    wrapper.buildCategories();

    // unmarshal validation cache
    wrapper.cache.unmarshalFromData();

    wrapper.marshalCacheToCustomData = function () {
      wrapper.cache.marshalToData();
    };

    wrapper.getCustomValue = function(field) {
      if (!(wrapper.cache.customData && (typeof (wrapper.cache.customData[field]) !== 'undefined')))
        return undefined;
      return wrapper.cache.customData[field];
    };

    wrapper.getCacheHitCount = function() {
      return this.callbacks.cacheHitCount();
    };

    wrapper.getCachePutCount = function() {
      return this.callbacks.cachePutCount();
    };

    wrapper.buttonIsClicked = function (field) {
        if ("buttonIsClicked" in obj) {
            return obj.buttonIsClicked(field, this.currentMoneyInstance, this.currentImportExportInstance);
        }
        else
            return false;
    };

    wrapper.fieldHasFocus = function (field) {
        if ("fieldHasFocus" in obj) {
            return obj.fieldHasFocus(field, this.currentMoneyInstance, this.currentImportExportInstance);
        }
        else
            return false;
    };

    wrapper.getTransactionField = function(field) {
      /*if (wrapper.cache[field] === undefined) {
       wrapper.cache[field] = marshalToJS(obj.getTransactionField(field));
       }
       return wrapper.cache[field];*/
      return marshalToJS(obj.getTransactionField(field));
    };

    wrapper.getMoneySize = function() {
      return obj.getMoneySize();
    };

    wrapper.getMoneyField = function(instance, field) {
      /*var key = "ma[" + instance +"]" + field;
       if (wrapper.cache[key] === undefined) {
       wrapper.cache[key] = marshalToJS(obj.getMoneyField(instance, field));
       }
       return wrapper.cache[key];*/
      return marshalToJS(obj.getMoneyField(instance, field));
    };

    wrapper.getImportExportSize = function(moneyInstance) {
      return obj.getImportExportSize(moneyInstance);
    };

    wrapper.getImportExportField = function(moneyInstance, instance, field) {
      return marshalToJS(obj.getImportExportField(moneyInstance, instance, field));
    };


    wrapper.logTransactionLifetimeEvent = function (type, field, code, msg) {
        obj.logTransactionEvent(type, field, code, msg, true);
    };

    wrapper.logMoneyLifetimeEvent = function (type, instance, field, code, msg) {
        obj.logMoneyEvent(type, instance, field, code, msg, true);
    };

    wrapper.logImportExportLifetimeEvent = function (type, moneyInstance, instance, field, code, msg) {
        obj.logImportExportEvent(type, moneyInstance, instance, field, code, msg, true);
    };

    wrapper.logTransactionEvent = function(type, field, code, msg) {
      obj.logTransactionEvent(type, field, code, msg, false);
    };

    wrapper.logMoneyEvent = function(type, instance, field, code, msg) {
      obj.logMoneyEvent(type, instance, field, code, msg, false);
    };

    wrapper.logImportExportEvent = function(type, moneyInstance, instance, field, code, msg) {
      obj.logImportExportEvent(type, moneyInstance, instance, field, code, msg, false);
    };

    wrapper.logTransactionDisplayEvent = function(type, field, value) {
      obj.logTransactionDisplayEvent(type, field, value);
    };

    wrapper.logMoneyDisplayEvent = function(type, instance, field, value) {
      obj.logMoneyDisplayEvent(type, instance, field, value);
    };

    wrapper.logImportExportDisplayEvent = function(type, moneyInstance, instance, field, value) {
      obj.logImportExportDisplayEvent(type, moneyInstance, instance, field, value);
    };

    wrapper.lookups = wrapLookup(lookups);

    return wrapper;
  }

  return {
    wrapLookup:wrapLookup,
    wrapContext:wrapContext,

    wrapCallbacks: wrapCallbacks,
    StatusType: StatusType,
    CachePeriod: CachePeriod,
    new_CacheStrategy: new_CacheStrategy,
    new_ValidationInputs: new_ValidationInputs,
    new_ValidationResult: new_ValidationResult,
    new_CustomValidateCache: new_CustomValidateCache
  }
});
