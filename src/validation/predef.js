define(function (require) {

  function isFunction(t){
    return typeof t == 'function';
  }

  function resolveConstant(k){
    return isFunction(k)? k() : k;
  }
  //var _ = require("../../node_modules/lodash/lodash.min");

  ////------------------------------------------------------
  //// Display support functions.
  ////------------------------------------------------------

  function displaybase(type, value, srcField, assertion) {
    var result = {
      type     : type,
      value    : value,
      srcField : srcField,
      assertion: assertion
    };
    result.onInflow = function () {
      this.flow = "IN";
      return this;
    };
    result.onOutflow = function () {
      this.flow = "OUT";
      return this;
    };
    result.onSection = function (sections) {
      this.section = sections;
      return this;
    };
    result.onCategory = function (categories) {
      if(arguments[1]!=null) throw new Error("Incorrect usage of onCategory - ony one parameter allowed and it has to be a string or array")
      if (typeof categories == "string") {
        this.categories = [];
        this.categories.push(categories);
      } else
        this.categories = categories;
      return this;
    };
    result.notOnCategory = function (categories) {
      if(arguments[1]!=null) throw new Error("Incorrect usage of notOnCategory - ony one parameter allowed and it has to be a string or array")
      if (typeof categories == "string") {
        this.notCategories = [];
        this.notCategories.push(categories);
      } else
        this.notCategories = categories;
      return this;
    };
    return result;
  }

  function returnTrue(context, value) {
    return true;
  }

  function returnFalse(context, value) {
    return false;
  }

  function extend(){
    return displaybase("EXTEND", null, null, returnFalse);
  }


  function excludeValue(value, assertion) {
    
    var actualValue;

    if(typeof value == 'function') {
      actualValue = value()
    } else {
      actualValue = value;
    }

    if (!assertion)
      return displaybase("EXCLUDE", actualValue, null, returnTrue);
    else
      return displaybase("EXCLUDE", actualValue, null, assertion);
  }

  function limitValue(value, assertion) {
    if (!assertion)
      return displaybase("LIMIT", value, null, returnTrue);
    else
      return displaybase("LIMIT", value, null, assertion);
  }

  function filterValue(filterFn, assertion) {
    if (!assertion)
      return displaybase("FILTER", filterFn, null, returnTrue);
    else
      return displaybase("FILTER", filterFn, null, assertion);
  }

  function disableValue(value, assertion) {
    if(!assertion)
      return displaybase("DISABLEVALUE", value, null, returnTrue);
    else
      return displaybase("DISABLEVALUE", value, null, assertion);
  }


  function show(assertion) {
    if (!assertion)
      return displaybase("SHOW", null, null, returnTrue);
    else
      return displaybase("SHOW", null, null, assertion);
  }

  function hide(assertion) {
    if (!assertion)
      return displaybase("HIDE", null, null, returnTrue);
    else
      return displaybase("HIDE", null, null, assertion);
  }

  function enable(assertion) {
    if (!assertion)
      return displaybase("ENABLE", null, null, returnTrue);
    else
      return displaybase("ENABLE", null, null, assertion);
  }

  function disable(assertion) {
    if (!assertion)
      return displaybase("DISABLE", null, null, returnTrue);
    else
      return displaybase("DISABLE", null, null, assertion);
  }

  function clearValue(assertion) {
    if (!assertion)
      return displaybase("CLEAR", null, null, returnTrue);
    else
      return displaybase("CLEAR", null, null, assertion);
  }

  function setValue(value, srcField, assertion) {
    if (!assertion)
      return displaybase("SET", value, srcField, returnTrue);
    else
      return displaybase("SET", value, srcField, assertion);
  }


  function setLabel(value, srcField, assertion) {
    if (!assertion)
      return displaybase("SETLABEL", value, srcField, returnTrue);
    else
      return displaybase("SETLABEL", value, srcField, assertion);
  }

  function appendValue(value, srcField, assertion) {
    if (!assertion)
      return displaybase("APP", value, srcField, returnTrue);
    else
      return displaybase("APP", value, srcField, assertion);
  }

  function map(map, key) {
    return (map && map[key] ? map[key] : "")
  }

  function stringMapper(map, string) {
    if(map[string]) return map[string];
    var matcher = /(?:{{([\w]+)}})/;
    for (var match = string.match(matcher); match; match = string.match(matcher)) {
      string = string.replace(match[0],map[match[1]]);
    }
    return string;
  }


  ////------------------------------------------------------
  //// Validation support function
  ////------------------------------------------------------


  function rulebase(type, name, validate, code, message, assertion) {
    var result = {
      type     : type,
      name     : name,
      code     : code,
      message  : message,
      validate : validate,
      assertion: assertion
    };
    result.onInflow = function () {
      this.flow = "IN";
      return this;
    };
    result.onOutflow = function () {
      this.flow = "OUT";
      return this;
    };
    result.onSection = function (sections) {
      this.section = sections;
      return this;
    };
    result.onCategory = function (categories) { 
      if(arguments[1]!=null) throw new Error("Incorrect usage of onCategory - ony one parameter allowed and it has to be a string or array")
      if (typeof categories == "string") {
        this.categories = [];
        this.categories.push(categories);
      } else
        this.categories = categories;
      return this;
    };
    result.notOnCategory = function (categories) {
      if(arguments[1]!=null) throw new Error("Incorrect usage of onCategory - ony one parameter allowed and it has to be a string or array")
      if (typeof categories == "string") {
        this.notCategories = [];
        this.notCategories.push(categories);
      } else
        this.notCategories = categories;
      return this;
    };
    return result;
  }

  function failure(name, code, message, assertion) {
    return rulebase("ERROR", name, null, code, message, assertion);
  }

  function warning(name, code, message, assertion) {
    return rulebase("WARNING", name, null, code, message, assertion);
  }

  function validate(name, validate, assertion) {
    return rulebase("VALIDATE", name, validate, "XXX", "Ignore me", assertion);
  }

  function deprecated(name, code, message, assertion) {
    return rulebase("DEPRECATED", name, null, code, message, assertion);
  }

  function ignore(name) {
    return rulebase("WARNING", name, null, "XXX", "Ignore me", function () {
      return false;
    });
  }

  function message(name, code, message) {
    return rulebase("MESSAGE", name, null, code, message, null);
  }

  function document(name,type,description,assertion){
    return rulebase("DOCUMENT", name, null, type, description, assertion);
  }

  function isTooLong(constant) {
    return function (context, value) {
      if (value && typeof value === "string")
        return value.trim().length > constant;
      return false;
    };
  }

  function isTooShort(constant) {
    return function (context, value) {
      if (value && typeof value === "string")
        return value.trim().length < constant;
      return false;
    };
  }

  function isWrongLength(constant) {
    return function (context, value) {
      if (value && typeof value === "string")
        return value.trim().length != constant;
      return false;
    };
  }

  ////------------------------------------------------------
  //// Internal Validation support function
  ////------------------------------------------------------

  function localAmountForIDNumber(context, value) {
    var hashMap = new Map();
    var idNumber = "";

    for (i = 0; i < context.getMoneySize(); i++) {
      var subjectField = context.getMoneyField(i, "AdHocRequirement.Subject");
      var AmountField = context.getMoneyField(i, _export.getMap("{{LocalValue}}"));

      if (subjectField == "SDA") {
        if(AmountField != null) {
          AmountField = AmountField.replace(",", "");
          if (context.getMoneyField(i, "ThirdParty.Individual.IDNumber"))
            idNumber = context.getMoneyField(i, "ThirdParty.Individual.IDNumber");
          else if (context.getTransactionField("Resident.Individual.IDNumber"))
            idNumber = context.getTransactionField("Resident.Individual.IDNumber");

          if(idNumber != "") {
            if (hashMap.has(idNumber)) {
              var originalAmount = hashMap.get(idNumber);
              var newAmount = 0.0;
              newAmount = parseFloat(originalAmount) + parseFloat(AmountField);
              hashMap.set(idNumber, newAmount);
            }
            else {
              hashMap.set(idNumber, AmountField);
            }
          }
        }
      }
    }

    return hashMap.get(value) ? hashMap.get(value) : 0;
  }

  function localAmountForIDNumberFIA(context, value) {
    debugger;
    var hashMap = new Map();
    var idNumber = "";

    for (i = 0; i < context.getMoneySize(); i++) {
      // var subjectField = context.getMoneyField(i, "AdHocRequirement.Subject");
      var categoryCode = context.getMoneyField(i, "CategoryCode")
      
      var AmountField = context.getMoneyField(i, _export.getMap("{{LocalValue}}"));

      // if (subjectField == "FIA") {
      if(categoryCode == "512" || categoryCode == "513") {
        if(AmountField != null) {
          AmountField = AmountField.replace(",", "");
          if (context.getMoneyField(i, "ThirdParty.Individual.IDNumber"))
            idNumber = context.getMoneyField(i, "ThirdParty.Individual.IDNumber");
          else if (context.getTransactionField("Resident.Individual.IDNumber"))
            idNumber = context.getTransactionField("Resident.Individual.IDNumber");

          if(idNumber != "") {
            if (hashMap.has(idNumber)) {
              var originalAmount = hashMap.get(idNumber);
              var newAmount = 0.0;
              newAmount = parseFloat(originalAmount) + parseFloat(AmountField);
              hashMap.set(idNumber, newAmount);
            }
            else {
              hashMap.set(idNumber, AmountField);
            }
          }
        }
      }
    }

    return hashMap.get(value) ? hashMap.get(value) : 0;
  }
  
  function customDisableCategoryCodes(contect, value) {
    var disableCategoryCodes = ["101"];

    return disableCategoryCodes;
  }

  var internalUtils = {
    localAmountForIDNumber : localAmountForIDNumber,
    localAmountForIDNumberFIA : localAmountForIDNumberFIA,
    customDisableCategoryCodes : customDisableCategoryCodes
  }

  ////------------------------------------------------------

  // The export object which is the DSL...
  var _export = {
    // Default string mappings.
    mappings: {LocalCurrencySymbol: "R", LocalCurrencyName: "Local Currency", LocalCurrency: "ZAR", LocalValue: "LocalValue", Regulator: "Regulator"},

    // Display API
    ////------------------------------------------------------
    extend       : extend,
    excludeValue : excludeValue,
    limitValue   : limitValue,
    filterValue  : filterValue,
    disableValue  : disableValue,
    show         : show,
    hide         : hide,
    enable       : enable,
    disable      : disable,
    clearValue   : clearValue,
    setValue     : setValue,
    setLabel     : setLabel,
    appendValue  : appendValue,

    // Validation API
    ////------------------------------------------------------
    failure      : failure,
    warning      : warning,
    validate     : validate,
    deprecated   : deprecated,
    ignore       : ignore,
    isTooLong    : isTooLong,
    isTooShort   : isTooShort,
    isWrongLength: isWrongLength,
    message      : message,
    returnTrue   : returnTrue,
    document     : document,

    // Internal Functions
    ////------------------------------------------------------
    internalUtils: internalUtils,

    //mappings
    getMap: function(str){
      return stringMapper(_export.mappings,str);
    },
    map : function(str){
      return function(){
        return _export.getMap(str);
      }
    }
  };

  ////------------------------------------------------------
  // Cetegory helpers
  ////------------------------------------------------------

   _export.categoryNotInList = function(category, mainCategory, ruleList) {
    var result = true;
    for (var ruleItem, i = 0; ruleItem = ruleList[i]; i++) {
      var pos = ruleItem.indexOf('/');
      if (pos == -1) {
        if (mainCategory === ruleItem) {
          result = false;
          break;
        }
      } else {
        if (category === ruleItem) {
          result = false;
          break;
        }
      }
    }
    return result;
  }

   _export.categoriesNotInList = function(categories, mainCategories, ruleList) {
    for (var i = 0; i < categories.length; i++) {
      if (_export.categoryNotInList(categories[i], mainCategories[i], ruleList))
        return true;
    }
    return false;
  }

  /**
   * Set up the Function class' 'and' and 'or' implementations to allow for composable logic
   */
  _export.fnAndOrUp = function(){

    // Adding logical operators to the Function prototype
    Function.prototype.or = function (argFunction) {
      var invokingFunction = this;
      return function () {
        return invokingFunction.apply(this, arguments) || argFunction.apply(this, arguments);
      }
    };

    Function.prototype.and = function (argFunction) {
      var invokingFunction = this;
      return function () {
        return invokingFunction.apply(this, arguments) && argFunction.apply(this, arguments);
      }
    };

  }

  /**
   * Set up the Function class' 'and' and 'or' implementations to allow for composable string output from DSL compositions.
   */
  _export.fnAndOrStrUp = function(){

    // Adding logical operators to the Function prototype
    Function.prototype.or = function (argFunction) {
      var invokingFunction = this;
      return function () {
        return invokingFunction.apply(this, arguments) + ".or(" + argFunction.apply(this, arguments) + ")";
      }
    };

    Function.prototype.and = function (argFunction) {
      var invokingFunction = this;
      return function () {
        return invokingFunction.apply(this, arguments) + ".and(" + argFunction.apply(this, arguments) + ")";
      }
    };

  }

  /**
   * Scared the 'and' and 'or' implementations may mess with other code? Call this.
   */
  _export.fnAndOrDown = function(){
    delete Function.prototype.or;
    delete Function.prototype.and;
  }



// Defaults validation rule parameters
  var rule_parameters = {
    todayDate : new Date(),
    goLiveDate: new Date(2013, 7, 19)
  };

  _export.setRuleParameters = function (_params) {
    rule_parameters = _params;
  }

// Constants for the evaluation definitions
  var rep = {
    REPORTABLE    : 'REPORTABLE',
    ZZ1REPORTABLE : 'ZZ1REPORTABLE',
    PROVREPORTABLE: 'PROVREPORTABLE',
    NONREPORTABLE : 'NONREPORTABLE',
    EXCLUDED      : 'EXCLUDED',
    UNSUPPORTED   : 'UNSUPPORTED',
    UNKNOWN       : 'UNKNOWN'
  };
  var flowDir = {
    IN : 'IN',
    OUT: 'OUT'
  };
  var curr = {
    ZAR  : 'ZAR',
    OTHER: '-'
  };
  var cnt = {
    ZA: 'ZA',
    __: '-'
  };
  var at = {
    _NONE_: '',
    RE_OTH: 'RESIDENT OTHER',
    RE_CFC: 'CFC RESIDENT',
    RE_FCA: 'FCA RESIDENT',
    _CASH_: 'CASH',
    VOSTRO: 'VOSTRO',
    NR_OTH: 'NON RESIDENT OTHER',
    NR_RND: 'NON RESIDENT RAND',
    NR_FCA: 'NON RESIDENT FCA',
    RE_FBC: 'RES FOREIGN BANK ACCOUNT'
  };
  var repBy = {
    ORIG: 'Originating',
    RECV: 'Receiving'
  };
  var period = {
    MAR14: new Date(2014, 2, 14),
    DEC14: new Date(2014, 11, 14)
  };


  _export.not = function not(func) {
    return function (context, value) {
      return !func(context, value);
    }
  }



  function leftPadZero(i) {
    return (i < 10) ? "0" + i : "" + i;
  }

  function formatDate(mydate) {
    return leftPadZero(mydate.getFullYear()) + "-" + leftPadZero(1 + mydate.getMonth()) + "-" + leftPadZero(mydate.getDate());
  }

// Functions used by the validations rules
  _export.notEmpty = function notEmpty(context, value) {
    return value && (typeof value != "string" || value.trim().length > 0);
  }

  _export.isComplexValue = function (context, value) {
    if (!value)
      return true;
    return typeof value === "object";
  }

  _export.notComplexValue = function (context, value) {
    if (!value)
      return false;
    return !(typeof value === "object");
  }

  _export.isArrayValue = function (context, value) {
    if (!value)
      return true;

    var isArray = false;
    if (Array.isArray)
      isArray = Array.isArray(value);
    else {
      isArray = Object.prototype.toString.call(value) === "[object Array]";
    }
    return isArray;
  }

  _export.notArrayValue = function (context, value) {
    if (!value)
      return false;

    return !_export.isArrayValue(context, value);
  }

  _export.isSimpleValue = function (context, value) {
    if (!value)
      return true;

    return typeof value === "string" || typeof value === "number" || typeof value === "boolean" || typeof value === "symbol";
  }

  _export.notSimpleValue = function (context, value) {
    if (!value)
      return false;

    return !_export.isSimpleValue(context, value);
  }

  _export.notValidNumber = function (context, value) {
    return !_export.isNumber(context, value);
  }

  _export.isButtonPressed = function (field) {
      return function (context, value) {
          return context.buttonIsClicked(resolveConstant(field));
      }
  }

  _export.isFocusOnField = function (field) {
      return function (context, value) {
          return context.fieldHasFocus(resolveConstant(field));
      }
  }
  _export.notFocusOnField = function(field){
    return function(context, value) {
      return !context.fieldHasFocus(resolveConstant(field));
    }
  }


  _export.isDefined = function isDefined(context, value) {
    return value != null;
  }

  _export.isEmpty = function isEmpty(context, value) {
    return !value || (typeof value == "string" && value.trim().length == 0);
  }

  _export.hasAdditionalSpaces = function hasAdditionalSpaces(context, value) {
    return value && (typeof value == "string" && (value.indexOf('  ') > -1 || value.trim() !== value));
  }

  _export.hasSpaces = function hasSpaces(context, value) {
    return value && (typeof value == "string" && (value.indexOf(' ') > -1 || value.trim() !== value));
  }

  _export.hasValue = function hasValue(constant) {
    return function (context, value) {
      if(value==null && constant == null) return true;
      return value && value === resolveConstant(constant);
    }
  }

  _export.notValue = function notValue(constant) {
    return function (context, value) {
      return !value || value != resolveConstant(constant);
    }
  }

  _export.hasPattern = function hasPattern(pattern) {
    return function (context, value) {
      return value && value.match(pattern);
    }
  }

  _export.notPattern = function notPattern(pattern) {
    return function (context, value) {
      return !value || !value.toString().match(pattern);
    }
  }

  _export.hasDatePattern = function hasDatePattern(context, value) {
    return value && (typeof value == "string" && value.match(/^(19|20|21)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/));
  }

  _export.notDatePattern = function notDatePattern(context, value) {
    return !value || !(typeof value == "string" && value.match(/^(19|20|21)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/));
  }

  _export.hasCustomField = function hasCustomField(field){
    return function (context, value) {
      return !!(context.getCustomValue(resolveConstant(field)));
    }
  }

  _export.hasCustomValue = function hasCustomValue(field, _value) {
    return function (context, value) {
      return (context.getCustomValue(resolveConstant(field)) === resolveConstant(_value));
    }
  }


  _export.notCustomValue = function notCustomValue(field, _value) {
    return function (context, value) {
      return (context.getCustomValue(resolveConstant(field)) !== resolveConstant(_value));
    }
  }

  /**
  Used to see if the current field value is in a lookup, by optional lookupKey
   @param:lookupName The name of the lookup to search
   @param:lookupKey Optional lookup key if the lookup is not a simple array
   */
  _export.isInLookup = function isInLookup(lookupName, lookupKey) {
    return function (context, value) {
      var lookup = context.lookups.lookups[lookupName];
      if (!!lookup) {
        return lookup.find(function (item) {
          return (!!lookupKey ?
              (item[lookupKey] == value)
              : (item == value)
          )
        })
      }
    }
  }

  /**

  Used to test if values match on compound lookups. It does not make sense to use this on a simple lookup.

  @param:lookupName The name of the lookup to search
  @param:lookupKey  The lookup key to use when matching against the field value.
  @param:key Once the lookup is found evaluate against this key.
  @param:lookupValue The value to test against the key of the found lookup item. If not defined, will just check existence (Optional)
   */
  _export.hasLookupValue = function hasLookupValue(lookupName, lookupKey, key, lookupValue) {
    return function (context, value) {
      var lookup = context.lookups.lookups[lookupName];
      if (!!lookup) {
        var item = lookup.find( function (item) {
          return ((item[lookupKey] == value) || (''+item[lookupKey] == ''+value));
        })
        if (!!item) {
          return (typeof lookupValue !== 'undefined') ? ((item[key] == lookupValue) || (''+item[key] == ''+lookupValue)) : (!!item[key]);
        }
      }
    }
  }

  _export.hasLookupTransactionFieldValue = function(lookupName, lookupKey, key, field) {
    return function (context, value) {
      var fieldValue = context.getTransactionField(resolveConstant(field));
      var lookup = context.lookups.lookups[lookupName];
      if (!!lookup) {
        var item = lookup.find( function (item) {
          return  (item[lookupKey] == value)
        })
        if(!!item){
          return (item[key]==fieldValue);
        }
      }
    }
  }

  _export.dealerTypeADLA = function dealerTypeADLA(context, value) {
    return (context.getCustomValue("DealerType") === "ADLA");
  }

  _export.dealerTypeAD = function dealerTypeAD(context, value) {
    return (context.getCustomValue("DealerType") === "AD");
  }

  _export.importUndertakingClient = function(context, value) {
    var iuc = context.getCustomValue("LUClient");
    return iuc && (iuc === "Y");
  }

  _export.notImportUndertakingClient = function(context, value) {
    var iuc = context.getCustomValue("LUClient");
    return iuc && (iuc === "N");
  }

  _export.dealerTypeADLA = function dealerTypeADLA(context, value) {
    return (context.getCustomValue("DealerType") === "ADLA");
  }

  _export.notAccountFLow = function notAccountFLow(context, value) {
    var accFlow = context.getCustomValue("AccountFlow");
    return accFlow && accFlow != context.flow;
  }

  _export.isInflow = function isInflow(context, value) {
    return context.flow === "IN";
  }

  _export.notInflow = function notInflow(context, value) {
    return context.flow !== "IN";
  }


  _export.isOutflow = function isOutflow(context, value) {
    return context.flow === "OUT";
  }

  _export.notOutflow = function notOutflow(context, value) {
    return context.flow !== "OUT";
  }

  _export.isPositive = function isPositive(context, value) {
    var test = Number(value);
    return test && _export.isNumber(context, test) && test >= 0;
  }

  _export.isNumber = function isNumber(context, value) {
    return (typeof value === "string" && value.match(/\d+(\.\d{1,2})?/)) ||
      ((typeof value === "number") && isFinite(value)) &&  
        ((Number.isNaN) ? !Number.isNaN(value) : 
          ((typeof value === "number") && isFinite(value))); 
        // Bit of a hack, but it's for IE9 (isNaN isn't on IE9.. How sad is that?)
  };

  _export.isNegative = function isNegative(context, value) {
    var test = Number(value);
    return test && _export.isNumber(context, test) && test < 0;
  }

  _export.hasValueIn = function hasValueIn(constant) {
    return function (context, value) {
      var _constant = resolveConstant(constant);
      if (value) {
        if (typeof _constant === "string") {
          if (value === _constant)
            return true;
        } else {
          for (var i = 0; i < _constant.length; i++) {
            if (value === _constant[i])
              return true;
          }
        }
      }
      return false;
    }
  }

  _export.notValueIn = function notValueIn(constant) {
    return function (context, value) {
      var _constant = resolveConstant(constant);
      if (value) {
        if (typeof _constant === "string") {
          if (value === _constant)
            return false;
        } else {
          for (var i = 0; i < _constant.length; i++) {
            if (value === _constant[i])
              return false;
          }
        }
      }
      return true;
    }
  }

  _export.isMissingField = function isMissingField(fields) {
    return function (context, value) {
      var _fields = resolveConstant(fields);
      if (value) {
        if (typeof _fields === "string") {
          if (_fields in value)
            return false;
        } else {
          for (var i = 0; i < _fields.length; i++) {
            if (_fields[i] in value)
              return false;
          }
        }
      }
      return true;
    }
  }

  _export.isCurrencyIn = function isCurrencyIn(currencies) {
    return function (context, value) {
      var _currencies = resolveConstant(currencies);
      var currency = context.getTransactionField("FlowCurrency");
      if (currency) {
        if (typeof _currencies === "string") {
          if (currency === _currencies)
            return true;
        } else {
          for (var i = 0; i < _currencies.length; i++) {
            if (currency === _currencies[i])
              return true;
          }
        }
      }
      return false;
    }
  }

  _export.notCurrencyIn = function notCurrencyIn(currencies) {
    return function (context, value) {
      var _currencies = resolveConstant(currencies);
      var currency = context.getTransactionField("FlowCurrency");
      if (currency) {
        if (typeof _currencies === "string") {
          if (currency === _currencies)
            return false;
        } else {
          for (var i = 0; i < _currencies.length; i++) {
            if (currency === _currencies[i])
              return false;
          }
        }
      }
      return true;
    }
  }

  _export.notMatchToCurrency = function notMatchToCurrency(context, value) {
    if ((typeof value == "string" && value.length == 2)) {
      var curr = context.getTransactionField("FlowCurrency");
      if (curr.substr(0, 2) === value)
        return false;
    }
    return true;
  }

  _export.notSumForeignValue = function notSumForeignValue(context, value) {
    var total = 0;
    var sumValue = context.getTransactionField("TotalForeignValue");
    for (var i = 0; i < context.getMoneySize(); i++) {
      var fvalue = context.getMoneyField(i, "ForeignValue");
      if (fvalue)
        total += Number(fvalue);
    }
    var a = Math.round(Number(sumValue) * 100);
    var b = Math.round(total * 100);
    return a != b;
  }

  _export.notSumTotalValue = function notSumTotalValue(context, value) {
    var total = 0;
    var sumValue = context.getTransactionField("TotalValue");
    for (var i = 0; i < context.getMoneySize(); i++) {
      var fvalue = context.getMoneyField(i, "ForeignValue");
      if (fvalue)
        total += Number(fvalue);
      var rvalue = context.getMoneyField(i, _export.getMap("LocalValue"));
      if (rvalue)
        total += Number(rvalue);
    }
    var a = Math.round(Number(sumValue) * 100);
    var b = Math.round(total * 100);
    return a != b;
  }

  _export.notSumZAREquivalent = function notSumZAREquivalent(context, value) {
    var total = 0;
    var sumValue = context.getTransactionField("ZAREquivalent");
    for (var i = 0; i < context.getMoneySize(); i++) {
      var fvalue = context.getMoneyField(i, _export.getMap("LocalValue"));
      if (fvalue)
        total += Number(fvalue);
    }
    var a = Math.round(Number(sumValue) * 100);
    var b = Math.round(total * 100);
    return a != b;
  }

  _export.notSumLocalValue = function notSumLocalValue(context, value) {
    var total = 0;
    for (var i = 0; i < context.getMoneySize(); i++) {
      var rvalue = context.getMoneyField(i, _export.getMap("LocalValue"));
      if (rvalue)
        total += Number(rvalue);
    }
    var a = Math.round(Number(value) * 100);
    var b = Math.round(total * 100);
    return a != b;
  }

  _export.notSumCardValue = function notSumCardValue(context, value) {
    var total = 0;
    for (var i = 0; i < context.getMoneySize(); i++) {
      var c1value = context.getMoneyField(i, _export.getMap("ForeignCardHoldersPurchases{{LocalValue}}"));
      var c2value = context.getMoneyField(i, _export.getMap("ForeignCardHoldersCashWithdrawals{{LocalValue}}"));
      if (c1value)
        total += Number(c1value);
      if (c2value)
        total += Number(c2value);
    }
    var a = Math.round(Number(value) * 100);
    var b = Math.round(total * 100);
    return a != b;
  }

  _export.hasSumLocalValue = function(operation, constant) {
    return function(context, value) {
      var total = 0;
      for ( var i = 0; i < context.getMoneySize(); i++) {
        var rvalue = context.getMoneyField(i, _export.getMap("LocalValue"));
        if (rvalue)
          total += Number(rvalue);
      }
      var givenValue = Math.round(Number(resolveConstant(constant)) * 100);
      var randSumValue = Math.round(total * 100);
      if (operation === ">")
        return randSumValue > givenValue;
      if (operation === ">=")
        return randSumValue >= givenValue;
      if (operation === "<")
        return randSumValue < givenValue;
      if (operation === "<=")
        return randSumValue <= givenValue;
      if (operation === "=")
        return randSumValue == givenValue;
    }
  };

  _export.hasResException = function hasResException(type) {
    return function (context, value) {
      var exception = context.getTransactionField("Resident.Exception.ExceptionName");
      if (exception)
        return exception === type;
      return false;
    }
  }

  _export.notResException = function notResException(type) {
    return function (context, value) {
      var exception = context.getTransactionField("Resident.Exception.ExceptionName");
      if (exception)
        return exception != type;
      return true;
    }
  }

  _export.hasTransactionField = function hasTransactionField(field) {
    return function (context, value) {
      var fieldValue = context.getTransactionField(resolveConstant(field));
      if (fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0))
        return true;
      else
        return false;
    }
  }

  _export.notTransactionField = function notTransactionField(field) {
    return function (context, value) {
      var fieldValue = context.getTransactionField(resolveConstant(field));
      if (!fieldValue || (typeof fieldValue == "string" && fieldValue.trim().length == 0))
        return true;
      else
        return false;
    }
  }

  _export.hasResidentField = function hasResidentField(field) {
    return function (context, value) {
      var _field = resolveConstant(field);
      var fieldValue = context.getTransactionField("Resident.Individual." + _field);
      if (!fieldValue)
        fieldValue = context.getTransactionField("Resident.Entity." + _field);
      if (fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0))
        return true;
      else
        return false;
    }
  }

  _export.notResidentField = function notResidentField(field) {
    return function (context, value) {
      var _field = resolveConstant(field);
      var fieldValue = context.getTransactionField("Resident.Individual." + _field);
      if (!fieldValue)
        fieldValue = context.getTransactionField("Resident.Entity." + _field);
      if (!fieldValue || (typeof fieldValue == "string" && fieldValue.trim().length == 0))
        return true;
      else
        return false;
    }
  }

  _export.hasNonResidentField = function hasNonResidentField(field) {
    return function (context, value) {
      var _field = resolveConstant(field);
      var fieldValue = context.getTransactionField("NonResident.Individual." + _field);
      if (!fieldValue)
        fieldValue = context.getTransactionField("NonResident.Entity." + _field);
      if (fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0))
        return true;
      else
        return false;
    }
  }

  _export.hasTransactionFieldValue = function hasTransactionFieldValue(field, constant) {
    return function (context, value) {
      var _constant = resolveConstant(constant);
      var fieldValue = context.getTransactionField(resolveConstant(field));
      if (fieldValue) {
        if (typeof _constant === "string") {
          if (fieldValue === _constant)
            return true;
        } else {
          for (var i = 0; i < _constant.length; i++) {
            if (fieldValue === _constant[i])
              return true;
          }
        }
      }
      return false;
    }
  }

  _export.notTransactionFieldValue = function notTransactionFieldValue(field, constant) {
    return function (context, value) {
      var _constant = resolveConstant(constant);
      var fieldValue = context.getTransactionField(resolveConstant(field));
      if (fieldValue) {
        if (typeof _constant === "string") {
          if (fieldValue === _constant)
            return false;
        } else {
          for (var i = 0; i < _constant.length; i++) {
            if (fieldValue === _constant[i])
              return false;
          }
        }
      }
      return true;
    }
  }

  _export.matchesTransactionField = function matchesTransactionField(field) {
    return function (context, value) {
      var fieldValue = context.getTransactionField(resolveConstant(field));
      if (fieldValue) {
        if (fieldValue === value)
          return true;
      }
      return false;
    }
  }

  _export.notMatchesTransactionField = function notMatchesTransactionField(field) {
    return function (context, value) {
      var fieldValue = context.getTransactionField(resolveConstant(field));
      if (fieldValue) {
        if (fieldValue === value)
          return false;
      }
      return true;
    }
  }

  _export.startsWithTransactionField = function startsWithTransactionField(field) {
    return function (context, value) {
      var fieldValue = context.getTransactionField(resolveConstant(field));
      if (value && fieldValue) {
        if (value.indexOf(fieldValue) === 0)
          return true;
      }
      return false;
    }
  }

  _export.notStartsWithTransactionField = function notStartsWithTransactionField(field) {
    return function (context, value) {
      var fieldValue = context.getTransactionField(resolveConstant(field));
      if (value && fieldValue) {
        if (value.indexOf(fieldValue) === 0)
          return false;
      }
      return true;
    }
  }

  _export.matchesNonResidentField = function matchesNonResidentField(field) {
    return function (context, value) {
      var _field = resolveConstant(field);
      var fieldValue = context.getTransactionField("NonResident.Individual." + _field);
      if (!fieldValue)
        fieldValue = context.getTransactionField("NonResident.Entity." + _field);
      if (fieldValue) {
        if (fieldValue === value)
          return true;
      }
      return false;
    }
  }

  _export.matchesResidentField = function matchesResidentField(field) {
    return function (context, value) {
      var _field = resolveConstant(field);
      var fieldValue = context.getTransactionField("Resident.Individual." + _field);
      if (!fieldValue)
        fieldValue = context.getTransactionField("Resident.Entity." + _field);
      if (fieldValue) {
        if (fieldValue === value)
          return true;
      }
      return false;
    }
  }

  _export.notMatchesGenderToIDNumber = function(field){
    return function(context,value){
      var _field = resolveConstant(field);
      var idStr;
      var gender = value;
      if(field.indexOf("Resident")!=-1){
        idStr = context.getTransactionField(_field);
      }else{
        idStr = context.getMoneyField(context.currentMoneyInstance, _field);
      }
      var midNumber = Number(idStr.substr(6,4));
      return midNumber<5000?(gender=="M"):(gender=="F");
    }
  }

  _export.notMatchResidentDateToIDNumber = function notMatchResidentDateToIDNumber(context, value) {
    // we won't be using this field's value, we're matching other fields
    var dateStr = value;//context.getTransactionField("Resident.Individual.DateOfBirth");
    var idStr = context.getTransactionField("Resident.Individual.IDNumber");
    if (_export.notEmpty(context, dateStr) && _export.notEmpty(context, idStr)) {
      dateStr = dateStr.trim();
      idStr = idStr.trim();
      if (dateStr.length >= 10 && idStr.length >= 6) {
        if (dateStr[2] == idStr[0] && dateStr[3] == idStr[1] &&
          dateStr[5] == idStr[2] && dateStr[6] == idStr[3] &&
          dateStr[8] == idStr[4] && dateStr[9] == idStr[5]) {
          return false;
        }
      }
    }
    return true;
  }

  _export.notMatchThirdPartyDateToIDNumber = function notMatchThirdPartyDateToIDNumber(context, value) {
    // we won't be using this field's value, we're matching other fields
    var dateStr = value;//context.getMoneyField(context.currentMoneyInstance, "ThirdParty.Individual.DateOfBirth");
    var idStr = context.getMoneyField(context.currentMoneyInstance, "ThirdParty.Individual.IDNumber");
    if (_export.notEmpty(context, dateStr) && _export.notEmpty(context, idStr)) {
      dateStr = dateStr.trim();
      idStr = idStr.trim();
      if (dateStr.length >= 10 && idStr.length >= 6) {
        if (dateStr[2] == idStr[0] && dateStr[3] == idStr[1] &&
          dateStr[5] == idStr[2] && dateStr[6] == idStr[3] &&
          dateStr[8] == idStr[4] && dateStr[9] == idStr[5]) {
          return false;
        }
      }
    }
    return true;
  }

  _export.matchOtherFields = function matchOtherFields(fieldA, fieldB) {
    return function (context, value) {
      // we won't be using this field's value, we're matching other fields
      var fieldValueA = context.getTransactionField(fieldA);
      var fieldValueB = context.getTransactionField(fieldB);
      return (fieldValueA === fieldValueB);
    }
  }


  _export.notValidSequenceNumbers = function notValidSequenceNumbers(context, value) {
    var validSequenceNumbers = true;
    var missingSequenceNumbers = false;
    var replacementIndicator = context.getTransactionField("ReplacementTransaction");
    if (!replacementIndicator || replacementIndicator !== "Y") {
      var moneyInstance;
      for (moneyInstance = 0; moneyInstance < context.getMoneySize(); moneyInstance++) {
        var fieldValue = context.getMoneyField(moneyInstance, "SequenceNumber");
        if (fieldValue) {
          if (missingSequenceNumbers || Number(fieldValue) != moneyInstance+1)
            validSequenceNumbers = false;
        }
        else {
          missingSequenceNumbers = true;
        }
      }
    }
    return !validSequenceNumbers;
  }


  _export.notValidSubSequenceNumbers = function notValidSubSequenceNumbers(context, value) {
    var validSequenceNumbers = true;
    var missingSequenceNumbers = false;
    var ieInstance;
    var moneyIndex = context.currentMoneyInstance;
    if (moneyIndex == -1)
      moneyIndex = 0;

    for (ieInstance = 0; ieInstance < context.getImportExportSize(moneyIndex); ieInstance++) {
      var fieldValue = context.getImportExportField(moneyIndex, ieInstance, "SubSequence");
      if (fieldValue) {
        if (missingSequenceNumbers || Number(fieldValue) != ieInstance+1)
          validSequenceNumbers = false;
      }
      else {
        missingSequenceNumbers = true;
      }
    }
    return !validSequenceNumbers;
  }

  _export.emptyMoneyField = function emptyMoneyField(context, value) {
    return context.getMoneySize() == 0;
  }

  _export.notEmptyMoneyField = function notEmptyMoneyField(context, value) {
    return context.getMoneySize() > 0;
  }

  _export.notFirstMoneyField = function notFirstMoneyField(context, value) {
    return context.currentMoneyInstance > 0;
  }

  _export.hasMoneyField = function hasMoneyField(field) {
    return function (context, value) {
      var fieldValue = context.getMoneyField(context.currentMoneyInstance, resolveConstant(field));
      if (fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0))
        return true;
      else
        return false;
    }
  }

  _export.notMoneyField = function notMoneyField(field) {
    return function (context, value) {
      var fieldValue = context.getMoneyField(context.currentMoneyInstance, resolveConstant(field));
      // if(field == 'RegulatorAuth.CBAuthAppNumber'){
      //   // console.log('field Value: ' + fieldValue);
      //   console.log('Reg: ' + context.getMoneyField(context.currentMoneyInstance, 'RegulatorAuth.CBAuthAppNumber'));
      // } else {
      //   console.log('SARB: ' + context.getMoneyField(context.currentMoneyInstance, 'SARBAuth.RulingsSection'));
      // }
      if (!fieldValue || (typeof fieldValue == "string" && fieldValue.trim().length == 0))
        return true;
      else
        return false;
    }
  }

  _export.hasAnyMoneyFieldValue = function hasAnyMoneyFieldValue(field, constant) {
    return function (context, value) {
      var _field = resolveConstant(field);
      var _constant = resolveConstant(constant);
      var moneyInstance;
      for (moneyInstance = 0; moneyInstance < context.getMoneySize(); moneyInstance++) {
        var fieldValue = context.getMoneyField(moneyInstance, _field);
        if (fieldValue) {
          if (typeof _constant === "string") {
            if (fieldValue === _constant)
              return true;
          } else {
            for (var i = 0; i < _constant.length; i++) {
              if (fieldValue === _constant[i])
                return true;
            }
          }
        }
      }
      return false;
    }
  }

  _export.hasAnyMoneyField = function hasAnyMoneyField(field) {
    return function (context, value) {
      var moneyInstance;
      for (moneyInstance = 0; moneyInstance < context.getMoneySize(); moneyInstance++) {
        var fieldValue = context.getMoneyField(moneyInstance, resolveConstant(field));
        if (fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0))
          return true;
      }
      return false;
    }
  }

  _export.hasAllMoneyField = function (field) {
    return function (context, value) {
      var moneyInstance;
      for (moneyInstance = 0; moneyInstance < context.getMoneySize(); moneyInstance++) {
        var fieldValue = context.getMoneyField(moneyInstance, resolveConstant(field));
        if (!(fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0)))
          return false;
      }
      return context.getMoneySize() > 0;
    }
  }

  _export.hasImportExportField = function hasImportExportField(field) {
    return function (context, value) {
      var fieldValue = context.getImportExportField(context.currentMoneyInstance, context.currentImportExportInstance, resolveConstant(field));
      if (fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0))
        return true;
      else
        return false;
    }
  }

  _export.notImportExportField = function notImportExportField(field) {
    return function (context, value) {
      var fieldValue = context.getImportExportField(context.currentMoneyInstance, context.currentImportExportInstance, resolveConstant(field));
      if (!fieldValue || (typeof fieldValue == "string" && fieldValue.trim().length == 0))
        return true;
      else
        return false;
    }
  }

  _export.emptyImportExport = function emptyImportExport(context, value) {
    return context.getImportExportSize(context.currentMoneyInstance) == 0;
  }

  _export.notEmptyImportExport = function notEmptyImportExport(context, value) {
    return context.getImportExportSize(context.currentMoneyInstance) > 0;
  }

  _export.singleImportExport = function singleImportExport(context, value) {
    return context.getImportExportSize(context.currentMoneyInstance) == 1;
  }

  _export.singleMonetaryAmount = function (context, value) {
    return context.getMoneySize() == 1;
  }

  _export.multipleImportExport = function multipleImportExport(context, value) {
    return context.getImportExportSize(context.currentMoneyInstance) > 1;
  }

  _export.equalsMoneyField = function equalsMoneyField(field) {
    return function (context, value) {
      var fieldValue = context.getMoneyField(context.currentMoneyInstance, resolveConstant(field));
      if (fieldValue)
        return (fieldValue == value);
      else
        return false;
    }
  }

    _export.hasMoneyFieldLookupValue = function (field, lookupName, exclude) {
        return function (context, value) {
            var idx = context.currentMoneyInstance;

            if (idx == -1) { idx = 0; }

            var lookup = context.lookups.lookups[lookupName];

            var fieldValue = context.getMoneyField(idx, resolveConstant(field));
            if (fieldValue) {
                for (var i = 0; i < lookup.length; i++) {
                    var lookupVal = lookup[i];

                    if (typeof lookupVal != "object") {
                        if(exclude && exclude.indexOf(lookupVal) < 0)
                            if (fieldValue === lookupVal) { return true; }
                    } else {
                        for (var obj in lookupVal) {
                            if(exclude && exclude.indexOf(lookupVal[obj]) < 0)
                                if (fieldValue === lookupVal[obj]) { return true; }
                        }
                    }
                }
            }
            return false;
        }
    }


  /*
  Eval functions are useful for reusing the rule DSL to validate other fields.

  Useful for display rules: e.g.
    You have a button which you want to enable only when the ICN,TDN and CCN is valid:

   enable(
   evalIEField("ImportControlNumber",isValidICN)
   .and(evalIEField("TransportDocumentNumber",isTooLong(1)))
   .and(evalResidentField("CustomsClientNumber",isValidCCN))
   .and(ImportUndertakingClient)

   )

   */
  _export.evalTransactionField = function (field, fEval) {
    return function(context, value) {
      var fieldValue = context.getTransactionField(resolveConstant(field));

      if (fieldValue) {
        return fEval(context, fieldValue);
      }
      return false;
    };
  }


  _export.evalResidentField = function (field, fEval) {
    return function(context, value) {
      var _field = resolveConstant(field);
      var fieldValue = context.getTransactionField("Resident.Individual." + _field);
      if (!fieldValue)
        fieldValue = context.getTransactionField("Resident.Entity." + _field);

      if (fieldValue) {
        return fEval(context, fieldValue);
      }
      return false;
    };
  }

  _export.evalNonResidentField = function (field, fEval) {
    return function(context, value) {
      var _field = resolveConstant(field);
      var fieldValue = context.getTransactionField("NonResident.Individual." + _field);
      if (!fieldValue)
        fieldValue = context.getTransactionField("NonResident.Entity." + _field);

      if (fieldValue) {
        return fEval(context, fieldValue);
      }
      return false;
    };
  }


    /*
    Note: evalMoneyField can only operate in the current money context
   */
    _export.evalMoneyField = function (field, fEval) {
        return function(context, value) {
          var index = context.currentMoneyInstance;
          if (index == -1)
              index = 0;

          var fieldValue = context.getMoneyField(index, resolveConstant(field));

          if (fieldValue) {
              return fEval(context, fieldValue);
          }
          return false;
        };
    }
    /*
     Note: evalIEField can only operate in the current money context and current importExport context
   */
    _export.evalIEField = function (field, fEval) {
      return function(context, value) {
        var moneyIndex = context.currentMoneyInstance;
        if (moneyIndex == -1)
          moneyIndex = 0;
        var index = context.currentImportExportInstance;
        if (index == -1)
          index = 0;
        var fieldValue = context.getImportExportField(moneyIndex,index, resolveConstant(field));

        if (fieldValue) {
          return fEval(context, fieldValue);
        }
        return false;
      };
    }

    _export.hasMoneyFieldValue = function (field, constant) {
        return function (context, value) {
            var _constant = resolveConstant(constant);
            var index = context.currentMoneyInstance;
            if (index == -1)
                index = 0;
            var fieldValue = context.getMoneyField(index, resolveConstant(field));
            if (typeof fieldValue !== 'undefined') {
                if ((typeof _constant === "string")||(typeof _constant === "boolean")) {
                    if (fieldValue === _constant)
                        return true;
                } else {
                    for (var i = 0; i < _constant.length; i++) {
                        if (fieldValue === _constant[i])
                            return true;
                    }
                }

            }
            return false;
        }
  }

  _export.notMoneyFieldValue = function (field, constant) {
    return function (context, value) {
      var _constant = resolveConstant(constant);
      var index = context.currentMoneyInstance;
      if (index == -1)
        index = 0;
      var fieldValue = context.getMoneyField(index, resolveConstant(field));
      if (fieldValue) {
        if (typeof constant === "string") {
          if (fieldValue === _constant)
            return false;
        } else {
          for (var i = 0; i < _constant.length; i++) {
            if (fieldValue === _constant[i])
              return false;
          }
        }

      }
      return true;
    }
  }

  _export.hasNonResException = function (type) {
    return function (context, value) {
      var exception = context.getTransactionField("NonResident.Exception.ExceptionName");
      if (exception)
        return exception === type;
      return false;
    }
  }

  _export.notNonResException = function (type) {
    return function (context, value) {
      var exception = context.getTransactionField("NonResident.Exception.ExceptionName");
      if (exception)
        return exception != type;
      return true;
    }
  }

  _export.hasResidentFieldValue = function (field, constant) {
    return function (context, value) {
      var _constant = resolveConstant(constant);
      var _field = resolveConstant(field);

      var fieldValue = context.getTransactionField("Resident.Individual." + _field);
      if (!fieldValue)
        fieldValue = context.getTransactionField("Resident.Entity." + _field);
      if (fieldValue) {
        if (typeof _constant === "string") {
          if (fieldValue === _constant)
            return true;
        } else {
          for (var i = 0; i < _constant.length; i++) {
            if (fieldValue === _constant[i])
              return true;
          }
        }
      }
      return false;
    }
  }

  _export.notResidentFieldValue = function (field, constant) {
    return function (context, value) {
      var _constant = resolveConstant(constant);
      var _field = resolveConstant(field);
      var fieldValue = context.getTransactionField("Resident.Individual." + _field);
      if (!fieldValue)
        fieldValue = context.getTransactionField("Resident.Entity." + _field);
      if (fieldValue) {
        if (typeof _constant === "string") {
          if (fieldValue === _constant)
            return false;
        } else {
          for (var i = 0; i < _constant.length; i++) {
            if (fieldValue === _constant[i])
              return false;
          }
        }
      }
      return true;
    }
  }

  _export.hasNonResidentFieldValue = function (field, constant) {
    return function (context, value) {
      var _constant = resolveConstant(constant);
      var _field = resolveConstant(field);
      var fieldValue = context.getTransactionField("NonResident.Individual." + _field);
      if (!fieldValue)
        fieldValue = context.getTransactionField("NonResident.Entity." + _field);
      if (fieldValue) {
        if (typeof _constant === "string") {
          if (fieldValue === _constant)
            return true;
        } else {
          for (var i = 0; i < _constant.length; i++) {
            if (fieldValue === _constant[i])
              return true;
          }
        }
      }
      return false;
    }
  }

  _export.notNonResidentFieldValue = function (field, constant) {
    return function (context, value) {
      var _constant = resolveConstant(constant);
      var _field = resolveConstant(field);
      var fieldValue = context.getTransactionField("NonResident.Individual." + _field);
      if (!fieldValue)
        fieldValue = context.getTransactionField("NonResident.Entity." + _field);
      if (fieldValue) {
        if (typeof _constant === "string") {
          if (fieldValue === _constant)
            return false;
        } else {
          for (var i = 0; i < _constant.length; i++) {
            if (fieldValue === _constant[i])
              return false;
          }
        }
      }
      return true;
    }
  }

  _export.isDaysInFuture = function (days) {
    return function (context, value) {
      var today = new Date(rule_parameters.todayDate.getTime());
      var futureDate = new Date(rule_parameters.todayDate.getTime());
      futureDate.setDate(today.getDate() + Number(days));

      if (typeof value === "string") {
        var dateParts = value.split("-");
        var date = new Date(dateParts[0], (dateParts[1] - 1), dateParts[2]);
        return date > futureDate;
      }
      return value > futureDate;
    }
  }

  _export.isDaysInPast = function (days) {
    return function (context, value) {
      var today = new Date(rule_parameters.todayDate.getTime());
      var pastDate = new Date(rule_parameters.todayDate.getTime());
      pastDate.setDate(today.getDate() - Number(days));

      if (typeof value === "string") {
        var dateParts = value.split("-");
        var date = new Date(dateParts[0], (dateParts[1] - 1), dateParts[2]);
        return date < pastDate;
      }
      return value < pastDate;
    }
  }

  _export.isDateBefore = function(datePar) {
    return function (context, value) {
      try {
        if (typeof value === "string") {
          var dateParts = value.split("-");
          var date = new Date(dateParts[0], (dateParts[1] - 1), dateParts[2]);
          var datePartsPar = datePar.split("-");
          var dateParConvert = new Date(datePartsPar[0], (datePartsPar[1] - 1), datePartsPar[2]);
        }
      } catch(err) {
        return false;
      }
    
      return date < dateParConvert;
    }
  }

  _export.isDateAfter = function(datePar) {
    return function (context, value) {
      try {
        if (typeof value === "string") {
          var dateParts = value.split("-");
          var date = new Date(dateParts[0], (dateParts[1] - 1), dateParts[2]);
          var datePartsPar = datePar.split("-");
          var dateParConvert = new Date(datePartsPar[0], (datePartsPar[1] - 1), datePartsPar[2]);
        }
      } catch(err) {
        return false;
      }
    
      return dateParConvert >= date;
    }
  }

  _export.isBeforeGoLive = function (context, value) {
    if (typeof value === "string") {
      var dateParts = value.split("-");
      var date = new Date(dateParts[0], (dateParts[1] - 1), dateParts[2]);
      return date < rule_parameters.goLiveDate;
    }
    return value < rule_parameters.goLiveDate;
  }

  _export.isGreaterThan = function (amount) {
    return function (context, value) {
      return Number(value) > Number(amount);
    }
  }

  _export.isLessThan = function (amount) {
    return function (context, value) {
      return Number(value) < Number(amount);
    }
  }

  _export.isValidRSAID = function (context, value) {
    if (typeof value != "string")
      return false;

    if (!value.match("^\\d{13}$"))
      return false;

    var regDate = /^[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])$/;
    if (regDate.test(value.substr(0, 6)) == false)
      return false;

    var digits = [];
    var i;

    for (i = 0; i < value.length; i++) {
      digits.push(Number(value.charAt(i)));
    }

    var a = 0;
    for (i = 0; i < 6; i++) {
      a += digits[i * 2];
    }

    var b = 0;
    for (i = 0; i < 6; i++) {
      var n = 2 * (digits[i * 2 + 1]);
      b += Math.floor(n / 10) + (n % 10);
    }

    var total = a + b;
    var tmp = (10 * (Math.floor(total / 10) + 1)) - total;
    if (tmp < 10) {
      return (tmp % 10) == digits[12];
    } else if (tmp == 10) {
      return 0 == digits[12];
    } else {
      return false;
    }
  }

  _export.notValidRSAID = function (context, value) {
    if (!value)
      return true;
    return !_export.isValidRSAID(context, value);
  }

  _export.isValidZAVATNumber = function (context, value) {
    var isChecksumCorrect = true;
    if (!(value && (typeof value == "string" || value.trim().length > 0))) {
      isChecksumCorrect = false;
    }

    if (value === "NO VAT NUMBER")
      return true;

    //- The length must be 10: 4090103146
    //- Must start with a 4:  4090103146
    //- The 4th digit must be 0: 4090103146
    //- The 5th digit must either be 1 or 2: 4090103146 or 4410229209

    if (value.match(/[^0-9]+/g, '')) {
      isChecksumCorrect = false;
    }

    var vatNumber = value.split('');

    if (vatNumber.length < 10) {
      isChecksumCorrect = false;
    }
    else if (vatNumber[4 - 1] !== "0") {
      isChecksumCorrect = false;
    }
    else if (vatNumber[5 - 1] !== "1" && vatNumber[5 - 1] !== "2") {
      isChecksumCorrect = false;
    }

    //console.log(isChecksumCorrect, vatNumber.length, vatNumber[4 - 1], vatNumber[5 - 1]);
    return isChecksumCorrect;
  }

  _export.isValidZATaxNumber= function (context, value) {
    var val = value;
    var isChecksumCorrect = false;

    if (!(val && (typeof val == "string" || val.trim().length > 0)))
      return isChecksumCorrect; //invalid
    /*
     0254/089/06/3,
     * Ignore all non-numeric values.
     * Take each digit in an ODD position and multiply it by 2.
     * Total A (0x2) + (5x2) +(0x2) + (9x2) + (6x2)
     0    +  1+0   +   0    +  1+8  + 1+2
     Add these values giving Total A.   13

     Add all the digits in EVEN positions giving Total B.
     Total B 2 + 4 + 8 + 0 = 14 (ignore last digit, as that is the check digit)

     Add Total A and Total B together to give you Total C Total C 13 + 14 = 27

     Subtract Total C from the next highest multiple of 10, giving the check digit.

     Check digit 30 � 27 = 3
     The last digit i.e. 3 is the check digit.

     */

    var taxNumber = val
      .replace(/[^0-9]+/g, '')
      .split('');

    function checksum(value, digits) {
      var totA = 0;
      for (var i = 0; i < digits.length - 1; i = i + 2) {
        var numerics = (digits[i] * 2).toString().split('');
        for (var k = 0; k < numerics.length; k++) {
          totA = totA + (numerics[k] * 1);
        }
      }

      var totB = 0;
      for (var j = 1; j < digits.length - 1; j = j + 2) {
        totB = totB + (digits[j] * 1);
      }

      var totC = totA + totB;

      //console.log(totA, totB, totC);
      return 10 - (totC % 10);
    }

    isChecksumCorrect = checksum(val, taxNumber) === taxNumber[taxNumber.length - 1] * 1;

    //console.log(taxNumber, checksum);

    return isChecksumCorrect;
  }

  _export.isValidVATNumber = function (context, value) {
    if (typeof value != "string")
      return false;

    if (!value.match("^\\d+$")) {
      if (value === "NO VAT NUMBER" || value === "NO VAT NUM")
        return true;
      else
        return false;
    }
    return true;
  }

  _export.notValidVATNumber = function (context, value) {
    if (!value)
      return true;
    return !_export.isValidVATNumber(context, value);
  }

  _export.isValidCCN = function (context, value) {
    if (typeof value != "string")
      return false;

    if (!value.match("^\\d{8}$"))
      return false;

    var digits = [];
    var i;

    for (i = 0; i < value.length; i++) {
      digits.push(Number(value.charAt(i)));
    }

    var total = 0;
    var multiplier = 9;
    for (i = 0; i < 8; i++) {
      total += digits[i] * multiplier;
      multiplier -= 1;
      if (multiplier == 5)
        multiplier -= 1;
    }
    return (total % 11) == 0 || (total % 10) == 0;
  }

  _export.notValidCCN = function (context, value) {
    if (!value)
      return true;
    return !_export.isValidCCN(context, value);
  }

  _export.hasInvalidSWIFTCountry = function (context, value) {
    if (!value)
      return true;
    return !context.lookups.isValidCountryCode(value);
  }

  _export.hasInvalidSWIFTCurrency = function (context, value) {
    if (!value)
      return true;
    return !context.lookups.isValidCurrencyCode(value);
  }

  _export.hasInvalidCategory = function (context, value) {
    if (!value)
      return true;
    var flow = context.flow;
    return !context.lookups.isValidCategory(flow, value);
  }

  _export.hasInvalidSubCategory = function (context, value) {
    if (typeof value === 'undefined')
      return true;
    var flow = context.flow;
    var category = context.getMoneyField(context.currentMoneyInstance, 'CategoryCode');
    return !context.lookups.isValidSubCategory(flow, category, value);
  }

  _export.isExceptionName = function (context, value) {
    if (!value)
      return false;
    return context.lookups.isNonResExceptionName(value.toUpperCase()) || context.lookups.isResExceptionName(value.toUpperCase());
  }

  _export.isValidBranchCode = function (context, value) {
    var branchCode = value;
    if (context.lookups.lookups.branches) {
      var i;
      var branch;
      for (i = 0; i < context.lookups.lookups.branches.length; i++) {
        branch = context.lookups.lookups.branches[i];
        if (branch.code === branchCode) {
          return true;
        }
      }
    }
    return false;
  }

  _export.isValidBranchName = function (context, value) {
    var branchCode = context.getTransactionField("BranchCode");
    if (branchCode && context.lookups.lookups.branches) {
      var i;
      var branch;
      for (i = 0; i < context.lookups.lookups.branches.length; i++) {
        branch = context.lookups.lookups.branches[i];
        if (branch.code === branchCode) {
          if (branch.name === value)
            return true;
        }
      }
    }
    return false;
  }

  _export.isValidHubCode = function (context, value) {
    var hubCode = value;
    if (context.lookups.lookups.branches) {
      var i;
      var hub;
      for (i = 0; i < context.lookups.lookups.branches.length; i++) {
        hub = context.lookups.lookups.branches[i];
        if (hub.hubCode === hubCode) {
          return true;
        }
      }
    }
    return false;
  }

  _export.isValidHubName = function (context, value) {
    var hubCode = context.getTransactionField("HubCode");
    if (hubCode && context.lookups.lookups.branches) {
      var i;
      var hub;
      for (i = 0; i < context.lookups.lookups.branches.length; i++) {
        hub = context.lookups.lookups.branches[i];
        if (hub.hubCode === hubCode) {
          if (hub.hubName === value)
            return true;
        }
      }
    }
    return false;
  }

  _export.notNonResExceptionName = function (context, value) {
    if (!value)
      return true;
    return !context.lookups.isNonResExceptionName(value);
  }

  _export.notReportingQualifier = function (context, value) {
    if (!value)
      return true;
    return !context.lookups.isReportingQualifier(value);
  }

  _export.notResExceptionName = function (context, value) {
    if (!value)
      return true;
    return !context.lookups.isResExceptionName(value);
  }

  _export.notMoneyTransferAgent = function (context, value) {
    if (!value)
      return true;
    return !context.lookups.isMoneyTransferAgent(value);
  }

  _export.notValidProvince = function (context, value) {
    if (!value)
      return true;
    return !context.lookups.isValidProvince(value);
  }

  _export.notValidCardType = function (context, value) {
    if (!value)
      return true;
    return !context.lookups.isValidCardType(value);
  }

  _export.notValidIndustrialClassification = function (context, value) {
    return !context.lookups.isValidIndustrialClassification(value);
  }

  _export.notValidInstitutionalSector = function (context, value) {
    return !context.lookups.isValidInstitutionalSector(value);
  }

  _export.notValidPostalCode = function (context, value) {
    if (!value)
      return true;

    var regPostcode = /^([0-9]){1,4}$/;

    if (regPostcode.test(value) == true) {
      return false;
    }
    return true;
  }

  _export.isValidICN = function (context, value) {
    if (value)
      return !_export.notValidICN(context, value);
    return false;
  };

  _export.isValidICNCustomsOffice = function (context, value) {
    if (value)
      return !_export.notStartsWithValidCustomsOfficeCode(context, value);
    return false;
  };

  _export.notValidICNCustomsOffice = function (context, value) {
    if (!value)
      return true;
    if (value.length <= 3)
      return true;
    if (!context.lookups.isValidCustomsOfficeCode(value.substr(0, 3)))
      return true;
    return false;
  }
  
  _export.notValidICN = function (context, value) {
    if (!value)
      return true;
    if (value.length != 18)
      return true;
    if (!context.lookups.isValidCustomsOfficeCode(value.substr(0, 3)))
      return true;
    var regDate = /^(19|20)[0-9]{2}(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])$/;
    if (regDate.test(value.substr(3, 8)) == false)
      return true;
    if (isNaN(new Date(value.substr(3, 4) + '-' + value.substr(7, 2) + '-' + value.substr(9, 2))))
      return true;
    var regNumeric = /^[0-9]*$/;
    if (regNumeric.test(value.substr(11, 7)) == false)
      return true;
    return false;
  }

  _export.notValidUCR = function (context, value) {
    if (!value)
      return true;
    if (value.length < 12 || value.length > 35)
      return true;
    var regUCR = /^[0-9]ZA[0-9]{8}[0-9a-zA-Z]{1,24}$/;
    return (regUCR.test(value) == false);
  }

  // NOTE: There can be invalid-Valid CCNs, therefore this should be handled oby an external call
  //_export.notValidCCNinUCR = function (context, value) {
  //  var CCNinUCR = value.substr(3, 8);
  //  return _export.notValidCCN(context, CCNinUCR);
  //}

  _export.notMatchCCNinUCR = function (context, value) {
    var CCNinUCR = value.substr(3, 8);

    var ResidentCCN = context.getTransactionField("Resident.Individual.CustomsClientNumber")
      || context.getTransactionField("Resident.Entity.CustomsClientNumber");

    var ThirdPartyCCN = context.getMoneyField(context.currentMoneyInstance,"ThirdParty.CustomsClientNumber");

    return !((CCNinUCR==ResidentCCN)||(CCNinUCR==ThirdPartyCCN));
  }


  _export.isValidEmail = function (context, value) {
    if (!value)
      return false;
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(value);
  }

  _export.notValidEmail = function (context, value) {
    if (!value)
      return true;
    return !_export.isValidEmail(context, value);
  }

  _export.notValidECI = function (context, value) {
    var cardType = context.getMoneyField(context.currentMoneyInstance, 'CardIndicator');
    var regECI;
    if (cardType) {
      if (cardType === 'VISA' || cardType === 'ELECTRON') {
        regECI = /^(0[0-9]|XX)$/;
      } else if (cardType === 'MASTER' || cardType === 'MAESTRO') {
        regECI = /^([0-3]|X)$/;
      } else if (cardType === 'AMEX') {
        regECI = /^0[5-7]$/;
      } else if (cardType === 'DINERS') {
        regECI = /^0[5-8]$/;
      }
    }
    if (regECI)
      return (regECI.test(value) == false);
    return false;
  }

  _export.notValidPOSEntryMode = function (context, value) {
    var cardType = context.getMoneyField(context.currentMoneyInstance, 'CardIndicator');
    var regPEM;
    if (cardType) {
      if (cardType === 'VISA') {
        regPEM = /^(0[0-7]|10|84|90|91|95|96|XX)$/;
      } else if (cardType === 'MASTER') {
        regPEM = /^[019][0126ABCFMNORST]$/;
      } else if (cardType === 'AMEX') {
        regPEM = /^[0-6][0-6]$/;
      } else if (cardType === 'DINERS') {
        regPEM = /^[012349S][01234569]$/;
      } else if (cardType === 'MAESTRO') {
        regPEM = /^(0[0-8]|79|80|81|82|90|91|92)$/;
      } else if (cardType === 'ELECTRON') {
        regPEM = /^(0[0-7]|10|81|84|86|90|91|95|96|XX)$/;
      } else if (cardType === 'BOCEXPRESS') {
        regPEM = null;
      }
    }
    if (regPEM) {
      if (!value)
        return false;

      return (regPEM.test(value) == false);
    }
    return false;
  }

  _export.notValidImportExportCurrency = function (context, value) {
    var firstCurr = context.getImportExportField(context.currentMoneyInstance, 0, "PaymentCurrencyCode");
    if (!firstCurr)
      return true;

    var domesticCurr;
    var foreignCurr = context.getTransactionField("FlowCurrency");

    var domesticValue = context.getMoneyField(context.currentMoneyInstance, _export.getMap("LocalValue"));
    if (domesticValue) {
      domesticCurr = context.domesticCurrency;
    }
    if ((foreignCurr && firstCurr === foreignCurr) || (domesticCurr && firstCurr === domesticCurr)) {
      if (firstCurr !== value)
        return true;
    } else
      return true;

    return false;
  }

  // _export.hasMultipleMonetaryValues = function (context, value) {
  //   if(context.getMoneySize() > 1)
  //     return true;
  //   return false;
  // }

  _export.notChecksumImportExport = function (context, value) {
    if (context.getImportExportSize(context.currentMoneyInstance) > 0) {
      var total = 0;
      var moneyValue;
      for (var i = 0; i < context.getImportExportSize(context.currentMoneyInstance); i++) {
        var fvalue = context.getImportExportField(context.currentMoneyInstance, i, "PaymentValue");
        if (fvalue)
          total += Number(fvalue);
      }
      if (context.getImportExportField(context.currentMoneyInstance, 0, "PaymentCurrencyCode") === context.domesticCurrency) {
        moneyValue = context.getMoneyField(context.currentMoneyInstance, _export.getMap("LocalValue"));
        var flowCurr = context.getTransactionField("FlowCurrency");
        if (!moneyValue && flowCurr === context.domesticCurrency)
          moneyValue = context.getMoneyField(context.currentMoneyInstance, "ForeignValue");
      }
      else
        moneyValue = context.getMoneyField(context.currentMoneyInstance, "ForeignValue");

      return Math.abs(total - moneyValue) > (moneyValue / 100);
    }
    return false;
  }

  _export.outOfMidRateVariance = function (context, value) {
    return false;
  }

// Display Functions
  _export.categoryDescription = function (context) {
    var cat = context.lookups.getCategory(context.flow,
      context.getMoneyField(context.currentMoneyInstance, 'CategoryCode'),
      context.getMoneyField(context.currentMoneyInstance, 'CategorySubCode'));
    if (cat)
      return cat.description;
    return "";
  }

  _export.dateOfBirthFromSAID = function (field) {
    return function (context) {
      var _field =  resolveConstant(field);
      var idStr;
      if (_field === "ThirdParty.Individual.IDNumber")
        idStr = context.getMoneyField(context.currentMoneyInstance, _field);
      else
        idStr = context.getTransactionField(_field);

      if (_export.notEmpty(context, idStr)) {
        var dateStr;
        idStr = idStr.trim();
        if (idStr.length >= 6) {
          var year = Number(idStr[0] + idStr[1]);
          if (year > 25)
            dateStr = "19";
          else if (year < 10)
            dateStr = "20";
          else
            return "";

          dateStr += idStr[0];
          dateStr += idStr[1];
          dateStr += "-";
          dateStr += idStr[2];
          dateStr += idStr[3];
          dateStr += "-";
          dateStr += idStr[4];
          dateStr += idStr[5];
          return dateStr;
        }
      }
      return "";
    }
  }

  _export.genderFromSAID = function (field) {
    return function (context) {
      var _field =  resolveConstant(field);
      var idStr;
      if (field === "ThirdParty.Individual.IDNumber")
        idStr = context.getMoneyField(context.currentMoneyInstance, _field);
      else
        idStr = context.getTransactionField(_field);

      if (_export.notEmpty(context, idStr)) {
        var dateStr;
        idStr = idStr.trim();
        if (idStr.length >= 7) {
          var genderValue = Number(idStr[6]);
          if (genderValue >= 5)
            return "M";
          else
            return "F";
        }
      }
      return "";
    }
  }

  _export.countryFromSWIFTBIC = function (field) {
    return function (context) {
      var swiftBIC = context.getTransactionField(resolveConstant(field));

      if (_export.notEmpty(context, swiftBIC)) {
        if (swiftBIC.length >= 6) {
          var country = swiftBIC[4] + swiftBIC[5];
          if (context.lookups.isValidCountryCode(country))
            return country;
        }
      }
      return "";
    }
  }

  _export.moneyFieldValue = function (field, instance) {
    return function (context) {
      if (instance === undefined)
        instance = context.currentMoneyInstance;
      var value = context.getMoneyField(instance, resolveConstant(field));

      if (value) {
        return value;
      }
      return "";
    }
  }

  _export.firstMoneyFieldValue = function (field) {
    return function (context) {
      var moneyInstance;
      for (moneyInstance = 0; moneyInstance < context.getMoneySize(); moneyInstance++) {
        var fieldValue = context.getMoneyField(moneyInstance, resolveConstant(field));
        if (fieldValue && (typeof fieldValue != "string" || fieldValue.trim().length > 0))
          return fieldValue;
      }
      return "";
    }
  }

  function findDataField(data, fieldName){
    var objChain = fieldName.split('.');
    var _data = data;
    for (var i = 0; i < objChain.length; i++) {
      if (!_data)
        break;
      var val = objChain[i];
      if (_data[val]) {
        _data = _data[val];
      } else {
        _data = undefined;
        break;
      }
    }
    return _data;
  }

  _export.findTransactionDataField = findDataField; 
  
  _export.findMoneyDataField = function(data, fieldName, instance){
    if(data&&fieldName&&(typeof instance == 'number')) {
      if(data.MonetaryAmount && data.MonetaryAmount[instance]){
        var money = data.MonetaryAmount[instance];
        return findDataField(money,fieldName);
      }
    }
  }; 


  _export.isTransactionFieldProvided = function (fieldName) {
      return function (context, value) {
        var tmp = context.getCustomValue('snapShot');
        if(!tmp)return false;
        var snapShot = tmp.transaction ? tmp.transaction : tmp;
        var snapVal = _export.findTransactionDataField(snapShot, resolveConstant(fieldName));
        if (snapVal) { return true; }
        else {return false;}
    }
  }

  // Note: this predef only works in money context.
  _export.isMoneyFieldProvided = function (fieldName) {
    return function (context, value) {
      var tmp = context.getCustomValue('snapShot');
      if(!tmp)return false;
      var snapShot = tmp.transaction ? tmp.transaction : tmp;
      var snapVal = _export.findMoneyDataField(snapShot, resolveConstant(fieldName), context.currentMoneyInstance);
      if (snapVal) { return true; }
      else {return false;}
  }
}




// IBAN Validation Logic

// add ISO13616Prepare method to strings
  function iban_ISO13616Prepare(value) {
    var i, isostr;
    isostr = value.toUpperCase();
    isostr = isostr.substr(4) + isostr.substr(0, 4);
    for (i = 0; i <= 25; i++) {
      while (isostr.search(String.fromCharCode(i + 65)) !== -1) {
        isostr = isostr.replace(String.fromCharCode(i + 65), String(i + 10));
      }
    }
    return isostr;
  }

// add ISO7064Mod97_10 method to strings
  function iban_ISO7064Mod97_10(value) {
    var parts, remainer, i;
    parts = Math.ceil(value.length / 7);
    remainer = "";
    for (i = 1; i <= parts; i++) {
      remainer = String(parseFloat(remainer + value.substr((i - 1) * 7, 7)) % 97);
    }
    return remainer;
  }

  var iban_altxt = ["The IBAN contains illegal characters.",
    "The structure of IBAN is wrong.",
    "The check digits of IBAN are wrong.",
    "Can not check correct length of IBAN because ",
    " is currently not respected.",
    "The length of IBAN is wrong. The IBAN of ",
    " needs to be ",
    " characters long.",
    "The IBAN seems to be correct.",
    "The IBAN is incorrect."];

// country codes, fixed length for those countries, inner structure, IBAN example, IBAN requirement, SEPA, appliance of EU REGULATION 924/2009;260/2012 and EUR
  var iban_ilbced = [
    {
      country  : "AD",
      length   : 24,
      structure: "F04F04A12",
      example  : "AD1200012030200359100100",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "AE",
      length   : 23,
      structure: "F03F16",
      example  : "AE070331234567890123456",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "AL",
      length   : 28,
      structure: "F08A16",
      example  : "AL47212110090000000235698741",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "AT",
      length   : 20,
      structure: "F05F11",
      example  : "AT611904300234573201",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "AZ",
      length   : 28,
      structure: "U04A20",
      example  : "AZ21NABZ00000000137010001944",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "BA",
      length   : 20,
      structure: "F03F03F08F02",
      example  : "BA391290079401028494",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "BE",
      length   : 16,
      structure: "F03F07F02",
      example  : "BE68539007547034",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "BG",
      length   : 22,
      structure: "U04F04F02A08",
      example  : "BG80BNBG96611020345678",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : false,
      euMember : true
    },
    {
      country  : "BH",
      length   : 22,
      structure: "U04A14",
      example  : "BH67BMAG00001299123456",
      required : true,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "BR",
      length   : 29,
      structure: "F08F05F10U01A01",
      example  : "BR9700360305000010009795493P1",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "CH",
      length   : 21,
      structure: "F05A12",
      example  : "CH9300762011623852957",
      required : false,
      sepa     : true,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "CR",
      length   : 21,
      structure: "F03F14",
      example  : "CR0515202001026284066",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "CY",
      length   : 28,
      structure: "F03F05A16",
      example  : "CY17002001280000001200527600",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "CZ",
      length   : 24,
      structure: "F04F06F10",
      example  : "CZ6508000000192000145399",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : false,
      euMember : true
    },
    {
      country  : "DE",
      length   : 22,
      structure: "F08F10",
      example  : "DE89370400440532013000",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "DK",
      length   : 18,
      structure: "F04F09F01",
      example  : "DK5000400440116243",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : false,
      euMember : true
    },
    {
      country  : "DO",
      length   : 28,
      structure: "U04F20",
      example  : "DO28BAGR00000001212453611324",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "EE",
      length   : 20,
      structure: "F02F02F11F01",
      example  : "EE382200221020145685",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "ES",
      length   : 24,
      structure: "F04F04F01F01F10",
      example  : "ES9121000418450200051332",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "FI",
      length   : 18,
      structure: "F06F07F01",
      example  : "FI2112345600000785",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "FO",
      length   : 18,
      structure: "F04F09F01",
      example  : "FO6264600001631634",
      required : false,
      sepa     : true,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "FR",
      length   : 27,
      structure: "F05F05A11F02",
      example  : "FR1420041010050500013M02606",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "GB",
      length   : 22,
      structure: "U04F06F08",
      example  : "GB29NWBK60161331926819",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : false,
      euMember : true
    },
    {
      country  : "GE",
      length   : 22,
      structure: "U02F16",
      example  : "GE29NB0000000101904917",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "GI",
      length   : 23,
      structure: "U04A15",
      example  : "GI75NWBK000000007099453",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : false,
      euMember : false
    },
    {
      country  : "GL",
      length   : 18,
      structure: "F04F09F01",
      example  : "GL8964710001000206",
      required : false,
      sepa     : true,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "GR",
      length   : 27,
      structure: "F03F04A16",
      example  : "GR1601101250000000012300695",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "GT",
      length   : 28,
      structure: "A04A20",
      example  : "GT82TRAJ01020000001210029690",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "HR",
      length   : 21,
      structure: "F07F10",
      example  : "HR1210010051863000160",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : false,
      euMember : true
    },
    {
      country  : "HU",
      length   : 28,
      structure: "F03F04F01F15F01",
      example  : "HU42117730161111101800000000",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : false,
      euMember : true
    },
    {
      country  : "IE",
      length   : 22,
      structure: "U04F06F08",
      example  : "IE29AIBK93115212345678",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "IL",
      length   : 23,
      structure: "F03F03F13",
      example  : "IL620108000000099999999",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "IS",
      length   : 26,
      structure: "F04F02F06F10",
      example  : "IS140159260076545510730339",
      required : false,
      sepa     : true,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "IT",
      length   : 27,
      structure: "U01F05F05A12",
      example  : "IT60X0542811101000000123456",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "KW",
      length   : 30,
      structure: "U04A22",
      example  : "KW81CBKU0000000000001234560101",
      required : true,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "KZ",
      length   : 20,
      structure: "F03A13",
      example  : "KZ86125KZT5004100100",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "LB",
      length   : 28,
      structure: "F04A20",
      example  : "LB62099900000001001901229114",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "LI",
      length   : 21,
      structure: "F05A12",
      example  : "LI21088100002324013AA",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : false,
      euMember : false
    },
    {
      country  : "LT",
      length   : 20,
      structure: "F05F11",
      example  : "LT121000011101001000",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : false,
      euMember : true
    },
    {
      country  : "LU",
      length   : 20,
      structure: "F03A13",
      example  : "LU280019400644750000",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "LV",
      length   : 21,
      structure: "U04A13",
      example  : "LV80BANK0000435195001",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : false,
      euMember : true
    },
    {
      country  : "MC",
      length   : 27,
      structure: "F05F05A11F02",
      example  : "MC5811222000010123456789030",
      required : false,
      sepa     : true,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "MD",
      length   : 24,
      structure: "A20",
      example  : "MD24AG000225100013104168",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "ME",
      length   : 22,
      structure: "F03F13F02",
      example  : "ME25505000012345678951",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "MK",
      length   : 19,
      structure: "F03A10F02",
      example  : "MK07250120000058984",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "MR",
      length   : 27,
      structure: "F05F05F11F02",
      example  : "MR1300020001010000123456753",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "MT",
      length   : 31,
      structure: "U04F05A18",
      example  : "MT84MALT011000012345MTLCAST001S",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "MU",
      length   : 30,
      structure: "U04F02F02F12F03U03",
      example  : "MU17BOMM0101101030300200000MUR",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "NL",
      length   : 18,
      structure: "U04F10",
      example  : "NL91ABNA0417164300",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "NO",
      length   : 15,
      structure: "F04F06F01",
      example  : "NO9386011117947",
      required : false,
      sepa     : true,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "PK",
      length   : 24,
      structure: "U04A16",
      example  : "PK36SCBL0000001123456702",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "PL",
      length   : 28,
      structure: "F08F16",
      example  : "PL61109010140000071219812874",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : false,
      euMember : true
    },
    {
      country  : "PS",
      length   : 29,
      structure: "U04A21",
      example  : "PS92PALS000000000400123456702",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "PT",
      length   : 25,
      structure: "F04F04F11F02",
      example  : "PT50000201231234567890154",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "RO",
      length   : 24,
      structure: "U04A16",
      example  : "RO49AAAA1B31007593840000",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : false,
      euMember : true
    },
    {
      country  : "RS",
      length   : 22,
      structure: "F03F13F02",
      example  : "RS35260005601001611379",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "SA",
      length   : 24,
      structure: "F02A18",
      example  : "SA0380000000608010167519",
      required : true,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "SE",
      length   : 24,
      structure: "F03F16F01",
      example  : "SE4550000000058398257466",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : false,
      euMember : true
    },
    {
      country  : "SI",
      length   : 19,
      structure: "F05F08F02",
      example  : "SI56263300012039086",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : false,
      euMember : true
    },
    {
      country  : "SK",
      length   : 24,
      structure: "F04F06F10",
      example  : "SK3112000000198742637541",
      required : true,
      sepa     : true,
      euReg    : true,
      eur      : true,
      euMember : true
    },
    {
      country  : "SM",
      length   : 27,
      structure: "U01F05F05A12",
      example  : "SM86U0322509800000000270100",
      required : false,
      sepa     : true,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "TN",
      length   : 24,
      structure: "F02F03F13F02",
      example  : "TN5910006035183598478831",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "TR",
      length   : 26,
      structure: "F05A01A16",
      example  : "TR330006100519786457841326",
      required : true,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    },
    {
      country  : "VG",
      length   : 24,
      structure: "U04F16",
      example  : "VG96VPVG0000012345678901",
      required : false,
      sepa     : false,
      euReg    : false,
      eur      : false,
      euMember : false
    }
  ];

  function iban_getRecordByCountry(ilbced, country) {
    var record, i;
    for (i = 0; i < ilbced.length; i++) {
      record = ilbced[i];
      if (record.country === country) {
        return record;
      }
    }
    return null;
  }

  function iban_testpart(pattern, kind) {
    var testpattern = "(";
    if (kind === "any") {
      testpattern += ".";
    } else {
      testpattern += "[";
      if (kind === "reverse") {
        testpattern += "^";
      }
      switch (pattern.substr(0, 1)) {
        case "A":
          testpattern += "0-9A-Za-z";
          break;
        case "B":
          testpattern += "0-9A-Z";
          break;
        case "C":
          testpattern += "A-Za-z";
          break;
        case "F":
          testpattern += "0-9";
          break;
        case "L":
          testpattern += "a-z";
          break;
        case "U":
          testpattern += "A-Z";
          break;
        case "W":
          testpattern += "0-9a-z";
          break;
      }
      testpattern += "]";
    }
    if (((pattern.substr(1, 2) * 1) > 1) && (kind !== "reverse")) {
      testpattern += "{" + String(pattern.substr(1, 2) * 1) + "}";
    }
    testpattern += ")";
    return testpattern;
  }

  function iban_buildtest(structure, kind) {
    var result, testpattern, patterncount, i;
    result = "";
    testpattern = structure.match(/([ABCFLUW]\d{2})/g);
    patterncount = testpattern.length;
    for (i = 0; i < patterncount; ++i) {
      if (((kind >= 0) && (i !== kind)) || (kind === -2)) {
        result += iban_testpart(testpattern[i], "any");
      } else {
        result += iban_testpart(testpattern[i], "standard");
      }
    }
    return new RegExp(result);
  }

  function iban_getstructurealert(structure, iban) {
    var any = -2;
    structure = "B04" + structure;
    var result = "";
    var failpattern = "";
    var testpattern = structure.match(/([ABCFLUW]\d{2})/g);
    var patterncount = testpattern.length;
    var ibanparts;
    for (var i = 0; i < patterncount; ++i) {
      failpattern = iban_buildtest(structure, i);
      if (!failpattern.test(iban)) {
        failpattern = iban_buildtest(structure, any);
        ibanparts = iban.match(failpattern);
        var ibanpart = ibanparts[i + 1];
        var partpattern = structure.match(/([ABCFLUW]\d{2})/g);
        var failures = ibanpart.match(new RegExp(iban_testpart(partpattern[i], "reverse"), "g"));
        var failure = 0;
        var pos = 0;
        while ((pos < ibanpart.length) && (failure < failures.length)) {
          if (ibanpart.substr(pos, 1) === failures[failure]) {
            result += "|" + ibanpart.substr(pos, 1) + "|";
            ++failure;
          }
          else {
            result += ibanpart.substr(pos, 1);
          }
          ++pos;
        }
        result += ibanpart.substr(pos) + " ";
      }
      else {
        ibanparts = iban.match(failpattern);
        result += ibanparts[i + 1] + " ";
      }
    }
    result = result.replace(/||/g, "");
    return result;
  }

  function iban_checkIBAN(iban) {
    var result = {
      valid : true,
      reason: ""
    };

    var standard = -1;
    var illegal = /\W|_/; // contains chars other than (a-zA-Z0-9)
    if (illegal.test(iban)) { // yes, alert and exit
      illegal = /((\W|_)+)/g;
      var ichars;
      var aliban = "";
      var lindex = -1;
      while (ichars = illegal.exec(iban)) {
        aliban += iban.substring(lindex + 1, ichars.index) + "<strong>" + ichars[1] + "</strong>";
        lindex = ichars.index;
      }
      aliban += iban.substr(lindex + 1);
      aliban = aliban.replace(/\|/g, "%7C");
      result.reason = aliban + "\n\n" + iban_altxt[0];
      result.valid = false;
      return result;
    }
    else { // no, continue
      illegal = /^\D\D\d\d.+/; // first chars are letter letter digit digit
      if (illegal.test(iban) === false) { // no, alert and exit
        result.reason = "|" + iban.substr(0, 4) + "|" + iban.substr(5) + "\n\n" + iban_altxt[1];
        result.valid = false;
        return result;
      }
      else { // yes, continue
        illegal = /^\D\D00.+|^\D\D01.+|^\D\D99.+/; // check digit are 00 or 01 or 99
        if (illegal.test(iban)) { // yes, alert and exit
          result.reason = iban.substr(0, 2) + "|" + iban.substr(2, 2) + "|" + iban.substr(5) + "\n\n" + iban_altxt[2];
          result.valid = false;
          return result;
        }
        else { // no, continue
          var countryCode = iban.substr(0, 2).toUpperCase();
          var record = iban_getRecordByCountry(iban_ilbced, countryCode);
          var ibanLength;
          if (record !== null) {
            ibanLength = record.length;
          }
          else { // not respected, set reason
            result.reason = iban_altxt[3] + iban.substr(0, 2).toUpperCase() + iban_altxt[4];
            ibanLength = iban.length;
          }  // but continue
          if ((iban.length - ibanLength) !== 0) { // fits length to country
            result.reason = iban_altxt[5] + countryCode + iban_altxt[6] + ibanLength + iban_altxt[7];
            result.valid = false;
            return result;
          } // yes, continue
          if (record !== null) {
            illegal = iban_buildtest("B04" + record.structure, standard);
          } // fetch sub structure of respected country
          else {
            illegal = /.+/;
          } // or take care of not respected country
          if (illegal.test(iban) === false) { // fits sub structure to country
            result.reason = iban_getstructurealert(record.structure, iban) + "\n\n" + iban_altxt[1];
            result.valid = false;
            return result;
          }
          else { // yes, continue
            var preparedStr = iban_ISO13616Prepare(iban);
            var remainderStr = iban_ISO7064Mod97_10(preparedStr);
            if (remainderStr === "1") {
              result.reason = iban_altxt[8];
              result.valid = true;
            }
            else {
              result.reason = iban_altxt[9];
              result.valid = false;
            }
            return result;
          }
        }
      }
    }
  }

  function iban_checkIBAN_WS(iban) {
    return iban_checkIBAN(iban.replace(/\s+/g, ''));
  }

  _export.isValidIBAN = function (context, value) {
    if (!value)
      return false;
    return iban_checkIBAN_WS(value).valid;
  }

  _export.notValidIBAN = function (context, value) {
    if (!value)
      return true;
    return !iban_checkIBAN_WS(value).valid;
  }

  _export.isTransactionFieldIBANCountry = function (field) {
    return function (context, value) {
      var fieldValue = context.getTransactionField(resolveConstant(field));
      if (fieldValue) {
        var record, i, result;
        for (i = 0; i < iban_ilbced.length; i++) {
          record = iban_ilbced[i];
          if (record.country === fieldValue) {
            return true;
          }
        }
      }
      return false;
    }
  }

  _export.isValidSDAAmount = function (context, value) {
    var SDALimit = 1000000.00;
    var localIDAmount = _export.internalUtils.localAmountForIDNumber(context, value);

    return localIDAmount <= SDALimit ? true : false;
  }

  _export.isValidFIAAmount = function (context, value) {
    var FIALimit = 1000000.00;
    var localRegAmount = _export.internalUtils.localAmountForIDNumberFIA(context, value);

    return localRegAmount <= FIALimit ? true : false;
  }
  
  _export.getCustomDisableFields = function (context, value) {
    var customCodes = _export.internalUtils.customDisableCategoryCodes(context, value);

    return customCodes;
  }

  _export.hasSDAMonetaryValue = function (context, value) {
    var idNumber = "";

    for (var i = 0; i < context.getMoneySize(); i++) {
      var subjectField = context.getMoneyField(i, "AdHocRequirement.Subject");
      var AmountField = context.getMoneyField(i, _export.getMap("{{LocalValue}}"));

      if (subjectField == "SDA" && AmountField != null) {
        AmountField = AmountField.replace(",", "");
        if(parseFloat(AmountField) > 0) {
          if (context.getMoneyField(i, "ThirdParty.Individual.IDNumber"))
            idNumber = context.getMoneyField(i, "ThirdParty.Individual.IDNumber");
          else if (context.getTransactionField("Resident.Individual.IDNumber"))
            idNumber = context.getTransactionField("Resident.Individual.IDNumber");

          if(idNumber != "" && idNumber == value) {
          return true;
          }
        }
      }
    }

    return false;
  }

  _export.hasFIAMonetaryValue = function (context, value) {
    var idNumber = "";

    for (var i = 0; i < context.getMoneySize(); i++) {
      // var subjectField = context.getMoneyField(i, "AdHocRequirement.Subject");
      var categoryCode = context.getMoneyField(i, "CategoryCode");
      var AmountField = context.getMoneyField(i, _export.getMap("{{LocalValue}}"));

      if ((categoryCode == "512" || categoryCode == "513") && AmountField != null) {
        AmountField = AmountField.replace(",", "");
        if(parseFloat(AmountField) > 0) {
          if (context.getMoneyField(i, "ThirdParty.Individual.IDNumber"))
            idNumber = context.getMoneyField(i, "ThirdParty.Individual.IDNumber");
          else if (context.getTransactionField("Resident.Individual.IDNumber"))
            idNumber = context.getTransactionField("Resident.Individual.IDNumber");

          if(idNumber != "" && idNumber == value) {
            return true;
          }
        }
      }
    }

    return false;
  }

  _export.isTransactionFieldIBANReqCountry = function (field) {
    return function (context, value) {
      var fieldValue = context.getTransactionField(resolveConstant(field));
      if (fieldValue) {
        var record, i, result;
        for (i = 0; i < iban_ilbced.length; i++) {
          record = iban_ilbced[i];
          if (record.country === fieldValue) {
            return record.required;
          }
        }
      }
      return false;
    }
  }

  _export.notTransactionFieldIBANReqCountry = function (field) {
    return function (context, value) {
      var fieldValue = context.getTransactionField(resolveConstant(field));
      if (fieldValue) {
        var record, i, result;
        for (i = 0; i < iban_ilbced.length; i++) {
          record = iban_ilbced[i];
          if (record.country === fieldValue) {
            return !record.required;
          }
        }
      }
      return true;
    }
  }

  _export.isTransactionFieldEUCountry = function (field) {
    return function (context, value) {
      var fieldValue = context.getTransactionField(resolveConstant(field));
      if (fieldValue) {
        var record, i, result;
        for (i = 0; i < iban_ilbced.length; i++) {
          record = iban_ilbced[i];
          if (record.country === fieldValue) {
            return record.euMember;
          }
        }
      }
      return false;
    }
  }

  _export.notTransactionFieldEUCountry = function (field) {
    return function (context, value) {
      var fieldValue = context.getTransactionField(resolveConstant(field));
      if (fieldValue) {
        var record, i, result;
        for (i = 0; i < iban_ilbced.length; i++) {
          record = iban_ilbced[i];
          if (record.country === fieldValue) {
            return !record.euMember;
          }
        }
      }
      return true;
    }
  }

  _export.hasMoneyCardValue = function (context, value) {
    var total = 0;
    var c1value = context.getMoneyField(context.currentMoneyInstance, _export.getMap("ForeignCardHoldersPurchases{{LocalValue}}"));
    var c2value = context.getMoneyField(context.currentMoneyInstance, _export.getMap("ForeignCardHoldersCashWithdrawals{{LocalValue}}"));
    if (c1value)
      total += Number(c1value);
    if (c2value)
      total += Number(c2value);
    var cents = Math.round(total * 100);
    return cents != 0;
  }

  _export.isForeignGambling = function (context, value) {
    var merchantCode = context.getTransactionField('NonResident.Entity.CardMerchantCode');
    if (merchantCode &&
        (merchantCode === '7995'
            || merchantCode === "7800"
            || merchantCode === "7801"
            || merchantCode === "7802")) {
      var cardType = context.getMoneyField(context.currentMoneyInstance, 'CardIndicator');
      var posEntryMode = context.getMoneyField(context.currentMoneyInstance, 'POSEntryMode');
      if (cardType) {
        var regPEM;
        if (cardType === 'VISA') {
          regPEM = /^(00|01|95)$/;
        } else if (cardType === 'MASTER') {
          regPEM = /^[09][0T]$/;
        } else if (cardType === 'AMEX') {
          regPEM = /^[016][056]$/;
        } else if (cardType === 'DINERS') {
          regPEM = /^[12349][016]$/;
        } else if (cardType === 'MAESTRO') {
          regPEM = /^(00|01)$/;
        } else if (cardType === 'ELECTRON') {
          regPEM = /^(00|01|95)$/;
        }
        if (regPEM) {
          if (!value)
            return false;

          return regPEM.test(value);
        }
      }
    }
    return false;
  }

  
  /**
   * This is to remove the uglify-js dependency. 
   * 
   * Instead of parsing the rule files, just excecute them with this predef as input and the assertions will return as
   * partially applied functions which when run will return a string representation of the assertion.
   * 
   * N.B. you need to run predef.fnAndOrStrUp() BEFORE applying this predef, and run predef.fnAndOrUp() after to resume normal opperation.
   */
  _export.generateStringPredef = function(){

    var Pr={};
    Object.assign(Pr,_export);
  
    var DSLFnMatcher = /function\s*\w*\s*\(\s*context\s*,\s*value\s*\)/g;
  
    for (var i in Pr) {
      if (Pr.hasOwnProperty(i) && (typeof Pr[i] == 'function') && (Pr[i].toString().match(DSLFnMatcher))) {
        (function (i) {
  
          Pr[i] = function () {
            if (arguments.length > 0) {
              var args = Array.prototype.slice.apply(arguments);
              for (var a, ind = 0; a = args[ind]; ind++) {
                if (typeof a == 'string') args[ind] = '"' + a + '"';
                else if (typeof a == 'function') args[ind] = args[ind]();
                // numbers can fall through...
              }
              return function () { return i+'('+args.join(', ')+')'; }
            } else {
              return i;
            }
          }
        })(i)
      }
    }
  
  
    return Pr;
  }



  return _export;

})
