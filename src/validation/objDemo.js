define(['require', 'coreRules', './objContext'], function (require, rules, objContext) {
  rules.setRuleParameters({
    // Set up rule parameters for this demo
    todayDate : new Date(2013, 0, 20),
    goLiveDate: new Date(2013, 7, 18)
  });

  function addText(elementId) {
    return function(text) {
      var objTo = document.getElementById(elementId);
      var para = document.createElement("p");
      para.innerHTML = text;
      objTo.appendChild(para)
    }
  }


  function processValidations(obj, customData, displayFunc) {
    var ctx = objContext.produceObjContext(obj);
    var wrappedCtx = rules.wrapContext(ctx, rules.lookups, customData);

    var rule_sets = [rules.stdTrans, rules.stdMoney, rules.stdImportExport];
    var rulesFound = rules.runValidation(wrappedCtx, rule_sets);

    for (var validation, i = 0; validation = ctx.validationEvent[i]; i++) {
      displayFunc(validation.type + ': scope=' + validation.event + ', field=' + validation.field +
          (validation['instance'] ? '[' + validation.instance + ']': '') +
          ', code=' + validation.code + ', msg=' + validation.msg);
    }
  }

  function processDisplays(obj, customData, displayFunc) {
    var ctx = objContext.produceObjContext(obj);
    var wrappedCtx = rules.wrapContext(ctx, rules.lookups, customData);

    var display_set = [rules.detailDisplay.detailTrans, rules.detailDisplay.detailMoney, rules.detailDisplay.detailImportExport];
    var rulesFound = rules.runDisplayRules(wrappedCtx, display_set);

    for (var display, i = 0; display = ctx.displayEvent[i]; i++) {
      displayFunc(display.type + ': scope=' + display.event + ', field=' + display.field +
          ', value=' + display.value);
    }
  }

  var _export = {
    processValidationRules: function (obj, customData, elementId) {
      processValidations(obj, customData, addText(elementId));
    },

    processDisplayRules: function (obj, customData, elementId) {
      processDisplays(obj, customData, addText(elementId));
    }
  }


  return _export;

})