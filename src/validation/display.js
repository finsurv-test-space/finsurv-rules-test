define([ 'require', './predef'], function ( require, predef) {


  function logDisplayEvent(context, appendedFields, assertion, scope, type, field, setting, srcField, moneyInstance, ieInstance) {
    var logEvent = true;
    var composedSetting;

    var fieldValue = null;
    if (scope === "transaction") {
      fieldValue = context.getTransactionField(field);
    }
    if (scope === "money") {
      if (!moneyInstance) {
        moneyInstance = 0;
      }
      fieldValue = context.getMoneyField(moneyInstance, field);
    }
    if (scope === "importexport") {
      if (!moneyInstance) {
        moneyInstance = 0;
      }
      if (!ieInstance) {
        ieInstance = 0;
      }
      fieldValue = context.getImportExportField(moneyInstance, ieInstance, field);
    }

    if((typeof setting === "function")&&(type!='FILTER')){
      // if setting is a function, assume that srcField is the assertion.
      assertion = srcField;
      srcField = undefined;
      //composedSetting = setting(context,fieldValue);
    } //else {
      composedSetting = setting;
    //}

    if (srcField) {
      var srcValue = null;

      if (typeof srcField === "string") {
        var lookupScope = scope;
        var posScope = srcField.indexOf("::");
        if (posScope > -1) {
          lookupScope = srcField.substr(0, posScope);
          srcField = srcField.substr(posScope + 2);
        }
        if (lookupScope === "transaction") {
          srcValue = context.getTransactionField(srcField);
        }
        if (lookupScope === "money") {
          if (!moneyInstance) {
            moneyInstance = 0;
          }
          srcValue = context.getMoneyField(moneyInstance, srcField);
        }
        if (lookupScope === "importexport") {
          if (!moneyInstance) {
            moneyInstance = 0;
          }
          if (!ieInstance) {
            ieInstance = 0;
          }
          srcValue = context.getImportExportField(moneyInstance, ieInstance, srcField);
        }
      }



      if (typeof srcField === "function") {
        context.currentMoneyInstance = moneyInstance;
        context.currentImportExportInstance = ieInstance;

        srcValue = srcField(context, fieldValue);
      }

      if (!srcValue) {
        logEvent = false;
      }



      if (setting && srcValue) {
        composedSetting = setting.replace("%s", String(srcValue));
      }
    }


    if (type === "SET" && fieldValue && fieldValue == composedSetting) {
      logEvent = false;
    }
    if (logEvent && assertion && !assertion(context, fieldValue)) {
      logEvent = false;
    }

    if (logEvent) {
      if (type === "APP") {
        if (appendedFields.indexOf(field + ":" + moneyInstance + ":" + ieInstance) == -1) {
          type = "SET";
          appendedFields.push(field + ":" + moneyInstance + ":" + ieInstance);
        }
      }

      if (scope === "transaction") {
        context.logTransactionDisplayEvent(type, field, composedSetting);
      }
      if (scope === "money") {
        context.logMoneyDisplayEvent(type, moneyInstance, field, composedSetting);
      }
      if (scope === "importexport") {
        context.logImportExportDisplayEvent(type, moneyInstance, ieInstance, field, composedSetting);
      }

    }
  }

  function displayRule(scope, field, type, setting, srcField, categories, notCategories, assertion) {
    if(field.indexOf("{{")!=-1) field = predef.getMap(field);
    if(typeof srcField == 'string' && srcField.indexOf("{{")!=-1) srcField = predef.getMap(srcField);
    return function (context, appendedFields) {
      var matchedRule = false;
      var runRule;
      var moneySize;
      var moneyCategory;
      var moneyMainCategory;
      var i;
      if (scope === "transaction") {
        matchedRule = true;
        logDisplayEvent(context, appendedFields, assertion, scope, type, field, setting, srcField);
      } else if (scope === "money") {
        moneySize = context.getMoneySize();
        for (i = 0; i < moneySize; i++) {
          runRule = true;
          moneyCategory = context.categories[i];
          moneyMainCategory = context.mainCategories[i];
          if (categories && categories.indexOf(moneyCategory) == -1 && categories.indexOf(moneyMainCategory) == -1)
            runRule = false;
          if (notCategories && !predef.categoryNotInList(moneyCategory, moneyMainCategory, notCategories))
            runRule = false;
          if (runRule) {
            matchedRule = true;
            context.currentMoneyInstance = i;
            logDisplayEvent(context, appendedFields, assertion, scope, type, field, setting, srcField, i);
          }
        }
      } else if (scope === "importexport") {
        moneySize = context.getMoneySize();
        for (i = 0; i < moneySize; i++) {
          runRule = true;
          moneyCategory = context.categories[i];
          moneyMainCategory = context.mainCategories[i];
          if (categories && categories.indexOf(moneyCategory) == -1 && categories.indexOf(moneyMainCategory) == -1)
            runRule = false;
          if (notCategories && !predef.categoryNotInList(moneyCategory, moneyMainCategory, notCategories))
            runRule = false;

          if (runRule) {
            var importSize = context.getImportExportSize(i);
            for (var j = 0; j < importSize; j++) {
              matchedRule = true;
              context.currentMoneyInstance = i;
              context.currentImportExportInstance = j;
              logDisplayEvent(context, appendedFields, assertion, scope, type, field, setting, srcField, i, j);
            }
          }
        }
      }
      return matchedRule;
    };
  }

  /*
   * Helper function to expand the rules with an Array of fields...(multi-field-rules)
   */
  function insertDisplayRules(rulelist, fieldName, ruleset, rule) {
    rulelist.push(displayRule(ruleset.scope, fieldName, rule.type, rule.value, rule.srcField, rule.categories, rule.notCategories, rule.assertion));
  }

  // function insertDisplayRules(resultMap, fieldName, ruleset, rule) {
  //   if(!resultMap[fieldName]) resultMap[fieldName] = [];
  //   resultMap[fieldName].push(displayRule(ruleset.scope, fieldName, rule.type, rule.value, rule.srcField, rule.categories, rule.notCategories, rule.assertion));
  // }

  /**
   * Checks if a rule should be run based on where in the heirarcy it is and whether extend() is used.
   * @param {*} fieldEntry - Rule entry
   * @param {*} fieldName - field name
   * @param {*} nameList - MUTATED - adds to fieldName-Section map to list if conditions met.
   * @param {*} ruleMap - MUTATED - Needed to keep track of Extends.
   * @param {*} fieldNames - MUTATED - List of field names
   * @param {*} section - The section to filter by.
   */
  function filterOverrides(fieldEntry, fieldName, nameList, ruleMap, fieldNames, section) {
    var sectionField = fieldName + '.' + section;
    if (nameList.indexOf(sectionField) === -1) {
      nameList.push(sectionField);
      ruleMap[sectionField] = fieldEntry;
      fieldNames.push(fieldName);
    } else {
      // the order is serial, so we can just carry on until there is no more "EXTEND" rule.
      var prevRule = ruleMap[sectionField];
      var extend = prevRule.display.find(function (rule) {
        return rule.type == "EXTEND"
      });
      if (extend) {
        ruleMap[sectionField] = fieldEntry;
        fieldNames.push(fieldName);
      }
    }
  }

  /*
  Create array of applicable field names...
  ...this is what causes display rules to override...
  */
  function displayRulesFromRuleSets(rulesets, flow, section, categories, mainCategories, filter) {
    var result = [];
    var resultMap = {}
    for (var rs = 0; rs < rulesets.length; rs++) {

      var nameList = [];
      var ruleMap = {};
      var ruleset = rulesets[rs];
      for (var vs = 0; vs < ruleset.fields.length; vs++) {
        var fieldEntry = ruleset.fields[vs];

        var fieldNames = [];

        if (Array.isArray(fieldEntry.field)) {
          for (var field, j = 0; field = fieldEntry.field[j]; j++) {
            filterOverrides(fieldEntry, field, nameList, ruleMap, fieldNames, section);
          }
        }
        else {
          filterOverrides(fieldEntry, fieldEntry.field, nameList, ruleMap, fieldNames, section);
        }

        for (var fieldName, k = 0; fieldName = fieldNames[k]; k++) {
          if (fieldEntry.display) {
            for (var r = 0; r < fieldEntry.display.length; r++) {
              var i;
              var displayRule = fieldEntry.display[r];

              if (flow && displayRule.flow && !(displayRule.flow === flow))
                continue;
              if(typeof section === 'string' && section ===""){
                if(displayRule.section)
                  continue;
              }
              if (section && displayRule.section && displayRule.section.indexOf(section) == -1)
                continue;
              if (categories && displayRule.categories) {
                var hasCategory = false;
                for (i = 0; i < categories.length; i++) {
                  if (displayRule.categories.indexOf(categories[i]) >= 0) {
                    hasCategory = true;
                    break;
                  }
                }
                if (!hasCategory) {
                  for (i = 0; i < mainCategories.length; i++) {
                    if (displayRule.categories.indexOf(mainCategories[i]) >= 0) {
                      hasCategory = true;
                      break;
                    }
                  }
                }
                if (!hasCategory)
                  continue;
              }
              if (categories && displayRule.notCategories) {
                if (!predef.categoriesNotInList(categories, mainCategories, displayRule.notCategories))
                  continue;
              }

              if (filter && filter != fieldName)
                continue;

              insertDisplayRules(result, fieldName, ruleset, displayRule);
            }
          }
        }
      }
    }

    return result;
  }

  function runDisplayRules(context, rulesets, filter) {
    var rulesRun = 0;
    var filteredRules = displayRulesFromRuleSets(rulesets, context.flow, context.section, context.categories, context.mainCategories, filter);
    var appendedFields = [];

    for (var r = 0; r < filteredRules.length; r++) {
      var rule = filteredRules[r];
      if (rule(context, appendedFields))
        rulesRun++;
    }
    return rulesRun > 0;
  }

  return {
    runDisplayRules: runDisplayRules
  }

});