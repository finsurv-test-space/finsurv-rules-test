function getTestDataByField(scope, obj, field) {
    var fieldParts = field.split('.');
    var localObj = obj;
    for (var i=0; i<fieldParts.length; i++) {
        var fieldPart = fieldParts[i];
        if (fieldPart in localObj) {
            localObj = localObj[fieldPart];
        }
        else {
            localObj = null;
        }
        var goDeep = !localObj;
        if (goDeep && typeof localObj == "string" && localObj.length == 0)
          goDeep = false;

        if (goDeep) {
            if (i >= fieldParts.length-1)
                return null;
            else {
                if (scope === 'transaction' &&
                        (field.indexOf('Resident.') == 0 || field.indexOf('NonResident.') == 0) &&
                        i < 2)
                    return undefined;
                else
                    return null;
            }
        }
    }
    return localObj;
}

/*
 status: 'pass', 'fail', 'error', 'busy'
 */
function wrapCallbacks() {
  /*
   customValidation : [
   IVS_Validation: {
   func : function() {}
   },
   CCN_Validation: {
   func : function() {}
   }
   ]
   */
  var callbacks = {};
  var callbackResults = {};

  function lookupScopedFieldValue(context, scope, field) {
    var lookupScope = scope;
    var lookupField = field;
    var moneyInstance = context.currentMoneyInstance;
    var ieInstance = context.currentImportExportInstance;
    var lookupValue;
    var posScope = lookupField.indexOf("::");
    if (posScope > -1) {
      lookupScope = lookupField.substr(0, posScope);
      lookupField = lookupField.substr(posScope+2);
    }
    if (lookupScope === "transaction") {
      lookupValue = context.getTransactionField(lookupField);
    }
    if (lookupScope === "money") {
      if (!moneyInstance) {
        moneyInstance = 0;
      }
      lookupValue = context.getMoneyField(moneyInstance, lookupField);
    }
    if (lookupScope === "importexport") {
      if (!moneyInstance) {
        moneyInstance = 0;
      }
      if (!ieInstance) {
        ieInstance = 0;
      }
      lookupValue = context.getImportExportField(moneyInstance, ieInstance, lookupField);
    }
    return lookupValue;
  }

  function lookupScopedFieldValues(context, scope, fieldList) {
    var result = [];
    for ( var i = 0; i < fieldList.length; i++) {
      result.push(lookupScopedFieldValue(context, scope, fieldList[i]));
    }
    return result;
  }

  function processCall(name, value, scope, context, resultCallback) {
    if (name in callbacks) {
      var callback = callbacks[name];

      var inputArray = lookupScopedFieldValues(context, scope, callback.inputs);

      // check to see if we have cached value
      var resultList;
      var result;
      if (name in callbackResults) {
        resultList = callbackResults[name];
        for (var i = 0; i < resultList.length; i++) {
          result = resultList[i];
          if (value === result.value) {
            if (result.status !== 'pass')
              resultCallback(result.status, result.code, result.message);
            return;
          }
        }
      }
      else {
        resultList = [];
        callbackResults[name] = resultList;
      }

      result = {value: value, status: 'busy', code: '', message: 'awaiting validation'};
      resultList.push(result);
      resultCallback(result.status, result.code, result.message);

      var handleResult = function(status, code, message) {
        result.status = status;
        result.code = code;
        result.message = message;
        resultCallback(result.status, result.code, result.message);
      };

      // make call (function is of form -- function(value,arg1,...,argN,resultCallback) -- )
      var args = [value].concat(inputArray).concat(handleResult);
      callback.func.apply(context,args);// the 'this' context is not important, so pass null.

    }
    else {
      resultCallback('fail', '', 'The validation callback ' + name + ' has not been defined');
    }
  }

  return {
    registerValidationCallback: function (name, callback) {
      callbacks[name] = {};
      callbacks[name].func = callback;
      // The rest of the arguments are the 'inputs'
      callbacks[name].inputs = [].splice.call(arguments,2);
    },
    validate: function(name, value, scope, context, resultCallback) {
      processCall(name, value, scope, context, resultCallback);
    }
  };
}

function produceTestContext(obj) {
    obj.raisedEvent = [];
    obj.displayEvent = [];
    obj.callbacks = wrapCallbacks();

    obj.getTransactionField = function(field) {
        return getTestDataByField('transaction', obj, field);
    };

    obj.getMonetaryAmount =  function(){
       return Array.isArray(obj.MonetaryAmount) ? obj.MonetaryAmount: undefined;
    }

    obj.getMoneySize = function() {
        var moneyList = obj.getMonetaryAmount();
        if (moneyList)
            return moneyList.length;
        return 0;
    };

    obj.getMoneyField = function(instance, field) {
        var moneyList = obj.getMonetaryAmount();
        if (moneyList)
            return getTestDataByField('money', moneyList[instance], field);
        return null;
    };

    obj.getImportExportSize = function(moneyInstance) {
        var moneyList = obj.getMonetaryAmount();
        if (moneyList) {
            var money = moneyList[moneyInstance];
            var importList = money.ImportExport;
            if (importList) {
                return importList.length;
            }
        }
        return 0;
    };

    obj.getImportExportField = function(moneyInstance, instance, field) {
        var moneyList = obj.getMonetaryAmount();
        if (moneyList) {
            var money = moneyList[moneyInstance];
            var importList = money.ImportExport;
            if (importList) {
                return getTestDataByField('importexport', importList[instance], field);
            }
        }
        return null;
    };

    obj.logTransactionEvent = function(type, field, code, msg) {
      if(type=="MANDATORY") return;
      if(type==='BUSY'||type==='SUCCESS') return;

      this.raisedEvent.push({event: 'transaction', type: type, field: field, code: code, msg: msg });
    };

    obj.logMoneyEvent = function(type, instance, field, code, msg) {
      if(type=="MANDATORY") return;
      if(type==='BUSY'||type==='SUCCESS') return;

      this.raisedEvent.push({event: 'money', instance: instance, type: type, field: field, code: code, msg: msg });
    };

    obj.logImportExportEvent = function(type, moneyInstance, instance, field, code, msg) {
      if(type=="MANDATORY") return;
      if(type==='BUSY'||type==='SUCCESS') return;

      this.raisedEvent.push({event: 'importexport', moneyInstance: moneyInstance, instance: instance, type: type, field: field, code: code, msg: msg });
    };

    obj.logTransactionDisplayEvent = function(type, field, value) {
        this.displayEvent.push({event: 'transaction', type: type, field: field, value: value });
    };

    obj.logMoneyDisplayEvent = function(type, instance, field, value) {
        this.displayEvent.push({event: 'money', instance: instance, type: type, field: field, value: value });
    };

    obj.logImportExportDisplayEvent = function(type, moneyInstance, instance, field, value) {
        this.displayEvent.push({event: 'importexport', moneyInstance: moneyInstance, instance: instance, type: type, field: field, value: value });
    };

    return obj;
}


define({
    produceTestContext:produceTestContext
})