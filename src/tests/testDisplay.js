/**
 * Created by petruspretorius on 30/07/2015.
 */

define(function () {
  return function (testBase) {
    with (testBase) {

      testCases = [

        /////////////////////////////////////////
        // SUMMARY
        /////////////////////////////////////////
        /////////////////////////////////////////
        // TRANSACTION SUMMARY
        /////////////////////////////////////////

        // MainTransferPurpose
        assertShow('summary', "MainTransferPurpose", { ReportingQualifier: 'BOPCUS' }),
        assertHide('summary', "MainTransferPurpose", { ReportingQualifier: 'NON REPORTABLE' }),
        assertNoEvent('summary', "MainTransferPurpose", {}),

        // NonResidentTitle
        assertValue('summary', "NonResidentTitle", "Beneficiary Details", { Flow: 'OUT' }),
        assertValue('summary', "NonResidentTitle", "Remitter Details", { Flow: 'IN' }),

        //Resident.Description
        assertValue('summary', "Resident.Description", "Foo, Bar", { Resident: { Individual: { Name: 'Foo', Surname: "Bar" } } }),
        assertValue('summary', "Resident.Description", "Foo, t/a Bar", { Resident: { Entity: { EntityName: 'Foo', TradingName: "Bar" } } }),
        assertValue('summary', "Resident.Description", "Foo", { Resident: { Exception: { ExceptionName: 'Foo' } } }),

        //"Resident.AccountDescription"
        assertValue('summary', "Resident.AccountDescription", "Foo, [123], Bar", { Resident: { Individual: { AccountIdentifier: 'Foo', AccountNumber: '123', AccountName: 'Bar' } } }),
        assertValue('summary', "Resident.AccountDescription", "Foo, [123], Bar", { Resident: { Entity: { AccountIdentifier: 'Foo', AccountNumber: '123', AccountName: 'Bar' } } }),


        //"Resident.ContactDetailsDescription"
        assertValue('summary', "Resident.ContactDetailsDescription", "Foo, Bar, email: foo@bar, tel: 123, fax: 345",
          { Resident: { Individual: { ContactDetails: { ContactName: 'Foo', ContactSurname: 'Bar', Email: 'foo@bar', Telephone: '123', Fax: '345' } } } }),
        assertValue('summary', "Resident.ContactDetailsDescription", "Foo, Bar, email: foo@bar, tel: 123, fax: 345",
          { Resident: { Entity: { ContactDetails: { ContactName: 'Foo', ContactSurname: 'Bar', Email: 'foo@bar', Telephone: '123', Fax: '345' } } } }),


        //NonResident.Description
        assertValue('summary', "NonResident.Description", "Foo, Bar", { ReportingQualifier: 'BOPCUS', NonResident: { Individual: { Name: 'Foo', Surname: "Bar" } } }),
        assertValue('summary', "NonResident.Description", "Foo", { ReportingQualifier: 'BOPCUS', NonResident: { Entity: { EntityName: 'Foo' } } }),
        assertValue('summary', "NonResident.Description", "Foo", { ReportingQualifier: 'BOPCARD RESIDENT', NonResident: { Entity: { CardMerchantName: 'Foo' } } }),
        assertValue('summary', "NonResident.Description", "Foo", { ReportingQualifier: 'BOPCUS', NonResident: { Exception: { ExceptionName: 'Foo' } } }),


        // NonResident.Account
        assertShow('summary', "NonResident.Account", { NonResident: { Individual: {} } }),
        assertHide('summary', "NonResident.Account", { NonResident: { Exception: {} } }),

        //"NonResident.AccountDescription"
        assertValue('summary', "NonResident.AccountDescription", "Foo, [123]", { NonResident: { Individual: { AccountIdentifier: 'Foo', AccountNumber: '123', AccountName: 'Bar' } } }),
        assertValue('summary', "NonResident.AccountDescription", "Foo, [123]", { NonResident: { Entity: { AccountIdentifier: 'Foo', AccountNumber: '123', AccountName: 'Bar' } } }),


        // NonResident.Address
        assertShow('summary', "NonResident.Address", { NonResident: { Individual: {} } }),
        assertHide('summary', "NonResident.Address", { NonResident: { Exception: {} } }),

        // Resident.Account
        assertShow('summary', "Resident.Account", { Resident: { Individual: {} } }),
        assertHide('summary', "Resident.Account", { Resident: { Exception: {} } }),

        // Resident.ContactDetails
        assertShow('summary', "Resident.ContactDetails", { Resident: { Individual: {} } }),
        assertHide('summary', "Resident.ContactDetails", { Resident: { Exception: {} } }),

        //resident_table
        assertShow('summary', "resident_table", {}),
        assertHide('summary', "resident_table", { ReportingQualifier: 'BOPCARD NON RESIDENT' }),
        //non_resident_table
        assertShow('summary', "non_resident_table", {}),
        assertHide('summary', "non_resident_table", { ReportingQualifier: 'BOPCARD NON RESIDENT' }),

        /////////////////////////////////////////
        // MONEY SUMMARY
        /////////////////////////////////////////

        //AddMoneyButton
        assertNoEvent('summary', "money::AddMoneyButton", {}),
        assertHide('summary', "AddMoneyButton", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}] }),

        //Description
        assertValue('summary', "Description", "Loan Ref: 123",
          {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ CategoryCode: '801', LoanRefNumber: 123 }]
          }),
        assertValue('summary', "Description", "Loan Ref: 123, CCN: Foo",
          {
            ReportingQualifier: 'BOPCUS',
            Flow: "OUT",
            Resident: {
              Individual: { CustomsClientNumber: 'Foo' }
            },
            MonetaryAmount: [{ CategoryCode: '106', LoanRefNumber: '123', LoanInterestRate: '0.2' }]
          }),
        assertValue('summary', "Description", "Loan Tenor: Foo, Loan Int: 0.2",
          {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: { CustomsClientNumber: 'Foo' }
            },
            MonetaryAmount: [{ CategoryCode: '810', LoanInterestRate: '0.2', LoanTenor: "Foo" }]
          }),
        assertValue('summary', "Description", "Loan Ref: 123, CCN: 4567, Ruling: test",
          {
            ReportingQualifier: 'BOPCUS',
            Flow: "OUT",
            Resident: {
              Individual: { CustomsClientNumber: '4567' }
            },
            MonetaryAmount: [{
              CategoryCode: '106', LoanRefNumber: '123',
              SARBAuth: {
                RulingsSection: 'test'
              },
              ThirdParty: [{
                CustomsClientNumber: '12345'
              }]
            }]
          }),
        assertValue('summary', "Description", "Reversal: 1234",
          {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ ReversalTrnRefNumber: '1234', CategoryCode: '200' }]
          }),

        //ThirdPartyDescription

        assertValue('summary', "ThirdPartyDescription", "Individual: Foo, Bar",
          {
            MonetaryAmount: [{ ThirdParty: { Individual: { Surname: "Foo", Name: "Bar" } } }]
          }),

        assertValue('summary', "ThirdPartyDescription", "Entity: Bar",
          {
            MonetaryAmount: [{ ThirdParty: { Entity: { Name: "Bar" } } }]
          }),

        //ThirdPartyContact

        assertHide('summary', 'ThirdPartyContact', { MonetaryAmount: [{}] }),
        assertValue('summary', "ThirdPartyContact", "Bar, Foo",
          {
            MonetaryAmount: [{ ThirdParty: { ContactDetails: { ContactSurname: "Bar", ContactName: "Foo" } } }]
          }),
        assertShow('summary', 'ThirdPartyContact', {
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: "Foo" } } }]
        }),
        assertShow('summary', 'ThirdPartyContact', {
          MonetaryAmount: [{ ThirdParty: { Entity: { Name: "Bar" } } }]
        }),
        assertShow('summary', 'ThirdPartyContact', {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          Resident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '512' }]
        }),
        assertShow('summary', 'ThirdPartyContact', {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '256' }]
        }),
        assertShow('summary', 'ThirdPartyContact', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { SupplementaryCardIndicator: 'Y' } },
          MonetaryAmount: [{}]
        }),

        //ImportExport
        assertHide('summary', 'ImportExport', { MonetaryAmount: [{}] }),
        assertValue('summary', 'ImportExport', 'ICN: 1234', {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', ImportExport: [{ ImportControlNumber: '1234' }] }]
        }),
        assertValue('summary', 'ImportExport', 'UCR: 1234', {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '101', ImportExport: [{ UCR: '1234' }] }]
        }),
        assertValue('summary', 'ImportExport', 'MRN: 1234, Transport DN: 4321', {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '103', ImportExport: [{ ImportControlNumber: '1234', TransportDocumentNumber: '4321' }] }]
        }),



        /////////////////////////////////////////
        // IMPORT EXPORT SUMMARY
        /////////////////////////////////////////
        assertValue('summary', 'Description', 'MRN: 1234, Transport DN: 4321', {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '103', ImportExport: [{ ImportControlNumber: '1234', TransportDocumentNumber: '4321' }] }]
        }),
        assertValue('summary', 'Description', 'ICN: 1234', {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', ImportExport: [{ ImportControlNumber: '1234' }] }]
        }),
        assertValue('summary', 'Description', 'UCR: 1234', {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '101', ImportExport: [{ UCR: '1234' }] }]
        }),


        /////////////////////////////////////////
        // DETAIL
        /////////////////////////////////////////
        /////////////////////////////////////////
        // TRANSACTION DETAIL
        /////////////////////////////////////////
        //TrnReference
        assertNoEvent('detail', "TrnReference", {}),

        //ReportingQualifier -  ideally this field should not be in the partials...
        //assertHide('detail', "ReportingQualifier", {}),

        //Flow
        assertDisabled('detail', "Flow", {}),

        //ValueDate
        assertShow('detail', "ValueDate", {}),
        assertDisabled('detail', "ValueDate", {}),

        //FlowCurrency
        assertShow('detail', "FlowCurrency", {}),
        assertDisabled('detail', "FlowCurrency", {}),

        //TotalForeignValue
        assertNoEvent('detail', "TotalForeignValue", {}),

        //BranchCode
        assertHide('detail', "BranchCode", {}),

        //BranchName
        assertHide('detail', "BranchName", {}),

        //HubCode
        assertHide('detail', "HubCode", {}),

        //HubName
        assertHide('detail', "HubName", {}),

        //OriginatingBank
        assertNoEvent('detail', "OriginatingBank", {}),
        assertShow('detail', "OriginatingBank", { ReportingQualifier: 'BOPCUS', Flow: 'OUT' }),
        assertShow('detail', "OriginatingBank", { ReportingQualifier: 'BOPCUS', Flow: 'IN' }),
        assertNoEvent('detail', "OriginatingBank", { Flow: 'IN' }),
        assertShow('detail', "OriginatingBank", { ReportingQualifier: 'BOPCUS', Flow: 'IN', NonResident: { Entity: { AccountIdentifier: 'VOSTRO' } } }),
        assertShow('detail', "OriginatingBank", { ReportingQualifier: 'BOPCUS', Flow: 'IN', NonResident: { Entity: { AccountIdentifier: 'CASH' } } }),
        assertShow('detail', "OriginatingBank", { ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingCountry: 'ZA' }),
        assertNoEvent('detail', "OriginatingBank", { ReportingQualifier: 'BOPCARD NON RESIDENT', Flow: 'IN' }),
        //assertNoEvent('detail', "OriginatingBank", { ReportingQualifier: 'BOPCARD NON RESIDENT', }),

        //OriginatingCountry
        assertNoEvent('detail', "OriginatingCountry", {}),
        assertShow('detail', "OriginatingCountry", { ReportingQualifier: 'BOPCUS', Flow: 'OUT' }),
        assertShow('detail', "OriginatingCountry", { ReportingQualifier: 'BOPCUS', Flow: 'IN', NonResident: { Entity: { AccountIdentifier: 'VOSTRO' } } }),
        assertShow('detail', "OriginatingCountry", { ReportingQualifier: 'BOPCUS', Flow: 'IN', NonResident: { Entity: { AccountIdentifier: 'CASH' } } }),
        assertValue('detail', "OriginatingCountry", "ZA", { ReportingQualifier: 'BOPCUS' }),
        assertShow('detail', "OriginatingCountry", { ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingBank: "BANK0" }),

        //CorrespondentBank
        assertShow('detail', "CorrespondentBank", { ReportingQualifier: 'BOPCUS' }),
        assertShow('detail', "CorrespondentBank", { ReportingQualifier: 'NON RESIDENT RAND' }),
        assertShow('detail', "CorrespondentBank", { ReportingQualifier: 'NON REPORTABLE' }),
        assertShow('detail', "CorrespondentBank", { ReportingQualifier: 'INTERBANK' }),
        assertShow('detail', "CorrespondentBank", { ReportingQualifier: 'BOPDIR' }),
        assertHide('detail', "CorrespondentBank", { ReportingQualifier: 'BOPCARD RESIDENT' }),//E
        assertHide('detail', "CorrespondentBank", { ReportingQualifier: 'BOPCARD NON RESIDENT' }),//F

        //CorrespondentCountry
        assertShow('detail', "CorrespondentCountry", { ReportingQualifier: 'BOPCUS' }),
        assertShow('detail', "CorrespondentCountry", { ReportingQualifier: 'NON RESIDENT RAND' }),
        assertShow('detail', "CorrespondentCountry", { ReportingQualifier: 'NON REPORTABLE' }),
        assertShow('detail', "CorrespondentCountry", { ReportingQualifier: 'INTERBANK' }),
        assertShow('detail', "CorrespondentCountry", { ReportingQualifier: 'BOPDIR' }),
        assertHide('detail', "CorrespondentCountry", { ReportingQualifier: 'BOPCARD RESIDENT' }),//E
        assertHide('detail', "CorrespondentCountry", { ReportingQualifier: 'BOPCARD NON RESIDENT' }),//F

        //ReceivingBank
        //hide(),
        assertHide('detail', "ReceivingBank", {}),
        //show().onInflow().onSection("ABCDG"),
        assertShow('detail', "ReceivingBank", { ReportingQualifier: 'BOPCUS', Flow: 'IN' }),
        assertShow('detail', "ReceivingBank", { ReportingQualifier: 'NON RESIDENT RAND', Flow: 'IN' }),
        assertShow('detail', "ReceivingBank", { ReportingQualifier: 'NON REPORTABLE', Flow: 'IN' }),
        assertShow('detail', "ReceivingBank", { ReportingQualifier: 'INTERBANK', Flow: 'IN' }),
        assertShow('detail', "ReceivingBank", { ReportingQualifier: 'BOPDIR', Flow: 'IN' }),
        assertHide('detail', "ReceivingBank", { ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'IN' }),
        assertHide('detail', "ReceivingBank", { ReportingQualifier: 'BOPCARD NON RESIDENT', Flow: 'IN' }),
        //show(notResidentFieldValue("AccountIdentifier", "CASH")).onOutflow().onSection("ABCDG"),
        assertHide('detail', "ReceivingBank", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { AccountIdentifier: 'CASH' } } }),
        assertHide('detail', "ReceivingBank", { ReportingQualifier: 'BOPCARD NON RESIDENT', Flow: 'OUT', Resident: { Entity: { AccountIdentifier: 'CASH' } } }),
        assertShow('detail', "ReceivingBank", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { AccountIdentifier: 'VOSTRO' } } }),
        assertHide('detail', "ReceivingBank", { ReportingQualifier: 'BOPCARD NON RESIDENT', Flow: 'OUT', Resident: { Entity: { AccountIdentifier: 'VOSTRO' } } }),
        //hide(hasResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["250", "251"]),
        assertHide('detail', "ReceivingBank", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [
            {
              CategoryCode: "250"
            }]
        }),
        assertHide('detail', "ReceivingBank", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [
            {
              CategoryCode: "251"
            }]
        }),
        assertShow('detail', "ReceivingBank", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: "250"
            }]
        }),
        assertShow('detail', "ReceivingBank", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [
            {
              CategoryCode: "103"
            }]
        }),
        //hide(hasNonResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["255", "256"]),
        assertHide('detail', "ReceivingBank", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [
            {
              CategoryCode: "255"
            }]
        }),
        assertHide('detail', "ReceivingBank", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [
            {
              CategoryCode: "256"
            }]
        }),
        assertShow('detail', "ReceivingBank", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: "255"
            }]
        }),
        assertShow('detail', "ReceivingBank", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [
            {
              CategoryCode: "103"
            }]
        }),
        //show(hasTransactionField("ReceivingCountry")).onOutflow().onSection("ABCDG")
        assertShow('detail', "ReceivingBank", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', ReceivingCountry: "ZA" }),
        assertShow('detail', "ReceivingBank", { ReportingQualifier: 'NON RESIDENT RAND', Flow: 'OUT', ReceivingCountry: "ZA" }),
        assertShow('detail', "ReceivingBank", { ReportingQualifier: 'NON REPORTABLE', Flow: 'OUT', ReceivingCountry: "ZA" }),
        assertShow('detail', "ReceivingBank", { ReportingQualifier: 'INTERBANK', Flow: 'OUT', ReceivingCountry: "ZA" }),
        assertShow('detail', "ReceivingBank", { ReportingQualifier: 'BOPDIR', Flow: 'OUT', ReceivingCountry: "ZA" }),

        /*
         These will fail because it satisfies
         show(notResidentFieldValue("AccountIdentifier", "CASH")).onOutflow().onSection("ABCDG"),

         assertHide('detail', "ReceivingBank", { ReportingQualifier: 'BOPCUS', Flow: 'OUT' }),
         assertHide('detail', "ReceivingBank", { ReportingQualifier: 'NON RESIDENT RAND', Flow: 'OUT' }),
         assertHide('detail', "ReceivingBank", { ReportingQualifier: 'NON REPORTABLE', Flow: 'OUT' }),
         assertHide('detail', "ReceivingBank", { ReportingQualifier: 'INTERBANK', Flow: 'OUT' }),
         assertHide('detail', "ReceivingBank", { ReportingQualifier: 'BOPDIR', Flow: 'OUT' }),
         */

        //ReceivingCountry
        //hide(),
        assertHide('detail', "ReceivingCountry", {}),
        //show().onInflow().onSection("ABCDG"),
        assertShow('detail', "ReceivingCountry", { ReportingQualifier: 'BOPCUS', Flow: 'IN' }),
        assertShow('detail', "ReceivingCountry", { ReportingQualifier: 'NON RESIDENT RAND', Flow: 'IN' }),
        assertShow('detail', "ReceivingCountry", { ReportingQualifier: 'NON REPORTABLE', Flow: 'IN' }),
        assertShow('detail', "ReceivingCountry", { ReportingQualifier: 'INTERBANK', Flow: 'IN' }),
        assertShow('detail', "ReceivingCountry", { ReportingQualifier: 'BOPDIR', Flow: 'IN' }),
        assertHide('detail', "ReceivingCountry", { ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'IN' }),
        assertHide('detail', "ReceivingCountry", { ReportingQualifier: 'BOPCARD NON RESIDENT', Flow: 'IN' }),
        //show(notResidentFieldValue("AccountIdentifier", "CASH")).onOutflow().onSection("ABCDG"),
        assertHide('detail', "ReceivingCountry", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { AccountIdentifier: 'CASH' } } }),
        assertHide('detail', "ReceivingCountry", { ReportingQualifier: 'BOPCARD NON RESIDENT', Flow: 'OUT', Resident: { Entity: { AccountIdentifier: 'CASH' } } }),
        assertShow('detail', "ReceivingCountry", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { AccountIdentifier: 'VOSTRO' } } }),
        assertHide('detail', "ReceivingCountry", { ReportingQualifier: 'BOPCARD NON RESIDENT', Flow: 'OUT', Resident: { Entity: { AccountIdentifier: 'VOSTRO' } } }),
        //hide(hasResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["250", "251"]),
        assertHide('detail', "ReceivingCountry", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [
            {
              CategoryCode: "250"
            }]
        }),
        assertHide('detail', "ReceivingCountry", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [
            {
              CategoryCode: "251"
            }]
        }),
        assertShow('detail', "ReceivingCountry", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: "250"
            }]
        }),
        assertShow('detail', "ReceivingCountry", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [
            {
              CategoryCode: "103"
            }]
        }),
        //hide(hasNonResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["255", "256"]),
        assertHide('detail', "ReceivingCountry", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [
            {
              CategoryCode: "255"
            }]
        }),
        assertHide('detail', "ReceivingCountry", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [
            {
              CategoryCode: "256"
            }]
        }),
        assertShow('detail', "ReceivingCountry", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: "255"
            }]
        }),
        assertShow('detail', "ReceivingCountry", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [
            {
              CategoryCode: "103"
            }]
        }),
        //show(hasTransactionField("ReceivingBank")).onOutflow().onSection("ABCDG")
        assertShow('detail', "ReceivingCountry", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', ReceivingBank: "BANK0" }),
        assertShow('detail', "ReceivingCountry", { ReportingQualifier: 'NON RESIDENT RAND', Flow: 'OUT', ReceivingBank: "BANK0" }),
        assertShow('detail', "ReceivingCountry", { ReportingQualifier: 'NON REPORTABLE', Flow: 'OUT', ReceivingBank: "BANK0" }),
        assertShow('detail', "ReceivingCountry", { ReportingQualifier: 'INTERBANK', Flow: 'OUT', ReceivingBank: "BANK0" }),
        assertShow('detail', "ReceivingCountry", { ReportingQualifier: 'BOPDIR', Flow: 'OUT', ReceivingBank: "BANK0" }),

        /*
         these will fail because they are satisfied by
         show(notResidentFieldValue("AccountIdentifier", "CASH")).onOutflow().onSection("ABCDG"),
         assertHide('detail', "ReceivingCountry", { ReportingQualifier: 'BOPCUS', Flow: 'OUT' }),
         assertHide('detail', "ReceivingCountry", { ReportingQualifier: 'NON RESIDENT RAND', Flow: 'OUT' }),
         assertHide('detail', "ReceivingCountry", { ReportingQualifier: 'NON REPORTABLE', Flow: 'OUT' }),
         assertHide('detail', "ReceivingCountry", { ReportingQualifier: 'INTERBANK', Flow: 'OUT' }),
         assertHide('detail', "ReceivingCountry", { ReportingQualifier: 'BOPDIR', Flow: 'OUT' }),
         */

        //NonResident.Individual.Surname
        assertShow('detail', "NonResident.Individual.Surname", { ReportingQualifier: 'BOPCUS' }),
        assertShow('detail', "NonResident.Individual.Surname", { ReportingQualifier: 'NON RESIDENT RAND' }),
        assertShow('detail', "NonResident.Individual.Surname", { ReportingQualifier: 'NON REPORTABLE' }),
        assertShow('detail', "NonResident.Individual.Surname", { ReportingQualifier: 'INTERBANK' }),
        assertShow('detail', "NonResident.Individual.Surname", { ReportingQualifier: 'BOPDIR' }),
        assertNoEvent('detail', "NonResident.Individual.Surname", { ReportingQualifier: 'BOPCARD RESIDENT' }),
        assertNoEvent('detail', "NonResident.Individual.Surname", { ReportingQualifier: 'BOPCARD NON RESIDENT' }),

        //NonResident.Individual.Name
        assertShow('detail', "NonResident.Individual.Name", { ReportingQualifier: 'BOPCUS' }),
        assertShow('detail', "NonResident.Individual.Name", { ReportingQualifier: 'NON RESIDENT RAND' }),
        assertShow('detail', "NonResident.Individual.Name", { ReportingQualifier: 'NON REPORTABLE' }),
        assertShow('detail', "NonResident.Individual.Name", { ReportingQualifier: 'INTERBANK' }),
        assertShow('detail', "NonResident.Individual.Name", { ReportingQualifier: 'BOPDIR' }),
        assertNoEvent('detail', "NonResident.Individual.Name", { ReportingQualifier: 'BOPCARD RESIDENT' }),
        assertNoEvent('detail', "NonResident.Individual.Name", { ReportingQualifier: 'BOPCARD NON RESIDENT' }),

        //NonResident.Individual.Gender
        assertShow('detail', "NonResident.Individual.Gender", { ReportingQualifier: 'BOPCUS' }),
        assertShow('detail', "NonResident.Individual.Gender", { ReportingQualifier: 'NON RESIDENT RAND' }),
        assertShow('detail', "NonResident.Individual.Gender", { ReportingQualifier: 'NON REPORTABLE' }),
        assertShow('detail', "NonResident.Individual.Gender", { ReportingQualifier: 'INTERBANK' }),
        assertShow('detail', "NonResident.Individual.Gender", { ReportingQualifier: 'BOPDIR' }),
        assertNoEvent('detail', "NonResident.Individual.Gender", { ReportingQualifier: 'BOPCARD RESIDENT' }),
        assertNoEvent('detail', "NonResident.Individual.Gender", { ReportingQualifier: 'BOPCARD NON RESIDENT' }),

        //NonResident.Individual.PassportNumber
        assertShow('detail', "NonResident.Individual.PassportNumber", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: "250" }] }),
        assertShow('detail', "NonResident.Individual.PassportNumber", { ReportingQualifier: 'NON RESIDENT RAND', MonetaryAmount: [{ CategoryCode: "250" }] }),
        assertNoEvent('detail', "NonResident.Individual.PassportNumber", { ReportingQualifier: 'BOPCUS' }),
        assertNoEvent('detail', "NonResident.Individual.PassportNumber", { ReportingQualifier: 'NON RESIDENT RAND' }),
        assertNoEvent('detail', "NonResident.Individual.PassportNumber", { ReportingQualifier: 'NON REPORTABLE' }),
        assertNoEvent('detail', "NonResident.Individual.PassportNumber", { ReportingQualifier: 'INTERBANK' }),
        assertNoEvent('detail', "NonResident.Individual.PassportNumber", { ReportingQualifier: 'BOPDIR' }),
        assertNoEvent('detail', "NonResident.Individual.PassportNumber", { ReportingQualifier: 'BOPCARD RESIDENT' }),
        assertNoEvent('detail', "NonResident.Individual.PassportNumber", { ReportingQualifier: 'BOPCARD NON RESIDENT' }),

        //NonResident.Individual.PassportCountry
        assertShow('detail', "NonResident.Individual.PassportCountry", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: "250" }] }),
        assertShow('detail', "NonResident.Individual.PassportCountry", { ReportingQualifier: 'NON RESIDENT RAND', MonetaryAmount: [{ CategoryCode: "250" }] }),
        assertNoEvent('detail', "NonResident.Individual.PassportCountry", { ReportingQualifier: 'BOPCUS' }),
        assertNoEvent('detail', "NonResident.Individual.PassportCountry", { ReportingQualifier: 'NON RESIDENT RAND' }),
        assertNoEvent('detail', "NonResident.Individual.PassportCountry", { ReportingQualifier: 'NON REPORTABLE' }),
        assertNoEvent('detail', "NonResident.Individual.PassportCountry", { ReportingQualifier: 'INTERBANK' }),
        assertNoEvent('detail', "NonResident.Individual.PassportCountry", { ReportingQualifier: 'BOPDIR' }),
        assertNoEvent('detail', "NonResident.Individual.PassportCountry", { ReportingQualifier: 'BOPCARD RESIDENT' }),
        assertNoEvent('detail', "NonResident.Individual.PassportCountry", { ReportingQualifier: 'BOPCARD NON RESIDENT' }),

        //NonResident.Entity.EntityName
        assertShow('detail', "NonResident.Entity.EntityName", { ReportingQualifier: 'BOPCUS' }),
        assertShow('detail', "NonResident.Entity.EntityName", { ReportingQualifier: 'NON RESIDENT RAND' }),
        assertShow('detail', "NonResident.Entity.EntityName", { ReportingQualifier: 'NON REPORTABLE' }),
        assertShow('detail', "NonResident.Entity.EntityName", { ReportingQualifier: 'INTERBANK' }),
        assertShow('detail', "NonResident.Entity.EntityName", { ReportingQualifier: 'BOPDIR' }),
        assertHide('detail', "NonResident.Entity.EntityName", { ReportingQualifier: 'BOPCARD RESIDENT' }),
        assertNoEvent('detail', "NonResident.Entity.EntityName", { ReportingQualifier: 'BOPCARD NON RESIDENT' }),

        //NonResident.Entity.CardMerchantName
        assertHide('detail', "NonResident.Entity.CardMerchantName", { ReportingQualifier: 'BOPCUS' }),
        assertHide('detail', "NonResident.Entity.CardMerchantName", { ReportingQualifier: 'NON RESIDENT RAND' }),
        assertHide('detail', "NonResident.Entity.CardMerchantName", { ReportingQualifier: 'NON REPORTABLE' }),
        assertHide('detail', "NonResident.Entity.CardMerchantName", { ReportingQualifier: 'INTERBANK' }),
        assertHide('detail', "NonResident.Entity.CardMerchantName", { ReportingQualifier: 'BOPDIR' }),
        assertShow('detail', "NonResident.Entity.CardMerchantName", { ReportingQualifier: 'BOPCARD RESIDENT' }),
        assertNoEvent('detail', "NonResident.Entity.CardMerchantName", { ReportingQualifier: 'BOPCARD NON RESIDENT' }),

        //NonResident.Entity.CardMerchantCode
        assertHide('detail', "NonResident.Entity.CardMerchantCode", { ReportingQualifier: 'BOPCUS' }),
        assertHide('detail', "NonResident.Entity.CardMerchantCode", { ReportingQualifier: 'NON RESIDENT RAND' }),
        assertHide('detail', "NonResident.Entity.CardMerchantCode", { ReportingQualifier: 'NON REPORTABLE' }),
        assertHide('detail', "NonResident.Entity.CardMerchantCode", { ReportingQualifier: 'INTERBANK' }),
        assertHide('detail', "NonResident.Entity.CardMerchantCode", { ReportingQualifier: 'BOPDIR' }),
        assertShow('detail', "NonResident.Entity.CardMerchantCode", { ReportingQualifier: 'BOPCARD RESIDENT' }),
        assertNoEvent('detail', "NonResident.Entity.CardMerchantCode", { ReportingQualifier: 'BOPCARD NON RESIDENT' }),

        //NonResident.Exception
        //hide(hasTransactionField("Resident.Exception")).onSection("A"),
        //hide().onSection("BEG")
        assertNoEvent('detail', "NonResident.Exception", {}),
        assertHide('detail', "NonResident.Exception", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Exception: {} }
        }),
        assertNoEvent('detail', "NonResident.Exception", { ReportingQualifier: 'BOPCUS' }),//A
        assertNoEvent('detail', "NonResident.Exception", {
          ReportingQualifier: 'NON REPORTABLE',
          Resident: { Exception: {} }
        }),//C
        assertNoEvent('detail', "NonResident.Exception", {
          ReportingQualifier: 'INTERBANK',
          Resident: { Exception: {} }
        }),//D
        assertNoEvent('detail', "NonResident.Exception", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          Resident: { Exception: {} }
        }),//F
        assertHide('detail', "NonResident.Exception", { ReportingQualifier: 'NON RESIDENT RAND' }),//B
        assertHide('detail', "NonResident.Exception", { ReportingQualifier: 'BOPCARD RESIDENT' }),//E
        assertHide('detail', "NonResident.Exception", { ReportingQualifier: 'BOPDIR' }),//G

        //NonResident.Exception.ExceptionName
        //hide(hasTransactionField("Resident.Exception")).onSection("A"),
        //hide().onSection("BEG")
        assertNoEvent('detail', "NonResident.Exception.ExceptionName", {}),
        assertHide('detail', "NonResident.Exception", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Exception: {} }
        }),
        assertNoEvent('detail', "NonResident.Exception.ExceptionName", { ReportingQualifier: 'BOPCUS' }),//A
        assertNoEvent('detail', "NonResident.Exception.ExceptionName", {
          ReportingQualifier: 'NON REPORTABLE',
          Resident: { Exception: {} }
        }),//C
        assertNoEvent('detail', "NonResident.Exception.ExceptionName", {
          ReportingQualifier: 'INTERBANK',
          Resident: { Exception: {} }
        }),//D
        assertNoEvent('detail', "NonResident.Exception.ExceptionName", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          Resident: { Exception: {} }
        }),//F
        assertHide('detail', "NonResident.Exception.ExceptionName", { ReportingQualifier: 'NON RESIDENT RAND' }),//B
        assertHide('detail', "NonResident.Exception.ExceptionName", { ReportingQualifier: 'BOPCARD RESIDENT' }),//E
        assertHide('detail', "NonResident.Exception.ExceptionName", { ReportingQualifier: 'BOPDIR' }),//G
        //setValue('MUTUAL PARTY', null, isEmpty).onSection('A').onInflow().onCategory("252")
        assertValue('detail', "NonResident.Exception.ExceptionName", "MUTUAL PARTY", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "252" }]
        }),

        //NonResident.Individual.AccountIdentifier
        assertValue('detail', "NonResident.Individual.AccountIdentifier", "NON RESIDENT RAND", {
          ReportingQualifier: 'NON RESIDENT RAND',
          Flow: 'IN',
        }),

        //NonResident.Individual.AccountNumber
        assertShow('detail', 'NonResident.Individual.AccountNumber', {}),

        //NonResident.Entity.AccountNumber
        assertShow('detail', 'NonResident.Entity.AccountNumber', {}),


        //NonResident.Entity.Address.AddressLine1
        assertNoEvent('detail', 'NonResident.Entity.Address.AddressLine1', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'NonResident.Entity.Address.AddressLine1', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'NonResident.Entity.Address.AddressLine1', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'NonResident.Entity.Address.AddressLine1', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'NonResident.Entity.Address.AddressLine1', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'NonResident.Entity.Address.AddressLine1', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'NonResident.Entity.Address.AddressLine1', { ReportingQualifier: "BOPDIR" }), //"G";

        //NonResident.Entity.Address.AddressLine2
        assertNoEvent('detail', 'NonResident.Entity.Address.AddressLine2', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'NonResident.Entity.Address.AddressLine2', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'NonResident.Entity.Address.AddressLine2', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'NonResident.Entity.Address.AddressLine2', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'NonResident.Entity.Address.AddressLine2', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'NonResident.Entity.Address.AddressLine2', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'NonResident.Entity.Address.AddressLine2', { ReportingQualifier: "BOPDIR" }), //"G";

        //NonResident.Entity.Address.Suburb
        assertNoEvent('detail', 'NonResident.Entity.Address.Suburb', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'NonResident.Entity.Address.Suburb', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'NonResident.Entity.Address.Suburb', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'NonResident.Entity.Address.Suburb', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'NonResident.Entity.Address.Suburb', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'NonResident.Entity.Address.Suburb', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'NonResident.Entity.Address.Suburb', { ReportingQualifier: "BOPDIR" }), //"G";

        //NonResident.Entity.Address.City
        assertNoEvent('detail', 'NonResident.Entity.Address.City', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'NonResident.Entity.Address.City', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'NonResident.Entity.Address.City', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'NonResident.Entity.Address.City', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'NonResident.Entity.Address.City', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'NonResident.Entity.Address.City', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'NonResident.Entity.Address.City', { ReportingQualifier: "BOPDIR" }), //"G";

        //NonResident.Entity.Address.State
        assertNoEvent('detail', 'NonResident.Entity.Address.State', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'NonResident.Entity.Address.State', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'NonResident.Entity.Address.State', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'NonResident.Entity.Address.State', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'NonResident.Entity.Address.State', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'NonResident.Entity.Address.State', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'NonResident.Entity.Address.State', { ReportingQualifier: "BOPDIR" }), //"G";

        //NonResident.Entity.Address.PostalCode
        assertNoEvent('detail', 'NonResident.Entity.Address.PostalCode', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'NonResident.Entity.Address.PostalCode', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'NonResident.Entity.Address.PostalCode', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'NonResident.Entity.Address.PostalCode', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'NonResident.Entity.Address.PostalCode', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'NonResident.Entity.Address.PostalCode', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'NonResident.Entity.Address.PostalCode', { ReportingQualifier: "BOPDIR" }), //"G";


        //NonResident.Individual.Address.AddressLine1
        assertNoEvent('detail', 'NonResident.Individual.Address.AddressLine1', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'NonResident.Individual.Address.AddressLine1', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'NonResident.Individual.Address.AddressLine1', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'NonResident.Individual.Address.AddressLine1', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'NonResident.Individual.Address.AddressLine1', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'NonResident.Individual.Address.AddressLine1', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'NonResident.Individual.Address.AddressLine1', { ReportingQualifier: "BOPDIR" }), //"G";

        //NonResident.Individual.Address.AddressLine2
        assertNoEvent('detail', 'NonResident.Individual.Address.AddressLine2', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'NonResident.Individual.Address.AddressLine2', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'NonResident.Individual.Address.AddressLine2', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'NonResident.Individual.Address.AddressLine2', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'NonResident.Individual.Address.AddressLine2', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'NonResident.Individual.Address.AddressLine2', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'NonResident.Individual.Address.AddressLine2', { ReportingQualifier: "BOPDIR" }), //"G";

        //NonResident.Individual.Address.Suburb
        assertNoEvent('detail', 'NonResident.Individual.Address.Suburb', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'NonResident.Individual.Address.Suburb', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'NonResident.Individual.Address.Suburb', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'NonResident.Individual.Address.Suburb', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'NonResident.Individual.Address.Suburb', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'NonResident.Individual.Address.Suburb', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'NonResident.Individual.Address.Suburb', { ReportingQualifier: "BOPDIR" }), //"G";

        //NonResident.Individual.Address.City
        assertNoEvent('detail', 'NonResident.Individual.Address.City', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'NonResident.Individual.Address.City', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'NonResident.Individual.Address.City', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'NonResident.Individual.Address.City', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'NonResident.Individual.Address.City', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'NonResident.Individual.Address.City', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'NonResident.Individual.Address.City', { ReportingQualifier: "BOPDIR" }), //"G";

        //NonResident.Individual.Address.State
        assertNoEvent('detail', 'NonResident.Individual.Address.State', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'NonResident.Individual.Address.State', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'NonResident.Individual.Address.State', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'NonResident.Individual.Address.State', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'NonResident.Individual.Address.State', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'NonResident.Individual.Address.State', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'NonResident.Individual.Address.State', { ReportingQualifier: "BOPDIR" }), //"G";

        //NonResident.Individual.Address.PostalCode
        assertNoEvent('detail', 'NonResident.Individual.Address.PostalCode', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'NonResident.Individual.Address.PostalCode', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'NonResident.Individual.Address.PostalCode', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'NonResident.Individual.Address.PostalCode', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'NonResident.Individual.Address.PostalCode', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'NonResident.Individual.Address.PostalCode', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'NonResident.Individual.Address.PostalCode', { ReportingQualifier: "BOPDIR" }), //"G";


        //NonResident.Individual.Address.Country
        assertShow('detail', 'NonResident.Individual.Address.Country', { ReportingQualifier: "BOPCUS" }), //A
        assertShow('detail', 'NonResident.Individual.Address.Country', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertShow('detail', 'NonResident.Individual.Address.Country', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertShow('detail', 'NonResident.Individual.Address.Country', { ReportingQualifier: "INTERBANK" }), //"D";
        assertShow('detail', 'NonResident.Individual.Address.Country', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'NonResident.Individual.Address.Country', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertShow('detail', 'NonResident.Individual.Address.Country', { ReportingQualifier: "BOPDIR" }), //"G";

        //NonResident.Entity.Address.Country
        assertShow('detail', 'NonResident.Entity.Address.Country', { ReportingQualifier: "BOPCUS" }), //A
        assertShow('detail', 'NonResident.Entity.Address.Country', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertShow('detail', 'NonResident.Entity.Address.Country', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertShow('detail', 'NonResident.Entity.Address.Country', { ReportingQualifier: "INTERBANK" }), //"D";
        assertShow('detail', 'NonResident.Entity.Address.Country', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'NonResident.Entity.Address.Country', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertShow('detail', 'NonResident.Entity.Address.Country', { ReportingQualifier: "BOPDIR" }), //"G";

        //Resident.Individual.Surname
        assertShow('detail', 'Resident.Individual.Surname', { ReportingQualifier: "BOPCUS" }), //A
        assertShow('detail', 'Resident.Individual.Surname', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertShow('detail', 'Resident.Individual.Surname', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Individual.Surname', { ReportingQualifier: "INTERBANK" }), //"D";
        assertShow('detail', 'Resident.Individual.Surname', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'Resident.Individual.Surname', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertShow('detail', 'Resident.Individual.Surname', { ReportingQualifier: "BOPDIR" }), //"G";

        //Resident.Individual.Name
        assertShow('detail', 'Resident.Individual.Name', { ReportingQualifier: "BOPCUS" }), //A
        assertShow('detail', 'Resident.Individual.Name', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertShow('detail', 'Resident.Individual.Name', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Individual.Name', { ReportingQualifier: "INTERBANK" }), //"D";
        assertShow('detail', 'Resident.Individual.Name', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'Resident.Individual.Name', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertShow('detail', 'Resident.Individual.Name', { ReportingQualifier: "BOPDIR" }), //"G";

        //Resident.Individual.Gender
        assertShow('detail', 'Resident.Individual.Gender', { ReportingQualifier: "BOPCUS" }), //A
        assertShow('detail', 'Resident.Individual.Gender', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Individual.Gender', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Individual.Gender', { ReportingQualifier: "INTERBANK" }), //"D";
        assertShow('detail', 'Resident.Individual.Gender', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'Resident.Individual.Gender', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertShow('detail', 'Resident.Individual.Gender', { ReportingQualifier: "BOPDIR" }), //"G";

        //Resident.Individual.DateOfBirth
        assertShow('detail', 'Resident.Individual.DateOfBirth', { ReportingQualifier: "BOPCUS" }), //A
        assertShow('detail', 'Resident.Individual.DateOfBirth', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Individual.DateOfBirth', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Individual.DateOfBirth', { ReportingQualifier: "INTERBANK" }), //"D";
        assertShow('detail', 'Resident.Individual.DateOfBirth', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'Resident.Individual.DateOfBirth', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertShow('detail', 'Resident.Individual.DateOfBirth', { ReportingQualifier: "BOPDIR" }), //"G";

        //Resident.Individual.IDNumber
        assertShow('detail', 'Resident.Individual.IDNumber', { ReportingQualifier: "BOPCUS" }), //A
        assertShow('detail', 'Resident.Individual.IDNumber', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Individual.IDNumber', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Individual.IDNumber', { ReportingQualifier: "INTERBANK" }), //"D";
        assertShow('detail', 'Resident.Individual.IDNumber', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'Resident.Individual.IDNumber', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertShow('detail', 'Resident.Individual.IDNumber', { ReportingQualifier: "BOPDIR" }), //"G";

        //Resident.Individual.BeneficiaryID1
        assertHide('detail', 'Resident.Individual.BeneficiaryID1', {}),
        //Resident.Individual.BeneficiaryID2
        assertHide('detail', 'Resident.Individual.BeneficiaryID2', {}),
        //Resident.Individual.BeneficiaryID3
        assertHide('detail', 'Resident.Individual.BeneficiaryID3', {}),
        //Resident.Individual.BeneficiaryID4
        assertHide('detail', 'Resident.Individual.BeneficiaryID4', {}),

        //Resident.Individual.TempResPermitNumber
        assertShow('detail', 'Resident.Individual.TempResPermitNumber', { ReportingQualifier: "BOPCUS" }), //A
        assertShow('detail', 'Resident.Individual.TempResPermitNumber', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Individual.TempResPermitNumber', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Individual.TempResPermitNumber', { ReportingQualifier: "INTERBANK" }), //"D";
        assertShow('detail', 'Resident.Individual.TempResPermitNumber', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'Resident.Individual.TempResPermitNumber', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertShow('detail', 'Resident.Individual.TempResPermitNumber', { ReportingQualifier: "BOPDIR" }), //"G";
        //hide().onSection("AB").onCategory(["511", "512", "513", "514", "515"]),
        assertHide('detail', 'Resident.Individual.TempResPermitNumber', {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ CategoryCode: "511" }]
        }), //A
        assertHide('detail', 'Resident.Individual.TempResPermitNumber', {
          ReportingQualifier: "NON RESIDENT RAND",
          MonetaryAmount: [{ CategoryCode: "511" }]
        }), //"B"
        assertShow('detail', 'Resident.Individual.TempResPermitNumber', {
          ReportingQualifier: "BOPCUS",
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "401" }]
        }), //A
        assertShow('detail', 'Resident.Individual.TempResPermitNumber', {
          ReportingQualifier: "NON RESIDENT RAND",
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "401" }]
        }), //"B"
        //hide().onOutflow().onSection("AB").onCategory("401")
        assertHide('detail', 'Resident.Individual.TempResPermitNumber', {
          ReportingQualifier: "BOPCUS",
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: "401" }]
        }), //A
        assertHide('detail', 'Resident.Individual.TempResPermitNumber', {
          ReportingQualifier: "NON RESIDENT RAND",
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: "401" }]
        }), //"B"

        //Resident.Individual.ForeignIDNumber
        assertShow('detail', 'Resident.Individual.ForeignIDNumber', { ReportingQualifier: "BOPCUS" }), //A
        assertShow('detail', 'Resident.Individual.ForeignIDNumber', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Individual.ForeignIDNumber', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Individual.ForeignIDNumber', { ReportingQualifier: "INTERBANK" }), //"D";
        assertShow('detail', 'Resident.Individual.ForeignIDNumber', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'Resident.Individual.ForeignIDNumber', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertShow('detail', 'Resident.Individual.ForeignIDNumber', { ReportingQualifier: "BOPDIR" }), //"G";
        //hide().onSection("AB").onCategory(["511", "512", "513", "514", "515"]),
        assertHide('detail', 'Resident.Individual.ForeignIDNumber', {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ CategoryCode: "511" }]
        }), //A
        assertHide('detail', 'Resident.Individual.ForeignIDNumber', {
          ReportingQualifier: "NON RESIDENT RAND",
          MonetaryAmount: [{ CategoryCode: "511" }]
        }), //"B"
        assertShow('detail', 'Resident.Individual.ForeignIDNumber', {
          ReportingQualifier: "BOPCUS",
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "401" }]
        }), //A
        assertShow('detail', 'Resident.Individual.ForeignIDNumber', {
          ReportingQualifier: "NON RESIDENT RAND",
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "401" }]
        }), //"B"
        //hide().onOutflow().onSection("AB").onCategory("401")
        assertHide('detail', 'Resident.Individual.ForeignIDNumber', {
          ReportingQualifier: "BOPCUS",
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: "401" }]
        }), //A
        assertHide('detail', 'Resident.Individual.ForeignIDNumber', {
          ReportingQualifier: "NON RESIDENT RAND",
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: "401" }]
        }), //"B"

        //Resident.Individual.ForeignIDCountry
        assertShow('detail', 'Resident.Individual.ForeignIDCountry', { ReportingQualifier: "BOPCUS" }), //A
        assertShow('detail', 'Resident.Individual.ForeignIDCountry', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Individual.ForeignIDCountry', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Individual.ForeignIDCountry', { ReportingQualifier: "INTERBANK" }), //"D";
        assertShow('detail', 'Resident.Individual.ForeignIDCountry', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'Resident.Individual.ForeignIDCountry', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertShow('detail', 'Resident.Individual.ForeignIDCountry', { ReportingQualifier: "BOPDIR" }), //"G";
        //hide().onSection("AB").onCategory(["511", "512", "513", "514", "515"]),
        assertHide('detail', 'Resident.Individual.ForeignIDCountry', {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ CategoryCode: "511" }]
        }), //A
        assertHide('detail', 'Resident.Individual.ForeignIDCountry', {
          ReportingQualifier: "NON RESIDENT RAND",
          MonetaryAmount: [{ CategoryCode: "511" }]
        }), //"B"
        assertShow('detail', 'Resident.Individual.ForeignIDCountry', {
          ReportingQualifier: "BOPCUS",
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "401" }]
        }), //A
        assertShow('detail', 'Resident.Individual.ForeignIDCountry', {
          ReportingQualifier: "NON RESIDENT RAND",
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "401" }]
        }), //"B"
        //hide().onOutflow().onSection("AB").onCategory("401")
        assertHide('detail', 'Resident.Individual.ForeignIDCountry', {
          ReportingQualifier: "BOPCUS",
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: "401" }]
        }), //A
        assertHide('detail', 'Resident.Individual.ForeignIDCountry', {
          ReportingQualifier: "NON RESIDENT RAND",
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: "401" }]
        }), //"B"

        //Resident.Individual.PassportNumber
        assertShow('detail', 'Resident.Individual.PassportNumber', { ReportingQualifier: "BOPCUS" }), //A
        assertShow('detail', 'Resident.Individual.PassportNumber', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Individual.PassportNumber', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Individual.PassportNumber', { ReportingQualifier: "INTERBANK" }), //"D";
        assertShow('detail', 'Resident.Individual.PassportNumber', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'Resident.Individual.PassportNumber', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertHide('detail', 'Resident.Individual.PassportNumber', { ReportingQualifier: "BOPDIR" }), //"G";
        assertHide('detail', 'Resident.Individual.PassportNumber', {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ CategoryCode: "255" }]
        }), //A
        assertHide('detail', 'Resident.Individual.PassportNumber', {
          ReportingQualifier: "NON RESIDENT RAND",
          MonetaryAmount: [{ CategoryCode: "255" }]
        }), //"B";

        //Resident.Individual.PassportCountry
        assertShow('detail', 'Resident.Individual.PassportCountry', { ReportingQualifier: "BOPCUS" }), //A
        assertShow('detail', 'Resident.Individual.PassportCountry', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Individual.PassportCountry', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Individual.PassportCountry', { ReportingQualifier: "INTERBANK" }), //"D";
        assertShow('detail', 'Resident.Individual.PassportCountry', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertNoEvent('detail', 'Resident.Individual.PassportCountry', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertHide('detail', 'Resident.Individual.PassportCountry', { ReportingQualifier: "BOPDIR" }), //"G";
        assertHide('detail', 'Resident.Individual.PassportCountry', {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ CategoryCode: "255" }]
        }), //A
        assertHide('detail', 'Resident.Individual.PassportCountry', {
          ReportingQualifier: "NON RESIDENT RAND",
          MonetaryAmount: [{ CategoryCode: "255" }]
        }), //"B";

        //Resident.Entity.EntityName
        assertNoEvent('detail', 'Resident.Entity.EntityName', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.EntityName', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Entity.EntityName', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Entity.EntityName', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.EntityName', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.EntityName', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.EntityName', { ReportingQualifier: "BOPDIR" }), //"G";


        //Resident.Entity.TradingName
        assertNoEvent('detail', 'Resident.Entity.TradingName', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.TradingName', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Entity.TradingName', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Entity.TradingName', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.TradingName', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.TradingName', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.TradingName', { ReportingQualifier: "BOPDIR" }), //"G";


        //Resident.Entity.RegistrationNumber
        assertNoEvent('detail', 'Resident.Entity.RegistrationNumber', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.RegistrationNumber', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Entity.RegistrationNumber', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Entity.RegistrationNumber', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.RegistrationNumber', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.RegistrationNumber', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.RegistrationNumber', { ReportingQualifier: "BOPDIR" }), //"G";


        //Resident.Entity.InstitutionalSector
        assertNoEvent('detail', 'Resident.Entity.InstitutionalSector', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.InstitutionalSector', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Entity.InstitutionalSector', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Entity.InstitutionalSector', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.InstitutionalSector', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.InstitutionalSector', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.InstitutionalSector', { ReportingQualifier: "BOPDIR" }), //"G";

        //Resident.Entity.IndustrialClassification
        assertNoEvent('detail', 'Resident.Entity.IndustrialClassification', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.IndustrialClassification', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Entity.IndustrialClassification', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Entity.IndustrialClassification', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.IndustrialClassification', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.IndustrialClassification', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.IndustrialClassification', { ReportingQualifier: "BOPDIR" }), //"G";

        //Resident.Exception
        assertNoEvent('detail', 'Resident.Exception', { ReportingQualifier: "BOPCUS" }), //A
        assertHide('detail', 'Resident.Exception', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Exception', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Exception', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'Resident.Exception', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Exception', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Exception', { ReportingQualifier: "BOPDIR" }), //"G";

        //Resident.Exception.ExceptionName
        assertExcluded('detail', 'Resident.Exception.ExceptionName', { ReportingQualifier: "BOPCUS" }), //A
        assertHide('detail', 'Resident.Exception.ExceptionName', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertExcluded('detail', 'Resident.Exception.ExceptionName', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertExcluded('detail', 'Resident.Exception.ExceptionName', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'Resident.Exception.ExceptionName', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Exception.ExceptionName', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertExcluded('detail', 'Resident.Exception.ExceptionName', { ReportingQualifier: "BOPDIR" }), //"G";

        //Resident.Exception.Country
        assertHide('detail', 'Resident.Exception.Country', { ReportingQualifier: "BOPCUS" }), //A
        assertHide('detail', 'Resident.Exception.Country', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Exception.Country', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Exception.Country', { ReportingQualifier: "INTERBANK" }), //"D";
        assertHide('detail', 'Resident.Exception.Country', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Exception.Country', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertHide('detail', 'Resident.Exception.Country', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual
        assertShow('detail', "Resident.Individual", {}),
        // Resident.Entity
        assertShow('detail', "Resident.Entity", {}),

        // Resident.Individual.AccountName
        assertShow('detail', "Resident.Individual.AccountName", {}),
        // Resident.Entity.AccountName
        assertShow('detail', "Resident.Entity.AccountName", {}),

        // Resident.Individual.AccountIdentifier
        assertLimited('detail', 'Resident.Individual.AccountIdentifier', { ReportingQualifier: "BOPCUS" }), //A
        assertLimited('detail', 'Resident.Individual.AccountIdentifier', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertLimited('detail', 'Resident.Individual.AccountIdentifier', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertLimited('detail', 'Resident.Individual.AccountIdentifier', { ReportingQualifier: "INTERBANK" }), //"D";
        assertLimited('detail', 'Resident.Individual.AccountIdentifier', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.AccountIdentifier', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertLimited('detail', 'Resident.Individual.AccountIdentifier', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.AccountIdentifier
        assertLimited('detail', 'Resident.Entity.AccountIdentifier', { ReportingQualifier: "BOPCUS" }), //A
        assertLimited('detail', 'Resident.Entity.AccountIdentifier', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertLimited('detail', 'Resident.Entity.AccountIdentifier', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertLimited('detail', 'Resident.Entity.AccountIdentifier', { ReportingQualifier: "INTERBANK" }), //"D";
        assertLimited('detail', 'Resident.Entity.AccountIdentifier', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.AccountIdentifier', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertLimited('detail', 'Resident.Entity.AccountIdentifier', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.AccountNumber
        assertNoEvent('detail', 'Resident.Individual.AccountNumber', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Individual.AccountNumber', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Individual.AccountNumber', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Individual.AccountNumber', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.AccountNumber', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.AccountNumber', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Individual.AccountNumber', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.AccountNumber
        assertNoEvent('detail', 'Resident.Entity.AccountNumber', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.AccountNumber', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Entity.AccountNumber', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Entity.AccountNumber', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.AccountNumber', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.AccountNumber', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.AccountNumber', { ReportingQualifier: "BOPDIR" }), //"G";

        //Resident.Individual.CustomsClientNumber
        assertHide('detail', 'Resident.Individual.CustomsClientNumber', {}),
        //show().onInflow().onSection("AB").onCategory(["101", "103", "105", "106"]),
        assertShow('detail', 'Resident.Individual.CustomsClientNumber', {
          ReportingQualifier: "BOPCUS",
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "101" }]

        }), //A
        assertShow('detail', 'Resident.Individual.CustomsClientNumber', {
          ReportingQualifier: "NON RESIDENT RAND",
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "101" }]
        }), //"B";

        assertHide('detail', 'Resident.Individual.CustomsClientNumber', {
          ReportingQualifier: "BOPCUS",
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "102" }]

        }), //A
        assertHide('detail', 'Resident.Individual.CustomsClientNumber', {
          ReportingQualifier: "NON RESIDENT RAND",
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "102" }]
        }), //"B";
        //show().onOutflow().onSection("AB").onCategory(["101", "102", "103", "104", "105", "106"])
        assertShow('detail', 'Resident.Individual.CustomsClientNumber', {
          ReportingQualifier: "BOPCUS",
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: "102" }]

        }), //A
        assertShow('detail', 'Resident.Individual.CustomsClientNumber', {
          ReportingQualifier: "NON RESIDENT RAND",
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: "102" }]
        }), //"B";

        //Resident.Entity.CustomsClientNumber
        assertHide('detail', 'Resident.Entity.CustomsClientNumber', {}),
        //show().onInflow().onSection("AB").onCategory(["101", "103", "105", "106"]),
        assertShow('detail', 'Resident.Entity.CustomsClientNumber', {
          ReportingQualifier: "BOPCUS",
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "101" }]

        }), //A
        assertShow('detail', 'Resident.Entity.CustomsClientNumber', {
          ReportingQualifier: "NON RESIDENT RAND",
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "101" }]
        }), //"B";

        assertHide('detail', 'Resident.Entity.CustomsClientNumber', {
          ReportingQualifier: "BOPCUS",
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "102" }]

        }), //A
        assertHide('detail', 'Resident.Entity.CustomsClientNumber', {
          ReportingQualifier: "NON RESIDENT RAND",
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "102" }]
        }), //"B";
        //show().onOutflow().onSection("AB").onCategory(["101", "102", "103", "104", "105", "106"])
        assertShow('detail', 'Resident.Entity.CustomsClientNumber', {
          ReportingQualifier: "BOPCUS",
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: "102" }]

        }), //A
        assertShow('detail', 'Resident.Entity.CustomsClientNumber', {
          ReportingQualifier: "NON RESIDENT RAND",
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: "102" }]
        }), //"B";

        // Resident.Individual.TaxNumber
        assertNoEvent('detail', 'Resident.Individual.TaxNumber', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Individual.TaxNumber', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Individual.TaxNumber', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Individual.TaxNumber', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.TaxNumber', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.TaxNumber', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Individual.TaxNumber', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.TaxNumber
        assertNoEvent('detail', 'Resident.Entity.TaxNumber', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.TaxNumber', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Entity.TaxNumber', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Entity.TaxNumber', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.TaxNumber', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.TaxNumber', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.TaxNumber', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.VATNumber
        assertNoEvent('detail', 'Resident.Individual.VATNumber', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Individual.VATNumber', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Individual.VATNumber', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Individual.VATNumber', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.VATNumber', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.VATNumber', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Individual.VATNumber', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.TaxNumber
        assertNoEvent('detail', 'Resident.Entity.VATNumber', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.VATNumber', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertNoEvent('detail', 'Resident.Entity.VATNumber', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertNoEvent('detail', 'Resident.Entity.VATNumber', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.VATNumber', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.VATNumber', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.VATNumber', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.TaxClearanceCertificateIndicator
        assertHide('detail', 'Resident.Individual.TaxClearanceCertificateIndicator', {}),
        //show().onOutflow().onSection("AB").onCategory(['512', '513'])
        assertShow('detail', 'Resident.Individual.TaxClearanceCertificateIndicator', {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //A
        assertShow('detail', 'Resident.Individual.TaxClearanceCertificateIndicator', {
          ReportingQualifier: "NON RESIDENT RAND",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"B";
        assertHide('detail', 'Resident.Individual.TaxClearanceCertificateIndicator', {
          ReportingQualifier: "NON REPORTABLE",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"C";
        assertHide('detail', 'Resident.Individual.TaxClearanceCertificateIndicator', {
          ReportingQualifier: "INTERBANK",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"D";
        assertHide('detail', 'Resident.Individual.TaxClearanceCertificateIndicator', {
          ReportingQualifier: "BOPCARD RESIDENT",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"E";
        assertHide('detail', 'Resident.Individual.TaxClearanceCertificateIndicator', {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"F";
        assertHide('detail', 'Resident.Individual.TaxClearanceCertificateIndicator', {
          ReportingQualifier: "BOPDIR",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"G";

        // Resident.Entity.TaxClearanceCertificateIndicator
        assertHide('detail', 'Resident.Entity.TaxClearanceCertificateIndicator', {}),
        //show().onOutflow().onSection("AB").onCategory(['512', '513'])
        assertShow('detail', 'Resident.Entity.TaxClearanceCertificateIndicator', {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //A
        assertShow('detail', 'Resident.Entity.TaxClearanceCertificateIndicator', {
          ReportingQualifier: "NON RESIDENT RAND",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"B";
        assertHide('detail', 'Resident.Entity.TaxClearanceCertificateIndicator', {
          ReportingQualifier: "NON REPORTABLE",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"C";
        assertHide('detail', 'Resident.Entity.TaxClearanceCertificateIndicator', {
          ReportingQualifier: "INTERBANK",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"D";
        assertHide('detail', 'Resident.Entity.TaxClearanceCertificateIndicator', {
          ReportingQualifier: "BOPCARD RESIDENT",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"E";
        assertHide('detail', 'Resident.Entity.TaxClearanceCertificateIndicator', {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"F";
        assertHide('detail', 'Resident.Entity.TaxClearanceCertificateIndicator', {
          ReportingQualifier: "BOPDIR",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"G";

        // Resident.Individual.TaxClearanceCertificateReference
        assertHide('detail', 'Resident.Individual.TaxClearanceCertificateReference', {}),
        //show().onOutflow().onSection("AB").onCategory(['512', '513'])
        assertShow('detail', 'Resident.Individual.TaxClearanceCertificateReference', {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //A
        assertShow('detail', 'Resident.Individual.TaxClearanceCertificateReference', {
          ReportingQualifier: "NON RESIDENT RAND",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"B";
        assertHide('detail', 'Resident.Individual.TaxClearanceCertificateReference', {
          ReportingQualifier: "NON REPORTABLE",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"C";
        assertHide('detail', 'Resident.Individual.TaxClearanceCertificateReference', {
          ReportingQualifier: "INTERBANK",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"D";
        assertHide('detail', 'Resident.Individual.TaxClearanceCertificateReference', {
          ReportingQualifier: "BOPCARD RESIDENT",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"E";
        assertHide('detail', 'Resident.Individual.TaxClearanceCertificateReference', {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"F";
        assertHide('detail', 'Resident.Individual.TaxClearanceCertificateReference', {
          ReportingQualifier: "BOPDIR",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"G";

        // Resident.Entity.TaxClearanceCertificateReference
        assertHide('detail', 'Resident.Entity.TaxClearanceCertificateReference', {}),
        //show().onOutflow().onSection("AB").onCategory(['512', '513'])
        assertShow('detail', 'Resident.Entity.TaxClearanceCertificateReference', {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //A
        assertShow('detail', 'Resident.Entity.TaxClearanceCertificateReference', {
          ReportingQualifier: "NON RESIDENT RAND",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"B";
        assertHide('detail', 'Resident.Entity.TaxClearanceCertificateReference', {
          ReportingQualifier: "NON REPORTABLE",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"C";
        assertHide('detail', 'Resident.Entity.TaxClearanceCertificateReference', {
          ReportingQualifier: "INTERBANK",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"D";
        assertHide('detail', 'Resident.Entity.TaxClearanceCertificateReference', {
          ReportingQualifier: "BOPCARD RESIDENT",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"E";
        assertHide('detail', 'Resident.Entity.TaxClearanceCertificateReference', {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"F";
        assertHide('detail', 'Resident.Entity.TaxClearanceCertificateReference', {
          ReportingQualifier: "BOPDIR",
          MonetaryAmount: [{ CategoryCode: "512" }],
          Flow: 'OUT'
        }), //"G";

        // Resident.Individual.StreetAddress.AddressLine1
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.AddressLine1', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.AddressLine1', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Individual.StreetAddress.AddressLine1', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Individual.StreetAddress.AddressLine1', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.AddressLine1', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.StreetAddress.AddressLine1', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.AddressLine1', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.StreetAddress.AddressLine1
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.AddressLine1', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.AddressLine1', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Entity.StreetAddress.AddressLine1', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Entity.StreetAddress.AddressLine1', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.AddressLine1', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.StreetAddress.AddressLine1', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.AddressLine1', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.StreetAddress.AddressLine2
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.AddressLine2', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.AddressLine2', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Individual.StreetAddress.AddressLine2', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Individual.StreetAddress.AddressLine2', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.AddressLine2', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.StreetAddress.AddressLine2', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.AddressLine2', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.StreetAddress.AddressLine2
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.AddressLine2', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.AddressLine2', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Entity.StreetAddress.AddressLine2', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Entity.StreetAddress.AddressLine2', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.AddressLine2', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.StreetAddress.AddressLine2', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.AddressLine2', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.StreetAddress.Suburb
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.Suburb', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.Suburb', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Individual.StreetAddress.Suburb', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Individual.StreetAddress.Suburb', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.Suburb', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.StreetAddress.Suburb', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.Suburb', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.StreetAddress.Suburb
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.Suburb', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.Suburb', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Entity.StreetAddress.Suburb', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Entity.StreetAddress.Suburb', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.Suburb', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.StreetAddress.Suburb', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.Suburb', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.StreetAddress.City
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.City', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.City', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Individual.StreetAddress.City', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Individual.StreetAddress.City', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.City', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.StreetAddress.City', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.City', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.StreetAddress.City
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.City', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.City', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Entity.StreetAddress.City', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Entity.StreetAddress.City', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.City', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.StreetAddress.City', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.City', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.StreetAddress.Province
        assertLimited('detail', 'Resident.Individual.StreetAddress.Province', { ReportingQualifier: "BOPCUS" }), //A
        assertLimited('detail', 'Resident.Individual.StreetAddress.Province', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Individual.StreetAddress.Province', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Individual.StreetAddress.Province', { ReportingQualifier: "INTERBANK" }), //"D";
        assertLimited('detail', 'Resident.Individual.StreetAddress.Province', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.StreetAddress.Province', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertLimited('detail', 'Resident.Individual.StreetAddress.Province', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.StreetAddress.Province
        assertLimited('detail', 'Resident.Entity.StreetAddress.Province', { ReportingQualifier: "BOPCUS" }), //A
        assertLimited('detail', 'Resident.Entity.StreetAddress.Province', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Entity.StreetAddress.Province', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Entity.StreetAddress.Province', { ReportingQualifier: "INTERBANK" }), //"D";
        assertLimited('detail', 'Resident.Entity.StreetAddress.Province', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.StreetAddress.Province', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertLimited('detail', 'Resident.Entity.StreetAddress.Province', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.StreetAddress.PostalCode
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.PostalCode', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.PostalCode', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Individual.StreetAddress.PostalCode', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Individual.StreetAddress.PostalCode', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.PostalCode', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.StreetAddress.PostalCode', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Individual.StreetAddress.PostalCode', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.StreetAddress.PostalCode
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.PostalCode', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.PostalCode', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Entity.StreetAddress.PostalCode', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Entity.StreetAddress.PostalCode', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.PostalCode', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.StreetAddress.PostalCode', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.StreetAddress.PostalCode', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.PostalAddress.PostalCode
        assertNoEvent('detail', 'Resident.Individual.PostalAddress.PostalCode', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Individual.PostalAddress.PostalCode', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Individual.PostalAddress.PostalCode', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Individual.PostalAddress.PostalCode', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.PostalAddress.PostalCode', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.PostalAddress.PostalCode', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Individual.PostalAddress.PostalCode', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.PostalAddress.PostalCode
        assertNoEvent('detail', 'Resident.Entity.PostalAddress.PostalCode', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.PostalAddress.PostalCode', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Entity.PostalAddress.PostalCode', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Entity.PostalAddress.PostalCode', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.PostalAddress.PostalCode', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.PostalAddress.PostalCode', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.PostalAddress.PostalCode', { ReportingQualifier: "BOPDIR" }), //"G";


        // Resident.Individual.ContactDetails.Email
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.Email', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.Email', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Individual.ContactDetails.Email', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Individual.ContactDetails.Email', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.Email', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.ContactDetails.Email', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.Email', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.ContactDetails.Email
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.Email', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.Email', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Entity.ContactDetails.Email', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Entity.ContactDetails.Email', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.Email', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.ContactDetails.Email', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.Email', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.ContactDetails.ContactSurname
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.ContactSurname', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.ContactSurname', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Individual.ContactDetails.ContactSurname', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Individual.ContactDetails.ContactSurname', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.ContactSurname', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.ContactDetails.ContactSurname', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.ContactSurname', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.ContactDetails.ContactSurname
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.ContactSurname', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.ContactSurname', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Entity.ContactDetails.ContactSurname', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Entity.ContactDetails.ContactSurname', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.ContactSurname', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.ContactDetails.ContactSurname', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.ContactSurname', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.ContactDetails.ContactName
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.ContactName', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.ContactName', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Individual.ContactDetails.ContactName', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Individual.ContactDetails.ContactName', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.ContactName', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.ContactDetails.ContactName', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.ContactName', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.ContactDetails.ContactName
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.ContactName', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.ContactName', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Entity.ContactDetails.ContactName', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Entity.ContactDetails.ContactName', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.ContactName', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.ContactDetails.ContactName', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.ContactName', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.ContactDetails.Telephone
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.Telephone', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.Telephone', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Individual.ContactDetails.Telephone', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Individual.ContactDetails.Telephone', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.Telephone', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.ContactDetails.Telephone', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.Telephone', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.ContactDetails.Telephone
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.Telephone', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.Telephone', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Entity.ContactDetails.Telephone', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Entity.ContactDetails.Telephone', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.Telephone', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.ContactDetails.Telephone', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.Telephone', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.ContactDetails.Fax
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.Fax', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.Fax', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Individual.ContactDetails.Fax', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Individual.ContactDetails.Fax', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.Fax', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.ContactDetails.Fax', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Individual.ContactDetails.Fax', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.ContactDetails.Fax
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.Fax', { ReportingQualifier: "BOPCUS" }), //A
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.Fax', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Entity.ContactDetails.Fax', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Entity.ContactDetails.Fax', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.Fax', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.ContactDetails.Fax', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertNoEvent('detail', 'Resident.Entity.ContactDetails.Fax', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.CardNumber
        assertHide('detail', 'Resident.Individual.CardNumber', { ReportingQualifier: "BOPCUS" }), //A
        assertHide('detail', 'Resident.Individual.CardNumber', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Individual.CardNumber', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Individual.CardNumber', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.CardNumber', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.CardNumber', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertHide('detail', 'Resident.Individual.CardNumber', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.CardNumber
        assertHide('detail', 'Resident.Entity.CardNumber', { ReportingQualifier: "BOPCUS" }), //A
        assertHide('detail', 'Resident.Entity.CardNumber', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Entity.CardNumber', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Entity.CardNumber', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.CardNumber', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.CardNumber', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertHide('detail', 'Resident.Entity.CardNumber', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Individual.SupplementaryCardIndicator
        assertHide('detail', 'Resident.Individual.SupplementaryCardIndicator', { ReportingQualifier: "BOPCUS" }), //A
        assertHide('detail', 'Resident.Individual.SupplementaryCardIndicator', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Individual.SupplementaryCardIndicator', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Individual.SupplementaryCardIndicator', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Individual.SupplementaryCardIndicator', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Individual.SupplementaryCardIndicator', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertHide('detail', 'Resident.Individual.SupplementaryCardIndicator', { ReportingQualifier: "BOPDIR" }), //"G";

        // Resident.Entity.SupplementaryCardIndicator
        assertHide('detail', 'Resident.Entity.SupplementaryCardIndicator', { ReportingQualifier: "BOPCUS" }), //A
        assertHide('detail', 'Resident.Entity.SupplementaryCardIndicator', { ReportingQualifier: "NON RESIDENT RAND" }), //"B";
        assertHide('detail', 'Resident.Entity.SupplementaryCardIndicator', { ReportingQualifier: "NON REPORTABLE" }), //"C";
        assertHide('detail', 'Resident.Entity.SupplementaryCardIndicator', { ReportingQualifier: "INTERBANK" }), //"D";
        assertNoEvent('detail', 'Resident.Entity.SupplementaryCardIndicator', { ReportingQualifier: "BOPCARD RESIDENT" }), //"E";
        assertHide('detail', 'Resident.Entity.SupplementaryCardIndicator', { ReportingQualifier: "BOPCARD NON RESIDENT" }), //"F";
        assertHide('detail', 'Resident.Entity.SupplementaryCardIndicator', { ReportingQualifier: "BOPDIR" }), //"G";


        /////////////////////////////////////////
        // MONEY DETAIL
        /////////////////////////////////////////

        //basics
        assertShow('detail', "SequenceNumber", { MonetaryAmount: [{}] }),
        assertShow('detail', "MoneyTransferAgentIndicator", { MonetaryAmount: [{}] }),
        assertNoEvent('detail', "RandValue", { MonetaryAmount: [{}] }),
        assertHide('detail', "RandValue", {
          ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}]
        }),
        assertNoEvent('detail', "ForeignValue", { MonetaryAmount: [{}] }),
        assertHide('detail', "ForeignValue", {
          ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}]
        }),
        assertNoEvent('detail', "CategoryCode", { MonetaryAmount: [{}] }),
        assertHide('detail', "CategoryCode", {
          ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}]
        }),
        assertNoEvent('detail', "CategorySubCode", { MonetaryAmount: [{}] }),
        assertHide('detail', "CategorySubCode", {
          ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}]
        }),
        assertShow('detail', "SWIFTDetails", { MonetaryAmount: [{}] }),

        //StrateRefNumber
        assertHide('detail', "StrateRefNumber", { MonetaryAmount: [{}] }),
        assertShow('detail', "StrateRefNumber", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{}]
        }),
        assertShow('detail', "StrateRefNumber", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '601', CategorySubCode: "01" }]
        }),

        //Travel
        assertHide('detail', "Travel", { MonetaryAmount: [{}] }),
        assertHide('detail', "Travel.Surname", { MonetaryAmount: [{}] }),
        assertHide('detail', "Travel.Name", { MonetaryAmount: [{}] }),
        assertHide('detail', "Travel.IDNumber", { MonetaryAmount: [{}] }),
        assertHide('detail', "Travel.DateOfBirth", { MonetaryAmount: [{}] }),
        assertHide('detail', "Travel.TempResPermitNumber", { MonetaryAmount: [{}] }),

        //LoanRefNumber
        assertHide('detail', "LoanRefNumber", { MonetaryAmount: [{}] }),
        assertShow('detail', "LoanRefNumber", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '801' }]
        }),
        assertShow('detail', "LoanRefNumber", {
          ReportingQualifier: 'BOPCUS',
          Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: '106' }]
        }),
        assertValue('detail', 'LoanRefNumber', '99012301230123', {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '801', LocationCountry: "LS" }]
        }),
        assertValue('detail', 'LoanRefNumber', '99456745674567', {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '801', LocationCountry: "SZ" }]
        }),
        assertValue('detail', 'LoanRefNumber', '99789078907890', {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '801', LocationCountry: "NA" }]
        }),
        assertValue('detail', 'LoanRefNumber', '99012301230123', {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '106', LocationCountry: "LS" }]
        }),
        assertValue('detail', 'LoanRefNumber', '99456745674567', {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '106', LocationCountry: "SZ" }]
        }),
        assertValue('detail', 'LoanRefNumber', '99789078907890', {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '106', LocationCountry: "NA" }]
        }),

        //LoanTenor
        assertShow('detail', "LoanTenor", {
          ReportingQualifier: 'BOPCUS',
          Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: '815' }]
        }),
        assertHide('detail', "LoanTenor", {
          ReportingQualifier: 'NON REPORTABLE',
          MonetaryAmount: [{}]
        }),
        assertNoEvent('detail', "LoanTenor", {
          ReportingQualifier: 'BOPCUS',
          Flow: "IN",
          MonetaryAmount: [{ CategoryCode: '815' }]
        }),

        //LoanInterestRate
        assertHide('detail', "LoanInterestRate", { MonetaryAmount: [{}] }),
        assertShow('detail', "LoanInterestRate", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815' }]
        }),
        assertShow('detail', "LoanInterestRate", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '309', CategorySubCode: "06" }]
        }),
        assertShow('detail', "LoanInterestRate", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '309', CategorySubCode: "05" }]
        }),


        //SARBAuth.RulingsSection
        assertHide('detail', "SARBAuth.RulingsSection", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}]
        }),
        assertNoEvent('detail', "SARBAuth.RulingsSection", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}]
        }),
        assertHide('detail', "SARBAuth.RulingsSection", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '200' }]
        }),

        //SARBAuth.ADInternalAuthNumber
        assertHide('detail', "SARBAuth.ADInternalAuthNumber", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}]
        }),
        assertNoEvent('detail', "SARBAuth.ADInternalAuthNumber", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}]
        }),
        assertHide('detail', "SARBAuth.ADInternalAuthNumber", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '200' }]
        }),
        assertShow('detail', "SARBAuth.ADInternalAuthNumber", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '200', SARBAuth: { ADInternalAuthNumberDate: "2015-01-01" } }]
        }),

        //SARBAuth.ADInternalAuthNumberDate
        assertHide('detail', "SARBAuth.ADInternalAuthNumberDate", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}]
        }),
        assertNoEvent('detail', "SARBAuth.ADInternalAuthNumberDate", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}]
        }),
        assertHide('detail', "SARBAuth.ADInternalAuthNumberDate", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '200' }]
        }),
        assertShow('detail', "SARBAuth.ADInternalAuthNumberDate", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '200', SARBAuth: { ADInternalAuthNumber: "2015-01-01" } }]
        }),

        //SARBAuth.SARBAuthAppNumber
        assertHide('detail', "SARBAuth.SARBAuthAppNumber", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}]
        }),
        assertNoEvent('detail', "SARBAuth.SARBAuthAppNumber", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}]
        }),
        assertHide('detail', "SARBAuth.SARBAuthAppNumber", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '200' }]
        }),

        //SARBAuth.SARBAuthRefNumber
        assertHide('detail', "SARBAuth.SARBAuthRefNumber", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}]
        }),
        assertNoEvent('detail', "SARBAuth.SARBAuthRefNumber", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}]
        }),
        assertHide('detail', "SARBAuth.SARBAuthRefNumber", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '200' }]
        }),

        //CannotCategorize
        assertHide('detail', "CannotCategorize", { MonetaryAmount: [{}] }),
        assertShow('detail', "CannotCategorize", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '830' }]
        }),

        //AdHocRequirement.Subject
        assertHide('detail', "AdHocRequirement.Subject", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}]
        }),
        assertNoEvent('detail', "AdHocRequirement.Subject", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}]
        }),


        //AdHocRequirement.Description
        assertHide('detail', "AdHocRequirement.Description", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}]
        }),
        assertNoEvent('detail', "AdHocRequirement.Description", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}]
        }),


        //LocationCountry
        assertHide('detail', "LocationCountry", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}]
        }),
        assertExcluded('detail', "LocationCountry", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}]
        }),

        //ReversalTrnRefNumber
        assertHide('detail', "ReversalTrnRefNumber", { MonetaryAmount: [{}] }),
        assertShow('detail', "ReversalTrnRefNumber", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '300' }]
        }),
        assertShow('detail', "ReversalTrnRefNumber", {
          ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{ CardChargeBack: 'Y' }]
        }),

        //ReversalTrnSeqNumber
        assertHide('detail', "ReversalTrnSeqNumber", { MonetaryAmount: [{}] }),
        assertShow('detail', "ReversalTrnSeqNumber", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '300' }]
        }),

        //BOPDIRTrnReference
        assertNoEvent('detail', "BOPDIRTrnReference", { MonetaryAmount: [{}] }),
        assertNoEvent('detail', "BOPDIRTrnReference", {
          ReportingQualifier: 'BOPDIR', MonetaryAmount: [{}]
        }),
        assertHide('detail', "BOPDIRTrnReference", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '300' }]
        }),

        //BOPDIRADCode
        assertNoEvent('detail', "BOPDIRADCode", { MonetaryAmount: [{}] }),
        assertNoEvent('detail', "BOPDIRADCode", {
          ReportingQualifier: 'BOPDIR', MonetaryAmount: [{}]
        }),
        assertHide('detail', "BOPDIRADCode", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '300' }]
        }),

        //ThirdParty.Individual.Surname
        assertShow('detail', "ThirdParty.Individual.Surname", { MonetaryAmount: [{}] }),
        //ThirdParty.Individual.Name
        assertShow('detail', "ThirdParty.Individual.Name", { MonetaryAmount: [{}] }),
        //ThirdParty.Individual.Gender
        assertShow('detail', "ThirdParty.Individual.Gender", { MonetaryAmount: [{}] }),
        //ThirdParty.Individual.IDNumber
        assertShow('detail', "ThirdParty.Individual.IDNumber", { MonetaryAmount: [{}] }),
        //ThirdParty.Individual.DateOfBirth
        assertShow('detail', "ThirdParty.Individual.DateOfBirth", { MonetaryAmount: [{}] }),
        //ThirdParty.Individual.TempResPermitNumber
        assertShow('detail', "ThirdParty.Individual.TempResPermitNumber", { MonetaryAmount: [{}] }),
        //ThirdParty.Individual.PassportNumber
        assertShow('detail', "ThirdParty.Individual.PassportNumber", { MonetaryAmount: [{}] }),
        //ThirdParty.Individual.PassportCountry
        assertShow('detail', "ThirdParty.Individual.PassportCountry", { MonetaryAmount: [{}] }),
        //ThirdParty.Entity.Name
        assertShow('detail', "ThirdParty.Entity.Name", { MonetaryAmount: [{}] }),
        //ThirdParty.Entity.RegistrationNumber
        assertShow('detail', "ThirdParty.Entity.RegistrationNumber", { MonetaryAmount: [{}] }),

        //ThirdParty.CustomsClientNumber
        assertNoEvent('detail', "ThirdParty.CustomsClientNumber", { MonetaryAmount: [{}] }),
        assertNoEvent('detail', "ThirdParty.CustomsClientNumber", {
          ReportingQualifier: 'BOPDIR', MonetaryAmount: [{}]
        }),
        assertHide('detail', "ThirdParty.CustomsClientNumber", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ CategoryCode: '300' }]
        }),

        //ThirdParty.TaxNumber
        assertNoEvent('detail', "ThirdParty.TaxNumber", { MonetaryAmount: [{}] }),
        assertNoEvent('detail', "ThirdParty.TaxNumber", {
          ReportingQualifier: 'BOPDIR', MonetaryAmount: [{}]
        }),
        assertHide('detail', "ThirdParty.TaxNumber", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ CategoryCode: '300' }]
        }),

        //ThirdParty.TaxNumber
        assertNoEvent('detail', "ThirdParty.VATNumber", { MonetaryAmount: [{}] }),
        assertNoEvent('detail', "ThirdParty.VATNumber", {
          ReportingQualifier: 'BOPDIR', MonetaryAmount: [{}]
        }),
        assertHide('detail', "ThirdParty.VATNumber", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ CategoryCode: '300' }]
        }),

        //ThirdParty.StreetAddress.AddressLine1
        assertShow('detail', "ThirdParty.StreetAddress.AddressLine1", { MonetaryAmount: [{}] }),
        //ThirdParty.StreetAddress.AddressLine2
        assertShow('detail', "ThirdParty.StreetAddress.AddressLine2", { MonetaryAmount: [{}] }),
        //ThirdParty.StreetAddress.Suburb
        assertShow('detail', "ThirdParty.StreetAddress.Suburb", { MonetaryAmount: [{}] }),
        //ThirdParty.StreetAddress.City
        assertShow('detail', "ThirdParty.StreetAddress.City", { MonetaryAmount: [{}] }),
        //ThirdParty.StreetAddress.Province
        assertShow('detail', "ThirdParty.StreetAddress.Province", { MonetaryAmount: [{}] }),
        //ThirdParty.StreetAddress.PostalCode
        assertShow('detail', "ThirdParty.StreetAddress.PostalCode", { MonetaryAmount: [{}] }),
        //ThirdParty.PostalAddress.AddressLine1
        assertShow('detail', "ThirdParty.PostalAddress.AddressLine1", { MonetaryAmount: [{}] }),
        //ThirdParty.PostalAddress.AddressLine2
        assertShow('detail', "ThirdParty.PostalAddress.AddressLine2", { MonetaryAmount: [{}] }),
        //ThirdParty.PostalAddress.Suburb
        assertShow('detail', "ThirdParty.PostalAddress.Suburb", { MonetaryAmount: [{}] }),
        //ThirdParty.PostalAddress.City
        assertShow('detail', "ThirdParty.PostalAddress.City", { MonetaryAmount: [{}] }),
        //ThirdParty.PostalAddress.Province
        assertShow('detail', "ThirdParty.PostalAddress.Province", { MonetaryAmount: [{}] }),
        //ThirdParty.PostalAddress.PostalCode
        assertShow('detail', "ThirdParty.PostalAddress.PostalCode", { MonetaryAmount: [{}] }),

        //ThirdParty.ContactDetails.ContactName
        assertShow('detail', "ThirdParty.ContactDetails.ContactName", { MonetaryAmount: [{}] }),
        //ThirdParty.ContactDetails.ContactName
        assertShow('detail', "ThirdParty.ContactDetails.Email", { MonetaryAmount: [{}] }),
        //ThirdParty.ContactDetails.ContactName
        assertShow('detail', "ThirdParty.ContactDetails.Fax", { MonetaryAmount: [{}] }),
        //ThirdParty.ContactDetails.ContactName
        assertShow('detail', "ThirdParty.ContactDetails.Telephone", { MonetaryAmount: [{}] }),

        //CardChargeBack
        assertNoEvent('detail', "CardChargeBack", { MonetaryAmount: [{}] }),
        assertNoEvent('detail', "CardChargeBack", {
          ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]
        }),
        assertHide('detail', "CardChargeBack", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ CategoryCode: '300' }]
        }),

        //CardIndicator
        assertNoEvent('detail', "CardIndicator", { MonetaryAmount: [{}] }),
        assertNoEvent('detail', "CardIndicator", {
          ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]
        }),
        assertHide('detail', "CardIndicator", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ CategoryCode: '300' }]
        }),

        //ElectronicCommerceIndicator
        assertNoEvent('detail', "ElectronicCommerceIndicator", { MonetaryAmount: [{}] }),
        assertNoEvent('detail', "ElectronicCommerceIndicator", {
          ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]
        }),
        assertHide('detail', "ElectronicCommerceIndicator", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ CategoryCode: '300' }]
        }),

        //POSEntryMode
        assertNoEvent('detail', "POSEntryMode", { MonetaryAmount: [{}] }),
        assertNoEvent('detail', "POSEntryMode", {
          ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]
        }),
        assertHide('detail', "POSEntryMode", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ CategoryCode: '300' }]
        }),

        //CardFraudulentTransactionIndicator
        assertNoEvent('detail', "CardFraudulentTransactionIndicator", { MonetaryAmount: [{}] }),
        assertNoEvent('detail', "CardFraudulentTransactionIndicator", {
          ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]
        }),
        assertHide('detail', "CardFraudulentTransactionIndicator", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ CategoryCode: '300' }]
        }),

        //ForeignCardHoldersPurchasesRandValue
        assertNoEvent('detail', "ForeignCardHoldersPurchasesRandValue", { MonetaryAmount: [{}] }),
        assertNoEvent('detail', "ForeignCardHoldersPurchasesRandValue", {
          ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}]
        }),
        assertHide('detail', "ForeignCardHoldersPurchasesRandValue", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ CategoryCode: '300' }]
        }),


        //ForeignCardHoldersCashWithdrawalsRandValue
        assertNoEvent('detail', "ForeignCardHoldersCashWithdrawalsRandValue", { MonetaryAmount: [{}] }),
        assertNoEvent('detail', "ForeignCardHoldersCashWithdrawalsRandValue", {
          ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}]
        }),
        assertHide('detail', "ForeignCardHoldersCashWithdrawalsRandValue", {
          ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ CategoryCode: '300' }]
        }),


        //ImportExport
        assertHide('detail', "ImportExport", { MonetaryAmount: [{}] }),
        assertShow('detail', "ImportExport", {
          ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '101' }]
        }),


        /////////////////////////////////////////
        // IMPORT EXPORT DETAIL
        /////////////////////////////////////////

        //ImportControlNumber
        assertHide('detail', "ImportControlNumber", { MonetaryAmount: [{ ImportExport: [{}] }] }),
        assertShow('detail', "ImportControlNumber", {
          ReportingQualifier: 'BOPCUS',
          Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: '101', ImportExport: [{}] }]
        }),

        //TransportDocumentNumber
        assertHide('detail', "TransportDocumentNumber", { MonetaryAmount: [{ ImportExport: [{}] }] }),
        assertShow('detail', "TransportDocumentNumber", {
          ReportingQualifier: 'BOPCUS',
          Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: '103', ImportExport: [{}] }]
        }),

        //MRNNotOnIVS
        assertHide('detail', "MRNNotOnIVS", { MonetaryAmount: [{ ImportExport: [{}] }] }),
        assertShow('detail', "MRNNotOnIVS", {
          ReportingQualifier: 'BOPCUS',
          Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: '103', ImportExport: [{}] }]
        }),

        //UCR
        assertHide('detail', "UCR", { MonetaryAmount: [{ ImportExport: [{}] }] }),
        assertShow('detail', "UCR", {
          ReportingQualifier: 'BOPCUS',
          Flow: "IN",
          MonetaryAmount: [{ CategoryCode: '103', ImportExport: [{}] }]
        }),

        //PaymentCurrencyCode
        assertShow('detail', "PaymentCurrencyCode", { MonetaryAmount: [{ ImportExport: [{}] }] }),

        //PaymentValue
        assertShow('detail', "PaymentValue", { MonetaryAmount: [{ ImportExport: [{}] }] }),


      ]

    }

    if (testBase.errorCount == 0)
      testBase.addParagraph("Completed " + testBase.testCount + " display tests Successfully :-)");
    else {
      if (testBase.errorCount == 1)
        testBase.addParagraph("Ran " + testBase.testCount + " display tests with 1 error!");
      else
        testBase.addParagraph("Ran " + testBase.testCount + " display tests with " + testBase.errorCount + " errors!");
    }

    console.log("##teamcity[testSuiteFinished name='displayTest']");
  }
})

