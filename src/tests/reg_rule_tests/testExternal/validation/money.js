define(function () {
  return function (predef) {
    var extTrans;
    with (predef) {

      extTrans = {
        ruleset: "External Money Rules",
        scope: "money",
        validations: [
           {
             field : "ReversalTrnRefNumber",
             rules : [
               //410, "Original transaction and SequenceNumber combination, with an opposite flow, not stored on database"
               //411, "Incorrect reversal category used with original transaction category"
               validate("ext_mrtrn1", "Validate_ReversalTrnRef",
                     notEmpty.and(hasMoneyField('ReversalTrnSeqNumber'))).onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]).onSection("ABG")
             ]
           },
           {
            field : "ReversalTrnSeqNumber",
            rules : [
              //410, "Original transaction and SequenceNumber combination, with an opposite flow, not stored on database"
              //411, "Incorrect reversal category used with original transaction category"
              validate("ext_mrtrn1", "Validate_ReversalTrnRef",
                    notEmpty.and(hasMoneyField('ReversalTrnRefNumber'))).onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]).onSection("ABG")
            ]
          },
          {
            field: "LoanRefNumber",
            rules: [
              validate("ext_mlrn1", "Validate_LoanRef", //374, "Invalid loan reference number"
                notEmpty).onSection("ABG")
            ]
          },
          {
            field: "ThirdParty.CustomsClientNumber",
            rules: [
              validate("ext_tpccn1", "Validate_ValidCCN", //322, "Not a registered customs client number"
                notEmpty.and(hasPattern(/^\d{8}$/))).onSection("AB")
            ]
          },
          {
            field: "ThirdParty.Individual.IDNumber",
            rules: [
              validate("ext_mlval2", "Validate_IndividualSDA",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))
                .and(hasTransactionField("ValueDate"))
                .and(hasSDAMonetaryValue)).onOutflow().onSection("A")
            ]
          }
        ]
      };

    }
    return extTrans;
  }
});
