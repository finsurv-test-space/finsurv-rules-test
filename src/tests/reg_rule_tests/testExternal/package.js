define({
  engine: {major: "1", minor: "0"},
  mappings: {
    LocalCurrencySymbol: "R",
    LocalCurrencyName: "Local Currency",
    LocalCurrency: "ZAR",
    Locale: "ZA",
    LocalValue: "DomesticValue",
    Regulator: "Regulator",
    DealerPrefix: "RE",
    RegulatorPrefix: "CB",
    StateName: "Province"
  }
})