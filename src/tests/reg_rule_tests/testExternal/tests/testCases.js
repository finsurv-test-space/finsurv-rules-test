define(function() {
  return function(testBase) {
      with (testBase) {

        var test_cases = [
          // Resident.Individual.CustomsClientNumber
          // Not a registered import undertaking client
          assertSuccessCustom("ext_ccn1:1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: {
                CustomsClientNumber: "22222222"
              }
            }
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          assertSuccessCustom("ext_ccn1:1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: {
                CustomsClientNumber: "22222222"
              }
            }
          },
          {
            ValidationCache: [{
              Name: "Validate_ImportUndertakingCCN",
              Expiry: "2010-01-01 00:00:00",
              Inputs: {
                Value: "22222222"
              },
              Result: {
                   Status: "Pass" //Pass, Fail, Error
              }
            }],
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          assertSuccessCustom("ext_ccn1:1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: {
                CustomsClientNumber: "22222222"
              }
            }
          },
          {
            ValidationCache: [{
              Name: "Validate_ImportUndertakingCCN",
              Inputs: {
                Value: "22222222"
              },
              Result: {
                   Status: "Pass" //Pass, Fail, Error
              }
            }],
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 1
            }
          }),

          assertFailureCustom("ext_ccn1:1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: {
                CustomsClientNumber: "22222222"
              }
            }
          },
          {
            ValidationCache: [{
              Name: "Validate_ImportUndertakingCCN",
              Inputs: {
                Value: "22222222"
              },
              Result: {
                Status: "Fail", //Pass, Fail, Error
                Code: "323",
                Message: "Not a registered import undertaking client"
              }
            }],
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 1
            }
          }),

          // Should not call the external validation because of invalid CCN format
          assertSuccessCustom("ext_ccn1:1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: {
                CustomsClientNumber: "Invalid CCN format"
              }
            }
          },
          {
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 0
            }
          }),

          // Should not call the external validation because of invalid CCN format
          assertSuccessCustom("ext_ccn1:1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: {
                CustomsClientNumber: ""
              }
            }
          },
          {
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 0
            }
          }),

          assertFailureCustom("ext_ccn1:1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: {
                CustomsClientNumber: "11111111"
              }
            }
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          assertSuccessCustom("ext_ccn1:1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: {
                CustomsClientNumber: "11111111"
              }
            }
          },
          {
            ValidationCache: [{
              Name: "Validate_ImportUndertakingCCN",
              Inputs: {
                Value: "11111111"
              },
              Result: {
                   Status: "Pass" //Pass, Fail, Error
              }
            }],
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 1
            }
          }),

          assertFailureCustom("ext_ccn1:1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: {
                CustomsClientNumber: "33333333"
              }
            }
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),
          assertFailureCustom("ext_ccn1:1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: {
                CustomsClientNumber: "44444444"
              }
            }
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          // Resident.Entity.CustomsClientNumber
          // Not a registered import undertaking client (the Flow is OUT and category is 102/01 to 102/10 or 104/01 to 104/10 is used)
          assertSuccessCustom("ext_ccn1:2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Entity: {
                CustomsClientNumber: "22222222"
              }
            }
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          assertSuccessCustom("ext_ccn1:2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Entity: {
                CustomsClientNumber: "22222222"
              }
            }
          },
          {
            ValidationCache: [{
              Name: "Validate_ImportUndertakingCCN",
              Inputs: {
                Value: "22222222"
              },
              Result: {
                   Status: "Pass" //Pass, Fail, Error
              }
            }],
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 1
            }
          }),

          assertFailureCustom("ext_ccn1:2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Entity: {
                CustomsClientNumber: "22222222"
              }
            }
          },
          {
            ValidationCache: [{
              Name: "Validate_ImportUndertakingCCN",
              Inputs: {
                Value: "22222222"
              },
              Result: {
                Status: "Fail", //Pass, Fail, Error
                Code: "323",
                Message: "Not a registered import undertaking client"
              }
            }],
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 1
            }
          }),

          // Should not call the external validation because of invalid CCN format
          assertSuccessCustom("ext_ccn1:2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Entity: {
                CustomsClientNumber: "Invalid CCN format"
              }
            }
          },
          {
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 0
            }
          }),

          // Should not call the external validation because of invalid CCN format
          assertSuccessCustom("ext_ccn1:2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Entity: {
                CustomsClientNumber: ""
              }
            }
          },
          {
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 0
            }
          }),

          assertFailureCustom("ext_ccn1:2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Entity: {
                CustomsClientNumber: "11111111"
              }
            }
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          assertSuccessCustom("ext_ccn1:2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Entity: {
                CustomsClientNumber: "11111111"
              }
            }
          },
          {
            ValidationCache: [{
              Name: "Validate_ImportUndertakingCCN",
              Inputs: {
                Value: "11111111"
              },
              Result: {
                   Status: "Pass" //Pass, Fail, Error
              }
            }],
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 1
            }
          }),

          assertFailureCustom("ext_ccn1:2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Entity: {
                CustomsClientNumber: "33333333"
              }
            }
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),
          assertFailureCustom("ext_ccn1:2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Entity: {
                CustomsClientNumber: "44444444"
              }
            }
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          // Resident.Individual.CustomsClientNumber
          // Not a registered customs client number
          assertSuccessCustom("ext_ccn2:1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Individual: {
                CustomsClientNumber: "22222222"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          assertSuccessCustom("ext_ccn2:1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Individual: {
                CustomsClientNumber: "22222222"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            ValidationCache: [{
              Name: "Validate_ValidCCN",
              Inputs: {
                Value: "22222222"
              },
              Result: {
                   Status: "Pass" //Pass, Fail, Error
              }
            }],
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 1
            }
          }),

          assertFailureCustom("ext_ccn2:1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Individual: {
                CustomsClientNumber: "22222222"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            ValidationCache: [{
              Name: "Validate_ValidCCN",
              Inputs: {
                Value: "22222222"
              },
              Result: {
                Status: "Fail", //Pass, Fail, Error
                Code: "322",
                Message: "Not a registered customs client number"
              }
            }],
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 1
            }
          }),

          // Should not call the external validation because of invalid CCN format
          assertSuccessCustom("ext_ccn2:1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Individual: {
                CustomsClientNumber: "Invalid CCN format"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 0
            }
          }),

          // Should not call the external validation because of invalid CCN format
          assertSuccessCustom("ext_ccn2:1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Individual: {
                CustomsClientNumber: ""
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 0
            }
          }),

          assertSuccessCustom("ext_ccn2:1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Individual: {
                CustomsClientNumber: "11111111"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          assertSuccessCustom("ext_ccn2:1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Individual: {
                CustomsClientNumber: "11111111"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            ValidationCache: [{
              Name: "Validate_ValidCCN",
              Inputs: {
                Value: "11111111"
              },
              Result: {
                   Status: "Pass" //Pass, Fail, Error
              }
            }],
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 1
            }
          }),

          assertFailureCustom("ext_ccn2:1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Individual: {
                CustomsClientNumber: "33333333"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),
          assertFailureCustom("ext_ccn2:1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Individual: {
                CustomsClientNumber: "44444444"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          // Resident.Entity.CustomsClientNumber
          // Not a registered import undertaking client (the Flow is OUT and category is 102/01 to 102/10 or 104/01 to 104/10 is used)
          assertSuccessCustom("ext_ccn2:2", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Entity: {
                CustomsClientNumber: "22222222"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          assertSuccessCustom("ext_ccn2:2", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Entity: {
                CustomsClientNumber: "22222222"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            ValidationCache: [{
              Name: "Validate_ValidCCN",
              Inputs: {
                Value: "22222222"
              },
              Result: {
                   Status: "Pass" //Pass, Fail, Error
              }
            }],
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 1
            }
          }),

          assertFailureCustom("ext_ccn2:2", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Entity: {
                CustomsClientNumber: "22222222"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            ValidationCache: [{
              Name: "Validate_ValidCCN",
              Inputs: {
                Value: "22222222"
              },
              Result: {
                Status: "Fail", //Pass, Fail, Error
                Code: "323",
                Message: "Not a registered import undertaking client"
              }
            }],
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 1
            }
          }),

          // Should not call the external validation because of invalid CCN format
          assertSuccessCustom("ext_ccn2:2", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Entity: {
                CustomsClientNumber: "Invalid CCN format"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 0
            }
          }),

          // Should not call the external validation because of invalid CCN format
          assertSuccessCustom("ext_ccn2:2", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Entity: {
                CustomsClientNumber: ""
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 0
            }
          }),

          assertSuccessCustom("ext_ccn2:2", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Entity: {
                CustomsClientNumber: "11111111"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          assertSuccessCustom("ext_ccn2:2", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Entity: {
                CustomsClientNumber: "11111111"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            ValidationCache: [{
              Name: "Validate_ValidCCN",
              Inputs: {
                Value: "11111111"
              },
              Result: {
                   Status: "Pass" //Pass, Fail, Error
              }
            }],
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 1
            }
          }),

          assertFailureCustom("ext_ccn2:2", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Entity: {
                CustomsClientNumber: "33333333"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),
          assertFailureCustom("ext_ccn2:2", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {
              Entity: {
                CustomsClientNumber: "44444444"
              }
            },
            MonetaryAmount: [{CategoryCode: '102', CategorySubCode: '02'}]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          // ReplacementOriginalReference
          // Transaction reference of cancelled transaction not stored on SARB database"
          assertSuccessCustom("ext_repot1", {
            ReportingQualifier: 'BOPCUS',
            TrnReference: 'NewTrnRef',
            ReplacementOriginalReference: 'ExistingINTrnCancelled',
            ReplacementTransaction: 'Y'
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),
          assertFailureCustom("ext_repot1", {
            ReportingQualifier: 'BOPCUS',
            TrnReference: 'NewTrnRef',
            ReplacementOriginalReference: 'MissingTrn',
            ReplacementTransaction: 'Y'
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),
          assertSuccessCustom("ext_repot1", {
            ReportingQualifier: 'BOPCUS',
            TrnReference: 'NewTrnRef',
            ReplacementOriginalReference: 'MissingTrn',
            ReplacementTransaction: 'Y'
          },
          {
            ValidationCache: [{
              Name: "Validate_ReplacementTrnReference",
              Inputs: {
                Value: "MissingTrn",
                Other: [ "NewTrnRef", "Y", "MissingTrn" ]
              },
              Result: {
                   Status: "Pass" //Pass, Fail, Error
              }
            }],
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 1
            }
          }),

          // ReversalTrnRefNumber
          //410, "Original transaction and SequenceNumber combination, with an opposite flow, not stored on database"
          //411, "Incorrect reversal category used with original transaction category"
          assertSuccessCustom("ext_mrtrn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [
              {
                CategoryCode: '100',
                ReversalTrnRefNumber: 'ExistingIN101Trn',
                ReversalTrnSeqNumber: '1'
              }
            ]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          assertFailureCustom("ext_mrtrn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [
              {
                CategoryCode: '100',
                ReversalTrnRefNumber: 'MissingTrn',
                ReversalTrnSeqNumber: '1'
              }
            ]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0,
              Code: '410'
            }
          }),

          assertFailureCustom("ext_mrtrn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [
              {
                CategoryCode: '100',
                ReversalTrnRefNumber: 'ExistingOUT101Trn',
                ReversalTrnSeqNumber: '1'
              }
            ]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0,
              Code: '410'
            }
          }),

          // Resident.Individual.IDNumber
          // DAL, You have exceeded the discretionary amount limit of R% (for % used during %)
          assertSuccessCustom("ext_mlval1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            TrnReference: 'TenThousandUnderLimit',
            ValueDate: '2020-12-25',
            Resident: { Individual: { IDNumber: '6811035039084' }},
            MonetaryAmount: [
              {
                DomesticValue: '1000',
                AdHocRequirement: { Subject: "SDA" }
              }
            ]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          assertSuccessCustom("ext_mlval1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            TrnReference: 'TenThousandUnderLimit',
            ValueDate: '2020-12-25',
            Resident: { Individual: { IDNumber: '6811035039084' }},
            MonetaryAmount: [
              {
                DomesticValue: '1000'
              }
            ]
          },
          {
            Assert: {
              ValidationCachePuts: 0,
              ValidationCacheHits: 0
            }
          }),

          assertFailureCustom("ext_mlval1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            TrnReference: 'JustUnderLimit',
            ValueDate: '2020-12-25',
            Resident: { Individual: { IDNumber: '6811035039084' }},
            MonetaryAmount: [
              {
                DomesticValue: '1000',
                AdHocRequirement: { Subject: "SDA" }
              }
            ]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          assertFailureCustom("ext_mlval1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            TrnReference: 'JustUnderLimit',
            ValueDate: '2020-12-25',
            Resident: { Individual: { IDNumber: '6811035039084' }},
            MonetaryAmount: [
              {
                DomesticValue: '0.01',
                AdHocRequirement: { Subject: "SDA" }
              },
              {
                DomesticValue: '13,900.00',
                AdHocRequirement: { Subject: "SDA" }
              }
            ]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          assertSuccessCustom("ext_mlval1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            TrnReference: 'TenThousandUnderLimit',
            ValueDate: '2020-12-25',
            Resident: { Individual: { IDNumber: '6811035039084' }},
            MonetaryAmount: [
              {
                DomesticValue: '500',
                AdHocRequirement: { Subject: "SDA" }
               },
              {
                DomesticValue: '500',
                AdHocRequirement: { Subject: "SDA" }
              },
              {
                DomesticValue: '500',
                AdHocRequirement: { Subject: "SDA" }
              }
            ]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          }),

          assertFailureCustom("ext_mlval1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            TrnReference: 'TenThousandUnderLimit',
            ValueDate: '2020-12-25',
            Resident: { Individual: { IDNumber: '6811035039084' }},
            MonetaryAmount: [
              {
                DomesticValue: '20000',
                AdHocRequirement: { Subject: "SDA" }
              }
            ]
          },
          {
            Assert: {
              ValidationCachePuts: 1,
              ValidationCacheHits: 0
            }
          })

        ]
      }
    return testBase;
  }
})
