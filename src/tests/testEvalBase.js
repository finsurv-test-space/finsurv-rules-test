define(function (require) {
  var rules = require("../coreRules");
  var fs = require("fs");
  var evaluator = rules.evaluator;

  //From: http://stackoverflow.com/questions/1068834/object-comparison-in-javascript
  // this works for 'simple' objects - enough for our purposes...
  function equals(x, y) {
    if (x === y) return true;
    if (!(x instanceof Object) || !(y instanceof Object)) return false;
    if (x.constructor !== y.constructor) return false;
    for (var p in x) {
      if (!x.hasOwnProperty(p)) continue;
      if (!y.hasOwnProperty(p)) return false;
      if (x[p] === y[p]) continue;
      if (typeof x[p] !== "object") return false;
      if (!Object.equals(x[p], y[p])) return false;
    }
    for (p in y) {
      if (y.hasOwnProperty(p) && !x.hasOwnProperty(p)) return false;
    }
    return true;
  }

  var addDuplicateRow = function (drResStatus, drAccType, crResStatus, crAccType, notes) {
    console.log(drResStatus, drAccType, crResStatus, crAccType, notes);
  };

  function red(s) {
    return '\033[31m' + s + '\033[0m';
  }

  function green(s) {
    return '\033[32m' + s + '\033[0m';
  }

  var addRow = function () {

    var args = Array.prototype.slice.apply(arguments);

    // var text = args.shift();

    // if(args[0]!=='passed'){
    //   args.unshift('color:red;');

    // } else {
    //   args.unshift('color:green;');
    // }
    // args.unshift('%c'+text);
    /*
    if(args[1]!=='passed'){
      args[0] = red(args[0]);
    } else {
      args[0] = green(args[0]);
    }

    console.log.apply(undefined,args);
    */

    if(args.slice(-1)[0] != 'passed'){
      args[0] = red(args[0]);
    }
    if(_export.printToConsole){
      console.log.apply(undefined, args);
    }
    fs.appendFileSync(_export.resultDetailLog, JSON.stringify(args)+"\n");
  };

  var compareProperties = function (expected_obj, got_obj, property) {
    var isSame = true;
    var note;
    var exp_value = undefined;
    var got_value = undefined;

    if (typeof expected_obj[property] !== "undefined")
      exp_value = expected_obj[property];
    if (typeof got_obj[property] !== "undefined") got_value = got_obj[property];
    if (exp_value != undefined && got_value != undefined) {
      if (exp_value != got_value) {
        isSame = false;
        note =
          property +
          ": expecting " +
          exp_value +
          ", but instead got " +
          got_value;
      }
    } else {
      if (exp_value != undefined && got_value == undefined) {
        isSame = false;
        note = property + ": expecting " + exp_value + ", but got nothing";
      }
      if (exp_value == undefined && got_value != undefined) {
        isSame = false;
        note = property + ": expecting nothing, but got " + got_value;
      }
    }
    return { isSame: isSame, note: note };
  };

  var compareDecision = function (test, res, testDecision) {
    var failed = false;
    var failedReason = "Failed Test: " + test;

    var comp = compareProperties(testDecision, res, "decision");
    failed = failed || !comp.isSame;
    failedReason += !comp.isSame ? "; " + comp.note : "";

    if(res.descision == "ReportToRegulator"){

      comp = compareProperties(testDecision, res, "flow");
      failed = failed || !comp.isSame;
      failedReason += !comp.isSame ? "; " + comp.note : "";
      comp = compareProperties(testDecision, res, "reportingSide");
      failed = failed || !comp.isSame;
      failedReason += !comp.isSame ? "; " + comp.note : "";
      comp = compareProperties(testDecision, res, "nonResSide");
      failed = failed || !comp.isSame;
      failedReason += !comp.isSame ? "; " + comp.note : "";
      comp = compareProperties(testDecision, res, "nonResAccountType");
      failed = failed || !comp.isSame;
      failedReason += !comp.isSame ? "; " + comp.note : "";
      comp = compareProperties(testDecision, res, "nonResException");
      failed = failed || !comp.isSame;
      failedReason += !comp.isSame ? "; " + comp.note : "";
      comp = compareProperties(testDecision, res, "resSide");
      failed = failed || !comp.isSame;
      failedReason += !comp.isSame ? "; " + comp.note : "";
      comp = compareProperties(testDecision, res, "resAccountType");
      failed = failed || !comp.isSame;
      failedReason += !comp.isSame ? "; " + comp.note : "";
      comp = compareProperties(testDecision, res, "resException");
      failed = failed || !comp.isSame;
      failedReason += !comp.isSame ? "; " + comp.note : "";
      // comp = compareProperties(testDecision, res, "reportingQualifier");
      // failed = failed || !comp.isSame;
      // failedReason += !comp.isSame ? "; " + comp.note : "";
    }


    return { failed: failed, failedReason: failed ? failedReason : "passed" };
  };

  function assertBase(test, drResStatus, drAccType, drAccStatus, crResStatus, crAccType, crAccStatus, testDecisions) {
    var res = evaluator.evaluateCustomerOnUsFiltered(drResStatus, drAccType, drAccStatus, crResStatus, crAccType, crAccStatus);

    if (res.length == testDecisions.length) {
      var comps = testDecisions.map(function (testDecision, ind) {
        return compareDecision(test, res[ind], testDecision)
      });
      if(comps.find(function(comp){return comp.failed})){
        _export.errorCount++;
        addRow(test, comps.map(function(comp,ind){
          return ind+': '+comp.failedReason;
        }).join(' & '));
      } else {
        addRow(test, "passed");
      }
    } else {
      _export.errorCount++;
      addRow(test, "Expecting " + testDecisions.length + " results!");
    }
  }

  var _export = {
    resultDetailLog: "",
    printToConsole: false,

    errorCount: 0,
    testCount: 0,
    evaluator: evaluator,
    setup: evaluator.setup,
    rep: rules.evaluationEx.rep,
    flowDir: rules.evaluationEx.flowDir,
    at: rules.evaluationEx.at,
    drcr: rules.evaluationEx.drcr,
    resStatus: rules.evaluationEx.resStatus,
    accType: rules.evaluationEx.accType,
    cpStatus: rules.evaluationEx.cpStatus,
    accStatus: rules.evaluationEx.accStatus,
    dec : rules.evaluationEx.descision,

    // rules methods
    loadLookups: rules.loadLookups,
    loadRulePack: rules.loadRulePack,
    loadRulesPackage: rules.loadRulesPackage,
    setRulesRepoPath: function (rulesRepoPath) {
      rules.rulesRepoPath = rulesRepoPath;
    },

    setLogName: function(logName1){ // get the path and name of the log files created in testChannelRulesNode.js
      _export.resultDetailLog = logName1;
    },

    setPrintConsole: function(decision){ // set flag to print or not to print to console
      _export.printToConsole = decision;
    },

    addParagraph: function (text) {
      var output = "";

      if (typeof document !== "undefined") {
        var objTo = document.getElementById("results");
        var para = document.createElement("p");
        para.innerHTML = text;
        objTo.appendChild(para);
      } else {
        if (text.indexOf("Fail") == -1) {
          output = text;
        } else {
          //output = "%c" + text, "color:red;";
          output = red(text)
        }

        if(_export.printToConsole){
          console.log(output);
        }
        fs.appendFileSync(_export.resultDetailLog, output+"\n");
      }

      // var objTo = document.getElementById("results");
      // var para = document.createElement("p");
      // para.innerHTML = text;
      // objTo.appendChild(para);
    },

    printDuplicates: function () {
      // Report Duplicate Rules - This can hide some subtle configuration issues
      for (var rule, i = 0; (rule = duplicateRules[i]); i++) {
        addDuplicateRow(rule.drResStatus, rule.drAccType, rule.crResStatus, rule.crAccType, rule.duplicateNote);
      }
      if (duplicateRules.length > 0) {
        if (duplicateRules.length == 1)
          _export.addParagraph(
            "There is 1 duplicated evaluation rule. Please investigate!"
          );
        else
          _export.addParagraph(
            "There are " +
            duplicateRules.length +
            " duplicated evaluation rules. Please investigate!"
          );
      }
    },

    assertOnUs: function (test, drResStatus, drAccType, crResStatus, crAccType, testDecision) {
      _export.testCount++;
      assertBase(test, drResStatus, drAccType, null, crResStatus, crAccType, null, [testDecision]);
    },
    assertOnUsIdv: function (test, drResStatus, drAccType, crResStatus, crAccType, testDecision) {
      _export.testCount++;
      assertBase(test, drResStatus, drAccType, _export.accStatus.Individual, crResStatus, crAccType, null, [testDecision]);
    },
    assertOnUsEnt: function (test, drResStatus, drAccType, crResStatus, crAccType, testDecision) {
      _export.testCount++;
      assertBase(test, drResStatus, drAccType, _export.accStatus.Entity, crResStatus, crAccType, null, [testDecision]);
    },

    assertOnUs2Results: function (test, drResStatus, drAccType, crResStatus, crAccType, testDecision1, testDecision2) {
      _export.testCount++;
      assertBase(test, drResStatus, drAccType, null, crResStatus, crAccType, null, [testDecision1, testDecision2]);
    },

    assert2Results: function (test, drResStatus, drAccType, crResStatus, crAccType, testDecision1, testDecision2) {
      _export.testCount++;
      assertBase(test, drResStatus, drAccType, null, crResStatus, crAccType, null, [testDecision1, testDecision2]);
    },

    assertUnknownCrSide: function (testName, drResStatus, drAccType, counterpartyStatus, destinationCurrency, accountHolderStatus, expectedResult) {
      _export.testCount++;

      var result = evaluator.evaluateUnknownCrSide(drResStatus, drAccType, counterpartyStatus, destinationCurrency, accountHolderStatus);

      if (!equals(result, expectedResult)) {
        _export.errorCount++;
        addRow(testName, drResStatus, drAccType, counterpartyStatus, destinationCurrency, accountHolderStatus, result,
          "Failure, does not match :" + JSON.stringify(expectedResult)
        );
      } else {
        addRow(testName, drResStatus, drAccType, counterpartyStatus,
          destinationCurrency, accountHolderStatus, result,
          "passed"
        );
      }
    },
    assertUnknownDrSide: function (testName, crResStatus, crAccType, counterpartyStatus, sourceCurrency, accountHolderStatus, field72, expectedResult) {
      _export.testCount++;

      var result = evaluator.evaluateUnknownDrSide(crResStatus, crAccType, counterpartyStatus, sourceCurrency, accountHolderStatus, field72);

      if (!equals(result, expectedResult)) {
        _export.errorCount++;
        addRow(testName, crResStatus, crAccType, counterpartyStatus, sourceCurrency,
          accountHolderStatus, field72, result,
          "Failure, does not match :" + JSON.stringify(expectedResult)
        );
      } else {
        addRow(testName, crResStatus, crAccType, counterpartyStatus,
          sourceCurrency, accountHolderStatus, field72, result,
          "passed"
        );
      }
    },
    assertAssumption: function (testName, drBankBIC, drResStatus, drAccType, drCurrency, drField72, crBankBIC, crResStatus, crAccType, crCurrency, expectedResult) {
      return _assertAssumption(testName, drBankBIC, drResStatus, drAccType, drCurrency, drField72, null, crBankBIC, crResStatus, crAccType, crCurrency, null, expectedResult)
    },
    assertAssumptionIdv: function(testName, drBankBIC, drResStatus, drAccType, drCurrency, drField72, crBankBIC, crResStatus, crAccType, crCurrency, expectedResult){
      return _assertAssumption(testName, drBankBIC, drResStatus, drAccType, drCurrency, drField72, {AccountHolderStatus:"Individual"}, crBankBIC, crResStatus, crAccType, crCurrency, {AccountHolderStatus:"Individual"}, expectedResult)
   
    },
    assertAssumptionEnt: function(testName, drBankBIC, drResStatus, drAccType, drCurrency, drField72, crBankBIC, crResStatus, crAccType, crCurrency, expectedResult){
      return _assertAssumption(testName, drBankBIC, drResStatus, drAccType, drCurrency, drField72, {AccountHolderStatus:"Entity"}, crBankBIC, crResStatus, crAccType, crCurrency, {AccountHolderStatus:"Entity"}, expectedResult)
        
    },
    assertAssumptionAdditionalParams: function (testName, drBankBIC, drResStatus, drAccType, drCurrency, drField72, drAdditionalParams, crBankBIC, crResStatus, crAccType, crCurrency, crAdditionalParams, expectedResult) {
      return _assertAssumption(testName, drBankBIC, drResStatus, drAccType, drCurrency, drField72, drAdditionalParams, crBankBIC, crResStatus, crAccType, crCurrency, crAdditionalParams, expectedResult)
    }
  };

  var _assertAssumption = function (testName, drBankBIC, drResStatus, drAccType, drCurrency, drField72, drAdditionalParams, crBankBIC, crResStatus, crAccType, crCurrency, crAdditionalParams, expectedResult) {
    _export.testCount++;

    //var drAdditionalParams = { "AccountHolderStatus" : accountHolderStatus};
    //var crAdditionalParams = { "AccountHolderStatus" : accountHolderStatus};

    var result = evaluator.consolidatedEvaluation(drBankBIC, drResStatus, drAccType,
      drCurrency, drField72, drAdditionalParams ,
      crBankBIC, crResStatus, crAccType, crCurrency, crAdditionalParams, true, true);

      if(!result.decisions[0]){
        _export.errorCount++;
        addRow(testName, null, null, null, null,
          null, null, result,
          "Failure, no result :" + JSON.stringify(expectedResult)
        );
      }else {
        var _cmp = compareDecision(testName, result.decisions[0], expectedResult)
        if(_cmp.failed){
          _export.errorCount++;
          addRow(testName, null, null, null, null,
            null, null, result,
            "Failure, does not match :" + JSON.stringify(_cmp.failedReason) + "\n Decision returned:" + JSON.stringify(result.decisions[0])
          );

        }else{
          addRow(testName, null, null, null,
            null, null, null, result,
            "passed"
          );
        }
      } 
  }

  return _export;
});

// ['./evaluationEx', './reg_rule_repo/coreSARB/evaluation/evalRules', './reg_rule_repo/coreSARB/evaluation/evalAssumptions'], function (evaluationEx, evalRules, evalAssumptions) {
// var evaluator = evaluationEx.evaluator();
// var duplicateRules = evaluator.setup(evalRules, evalAssumptions, new Date(2015, 0, 1));
