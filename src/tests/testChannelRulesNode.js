var fs = require("fs");
var path = require("path");

/* get datetime used as log path timestamp*/
var today = new Date();
var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
var dateTime = date+"-"+time;

/* create ./logs/timestamp/ path, create inner directory non-recursively for Windows compatibility*/
var logDir = `./logs/${dateTime}`;
if(!(fs.existsSync("./logs"))){
  fs.mkdirSync("./logs", {recursive: false}, err => {}); 
}
if(!(fs.existsSync(logDir))){
  fs.mkdir(`./logs/${dateTime}`, {recursive: false}, err => {});
}
var resultSumLog = `${logDir}/UnitTestResult_Summary.txt`;
var resultDetailLog = `${logDir}/UnitTestResult_Detail.txt`;

require("amdefine/intercept");

console.log("Unit Testing of the FINSURV rules ...");
console.log("");

/* Object to contain unit tests results*/
var testResults = {
  overall: {
    "overall": {
      "total": 0,
      "success": 0,
      "error": 0,
      "uncovered": 0
    },
    "validation": {
      "total": 0,
      "success": 0,
      "error": 0,
      "uncovered": 0
    }, 
    "evaluation": {
      "total": 0,
      "success": 0,
      "error": 0
    },
    "scenario": {
      "total": 0,
      "success": 0,
      "error": 0
    }
  },
  individual: {},
  lastUpdated: 'Unavailable',
}

function getChannels(srcpath) {
  return fs
    .readdirSync(path.resolve(__dirname, srcpath))
    .filter(file =>
      fs
        .statSync(path.resolve(__dirname, path.join(srcpath, file)))
        .isDirectory()
    );
}

var dbg = {};
var ruleBase = "../../reg_rule_repo/";
var testBase = require("./testBase");
var testEvalBase = require("./testEvalBase");

testBase.setLogName(resultDetailLog);
testEvalBase.setLogName(resultDetailLog);
testBase.setRulesRepoPath("../reg_rule_repo/");

var channels = getChannels(ruleBase);

var loadRulePackagePromise = function (channel) {
  return new Promise(function (resolve, reject) {
    testBase.loadRulesPackage(channel, pack => {
      var testLookupPath = path.resolve(__dirname, ruleBase, channel, "tests", "testLookups.js");
      if (fs.existsSync(testLookupPath)) {
        testBase.loadLookups(testLookupPath, () => {
          resolve(pack);
        });
      } else {
        resolve(pack);
      }
    });
  });
};

function doTests(channels) {
  if (!channels.length)
  {
    testResults.lastUpdated = dateTime;

    /* Write test results to JSON file after completed*/
    fs.writeFileSync(resultSumLog, JSON.stringify(testResults, null, 2));

    console.log("\nOverall Test Results:");
    console.log(testResults.overall);
    console.log("Successfully saved results to JSON file");
    return console.log(testResults.overall.overall.error);
  }

  var channel = channels.pop();
  testEvalBase.addParagraph("\n================================================================================\nLoading Channel: " + channel);

  loadRulePackagePromise(channel)

    /*-------------------------------------------------------------------------------------------
    *** evaluation tests (testEvalBase)
    -------------------------------------------------------------------------------------------*/
    .then(() => {
      var testEvalPath = path.resolve(__dirname, ruleBase, channel, "tests", "testEvalCases.js");
      if (fs.existsSync(testEvalPath)) {
        return require(testEvalPath);
      }
    })
    .then(testEvalCases => {
      testResults.individual[channel]=(testResults.individual[channel]||{});
      if (!testEvalCases) {
        testEvalBase.addParagraph(
          "NO EVALUATION TESTS FOR Channel: " + channel
        );
      } else {
        testEvalBase.addParagraph("Channel evaluation results for: " + channel);
        testEvalBase.errorCount = 0;
        testEvalBase.testCount = 0;
        testEvalBase.testRuleNames = [];

        _testCases = testEvalCases(testEvalBase);
        
        /* Evaluation test results*/
        testResults.individual[channel].evaluation = testResults.individual[channel].evaluation||{};

        /* Update individual evaluation test cases:*/
        testResults.individual[channel].evaluation.total = testEvalBase.testCount;
        testResults.individual[channel].evaluation.success = testEvalBase.testCount - testEvalBase.errorCount;
        testResults.individual[channel].evaluation.error = testEvalBase.errorCount;
        /* Update overall evaluation test cases:*/
        testResults.overall.evaluation.total += testEvalBase.testCount;
        testResults.overall.evaluation.success += testEvalBase.testCount - testEvalBase.errorCount;
        testResults.overall.evaluation.error += testEvalBase.errorCount;
        /* update summary*/
        testResults.overall.overall.total += testEvalBase.testCount;
        testResults.overall.overall.success += testEvalBase.testCount - testEvalBase.errorCount;
        testResults.overall.overall.error += testEvalBase.errorCount;

        if (testEvalBase.testCount == 0){
          testEvalBase.addParagraph(
            "WARNING: No evaluation tests found in file testEvalCases.js"
          );
        }
        else{
          if (testEvalBase.errorCount == 0){
            testEvalBase.addParagraph(
              "Completed " + testEvalBase.testCount + " evaluation tests Successfully :-)"
            );
          }
          else{
            if (testEvalBase.errorCount == 1){
              testEvalBase.addParagraph(
                "Ran " + testEvalBase.testCount + " evaluation tests with 1 error!"
              );
            }
            else{
              testEvalBase.addParagraph(
                "Ran " +
                testEvalBase.testCount +
                " evaluation tests with " +
                testEvalBase.errorCount +
                " errors!"
              );
            }
          }
        }
      }
    })

    /*-------------------------------------------------------------------------------------------
    *** scenario tests (testEvalBase)
    -------------------------------------------------------------------------------------------*/
    .then(() => {
      //setup evals
      // testBase.setup(pack.evals)
      //load the testCases
      var testPath = path.resolve(__dirname, ruleBase, channel, "tests", "testScenarioEvalCases.js");
      if (fs.existsSync(testPath)) {
        return require(testPath);
      }
    })
    .then(testScenarioCases => {
      /* Update individual scenarion test cases:*/
      testResults.individual[channel]=(testResults.individual[channel]||{});
      testResults.individual[channel].scenarioTests = true;
      if (!testScenarioCases) {
        testBase.addParagraph("NO SCENARIO TESTS FOR Channel: " + channel);
      } else {
        testEvalBase.addParagraph("Channel scenario results for: " + channel);
        testEvalBase.errorCount = 0;
        testEvalBase.testCount = 0;
        testEvalBase.testRuleNames = [];

        _testCases = testScenarioCases(testEvalBase);

        /* Scenario test results*/
        testResults.individual[channel].scenario = testResults.individual[channel].scenario||{};

        /* Update individual scenario test cases:*/
        testResults.individual[channel].scenario.total = testEvalBase.testCount;
        testResults.individual[channel].scenario.success = testEvalBase.testCount - testEvalBase.errorCount;
        testResults.individual[channel].scenario.error = testEvalBase.errorCount;
        /* Update overall scenario test cases:*/
        testResults.overall.scenario.total += testEvalBase.testCount;
        testResults.overall.scenario.success += testEvalBase.testCount - testEvalBase.errorCount;
        testResults.overall.scenario.error += testEvalBase.errorCount;
        /* update summary*/
        testResults.overall.overall.total += testEvalBase.testCount;
        testResults.overall.overall.success += testEvalBase.testCount - testEvalBase.errorCount;
        testResults.overall.overall.error += testEvalBase.errorCount;

        if (testEvalBase.testCount == 0){
          testEvalBase.addParagraph(
            "WARNING: No scenario tests found in file testScenarioEvalCases.js"
          );
        }
        else{
          if (testEvalBase.errorCount == 0){
            testEvalBase.addParagraph(
              "Completed " + testEvalBase.testCount + " scenario tests Successfully :-)"
            );
          }
          else{
            if (testEvalBase.errorCount == 1){
              testEvalBase.addParagraph(
                "Ran " + testEvalBase.testCount + " scenario tests with 1 error!"
              );
            }
            else{
              testEvalBase.addParagraph(
                "Ran " +
                testEvalBase.testCount +
                " scenario tests with " +
                testEvalBase.errorCount +
                " errors!"
              );
            }
          }
        }
      }
    })

    /*-------------------------------------------------------------------------------------------
    *** validation tests (testBase)
    -------------------------------------------------------------------------------------------*/
    .then(() => {
      //setup evals
      // testBase.setup(pack.evals)
      //load the testCases
      var testPath = path.resolve(__dirname, ruleBase, channel, "tests", "testCases.js");
      if (fs.existsSync(testPath)) {
        return require(testPath);
      }
    })
    .then(testCases => {
      testResults.individual[channel]=(testResults.individual[channel]||{});

      if (!testCases) {
        testBase.addParagraph("NO VALIDATION TESTS FOR Channel: " + channel);
      } else {
        //testBase.addParagraph("\n----------------------------------------");
        testBase.addParagraph("Channel validation results for: " + channel);
        //testBase.addParagraph("----------------------------------------");
        testBase.errorCount = 0;
        testBase.testCount = 0;
        testBase.testRuleNames = [];
        
        _testCases = testCases(testBase);
        
        /* Validation test results*/
        testResults.individual[channel].validation = testResults.individual[channel].validation||{};
        /* Update individual test results:*/
        testResults.individual[channel].validation.total = testBase.testCount;
        testResults.individual[channel].validation.success = testBase.testCount - testBase.errorCount;
        testResults.individual[channel].validation.error = testBase.errorCount;
        /* Update overall test cases:*/
        testResults.overall.validation.total += testBase.testCount;
        testResults.overall.validation.success += testBase.testCount - testBase.errorCount;
        testResults.overall.validation.error += testBase.errorCount;
        /* Update summary*/
        testResults.overall.overall.total += testBase.testCount;
        testResults.overall.overall.success += testBase.testCount - testBase.errorCount;
        testResults.overall.overall.error += testBase.errorCount;

        if (testBase.testCount == 0){
          testBase.addParagraph(
            "WARNING: No validation tests found in file testCases.js"
          );
        }
        else{
          if (testBase.errorCount == 0){
            testBase.addParagraph(
              "Completed " + testBase.testCount + " validation tests successfully :-)"
            ); 
          }
          else {
            if (testBase.errorCount == 1)
              testBase.addParagraph(
                "Ran " + testBase.testCount + " validation tests with 1 error!"
              );
            else
            {
              testBase.addParagraph(
                "Ran " +
                testBase.testCount +
                " validation tests with " +
                testBase.errorCount +
                " errors!"
              );
            }
          }
        }
      }

      return testBase.getRuleNamesForChannel(channel);
    })
    .then(function (channelRuleNames) {
      var reducedTestNames = testBase.testRuleNames.reduce(function (
        memo,
        ruleName
      ) {
        var nameOnly = ruleName.split(":")[0];
        if (memo.indexOf(nameOnly) == -1) {
          memo.push(nameOnly);
        }
        return memo;
      },
        []);

      var uncoveredRules = channelRuleNames.reduce(function (memo, item) {
        if (reducedTestNames.indexOf(item) == -1) memo.push(item);
        return memo;
      }, []);
      
      testBase.addParagraph("\n(" + uncoveredRules.length  + ") tests still uncovered: \n" + uncoveredRules);

       /* Update individual uncovered rules:*/
      testResults.individual[channel].validation = testResults.individual[channel].validation||{};
      testResults.individual[channel].validation.uncoveredRules = uncoveredRules.length;
      testResults.overall.validation.uncovered += uncoveredRules.length;
      testResults.overall.overall.uncovered += uncoveredRules.length;


      var testsNotFromHere = reducedTestNames.reduce(function (memo, item) {
        if (channelRuleNames.indexOf(item) == -1) memo.push(item);
        return memo;
      }, []); 
      testBase.addParagraph(
        "\n(" + testsNotFromHere.length + ") tests not from this channel: \n" + testsNotFromHere
      );
      testBase.addParagraph("--------------------------------------------------------------------------------");

      /* Update individual not from this channel:*/
      testResults.individual[channel].validation.testsNotFromHere = testsNotFromHere.length;

      dbg[channel] = {};
      dbg[channel].testCases = _testCases; /* recursively call all channels.*/
      /* recursively call all channels.*/
      doTests(channels);
    });
}

/* input argv[2]: channel*/
if(process.argv[2]){
  channels = channels.filter(channel=>channel==process.argv[2]);
  if(!channels.length){
    throw new Error('Channel name not found...');
  }
}

/* input argv[3]: console flag*/
if(process.argv[3]){
  testBase.setPrintConsole(process.argv[3]);
  testEvalBase.setPrintConsole(process.argv[3]);
}

doTests(channels);
