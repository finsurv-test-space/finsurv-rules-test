define(['./evaluationEx', './reg_rule_repo/coreSARB/evaluation/evalRules', './reg_rule_repo/coreSARB/evaluation/evalAssumptions'], function (evaluationEx, evalRules, evalAssumptions) {
  var evaluator = evaluationEx.evaluator();
  var duplicateRules = evaluator.setup(evalRules, evalAssumptions, new Date(2015, 0, 1));

  //From: http://stackoverflow.com/questions/1068834/object-comparison-in-javascript
  // this works for 'simple' objects - enough for our purposes...
  function equals( x, y ) {
    if ( x === y ) return true;
    if ( ! ( x instanceof Object ) || ! ( y instanceof Object ) ) return false;
    if ( x.constructor !== y.constructor ) return false;
    for ( var p in x ) {
      if ( ! x.hasOwnProperty( p ) ) continue;
      if ( ! y.hasOwnProperty( p ) ) return false;
      if ( x[ p ] === y[ p ] ) continue;
      if ( typeof( x[ p ] ) !== "object" ) return false;
      if ( ! Object.equals( x[ p ],  y[ p ] ) ) return false;
    }
    for ( p in y ) {
      if ( y.hasOwnProperty( p ) && ! x.hasOwnProperty( p ) ) return false;
    }
    return true;
  }

  var addDuplicateRow = function(drResStatus, drAccType, crResStatus, crAccType, notes) {
    var table = document.getElementById('duplicaterules');

    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);

    var cell = row.insertCell(0);
    cell.innerHTML = drResStatus;
    cell = row.insertCell(1);
    cell.innerHTML = drAccType;
    cell = row.insertCell(2);
    cell.innerHTML = crResStatus;
    cell = row.insertCell(3);
    cell.innerHTML = crAccType;
    cell = row.insertCell(4);
    cell.innerHTML = notes;
  }

  var addRow = function(test, drResStatus, drAccType, crResStatus, crAccType, decision, note) {
    var table = document.getElementById('rules');

    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);

    var cell = row.insertCell(0);
    cell.innerHTML = test;
    cell = row.insertCell(1);
    cell.innerHTML = drResStatus;
    cell = row.insertCell(2);
    cell.innerHTML = drAccType;
    cell = row.insertCell(3);
    cell.innerHTML = crResStatus;
    cell = row.insertCell(4);
    cell.innerHTML = crAccType;

    var decisions = decision;
    if (!Array.isArray(decisions)) {
      decisions = [];
      decisions.push(decision);
    }

    var cell5 = row.insertCell(5);
    var cell6 = row.insertCell(6);
    var cell7 = row.insertCell(7);
    var cell8 = row.insertCell(8);
    var cell9 = row.insertCell(9);
    var cell10 = row.insertCell(10);
    var cell11 = row.insertCell(11);
    var cell12 = row.insertCell(12);
    var cell13 = row.insertCell(13);
    var cell14 = row.insertCell(14);

    for (var i = 0, dec; dec = decisions[i]; i++) {
      cell5.innerHTML += dec.reportable;
      if (dec.flow !== undefined)
        cell6.innerHTML += dec.flow;
      else
        cell6.innerHTML += 'n/a';
      if (dec.reportingSide !== undefined)
        cell7.innerHTML += dec.reportingSide;
      else
        cell7.innerHTML += 'n/a';

      if (dec.nonResSide !== undefined)
        cell8.innerHTML += dec.nonResSide;
      else
        cell8.innerHTML += 'n/a';
      if (dec.nonResAccountType !== undefined)
        cell9.innerHTML += dec.nonResAccountType;
      else
        cell9.innerHTML += 'n/a';
      if (dec.nonResException !== undefined)
        cell10.innerHTML += dec.nonResException;
      else
        cell10.innerHTML += 'n/a';
      if (dec.resSide !== undefined)
        cell11.innerHTML += dec.resSide;
      else
        cell11.innerHTML += 'n/a';
      if (dec.resAccountType !== undefined)
        cell12.innerHTML += dec.resAccountType;
      else
        cell12.innerHTML += 'n/a';
      if (dec.resException !== undefined)
        cell13.innerHTML += dec.resException;
      else
        cell13.innerHTML += 'n/a';

      if (i < decisions.length-1) {
        cell5.innerHTML += ' | ';
        cell6.innerHTML += ' | ';
        cell7.innerHTML += ' | ';
        cell8.innerHTML += ' | ';
        cell9.innerHTML += ' | ';
        cell10.innerHTML += ' | ';
        cell11.innerHTML += ' | ';
        cell12.innerHTML += ' | ';
        cell13.innerHTML += ' | ';
        cell14.innerHTML += ' | ';
      }
    }
    cell14.innerHTML = note;
  }

  var compareProperties = function(expected_obj, got_obj, property) {
    var isSame = true;
    var note;
    var exp_value = undefined;
    var got_value = undefined;

    if (typeof (expected_obj[property]) !== 'undefined')
      exp_value = expected_obj[property];
    if (typeof (got_obj[property]) !== 'undefined')
      got_value = got_obj[property];
    if (exp_value != undefined && got_value != undefined) {
      if (exp_value != got_value) {
        isSame = false;
        note = property + ': expecting ' + exp_value + ', but instead got ' + got_value;
      }
    }
    else {
      if (exp_value != undefined && got_value == undefined) {
        isSame = false;
        note = property + ': expecting ' + exp_value + ', but got nothing';
      }
      if (exp_value == undefined && got_value != undefined) {
        isSame = false;
        note = property + ': expecting nothing, but got ' + got_value;
      }
    }
    return {isSame: isSame, note: note};
  }

  var compareDecision = function(test, res, testDecision) {
    var failed = false;
    var failedReason = 'Failed Test: ' + test;

    var comp = compareProperties(testDecision, res, 'reportable');
    failed = failed || !comp.isSame;
    failedReason += (!comp.isSame ? '; ' + comp.note : '');
    comp = compareProperties(testDecision, res, 'flow');
    failed = failed || !comp.isSame;
    failedReason += (!comp.isSame ? '; ' + comp.note : '');
    comp = compareProperties(testDecision, res, 'reportingSide');
    failed = failed || !comp.isSame;
    failedReason += (!comp.isSame ? '; ' + comp.note : '');
    comp = compareProperties(testDecision, res, 'nonResSide');
    failed = failed || !comp.isSame;
    failedReason += (!comp.isSame ? '; ' + comp.note : '');
    comp = compareProperties(testDecision, res, 'nonResAccountType');
    failed = failed || !comp.isSame;
    failedReason += (!comp.isSame ? '; ' + comp.note : '');
    comp = compareProperties(testDecision, res, 'nonResException');
    failed = failed || !comp.isSame;
    failedReason += (!comp.isSame ? '; ' + comp.note : '');
    comp = compareProperties(testDecision, res, 'resSide');
    failed = failed || !comp.isSame;
    failedReason += (!comp.isSame ? '; ' + comp.note : '');
    comp = compareProperties(testDecision, res, 'resAccountType');
    failed = failed || !comp.isSame;
    failedReason += (!comp.isSame ? '; ' + comp.note : '');
    comp = compareProperties(testDecision, res, 'resException');
    failed = failed || !comp.isSame;
    failedReason += (!comp.isSame ? '; ' + comp.note : '');

    return {failed: failed, failedReason: failed ? failedReason : 'passed'};
  }

  var _export = {

    errorCount   : 0,
    testCount    : 0,
    evaluator    : evaluator,
    rep          : evaluationEx.rep,
    flowDir      : evaluationEx.flowDir,
    at           : evaluationEx.at,
    drcr         : evaluationEx.drcr,
    resStatus    : evaluationEx.resStatus,
    accType      : evaluationEx.accType,
    cpStatus      : evaluationEx.cpStatus,
    accStatus      : evaluationEx.accStatus,

    addParagraph: function(text) {
      var objTo = document.getElementById("results");
      var para = document.createElement("p");
      para.innerHTML = text;
      objTo.appendChild(para)
    },

    printDuplicates: function() {
      // Report Duplicate Rules - This can hide some subtle configuration issues
      for (var rule, i = 0; rule = duplicateRules[i]; i++) {
        addDuplicateRow(rule.drResStatus, rule.drAccType, rule.crResStatus, rule.crAccType, rule.duplicateNote);
      }
      if (duplicateRules.length > 0) {
        if (duplicateRules.length == 1)
          _export.addParagraph("There is 1 duplicated evaluation rule. Please investigate!");
        else
          _export.addParagraph("There are " + duplicateRules.length + " duplicated evaluation rules. Please investigate!");
      }
    },

    assert: function(test, drResStatus, drAccType, crResStatus, crAccType, testDecision) {
      _export.testCount++;

      var res = evaluator.evaluateRaw(drResStatus, drAccType, crResStatus, crAccType);

      var comp = compareDecision(test, res, testDecision);

      if (comp.failed) {
        _export.errorCount++;
        addRow(test, drResStatus, drAccType, crResStatus, crAccType, res, comp.failedReason);
      }
      else {
        addRow(test, drResStatus, drAccType, crResStatus, crAccType, res, 'passed');
      }
    },

    assert2Results: function(test, drResStatus, drAccType, crResStatus, crAccType, testDecision1, testDecision2) {
      _export.testCount++;

      var res = evaluator.evaluateRaw(drResStatus, drAccType, crResStatus, crAccType);

      if (res.length == 2) {
        var comp1 = compareDecision(test, res[0], testDecision1);
        var comp2 = compareDecision(test, res[1], testDecision2);

        if (comp1.failed || comp2.failed) {
          _export.errorCount++;
          addRow(test, drResStatus, drAccType, crResStatus, crAccType, res, '1: ' + comp1.failedReason + ' & 2:' + comp2.failedReason);
        }
        else {
          addRow(test, drResStatus, drAccType, crResStatus, crAccType, res, 'passed');
        }
      }
      else {
        addRow(test, drResStatus, drAccType, crResStatus, crAccType, res, 'Expecting 2 results!');
      }
    },
    assertUnknownCrSide : function(testName, drResStatus, drAccType, counterpartyStatus, destinationCurrency, accountHolderStatus, expectedResult ){
      _export.testCount++;

      var result = evaluator.evaluateUnknownCrSide(drResStatus, drAccType, counterpartyStatus, destinationCurrency, accountHolderStatus);

      if(!equals(result,expectedResult)){
        _export.errorCount++;
        addRow(testName, drResStatus, drAccType, counterpartyStatus, destinationCurrency, accountHolderStatus, result, 'Failure, does not match :'+JSON.stringify(expectedResult));
      }
      else {
        addRow(testName, drResStatus, drAccType, counterpartyStatus, destinationCurrency, accountHolderStatus, result, 'passed');
      }
    },
    assertUnknownDrSide : function(testName, crResStatus,  crAccType,
                                   counterpartyStatus,  sourceCurrency,
                                   accountHolderStatus,
                                   field72, expectedResult ){
      _export.testCount++;

      var result = evaluator.evaluateUnknownDrSide(crResStatus,  crAccType,
        counterpartyStatus,  sourceCurrency,
        accountHolderStatus,
        field72)

      if(!equals(result,expectedResult)){
        _export.errorCount++;
        addRow(testName, crResStatus,  crAccType,
          counterpartyStatus,  sourceCurrency,
          accountHolderStatus,
          field72, result, 'Failure, does not match :'+JSON.stringify(expectedResult));
      }
      else {
        addRow(testName, crResStatus,  crAccType,
          counterpartyStatus,  sourceCurrency,
          accountHolderStatus,
          field72, result, 'passed');
      }
    }

  };


  return _export;

});