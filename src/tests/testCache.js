require('amdefine/intercept');

console.log("Unit Testing of the FINSURV Cache functionality ...");
console.log("");

var dbg = {};


const assert = require('assert').strict;
var ctx = require("./../validation/contextWrapper");

function sleepCode(millis) {
  var date = Date.now();
  var curDate = null;
  do {
    curDate = Date.now();
  } while (curDate-date < millis);
}

const PASS = "Pass";
const FAIL = "Fail";
const ERROR = "ERROR";

const StatusType = ctx.StatusType;
const CachePeriod = ctx.CachePeriod;
const new_ValidationResult = ctx.new_ValidationResult;
const new_ValidationInputs = ctx.new_ValidationInputs;

var registry = ctx.wrapCallbacks();
var cs = ctx.new_CacheStrategy().
         set(StatusType.PASS, CachePeriod.LONG).
         set(StatusType.FAIL, CachePeriod.MEDIUM).
         set(StatusType.ERROR, CachePeriod.SHORT);

registry.registerValidationCallbackEx(PASS, cs, function() {return new_ValidationResult(StatusType.PASS);});
registry.registerValidationCallbackEx(FAIL, cs, function() {return new_ValidationResult(StatusType.FAIL, "001", "Failure Message");});
registry.registerValidationCallbackEx(ERROR, cs, function() {return new_ValidationResult(StatusType.ERROR, "002", "Error Message");});

var valCache = ctx.new_CustomValidateCache(registry);

valCache.setKeepLongMillis(1000);
valCache.setKeepMediumMillis(500);
valCache.setKeepShortMillis(100);

var basicInputs = new_ValidationInputs("MyValue", []);
var passResult = new_ValidationResult(StatusType.PASS);
var failResult = new_ValidationResult(StatusType.FAIL, "001", "Failure Message");
var errorResult = new_ValidationResult(StatusType.ERROR, "002", "Error Message");

valCache.cacheResult(PASS, basicInputs, passResult, null);
valCache.cacheResult(FAIL, basicInputs, failResult, null);
valCache.cacheResult(ERROR, basicInputs, errorResult, null);

var blastRadius = 1000;
// Blast the cache
for (var i=0; i<blastRadius; i++) {
  var inputs = [];
  inputs.push(i);
  var randomInputs = new_ValidationInputs( "Value#" + String(i), inputs);

  valCache.cacheResult(PASS, randomInputs, passResult, null);
  valCache.cacheResult(FAIL, randomInputs, failResult, null);
  valCache.cacheResult(ERROR, randomInputs, errorResult, null);
}

var startCacheCount = (valCache.getResult(PASS, basicInputs)) ? 1 : 0;
startCacheCount += (valCache.getResult(FAIL, basicInputs)) ? 1 : 0;
startCacheCount += (valCache.getResult(ERROR, basicInputs)) ? 1 : 0;

var cacheSize = valCache.size();
console.log("At start: Cache Size = " + String(valCache.size()));
console.log("At start: Sparseness Metric = " + String(valCache.sparsenessMetric()));
assert.equal(startCacheCount, 3, "Expecting to find 3 specific cached entries");
assert.equal(cacheSize, 3*blastRadius + 3, "At start: Expecting " + (3*blastRadius + 3) + " cached entries");

sleepCode(120);

valCache.expiredCleanup();
cacheSize = valCache.size();
console.log("After 120 ms: Cache Size = " + String(cacheSize));
assert.equal((valCache.getResult(PASS, basicInputs) ? true : false), true, "After 120 ms: Expecting PASS to be here");
assert.equal((valCache.getResult(FAIL, basicInputs) ? true : false), true, "After 120 ms: Expecting FAIL to be here");
assert.equal((valCache.getResult(ERROR, basicInputs) ? true : false), false, "After 120 ms: Expecting ERROR to be missing");
assert.equal(cacheSize, 2*blastRadius + 2, "After 120 ms: Expecting " + String(2*blastRadius + 2) + " cached entries");

sleepCode(400);

valCache.expiredCleanup();
cacheSize = valCache.size();
console.log("After 520 ms: Cache Size = " + String(cacheSize));
assert.equal((valCache.getResult(PASS, basicInputs) ? true : false), true, "After 520 ms: Expecting PASS to be here");
assert.equal((valCache.getResult(FAIL, basicInputs) ? true : false), false, "After 520 ms: Expecting FAIL to be missing");
assert.equal((valCache.getResult(ERROR, basicInputs) ? true : false), false, "After 520 ms: Expecting ERROR to be missing");
assert.equal(cacheSize, 1*blastRadius + 1, "After 520 ms: Expecting " + String(1*blastRadius + 1) + " cached entries");

sleepCode(500);

valCache.expiredCleanup();
cacheSize = valCache.size();
console.log("After 1020 ms: Cache Size = " + String(cacheSize));
assert.equal((valCache.getResult(PASS, basicInputs) ? true : false), false, "After 1020 ms: Expecting PASS to be missing");
assert.equal((valCache.getResult(FAIL, basicInputs) ? true : false), false, "After 1020 ms: Expecting FAIL to be missing");
assert.equal((valCache.getResult(ERROR, basicInputs) ? true : false), false, "After 1020 ms: Expecting ERROR to be missing");
assert.equal(cacheSize, 0, "After 1020 ms: Expecting 0 cached entries");



//*****************************************************************************************************************
// Validation checks
//*****************************************************************************************************************

var testBase = require("./testBase");

testBase.setPrintConsole(true);
testBase.setPrintTestCases(true);

var ccnInfoMap = {
  '11111111': {importUndertaking: false, validCCN: true},
  '22222222': {importUndertaking: true, validCCN: true},
  '33333333': {importUndertaking: false, validCCN: false},
  '44444444': {importUndertaking: false, validCCN: false}
};
var trnInfoMap = {
  'ExistingIN101Trn': {exists: true, cancelled: false, flow: 'IN', category: '101'},
  'ExistingOUT101Trn': {exists: true, cancelled: false, flow: 'OUT', category: '101'},
  'ExistingINTrnCancelled': {exists: true, cancelled: true, flow: 'IN'},
  'ExistingOUTTrnCancelled': {exists: true, cancelled: true, flow: 'OUT'},
  'MissingTrn': {exists: false}
};

function validate_ImportUndertakingCCN(value, resultCallback) {
  var ccn = String(value);
  if (ccn in ccnInfoMap) {
    var ccnInfo = ccnInfoMap[ccn];

    if (ccnInfo.validCCN) {
      if (ccnInfo.importUndertaking)
        return resultCallback(StatusType.PASS);
      else
        return resultCallback(StatusType.FAIL, "323", "Not a registered import undertaking client");
    }
    else {
      return resultCallback(StatusType.FAIL, "322", "This is an invalid customs client number");
    }
  }
  else {
    return resultCallback(StatusType.PASS);
  }
}

function validate_ValidCCN(value, resultCallback) {
  var ccn = String(value);
  if (ccn in ccnInfoMap) {
    var ccnInfo = ccnInfoMap[ccn];

    if (ccnInfo.validCCN)
      return resultCallback(StatusType.PASS);
    else
      return resultCallback(StatusType.FAIL, "322", "Not a registered customs client number");
  }
  else {
    return resultCallback(StatusType.PASS);
  }
}

function validate_ReversalTrnRef(value, flow, sequence, category, resultCallback) {
  var trnRef = String(value);

  // call tp find flow of old transaction with trnRef and sequence
  var oldFlow = null;
  var oldCategory = null;
  if (trnRef in trnInfoMap) {
    oldFlow = trnInfoMap[trnRef].flow;
    oldCategory = trnInfoMap[trnRef].category;
  }

  if (!oldFlow) {
    return resultCallback(StatusType.FAIL, "410", "Original transaction and SequenceNumber combination not stored on database");
  }
  else
  if (oldFlow == flow) {
    return resultCallback(StatusType.FAIL, "410", "Original transaction and SequenceNumber combination stored on database, but not with an opposite flow");
  }
  else
  if (category && category.length > 0 && oldCategory && oldCategory.length > 0 &&
          category.charAt(0) != oldCategory.charAt(0)) {
    // ErrorId: 2
    return resultCallback(StatusType.FAIL, "411", "Incorrect reversal category used with original transaction category. Original category is " + oldCategory);
  }
  else {
    return resultCallback(StatusType.PASS);
  }
}

function validate_ReplacementTrnReference(value, trnRef, replacementYN, replacementTrnRef, resultCallback) {
  if (replacementYN && replacementYN == "N") {
    if (trnRef in trnInfoMap) {
      var trnInfo = trnInfoMap[trnRef];

      if (trnInfo.exists && trnInfo.cancelled)
        return resultCallback(StatusType.FAIL, "389", "If ReplacementTransaction is N and the TrnReference is the same as a transaction previously cancelled, the ReplacementTransaction must be Y");
      else
        return resultCallback(StatusType.PASS);
    }
  }
  else
  if (replacementYN && replacementYN == "Y" && replacementTrnRef && replacementTrnRef.length > 0)
  {
    if (replacementTrnRef in trnInfoMap) {
      var trnInfo = trnInfoMap[replacementTrnRef];

      if (!trnInfo.exists || !trnInfo.cancelled)
        return resultCallback(StatusType.FAIL, "212", "Transaction reference of cancelled transaction not stored on SARB database");
      else
        return resultCallback(StatusType.PASS);
    }
  }
  return resultCallback(StatusType.PASS);
}

function validate_IndividualSDA(value, trnReference, valueDate, SDATotal, resultCallback) {
  var IDNumber = String(value);
  var SDALimit = 1000000.00;

  if (!SDATotal || SDATotal == "" || SDATotal == "0") {
    return resultCallback(StatusType.PASS);
  }

  var numSDATotal = parseFloat(SDATotal);
  var totalValueForYear = 0;
  if (trnReference == "OverTheLimit")
    totalValueForYear = SDALimit + 1.00;
  if (trnReference == "JustUnderLimit")
    totalValueForYear = SDALimit - 1.00;
  if (trnReference == "TenThousandUnderLimit")
    totalValueForYear = SDALimit - 10000.00;
  if(totalValueForYear + numSDATotal > SDALimit) {
    return resultCallback(StatusType.FAIL, "DAL", "You have exceeded the discretionary amount limit of R" +String(SDALimit) + " (for " + IDNumber + " used during 2020)");
  } else {
    return resultCallback(StatusType.PASS);
  }
}

var callbackProperties = {
  failOnBusy: true
}

testBase.setValidationCallbacks([
  ["Validate_ImportUndertakingCCN", validate_ImportUndertakingCCN],
  ["Validate_ValidCCN", validate_ValidCCN],
  ["Validate_ReversalTrnRef", validate_ReversalTrnRef,callbackProperties, "transaction::Flow", "ReversalTrnSeqNumber", "CategoryCode"],
  ["Validate_IndividualSDA", validate_ReversalTrnRef, "transaction::TrnReference", "transaction::ValueDate", "function::localAmountForIDNumber"],
  ["Validate_ReplacementTrnReference", validate_ReplacementTrnReference, "TrnReference", "ReplacementTransaction", "ReplacementOriginalReference"],
  ["Validate_IndividualSDA", validate_IndividualSDA, "transaction::TrnReference", "transaction::ValueDate", "function::localAmountForIDNumber"]
]);

testBase.setRulesRepoPath(__dirname + '/reg_rule_tests/');

var loadRulePackagePromise = function (path) {
    return new Promise(function (resolve, reject) {
        testBase.loadRulesPackage(path, resolve)
    })
};


var testCases = require("./reg_rule_tests/testExternal/tests/testCases");

loadRulePackagePromise("testExternal")
.then(function () {
    testBase.errorCount = 0;

    testCases(testBase);

    if (testBase.errorCount == 0)
        testBase.addParagraph("Completed " + testBase.testCount + " tests Successfully :-)");
    else {
        if (testBase.errorCount == 1)
            testBase.addParagraph("Ran " + testBase.testCount + " tests with 1 error!");
        else
            testBase.addParagraph("Ran " + testBase.testCount + " tests with " + testBase.errorCount + " errors!");
    }

    console.log("External tests finished");
    dbg.testCases = testCases;
})
