define(['testEvalBase'], function (testBase) {
  with (testBase) {
    var test_cases = [
      assertUnknownCrSide("Resident ZAR - Onshore ZAR", resStatus.RESIDENT, accType.RAND, cpStatus.ONSHORE, "ZAR", null,
        {reportable: rep.NONREPORTABLE}),
      assertUnknownCrSide("Resident ZAR - Offshore USD", resStatus.RESIDENT, accType.RAND, cpStatus.OFFSHORE, "USD", null,
        {
          reportable: rep.REPORTABLE, flow: flowDir.OUT, reportingSide: drcr.DR,
          nonResSide: drcr.CR, nonResAccountType: at.NR_OTH,
          resSide: drcr.DR, resAccountType: at.RE_OTH
        }),
      assertUnknownCrSide("Resident ZAR - Resident FCA", resStatus.RESIDENT, accType.RAND, cpStatus.ONSHORE, "USD", accStatus.Entity,
        {
          reportable: rep.ZZ1REPORTABLE, flow: flowDir.OUT, reportingSide: drcr.DR,
          nonResException: "FCA RESIDENT NON REPORTABLE",
          resSide: drcr.DR, resAccountType: at.RE_OTH
        }),
      assert2UnknownCrSide("Resident ZAR - Resident FCA", resStatus.RESIDENT, accType.RAND, cpStatus.ONSHORE, "USD", accStatus.Individual,
        {
          reportable: rep.REPORTABLE, flow: flowDir.OUT, reportingSide: drcr.DR,
          nonResSide: drcr.CR, nonResAccountType: at.RE_FCA,
          resSide: drcr.DR, resAccountType: at.RE_OTH
        },
        {
          reportable: rep.ZZ1REPORTABLE, flow: flowDir.OUT, reportingSide: drcr.DR,
          nonResException: "FCA RESIDENT NON REPORTABLE",
          resSide: drcr.DR, resAccountType: at.RE_OTH
        })
    ];
  }
  return testBase;
});

