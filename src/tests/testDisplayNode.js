/**
 * Created by nishaal on 2/1/16.
 */

require('amdefine/intercept');

console.log("##teamcity[testSuiteStarted name='displayTest']");
console.log("Unit Testing of the FINSURV display rules ...");
console.log("");

var dbg = {};
var testBase = require("./testBase");
var testCases = require("./testDisplay.js");

var loadRulePromise = function (name, path) {
    return new Promise(function (resolve, reject) {
        testBase.loadDisplayRuleSet(name, path, resolve)
    })
};
var loadLookupPromise = function (path) {
    return new Promise(function (resolve, reject) {
        testBase.loadLookups(path, resolve)
    })
};

Promise.all([
    loadRulePromise("detailDisplay","./reg_rule_repo/coreSARB/display/detailDisplay"),
    loadRulePromise("summaryDisplay","./reg_rule_repo/coreSARB/display/summaryDisplay"),
    loadRulePromise("lookupFilterRules","./reg_rule_repo/coreSARB/display/lookupFilterRules"),
    loadLookupPromise("./reg_rule_repo/testSARB/data/lookups"),
    loadLookupPromise("./reg_rule_repo/coreSARB/data/lookups")
]).then(function () {
    testBase.errorCount = 0;

    testCases(testBase);

    if (testBase.errorCount == 0)
        testBase.addParagraph("Completed " + testBase.testCount + " tests Successfully :-)");
    else {
        if (testBase.errorCount == 1)
            testBase.addParagraph("Ran " + testBase.testCount + " tests with 1 error!");
        else
            testBase.addParagraph("Ran " + testBase.testCount + " tests with " + testBase.errorCount + " errors!");
    }

    console.log("##teamcity[testSuiteFinished name='ruleTest']");
    dbg.testCases = testCases;
})

