require('amdefine/intercept');

console.log("##teamcity[testSuiteStarted name='ruleTest']");
console.log("Unit Testing of the FINSURV rules ...");
console.log("");

var dbg = {};
var testBase = require("./testBase");
var testCases = require("./testCases");

testBase.setRulesRepoPath('./reg_rule_repo/')

var loadRulePackagePromise = function (path) {
    return new Promise(function (resolve, reject) {
        testBase.loadRulesPackage(path, resolve)
    })
};

loadRulePackagePromise("testSARB")
.then(function () {
    testBase.errorCount = 0;

    testCases(testBase);

    if (testBase.errorCount == 0)
        testBase.addParagraph("Completed " + testBase.testCount + " tests Successfully :-)");
    else {
        if (testBase.errorCount == 1)
            testBase.addParagraph("Ran " + testBase.testCount + " tests with 1 error!");
        else
            testBase.addParagraph("Ran " + testBase.testCount + " tests with " + testBase.errorCount + " errors!");
    }

    console.log("##teamcity[testSuiteFinished name='ruleTest']");
    dbg.testCases = testCases;
})
