/**
 * Created by petruspretorius on 13/03/2015.
 */

define( function (require) {

  var rules = require("../coreRules"),
  testContext = require('./testContext'),
  getRuleFn;

  var displayRuleSets;
  var fs = require("fs");

  rules.setRuleParameters({
    // Set up rule parameters for the tests
    todayDate: new Date(2013, 0, 20),
    goLiveDate: new Date(2013, 7, 18)
  });

  function init() {
    displayRuleSets = {
      detail: [
        rules.detailDisplay.detailTrans,
        rules.detailDisplay.detailMoney,
        rules.detailDisplay.detailImportExport,
        rules.lookupFilterRules.filterLookupTrans,
        rules.lookupFilterRules.filterLookupMoney,
        rules.lookupFilterRules.filterLookupImportExport
      ],
      summary: [rules.summaryDisplay.summaryTrans, rules.summaryDisplay.summaryMoney, rules.summaryDisplay.summaryImportExport]
    }
  }

  var validationCallbacks = [];

  function read_ExtraAssertion(jsMeta) {
    var entry = {
      expectCachePuts: 0,
      expectCacheHits: 0,
      expectCode: null
    };

    var jsAssert = jsMeta["Assert"];
    if (jsAssert) {
      entry.expectCachePuts = jsAssert["ValidationCachePuts"];
      entry.expectCacheHits = jsAssert["ValidationCacheHits"];
      entry.expectCode = jsAssert["Code"];
    }
    else
      return null;

    return entry;
  }

  init();

  var assertRuleBase = function (rulename, obj, customData, expectType, number, mustMatchRule) {
    _export.writeTest(rulename, obj, customData, expectType, number, mustMatchRule);
    _export.testRuleNames.push(rulename);
    _export.testCount++;
    var c = testContext.produceTestContext(obj);
    var wc = rules.wrapContext(c, rules.lookups, customData);
    var validated = false;
    var finsurv_rulesets = [];
    if(rules.stdTrans) finsurv_rulesets.push(rules.stdTrans)
    if(rules.stdMoney) finsurv_rulesets.push(rules.stdMoney)
    if(rules.stdImportExport) finsurv_rulesets.push(rules.stdImportExport)

    if(['VALIDATE','NOVALIDATE'].indexOf(expectType)!=-1){
      var rule = rules.getValidationRule(wc, finsurv_rulesets, rulename);
      // quickly add a fake ext Validation
      if (rule[0] && rule[0].validate)
        validationCallbacks = [[rule[0].validate,
        function (val, cb) {
          cb('pass');
          validated = true;
        }]];
    }
    // Register Validation Callbacks
    validationCallbacks.forEach(function (validationCallbackParams) {
      wc.callbacks.registerValidationCallback.apply(undefined, validationCallbackParams);
    })

    if (_export.printTestCases) {
      _export.addParagraph('*******-----> Running Test: ' + rulename + '#' + _export.countRuleInstances(rulename));
      _export.addParagraph('Report: ' + JSON.stringify(obj, undefined, 2));
      if (customData) {
        _export.addParagraph('Initial Meta: ' + JSON.stringify(customData, undefined, 2));
      }
    }
    var extraAssertion = read_ExtraAssertion(customData);

    var rulesFound = rules.runValidation(wc, finsurv_rulesets, rulename);
      
    if (_export.printTestCases) {
      wc.marshalCacheToCustomData();
      if (customData) {
        _export.addParagraph('New Meta: ' + JSON.stringify(customData, undefined, 2));
      }
    }

    if (!rulesFound && mustMatchRule) {
      _export.errorCount++;
      _export.addParagraph('Failed Test: ' + rulename + '#' + _export.countRuleInstances(rulename) + ' - No matching rule found');
    }
    else {
      if (!expectType) {
        if (c.raisedEvent.length > 0) {
          _export.errorCount++;
          _export.addParagraph('Failed Test: ' + rulename + '#' + _export.countRuleInstances(rulename) + ' - Expecting Success, but raised ' + c.raisedEvent.length + ': ' + c.raisedEvent[0].type);
        }
        else {
          // Success so far... Now lets check the extra assertions
          if (extraAssertion) {
            if (extraAssertion.expectCachePuts != wc.getCachePutCount()) {
              _export.errorCount++;
              _export.addParagraph("Failed Test: " + rulename + '#' + String(_export.countRuleInstances(rulename)) +
                      " - Result Correct, expecting " + String(extraAssertion.expectCachePuts) +
                      " validation cache puts, but instead got " + String(wc.getCachePutCount()));
            }
            else
            if (extraAssertion.expectCacheHits != wc.getCacheHitCount()) {
              _export.errorCount++;
              _export.addParagraph("Failed Test: " + rulename + '#' + String(_export.countRuleInstances(rulename)) +
                      " - Result Correct, expecting " + String(extraAssertion.expectCacheHits) +
                      " validation cache hits, but instead got " + String(wc.getCacheHitCount()));
            }
          }
        }
      }
      else {
        if (['VALIDATE','NOVALIDATE'].indexOf(expectType)!=-1) {
          if (expectType=="VALIDATE"&&!validated) {
            _export.errorCount++;
            _export.addParagraph("Failed Test: " + rulename + "#" + _export.countRuleInstances(rulename) + " - Expecting " + expectType + ", but did not validate!");
          } else if(expectType=="NOVALIDATE"&&validated){
            _export.errorCount++;
            _export.addParagraph("Failed Test: " + rulename + "#" + _export.countRuleInstances(rulename) + " - Expecting " + expectType + ", but validated!");
          }
        } else if (c.raisedEvent.length == 0 || c.raisedEvent[0].type != expectType || c.raisedEvent.length != number) {
          _export.errorCount++;
          if (c.raisedEvent.length == 0) _export.addParagraph("Failed Test: " + rulename + "#" + _export.countRuleInstances(rulename) + " - Expecting " + expectType + ", but instead succeeded");
          else if (c.raisedEvent[0].type != expectType) _export.addParagraph("Failed Test: " + rulename + "#" + _export.countRuleInstances(rulename) + " - Expecting " + expectType + ", but instead raised: " + c.raisedEvent[0].type);
          else if (c.raisedEvent.length != number) _export.addParagraph("Failed Test: " + rulename + "#" + _export.countRuleInstances(rulename) + " - Expecting " + number + "x" + expectType + ", but only raised " + c.raisedEvent.length);
        }
        else {
          // Success so far... Now lets check the extra assertions
          var firstCode = null;
          if (c.raisedEvent.length > 0) {
            firstCode = c.raisedEvent[0].code;
          }
          if (extraAssertion) {
            if (extraAssertion.expectCachePuts != wc.getCachePutCount()) {
              _export.errorCount++;
              _export.addParagraph("Failed Test: " + rulename + '#' + String(_export.countRuleInstances(rulename)) +
                      " - Result Correct, expecting " + String(extraAssertion.expectCachePuts) +
                      " validation cache puts, but instead got " + String(wc.getCachePutCount()));
            }
            else
            if (extraAssertion.expectCacheHits != wc.getCacheHitCount()) {
              _export.errorCount++;
              _export.addParagraph("Failed Test: " + rulename + '#' + String(_export.countRuleInstances(rulename)) +
                      " - Result Correct, expecting " + String(extraAssertion.expectCacheHits) +
                      " validation cache hits, but instead got " + String(wc.getCacheHitCount()));
            }
            else
            if (extraAssertion.expectCode && extraAssertion.expectCode != firstCode) {
              _export.errorCount++;
              _export.addParagraph("Failed Test: " + rulename + '#' + String(_export.countRuleInstances(rulename)) +
                      " - Result Correct, expecting Code '" + String(extraAssertion.expectCode) +
                      "' , but instead got '" + firstCode + "'");
            }
          }
        }
      }
    }
  }


  function resultantValue(c) {
    var result;
    for (var event, i = 0; event = c.displayEvent[i]; i++) {
      if (event.type === "SET")
        result = event.value;
      if (event.type === "APP")
        result += ", " + event.value;
    }
    return result;
  }

  function resultantVisibility(c) {
    var result;
    for (var event, i = 0; event = c.displayEvent[i]; i++) {
      if (event.type === "HIDE" || event.type === "SHOW")
        result = event.type;
    }
    return result;
  }

  function resultantLimitExclude(c) {
    var result;
    for (var event, i = 0; event = c.displayEvent[i]; i++) {
      if (event.type === "LIMIT" || event.type === "EXCLUDE")
        result = event.type;
    }
    return result;
  }

  function assertDisplayRuleBase(displayset, field, obj, customData, expectType, expectedSetting, mustMatchRule) {
    _export.writeDisplayTest(displayset, field, obj, customData, expectType, expectedSetting, mustMatchRule);
    _export.testRuleNames.push(field);
    _export.testCount++;
    var c = testContext.produceTestContext(obj);
    var wc = rules.wrapContext(c, rules.lookups, customData);
    var rulesFound = rules.runDisplayRules(wc, displayRuleSets[displayset], field);
    if (!rulesFound && mustMatchRule) {
      _export.errorCount++;
      _export.addParagraph('Failed Test: ' + field + '#' + _export.countRuleInstances(field) + ' - No matching rule found');
    }
    else {
      if (!expectType) {
        if (c.displayEvent.length > 0) {
          _export.errorCount++;
          _export.addParagraph('Failed Test: ' + field + '#' + _export.countRuleInstances(field) + ' - Expecting No Event, but had ' + c.displayEvent.length + ' event(s)');
        }
      }
      else {
        var value = resultantValue(c);
        var visibility = resultantVisibility(c);
        var limitExclude = resultantLimitExclude(c);
        if (c.displayEvent.length == 0 || (expectType === "SET" && expectedSetting != value) ||
          ((expectType === "SHOW" || expectType === "HIDE") && expectType != visibility) ||
          (expectType === "LIMIT" || expectType === "EXCLUDE") && expectType != limitExclude) {
          _export.errorCount++;
          if (c.displayEvent.length == 0)
            _export.addParagraph('Failed Test: ' + field + '#' + _export.countRuleInstances(field) + ' - Expecting ' + expectType + ', but instead no display event recorded');
          else if (expectType === "SET" && expectedSetting != value)
            _export.addParagraph('Failed Test: ' + field + '#' + _export.countRuleInstances(field) + ' - Expecting SET to "' + expectedSetting + '", but instead got: "' + value + '"');
          else if ((expectType === "SHOW" || expectType === "HIDE") && expectType != visibility)
            _export.addParagraph('Failed Test: ' + field + '#' + _export.countRuleInstances(field) + ' - Expecting to ' + expectType + ', but instead resulted in ' + visibility);
          else if ((expectType === "LIMIT" || expectType === "EXCLUDE") && expectType != limitExclude)
            _export.addParagraph('Failed Test: ' + field + '#' + _export.countRuleInstances(field) + ' - Expecting to ' + expectType + ', but instead resulted in ' + limitExclude);

        }
      }
    }
  }

  if (typeof document !== 'undefined') {
    var outPutTable = document.getElementById('testTable');
  }
  var outPutDisplayCSV = "sep=|\ndisplayset|field|obj|customData|expectType|expectedSetting\n";
  var outPutTestCSV = "sep=|\nrulename|obj|customData|expectType\n";

  function download(filename, text) {

    if (document) {
      var pom = document.createElement('a');
      pom.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
      pom.setAttribute('download', filename);

      if (document.createEvent) {
        var event = document.createEvent('MouseEvents');
        event.initEvent('click', true, true);
        pom.dispatchEvent(event);
      }
      else {
        pom.click();
      }
    }

    else {
      //TODO
    }

  }

  function red(s) {
    return '\033[31m' + s + '\033[0m';
  }

  var _export = {
    resultDetailLog: "",
    printToConsole: false,
    printTestCases: false,
    
    setMappings: rules.setMappings,
    loadCustomRules: rules.loadCustomRules,
    loadValidationRules: rules.loadValidationRules,
    loadValidationRuleSet: rules.loadValidationRuleSet,
    loadDisplayRuleSet: rules.loadDisplayRuleSet,
    loadLookups: rules.loadLookups,
    loadDisplayRules: rules.loadDisplayRules,
    loadRulePack : rules.loadRulePack,
    loadRulesPackage: rules.loadRulesPackage,
    getRuleNamesForChannel: rules.getRuleNamesForChannel,
    setRulesRepoPath: function(rulesRepoPath){rules.rulesRepoPath=rulesRepoPath},
    errorCount: 0,
    testCount: 0,
    testRuleNames: [],

    setLogName: function(logName1){ // get the path and name of the log files created in testChannelRulesNode.js
      _export.resultDetailLog = logName1;
    },

    setPrintConsole: function(decision){ // set flag to print or not to print to console
      _export.printToConsole = decision;
    },

    setPrintTestCases: function(decision){ // set flag to print or not to print to console
      _export.printTestCases = decision;
    },

    getDisplayCSV: function () {
      download("displayTests.csv", outPutDisplayCSV)
    },

    getTestCSV: function () {
      download("ruleTests.csv", outPutTestCSV)
    },

    writeDisplayTest: function (displayset, field, obj, customData, expectType, expectedSetting, mustMatchRule) {
      outPutDisplayCSV += [displayset, field, JSON.stringify(obj), JSON.stringify(customData), expectType ? expectType : mustMatchRule ? "SUCCESS" : "NO EVENT", expectedSetting].join('|') + '\n';
      if (!outPutTable) return;// do nothing if there is no output table...

      if (document) {
        var row = document.createElement('tr');

        var td = document.createElement('td');
        td.innerHTML = _export.testCount + 1;
        row.appendChild(td);

        var td = document.createElement('td');
        td.innerHTML = displayset;
        row.appendChild(td);

        td = document.createElement('td');
        td.innerHTML = field;
        row.appendChild(td);

        td = document.createElement('td');
        td.innerHTML = JSON.stringify(obj);
        row.appendChild(td);

        td = document.createElement('td');
        td.innerHTML = customData ? JSON.stringify(customData) : "";
        row.appendChild(td);

        td = document.createElement('td');
        td.innerHTML = expectType ? expectType : mustMatchRule ? "SUCCESS" : "NO EVENT";
        row.appendChild(td);

        td = document.createElement('td');
        td.innerHTML = expectedSetting;
        row.appendChild(td);


        outPutTable.appendChild(row);
      }
      else {
        //TODO
      }
    },

    writeTest: function (rulename, obj, customData, expectType, number, mustMatchRule) {
      outPutTestCSV += [rulename, JSON.stringify(obj), JSON.stringify(customData), expectType ? expectType : mustMatchRule ? "SUCCESS" : "NO RULE"].join('|') + '\n';
      if (!outPutTable) return;// do nothing if there is no output table...

      if (document) {

        var row = document.createElement('tr');

        var td = document.createElement('td');
        td.innerHTML = _export.testCount + 1;
        row.appendChild(td);

        var td = document.createElement('td');
        td.innerHTML = rulename;
        row.appendChild(td);

        td = document.createElement('td');
        td.innerHTML = JSON.stringify(obj);
        row.appendChild(td);

        td = document.createElement('td');
        td.innerHTML = customData ? JSON.stringify(customData) : "";
        row.appendChild(td);

        td = document.createElement('td');
        td.innerHTML = expectType ? expectType : mustMatchRule ? "SUCCESS" : "NO RULE";
        row.appendChild(td);

        td = document.createElement('td');
        td.innerHTML = number;
        row.appendChild(td);


        outPutTable.appendChild(row);
      }

      else {
        //TODO
      }


    },

    setValidationCallbacks: function (callbacks) {
      validationCallbacks = callbacks;
    },
 


    addParagraph: function (text) {
      var output = "";

      if (typeof document !== 'undefined') {
        var objTo = document.getElementById("results");
        var para = document.createElement("p");
        para.innerHTML = text;
        objTo.appendChild(para)
      }
      else {
        if (text.indexOf("Fail") == -1) {
          output = text;
        }
        else {
          output = red(text);
        }

        if(_export.printToConsole){
          console.log(output);
        }
        if (_export.resultDetailLog) {
          fs.appendFileSync(_export.resultDetailLog,text+"\n");
        }
      }
    },

    countRuleInstances: function (rulename) {
      var count = 0;
      var start = _export.testRuleNames.indexOf(rulename);
      while (start != -1) {
        count++;
        start = _export.testRuleNames.indexOf(rulename, start + 1);
      }
      return count;
    }
    ,

    assertFailure: function (rulename, obj) {
      return assertRuleBase(rulename, obj, {DealerType: "AD"}, "ERROR", 1, true);
    },

    assert2Failures: function (rulename, obj) {
      return assertRuleBase(rulename, obj, {DealerType: "AD"}, "ERROR", 2, true);
    },

    assertWarning: function (rulename, obj) {
      return assertRuleBase(rulename, obj, {DealerType: "AD"}, "WARNING", 1, true);
    },

    assertDeprecated: function (rulename, obj) {
      return assertRuleBase(rulename, obj, {DealerType: "AD"}, "DEPRECATED", 1, true);
    },

    assertSuccess: function (rulename, obj) {
      return assertRuleBase(rulename, obj, {DealerType: "AD"}, null, 1, true);
    },

    assertNoRule: function (rulename, obj) {
      return assertRuleBase(rulename, obj, {DealerType: "AD"}, null, 1, false);
    },

    assertFailureCustom: function (rulename, obj, customData) {
      return assertRuleBase(rulename, obj, customData, "ERROR", 1, true);
    },

    assert2FailuresCustom: function (rulename, obj, customData) {
      return assertRuleBase(rulename, obj, customData, "ERROR", 2, true);
    },

    assertWarningCustom: function (rulename, obj, customData) {
      return assertRuleBase(rulename, obj, customData, "WARNING", 1, true);
    },

    assertDeprecatedCustom: function (rulename, obj, customData) {
      return assertRuleBase(rulename, obj, customData, "DEPRECATED", 1, true);
    },

    assertSuccessCustom: function (rulename, obj, customData) {
      return assertRuleBase(rulename, obj, customData, null, 1, true);
    },

    assertNoRuleCustom: function (rulename, obj, customData) {
      return assertRuleBase(rulename, obj, customData, null, 1, false);
    },

    assertValue: function (displayset, field, setting, obj, customData) {
      return assertDisplayRuleBase(displayset, field, obj, customData, "SET", setting, true);
    },
    assertShow: function (displayset, field, obj, customData) {
      return assertDisplayRuleBase(displayset, field, obj, customData, "SHOW", null, true);
    },
    assertHide: function (displayset, field, obj, customData) {
      return assertDisplayRuleBase(displayset, field, obj, customData, "HIDE", null, true);
    },
    assertNoEvent: function (displayset, field, obj, customData) {
      return assertDisplayRuleBase(displayset, field, obj, customData, null, null, false);
    },
    assertDisabled: function (displayset, field, obj, customData) {
      return assertDisplayRuleBase(displayset, field, obj, customData, "DISABLE", null, true);
    },
    assertEnabled: function (displayset, field, obj, customData) {
      return assertDisplayRuleBase(displayset, field, obj, customData, "ENABLE", null, true);
    },
    assertExcluded: function (displayset, field, obj, customData) {
      return assertDisplayRuleBase(displayset, field, obj, customData, "EXCLUDE", null, true);
    },
    assertLimited: function (displayset, field, obj, customData) {
      return assertDisplayRuleBase(displayset, field, obj, customData, "LIMIT", null, true);
    },
    assertValidation: function (rulename,obj) {
      return assertRuleBase(rulename, obj, {DealerType: "AD"}, "VALIDATE", 1, true);
    },
    assertNoValidation: function (rulename,obj) {
      return assertRuleBase(rulename, obj, {DealerType: "AD"}, "NOVALIDATE", 1, false);
    },

    assertCustomValidation: function (rulename,obj, customData) {
      return assertRuleBase(rulename, obj, customData, "VALIDATE", 1, true);
    },
    assertCustomNoValidation: function (rulename,obj,customData) {
      return assertRuleBase(rulename, obj, customData, "NOVALIDATE", 1, false);
    },
    //TODO: Need a assertFilter
  }


  return _export;

})