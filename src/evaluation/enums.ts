
export enum ReportableType {
    /**
     * No rule has been provided in the configuration to cover this scenario
     */
    Unknown="UNKNOWN",
    /**
     * This is an illegal transaction. The the given parameters make no sense
     */
    Illegal="ILLEGAL",
    /**
     * This is a reportable transaction. The ReportingQualifier must be set to BOPCUS
     */
    Reportable="REPORTABLE",
    /**
     * This is a reportable transaction. The ReportingQualifier must be set to NON REPORTABLE
     */
    ZZ1Reportable="ZZ1REPORTABLE",
 
    /**
   * This is a reportable transaction. The ReportingQualifier must be set to BOPCARD RESIDENT
   */
  CARDResReportable="CARDRESREPORTABLE",
  /**
   * This is a reportable transaction. The ReportingQualifier must be set to BOPCARD NON RESIDENT
   */
  CARDNonResReportable="CARDNONRESREPORTABLE",   
  /**
  * This transaction need not be reported to the SARB at all. E.g. Resident Rand to Resident Rand
  */ 
    NotReportable="NONREPORTABLE"
}

export enum ReportableDecision{
    /** The transaction MUST be reported to the regulator */
    ReportToRegulator,
    /** the transaction MUST NOT be reported to the regulator */
    DoNotReport,
    /** the transaction scenario is illegal - this transaction is not considered to be a valid transaction within the jurisdiction */
    Illegal,
    /** the reportability of the transaction is unknown */
    Unknown
}

export enum FlowType {
    /** The Flow is not known */
    Unknown,
    /** This is an in-flow transaction */
    Inflow,
    /** This is an out-flow transaction */
    Outflow
}

export enum DrCr {
    /** Credit/Debit side unknown */
    Unknown,
    /** Debit side  */
    DR="DEBIT",
    /** Credit side  */
    CR="CREDIT"
}

export enum BankAccountType {
    /**
     * Only used when configured account type cannot be determined
     */
    Unknown,
    /**
     * South African Rand account
     */
    RAND,
    /**
     * Customer Foreign Currency account that is used for offshore business like import/export, services etc.
     */
    CFC,
    /**
     * A foreign currency investment account held in the books of South African banks. Money transferred to this
     * type of account is done so using either the discretionary or foreign investment allowance.
     */
    FCA,
    /**
     * A local bank’s foreign currency account in the books of an offshore bank. However in the case of non-residents
     * transferring funds from their own offshore accounts we classify these as NOSTRO accounts
     * (since the transfer goes via the local bank’s NOSTRO account)
     */
    NOSTRO,
    /**
     * A non-resident bank’s Rand account in the books of a local bank
     */
    VOSTRO,
    /**
     * This used where the funds for a transaction are provided in cash as opposed to an account.
     * In this case the cash provided is in Rand
     */
    CASH_ZAR,
    /**
     * This used where the funds for a transaction are provided in cash as opposed to an account.
     * In this case the cash provided is in a foreign currency
     */
    CASH_CURR
}

/**
 * Evaluation Decision Object
 */
export interface EvaluationDecision {
    /** The reportability of the decision */
    decision: ReportableDecision;
    /** The reporting qualifier to use */
    reportingQualifier: string;
    /** The reportability type */
    reportable: ReportableType;
    /** The flow of the transacion */
    flow: FlowType;
    /** Shows weither the reporting side is the debiting party or the crediting party */
    reportingSide: DrCr;
    /** Is the non resident the debiting party or the crediting party */
    nonResSide: DrCr;
    /** Account type for the non resident */
    nonResAccountType: SarbAccountType;
    /** If the non resident is an exception, this field will indicate the exception type */
    nonResException: string;
    /** Is the resident the debiting party or the crediting party */
    resSide: DrCr;
    /** Account type for the resident */
    resAccountType: SarbAccountType;
    /** If the resident is an exception, this field will indicate the exception type */
    resException: string;
    /** Subject, if any */
    subject: string;
    /** Limitations, if any, on the location counytry this transaction may happen in */
    locationCountry: string;
    /** What section of the manual dicatates this decision */
    manualSection: string;
    /** Debiting party's valid SWIFT codes, if any */
    drSideSwift: string;
    /** Limitations on category selection for this decision */
    category: Array<string>;
    /** Category exclusions for this decision */
    notCategory: Array<string>;
    /** Valid account types for the debiting party */
    drAccountTypes: Array<BankAccountType>;
    /** Valid account types for the crediting party */
    crAccountTypes: Array<BankAccountType>;
}

/** The valid SARB account types */
export enum SarbAccountType {
    Unknown = "",
    RE_OTH = "RESIDENT OTHER",
    RE_CFC = "CFC RESIDENT",
    RE_FCA = "FCA RESIDENT",
    CASH = "CASH",
    VOSTRO = "VOSTRO",
    NR_OTH = "NON RESIDENT OTHER",
    NR_RND = "NON RESIDENT RAND",
    NR_FCA = "NON RESIDENT FCA",
    RE_FBC = "RES FOREIGN BANK ACCOUNT"
}

/** Reportability indicator */
export enum rep {
    REPORTABLE = 'REPORTABLE',
    ZZ1REPORTABLE = 'ZZ1REPORTABLE',
    NONREPORTABLE = 'NONREPORTABLE',
    ILLEGAL = 'ILLEGAL',
    UNKNOWN = 'UNKNOWN'
}

/** Flow direction - used for the decision so no unknown allowed */
export enum flowDir {
    IN = 'IN',
    OUT = 'OUT'
}

export enum bankStatus {
    ON_US = 'ON_US',
    OTHER = 'OTHER'
}

export enum resStatus {
    RESIDENT = 'Resident',
    HOLDCO = 'HOLDCO',
    NONRES = 'NonResident',
    IHQ = 'IHQ'
}

export enum accType {
    UNKNOWN = 'UNKNOWN',
    LOCAL_ACC = 'LOCAL_ACC',
    CFC = 'CFC',
    FCA = 'FCA',
    NOSTRO = 'NOSTRO',
    VOSTRO = 'VOSTRO',
    CASH_LOCAL = 'CASH_LOCAL',
    CASH_CURR = 'CASH_CURR'
}

export enum cpInstitution {
  Unknown = "Unknown",
  OFFSHORE = "Offshore",
  ONSHORE_AD = "OnshoreAD",
  ONSHORE_OTHER = "OnshoreOther"
}

export enum accStatus {
    Unknown = "Unknown",
    Entity = "Entity",
    Individual = "Individual"
}