function initialiseEvaluations(flows) {
    for(var evalSet,s=0;evalSet=flows[s];s++) {
        for(var entry,i=0;entry=evalSet[i];i++) {
            var isOutflow = false;
            if (entry.origAccType)
                isOutflow = true;
            for(var evaluation,j=0;evaluation=entry.evaluations[j];j++) {
                if (evaluation.when) {
                    evaluation.validFrom = evaluation.when;
                    for(var eval2,k=0;eval2=entry.evaluations[k];k++) {
                        if (isOutflow) {
                            //Outflows use: recCountry and recAccType
                            if (k != j && eval2.recCountry === evaluation.recCountry && eval2.recAccType === evaluation.recAccType) {
                                if (!eval2.validTo && (!eval2.when || eval2.when < evaluation.when)) {
                                    eval2.validTo = evaluation.when;
                                }
                            }
                        }
                        else {
                            //Inflows use: origCountry and origAccType
                            if (k != j && eval2.origCountry === evaluation.origCountry && eval2.origAccType === evaluation.origAccType) {
                                if (!eval2.validTo && (!eval2.when || eval2.when < evaluation.when)) {
                                    eval2.validTo = evaluation.when;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    return flows[0];
}

function getOutflowEntry(outflows, origAccType, destCurrency) {
    var statusCurrency = curr.OTHER;
    if (destCurrency == "ZAR")
        statusCurrency = curr.ZAR;

    for(var entry,i=0;entry=outflows[i];i++) {
        if (entry.destCurrency === statusCurrency) {
            if (Array.isArray(entry.origAccType)) {
                for(var accType,j=0;accType=entry.origAccType[j];j++) {
                    if (accType == origAccType)
                        return entry;
                }
            }
            else {
                if (entry.origAccType == origAccType)
                    return entry;
            }
        }
    }
    return undefined;
}

function isValidForDate(evaluation, valueDate) {
    if (evaluation.validFrom && valueDate < evaluation.validFrom)
        return false;
    if (evaluation.validTo && valueDate >= evaluation.validTo)
        return false;
    return true;
}

function isRecAccTypeNeeded(entry, valueDate) {
    for(var evaluation,i=0;evaluation=entry.evaluations[i];i++) {
        if (isValidForDate(evaluation, valueDate) &&
                (evaluation.reportable === rep.REPORTABLE || evaluation.reportable === rep.PROVREPORTABLE)) {
            if (evaluation.recCountry === cnt.ZA && (!evaluation.by || evaluation.by === repBy.ORIG))
                return true;
        }
    }
    return false;
}

function noteForRecAccType(entry, valueDate) {
    var result;
    var accTypes = [];
    for(var evaluation,i=0;evaluation=entry.evaluations[i];i++) {
        if (isValidForDate(evaluation, valueDate) &&
                        (evaluation.reportable === rep.REPORTABLE || evaluation.reportable === rep.PROVREPORTABLE)) {
            if (evaluation.recCountry === cnt.ZA && (!evaluation.by || evaluation.by === repBy.ORIG))
                accTypes.push(evaluation.recAccType);
        }
    }
    if (accTypes.length > 0) {
        result = 'If the RecAccId is ';
        if (accTypes.length == 1) {
            result += accTypes[0];
        }
        else {
            result += 'either ';
            for(var accType, j=0;accType=accTypes[j];j++) {
                result += accType;
                if (j < accTypes.length-1)
                    result += ' or ';
            }
        }
        result += ' then this transaction would be reportable by the originating institution';
    }
    else
        result = "Unknown why RecAccId must be specified";
    return result;
}

function noteForOutwardNoScenario(entry) {
    var result = "None of the rules for ";
    if (entry.destCurrency == curr.OTHER)
        result += "currency";
    else
        result += "ZAR";
    result += " transfers from a " + entry.origAccType;
    result += " account are reportable by the originating institution";
    return result;
}

function getOutwardEvaluation(bopcus_evalset, valueDate, origAccType, destCurrency, recCountry, recAccType, isADLA) {
    if (!valueDate)
        valueDate = rule_parameters.todayDate;

    if (isADLA)
        return {reportable: rep.NONREPORTABLE, reason: "ADLA tran already reported"};

    if (!destCurrency || destCurrency.length == 0)
        return {reportable: rep.UNKNOWN, reason: "Destination Currency is not specified"};

    if (!recCountry || recCountry.length == 0)
        return {reportable: rep.UNKNOWN, reason: "Receiving Country is not specified"};

    var statusCountry = cnt.__;
    if (recCountry == "ZA")
        statusCountry = cnt.ZA;

    var entry = getOutflowEntry(bopcus_evalset.outflows, origAccType, destCurrency);
    if (!entry) {
        return {reportable: rep.UNKNOWN, reason: "Outward rule not defined", notes: "No rules match " + destCurrency + " from " + origAccType + " account"};
    }
    var recAccTypeNeeded = isRecAccTypeNeeded(entry, valueDate);

    if (!recAccType && statusCountry === cnt.ZA) {
        if (recAccTypeNeeded)
            return {reportable: rep.UNKNOWN, reason: "RecAccId must be specified", notes: noteForRecAccType(entry, valueDate)};
        else
            return {reportable: rep.NONREPORTABLE, reason: "No reportable scenarios", notes: noteForOutwardNoScenario(entry)};
    }

    for(var evaluation,i=0;evaluation=entry.evaluations[i];i++) {
        if (evaluation.recCountry === statusCountry && isValidForDate(evaluation, valueDate)) {
            if (statusCountry === cnt.__ || evaluation.recAccType == recAccType) {
                var notes;
                if (evaluation.by && evaluation.by === repBy.RECV) {
                    notes = "Reportable by the receiving institution as an OUTFLOW";
                }
                if (notes && evaluation.notes)
                    notes += ". " + evaluation.notes;
                return {reportable: evaluation.reportable, reason: evaluation.reason, notes: notes};
            }
        }
    }
    return {reportable: rep.UNKNOWN, reason: "Outward rule not defined"};
}

function getInflowEntry(inflows, recAccType, srcCurrency) {
    var statusCurrency = curr.OTHER;
    if (srcCurrency == "ZAR")
        statusCurrency = curr.ZAR;

    for(var entry,i=0;entry=inflows[i];i++) {
        if (entry.srcCurrency === statusCurrency) {
            if (Array.isArray(entry.recAccType)) {
                for(var accType,j=0;accType=entry.recAccType[j];j++) {
                    if (accType == recAccType)
                        return entry;
                }
            }
            else {
                if (entry.recAccType == recAccType)
                    return entry;
            }
        }
    }
    return undefined;
}

function isOrigAccTypeNeeded(entry, valueDate) {
    for(var evaluation,i=0;evaluation=entry.evaluations[i];i++) {
        if (isValidForDate(evaluation, valueDate) &&
                (evaluation.reportable === rep.REPORTABLE || evaluation.reportable === rep.PROVREPORTABLE)) {
            if (evaluation.origCountry === cnt.ZA && (!evaluation.by || evaluation.by === repBy.RECV))
                return true;
        }
    }
    return false;
}

function noteOrigAccType(entry, valueDate) {
    var result;
    var accTypes = [];
    for(var evaluation,i=0;evaluation=entry.evaluations[i];i++) {
        if (isValidForDate(evaluation, valueDate) &&
                (evaluation.reportable === rep.REPORTABLE || evaluation.reportable === rep.PROVREPORTABLE)) {
            if (evaluation.origCountry === cnt.ZA && (!evaluation.by || evaluation.by === repBy.RECV))
                accTypes.push(evaluation.origAccType);
        }
    }
    if (!result) {
        result = 'If the OrigAccId is ';
        if (accTypes.length == 1) {
            result += accTypes[0];
        }
        else {
            result += 'either ';
            for(var accType, j=0;accType=accTypes[j];j++) {
                result += accType;
                if (j < accTypes.length-1)
                    result += ' or ';
            }
        }
        result += ' then this transaction would be reportable by the receiving institution';
    }
    else
        result = "Unknown why OrigAccId must be specified";
    return result;
}

function noteForInwardNoScenario(entry) {
    var result = "None of the rules for ";
    if (entry.srcCurrency == curr.OTHER)
        result += "currency";
    else
        result += "ZAR";
    result += " transfers to a " + entry.recAccType;
    result += " account are reportable by the receiving institution";
    return result;
}

function getInwardEvaluation(bopcus_evalset, valueDate, recAccType, srcCurrency, origCountry, origAccType, isADLA) {
    if (!valueDate)
        valueDate = rule_parameters.todayDate;

    if (isADLA)
        return {reportable: rep.NONREPORTABLE, reason: "ADLA tran already reported"};

    if (!srcCurrency || srcCurrency.length == 0)
        return {reportable: rep.UNKNOWN, reason: "Source Currency is not specified"};

    if (!origCountry || origCountry.length == 0)
        return {reportable: rep.UNKNOWN, reason: "Originating Country is not specified"};

    var statusCountry = cnt.__;
    if (origCountry == "ZA")
        statusCountry = cnt.ZA;

    var entry = getInflowEntry(bopcus_evalset.inflows, recAccType, srcCurrency);
    if (!entry) {
        return {reportable: rep.UNKNOWN, reason: "Inward rule not defined", notes: "No rules match " + srcCurrency + " to " + recAccType + " account"};
    }
    var recAccTypeNeeded = isOrigAccTypeNeeded(entry, valueDate);

    if (!origAccType && statusCountry === cnt.ZA) {
        if (recAccTypeNeeded)
            return {reportable: rep.UNKNOWN, reason: "OrigAccId must be specified", notes: noteOrigAccType(entry, valueDate)};
        else
            return {reportable: rep.NONREPORTABLE, reason: "No reportable scenarios", notes: noteForInwardNoScenario(entry)};
    }

    for(var evaluation,i=0;evaluation=entry.evaluations[i];i++) {
        if (evaluation.origCountry === statusCountry && isValidForDate(evaluation, valueDate)) {
            if (statusCountry === cnt.__ || evaluation.origAccType == origAccType) {
                var notes;
                if (evaluation.by && evaluation.by === repBy.ORIG) {
                    notes = "Actually should be reported by the originating institution as an INFLOW";
                }
                if (notes && evaluation.notes)
                    notes += ". " + evaluation.notes;
                return {reportable: evaluation.reportable, reason: evaluation.reason, notes: notes};
            }
        }
    }
    return {reportable: rep.UNKNOWN, reason: "Inward rule not defined"};
}
