import {
  resStatus,
  accType,
  cpInstitution,
  accStatus,
  EvaluationDecision
} from "./enums";

export function evaluator(): Evaluator;

export interface Evaluator {
    evalContext: any;
  setup(ruleset, assumptions, valueDate, evalContext);

  evaluateCustomerTransaction(
    drResStatus: resStatus,
    drAccType: accType,
    crResStatus: resStatus,
    crAccType: accType
  ): Array<EvaluationDecision>;

  evaluateRaw(
    drResStatus: resStatus,
    drAccType: accType,
    crResStatus: resStatus,
    crAccType: accType
  ): Array<EvaluationDecision>;

  evaluateUnknownCrSide(
    drResStatus: resStatus,
    drAccType: accType,
    counterpartyInstituition: cpInstitution,
    counterpartyInstituitionBIC: String,
    destinationCurrency: string,
    accountHolderStatus: accStatus
  ): Array<EvaluationDecision>;

  evaluateUnknownDrSide(
    crResStatus: resStatus,
    crAccType: accType,
    counterpartyInstituition: cpInstitution,
    counterpartyInstituitionBIC: String,
    sourceCurrency: string,
    accountHolderStatus: accStatus,
    field72: string
  ): Array<EvaluationDecision>;

  evaluateCustomerOnUs(
    drResStatus: resStatus,
    drAccType: accType,
    drAccountHolderStatus: accStatus,
    crResStatus: resStatus,
    crAccType: accType,
    crAccountHolderStatus: accStatus
  ): Array<EvaluationDecision>;

  evaluateCustomerOnUsFiltered(
    drResStatus: resStatus,
    drAccType: accType,
    drAccountHolderStatus: accStatus,
    crResStatus: resStatus,
    crAccType: accType,
    crAccountHolderStatus: accStatus
  ): Array<EvaluationDecision>;

  evaluateCustomerOnUsDrOnerous(
    drResStatus: resStatus,
    drAccType: accType,
    drAccountHolderStatus: accStatus,
    crResStatus: resStatus,
    crAccType: accType,
    crAccountHolderStatus: accStatus
  ): Array<EvaluationDecision>;

  evaluateCustomerOnUsCrOnerous(
    drResStatus: resStatus,
    drAccType: accType,
    drAccountHolderStatus: accStatus,
    crResStatus: resStatus,
    crAccType: accType,
    crAccountHolderStatus: accStatus
  ): Array<EvaluationDecision>;

  evaluateCustomerOnUsDrFiltered(
    drResStatus: resStatus,
    drAccType: accType,
    drAccountHolderStatus: accStatus,
    crResStatus: resStatus,
    crAccType: accType,
    crAccountHolderStatus: accStatus
  ): Array<EvaluationDecision>;

  evaluateCustomerOnUsCrFiltered(
    drResStatus: resStatus,
    drAccType: accType,
    drAccountHolderStatus: accStatus,
    crResStatus: resStatus,
    crAccType: accType,
    crAccountHolderStatus: accStatus
  ): Array<EvaluationDecision>;

  evaluateCustomerPayment(
    drResStatus: resStatus,
    drAccType: accType,
    beneficiaryBankStatus_or_BIC: cpInstitution | String,
    destinationCurrency: string,
    accountHolderStatus: accStatus
  ): Array<EvaluationDecision>;

  evaluateCustomerPaymentEx(
    drResStatus: resStatus,
    drAccType: accType,
    beneficiaryBankStatus_or_BIC: cpInstitution | String,
    destinationCurrency: string,
    accountHolderStatus: accStatus
  ): Array<EvaluationDecision>;

  evaluateCustomerPaymentDrFiltered(
    drResStatus: resStatus,
    drAccType: accType,
    beneficiaryBankStatus_or_BIC: cpInstitution | String,
    destinationCurrency: string,
    accountHolderStatus: accStatus
  ): Array<EvaluationDecision>;

  evaluateCustomerReceipt(
    crResStatus: resStatus,
    crAccType: accType,
    orderingBankStatus_or_BIC: cpInstitution | String,
    sourceCurrency: string,
    accountHolderStatus: accStatus,
    field72: string
  ): Array<EvaluationDecision>;

  evaluateCustomerReceiptEx(
    crResStatus: resStatus,
    crAccType: accType,
    orderingBankStatus_or_BIC: cpInstitution | String,
    sourceCurrency: string,
    accountHolderStatus: accStatus,
    field72: string
  ): Array<EvaluationDecision>;

  evaluateCustomerReceiptCrFiltered(
    crResStatus: resStatus,
    crAccType: accType,
    orderingBankStatus_or_BIC: cpInstitution | String,
    sourceCurrency: string,
    accountHolderStatus: accStatus,
    field72: string
  ): Array<EvaluationDecision>;

  evaluateBankPayment(
    orderingBIC: string,
    beneficiaryBIC: string,
    destinationCurrency: string
  ): Array<EvaluationDecision>;

  evaluateBankReceipt(
    beneficiaryBIC: string,
    orderingBIC: string,
    sourceCurrency: string
  ): Array<EvaluationDecision>;

  evaluateCARD(
    issueingCountry: string,
    useCountry: string
  ): Array<EvaluationDecision>;

  consolidatedEvaluation(
    drBankBIC: string, drResStatus: resStatus, drAccType: accType,
    drCurrency: string, drField72: string,
    drAdditionalParams: any,
    crBankBIC: string, crResStatus: resStatus, crAccType: accType, crCurrency: string,
    crAdditionalParams: any,
    sideFiltered: string, mostOnerousFiltered: boolean
  ): Array<EvaluationDecision>;
}
