define(['require', './config', './evaluation/evaluationEx', './validation/contextWrapper', './validation/validation', './validation/display', './validation/predef', './validation/polyfills'], function (require, config, evaluationEx, contextWrapper, validation, display, predef) {

// Testing git branching submodules

  function intersection(a, b) {
    if (!b) return [];
    var r = [];

    if (a)
      a.forEach(function (i) {
        b.forEach(function (j) {
          if (i == j)
            r.push(i);
        })
      })
    return r;
  }

  // function that takes in a section to make a function 
  // that takes source and dest rules and makes a function 
  // to which you can pass the predef
  function mergeMaker(mergeFn) {
    return function (ruleSetDest, ruleSetSrc) {
      return function (predef) {
        if (ruleSetSrc) {
          ruleSetSrc = ruleSetSrc(predef);
          if (ruleSetDest) {
            ruleSetDest = ruleSetDest(predef);
            if (ruleSetDest) {
              if(ruleSetSrc){
                mergeFn(ruleSetDest, ruleSetSrc);
              }else {
                return ruleSetDest;
              }
            } else {
              return ruleSetSrc;
            }
          } else {
            return ruleSetSrc;
          }
        }
        return (typeof ruleSetDest == 'function' ?
          ruleSetDest(predef) : ruleSetDest);
      }

    }
  }


  var mergeRules = mergeMaker(function (ruleSetDest, ruleSetSrc) {
    ruleSetSrc.validations.forEach(function (rule) {
      ruleSetDest.validations.push(rule);
    })
  });

  var mergeDisplay = mergeMaker(function (ruleSetDest, ruleSetSrc) {
    for (var section in ruleSetSrc) {
      var _rules = ruleSetSrc[section];
      if(_rules) _rules.fields.forEach(function (rule) {
        if (ruleSetDest[section]) { ruleSetDest[section].fields.push(rule); }
      })
    }
  });

  var mergeEvals = mergeMaker(function (ruleSetDest, ruleSetSrc) {
    _ruleSetDest = {
      scenarios: ruleSetDest.scenarios.concat(ruleSetSrc.scenarios),
      context: Object.assign(ruleSetDest.context, Object.assign(ruleSetSrc.context, ruleSetDest.context)),
      rules:ruleSetDest.rules.concat(ruleSetSrc.rules)
    }

    Object.assign(ruleSetDest,_ruleSetDest);

    // for (var section in ruleSetSrc) {
    //   var _rules = ruleSetSrc[section];
    //   if(_rules) _rules.validations.forEach(function (rule) {
    //     if (ruleSetDest[section]) { ruleSetDest[section].validations.push(rule); }
    //   })
    // }
  });

  var mergeDocs = mergeMaker(function (ruleSetDest, ruleSetSrc) {
    for (var section in ruleSetSrc) {
      var _rules = ruleSetSrc[section];
      if(_rules) _rules.validations.forEach(function (rule) {
        if (ruleSetDest[section]) { ruleSetDest[section].validations.push(rule); }
      })
    }
  });

  
  function mergeFormDefs(src, dest) {

    var formDef = Object.assign({}, src, dest);

    for (var prop in formDef.detail) {
      if (src.detail[prop]) {
        var diff = src.detail[prop].fields.filter(function (itm) {
          return formDef.detail[prop].fields.indexOf(itm) == -1;
        })
        if (diff && diff.length) {
          formDef.detail[prop].fields = formDef.detail[prop].fields.concat(diff);
        }
      }
    }

    return formDef;
  }

  function mergeAllRules(dest, src) {
    dest.eval = mergeEvals(dest.eval, src.eval);
    dest.documentRules = mergeDocs(dest.documentRules, src.documentRules);
    dest.stdTrans = mergeRules(dest.stdTrans, src.stdTrans);
    dest.stdMoney = mergeRules(dest.stdMoney, src.stdMoney);
    dest.stdImportExport = mergeRules(dest.stdImportExport, src.stdImportExport);
    dest.detailDisplay = mergeDisplay(dest.detailDisplay, src.detailDisplay);
    dest.summaryDisplay = mergeDisplay(dest.summaryDisplay, src.summaryDisplay);
    dest.lookupFilterRules = mergeDisplay(dest.lookupFilterRules, src.lookupFilterRules);

    if (!dest.formDefinition) dest.formDefinition = {};
    if (!src.formDefinition) src.formDefinition = {};

    //dest.formDefinition = mergeFormDefs(src.formDefinition, dest.formDefinition);

    Object.assign(dest.formDefinition, Object.assign(src.formDefinition, dest.formDefinition));

    if (src.generatedLookups && !dest.generatedLookups) {
      dest.generatedLookups = src.generatedLookups;
    } else if (src.generatedLookups && dest.generatedLookups) {
      Object.assign(dest.generatedLookups,Object.assign({},src.generatedLookups, dest.generatedLookups));
    }
    if (!dest.info.mappings) dest.info.mappings = {};
    //if (!src.info.mappings) src.info.mappings = {};
    //override the destination only if the values do not exist (hence the symetrical double-extend).
    Object.assign(dest.info.mappings, Object.assign({},src.info.mappings, dest.info.mappings));

    if (!dest.customFns) dest.customFns = function(){};
    if (!src.customFns) src.customFns = function(){};
    dest.customFns = (function (destFn,srcFn) {
      return function (app, scope) {
        srcFn(app,scope);
        destFn(app,scope);
      }
    })(dest.customFns,src.customFns);

  }

  // function mergeDisplayRules(rulesetDest, ruleSetSrc) {
  //   return function (predef) {
  //     if (rulesetDest) rulesetDest = rulesetDest(predef);
  //     if (ruleSetSrc) ruleSetSrc = ruleSetSrc(predef);
  //     if (ruleSetSrc) {
  //       for (var section in ruleSetSrc) {
  //         var _rules = ruleSetSrc[section];
  //         _rules.fields.forEach(function (rule) {
  //           rulesetDest[section].fields.unshift(rule);
  //         })
  //       }
  //     }
  //   }
  // }

  function promiseLoadModule(path) {
    return new Promise(function (resolve, reject) {
      try {
        require([path], function (_module) {
          resolve(_module);
        }, function () {
          // treating fails as empty resolves.
          resolve(undefined);
        })
      }
      catch (er) { //This is weird, but it gets Node.js to not break when requirejs can't find the file.
        resolve(undefined);
      }
    })
  }

  // private evaluator variable...
   

  var _export = {
    version: config.version,
    rulesRepoPath: 'rules/reg_rule_repo/',
    wrapContext: contextWrapper.wrapContext,
    wrapLookup: contextWrapper.wrapLookup,
    runValidation: validation.runValidation,
    getValidationRule: validation.getRule,
    runDisplayRules: display.runDisplayRules,
    evaluationEx: evaluationEx,
    evaluator: evaluationEx.evaluator(),
    // place-holders
    eval: {
      context: undefined,
      rules: undefined,
      scenarios: undefined
    },

    documentRules : {
      trans: {
        ruleset: "Transaction document rules",
        scope: "transaction",
        validations: []
      },
      money: {
        ruleset: "Money document rules",
        scope: "money",
        validations: []
      },
      ie: {
        ruleset: "ImportExport document rules",
        scope: "importexport",
        validations: []
      },
    },
    stdTrans: {
      ruleset: "Transaction Validation Rules",
      scope: "transaction",
      validations: []
    },
    stdMoney: {
      ruleset: "Money Validation Rules",
      scope: "money",
      validations: []
    },
    stdImportExport: {
      ruleset: "ImportExport Validation Rules",
      scope: "importexport",
      validations: []
    },
    detailDisplay: {
      detailTrans: {
        ruleset: "Transaction Detail Display Rules",
        scope: "transaction",
        fields: []
      },
      detailMoney: {
        ruleset: "Money Detail Display Rules",
        scope: "money",
        fields: []
      },
      detailImportExport: {
        ruleset: "ImportExport Detail Display Rules",
        scope: "importexport",
        fields: []
      }
    },
    summaryDisplay: {
      summaryTrans: {
        ruleset: "Transaction Summary Display Rules",
        scope: "transaction",
        fields: []
      },
      summaryMoney: {
        ruleset: "Money Summary Display Rules",
        scope: "money",
        fields: []
      },
      summaryImportExport: {
        ruleset: "ImportExport Summary Display Rules",
        scope: "importexport",
        fields: []
      }
    },
    lookups: [],
    //----------------------------------------------------------
    // lookupFilterRules
    //----------------------------------------------------------
    lookupFilterRules: {
      filterLookupTrans: {
        ruleset: "Empty Transaction Lookup Filter Rules",
        scope: "transaction",
        fields: []
      },
      filterLookupMoney: {
        ruleset: "Empty Money Lookup Filter Rules",
        scope: "money",
        fields: []
      },
      filterLookupImportExport: {
        ruleset: "Empty ImportExport Lookup Filter Rules",
        scope: "importexport",
        fields: []
      }
    },
    getCategories: function (dir) {
      return _export.lookups.categories.filter(function (item) {
        return dir ? (item.flow == dir) : true;
      });
    },
    getTags: function (dir, tagList) {

      tagList = tagList ? tagList : [];

      var l = _export.getCategories(dir);

      return l.reduce(function (memo, item) {

        if (tagList.length > 0) {

          if ((item.tags) && (intersection(item.tags, tagList).length == tagList.length))
            item.tags.forEach(function (tag) {
              if ((memo.indexOf(tag) == -1) && (tagList.indexOf(tag) == -1)) memo.push(tag);
            })
        } else {// initial list comprised of 1st tags..

          if (item.tags && item.tags.length > 0) {
            if (memo.indexOf(item.tags[0]) == -1) memo.push(item.tags[0]);
          }
        }

        return memo;

      }, []).sort();
    },
    searchForTags: function (dir, tagList) {
      tagList = tagList ? tagList : [];
      var l = _export.getCategories(dir);
      return l.reduce(function (memo, item) {
        if (tagList.length > 0) {
          for (var i in tagList) {
            if ((tagList[i]) && (item.tags) && (item.tags.indexOf(tagList[i]) >= 0)) {
              console.log(">" + item.tags.slice(0, item.tags.indexOf(tagList[i]) + 1).join(' '));
              item.tags.forEach(function (tag) {
                if ((memo.indexOf(tag) == -1) && (tagList.indexOf(tag) == -1)) memo.push(tag);
              })
            }
          }
        }
        return memo;

      }, []).sort();
    },
    filterByTags: function (dir, tagList) {
      var l = _export.getCategories(dir);

      if (tagList.length <= 0) return l;

      return l.filter(function (item) {

        return intersection(item.tags, tagList).length == tagList.length;

      })
    },
    makeCCHeirarchy: function (dir) {
      var l = _export.getCategories(dir);

      return l.reduce(function (memo, item) {
        if (!memo[item.section]) {
          memo[item.section] = {};
        }
        if (!memo[item.section][item.subsection]) {
          memo[item.section][item.subsection] = {};
        }

        var codes = item.code.split('/');

        memo[item.section][item.subsection][item.description] = { category: codes[0], subCategory: codes[1] };

        return memo;
      }, {})


    },
    /** Get all the rules for a give package - also works when you pass in _export locally */
    getAllRules: function(packageData){
      function _a(_arr){
        var arr = (typeof _arr == 'function')? _arr(predef): _arr;
        return arr?(arr.validations?arr.validations:[]):[];
      }
      return _a(packageData.stdTrans).concat(_a(packageData.stdMoney)).concat(_a(packageData.stdImportExport));
    },
    /** get the rule names for a single channel, without pulling in dependent channel rules. */
    getRuleNamesForChannel: function (packageName) {

      var packagePath = _export.rulesRepoPath + packageName + "/";

      return _export._loadChannelPackage(packagePath)
        .then(function (packageData) {
          return _export.getAllRules(packageData).reduce(function (memo, item) {
            return memo.concat(item.rules
              .filter(function (item) {
                // warnings and errors only - no length checks rules since they are generated
                return (['ERROR', 'WARNING', 'VALIDATE' ].indexOf(item.type) != -1)
                  && (
                    item.name.indexOf('.minLen') == -1
                    && item.name.indexOf('.maxLen') == -1
                    && item.name.indexOf('.Len') == -1
                  ) && (
                    item.message != 'Ignore me'
                  );
              })
              .map(function (item) {
                return item.name;
              }));
            
          }, [])
        })
    },
    getRuleMessages: function (override) {

      var allRules = _export.getAllRules(_export);
      var messageRepl = override?allRules.reduce(function (memo, rule) {
        if (rule.rules) {


          rule.rules.reduce(function (memo, item) {
            if (item.type == "MESSAGE")
              memo[item.name] = item.message;
            return memo;
          }, memo);

        }

        return memo;

      }, {}):[];

      function ruleReduce(memo, rule, field) {
        return rule.rules.reduce(function (memo, r) {
          //  if (r.type != "MESSAGE" && r.message != "Ignore me") {
          if (memo.find(function (item) {
            return (item.message == r.message)
              && (item.type == r.type)
              && (item.code == r.code)
              && (item.name == r.name)
              && (item.field == field)
            //&& (item.flow == r.flow)
            //&& (item.sections == r.sections)
            //&& (item.categories == r.categories)
            //&& (item.notCategories == r.notCategories)
          }) == undefined) {
            memo.push({
              type: r.type,
              message: messageRepl[r.name] ? messageRepl[r.name] : r.message,
              code: r.code,
              name: r.name,
              flow: r.flow ? r.flow : "",
              section: r.section ? r.section : "",
              categories: r.categories ? r.categories.join(", ") : "",
              notCategories: r.notCategories ? r.notCategories.join(", ") : "",
              field: field
            });
          }
          //  }

          return memo;
        }, memo)
      }

      var reduced = allRules.reduce(function (memo, rule) {
        if (rule.rules) {

          if (Array.isArray(rule.field)) {
            rule.field.forEach( function (field) {
              ruleReduce(memo, rule, field)
            })
          } else {
            ruleReduce(memo, rule, rule.field)
          }


        }


        return memo;

      }, []);//.join('\n');

      reduced.sort(function (itmA, itmB) {
        return itmA.name > itmB.name ? 1 : -1;
      });

      var out =
        "TYPE|NAME|CODE|FLOW|SECTION|CATEGORIES|NOTCATEGORIES|FIELD|MESSAGE\n" +
        reduced.reduce(function (memo, item) {
          memo += [item.type, item.name, item.code, item.flow, item.section, item.categories, item.notCategories, item.field, item.message].join('|') + '\n';
          return memo;
        }, "");

      return out;

    },
    getPotentialDocsForCategory: function (compoundCat, flow, section) {
      var cc = compoundCat.split("/");
      var cat = cc[0], subCat = cc[1];
      section = section ? section : "ABCDEF";

      // for input 101/01 or 101, "IN", "A" .onSection("ABG").onInflow().notOnCategory(['101/11', '103/11']).onCategory(['101', '103', '105', '106']) should be true.
      var out = [];
      for (var section in _export.documentRules) {
        var ruleSet = _export.documentRules[section];
        out = out.concat(ruleSet.validations.reduce(function (memo, rules) {
          return memo.concat(rules.rules.filter(function (rule) {
            if(flow&&rule.flow&&(flow!=rule.flow)) return false;
            if (!(rule.categories || rule.notCategories)) return false;
            var found = true;
            if (rule.categories && predef.categoryNotInList(compoundCat, cat, rule.categories)) {
              found = false;
            }
            if (rule.notCategories) {
              if (predef.categoryNotInList(compoundCat, cat, rule.notCategories)) {
                found = found&&true;
              } else {
                found = false;
              }
            }
            return found;

          }));
        }, []));
      }
      return out;
    },


    //----------------------------------------------------------
    formDefinition: null,
    //----------------------------------------------------------
    predef: predef,
    setRuleParameters: predef.setRuleParameters,
    _loadRulesFromURL: function (url) {
      var xhr = createXHR();
      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          alert(xhr.responseText);
        }
      }
      xhr.open('GET', url, true);
      xhr.withCredentials = true;
      xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest')
      xhr.setRequestHeader('Content-type', 'application/javascript');
      xhr.send();
    },
    loadValidationRules: function (path, callBack) {
      require([path], function (rulesGen) {
        // rules loaded from a URL will return a function which expects a DSL Predef
        var customRules = rulesGen(predef);

        customRules.rules.forEach( function (rules) {
          var currentRuleSet = _export[({
            'transaction': 'stdTrans',
            'money': 'stdMoney',
            'importexport': 'stdImportExport'
          })[rules.scope]];
          currentRuleSet.validations = currentRuleSet.validations.concat(rules.validations);
        })
        callBack();
      })
    },

    loadValidationRuleSet: function (path, callBack) {
      require([path], function (rulesGen) {
        // rules loaded from a URL will return a function which expects a DSL Predef
        var ruleSet = rulesGen(predef);

        var currentRuleSet = _export[({
          'transaction': 'stdTrans',
          'money': 'stdMoney',
          'importexport': 'stdImportExport'
        })[ruleSet.scope]];

        currentRuleSet.validations = ruleSet.validations.concat(currentRuleSet.validations);
        callBack();
      })
    },


    loadLookups: function (path, callBack) {

      //deal with case where no lookups exist - eg, feature branches...
      if(typeof _export.lookups == 'undefined') _export.lookups = {};

      require([path], function (lookups) {
        var localLookups;
        if (lookups.lookups)
          localLookups = lookups.lookups;
        else
          localLookups = lookups;
        Object.assign(_export.lookups, localLookups);
        callBack();
      })
    },

    loadFormDefinition: function (path, callBack) {
      require([path], function (formDefinition) {
        _export.formDefinition = formDefinition;
        callBack();
      })
    },

    loadDisplayRuleSet: function (name, path, callBack) {

      require([path], function (customRuleSet) {

        if (typeof customRuleSet == "function")
          customRuleSet = customRuleSet(predef);

        if (Array.isArray(customRuleSet)) {
          customRules[name].forEach(function (_rulesSet) {
            for (var section in _rulesSet) {
              var _rules = _rulesSet[section];
              _rules.fields.forEach(function (rule) {
                _export[name][section].fields.unshift(rule);
              })
            }
          });
        } else {
          for (var section in customRuleSet) {
            var _rules = customRuleSet[section];
            _rules.fields.forEach( function (rule) {
              _export[name][section].fields.unshift(rule);
            })
          }
        }
      })
      callBack();
    },

    loadDisplayRules: function (path, callBack) {
      require([path], function (customRules) {

        function mergeDisplay(name) {
          if (customRules[name]) {
            if (Array.isArray(customRules[name])) {
              customRules[name].forEach( function (_rulesSet) {
                for (var section in _rulesSet) {
                  var _rules = _rulesSet[section];
                  _rules.fields.forEach( function (rule) {
                    _export[name][section].fields.unshift(rule);
                  })
                }
              });
            } else {
              for (var section in customRules[name]) {
                var _rules = customRules[name][section];
                _rules.fields.forEach(function (rule) {
                  _export[name][section].fields.unshift(rule);
                })
              }
            }
          }
        }

        mergeDisplay('detailDisplay');
        mergeDisplay('summaryDisplay');
        mergeDisplay('lookupFilterRules');

        callBack();
      })
    },
    /**
     * This loads a pre-merged rule package created by node tools/compileChannelPackages.js
     */
    loadRulePack: function (pack, callBack, features) {

      function mergeMaker(section) {
        return function (rulesetDest, ruleSetSrc) {
          if (ruleSetSrc) {
            ruleSetSrc[section].forEach(function (rule) {
              rulesetDest[section].push(rule);
            })
          }
        }
      }
      
      //var mergeRules = mergeMaker('validations');
      var mergeDisplay = mergeMaker('fields');

      features = features || {
        document: true,
        display: true,
        evaluations: true,
        validations: true,
        data: true,
        formDef: true
      }

      if (pack.predef) {
        predef = pack.predef(predef);
      }
      if (pack.info.mappings) {
        _export.setMappings(pack.info.mappings)
      }

      if(features.document && pack.documentRules){
        var docTrans, docMoney, docIE;
        if(typeof pack.documentRules == 'function'){
          var rules = pack.documentRules(predef).rules;
          docTrans = rules.filter(function(ruleSet){
            return ruleSet.scope == 'transaction'
          })
          docMoney = rules.filter(function(ruleSet){
            return ruleSet.scope == 'money'
          })
          docIE = rules.filter(function(ruleSet){
            return ruleSet.scope == 'importexport'
          })
        }else if(typeof pack.documentRules.trans == 'function'){
          docTrans = pack.documentRules.trans(predef);
          docMoney = pack.documentRules.money(predef);
          docIE = pack.documentRules.ie(predef);
        }
        if(docTrans){
          docTrans.forEach( function (ruleSet) {
            //mergeRules(_export.stdTrans, ruleSet);
            _export.documentRules.trans.validations = _export.documentRules.trans.validations.concat(ruleSet.validations);
          })
        }
        if(docMoney){
          docMoney.forEach( function (ruleSet) {
            //mergeRules(_export.stdMoney, ruleSet);
            _export.documentRules.money.validations = _export.documentRules.money.validations.concat(ruleSet.validations);
          })
        }
        if(docIE){
          docIE.forEach( function (ruleSet) {
            //mergeRules(_export.stdImportExport, ruleSet);
            _export.documentRules.ie.validations = _export.documentRules.ie.validations.concat(ruleSet.validations);
          })
        }
      }

      if (features.display) {
        var _detailDisplay = pack.display.detailDisplay(predef);
        var _summaryDisplay = pack.display.summaryDisplay(predef);
        var _lookupFilterRules = pack.display.lookupFilterRules(predef);
        if(pack.display.customFns) _export.customFns = pack.display.customFns;
        var detailDisplay = {
          detailTrans: _detailDisplay.filter( function (i) {
            return i.scope == 'transaction';
          }),
          detailMoney: _detailDisplay.filter( function (i) {
            return i.scope == 'money';
          }),
          detailImportExport: _detailDisplay.filter( function (i) {
            return i.scope == 'importexport';
          })
        };

        var summaryDisplay = {
          summaryTrans: _summaryDisplay.filter( function (i) {
            return i.scope == 'transaction';
          }),
          summaryMoney: _summaryDisplay.filter( function (i) {
            return i.scope == 'money';
          }),
          summaryImportExport: _summaryDisplay.filter( function (i) {
            return i.scope == 'importexport';
          })
        };

        var lookupFilterRules = {
          filterLookupTrans: _lookupFilterRules.filter( function (i) {
            return i.scope == 'transaction';
          }),
          filterLookupMoney: _lookupFilterRules.filter( function (i) {
            return i.scope == 'money';
          }),
          filterLookupImportExport: _lookupFilterRules.filter( function (i) {
            return i.scope == 'importexport';
          })
        };

        for(var key in detailDisplay){
          var section = detailDisplay[key];
          section.forEach( function (ruleSet) {
            mergeDisplay(_export.detailDisplay[key], ruleSet);
          })
        }

        for(var key in summaryDisplay){
          var section = summaryDisplay[key];
          section.forEach( function (ruleSet) {
            mergeDisplay(_export.summaryDisplay[key], ruleSet);
          })
        }

        for(var key in lookupFilterRules){
          var section = lookupFilterRules[key];
          section.forEach( function (ruleSet) {
            mergeDisplay(_export.lookupFilterRules[key], ruleSet);
          })
        }

      }

      if (features.evaluations) {
        //TODO!!! --  for now this is handled in api.js in BOPServer.
        if(typeof pack.evaluation == 'function'){
          _export.eval = pack.evaluation(evaluationEx)
        }else if(pack.evaluation.evalScenarios && (typeof pack.evaluation.evalScenarios == 'function')){
          _export.eval = {
            scenarios: pack.evaluation.evalScenarios(evaluationEx),
            context: pack.evaluation.evalContext(evaluationEx),
            rules: pack.evaluation.evalRules(evaluationEx)
          } 
        } else {
          _export.eval = {
            scenarios: [],
            context: [],
            rules: []
          } 
        }
        // setup the evaluator...
        if (_export.eval.scenarios&&_export.eval.scenarios.length==1){
          _export.eval.scenarios=_export.eval.scenarios[0];
        }
        _export.evaluator.setup(_export.eval.rules, _export.eval.scenarios, new Date(), _export.eval.context);
      }

      if (features.validations) {
        var stdTrans = pack.validation.transaction(predef);
        var stdMoney = pack.validation.money(predef);
        var stdImportExport = pack.validation.importexport(predef);

        stdTrans.forEach( function (ruleSet) {
          //mergeRules(_export.stdTrans, ruleSet);
          _export.stdTrans.validations = _export.stdTrans.validations.concat(ruleSet.validations);
        })
        stdMoney.forEach( function (ruleSet) {
          //mergeRules(_export.stdMoney, ruleSet);
          _export.stdMoney.validations = _export.stdMoney.validations.concat(ruleSet.validations);
        })
        stdImportExport.forEach( function (ruleSet) {
          //mergeRules(_export.stdImportExport, ruleSet);
          _export.stdImportExport.validations = _export.stdImportExport.validations.concat(ruleSet.validations);
        })

      }

      if (features.data) {
        _export.lookups = pack.data.lookups;

      }
      if (features.formDef) {
        _export.formDefinition = pack.formDefinition;

      }

      callBack(_export);
    },
    /**
     * This dynamically loads a rule package and performs merges.
     *  To be used for dev or client-side rule package tests... rather use loadRulePack in production.
     */
    loadRulesPackage: function (packageNameStr, callBack) {
      predef.mappings = {};
      _export._loadRulesPackage(packageNameStr, function (packageData) {
        _export.setMappings(packageData.info.mappings);

        _export.eval = packageData.eval(evaluationEx);
       
        // setup the evaluator...
        if(_export.eval.scenarios&&_export.eval.scenarios.length==1){
          _export.eval.scenarios=_export.eval.scenarios[0];
        }
        _export.evaluator.setup(_export.eval.rules, _export.eval.scenarios, new Date(), _export.eval.context);

        _export.documentRules = packageData.documentRules(predef);
        _export.stdTrans = packageData.stdTrans ? packageData.stdTrans(predef) : undefined;
        _export.stdMoney = packageData.stdMoney ? packageData.stdMoney(predef) : undefined;
        _export.stdImportExport = packageData.stdImportExport ? packageData.stdImportExport(predef) : undefined;
        _export.detailDisplay = packageData.detailDisplay ? packageData.detailDisplay(predef) : undefined;
        _export.summaryDisplay = packageData.summaryDisplay ? packageData.summaryDisplay(predef) : undefined;
        _export.lookupFilterRules = packageData.lookupFilterRules ? packageData.lookupFilterRules(predef) : undefined;
        _export.formDefinition = packageData.formDefinition;
        _export.lookups = packageData.generatedLookups;
        _export.customFns = packageData.customFns;

        callBack(_export);
      })
    },
    _loadChannelPackage: function (packagePath) {

      return Promise.all([
        //docs
        promiseLoadModule(packagePath + 'document/transaction'),
        promiseLoadModule(packagePath + 'document/money'),
        promiseLoadModule(packagePath + 'document/importexport'),
        //validation
        promiseLoadModule(packagePath + 'validation/transaction'),
        promiseLoadModule(packagePath + 'validation/money'),
        promiseLoadModule(packagePath + 'validation/importexport'),
        //display
        promiseLoadModule(packagePath + 'display/detailDisplay'),
        promiseLoadModule(packagePath + 'display/summaryDisplay'),
        promiseLoadModule(packagePath + 'display/lookupFilterRules'),
        //data,customFns,etc.
        promiseLoadModule(packagePath + 'predef/predef'),
        promiseLoadModule(packagePath + 'formDef/default'),
        promiseLoadModule(packagePath + 'data/lookups'),
        promiseLoadModule(packagePath + 'display/customFns'),
        promiseLoadModule(packagePath + 'package'),
        //eval
        promiseLoadModule(packagePath + 'evaluation/evalRules'),
        promiseLoadModule(packagePath + 'evaluation/evalScenarios'),
        promiseLoadModule(packagePath + 'evaluation/evalContext')
      ])
        .then(function (result) {
          var docTrans = result[0], docMoney = result[1], docIE = result[2],
            trans = result[3], money = result[4], ie = result[5],
            detail = result[6], summary = result[7],
            lookupFilter = result[8], _predef = result[9],
            formDef = result[10], lookups = result[11],
            customFns = result[12], rulePackageInfo = result[13],
            evalRules = result[14], evalScenarios= result[15], evalContext=result[16];
          if (_predef) {
            predef = _predef(predef);
          }
          return {
            eval: function(evaluationEx){
              //if(evalRules||evalScenarios||evalContext)
                return {
                  rules: evalRules ? evalRules(evaluationEx) : [],
                  scenarios: evalScenarios ? evalScenarios(evaluationEx) : [],
                  context: evalContext ? evalContext(evaluationEx) : {}
                }
            },
            documentRules: function (predef) {
              if(docTrans||docMoney||docIE)
                return {
                  trans: docTrans ? docTrans(predef) : undefined,
                  money: docMoney ? docMoney(predef) : undefined,
                  ie: docIE ? docIE(predef) : undefined
                }
            },
            stdTrans: trans,
            stdMoney: money,
            stdImportExport: ie,
            detailDisplay: detail,
            summaryDisplay: summary,
            lookupFilterRules: lookupFilter,
            generatedLookups: lookups,
            formDefinition: formDef,
            info: rulePackageInfo,
            customFns: customFns
          };
        })
    },
    _loadParent:function(packageData){
      return new Promise(function(resolve, reject){

        // load the parent
        if (packageData.info.dependsOn) {
          // not at the root, dig deeper.
          _export._loadRulesPackage(packageData.info.dependsOn, function (_packageData) {
            mergeAllRules(packageData, _packageData);

            // pass it down the chain.
            resolve(packageData);
          })
        } else {

          // pass it down the chain.
          resolve(packageData)
        }

      })
       
    },
    _loadRulesPackage: function (packageNameStr, callBack) {
      var packageName = packageNameStr.split("@")[0];
      var packageVersion = packageNameStr.split("@")[1];
      var packagePath = _export.rulesRepoPath + packageName + "/";

      _export._loadChannelPackage(packagePath).then(function(packageData){
          packageData.info.name = packageName;
          // load the features
          _export._loadParent(packageData)
          .then(function(packageData){
            if (packageData.info.features) {
              Promise.all(
                packageData.info.features.map( function (item) {
                  return new Promise(function (resolve, reject) {
                    _export._loadRulesPackage(item, resolve);
                  })
                })
              ).then(function (results) {
                  results.forEach( function (_packageData) {
                    mergeAllRules(packageData, _packageData);
                  })
                  callBack(packageData);
              })
            } else {
                callBack(packageData);
            }
          })

        })

    },

    loadCustomRules: function (path, callBack) {
      require([path], function (customRules) {

        function mergeRules(src, dest) {

          if (customRules[src]) {
            if (Array.isArray(customRules[src])) {
              customRules[src].forEach( function (_ruleSet) {
                if (typeof _ruleSet == 'function') _ruleSet = _ruleSet(predef);

                _ruleSet.validations.forEach( function (rule) {
                  _export[dest].validations.unshift(rule);
                })
              })
            } else {
              customRules[src].validations.forEach( function (rule) {
                _export[dest].validations.unshift(rule);
              })
            }
          }
        }

        function mergeDisplay(name) {
          if (customRules[name]) {
            if (Array.isArray(customRules[name])) {
              customRules[name].forEach( function (_rulesSet) {
                for (var section in _rulesSet) {
                  var _rules = _rulesSet[section];
                  _rules.fields.forEach( function (rule) {
                    _export[name][section].fields.unshift(rule);
                  })
                }
              });
            } else {
              for (var section in customRules[name]) {
                var _rules = customRules[name][section];
                _rules.fields.forEach( function (rule) {
                  _export[name][section].fields.unshift(rule);
                })
              }
            }
          }
        }

        mergeRules('Trans', 'stdTrans');
        mergeRules('Money', 'stdMoney');
        mergeRules('ImportExport', 'stdImportExport');

        mergeDisplay('detailDisplay');
        mergeDisplay('summaryDisplay');

        if (customRules.generatedLookups) {
          Object.assign(_export.lookups, customRules.generatedLookups)
        }

        mergeDisplay('lookupFilterRules');

        if (customRules.formDefinition) {
          _export.formDefinition = customRules.formDefinition;
        }

        callBack(_export);
      },
        function (error) {
          callBack('ERROR LOADING CUSTOM RULES.');
          throw error;
        })
    },
    setMappings: function(mappings,override) {
      Object.assign(predef.mappings, mappings);
    },
    getMappings: function(){
      return predef.mappings;
    },
    map: function(field){
      var _field =  (typeof field == 'function')? field() : field;
      return predef.getMap(_field);
    },
    setRuleParameters: predef.setRuleParameters,
  }

  // Initialise the 'and' and 'or' Function prototypes.
  predef.fnAndOrUp();

  return _export;
});