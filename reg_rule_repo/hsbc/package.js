define({
  engine: {major: "1", minor: "0"},
  dependsOn: "coreSARB",
  mappings: {
    LocalCurrencySymbol: "R",
    LocalCurrencyName: "Rand",
    LocalCurrency: "ZAR",
    Locale: "ZA",
    LocalValue: "RandValue",
    Regulator: "SARB",
    DealerPrefix: "AD",
    RegulatorPrefix: "SARB"
  }
})