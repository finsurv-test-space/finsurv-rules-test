define(function () {
  return function (evaluationEx) {
    var scenarios;
    with (evaluationEx) {
      scenarios = [
        {
          scenario: "BankReceipt - NTNRB",
          match: crThisBank.and(drHasValue("Field72", "contains", "NTNRB")),
          rules: [
            decide( {
                         manualSection: "All interbank transactions received from all banks (via local bank and marked NTNRB) are ZZ1",
                         reportable: rep.ZZ1REPORTABLE,
                         flow: flowDir.IN,
                         reportingSide: drcr.CR,
                         resSide: drcr.CR,
                         resAccountType: at.RE_OTH,
                         nonResSide: drcr.DR,
                         nonResAccountType: at.NR_OTH
                       })
          ]
        }
      ]
    }
    return scenarios;
  }
})