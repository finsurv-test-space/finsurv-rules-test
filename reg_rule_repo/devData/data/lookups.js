define({
  ReportingQualifier           : ["BOPCUS", "NON REPORTABLE", "BOPCARD RESIDENT", "BOPCARD NON RESIDENT", "NON RESIDENT RAND", "INTERBANK", "BOPDIR"],
  luClientCCNs: [
    {
     ccn: "12345678", 
     accountName: "JC ECKHARDT", 
     accountNumber: "321654987"
    },
    {
     ccn: "12345670", 
     accountName: "JC ECKHARDT", 
     accountNumber: "321654987"
    }
   ],
  branches : [
    {code: '101001', name : 'STANDARD BANK MZUZU', hubCode: "123", hubName: "TestHub123", validFrom: "2013-01-04"},
    {code: '124124', name : 'TestBranch124', validFrom: "2013-01-04", validTo : "2017-01-04"},
    {code: '124124', name : 'Maraisburg', hubCode: "", hubName: "", validFrom: "2016-01-04", validTo : "2017-01-04"},
  ],
  mtaAccounts : [
    {
      accountNumber : "11111",
      MTA : "RANDBUREAU",
      rulingSection : "Test Rulings Section",
      ADLALevel : "3",
      isADLA : false
    },
    {
      accountNumber : "22222",
      MTA : "MUKURU",
      rulingSection : "A.4 (B)(ii)",
      ADLALevel : "2",
      isADLA : true
	},
	{
		accountNumber : "33333",
		MTA : "PAYPAL",
		ADLALevel : "2",
		isADLA : true
    },
    {
      accountNumber : "444444",
      MTA : "SIKHONA",
      rulingSection : "A.3 (C)(iii)",
      ADLALevel : "2",
      isADLA : true
	},
  ],
  ihqCompanies : [
    {
      ihqCode : "IHQ123",
      registrationNumber : "2013/1234567/07",
      companyName : "company1"
    },
    {
      ihqCode : "IHQ124",
      registrationNumber : "2013/1234568/07",
      companyName : "company2"
    }
  ],
  holdcoCompanies : [
    {
      accountNumber : "1234567",
      registrationNumber : "2013/1234567/07",
      companyName : "holdco1"
    },
    {
      accountNumber : "1234568",
      registrationNumber : "2013/1234568/07",
      companyName : "holdco2"
    }
  ]
})
