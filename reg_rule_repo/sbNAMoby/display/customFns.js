define(function (require) {
    return function (app, config) {

        var superInit = function () { };

        if (config.initializationFn) superInit = config.initializationFn;

        var _config = {
            initializationFn: function (model, scope) {
                superInit(model, scope);
                //custom scope functions go here...

                model.MonetaryAmount[0].isAccGrpOpen = true;

                scope.getFlow = function(){
                    var data = app.getData();
          
                    var flow = data.transaction.Flow;
          
                    return flow;
                  }

                scope.functionTest = function(element){
                    var test = element.firstElementChild.className;
                    return true;
                }


                scope.getResidentType = function() {
                    var data = app.getData();
                    var residentType = data.transaction.Resident ? (data.transaction.Resident.Entity ? 'Entity' : data.transaction.Resident.Individual ? 'Individual' : 'Unknown') : undefined;
                    return residentType;   
                }

            },

            useTabs: true,
            //noCss: true,
            clearHidden: true,
            clearHiddenOnErrorOnly: true,
            snapShotValidate: true
        }

        Object.assign(config, _config);






        return config;
    }
})