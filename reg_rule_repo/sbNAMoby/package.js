define({
  engine: {major: "1", minor: "0"},
  dependsOn: "sbNAFlow",
  // features: ["featureBranchHub", "featureHOLDCO", "featureMTAAccounts"],
  mappings: {
    LocalCurrencySymbol: "$",
    LocalCurrencyName: "Dollar",
    LocalCurrency: "NAD",
    Locale: "NA",
    LocalValue: "DomesticValue",
    Regulator: "CB",
    RegulatorName: "Bank of Namibia",
    RegulatorAbbr: "BON",
    DealerPrefix: "RE",
    RegulatorPrefix: "CB",
    StateName: "Region",
    _minLenErrorType: "ERROR", // SUCCESS, ERROR, WARNING
    _maxLenErrorType: "ERROR",
    _lenErrorType: "ERROR"
  }
})