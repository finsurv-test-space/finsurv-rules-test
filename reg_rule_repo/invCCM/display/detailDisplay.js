define(function () {
  return function (predef) {
    var display;

    with (predef) {
      display = {
        detailTrans: {
          ruleset: "invCCM Transaction Display Rules",
          scope: "transaction",
          fields: [
            {
              field: "NonResident.Entity.Address.Country",
              display: [
                enable() 
                //setValue("%s", "Resident.Entity.StreetAddress.Country", notMatchesTransactionField("Resident.Entity.StreetAddress.Country").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                //setValue("%s", "LocationCountry", isEmpty.and(notMatchesTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(hasResidentFieldValue("StreetAddress.Country", "ZA"))))),
                //setValue("%s", "LocationCountry", isEmpty.and(hasTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N")))),
                //disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                //enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Individual.Address.Country",
              display: [
                enable() 
                //setValue("%s", "Resident.Individual.StreetAddress.Country", notMatchesTransactionField("Resident.Individual.StreetAddress.Country").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                //setValue("%s", "LocationCountry", isEmpty.and(notMatchesTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(hasResidentFieldValue("StreetAddress.Country", "ZA"))))),
                //setValue("%s", "LocationCountry", isEmpty.and(hasTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N")))),
                //disable(isTransactionFieldProvided("NonResident.Individual.Address.Country").or(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                //enable(not(isTransactionFieldProvided("NonResident.Individual.Address.Country")).and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))))
              ]
            }
          ]
        },
        detailMoney: {
          ruleset: "invCCM Money Display Rules",
          scope: "money",
          fields: [
            {
              field: "CategoryCode",
              display: [
                disable().onSection("C"),
                setValue('ZZ1').onSection("C")
              ]
            }
          ]
        },
        detailImportExport: {
          ruleset: "invCCM Import/Export Display Rules",
          scope: "importexport",
          fields: []

        }
      };
    }

    return display;
  }
});