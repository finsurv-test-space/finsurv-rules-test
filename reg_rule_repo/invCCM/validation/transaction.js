define(function () {
//THIS IS the Transaciotn rules
  return function (predef) {
      var transaction;

      with (predef) {
          transaction = {
              ruleset: "Transaction Rules for Flow form",
              scope: "transaction",
              validations: [
                {
                  field: "",
                  rules: [
                    ignore('cntft1'),
                    ignore('cnte1'),                    
                    ignore("field.maxLen"),
                    message("vd4", null, "Submission Date must be equal to or after the 2013-08-19"),
                    message("ocntry1", null, "If Originating Bank is completed, this must also be completed"),
                    message("ocntry4", null, "SWIFT country code may not be ZA for Inwards"),
                    message("cbank1", null, "If Intermediary Bank Country is completed, this must also be completed"),
                    message("ccntry1", null, "If Intermediary Bank Name is completed, this must also be completed"),
                    message("rbank3", null, "If Receiving Country is completed, must be completed"),
                    message("rcntry1", null, "If Beneficiary Bank is completed, this must also be completed"),
                    message("rcntry4", null, "SWIFT country code may not be ZA for Inwards"),
                    message("nrgn1", null, "Invalid gender"),
                    message("nr1", null, "Must contain one of Individual or Entity Remitter Type").onInflow(),
                    message("nr1", null, "Must contain one of Individual or Entity Beneficiary Type").onOutflow(),
                    message("nr6", null, "Not required for category chosen"),
                    message("nrsn1", null, "If Remitter Type Individual is completed, must be completed").onInflow(),
                    message("nrsn1", null, "If Beneficiary Type Individual is completed, must be completed").onOutflow(),
                    message("nrnm1", null, "Name is required"),
                    message("nrlen1", null, "Entity Name is required"),
                    message("nrpc2", null, "Invalid Passport Country of Issue"),
                    message("nrian2", null, "Cannot be the same account details as the remitter").onInflow(),
                    message("nrian2", null, "Cannot be the same account details as the beneficiary").onOutflow(),
                    message("ripc1", null, "Country is required"),
                    message("tma", null, "At least one Sequence Amount must be provided"),
                    message("ritrpn1", null, "Not required for category chosen"),
                    message("ritrpn2", null, "Not required for category chosen"),
                    message("ritrpn3", null, "For an Individual at least one of ID Number or Temporary Resident Permit Number or Passport Number must be completed"),
                    message("nriz3", null, "Code is too long"),
                    message("nrictry1", null, "Country is required"),
                    message("nrictry3", null, "Country cannot be ZA"),
                    message("nrictry7", null, "Currency does not correspond to country specified"),
                    message("rig1", null, "Gender is required"),
                    message("rig2", null, "Invalid Gender"),
                    message("ridob1", null, "Date of birth is required"),
                    message("ridob3", null, "Date of birth does not match the ID number provided"),
                    message("riidn2", null, "ID number is required for this category"),
                    message("rifidn1", null, "Not required for category chosen"),
                    message("rifidn2", null, "Not required for category chosen"),
                    message("rifidc1", null, "Country is required"),
                    message("g1", null, "An email address, fax number or phone number is required"),
                    message("accno3", null, "Applicant account number and Remitter account number cannot be the same").onInflow(),
                    message("accno3", null, "Applicant account number and Beneficiary account number cannot be the same").onOutflow(),
                    message("accno4", null, "Account number is required"),
                    message("ccn2", null, "Customs Client Number is required for the category code selected"),
                    message("ccn4", null, "Customs Client Number must be 8 digits"),
                    message("ccn5", null, "This number is not allowed"),
                    message("tn1", null, "Tax number is required for the category code selected"),
                    message("tn2", null, "Tax number is required for the category code selected"),
                    message("tcci1", null, "Tax Clearance Certificate Indicator is required for the category code selected"),
                    message("tccr1", null, "Tax Clearance Certificate Reference is required"),
                    message("cn1", null, "Contact name is required"),
                    message("cnte1", null, "Contact details are required"),
                    message("cntft1", null, "At least one of the Email, Fax or Telephone fields need to be completed"),
            
                    message("OriginatingBank.minLen", null, "Bank name is too short"),
                    message("LocationCountry.minLen", null, "A valid country code is 2 characters long"),
                    message("Resident.Individual.IDNumber.minLen", null, "ID number is too short"),
                    message("Resident.Individual.TempResPermitNumber.minLen", null, "Permit number is too short"),
                    message("Resident.Individual.ForeignIDNumber.minLen", null, "ID number is too short"),
                    message("Resident.Individual.ForeignIDCountry.minLen", null, "Country is too short"),
                    message("Resident.Individual.PassportNumber.minLen", null, "Passport number is too short"),
                    message("Resident.Individual.PassportCountry.minLen", null, "Country is too short"),
                    message("Resident.Individual.AccountName.minLen", null, "Name is too short"),
                    message("Resident.Individual.AccountNumber.minLen", null, "Account number is too short"),
                    message("Resident.Individual.CustomsClientNumber.minLen", null, "Client number is too short"),
                    message("Resident.Individual.TaxNumber.minLen", null, "Tax number is too short"),
                    message("Resident.Individual.VATNumber.minLen", null, "VAT number is too short"),
                    message("Resident.Individual.TaxClearanceCertificateReference.minLen", null, "Reference is too short"),
                    message("Resident.Individual.StreetAddress.AddressLine1.minLen", null, "Address is too short"),
                    message("Resident.Individual.StreetAddress.AddressLine2.minLen", null, "Address is too short"),
                    message("Resident.Individual.StreetAddress.Suburb.minLen", null, "Suburb is too short"),
                    message("Resident.Individual.StreetAddress.City.minLen", null, "City is too short"),
                    message("Resident.Individual.StreetAddress.State.minLen", null, "State is too short"),
                    message("Resident.Individual.StreetAddress.PostalCode.minLen", null, "Code is too short"),
                    message("Resident.Individual.ContactDetails.ContactSurname.minLen", null, "Surname is too short"),
                    message("Resident.Individual.ContactDetails.ContactName.minLen", null, "Name is too short"),
                    message("Resident.Individual.ContactDetails.Email.minLen", null, "Email address is too short"),
                    message("Resident.Individual.ContactDetails.Fax.minLen", null, "Fax number is too short"),
                    message("Resident.Individual.ContactDetails.Telephone.minLen", null, "Phone number is too short"),
            
                    message("NonResident.Individual.PassportNumber.minLen", null, "Passport number is too short"),
                    message("NonResident.Individual.AccountNumber.minLen", null, "Number is too short"),
                    message("NonResident.Entity.AccountNumber.minLen", null, "Number is too short"),
                    message("NonResident.Entity.EntityName.minLen", null, "Entity Name is too short"),
                    message("NonResident.Individual.Address.AddressLine1.minLen", null, "Address is too short"),
                    message("NonResident.Entity.Address.AddressLine1.minLen", null, "Address is too short"),
                    message("NonResident.Individual.Address.AddressLine2.minLen", null, "Address is too short"),
                    message("NonResident.Entity.Address.AddressLine2.minLen", null, "Address is too short"),
                    message("NonResident.Individual.Address.Suburb.minLen", null, "Suburb is too short"),
                    message("NonResident.Entity.Address.Suburb.minLen", null, "Suburb is too short"),
                    message("NonResident.Individual.Address.City.minLen", null, "City is too short"),
                    message("NonResident.Entity.Address.City.minLen", null, "City is too short"),
                    message("NonResident.Individual.Address.State.minLen", null, "State is too short"),
                    message("NonResident.Entity.Address.State.minLen", null, "State is too short"),
                    message("NonResident.Individual.Address.PostalCode.minLen", null, "Code is too short"),
                    message("NonResident.Entity.Address.PostalCode.minLen", null, "Code is too short"),
                    message("NonResident.Individual.Address.Country.minLen", null, "Country code is too short"),
                    message("NonResident.Entity.Address.Country.minLen", null, "Country code is too short"),
                    message("PaymentDetail.BeneficiaryBank.SWIFTBIC.minLen", null, "SWIFT BIC has a minimum of 11 characters"),
                    message("PaymentDetail.BeneficiaryBank.BankName.minLen", null, "Bank name is too short"),
                    message("PaymentDetail.BeneficiaryBank.BranchCode.minLen", null, "Branch code is too short"),
                    message("PaymentDetail.BeneficiaryBank.Address.minLen", null, "Address is too short"),
                    message("PaymentDetail.BeneficiaryBank.City.minLen", null, "City is too short"),
                    message("PaymentDetail.CorrespondentBank.SWIFTBIC.minLen", null, "SWIFT BIC has a minimum of 11 characters"),
                    message("PaymentDetail.CorrespondentBank.BankName.minLen", null, "Bank name is too short"),
                    message("PaymentDetail.CorrespondentBank.BranchCode.minLen", null, "Branch code is too short"),
                    message("PaymentDetail.CorrespondentBank.Address.minLen", null, "Address is too short"),
                    message("PaymentDetail.CorrespondentBank.City.minLen", null, "City is too short")
                  ]
                },
                {
                  field: ["Resident.Individual.ContactDetails.Email",
                    "Resident.Entity.ContactDetails.Email"],
                  rules: [
                    failure('inv_cntft1', "I340", 'Must be completed',
                      isEmpty).onSection("ABEG")
                  ]
                },
                // {
                //   field: "TransactionTotal",
                //   rules: [
                //     failure("inv_troot1", "244", "Please ensure that the sequence amount(s) add up to the total transaction amount",
                //       notTransactionField("ZAREquivalent").and(notSumForeignValue)).onSection("ABCDEG"),
                //     failure("inv_troot2", "244", "Please ensure that the sequence amount(s) add up to the total transaction amount",
                //       hasTransactionField("ZAREquivalent").and(notSumZAREquivalent)).onSection("ABCDEG")
                //   ]
                // },
                // {
                //   field: "ValueDate",
                //   rules: [
                //     warning("vd2", 216, "Submission date cannot be future-dated further than 30 days", notEmpty.and(isDaysInFuture(30))),
                //     warning("vd3", 217, "You are capturing a transaction that occurred in the past", notEmpty.and(hasPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/)).and(isDaysInPast(1))),
                //     failure("inv_vd1", "I25", "Must be in a valid date format: YYYY-MM-DD",
                //         notEmpty.and(notPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/))).onSection("ABG")
                //   ]
                // },
                // {
                //   field: "TrnReference",
                //   rules: [
                //     ignore("tref1")
                //   ]
                // },
                // {
                //   field: "TotalForeignValue",
                //   rules: [
                //     ignore("tfv1"),
                //     ignore("tfv2"),
                //     failure("inv_tfv1", "I37", "A Currency Amount must be provided",
                //       isEmpty).onInflow().onSection("ABCDEG"),
                //     failure("inv_tfv2", "I??", "A Currency Amount or a ZAR equivalent amount must be provided",
                //       isEmpty.and(notTransactionField("ZAREquivalent"))).onOutflow().onSection("ABCDEG")
                //   ]
                // },
                // {
                //   field: "ZAREquivalent",
                //   rules: [
                //     failure("inv_zar1", "I??", "A Currency Amount or a ZAR equivalent amount must be provided",
                //       isEmpty.and(notTransactionField("TotalForeignValue"))).onOutflow().onSection("ABCDEG"),
                //     failure("inv_zar2", "I??", "A ZAR equivalent amount cannot be provided if the the Currency Amount is provided",
                //       notEmpty.and(hasTransactionField("TotalForeignValue"))).onOutflow().onSection("ABCDEG")
                //   ]
                // },
                // {
                //   field: "FeesIncluded",
                //   rules: [
                //     failure("inv_fees1", "I??", "This must be completed",
                //       isEmpty.and(hasTransactionFieldValue("FlowCurrency", "ZAR").or(hasTransactionField("ZAREquivalent")))).onOutflow().onSection("ABCDEG")
                //   ]
                // },
                // {
                //   field: "OriginatingBank",
                //   rules: [
                //     ignore("obank1"),
                //     ignore("obank3"),
                //     ignore("obank4"),
                //     ignore("obank5"),
                //     ignore("obank6"),
                //     ignore("obank7"),
                //     ignore("obank8"),
                //     ignore("obank9")
                //   ]
                // },
                // {
                //   field: "ReceivingBank",
                //   rules: [
                //     ignore("rbank1"),
                //     ignore("rbank3"),
                //     ignore("rbank4"),
                //     ignore("rbank5"),
                //     ignore("rbank6")
                //   ]
                // },
                // {
                //   field: "CorrespondentBank",
                //   rules: [
                //     ignore("cbank1")
                //   ]
                // },
                // {
                //   field: "ReceivingCountry",
                //   rules: [
                //     failure("rcntry5", 234, "Country code may not be ZA",
                //             notEmpty.and(hasValue("ZA")).and(hasTransactionFieldValue("IsInvestecFCA", "N"))).onOutflow().onSection("ABG")
                //   ]
                // },
                // {
                //   field: "RateConfirmation",
                //   rules: [
                //     failure("inv_rate1", "I01", "This must be completed",
                //       isEmpty.and(notCurrencyIn("ZAR"))).onInflow().onSection("A")
                //   ]
                // },
                // {
                //   field: "IsInvestecFCA",
                //   rules: [
                //     failure("inv_iisa1", "I02", "This must be completed",
                //       isEmpty.and(hasTransactionField("FlowCurrency")).and(notCurrencyIn("ZAR"))).onSection("A"),
                //     failure("inv_iisa2", "I??", "This field may not be completed for ZAR payments",
                //       notEmpty.and(hasTransactionField("FlowCurrency")).and(isCurrencyIn("ZAR"))).onSection("A")
                //   ]
                // },
                // {
                //   field: "InvestecFCA",
                //   rules: [
                //     failure("inv_isa1", "I03", "This must be completed",
                //         isEmpty.and(hasTransactionFieldValue("IsInvestecFCA", "Y"))).onSection("A"),
                //     failure("inv_isa2", "I36", "Investec Foreign Currency Account (FCA) is not required and should be deleted",
                //         notEmpty.and(notTransactionFieldValue("IsInvestecFCA", "Y"))).onSection("A"),
                //     failure('inv_isa3', "I??", "Foreign Currency Account and Account to be Debitted cannot be the same",
                //         notEmpty.and(matchesResidentField("AccountNumber"))).onSection("ABCDG")
                //   ]
                // },
                // {
                //   field: "AccountHolderStatus",
                //   rules: [
                //     failure("inv_ahs1", "I04", "This must be completed",
                //       isEmpty).onSection("A"),
                //     failure("inv_ahs2", "I15", "Non Residents must use categories 250 or 251 for travel and not 255 or 256",
                //       hasValueIn(["Non Resident"])).onSection("A").onCategory(["255", "256"]),
                //     failure("inv_ahs3", "I16", "Travel categories 250, 251 are reserved for Non Residents only",
                //       hasValueIn(["South African Resident"])).onSection("A").onCategory(["250", "251"])
                //   ]
                // },
                // {
                //   field: "CounterpartyStatus",
                //   rules: [
                //     failure("inv_cps1", "I05", "This must be completed",
                //       isEmpty).onInflow().onSection("A")
                //   ]
                // },
                // {
                //   field: "LocationCountry",
                //   len: 2,
                //   rules: [
                //     failure("inv_lc1", 405, "Must be completed",
                //             isEmpty).onSection("ABG"),
                //     failure("inv_lc2", 238, "Country where service was requested from or where the merchandise was exported to, is not valid",
                //             notEmpty.and(hasInvalidSWIFTCountry)).onSection("ABG"),
                //     failure("inv_lc3", 290, "Originating Country of Services or Merchandise",
                //             notEmpty.and(hasValue("ZA"))).onSection("ABG"),
                //     failure("inv_lc4", 238, "Country where service was requested from or where merchandise was exported to may be EU may only if the Category is 513",
                //             notEmpty.and(hasValue("EU"))).onSection("AB").notOnCategory("513"),
                //     failure("inv_lc5", 407, "May not be completed",
                //             notEmpty).onSection("CDEF")
                //   ]
                // },
                {
                  field: "PaymentDetail.BeneficiaryBank.SWIFTBIC",
                  minLen: 11,
                  maxLen: 11,
                  rules: [
                    failure("inv_bbbic1", "I60", "SWIFTBIC is mandatory for all European Union payments",
                            isEmpty.and(isTransactionFieldEUCountry("ReceivingCountry"))).onOutflow().onSection("A"),
                    failure("inv_bbbic2", "I??", "Either Beneficiary SWIFTBIC or bank name must be provided",
                            isEmpty.and(notTransactionFieldEUCountry("ReceivingCountry")).and(notTransactionField("PaymentDetail.BeneficiaryBank.BankName"))).onOutflow().onSection("A")
                  ]
                },
                // {
                //   field: "PaymentDetail.BeneficiaryBank.BankName",
                //   minLen: 2,
                //   maxLen: 50,
                //   rules: [
                //     failure("inv_bbnm1", "I61", "Either Beneficiary bank name or SWIFTBIC must be provided",
                //             isEmpty.and(notTransactionField("PaymentDetail.BeneficiaryBank.SWIFTBIC"))).onOutflow().onSection("A")
                //   ]
                // },
                // {
                //   field: "PaymentDetail.BeneficiaryBank.BranchCode",
                //   minLen: 2,
                //   maxLen: 30,
                //   rules: []
                // },
                // {
                //   field: "PaymentDetail.BeneficiaryBank.Address",
                //   minLen: 2,
                //   maxLen: 200,
                //   rules: []
                // },
                // {
                //   field: "PaymentDetail.BeneficiaryBank.City",
                //   minLen: 2,
                //   maxLen: 100,
                //   rules: []
                // },
                // {
                //   field: "ReceivingCountry",
                //   rules: [
                //     failure("inv_bbc", "I62", "The country must be provided for the beneficiary bank",
                //             isEmpty.and(hasTransactionField("PaymentDetail.BeneficiaryBank.SWIFTBIC").
                //                     or(hasTransactionField("PaymentDetail.BeneficiaryBank.BankName")).
                //                     or(hasTransactionField("PaymentDetail.BeneficiaryBank.BranchCode")).
                //                     or(hasTransactionField("PaymentDetail.BeneficiaryBank.Address")).
                //                     or(hasTransactionField("PaymentDetail.BeneficiaryBank.City")))).onOutflow().onSection("A")
                //   ]
                // },
                // {
                //   field: "CorrespondentCountry",
                //   rules: [
                //     failure("inv_cbc1", "I65", "The country must be provided for the beneficiary bank",
                //             isEmpty.and(hasTransactionField("PaymentDetail.CorrespondentBank.SWIFTBIC").
                //                     or(hasTransactionField("PaymentDetail.CorrespondentBank.BankName")).
                //                     or(hasTransactionField("PaymentDetail.CorrespondentBank.BranchCode")).
                //                     or(hasTransactionField("PaymentDetail.CorrespondentBank.Address")).
                //                     or(hasTransactionField("PaymentDetail.CorrespondentBank.City")))).onOutflow().onSection("A")
                //   ]
                // },
                // {
                //   field: "NonResident.Individual.IsMutualParty",
                //   rules: [
                //     failure("inv_nrimp", "I38", "This must be completed",
                //       isEmpty.and(hasTransactionField("Resident.Individual"))).onInflow().onSection("A")
                //   ]
                // },
                // {
                //   field: "NonResident.Entity.IsMutualParty",
                //   rules: [
                //     failure("inv_nremp", "I39", "This must be completed",
                //       isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("A")
                //   ]
                // },
                // {
                //   field: "NonResident.Individual.Surname",
                //   rules: [
                //     ignore("nrsn2"),
                //     failure("inv_nrsn_l1", "I55", "Surname limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
                //   ]
            
                // },
                // {
                //   field: "NonResident.Individual.MiddleNames",
                //   rules: [
                //     failure("inv_nrmn_l1", "I55", "Middle Name is limited to 50 characters", notEmpty.and(isTooLong(50))).onSection("A")
                //   ]
                // },
                // {
                //   field: "NonResident.Individual.Name",
                //   rules: [
                //     failure("inv_nrnm1", "I17", "The Name must be completed",
                //       isEmpty).onSection("ABCDG"),
                //     failure("inv_nrnm_l1", "I55", "Name is limited to 50 characters", notEmpty.and(isTooLong(50))).onSection("A")
                //   ]
                // },
                // {
                //   field: "NonResident.Individual.Gender",
                //   rules: [
                //     failure("inv_nrgn1", "I18", "The Gender must be completed",
                //       isEmpty).onSection("ABCDG")
                //   ]
                // },
                // {
                //   field: "NonResident.Entity.EntityName",
                //   rules: [
                //     ignore("nrlen3"),
                //     failure("inv_nrlen_l1", "I90", "Entity Name is limited to 70 characters", notEmpty.and(isTooLong(70))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
                //   rules: [
                //     ignore("nriaid1"),
                //     ignore("nriaid4"),
                //     ignore("nriaid5"),
                //     ignore("nriaid6"),
                //     ignore("nriaid7"),
                //     ignore("nriaid9"),
                //     ignore("nriaid10"),
                //     ignore("nriaid11")
                //   ]
                // },
                // {
                //   field: ["NonResident.Individual.AccountNumber", "NonResident.Entity.AccountNumber"],
                //   rules: [
                //     ignore("nrian1"),
                //     failure("inv_nrian1", "I??", "The beneficiary account number or IBAN is mandatory",
                //              isEmpty.and(notTransactionFieldValue("IsInvestecFCA", "Y"))).onOutflow().onSection("ABCDG"),
                //     warning("inv_nrian2", "I??", "This country requires an IBAN number. The provided number fails the IBAN validation check and may result in payment processing delays",
                //              notEmpty.and(isTransactionFieldIBANReqCountry("ReceivingCountry").and(notValidIBAN))).onOutflow().onSection("ABCDG"),
                //     warning("inv_nrian3", "I??", "The provided IBAN number fails the IBAN validation check and may result in payment processing delays",
                //              notEmpty.and(notTransactionFieldIBANReqCountry("ReceivingCountry").and(isTransactionFieldIBANCountry("ReceivingCountry")).and(startsWithTransactionField("ReceivingCountry").and(notValidIBAN)))).onOutflow().onSection("ABCDG"),
                //     failure("inv_nrian4", "I??", "Account number is limited to 34 characters",
                //             notEmpty.and(isTooLong(34))).onSection("A"),
                //     failure("inv_nrian5", "I??", "The beneficiary account number/IBAN is not required when paying an internal Investec Foreign Currency account",
                //             notEmpty.and(hasTransactionFieldValue("IsInvestecFCA", "Y"))).onSection("A")
                //   ]
                // },
                // {
                //   field: "NonResident.Individual.PassportNumber",
                //   rules: [
                //     ignore("nrpn1")
                //   ]
                // },
                // {
                //   field: "NonResident.Individual.PassportCountry",
                //   rules: [
                //     ignore("nrpc1")
                //   ]
                // },
                // {
                //   field: ["NonResident.Individual.Address.AddressLine1", "NonResident.Entity.Address.AddressLine1"],
                //   rules: [
                //     failure("inv_nrial11", "I??", "The AddressLine1 must be completed", isEmpty).onSection("A"),
                //     failure("inv_nrial12", "I40", "Address is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["NonResident.Individual.Address.AddressLine2", "NonResident.Entity.Address.AddressLine2"],
                //   rules: [
                //     failure("inv_nrial21", "I41", "Address is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["NonResident.Individual.Address.Suburb", "NonResident.Entity.Address.Suburb"],
                //   rules: [
                //     failure("inv_nrial31", "I??", "The Suburb must be completed", isEmpty).onSection("A"),
                //     failure("inv_nrial32", "I42", "Suburb is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["NonResident.Individual.Address.City", "NonResident.Entity.Address.City"],
                //   rules: [
                //     failure("inv_nric1", "I??", "The City must be completed", isEmpty).onSection("A"),
                //     failure("inv_nric2", "I43", "City is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["NonResident.Individual.Address.State", "NonResident.Entity.Address.State"],
                //   rules: [
                //     failure("inv_nris1", "I??", "The State must be completed", isEmpty).onSection("A"),
                //     failure("inv_nris2", "I44", "State is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["NonResident.Individual.Address.PostalCode", "NonResident.Entity.Address.PostalCode"],
                //   rules: [
                //     failure("inv_nriz1", "I??", "The Code must be completed", isEmpty).onSection("A")
                //   ]
                // },
                // {
                //   field: "Resident",
                //   rules: [
                //     failure("rg1", 277, "Must contain one of either an Individual or Entity Applicant Type",
                //             notTransactionField("Resident.Individual").and(notTransactionField("Resident.Entity"))).onSection("ABCEG"),
                //     failure("rg2", 291, "If category 255 is used, an Entity Applicant Type must be completed",
                //             notTransactionField("Resident.Entity")).onCategory("255").onSection("AB"),
                //     failure("rg3", 293, "If category 256 is used, an Individual Applicant Type must be completed",
                //             notTransactionField("Resident.Individual")).onCategory("256").onSection("AB"),
                //     failure("rg4", 292, "If category is 511/01 to 511/07, 514/01 to 514/07 or 515/01 to 515/07 is specified an Entity Applicant Type may not be used",
                //             hasTransactionField("Resident.Entity")).onCategory(["511","514","515"]).onSection("A"),
                //     failure("rg6", 292, "If category 303, 304, 305, 306, 416 or 417 is used, an Entity Applicant Type may not be used",
                //             hasTransactionField("Resident.Entity").or(hasTransactionField("Resident.Exception"))).onInflow().onCategory(["303","304","305","306","416","417"]).onSection("AB")
                //   ]
                // },
                // {
                //   field: "Resident.Individual.Surname",
                //   rules: [
                //     failure("risn1", 254, "Surname is required", isEmpty).onSection("ABCEG"),
                //     ignore("risn2"),
                //     failure("inv_risn1", "I55", "Surname limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
                //   ]
                // },
                // {
                //   field: "Resident.Individual.Name",
                //   rules: [
                //     failure("rin1", 256, "Name is required", isEmpty.and(hasTransactionField("Resident.Individual"))).onSection("ABCEG"),
                //     failure("inv_rin1", "I56", "Name is limited to 50 characters", notEmpty.and(isTooLong(50))).onSection("A")
                //   ]
                // },
                // {
                //   field: "Resident.Individual.MiddleNames",
                //   rules: [
                //     failure("inv_rimn1", "I57", "Middle Name is limited to 50 characters", notEmpty.and(isTooLong(50))).onSection("A")
                //   ]
                // },
                // {
                //   field: "Resident.Individual.IDNumber",
                //   rules: [
                //     failure("riidn1", 298, "The ID Number must be completed",
                //             isEmpty).onSection("AB").onCategory(["511","512","513","514","515"]),
                //     failure("riidn3", 297, "ID number is not valid",
                //             notEmpty.and(notValidRSAID)).onSection("ABEG"),
                //     failure("riidn4", 294, "ID number is required for South African residents",
                //             isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "South African Resident"))).onSection("ABE"),
                //     failure("inv_riidn1", "I08", "ID number is only required for South African residents",
                //             notEmpty.and(notTransactionFieldValue("AccountHolderStatus", "South African Resident"))).onSection("ABE")
                //   ]
                // },
                // {
                //   field: "Resident.Individual.DateOfBirth",
                //   rules: [
                //     failure("ridob2", 215, "Must be in a valid date format: YYYY-MM-DD",
                //             notEmpty.and(notPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/))).onSection("ABEG"),
                //     failure("inv_ridob1", "I35", "Date of birth not valid",
                //             notEmpty.and(hasPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/)).and(isDaysInFuture(0))).onSection("ABEG")
                //   ]
                // },
                // {
                //   field: "Resident.Individual.TempResPermitNumber",
                //   rules: [
                //     failure("ritrpn3", 294, "For a Foreign Temporary Resident the Temporary Residence Permit Number must be completed",
                //         isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "Foreign Temporary Resident"))).onSection("AB"),
                //     failure("inv_ritrpn1", "I09", "Temporary resident permit number is only required for foreign temporary residents",
                //         notEmpty.and(notTransactionFieldValue("AccountHolderStatus", "Foreign Temporary Resident"))).onSection("AB"),
                //     failure("inv_ritrpn2", "I53", "Permit number limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
                //   ]
                // },
                // {
                //   field: "Resident.Individual.TempResExpiryDate",
                //   rules: [
                //     failure("inv_ritrpe1", "I21", "For a Foreign Temporary Resident the Temporary Residence Permit Expiry Date must be completed",
                //         isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "Foreign Temporary Resident"))).onSection("AB"),
                //     failure("inv_ritrpe2", "I22", "Only a Foreign Temporary Resident can provide a Temporary Residence Permit Expiry Date",
                //         notEmpty.and(notTransactionFieldValue("AccountHolderStatus", "Foreign Temporary Resident"))).onSection("AB"),
                //     failure("inv_ritrpe3", "I23", "Must be in a valid date format: YYYY-MM-DD",
                //         notEmpty.and(notDatePattern)).onSection("ABG"),
                //     warning("inv_ritrpe4", "I24", "This Temporary Resident Permit Number has expired",
                //         notEmpty.and(hasDatePattern).and(isDaysInPast(0))).onSection("ABG")
                //   ]
                // },
                // {
                //   field: "Resident.Individual.ForeignIDNumber",
                //   rules: [
                //     ignore("rifidn3"),
                //     ignore("rifidn4")
                //   ]
                // },
                // {
                //   field: "Resident.Individual.PassportNumber",
                //   rules: [
                //     failure("ripn1", 301, "If category 255 is used, Traveller Passport Number must be completed",
                //             notEmpty).onSection("AB").onCategory("255"),
                //     failure("inv_ripn1", 204, "If the category is 250 or 251, the Passport Number must be completed",
                //         isEmpty).onSection("A").onCategory(["250", "251"]),
                //     failure("inv_ripn2", 294, "For a Non Resident Individual the Passport Number must be completed",
                //         isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "Non Resident"))).onSection("AB"),
                //     failure("inv_ripn3", "I10", "If category 256 is used, for a South African Resident Individual the Passport Number must be completed",
                //         isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "South African Resident"))).onSection("AB").onCategory("256"),
                //     failure("inv_ripn4", "I52", "This field is too long and must be shortened to 15 characters", notEmpty.and(isTooLong(15))).onSection("A")
                //   ]
                // },
                // {
                //   field: "Resident.Individual.PassportExpiryDate",
                //   rules: [
                //     failure("inv_ripe1", "I11", "If category 255 is used, Traveller Passport Expiry Date must be completed",
                //         notEmpty).onSection("AB").onCategory("255"),
                //     failure("inv_ripe2", "I12", "If the category is 250 or 251, the Passport Expiry must be completed",
                //         isEmpty).onSection("A").onCategory(["250", "251"]),
                //     failure("inv_ripe3", "I13", "For a Non Resident the Passport Expiry must be completed",
                //         isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "Non Resident"))).onSection("AB"),
                //     failure("inv_ripe4", "I14", "If category 256 is used, for a South African Resident the Passport Expiry must be completed",
                //         isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "South African Resident"))).onSection("AB").onCategory("256"),
                //     failure("inv_ripe5", "I20", "Must be in a valid date format: YYYY-MM-DD",
                //         notEmpty.and(notDatePattern)).onSection("ABG"),
                //     warning("inv_ripe6", "I21", "This passport has expired",
                //         notEmpty.and(hasDatePattern).and(isDaysInPast(0))).onSection("ABG")
                //   ]
                // },
                // {
                //   field: "Resident.Entity.EntityName",
                //   rules: [
                //     failure("relen1", 304, "Required field for entities",
                //             isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("ABCE"),
                //     failure("inv_relen1", "I54", "Entity Name is limited to 70 characters", notEmpty.and(isTooLong(70))).onSection("A")
                //   ]
                // },
                // {
                //   field: "Resident.Entity.TradingName",
                //   rules: [
                //     ignore("retn1")
                //   ]
                // },
                // {
                //   field: "Resident.Entity.RegistrationNumber",
                //   rules: [
                //     failure("rern1", 306, "Required field for entities",
                //             isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("ABEG")
                //   ]
                // },
                // {
                //   field: "Resident.Entity.InstitutionalSector",
                //   rules: [
                //     ignore("reis1")
                //   ]
                // },
                // {
                //   field: "Resident.Entity.IndustrialClassification",
                //   rules: [
                //     ignore("reic1")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.AccountName", "Resident.Entity.AccountName"],
                //   rules: [
                //     ignore("an1")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
                //   rules: [
                //     ignore("accid1"),
                //     ignore("accid5"),
                //     ignore("accid6"),
                //     ignore("accid7")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.AccountNumber", "Resident.Entity.AccountNumber"],
                //   rules: [
                //     failure('inv_accno1', 279, "Must be completed",
                //             isEmpty).onInflow().onSection("ABCDG")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],
                //   rules: [
                //     failure('ccn1', 320, 'Must be completed if category is 101/01 to 101/10, 103/01 to 103/10, 105 or 106',
                //             isEmpty).onInflow().onSection("AB").notOnCategory(['101/11','103/11']).onCategory(['101','103','105','106']),
                //     failure('ccn3', 322, 'CustomsClientNumber must be numeric and contain exactly 8 digits',
                //             notEmpty.and(notValidCCN)).onInflow().onSection("AB").notOnCategory(['101/11','103/11']).onCategory(['101','103','105','106']),
                //     ignore("ccn7")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.TaxNumber", "Resident.Entity.TaxNumber"],
                //   rules: [
                //     failure("inv_tn1", "I45", "Tax number is limited to 30 characters", notEmpty.and(isTooLong(30))).onSection("A"),
                //     failure("inv_tn2", "I46", "Tax number can only contain numeric values", notEmpty.and(notPattern(/^\d+$/))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.VATNumber", "Resident.Entity.VATNumber"],
                //   rules: [
                //     failure('vn1', 326, 'VAT number is required for the category chosen',
                //             isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("AB").notOnCategory(['101/11','102/11','103/11','104/11']).onCategory(['101','102','103','104','105','106'])
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.TaxClearanceCertificateIndicator","Resident.Entity.TaxClearanceCertificateIndicator"],
                //   rules: [
                //     ignore('tcci1')
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.TaxClearanceCertificateReference","Resident.Entity.TaxClearanceCertificateReference"],
                //   rules: [
                //     failure('inv_tccr1', "I??", 'The Tax Clearance Certificate Reference number is not required',
                //             notEmpty.and(not(hasResidentFieldValue('TaxClearanceCertificateIndicator','Y')))).onSection("AB")
                //     ]
                // },
                // {
                //   field: ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1", "Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1"],
                //   rules: [
                //     ignore('a1_1')
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb", "Resident.Individual.PostalAddress.Suburb", "Resident.Entity.PostalAddress.Suburb"],
                //   rules: [
                //     ignore('s1')
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City", "Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City"],
                //   rules: [
                //     ignore('c1')
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1"],
                //   rules: [
                //     failure('inv_a1_1', 332, 'Must be completed', isEmpty).onSection("ABEG"),
                //     failure("inv_a1_2", "I47", "Address is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.StreetAddress.AddressLine2","Resident.Entity.StreetAddress.AddressLine2"],
                //   rules: [
                //     failure("inv_a2_1", "I48", "Address is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb"],
                //   rules: [
                //     failure('inv_s1', 333, 'Must be completed',
                //             isEmpty.and(hasResidentFieldValue("StreetAddress.Country", "ZA"))).onSection("ABEG"),
                //     failure("inv_s2", "I49", "Suburb is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City"],
                //   rules: [
                //     failure('inv_c1', 334, 'Must be completed', isEmpty).onSection("ABEG"),
                //     failure("inv_c2", "I50", "City is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province", "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
                //   rules: [
                //     ignore('p1')
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province"],
                //   rules: [
                //     failure('inv_p1', 336, 'Must be valid South African province',
                //       notValueIn(["GAUTENG", "LIMPOPO", "NORTH WEST", "WESTERN CAPE", "EASTERN CAPE", "NORTHERN CAPE", "FREE STATE", "MPUMALANGA", "KWAZULU NATAL"]).and(hasResidentFieldValue("StreetAddress.Country", "ZA"))).onSection("ABG"),
                //     failure('inv_p2', "I34", 'Must be completed',
                //       isEmpty.and(hasResidentFieldValue("StreetAddress.Country", "ZA"))).onSection("ABG"),
                //     failure("inv_p3", "I51", "Province is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.StreetAddress.PostalCode", "Resident.Entity.StreetAddress.PostalCode"],
                //   rules: [
                //     failure('spc1', 338, 'Invalid postal code',
                //             notEmpty.and(notPattern(/^([0-9]){4}$/)).and(hasResidentFieldValue("StreetAddress.Country", "ZA"))).onSection("ABEG"),
                //     failure('inv_spc1', "I33", 'Must be completed',
                //            isEmpty.and(hasResidentFieldValue("StreetAddress.Country", "ZA"))).onSection("ABEG"),
                //     failure("inv_spc_l1", "I51.1", "Postal code is limited to 10 characters",
                //             notEmpty.and(isTooLong(10))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.PostalAddress.PostalCode", "Resident.Entity.PostalAddress.PostalCode"],
                //   rules: [
                //     ignore('pc2')
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.StreetAddress.Country", "Resident.Entity.StreetAddress.Country"],
                //   rules: [
                //     failure('inv_rsac1', "I06", 'Must be completed',
                //       isEmpty).onSection("ABG"),
                //     failure('inv_rsac2', "I??", 'Please accept the address mandate or provide a South African address',
                //       notEmpty.and(notValue("ZA").and(notResidentFieldValue("StreetAddress.Mandate", "ACCEPT")).
                //               and(hasTransactionFieldValue("AccountHolderStatus", ["South African Resident", "Foreign Temporary Resident"])))).onSection("ABG")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.StreetAddress.Mandate", "Resident.Entity.StreetAddress.Mandate"],
                //   rules: [
                //     failure('inv_rsam1', "I??", 'Must be completed',
                //       isEmpty.and(hasResidentField("StreetAddress.Country")).and(notResidentFieldValue("StreetAddress.Country", "ZA")).
                //               and(hasTransactionFieldValue("AccountHolderStatus", ["South African Resident", "Foreign Temporary Resident"]))).onSection("ABG"),
                //     failure('inv_rsam2', "I??", 'You must either accept the use of the Investec address or provide a valid South African address',
                //       notEmpty.and(notValue("ACCEPT")).and(notResidentFieldValue("StreetAddress.Country", "ZA")).
                //                and(hasTransactionFieldValue("AccountHolderStatus", ["South African Resident", "Foreign Temporary Resident"]))).onSection("ABG"),
                //     failure('inv_rsam3', "I??", 'Must not be provided for a Resident South African address or Non Resident addresses',
                //       notEmpty.and(hasResidentFieldValue("StreetAddress.Country", "ZA").or(hasTransactionFieldValue("AccountHolderStatus", ["Non Resident"])))).onSection("ABG")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.ContactDetails.ContactSurname","Resident.Entity.ContactDetails.ContactSurname"],
                //   rules: [
                //     failure("inv_csn1", "I58", "Surname limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.ContactDetails.ContactName","Resident.Entity.ContactDetails.ContactName"],
                //   rules: [
                //     failure("inv_cn1", "I59", "Name is limited to 50 characters", notEmpty.and(isTooLong(50))).onSection("A")
                //   ]
                // },
                // {
                //   field: ["Resident.Individual.ContactDetails.Email", "Resident.Entity.ContactDetails.Email"],
                //   rules: [
                //     failure('inv_cnte1', "I34", 'Invalid email address',
                //       notEmpty.and(notValidEmail)).onSection("ABEG")
                //   ]
                // },            
                  {
                    field: ["Resident.Individual.StreetAddress.Country", "Resident.Entity.StreetAddress.Country"],
                    rules: [
                      ignore('inv_rsac1'),
                      ignore('inv_rsac2')
                    ]
                  },
                  {
                   field: ["Resident.Individual.StreetAddress.Mandate", "Resident.Entity.StreetAddress.Mandate"],
                    rules: [
                        ignore("inv_rsam1"),
                        ignore("inv_rsam2"),
                        ignore("inv_rsam3")
                    ]
                  },
                  {
                    field: "FeesIncluded",
                    rules: [
                      ignore("inv_fees1")
                    ]
                  },
                  {
                    field: "IsInvestecFCA",
                    rules: [
                      ignore("inv_iisa1"),
                      ignore("inv_iisa2")
                    ]
                  },
                  {
                    field: "InvestecFCA",
                    len: 13,
                    rules: [
                      ignore("inv_isa1"),
                      ignore("inv_isa2"),
                      ignore('inv_isa3'),
                      ignore('inv_isa4'),
                      ignore('inv_isa5')
                    ]
                  },
                  {
                    field: "RateConfirmation",
                    rules: [
                      ignore("inv_rate1")
                    ]
                  },
                  {
                    field: "NonResident.Individual.IsMutualParty",
                    rules: [
                      ignore("inv_nrimp")
                    ]
                  },
                  {
                    field: "NonResident.Entity.IsMutualParty",
                    rules: [
                      ignore("inv_nremp")
                    ]
                  },
                  {
                    //TEST CODE!!!
                    field: ["Resident.Individual.ContactDetails.ContactName", "Resident.Individual.ContactDetails.ContactSurname", 
                    "Resident.Entity.ContactDetails.ContactName", "Resident.Entity.ContactDetails.ContactSurname", 
                    "Resident.Individual.Name", "Resident.Individual.Surname", 
                    "NonResident.Individual.Name", "NonResident.Individual.Surname"],
                    rules: [
                      failure("inv_in_name", "I46", "Name can only contain alphabet characters", 
                        notEmpty.and(notPattern(/^[a-zA-Z ]+$/)))
                      ]
                  },
              ]
          }
      };

      return transaction;
  }

});

