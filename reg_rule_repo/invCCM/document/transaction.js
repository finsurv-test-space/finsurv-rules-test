define(function () {
  return function (predef) {
    var docs;
    with (predef) {
      docs = {
        ruleset: "Transaction Documents",
        scope: "transaction",
        validations: [
          {
            field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
            rules: [
              ignore('dtcc1')
            ]
          },
          {
            field: ["TrnReference"],
            rules: [
              document('ccm-id1', "ID_Document", "Please provide a scan of your ID Document",
                hasTransactionField('Resident.Individual').and(hasTransactionFieldValue('AccountHolderStatus',['South African Resident']))).onOutflow().onSection("AB"),
              document('ccm-decl1', "SDA_Declaration", "Please provide a scan of the declaration that you are remaining within your SDA",
                hasTransactionField('Resident.Individual').and(hasTransactionFieldValue('AccountHolderStatus',['South African Resident']))).onOutflow().onSection("AB")
            ]
          },
          {
            field: "Resident.Individual.IDNumber",
            rules: [
              ignore("drid1")
            ]
          },
        ]
      };
    }
    return docs;
  }
});