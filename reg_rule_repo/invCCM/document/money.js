define(function () {
  return function (predef) {
    var docs;
    with (predef) {
      docs = {
        ruleset: "Money Documents",
        scope: "money",
        validations: [
          {
            field: "SequenceNumber",
            rules: [
              ignore("dmsnpl1"),
              ignore("dmsntc1"),
              
              document("inv_dmsnpl1", "PINLetter", "SARS PIN Letter",
                notEmpty.and(hasMoneyFieldValue("TaxClearanceCertificateIndicator","Y"))).onOutflow().onCategory(["512","513"]),

              document("inv_dmsntc1", "TaxClearance", "Provide scan of the Tax Clearance certificate",
                notEmpty.and(hasMoneyFieldValue("TaxClearanceCertificateIndicator","Y"))).onOutflow().onCategory(["512","513"]),

             
              
            ]
          }
        ]
      };
    }
    return docs;
  }
});
