define({ customRules: [
    {
      ruleset: "Investec Transaction Summary Display Rules",
      scope  : "transaction",
      fields: [
        {
            field: "MainTransferPurpose",
            display: [
                hide()
            ]
        },
        {
            field: "TransactionAdd",
            display: [
                appendValue("(Click to add Additional Transaction Details)", null, notTransactionField("InvestecFCA").and(notTransactionField("LocationCountry"))),
                appendValue("FCA/CFC: %s", "InvestecFCA", hasTransactionField("InvestecFCA")),
                appendValue("%s", "LocationCountry", hasTransactionField("LocationCountry").and(notTransactionField("InvestecFCA")))
            ]
        },
        {
            field: "NonResident",
            display: [
                setValue("Beneficiary Details").onOutflow(),
                setValue("Remitter Details").onInflow()
            ]
        },
        {
            field: "Resident.Type",
            display: [
                setValue("Individual",null,hasTransactionField("Resident.Individual")),
                setValue("Entity",null,hasTransactionField("Resident.Entity")),
                setValue("Exception",null,hasTransactionField("Resident.Exception"))
            ]
        },
        {
            field: "Resident.Description",
            display: [
                appendValue("%s", "Resident.Individual.Name",hasTransactionField("Resident.Individual")),
                appendValue(" %s", "Resident.Individual.Surname",hasTransactionField("Resident.Individual")),
                appendValue("%s", "Resident.Entity.EntityName",hasTransactionField("Resident.Entity")),
                appendValue(" t/a %s", "Resident.Entity.TradingName",hasTransactionField("Resident.Entity.TradingName")),
                appendValue("%s", "Resident.Exception.ExceptionName",hasTransactionField("Resident.Exception")),
                appendValue("(Click to add Applicant Details)", null, hasTransactionField("Resident.Individual").and(notTransactionField("Resident.Individual.Surname").and(notTransactionField("Resident.Individual.Name")))),
                appendValue("(Click to add Applicant Details)", null, hasTransactionField("Resident.Entity").and(notTransactionField("Resident.Entity.EntityName").and(notTransactionField("Resident.Entity.TradingName"))))
            ]
        },
        {
            field: "Resident.AccountDescription",
            display: [
                appendValue("%s", "Resident.Individual.AccountIdentifier", hasTransactionField("Resident.Individual")),
                appendValue(": %s", "Resident.Individual.AccountNumber", hasTransactionField("Resident.Individual")),
                appendValue(", %s", "Resident.Individual.AccountName", hasTransactionField("Resident.Individual")),
                appendValue("%s", "Resident.Entity.AccountIdentifier", hasTransactionField("Resident.Entity")),
                appendValue(": %s", "Resident.Entity.AccountNumber", hasTransactionField("Resident.Entity")),
                appendValue(", %s", "Resident.Entity.AccountName", hasTransactionField("Resident.Entity"))
            ]
        },
        {
            field: "Resident.ContactDescription",
            display: [
              appendValue("%s", "Resident.Individual.ContactDetails.ContactName", hasTransactionField("Resident.Individual").and(hasTransactionField("Resident.Individual.ContactDetails.ContactName"))),
              appendValue(" %s", "Resident.Individual.ContactDetails.ContactSurname", hasTransactionField("Resident.Individual").and(hasTransactionField("Resident.Individual.ContactDetails.ContactSurname"))),
              appendValue(", <i>email</i>: %s", "Resident.Individual.ContactDetails.Email", hasTransactionField("Resident.Individual").and(hasTransactionField("Resident.Individual.ContactDetails.Email"))),
              appendValue("%s", "Resident.Entity.ContactDetails.ContactName", hasTransactionField("Resident.Entity").and(hasTransactionField("Resident.Entity.ContactDetails.ContactName"))),
              appendValue(" %s", "Resident.Entity.ContactDetails.ContactSurname", hasTransactionField("Resident.Entity").and(hasTransactionField("Resident.Entity.ContactDetails.ContactSurname"))),
              appendValue(", <i>email</i>: %s", "Resident.Entity.ContactDetails.Email", hasTransactionField("Resident.Entity").and(hasTransactionField("Resident.Entity.ContactDetails.Email"))),
              appendValue("(Click to add Address and Contact Details)", null,
                      hasTransactionField("Resident.Individual").and(notTransactionField("Resident.Individual.ContactDetails.ContactName").and(notTransactionField("Resident.Individual.ContactDetails.ContactSurname")).and(notTransactionField("Resident.Individual.ContactDetails.Email")))),
              appendValue("(Click to add Address and Contact Details)", null,
                      hasTransactionField("Resident.Entity").and(notTransactionField("Resident.Entity.ContactDetails.ContactName").and(notTransactionField("Resident.Entity.ContactDetails.ContactSurname")).and(notTransactionField("Resident.Entity.ContactDetails.Email"))))
            ]
        },
        {
            field: "NonResident.Type",
            display: [
                setValue("Individual",null,hasTransactionField("NonResident.Individual")),
                setValue("Entity",null,hasTransactionField("NonResident.Entity")),
                setValue("Exception",null,hasTransactionField("NonResident.Exception"))
            ]
        },
        {
            field: "NonResident.Description",
            display: [
                appendValue("%s", "NonResident.Individual.Name",hasTransactionField("NonResident.Individual")).onSection("ABCDG"),
                appendValue(" %s", "NonResident.Individual.Surname",hasTransactionField("NonResident.Individual")).onSection("ABCDG"),
                appendValue("%s", "NonResident.Entity.EntityName",hasTransactionField("NonResident.Entity")).onSection("ABCDG"),
                appendValue("%s", "NonResident.Entity.CardMerchantName",hasTransactionField("NonResident.Entity.CardMerchantName")).onSection("E"),
                appendValue("%s", "NonResident.Exception.ExceptionName",hasTransactionField("NonResident.Exception")).onSection("ABCDG"),
                appendValue("(Click to add Remitter Details)", null,
                        hasTransactionField("NonResident.Individual").and(notTransactionField("NonResident.Individual.Name").and(notTransactionField("NonResident.Individual.Surname")))).onInflow(),
                appendValue("(Click to add Remitter Details)", null,
                        hasTransactionField("NonResident.Entity").and(notTransactionField("NonResident.Entity.EntityName"))).onInflow(),
              appendValue("(Click to add Beneficiary Details)", null,
                      hasTransactionField("NonResident.Individual").and(notTransactionField("NonResident.Individual.Name").and(notTransactionField("NonResident.Individual.Surname")))).onOutflow(),
              appendValue("(Click to add Beneficiary Details)", null,
                      hasTransactionField("NonResident.Entity").and(notTransactionField("NonResident.Entity.EntityName"))).onOutflow()
            ]
        },
        {
            field: "NonResident.BankDescription",
            display: [
                appendValue("%s", "PaymentDetail.BeneficiaryBank.BankName", hasTransactionField("PaymentDetail.BeneficiaryBank.BankName")).onOutflow().onSection("ABCDG"),
                appendValue(" (%s)", "PaymentDetail.BeneficiaryBank.SWIFTBIC", hasTransactionField("PaymentDetail.BeneficiaryBank.BankName")).onOutflow().onSection("ABCDG"),
                appendValue("%s", "PaymentDetail.BeneficiaryBank.SWIFTBIC", notTransactionField("PaymentDetail.BeneficiaryBank.BankName")).onOutflow().onSection("ABCDG"),
                appendValue("(Click to add Beneficiary Bank Details)", null,
                        notTransactionField("PaymentDetail.BeneficiaryBank.SWIFTBIC").and(notTransactionField("PaymentDetail.BeneficiaryBank.BankName"))).onOutflow()
            ]
        },
        {
            field: "NonResident.Account",
            display: [
                show(notTransactionField("NonResident.Exception")),
                hide(hasTransactionField("NonResident.Exception"))
            ]
        },
        {
            field: "NonResident.AccountDescription",
            display: [
                appendValue("%s", "NonResident.Individual.AccountIdentifier", hasTransactionField("NonResident.Individual")),
                appendValue(": %s", "NonResident.Individual.AccountNumber", hasTransactionField("NonResident.Individual")),
                appendValue("%s", "NonResident.Entity.AccountIdentifier", hasTransactionField("NonResident.Entity")),
                appendValue(": %s", "NonResident.Entity.AccountNumber", hasTransactionField("NonResident.Entity"))
            ]
        },
        {
            field: "NonResident.Address",
            display: [
                show(notTransactionField("NonResident.Exception")),
                hide(hasTransactionField("NonResident.Exception"))
            ]
        },
        {
            field: "Resident.Account",
            display: [
                hide()
            ]
        },
        {
            field: "Resident.ContactDetails",
            display: [
                show(notTransactionField("Resident.Exception")),
                hide(hasTransactionField("Resident.Exception"))
            ]
        },
        {
          field: "resident_table",
          display : [
                     show(),
                     hide().onSection("F")
                     ]
        },
        {
          field: "non_resident_table",
          display : [
                     show(),
                     hide().onSection("F")
                     ]
        }
      ]
    },
    {
      ruleset: "Investec Money Summary Display Rules",
      scope  : "money",
      fields: [
        {
          field: "AddMoneyButton",
          display: [
           hide(notEmptyMoneyField).onSection("CDEFG")
          ]
        },
        {
          field: "Description",
          display: [
            appendValue("%s", categoryDescription).onSection("AB"),
            appendValue(", Loan Ref: %s", "LoanRefNumber").onSection("AB").onCategory(["801","802","803","804"]),
            appendValue(", Loan Ref: %s", "LoanRefNumber").onOutflow().onSection("AB").onCategory(["106","309/04","309/05","309/06","309/07"]),
            appendValue(", Loan Tenor: %s", "LoanTenor").onOutflow().onSection("AB").onCategory(["810","815","816","817","818","819"]),
            appendValue(", Loan Int: %s", "LoanInterestRate").onOutflow().onSection("AB").onCategory(["810","815","816","817","818","819"]),
            appendValue(", Loan Int: %s", "LoanInterestRate").onOutflow().onSection("ABG").onCategory(["309/04","309/05","309/06","309/07"]),
            appendValue(", Loan Int: %s", "LoanInterestRate").onInflow().onSection("ABG").onCategory(["309/01","309/02","309/03","309/04","309/05","309/06","309/07"]),
            appendValue(", CCN: %s", "transaction::Resident.Individual.CustomsClientNumber").onInflow().onSection("AB").onCategory(["101","103","105","106"]),
            appendValue(", CCN: %s", "transaction::Resident.Individual.CustomsClientNumber").onOutflow().onSection("AB").onCategory(["101","102","103","104","105","106"]) ,
            appendValue(", CCN: %s", "transaction::Resident.Entity.CustomsClientNumber").onInflow().onSection("AB").onCategory(["101","103","105","106"]),
            appendValue(", CCN: %s", "transaction::Resident.Entity.CustomsClientNumber").onOutflow().onSection("AB").onCategory(["101","102","103","104","105","106"]),
            appendValue(", CCN2: %s", "ThirdParty.CustomsClientNumber").onSection("AB"),
            appendValue("(Click to add Reporting Category Details)", null, notMoneyField("CategoryCode")).onSection("AB")
          ]
        },
        {
            field: "ThirdParty.Type",
            display: [
              setValue("Not Required", null),
              setValue("Traveller", null).onSection("AB").onCategory(["255","256"]),
              setValue("Third Party", null).onOutflow().onSection("AB").onCategory(["511","512","513"])
            ]
        },
        {
            field: "ThirdParty",
            display: [
              hide(),
              show().onSection("AB").onCategory(["255", "256"]),
              show(hasTransactionField("Resident.Entity")).onOutflow().onSection("AB").onCategory(["511","512","513"]),
              appendValue("Individual: %s, ", "ThirdParty.Individual.Surname", notMoneyField("ThirdParty.Entity.Name")),
              appendValue("%s", "ThirdParty.Individual.Name", notMoneyField("ThirdParty.Entity.Name")),
              appendValue("Entity: %s", "ThirdParty.Entity.Name", hasMoneyField("ThirdParty.Entity.Name")),
              appendValue("Individual: %s, ", "Resident.Individual.Surname", hasMoneyFieldValue("TravelMode.TravellerStatus", "Account Holder")),
              appendValue("%s, ", "Resident.Individual.Name", hasMoneyFieldValue("TravelMode.TravellerStatus", "Account Holder"))
            ]
        },
        {
            field: "ThirdPartyContact",
            display: [
              hide()
            ]
        },
        {
            field: "ImportExport",
            display: [
                hide(),
                appendValue("multiple", null,
                        multipleImportExport),
                appendValue("ICN: %s", "importexport::ImportControlNumber",
                        singleImportExport).onOutflow().onSection("ABG").onCategory(["101"]),
                appendValue("MRN: %s", "importexport::ImportControlNumber",
                        singleImportExport).onOutflow().onSection("AB").onCategory(["103","105","106"]),
                appendValue("UCR: %s", "importexport::UCR",
                        singleImportExport).onInflow().onSection("ABG").onCategory(["101","103","105","106"]),
                appendValue("Transport DN: %s", "importexport::TransportDocumentNumber",
                        singleImportExport).onOutflow().onSection("ABG").onCategory(["103","105","106"]),
                show(notEmptyImportExport),
                show().onSection("ABG").onCategory(["101", "103", "105", "106"])
            ]
        }
      ]
    },
    {
      ruleset: "Investec Import/Export Summary Display Rules",
      scope  : "importexport",
      fields: [
        {
            field: "Description",
            display: [
                appendValue("ICN: %s", "ImportControlNumber").onOutflow().onSection("ABG").onCategory(["101"]),
                appendValue("MRN: %s", "ImportControlNumber").onOutflow().onSection("AB").onCategory(["103","105","106"]),
                appendValue("UCR: %s", "UCR").onInflow().onSection("ABG").onCategory(["101","103","105","106"]),
                appendValue("Transport DN: %s", "TransportDocumentNumber").onOutflow().onSection("ABG").onCategory(["103","105","106"])
            ]
        }
      ]
    }
  ]
  }
)