define(function () {
  return function (predef) {
    var detailTrans, detailMoney, detailImportExport;
    with (predef) {

      detailTrans = {
        ruleset: "invCCM Transaction Display Rules",
        scope: "transaction",
        fields: [
          {
            field  : "ValueDate",
            display: [
              show(),
              disable(isTransactionFieldProvided("ValueDate"))
            ]
          },
          {
            field  : "TrnReference",
            display: [
              hide()
            ]
          },
          {
            field  : "FlowCurrency",
            display: [
              show()
            ]
          },
          {
            field  : "TotalForeignValue",
            display: [
              show()
            ]
          },
          {
            field  : "ZAREquivalent",
            display: [
              hide(),
              show(notTransactionFieldValue("FlowCurrency", "ZAR").and(notTransactionField("TotalForeignValue"))).onOutflow().onSection("A")
            ]
          },
          {
            field  : "FeesIncluded",
            display: [
              hide(),
              show(hasTransactionFieldValue("FlowCurrency", "ZAR").or(hasTransactionField("ZAREquivalent"))).onOutflow().onSection("A")
            ]
          },
          {
            field: "BranchCode",
            display: [
              hide(),
              disable()
            ]
          },
          {
            field: "BranchName",
            display: [
              hide(),
              disable()
            ]
          },
          {
            field  : "OriginatingBank",
            display: [
              hide(),
              disable()
            ]
          },
          {
            field  : "OriginatingCountry",
            display: [
              hide(),
              disable()
            ]
          },
          {
            field  : "AccountHolderStatus",
            display: [
              show()
              //disable(isTransactionFieldProvided("AccountHolderStatus"))
            ]
          },
          {
            field  : "CounterpartyStatus",
            display: [
              show(),
              disable(isTransactionFieldProvided("CounterpartyStatus"))
              //setValue("%s", "AccountHolderStatus", notMatchesTransactionField("AccountHolderStatus").and(hasTransactionField("AccountHolderStatus").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y"))))),
              //disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y")).or(isTransactionFieldProvided("CounterpartyStatus"))),
              //enable(not(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y")).or(isTransactionFieldProvided("CounterpartyStatus"))))
            ]
          },
          {
            field  : "RateConfirmation",
            display: [
              show(),
              hide(isCurrencyIn("ZAR")),
              setValue("N", null, notTransactionField("RateConfirmation"))
            ]
          },
          {
            field  : "IsInvestecFCA",
            display: [
              clearValue(hasTransactionField("IsInvestecFCA").and(isCurrencyIn("ZAR"))),
              show(),
              hide(notTransactionField("FlowCurrency").or(isCurrencyIn("ZAR"))),
              disable(isTransactionFieldProvided("IsInvestecFCA")).onOutflow()
            ]
          },
          {
            field  : "InvestecFCA",
            display: [
              hide(),
              show(notCurrencyIn("ZAR").and(hasTransactionFieldValue("IsInvestecFCA", "Y"))),
              disable(isTransactionFieldProvided("InvestecFCA")).onOutflow()
            ]
          },
          {
            field: "LocationCountry",
            display: [
              hide().onSection("CDEF")
            ]
          },
          {
            field  : "PaymentDetail.Charges",
            display: [
              hide(),
              show(notTransactionFieldValue("IsInvestecFCA", "Y")).onOutflow().onSection("A"),
              disable(isTransactionFieldProvided("PaymentDetail.Charges")).onOutflow()
            ]
          },
          {
            field  : "PaymentDetail.SWIFTDetails",
            display: [
              hide(),
              show().onOutflow().onSection("A")
            ]
          },
          {
            field: "PaymentDetail.BeneficiaryBank.SWIFTBIC",
            display: [
              hide(),
              show().onOutflow().onSection("A"),
              disable(isTransactionFieldProvided("PaymentDetail.BeneficiaryBank.SWIFTBIC")),
              setValue("IVESZAJJXXX", null, isEmpty.and(hasTransactionFieldValue("IsInvestecFCA", "Y"))).onOutflow().onSection("A"),
              clearValue(hasValue("IVESZAJJXXX").and(hasTransactionFieldValue("IsInvestecFCA", "N"))).onOutflow().onSection("A")
            ]
          },
          {
            field: "PaymentDetail.BeneficiaryBank.BankName",
            display: [
              hide(),
              show().onOutflow().onSection("A"),
              disable(isTransactionFieldProvided("PaymentDetail.BeneficiaryBank.BankName")),
              setValue("Investec Bank Limited", null, isEmpty.and(hasTransactionFieldValue("IsInvestecFCA", "Y"))).onOutflow().onSection("A"),
              clearValue(hasValue("Investec Bank Limited").and(hasTransactionFieldValue("IsInvestecFCA", "N"))).onOutflow().onSection("A")
            ]
          },
          {
            field: "PaymentDetail.BeneficiaryBank.BranchCode",
            display: [
              hide(),
              disable(isTransactionFieldProvided("PaymentDetail.BeneficiaryBank.BranchCode")),
              show().onOutflow().onSection("A")
            ]
          },
          {
            field: "PaymentDetail.BeneficiaryBank.Address",
            display: [
              hide(),
              disable(isTransactionFieldProvided("PaymentDetail.BeneficiaryBank.Address")),
              show().onOutflow().onSection("A")
            ]
          },
          {
            field: "PaymentDetail.BeneficiaryBank.City",
            display: [
              hide(),
              disable(isTransactionFieldProvided("PaymentDetail.BeneficiaryBank.City")),
              show().onOutflow().onSection("A")
            ]
          },
          {
            field  : "ReceivingBank",
            display: [
              hide()
            ]
          },
          {
            field  : "ReceivingCountry",
            display: [
              hide(),
              show().onOutflow().onSection("A"),
              disable(isTransactionFieldProvided("ReceivingCountry")),
              setValue("ZA", null, isEmpty.and(hasTransactionFieldValue("IsInvestecFCA", "Y"))).onOutflow().onSection("A"),
              setValue("%s", countryFromSWIFTBIC("PaymentDetail.BeneficiaryBank.SWIFTBIC"), isEmpty.and(hasTransactionField("PaymentDetail.BeneficiaryBank.SWIFTBIC"))).onOutflow().onSection("A")
            ]
          },
          {
            field: "PaymentDetail.IsCorrespondentBank",
            display: [
              hide(),
              show(hasTransactionFieldValue("IsInvestecFCA", "N")).onOutflow().onSection("A"),
              setValue("N", null, isEmpty).onOutflow().onSection("A"),
              disable(hasTransactionFieldValue("IsInvestecFCA", "Y")).onInflow(),
              disable().onOutflow()
            ]
          },
          {
            field: "PaymentDetail.CorrespondentBank.SWIFTBIC",
            display: [
              hide(),
              show(hasTransactionFieldValue("PaymentDetail.IsCorrespondentBank", "Y")).onOutflow().onSection("A"),
              disable().onOutflow().onSection("A")
            ]
          },
          {
            field: "PaymentDetail.CorrespondentBank.BankName",
            display: [
              hide(),
              show(hasTransactionFieldValue("PaymentDetail.IsCorrespondentBank", "Y")).onOutflow().onSection("A"),
              disable().onOutflow().onSection("A")
            ]
          },
          {
            field: "PaymentDetail.CorrespondentBank.BranchCode",
            display: [
              hide(),
              show(hasTransactionFieldValue("PaymentDetail.IsCorrespondentBank", "Y")).onOutflow().onSection("A"),
              disable().onOutflow().onSection("A")
            ]
          },
          {
            field: "PaymentDetail.CorrespondentBank.Address",
            display: [
              hide(),
              show(hasTransactionFieldValue("PaymentDetail.IsCorrespondentBank", "Y")).onOutflow().onSection("A"),
              disable().onOutflow().onSection("A")
            ]
          },
          {
            field: "PaymentDetail.CorrespondentBank.City",
            display: [
              hide(),
              show(hasTransactionFieldValue("PaymentDetail.IsCorrespondentBank", "Y")).onOutflow().onSection("A"),
              disable().onOutflow().onSection("A")
            ]
          },
          {
            field  : "CorrespondentBank",
            display: [
              hide()
            ]
          },
          {
            field  : "CorrespondentCountry",
            display: [
              hide(),
              show(hasTransactionFieldValue("PaymentDetail.IsCorrespondentBank", "Y")).onOutflow().onSection("A"),
              disable().onOutflow().onSection("A")
            ]
          },
          {
            field: "Resident.Individual.Surname",
            display: [
              //disable(isTransactionFieldProvided("Resident.Individual.Surname"))
            ]
          },
          {
            field: "Resident.Individual.Name",
            display: [
              //disable(isTransactionFieldProvided("Resident.Individual.Name"))
            ]
          },
          {
            field: "Resident.Individual.MiddleNames",
            display: [
              //disable(isTransactionFieldProvided("Resident.Individual.MiddleNames"))
            ]
          },
          {
            field  : "Resident.Individual.DateOfBirth",
            display: [
              show().onSection("ABCEG"),
              setValue("%s", dateOfBirthFromSAID("Resident.Individual.IDNumber"), isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", ["South African Resident"])))
              //disable(isTransactionFieldProvided("Resident.Individual.DateOfBirth"))
            ]
          },
          {
            field  : "Resident.Individual.Gender",
            display: [
              show().onSection("ABCEG"),
              setValue("%s", genderFromSAID("Resident.Individual.IDNumber"), isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", ["South African Resident"])))
              //disable(isTransactionFieldProvided("Resident.Individual.Gender"))
            ]
          },
          {
            field  : "Resident.Individual.IDNumber",
            display: [
              show().onSection("ABEG"),
              hide(hasTransactionFieldValue("AccountHolderStatus", ["Non Resident", "Foreign Temporary Resident"])).onSection("ABCEG")
              //disable(isTransactionFieldProvided("Resident.Individual.IDNumber"))
            ]
          },
          {
            field  : "Resident.Individual.TempResPermitNumber",
            display: [
              show().onSection("ABEG"),
              hide().onSection("AB").onCategory(["511", "512", "513", "514", "515"]),
              hide().onOutflow().onSection("AB").onCategory("401"),
              hide(hasTransactionFieldValue("AccountHolderStatus", ["Non Resident", "South African Resident"])).onSection("ABCEG")
              //disable(isTransactionFieldProvided("Resident.Individual.TempResPermitNumber"))
            ]
          },
          {
            field  : "Resident.Individual.TempResExpiryDate",
            display: [
              show().onSection("ABEG"),
              hide().onSection("AB").onCategory(["511", "512", "513", "514", "515"]),
              hide().onOutflow().onSection("AB").onCategory("401"),
              hide(hasTransactionFieldValue("AccountHolderStatus", ["Non Resident", "South African Resident"])).onSection("ABCEG")
              //disable(isTransactionFieldProvided("Resident.Individual.TempResExpiryDate"))
            ]
          },
          {
            field  : "Resident.Individual.ForeignIDNumber",
            display: [
              hide()
            ]
          },
          {
            field  : "Resident.Individual.ForeignIDCountry",
            display: [
              hide()//,
             // setValue("%s", "Resident.Individual.PassportCountry")
            ]
          },
          {
            field  : "Resident.Individual.PassportNumber",
            display: [
              hide(),
              show(hasTransactionFieldValue("AccountHolderStatus", "South African Resident")).onSection("ABEG").onCategory("256"),
              show(hasTransactionField("AccountHolderStatus").and(notTransactionFieldValue("AccountHolderStatus", "South African Resident"))).onSection("ABEG")
              //disable(isTransactionFieldProvided("Resident.Individual.PassportNumber"))
            ]
          },
          {
            field  : "Resident.Individual.PassportExpiryDate",
            display: [
              hide(),
              show(hasTransactionFieldValue("AccountHolderStatus", "South African Resident")).onSection("ABEG").onCategory("256"),
              show(hasTransactionField("AccountHolderStatus").and(notTransactionFieldValue("AccountHolderStatus", "South African Resident"))).onSection("ABEG")
              //disable(isTransactionFieldProvided("Resident.Individual.PassportExpiryDate"))
            ]
          },
          {
            field  : "Resident.Individual.PassportCountry",
            display: [
              hide(),
              show(hasTransactionFieldValue("AccountHolderStatus", "South African Resident")).onSection("ABEG").onCategory("256"),
              show(hasTransactionField("AccountHolderStatus").and(notTransactionFieldValue("AccountHolderStatus", "South African Resident"))).onSection("ABEG")
              //disable(isTransactionFieldProvided("Resident.Individual.PassportCountry"))
            ]
          },
          {
            field  : "Resident.Entity.InstitutionalSector",
            display: [
              hide()
            ]
          },
          {
            field  : "Resident.Entity.IndustrialClassification",
            display: [
              hide()
            ]
          },
          {
            field  : "Resident.Exception",
            display: [
              hide()
            ]
          },
          {
            field  : ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],
            display: [
              hide(),
              show().onInflow().onSection("AB").onCategory(["101", "103", "105", "106"]),
              show().onOutflow().onSection("AB").notOnCategory(['101/11', '102/11', '103/11', '104/11']).onCategory(['101', '102', '103', '104', '105', '106'])
            ]
          },
          {
            field  : ["Resident.Individual.VATNumber", "Resident.Entity.VATNumber"],
            display: [
              hide(),
              show(hasTransactionField("Resident.Entity")).onSection("AB")
            ]
          },
          {
            field: "Resident.Individual.TaxClearanceCertificateIndicator",
            display: [
              hide(),
              setValue("Y", null, hasTransactionField("Resident.Individual").and(notValue("Y").and(hasAnyMoneyFieldValue("TaxClearanceCertificateIndicator", "Y")))),
              setValue("N", null, hasTransactionField("Resident.Individual").and(notValue("N").and(not(hasAnyMoneyFieldValue("TaxClearanceCertificateIndicator", "Y")))))
            ]
          },
          {
            field: "Resident.Entity.TaxClearanceCertificateIndicator",
            display: [
              hide(),
              setValue("Y", null, hasTransactionField("Resident.Entity").and(notValue("Y").and(hasAnyMoneyFieldValue("TaxClearanceCertificateIndicator", "Y")))),
              setValue("N", null, hasTransactionField("Resident.Entity").and(notValue("N").and(not(hasAnyMoneyFieldValue("TaxClearanceCertificateIndicator", "Y")))))
            ]
          },
          {
            field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
            display: [
              hide(),
              show(hasResidentFieldValue("TaxClearanceCertificateIndicator", "Y")).onSection("AB")
            ]
          },
          {
            field  : ["Resident.Individual.AccountName", "Resident.Entity.AccountName"],
            display: [
              hide()
            ]
          },
          {
            field  : ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
            display: [
              hide()
            ]
          },
          {
            field: "Resident.Individual.AccountNumber",
            display: [
              hide().onSection("F"),
              disable(isTransactionFieldProvided("Resident.Individual.AccountNumber"))
            ]
          },
          {
            field:  "Resident.Entity.AccountNumber",
            display: [
              hide().onSection("F"),
              disable(isTransactionFieldProvided("Resident.Entity.AccountNumber"))
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.Country", "Resident.Entity.StreetAddress.Country"],
            display: [
              show()
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.Mandate", "Resident.Entity.StreetAddress.Mandate"],
            display: [
              hide(),
              show(hasResidentField("StreetAddress.Country").and(notResidentFieldValue("StreetAddress.Country", "ZA")).
                      and(hasTransactionFieldValue("AccountHolderStatus", ["South African Resident", "Foreign Temporary Resident"])))
            ]
          },
          {
            field  : ["Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1"],
            display: [
              hide()
            ]
          },
          {
            field  : ["Resident.Individual.PostalAddress.AddressLine2", "Resident.Entity.PostalAddress.AddressLine2"],
            display: [
              hide()
            ]
          },
          {
            field  : ["Resident.Individual.PostalAddress.Suburb", "Resident.Entity.PostalAddress.Suburb"],
            display: [
              hide()
            ]
          },
          {
            field  : ["Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City"],
            display: [
              hide()
            ]
          },
          {
            field  : ["Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
            display: [
              hide()
            ]
          },
          {
            field  : ["Resident.Individual.PostalAddress.PostalCode", "Resident.Entity.PostalAddress.PostalCode"],
            display: [
              hide()
            ]
          },
          {
            field: "NonResident.Entity.EntityName",
            display: [
              show().onSection("ABCDG"),
              appendValue("%s", "Resident.Entity.EntityName", notMatchesTransactionField("Resident.Entity.EntityName").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y"))),
              disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Entity.EntityName"))),
              enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Entity.EntityName"))))
            ]
          },
          {
            field: "NonResident.Individual.Surname",
            display: [
              show().onSection("ABCDG"),
              appendValue("%s", "Resident.Individual.Surname", notMatchesTransactionField("Resident.Individual.Surname").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y"))),
              disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Individual.Surname"))),
              enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Individual.Surname"))))
            ]
          },
          {
            field: "NonResident.Individual.Name",
            display: [
              show().onSection("ABCDG"),
              appendValue("%s", "Resident.Individual.Name", notMatchesTransactionField("Resident.Individual.Name").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y"))),
              disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Individual.Name"))),
              enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Individual.Name"))))
            ]
          },
          {
            field: "NonResident.Individual.MiddleNames",
            display: [
              show().onSection("ABCDG"),
              appendValue("%s", "Resident.Individual.MiddleNames", notMatchesTransactionField("Resident.Individual.MiddleNames").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y"))),
              disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Individual.MiddleNames"))),
              enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Individual.MiddleNames"))))
            ]
          },
          {
            field: "NonResident.Individual.Gender",
            display: [
              show().onSection("ABCDG"),
              appendValue("%s", "Resident.Individual.Gender", notMatchesTransactionField("Resident.Individual.Gender").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y"))),
              disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Individual.Gender"))),
              enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Individual.Gender"))))
            ]
          },
          {
            field  : "NonResident.Individual.AccountNumber",
            display: [
              hide(),
              show(hasTransactionFieldValue("IsInvestecFCA", "N").or(notTransactionField("IsInvestecFCA"))).onOutflow(),
              disable(isTransactionFieldProvided("NonResident.Individual.AccountNumber"))
            ]
          },
          {
            field: "NonResident.Individual.PassportNumber",
            display: [
              hide()
            ]
          },
          {
            field: "NonResident.Individual.PassportCountry",
            display: [
              hide()
            ]
          },
          {
            field  : "NonResident.Individual.IsMutualParty",
            display: [
              hide(),
              show(hasTransactionField("Resident.Individual")).onInflow(),
              disable(isTransactionFieldProvided("NonResident.Individual.IsMutualParty"))
            ]
          },
          {
            field  : "NonResident.Entity.IsMutualParty",
            display: [
              hide(),
              show(hasTransactionField("Resident.Entity")).onInflow(),
              disable(isTransactionFieldProvided("NonResident.Entity.IsMutualParty"))
            ]
          },
          {
            field  : "NonResident.Entity.AccountNumber",
            display: [
              hide(),
              show(hasTransactionFieldValue("IsInvestecFCA", "N").or(notTransactionField("IsInvestecFCA"))).onOutflow(),
              disable(isTransactionFieldProvided("NonResident.Entity.AccountNumber"))
            ]
          },
          {
            field: "NonResident.Individual.Address.AddressLine1",
            display: [
              setValue("%s", "Resident.Individual.StreetAddress.AddressLine1", notMatchesTransactionField("Resident.Individual.StreetAddress.AddressLine1").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              show(),
              disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
              enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
            ]
          },
          {
            field: "NonResident.Individual.Address.AddressLine2",
            display: [
              setValue("%s", "Resident.Individual.StreetAddress.AddressLine2", notMatchesTransactionField("Resident.Individual.StreetAddress.AddressLine2").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              show(),
              disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
              enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
            ]
          },
          {
            field: "NonResident.Individual.Address.Suburb",
            display: [
              setValue("%s", "Resident.Individual.StreetAddress.Suburb", notMatchesTransactionField("Resident.Individual.StreetAddress.Suburb").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              show(),
              disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
              enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
            ]
          },
          {
            field: "NonResident.Individual.Address.City",
            display: [
              setValue("%s", "Resident.Individual.StreetAddress.City", notMatchesTransactionField("Resident.Individual.StreetAddress.City").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              show(),
              disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
              enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
            ]
          },
          {
            field: "NonResident.Individual.Address.State",
            display: [
              setValue("%s", "Resident.Individual.StreetAddress.Province", notMatchesTransactionField("Resident.Individual.StreetAddress.Province").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              show(),
              disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
              enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
            ]
          },
          {
            field: "NonResident.Individual.Address.PostalCode",
            display: [
              setValue("%s", "Resident.Individual.StreetAddress.PostalCode", notMatchesTransactionField("Resident.Individual.StreetAddress.PostalCode").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              show(),
              disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
              enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
            ]
          },
          {
            field: "NonResident.Individual.Address.Country",
            display: [
              setValue("%s", "Resident.Individual.StreetAddress.Country", notMatchesTransactionField("Resident.Individual.StreetAddress.Country").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              setValue("%s", "LocationCountry", isEmpty.and(notMatchesTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(hasResidentFieldValue("StreetAddress.Country", "ZA"))))),
              setValue("%s", "LocationCountry", isEmpty.and(hasTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N")))),
              disable(isTransactionFieldProvided("NonResident.Individual.Address.Country").or(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              enable(not(isTransactionFieldProvided("NonResident.Individual.Address.Country")).and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))))
            ]
          },
          {
            field: "NonResident.Entity.Address.AddressLine1",
            display: [
              setValue("%s", "Resident.Entity.StreetAddress.AddressLine1", notMatchesTransactionField("Resident.Entity.StreetAddress.AddressLine1").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              show(),
              disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
              enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
            ]
          },
          {
            field: "NonResident.Entity.Address.AddressLine2",
            display: [
              setValue("%s", "Resident.Entity.StreetAddress.AddressLine2", notMatchesTransactionField("Resident.Entity.StreetAddress.AddressLine2").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              show(),
              disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
              enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
            ]
          },
          {
            field: "NonResident.Entity.Address.Suburb",
            display: [
              setValue("%s", "Resident.Entity.StreetAddress.Suburb", notMatchesTransactionField("Resident.Entity.StreetAddress.Suburb").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              show(),
              disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
              enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
            ]
          },
          {
            field: "NonResident.Entity.Address.City",
            display: [
              setValue("%s", "Resident.Entity.StreetAddress.City", notMatchesTransactionField("Resident.Entity.StreetAddress.City").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              show(),
              disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
              enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
            ]
          },
          {
            field: "NonResident.Entity.Address.State",
            display: [
              setValue("%s", "Resident.Entity.StreetAddress.Province", notMatchesTransactionField("Resident.Entity.StreetAddress.Province").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              show(),
              disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
              enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
            ]
          },
          {
            field: "NonResident.Entity.Address.PostalCode",
            display: [
              setValue("%s", "Resident.Entity.StreetAddress.PostalCode", notMatchesTransactionField("Resident.Entity.StreetAddress.PostalCode").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              show(),
              disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
              enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
            ]
          },
          {
            field: "NonResident.Entity.Address.Country",
            display: [
              setValue("%s", "Resident.Entity.StreetAddress.Country", notMatchesTransactionField("Resident.Entity.StreetAddress.Country").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
              setValue("%s", "LocationCountry", isEmpty.and(notMatchesTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(hasResidentFieldValue("StreetAddress.Country", "ZA"))))),
              setValue("%s", "LocationCountry", isEmpty.and(hasTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N")))),
              disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
              enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
            ]
          }
        ]
      };

      detailMoney = {
        ruleset: "invCCM Money Display Rules",
        scope: "money",
        fields: [
          {
            field  : "MoneyTransferAgentIndicator",
            display: [
              hide()
            ]
          },
          {
            field: "ForeignValue",
            display: [
              hide().onSection("F"),
              hide(hasTransactionField("ZAREquivalent")).onOutflow(),
              clearValue(notEmpty.and(hasTransactionField("ZAREquivalent"))).onOutflow(),
              appendValue("%s", "transaction::TotalForeignValue", isEmpty.and(hasMoneyFieldValue("SequenceNumber", "1")).and(hasTransactionField("TotalForeignValue")))
            ]
          },
          {
            field  : "RandValue",
            display: [
              hide(),
              show(hasTransactionField("ZAREquivalent")).onOutflow(),
              clearValue(notEmpty.and(notTransactionField("ZAREquivalent"))).onOutflow(),
              appendValue("%s", "transaction::ZAREquivalent", isEmpty.and(hasMoneyFieldValue("SequenceNumber", "1")).and(hasTransactionField("ZAREquivalent")))
            ]
          },
          {
            field: "TaxClearanceCertificateIndicator",
            display: [
              hide(),
              show().onOutflow().onCategory(["512","513"])
            ]
          },
          {
            field  : "StrateRefNumber",
            display: [
              hide()
            ]
          },
          {
            field  : "{{Regulator}}Auth.RulingsSection",
            display: [
              hide()
            ]
          },
          {
            field  : "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
            display: [
              hide()
            ]
          },
          {
            field  : "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
            display: [
              hide()
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
            display: [
              hide().onSection("CDEF"),
              hide().onSection("AB").onCategory(["100","200","300","400","500","600","700","800"])
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
            display: [
              hide()
            ]
          },
          {
            field  : "{{Regulator}}Auth.SARBAuthDate",
            display: [
              hide().onSection("CDEF"),
              hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"])
            ]
          },
          {
            field  : "AdHocRequirement.Subject",
            display: [
              hide()
            ]
          },
          {
            field  : "AdHocRequirement.Description",
            display: [
              hide()
            ]
          },
          {
            field  : "ReversalTrnRefNumber",
            display: [
              hide()
            ]
          },
          {
            field  : "ReversalTrnSeqNumber",
            display: [
              hide()
            ]
          },
          {
            field  : "BOPDIRTrnReference",
            display: [
              hide()
            ]
          },
          {
            field  : "BOPDIRADCode",
            display: [
              hide()
            ]
          },
          {
            field: "SWIFTDetails",
            display: [
              hide(),
              disable()
            ]
          },
          {
            field: "LoanInterestRate",
            display: [
              hide()
            ]
          },
          {
            field: "LoanInterest.BaseRate",
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"])
            ]
          },
          {
            field: "LoanInterest.Term",
            display: [
              hide(),
              show(hasMoneyField("LoanInterest.BaseRate").and(notMoneyFieldValue("LoanInterest.BaseRate", ["FIXED", "PRIME"]))).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"])
            ]
          },
          {
            field: "LoanInterest.PlusMinus",
            display: [
              hide(),
              show(hasMoneyField("LoanInterest.BaseRate").and(notMoneyFieldValue("LoanInterest.BaseRate", ["FIXED"]))).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"])
            ]
          },
          {
            field: "LoanInterest.Rate",
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
              show().onOutflow().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
              show().onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"])
            ]
          },
          {
            field: "LoanTenor",
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"])
            ]
          },
          {
            field  : "Monetary.ThirdParty.Heading",
            display: [
              setValue("Remove All Fields", null),
              setValue("Traveller's Details", null).onSection("AB").onCategory(["255","256"]),
              setValue("Third Party Details", null).onOutflow().onSection("AB").onCategory(["511","512","513"])
            ]
          },
          {
            field  : "TravelMode.TravellerStatus",
            display: [
              hide(),
              show().onCategory(["255", "256"])
            ]
          },
          {
            field  : "ThirdParty.Individual.Surname",
            display: [
              hide(),
              show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"]),
              show().onOutflow().onSection("AB").onCategory(["512", "513"])
            ]
          },
          {
            field  : "ThirdParty.Individual.Name",
            display: [
              hide(),
              show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"]),
              show().onOutflow().onSection("AB").onCategory(["512", "513"])
            ]
          },
          {
            field  : "ThirdParty.Individual.MiddleNames",
            display: [
              hide(),
              show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"]),
              show().onOutflow().onSection("AB").onCategory(["512", "513"])
            ]
          },
          {
            field  : "ThirdParty.Individual.Gender",
            display: [
              hide(),
              show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"]),
              show().onOutflow().onSection("AB").onCategory(["512", "513"]),
              setValue("%s", genderFromSAID("ThirdParty.Individual.IDNumber"), isEmpty.and(hasMoneyField("ThirdParty.Individual.IDNumber")))
            ]
          },
          {
            field  : "ThirdParty.Individual.IDNumber",
            display: [
              hide(),
              show(hasMoneyField("TravelMode.TravellerStatus").and(hasMoneyFieldValue("TravelMode.TravellerStatus", ["South African Resident"]))).onCategory(["255", "256"]),
              show().onOutflow().onSection("AB").onCategory(["512", "513"])
            ]
          },
          {
            field  : "ThirdParty.Individual.DateOfBirth",
            display: [
              hide(),
              show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"]),
              show().onOutflow().onSection("AB").onCategory(["512", "513"]),
              setValue("%s", dateOfBirthFromSAID("ThirdParty.Individual.IDNumber"), isEmpty.and(hasMoneyField("ThirdParty.Individual.IDNumber")))
            ]
          },
          {
            field  : "ThirdParty.Individual.TempResPermitNumber",
            display: [
              hide(),
              show(hasMoneyField("TravelMode.TravellerStatus").and(hasMoneyFieldValue("TravelMode.TravellerStatus", ["Foreign Temporary Resident"]))).onCategory(["255", "256"])
            ]
          },
          {
            field  : "ThirdParty.Individual.TempResExpiryDate",
            display: [
              hide(),
              show(hasMoneyField("TravelMode.TravellerStatus").and(hasMoneyFieldValue("TravelMode.TravellerStatus", ["Foreign Temporary Resident"]))).onCategory(["255", "256"])
            ]
          },
          {
            field  : "ThirdParty.Individual.PassportNumber",
            display: [
              hide(),
              show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"])
            ]
          },
          {
            field  : "ThirdParty.Individual.PassportCountry",
            display: [
              hide(),
              show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"])
            ]
          },
          {
            field  : "ThirdParty.Individual.PassportExpiryDate",
            display: [
              hide(),
              show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"])
            ]
          },
          {
            field  : "ThirdParty.Entity.Name",
            display: [
              hide()
            ]
          },
          {
            field  : "ThirdParty.Entity.RegistrationNumber",
            display: [
              hide()
            ]
          },
          {
            field  : "ThirdParty.CustomsClientNumber",
            display: [
              hide()
            ]
          },
          {
            field  : "ThirdParty.TaxNumber",
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(["512", "513"])
            ]
          },
          {
            field  : "ThirdParty.VATNumber",
            display: [
              hide()
            ]
          },
          {
            field  : "ThirdParty.StreetAddress.AddressLine1",
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(["512", "513"])
            ]
          },
          {
            field  : "ThirdParty.StreetAddress.AddressLine2",
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(["512", "513"])
            ]
          },
          {
            field  : "ThirdParty.StreetAddress.Suburb",
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(["512", "513"])
            ]
          },
          {
            field  : "ThirdParty.StreetAddress.City",
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(["512", "513"])
            ]
          },
          {
            field  : "ThirdParty.StreetAddress.Province",
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(["512", "513"])
            ]
          },
          {
            field  : "ThirdParty.StreetAddress.PostalCode",
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(["512", "513"])
            ]
          },
          {
            field  : "ThirdParty.PostalAddress.AddressLine1",
            display: [
              hide()
            ]
          },
          {
            field  : "ThirdParty.PostalAddress.AddressLine2",
            display: [
              hide()
            ]
          },
          {
            field  : "ThirdParty.PostalAddress.Suburb",
            display: [
              hide()
            ]
          },
          {
            field  : "ThirdParty.PostalAddress.City",
            display: [
              hide()
            ]
          },
          {
            field  : "ThirdParty.PostalAddress.Province",
            display: [
              hide()
            ]
          },
          {
            field  : "ThirdParty.PostalAddress.PostalCode",
            display: [
              hide()
            ]
          },
          {
            field  : "ThirdParty.ContactDetails.ContactSurname",
            display: [
              hide()
            ]
          },
          {
            field  : "ThirdParty.ContactDetails.ContactName",
            display: [
              hide()
            ]
          },
          {
            field  : "ThirdParty.ContactDetails.Email",
            display: [
              hide()
            ]
          },
          {
            field  : "ThirdParty.ContactDetails.Fax",
            display: [
              hide()
            ]
          },
          {
            field  : "ThirdParty.ContactDetails.Telephone",
            display: [
              hide()
            ]
          },
          {
            field  : "TravelMode.Mode",
            display: [
              hide(),
              show().onOutflow().onCategory(["255", "256"]),
              setValue("%s", firstMoneyFieldValue("TravelMode.Mode"), isEmpty.and(hasAnyMoneyField("TravelMode.Mode"))).onOutflow().onCategory(["255", "256"])
            ]
          },
          {
            field  : "TravelMode.BorderPost",
            display: [
              hide(),
              show(hasMoneyFieldValue("TravelMode.Mode", "ROAD")).onOutflow().onCategory(["255", "256"]),
              setValue("%s", firstMoneyFieldValue("TravelMode.BorderPost"), isEmpty.and(hasMoneyField("TravelMode.Mode", "ROAD").and(hasAnyMoneyField("TravelMode.BorderPost")))).onOutflow().onCategory(["255", "256"])
            ]
          },
          {
            field  : "TravelMode.TicketNumber",
            display: [
              hide(),
              show(hasMoneyFieldValue("TravelMode.Mode", ["AIR", "SEA", "RAIL"])).onOutflow().onCategory(["255", "256"])
            ]
          },
          {
            field  : "TravelMode.DepartureDate",
            display: [
              hide(),
              show().onOutflow().onCategory(["255", "256"]),
              setValue("%s", firstMoneyFieldValue("TravelMode.DepartureDate"), isEmpty.and(hasAnyMoneyField("TravelMode.DepartureDate"))).onOutflow().onCategory(["255", "256"])
            ]
          },
          {
            field  : "TravelMode.DestinationCountry",
            display: [
              hide(),
              show().onOutflow().onCategory(["255", "256"]),
              setValue("%s", firstMoneyFieldValue("TravelMode.DestinationCountry"), isEmpty.and(hasAnyMoneyField("TravelMode.DestinationCountry"))).onOutflow().onCategory(["255", "256"])
            ]
          }
        ]
      };

      detailImportExport = {
        ruleset: "invCCM Import/Export Display Rules",
        scope: "importexport",
        fields: [
          {
            field: "ImportControlNumberLabel",
            display: [
              setValue("Invoice Number", null, isEmpty).onOutflow().onSection("ABG").onCategory("101"),
              setValue("Movement Reference Number (MRN)", null, isEmpty).onOutflow().onSection("ABG").onCategory(["103","105","106"])
            ]
          },

          {
            field : 'PaymentCurrencyCode',
            display : [
              setValue('%s', 'transaction::FlowCurrency', isEmpty.or(notMatchesTransactionField("FlowCurrency").and(notTransactionField("ZAREquivalent")))),
              setValue('ZAR', null, isEmpty.or(notValue("ZAR").and(hasTransactionField("ZAREquivalent"))))
            ]
          },
          {
            field: "MRNNotOnIVS",
            display: [
              hide()
            ]
          }
        ]
      };

    }

    return {
      detailTrans: detailTrans,
      detailMoney: detailMoney,
      detailImportExport: detailImportExport
    }
  }

});

