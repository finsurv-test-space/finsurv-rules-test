define(function() {
  return function(testBase) {
      with (testBase) {
        var test_cases = [
          assertSuccess("inv_bbbic1", {
                ReportingQualifier: 'BOPCUS',
                Flow: 'OUT',
                ReceivingCountry: 'DE',
                PaymentDetail: {BeneficiaryBank: {SWIFTBIC: 'CITIDEHG'}}
              }),
          assertSuccess("inv_bbbic1", {
                ReportingQualifier: 'BOPCUS',
                Flow: 'OUT',
                ReceivingCountry: 'ZA',
                PaymentDetail: {BeneficiaryBank: {SWIFTBIC: ''}}
              }),
          assertFailure("inv_bbbic1", {
                ReportingQualifier: 'BOPCUS',
                Flow: 'OUT',
                ReceivingCountry: 'DE',
                PaymentDetail: {BeneficiaryBank: {SWIFTBIC: ''}}
              })
        ]
      }
    return testBase;
  }
})
