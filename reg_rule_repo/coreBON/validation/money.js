define(function () {
  return function (predef) {
    var stdMoney;
    with (predef) {
      stdMoney = {
        ruleset: "Standard BON Money Rules",
        scope: "money",
        validations: [
          {
            field: "",
            rules: [
              // field: "SequenceNumber",
              message('mseq1','443', null),
              // field: "MoneyTransferAgentIndicator",
              message('mta1','206', 'Must contain one of the following values: AD or ADLA or CARD or MONEYGRAM or MUKURU or WESTERNUNION'),
              ignore('mta2'),
              ignore('mta3'),
              message('mta4','206', null),
              message('mta5','310', null),
              // NOTE: Added ignores for mta6 and mta7
              ignore('mta6'),
              ignore('mta7'),
              // field: "{{LocalValue}}",
              message('mrv1','308', null),
              message('mrv2','311', null),
              ignore('mrv3'),
              message('mrv4','313', 'If a ForeignValue is completed and a DomesticValue is reported, the reported DomesticValue must be within a 15% variance with the applicable mid-rate calculated by the BoN.'),
              message('mrv5','319', null),
              ignore('mrv6'),
              ignore('mrv7'),
              message('mrv8','304', null),
              ignore('mrv9'),
              ignore('mrv10'),
              // field: "ForeignValue",
              // TODO: Check consistency of all rules in this category
              message('mfv1','308', 'At least one of DomesticValue or ForeignValue must be present'),
              message('mfv2','315', null),
              message('mfv3','314', null),
              message('mfv4','320', null),
              message('mfv5','304', null),
              message('mfv6','355', null),
              // field: "CategoryCode",
              message('mcc1','323', null),
              message('mcc2','324', null),
              message('mcc3','325', null),
              // NOTE: Added ignore for own rule S10
              // NOTE: Removed ignore for rule. Changed to Error on coreSADC
              // ignore('mcc4'),
              ignore('mcc5'),
              ignore('mcc6'),
              ignore('mcc7'),
              ignore('mcc8'),
              ignore('mcc9'),
              // field: "CategorySubCode",
              ignore('msc1'),
              ignore('msc2'),
              // NOTE: Added ignore
              ignore('msc3'),
              // field: "SWIFTDetails",
              // NOTE: added section A
              ignore('mswd1'),
              // field: "StrateRefNumber",
              ignore('msrn1'),
              ignore('msrn2'),
              // NOTE: Added rule 328
              message('msrn3', '328'), //.onSection('AEF'), TODO: Check rule sections
              ignore('msrn4'),
              // field: "LoanRefNumber",
              ignore('mlrn1'),
              ignore('mlrn2'),
              ignore('mlrn3'),
              ignore('mlrn4'),
              ignore('mlrn5'),
              ignore('mlrn6'),
              ignore('mlrn7'),
              ignore('mlrn8'),
              ignore('mlrn9'),
              message('mlrn10','331', null),
              ignore('mlrn11'),
              // field: "LoanTenor",
              ignore('mlt1'),
              // NOTE: Added ignore for mlt 2 (Must be a future date)
              ignore('mlt2'),
              ignore('mlt3'),
              // field: "LoanInterestRate",
              ignore('mlir1'),
              ignore('mlir2'),
              ignore('mlir3'),
              ignore('mlir4'),
              ignore('mlir5'),
              ignore('mlir6'),
              ignore('mlir7'),
              ignore('mlir8'),
              // field: "{{Regulator}}Auth.RulingsSection",
              ignore('mars1'),
              ignore('mars2'),
              ignore('mars3'),
              // field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
              ignore('maian1'),
              ignore('maian2'),
              // field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
              ignore("maiad1"),
              ignore("maiad2"),
              ignore('maiad3'),
              // field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
              ignore('masan1'),
              ignore('masan2'),
              ignore('masan3'),
              ignore('masan4'),
              // field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
              ignore('masar1'),
              ignore('masar3'),
              // field: "CannotCategorize",
              message('mexc1','340', 'If BoPCategory 83000 is used, must be completed'),
              message('mexc2','343', null),
              // field: "AdHocRequirement.Subject",
              ignore('madhs1'),
              ignore('madhs2'),
              ignore('madhs3'),
              ignore('madhs4'),
              ignore('madhs5'),
              ignore('madhs6'),
              ignore('madhs7'),
              ignore('madhs8'),
              ignore('madhs9'),
              ignore('madhs10'),
              ignore('madhs11'),
              ignore('madhs12'),
              ignore('madhs13'),
              ignore('madhs14'),
              ignore('madhs15'),
              ignore('madhs16'),
              ignore('madhs17'),
              // field: "AdHocRequirement.Description",
              message('madhd1', '346', null),
              ignore('madhd2'),
              ignore('madhd3'),
              // field: "LocationCountry",
              ignore('mlc1'),
              message('mlc2','229', null),
              ignore('mlc3'),
              ignore('mlc4'),
              message('mlc5','350', null),
              ignore('mlc6'),
              // NOTE: Added ignore for mic7
              ignore('mlc7'),
              ignore('mlc8'),
              // field: "ReversalTrnRefNumber",
              message('mrtrn1','351', 'If BoPCategory is 10000 or 20000 or 30000 or 40000 or 50000 or 60000 or 70000 or 80000 is used, it must be completed'),
              message('mrtrn2','349', null),
              message('mrtrn3','263', null),
              ignore('mrtrn4'),
              ignore('mrtrn5'),
              // field: "ReversalTrnSeqNumber",
              message('mrtrs1','351', 'If BoPCategory is 10000 or 20000 or 30000 or 40000 or 50000 or 60000 or 70000 or 80000 is used, it must be completed'),
              message('mrtrs2','352', null),
              message('mrtrs3','263', null),
              // field: "BOPDIRTrnReference",
              ignore('mdirtr1'),
              // field: "BOPDIR{{DealerPrefix}}Code",
              ignore('mdircd1'),
              ignore('mdircd2'),
              // field: "ThirdParty.Individual",
              // NOTE: Added ignore for mtpi1
              ignore('mtpi1'),
              // field: "ThirdParty.Individual.Surname",
              ignore('mtpisn1'),
              message('mtpisn2','357', null),
              message('mtpisn3','357', 'If the category is 25600 and the PassportNumber under Individual ResidentCustomerAccountHolder contains no value,IndividualThirdPartySurnamemust be completed.'),
              message('mtpisn4','357', 'If the category is 25500 or 25600 and the EntityCustomer element is completed, IndividualThirdPartySurnamemust be completed.'),
              message('mtpisn5','358', null),
              message('mtpisn6','357', null),
              ignore('mtpisn7'),
              ignore('mtpisn8'),
              ignore('mtpisn9'),
              ignore('mtpisn10'),
              // field: "ThirdParty.Individual.Name",
              ignore('mtpinm1'),
              message('mtpinm2','360', 'If the category is 25600 and the PassportNumber under IndividualCustomer contains no value, IndividualThirdPartyName must be completed.'),
              message('mtpinm3','360', 'If the category is 25500 or 25600 and the EntityCustomer element is completed, IndividualThirdPartyName must be completed'),
              message('mtpinm4','358', null),
              message('mtpinm5','361', null),
              message('mtpinm6','360', null),
              ignore('mtpinm7'),
              // field: "ThirdParty.Individual.Gender",
              ignore('mtpig1'),
              message('mtpig2','363', null),
              message('mtpig3','362', 'If the BoPcategory is 25600 and the PassportNumber under IndividualCustomer contains no value, IndividualThirdPartyGender must be completed.'),
              message('mtpig4','362', 'If the BoPcategory is 25500 and the EntityCustomer element is completed, IndividualThirdPartyGender must be completed.'),
              message('mtpig5','364', null),
              message('mtpig6','362', null),
              ignore('mtpig7'),
              ignore('mtpig8'),
              // field: "ThirdParty.Individual.IDNumber",
              ignore('mtpiid1'),
              ignore('mtpiid2'),
              ignore('mtpiid3'),
              message('mtpiid4','366', null),
              message('mtpiid5','365', null),
              message('mtpiid6','446', null),
              ignore('mtpiid7'),
              ignore('mtpiid8'),        
              ignore('mtpiid9'),
              // field: "ThirdParty.Individual.DateOfBirth",
              ignore('mtpibd1'),
              message('mtpibd2','367', 'If the BoPcategory is 25600 and the PassportNumber under IndividualCustomer contains no value, IndividualThirdPartyDateOfBirth must be completed.'),
              message('mtpibd3','367', 'If the BoPcategory is 25500 and the EntityCustomer element is completed , IndividualThirdPartyDateOfBirth must be completed.'),
              message('mtpibd4','368', null),
              message('mtpibd5','367', null),
              // NOTE: Added an ignore for mtpibd6 - is this valid date check required? (Form Jaco)Yes, it is required.
              // ignore('mtpibd6'), - BON rejects on this so cannot ignore...
              ignore('mtpibd7'),
              ignore('mtpibd8'),
              // field: "ThirdParty.Individual.TempResPermitNumber",
              ignore('mtpitp1'),
              ignore('mtpitp2'),
              message('mtpitp3','365', null),
              message('mtpitp4','369', null),
              message('mtpitp5','447', null),
              ignore('mtpitp6'),
              message('mtpitp7','261', 'If IndividualThirdPartySurname contains a value and the BoPCategory is 25000 and the Flow is OUT, either IndividualThirdPartyTempResPermit Number or IndividualThirdPartyPassportNumber must be completed. (This rule caters for non-residents, but excluding contract workers, traveling o.b.o. of a Namibian entity)'),
              // field: "ThirdParty.Individual.PassportNumber",
              message('mtpipn1','370', 'If the BoPCategory is 25600 and the PassportNumber under IndividualCustomer contains no value, IndividualThirdPartyPassportNumber must be completed.'),
              message('mtpipn2','370', 'If the BoPcategory is 25500 and the EntityCustomer element is completed, IndividualThirdPartyPassportNumber must be completed.'),
              message('mtpipn3','371', null),
              message('mtpipn4','448', null),
              ignore('mtpipn5'),
              message('mtpipn6','261', 'If IndividualThirdPartySurname contains a value and the BoPCategory is 25000 and the Flow is OUT, either IndividualThirdPartyTempResPermitNumber or IndividualThirdPartyPassportNumber must be completed. (This rule caters for non-residents, but excludes contract workers, traveling o.b.o. of a Namibian entity)'),
              // field: "ThirdParty.Individual.PassportCountry",
              message('mtpipc1','372', null),
              message('mtpipc2','373', null),
              message('mtpipc3','229', null),
              ignore('mtpipc4'),
              // field: "ThirdParty.Entity.Name",
              ignore('mtpenm2'),
              ignore('mtpenm3'),
              // field: "ThirdParty.Entity.RegistrationNumber",
              ignore('mtpern1'),
              message('mtpern2','449', null),
              ignore('mtpern3'),
              // field: "ThirdParty.CustomsClientNumber",
              ignore('mtpccn1'),
              ignore('mtpccn2'),
              ignore('mtpccn3'),
              ignore('mtpccn4'),
              ignore('mtpccn5'),  
              // field: "ThirdParty.TaxNumber",
              ignore('mtptx1'),
              ignore('mtptx2'),
              message('mtptx3','450', null),
              ignore('mtptx4'),
              ignore('mtptx5'),
              // field: "ThirdParty.VATNumber",
              ignore('mtpvn1'),
              ignore('mtpvn2'),
              ignore('mtpvn3'),
              // field: "ThirdParty.StreetAddress.AddressLine1",
              ignore('mtpsal11'),
              ignore('mtpsal12'),
              ignore('mtpsal13'),
              ignore('mtpsal14'),
              // field: "ThirdParty.StreetAddress.AddressLine2",
              ignore('mtpsal21'),
              ignore('mtpsal22'),
              // field: "ThirdParty.StreetAddress.Suburb",
              ignore('mtpsas1'),
              ignore('mtpsas2'),
              ignore('mtpsas3'),
              // field: "ThirdParty.StreetAddress.City",
              ignore('mtpsac1'),
              ignore('mtpsac2'),
              ignore('mtpsac3'),
              // field: "ThirdParty.StreetAddress.Province",
              ignore('mtpsap1'),
              ignore('mtpsap2'),
              ignore('mtpsap3'),
              ignore('mtpsap4'),
              // field: "ThirdParty.StreetAddress.PostalCode",
              ignore('mtpsaz1'),
              ignore('mtpsaz2'),
              ignore('mtpsaz3'),
              ignore('mtpsaz4'),
              // field: "ThirdParty.PostalAddress.AddressLine1",
              ignore('mtppal11'),
              ignore('mtppal12'),
              ignore('mtppal13'),
              ignore('mtppal14'),
              // field: "ThirdParty.PostalAddress.AddressLine2",
              ignore('mtppal21'),
              ignore('mtppal22'),
              // field: "ThirdParty.PostalAddress.Suburb",
              ignore('mtppas1'),
              ignore('mtppas2'),
              ignore('mtppas3'),
              // field: "ThirdParty.PostalAddress.City",
              ignore('mtppac1'),
              ignore('mtppac2'),
              ignore('mtppac3'),
              // field: "ThirdParty.PostalAddress.Province",
              ignore('mtppap1'),
              ignore('mtppap2'),
              ignore('mtppap3'),
              ignore('mtppap4'),
              // field: "ThirdParty.PostalAddress.PostalCode",
              ignore('mtppaz1'),
              ignore('mtppaz2'),
              ignore('mtppaz3'),
              ignore('mtppaz4'),
              // field: "ThirdParty.ContactDetails.ContactSurname",
              ignore('mtpcds1'),
              ignore('mtpcds2'),
              ignore('mtpcds3'),
              // field: "ThirdParty.ContactDetails.ContactName",
              ignore('mtpcdn1'),
              ignore('mtpcdn2'),
              ignore('mtpcdn3'),
              // field: "ThirdParty.ContactDetails.Email",
              ignore('mtpcde1'),
              ignore('mtpcde2'),
              ignore('mtpcde3'),
              // field: "ThirdParty.ContactDetails.Fax",
              ignore('mtpcdf1'),
              ignore('mtpcdf2'),
              ignore('mtpcdf3'),
              // field: "ThirdParty.ContactDetails.Telephone",
              ignore('mtpcdt1'),
              ignore('mtpcdt2'),
              ignore('mtpcdt3'),
              // field: "CardChargeBack",
              message('mcrdcb1','391', null),
              message('mcrdcb2','392', 'Must contain a value "Y" or "N"'),
              ignore('mcrdcb3'),
              // field: "CardIndicator",
              message('mcrdci1','393', null),
              message('mcrdci2','394', null),
              // field: "ElectronicCommerceIndicator",
              ignore('mcrdec1'),
              ignore('mcrdec2'),
              ignore('mcrdec3'),
              // field: "POSEntryMode",
              ignore('mcrdem1'),
              ignore('mcrdem2'),
              ignore('mcrdem3'),
              ignore('mcrdem4'),
              // field: "CardFraudulentTransactionIndicator",
              ignore('mcrdft1'),
              ignore('mcrdft2'),       
              // field: "ForeignCardHoldersPurchases{{LocalValue}}",
              message('mcrdfp1','397', null),
              message('mcrdfp2','398', null),
              message('mcrdfp3','311', null),
              // field: "ForeignCardHoldersCashWithdrawals{{LocalValue}}",
              message('mcrdfw1','399', null),
              message('mcrdfw2','400', null),
              message('mcrdfw3','311', null),
              // field: "ImportExport",
              message('mtie1','401', 'If the Flow is IN and BoPCategory is 10101 to 10110 or 10301 to 10310 or 10500 or 10600, the ImportExportData Element must be completed.'),
              ignore('mtie2'),
              message('mtie3','434', 'Total PaymentValue of all the SubSequences may not exceed a 1% variance with the DomesticValue or ForeignValue (NOTE: PaymentValue of a currency must be consistent in respect of all the SubSequences i.e. may not be a USD value in SubSequence 1 and a NAD value in SubSequrence 2.)'),
              message('mtie4','401', 'If the Flow is OUT and BoPCategory is 10101 to 10110 or 10301 to 10310 or 10500 or 10600, ImportExportData Element must be completed.'),
              message('mtie5','402', 'For any BoPCategory other than 10101 to 10111 or 10301 to 10311 or 10500 or 10600, the ImportExportDataElement must not be completed.'),
              message('mtie6','404', null),
              message('mtie6','405', null)
            ]
          },

//================================================================================================================================================
// coreBON specific money
//================================================================================================================================================ 
          {
            // #OnSectionIssue mtpisn7 -> nm_mtpisn7
            field: "ThirdParty.Individual.Surname",
            minLen: 1,
            maxLen: 35,
            rules: [
            failure("nm_mtpisn7", 359, "Must not be completed",
              notEmpty).onSection("F"),
            ]
          },
          // #OnSectionIssue mtpinm1 -> nm_mtpinm1
          {
            field: "ThirdParty.Individual.Name",
            minLen: 1,
            maxLen: 35,
            rules: [
              failure("nm_mtpinm1", 360, "If IndividualThirdPartySurname contains a value, the IndividualThirdPartyName must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname"))).onSection("AE"),
            ]
          },
          // #OnSectionIssue mtpig1 -> nm_mtpig1
          {
            field: "ThirdParty.Individual.Gender",
            len: 1,
            rules: [
              failure("nm_mtpig1", 362, "If IndividualThirdPartySurname contains a value, the IndividualThirdPartyGender must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname"))).onSection("AE"),
            ]
          },
           // #OnSectionIssue mtpiid1 -> nm_mtpiid1
          {
            field: "ThirdParty.Individual.IDNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("nm_mtpiid1", 365, "If IndividualThirdPartySurname contains a value, either IndividualThirdPartyIDNumber or IndividualThirdPartyTempResPermitNumber must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname")).and(notMoneyField("ThirdParty.Individual.TempResPermitNumber"))).onSection("AE"),
              failure("nm_mtpiid3", "SCN", "This value must be a number",
                notEmpty.and(notPattern(/^\d*$/)))
            ]
          },
          // #OnSectionIssue mtpibd1 -> nm_mtpibd1
          {
            field: "ThirdParty.Individual.DateOfBirth",
            rules: [
              failure("nm_mtpibd1", 367, "If IndividualThirdPartySurname contains a value, the IndividualThirdPartyDateOfBirth must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname"))).onSection("AE"),
            ]
          },
          // #OnSecitonIssue mtpitp1 -> nm_mtpitp1
          {
            field: "ThirdParty.Individual.TempResPermitNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("nm_mtpitp1", 365, "If IndividualThirdPartySurname contains a value, either IndividualThirdPartyIDNumber or IndividualThirdPartyTempResPermit Number must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname")).and(notMoneyField("ThirdParty.Individual.IDNumber"))).onSection("AE"),
            ]
          },
          // #OnSectionIssue mtpenm2 -> nm_mtpenm2
          {
            field: "ThirdParty.Entity.Name",
            minLen: 1,
            maxLen: 50,
            rules: [
              failure("nm_mtpenm2", 374, "Must not be completed",
                notEmpty).onSection("EF"),
            ]
          },
          // #OnSectionIssue mtpern1 -> nm_mtpern1
          {
            field: "ThirdParty.Entity.RegistrationNumber",
            minLen: 2,
            maxLen: 30,
            rules: [
                failure("nm_mtpern1", 375, "Must not be completed",
                notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.StreetAddress.AddressLine1",
            minLen: 2,
            maxLen: 70,
            rules: [
            // #OnSectionIssue mtpsal12 -> nm_mtpsal12
            failure("nm_mtpsal12", 377, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.StreetAddress.AddressLine2",
            minLen: 2,
            maxLen: 70,
            rules: [
              // #OnSectionIssue mtpsal21 -> nm_mtpsal21
              failure("nm_mtpsal21", 378, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.StreetAddress.Suburb",
            minLen: 2,
            maxLen: 35,
            rules: [
              // #OnSectionIssue mtpsas2 -> nm_mtpsas2
            failure("nm_mtpsas2", 379, "Must not be completed",
               notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.StreetAddress.City",
            minLen: 2,
            maxLen: 35,
            rules: [
            // #OnSectionIssue mtpsac2 -> nm_mtpsac2
            failure("nm_mtpsac2", 380, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            // NOTE: This field is referred to as StreetRegion according to the BON spec
            field: "ThirdParty.StreetAddress.Province",
            minLen: 2,
            maxLen: 35,
            rules: [
            // Added RegionCheck
            failure("nm_mtpsap1", 381, "Must be a valid region",
              notEmpty.and(notValidProvince)).onSection("A"),

            // #OnSectionIssue mtpsap2 -> nm_mtpsap2
            failure("nm_mtpsap2", 381, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.StreetAddress.PostalCode",
            minLen: 2,
            maxLen: 10,
            rules: [
              // #OnSectionIssue mtpsaz2 -> nm_mtpsaz2
            failure("nm_mtpsaz2", 382, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.PostalAddress.AddressLine1",
            minLen: 2,
            maxLen: 70,
            rules: [
            // #OnSectionIssue mtppal12 -> nm_mtppal12
            failure("nm_mtppal12", 383, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.PostalAddress.AddressLine2",
            minLen: 2,
            maxLen: 70,
            rules: [
            // #OnSectionIssue mtppal21 -> nm_mtppal21
            failure("nm_mtppal21", 384, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.PostalAddress.Suburb",
            minLen: 2,
            maxLen: 35,
            rules: [
            // #OnSectionIssue mtppas2 -> nm_mtppas2
            failure("nm_mtppas2", 385, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.PostalAddress.City",
            minLen: 2,
            maxLen: 35,
            rules: [
            // #OnSectionIssue mtppac2 -> nm_mtppac2
            failure("nm_mtppac2", 386, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.PostalAddress.Province",
            minLen: 2,
            maxLen: 35,
            rules: [
            // Added RegionCheck
            failure("nm_mtppap1", 387, "Must be a valid region",
              notEmpty.and(notValidProvince)).onSection("A"),

            // #OnSectionIssue mtppap2 -> nm_mtppap2
            failure("nm_mtppap2", 387, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.PostalAddress.PostalCode",
            minLen: 2,
            maxLen: 10,
            rules: [
            //  #OnSectionIssue mtppaz2 -> nm_mtppaz2
            failure("nm_mtppaz2", 262, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.ContactDetails.ContactSurname",
            minLen: 2,
            maxLen: 35,
            rules: [
              // #OnSectionIssue mtpcds2 -> nm_mtpcds2
              failure("nm_mtpcds2", 388, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.ContactDetails.ContactName",
            minLen: 2,
            maxLen: 35,
            rules: [
              // #OnSectionIssue mtpcdn2 -> nm_mtpcdn2
              failure("nm_mtpcdn2", 389, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.ContactDetails.Email",
            minLen: 2,
            maxLen: 120,
            rules: [
              // #OnSectionIssue mtpcde2 -> nm_mtpcde2
            failure("nm_mtpcde2", 390, "May not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.ContactDetails.Fax",
            minLen: 2,
            maxLen: 15,
            rules: [
            // #OnSectionIssue mtpcdf2 -> nm_mtpcdf2
            failure("nm_mtpcdf2", 390, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.ContactDetails.Telephone",
            minLen: 2,
            maxLen: 15,
            rules: [
              // #OnSectionIssue mtpcdt2 -> nm_mtpcdt2
              failure("nm_mtpcdt2", 390, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
              field: "SequenceNumber",
              rules: [
                failure("nm_mseq1", 309, "Must contain a sequential number that must start with the value 1.",
                 notValidSequenceNumbers)
                ]
            },
            {
              field: "{{LocalValue}}",
              rules: [
                // NOTE: These rules are Standard Bank specific. Any future ammendments hereto should be billed. LIBRA-1966
                failure("nm_nvl1", 322, "If DomesticCurrencyCode is NAD and the ForeignCurrencyCode is ZAR, the DomesticValue must be equal to ForeignValue.",
                  notEmpty.and(not(equalsMoneyField("ForeignValue")).and(isCurrencyIn(["ZAR"])))).onSection("AEF"),
                // failure("nm_nvl2", "LV1", "If DomesticCurrencyCode is NAD and the ForeignCurrencyCode is the same, the DomesticValue must be equal to ForeignValue.",
                //   notEmpty.and(not(equalsMoneyField("ForeignValue")).and(isCurrencyIn(["NAD"])))).onSection("AEF"),
                failure("nm_nvl3", 422, "If BoPCategory 10700 is used, the value must not exceed {{LocalCurrency}}500.00",
                  notEmpty.and(isGreaterThan(500))).onSection("A").onCategory("107"),
                failure("nm_nvl4", 312, "Must not equal ForeignValue",
                  notEmpty.and(equalsMoneyField("ForeignValue").and(notCurrencyIn(["ZAR","NAD"])))).onSection("AEF"),
                ]
            },
            // These validations are not required as it is already validated on our own flow currency field
              // {
              //   field: "ForeignCurrencyCode",
              //   rules: [
              //     // Temporarily set to warning, all must be 'Failures' (according to spec)
              //     warning('b_fcc1',316,"If ForeignValue is completed, must be completed",
              //       isEmpty.and(hasMoneyField('ForeignValue'))).onSection('AE'),
              //     warning('b_fcc2',318,"Invalid SWIFT currency code.",
              //       notEmpty.and(hasInvalidSWIFTCurrency)).onSection("AE"),
              //     warning("b_fcc3", 317, "SWIFT currency code must not be NAD.",
              //       notEmpty.and(hasValue("NAD"))).onSection("AE"),
              //     warning("b_fcc4", 322, "If ForeignCurrencyCode is ZAR, NADValue must be equal to ForeignValue.",
              //       notEmpty.and(hasValue("ZAR")).and(not(evalMoneyField(map('{{LocalValue}}'), equalsMoneyField('ForeignValue'))))).onSection("E"),
              //     warning("b_fcc5", 322, "Must not be completed.",
              //         notEmpty).onSection("F")
              //   ]
              // },
            {
              field: "DomesticCurrencyCode",
              rules: [
                // tXstream auto updates the value when the transaction is submitted to the regulator, the form also populates it based on the regulator
                // TODO: Look at proper fix (example: Update the DomesticCurrencyCode when tran is inserted into tXstream)
                // failure("nm_ncc1", 337, "Must be completed.",
                // isEmpty).onSection("E"),
                failure("nm_ncc2", 318, "Invalid SWIFT currency code.",
                  notEmpty.and(hasInvalidSWIFTCurrency)).onSection("AE"),
                failure("nm_ncc3", 345, "SWIFT currency code must only be NAD.",
                  notEmpty.and(notValue("NAD"))).onSection("AE"),
                failure("nm_ncc4", 322, "Must not be completed.",
                  notEmpty).onSection("F")
                ]
            },
            {

              field: "LoanRefNumber",
              rules: [
                failure("nm_mlrn1", 329, "If the BoPCategory 80100, or 80200, or 80300, or 80400, or 81000 or 81500 or 81600 or 81700 or 81800 or 81900 is used, must be completed.",
                  isEmpty).onCategory(['801', '802', '803', '804', '810','815','816','817','818','819']).onSection("A"),
                failure("nm_mlrn2", 329, "If the BoPCategory 10600 or 30901 or 30902 or 30903 or 30904 or 30905 or 30906 or 30907, it must be completed.",
                  isEmpty).onCategory(['106','309']).notOnCategory(['309/08']).onSection("A"),
                failure("nm_mlrn3", 331, "For any other BoPCategory other than 80100, or 80200, or 80300 or 80400 or 81000 or 81500 or 81600 or 81700 or 81800 or 81900 or 10600 or 30901 or 30902 or 30903 or 30904 or 30905 or 30906 or 30907, it must not be completed",
                  notEmpty).notOnCategory(['801','802','803','804','810','815','816','817','818','819','106','309/01','309/02','309/03','309/04','309/05','309/06','309/07']).onSection("A"),
                failure("nm_mlrn12", "LR1", "Invalid loan reference number",
                  notEmpty.and(not(hasPattern(/^[0-9]*$/)))).onSection("A")
                ]
            },
            {
              field: "LoanTenor",
              minLen: 9,
              maxLen: 10,
              rules: [
                  // #OnSectionIssue mlt3 -> nm_mlt3
                  failure("nm_mlt3", 332, "Must not be completed",
                  notEmpty).onSection("EF")
              ]
            },
            {
              field: "LoanInterestRate",
              rules: [
                failure("nm_mlir1", 333, "If the BoPCategory 30901 to 30907 is used, must be completed reflecting the percentage interest paid.",
                  isEmpty).onCategory(['309']).notOnCategory(['309/08']).onSection("A"),
                failure("nm_mlir2", 335, "If the BoPCategory 30901 to 30907 must be completed in the format 0.00",
                  notEmpty.and(notPattern("^\\d{1,3}\\.\\d{2}?$"))).onCategory(['309']).notOnCategory(['309/08']).onSection("A"),
                // Added an onSection A
                failure("nm_mlir3", 334, "Except for BoPCategory 30901, 30902, 30903, 30904, 30905, 30906, 30907 it must not be completed",
                  isEmpty).onCategory(['309']).notOnCategory(['309/08']).onSection("A"),
                  // #OnSectionIssue mlir2 -> nm_mlir4
                  failure("nm_mlir4", 334, "Must not be completed",
                  notEmpty).onSection("EF"),
                ]
            },
            {
              field: "LocationCountry",
              rules: [
                // #OnSectionIssue mlc1 -> nm_mlc1
                failure("nm_mlc1", 348, "Must be completed",
                  isEmpty).onSection("AF"),
                failure("nm_mlc3", 267, "SWIFT country code must not be NA.",
                  notEmpty.and(hasValue("NA"))).onSection("AF"),
                failure("nm_mlc4", 229, "The LocationCountry EU may only be used if the CategoryCode is 513 or 517",
                  notEmpty.and(hasValue("EU"))).onSection("A").notOnCategory(["513", "517"]),
                ]
          },
          {
            field: "{{Regulator}}Auth.RulingsSection",
            minLen: 2,
            maxLen: 30,
            rules: [
              failure("nm_mars1", 336, "Must not be completed",
                notEmpty).onSection("AEF"),
            ]
          },
          {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              failure("nm_maian2", 336, "Must not be completed",
                notEmpty).onSection("AEF")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
            rules: [
              failure("nm_maiad2", 336, "Must not be completed",
                notEmpty).onSection("AEF"),
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              //TODO: Must be completed if the RegistrationNumber is registered as an IHQ entity
              failure("nm_masan3", 336, "Must not be completed",
                notEmpty).onSection("AEF"),
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              //failure("masar2", 389, "Must be in the format CCYY-nnnnnnnn where CC is the Century, YY is the year and nnnnnnnn is the e-docs number or any other {{Regulator}} authorisation number allocated by the {{Regulator}} in the reply to the application",
              //        notEmpty.and(notPattern("^(19|20)\\d{2}-\\d+$"))).onSection("ABG"),
              failure("nm_masar3", 336, "Must not be completed",
                notEmpty).onSection("AEF")
            ]
          },
          {
            field: "AdHocRequirement.Subject",
            minLen: 2,
            maxLen: 30,
            rules: [
              // #OnSectionIssue madhs2 -> nm_madhs2
              failure("nm_madhs2", 344, "Must not be completed",
                notEmpty).onSection("EF")
               // TODO: Add a rule for 393

          ]
          },
          {
            field: "AdHocRequirement.Description",
            minLen: 2,
            maxLen: 100,
            rules: [
              // #OnSectionIssue madhd3 -> nm_madhd3
                failure("nm_madhd3", 347, "Must not be completed",
                notEmpty).onSection("EF")
          ]
          },
          {
            // TODO: Compare ccn1 and ccn2 check. Should CCN2 through an error when ccn is empty?
            field: "ThirdParty.CustomsClientNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              failure("nm_mtpccn1", 292, "Invalid CustomsClientNumber",
                notEmpty.and(notPattern(/^\d{2,15}$/))).onSection("A"),
              failure("nm_mtpccn2", 292, "If it contains a value, the CustomsClientNumber must contain a valid Customs Client number",
                notEmpty.and(notPattern(/^\d{2,15}$/))).onSection("A"),
              // This rule is only for SA, Namibia does not need 70707070 functionality
              // failure("nm_mtpccn2b", 292, "The value 70707070 implies an unknown customs client number",
              //   notEmpty.and(hasValue("70707070"))).onSection("A"),
              // #OnSectionIssue mtpccn3 -> nm_mtpccn3
              failure("nm_mtpccn3", 291, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
          field: "ThirdParty.TaxNumber",
          minLen: 2,
          maxLen: 15,
          rules: [
            // #OnSectionIssue mtptx2 -> nm_mtptx2
            failure("nm_mtptx2", 376, "Must not be completed",
            notEmpty).onSection("EF"),
            ]
          },
          {
            field: "ThirdParty.VATNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
            // #OnSectionIssue mtpvn1 -> nm_mtpvn1
            failure("nm_mtpvn1", 452, "Must not be completed",
              notEmpty).onSection("EF"),
            ]
          },
          {
            field: "SWIFTDetails",
            minLen: 2,
            maxLen: 100,
            rules: [
              failure("nm_mswd1", 269, "Must not be completed",
                notEmpty).onSection("AEF")
            ]
          },
          {
            field: "ElectronicCommerceIndicator",
            minLen: 1,
            maxLen: 2,
            rules: [
              failure("nm_mcrdec2", 395, "Must not be completed",
                notEmpty).onSection("AEF"),
            ]
          },
          {
            field: "POSEntryMode",
            len: 2,
            rules: [
              failure("nm_mcrdem1", 395, "Must not be completed",
                notEmpty).onSection("AEF")
            ]
          },
          {
            field: "CardFraudulentTransactionIndicator",
            rules: [
              failure("nm_mcrdft1", 396, "Must not be completed",
                notEmpty).onSection("AEF"),
            ]
          },
          {
            field: "ImportExport",
            rules: [
            // #OnSectionIssue mtie2 -> nm_mtie2
            failure("nm_mtie2", 402, "ImportExportData Element must not be completed",
              notEmptyImportExport).onSection("EF"),
            ]
          },
          {
            field: "ReversalTrnSeqNumber",
            rules: [
              failure("ReversalTrnSeqNumber.maxLen", 'L03', 'The field ReversalTrnSeqNumber is too long', 
                isTooLong(3)).onSection('AEF'),
            ]
          },
          {
            field: "ReversalTrnRefNumber",
            minLen: 1,
            maxLen: 30,
            rules: [
              failure("nm_mrtrn4", 217, "Additional spaces identified in data content",
                notEmpty.and(hasSpaces)).onSection("A")
            ]
          },         
          {
            // #OnSectionIssue mdirtr1 -> nm_mdirtr1
            field: "BOPDIRTrnReference",
            minLen: 1,
            maxLen: 30,
            rules: [
              failure("nm_mdirtr1", 354, "Must not be completed",
                notEmpty).onSection("AEF")
            ]
          },  
          {
            field: "BOPDIR{{DealerPrefix}}Code",
            len: 3,
            rules: [
              failure("nm_mdircd2", 356, "Must not be completed",
                notEmpty).onSection("AEF"),
              ]
          },     
          {
            field: "MoneyTransferAgentIndicator",
            rules: [
              failure("nm_mtai1", 253, "If the MoneyTransferAgentIndicator is TRAVEL CARD or TRAVELLERS CHEQUE, the category can only be 252, 255, 256 or 530/05",
                notEmpty.and(hasValueIn(["TRAVEL CARD", "TRAVELLERS CHEQUE"]))).onSection("A").notOnCategory(["252", "255", "256", "530/05"]),
              ]
          },
      ]        
    };
  }
  return stdMoney;
}
});