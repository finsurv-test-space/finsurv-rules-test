define(function () {
  return function (predef) {
    var stdImportExport;
    with(predef) {
      stdImportExport = {
        ruleset: "BON Import/Export Rules",
        scope: "importexport",
        validations: [
          {
            field: "",
            rules: [
              // field: "ImportControlNumber",
              // ==============================================================================
              // TEMPORARY LIBRA-1971
              // ==============================================================================
              // ignore('ieicn1'),
              // message('ieicn2','407', 'For any BoPCategory other than 10101 to 10111 or 10301 to 10311 or 10500 or 10600, the ImportControlNumber must not be completed.'),
              // message('ieicn3','408', 'If the Flow is OUT and the BoPCategory is 10101 to 10110, the first 3 characters must be INV followed by the invoice number. The minimum total number of characters must be 4.'),
              // ignore('ieicn4'),
              // //message('ieicn4','409', 'If the Flow is OUT and the BoPCategory is 10301 to 10310 or 10500 or 10600, the first 3 characters must be a valid customs office code.'),
              // //message('ieicn4','410', 'If the Flow is OUT and the BoPCategory is 10301 to 10310 or 10500 or 10600,, the format is as follows: AAACCYYMMDD0000000 where AAA is a valid customs office code in alpha format; CC is the century of import, YY is the year of import, MM is the month of import, DD is the day of import, and 0000000 is the 7 digit unique bill of entry number allocated by Inland Revenue as part of the MRN.'),
              // ignore('ieicn5'),
              // ignore('ieicn6'),

              ignore('ieicn1'),
              ignore('ieicn2'),
              ignore('ieicn3'),
              ignore('ieicn4'),
              ignore('ieicn5'),
              ignore('ieicn6'),
              // ==============================================================================
              // TEMPORARY LIBRA-1971
              // ==============================================================================

              // field: "TransportDocumentNumber",
              // ==============================================================================
              // TEMPORARY LIBRA-1971
              // ==============================================================================
              // ignore('ietdn1'),
              // message('ietdn2','413', 'For any BoPCategory other than 10301 to 10311or 10500 or 10600, the TransportDocumentNumber must not be completed.'),
              // ignore('ietdn3'),
              // ignore('ietdn4'),

              ignore('ietdn1'),
              ignore('ietdn2'),
              ignore('ietdn3'),
              ignore('ietdn4'),
              // ==============================================================================
              // TEMPORARY LIBRA-1971
              // ==============================================================================

              // field: "UCR",
              // ==============================================================================
              // TEMPORARY LIBRA-1971
              // ==============================================================================
              // ignore('ieucr1'),
              // message('ieucr2','415', 'Must be completed if the Flow is IN and the BoPCategory is 10101 to 10110 or 10301 to 10310 or 10500 or 10600'),
              // message('ieucr3','416', 'For any BoPCategory other than 10101 to 10110 or 10301 to 10310 or 10500 or 10600, the UCR must not be completed.'),
              // // message('ieucr4','416', null), // Might be valid, but not referenced in Docs (UCR not on Outflow)
              // ignore('ieucr4'),

              ignore('ieucr1'),
              ignore('ieucr2'),
              ignore('ieucr3'),
              ignore('ieucr4'),
              // ==============================================================================
              // TEMPORARY LIBRA-1971
              // ==============================================================================

              // TODO: There is no error 421?
              // field: "PaymentCurrencyCode",
              message('iepcc1','435', null),
              // NOTE: Is this rule needed? There is already a 318 implemented -> fcurr2
              message('iepcc2','318', null),
              message('iepcc2','436', 'PaymentCurrencyCode of all SubSequence is not NAD or does not match ForeignCurrencyCode under MonetaryDetails element. (NOTE: CurrencyCode must be consistent in respect of all the SubSequences i.e. may not be a USD in SubSequence 1 and a NAD in SubSequrence 2.)'),          
              // field: "PaymentValue",
              message('iepv1','417', null),
              // field: "MRNNotOnIVS",
              ignore("iemrn1")
            ]
          },

//================================================================================================================================================
// coreBON specific import/export
//================================================================================================================================================
             
          // ==============================================================================
          // TEMPORARY LIBRA-1971
          // ==============================================================================
          // {
          //   field: "ImportControlNumber",
          //   rules: [
          //     failure("nm_ieicn1", 406, "Must be completed if the Flow is OUT and the BoPCategory is 10101 to 10110 or 10301 to 10310 or 10500 or 10600.",
          //       isEmpty).onOutflow().onCategory(['101', '103', '105', '106']).notOnCategory(['101/11', '103/11']).onSection("A"),
          //     // failure('nm_ieicn4',410, 'If the Flow is OUT and the BoPCategory is 10301 to 10310 or 10500 or 10600,, the format is as follows: AAACCYYMMDD0000000 where AAA is a valid customs office code in alpha format; CC is the century of import, YY is the year of import, MM is the month of import, DD is the day of import, and 0000000 is the 7 digit unique bill of entry number allocated by Inland Revenue as part of the MRN.',
          //     //   notEmpty.and(notPattern(/^(\w){3}(19|20|21)\d{2}(0\d|10|11|12)(0[1-9]|1\d|2\d|3[01])(\d){7}$/))).onOutflow().onSection("A").notOnCategory("103/11").onCategory(["103", "105", "106"]),
          //     // failure('nm_ieicn4', 'ICN1', 'If the Flow is OUT and the BoPCategory is 10301 to 10310 or 10500 or 10600, the importControlNumber must have a length between 2 and 35 characters.',
          //     //   notEmpty.and(isTooShort(2).or(isTooLong(35)))).onOutflow().onSection("A").notOnCategory("103/11").onCategory(["103", "105", "106"]),
              
          //     // failure('ImportControlNumber.maxLen','LO3','The field ImportControlNumber is too long',
          //     //   notEmpty.and(isTooLong(35))),
          //     failure("nm_ieicn5", 217, "Additional spaces identified in data content (May also not contain a comma (,))",
          //       notEmpty.and(hasSpaces.or(hasPattern(/\,/)))).onSection("A")

          // Note: There is no rule for error 411
          //   ]
          // },
          {
            field: "ImportControlNumber",
            rules: [
              warning("tmp_ieicn1", 406, "Must be completed if the Flow is OUT and the BoPCategory is 10101 to 10110 or 10301 to 10310 or 10500 or 10600.",
                isEmpty).onOutflow().onCategory(['101', '103', '105', '106']).notOnCategory(['101/11', '103/11']).onSection("A"),
              warning("tmp_ieicn2", 407, 'For any BoPCategory other than 10101 to 10111 or 10301 to 10311 or 10500 or 10600, the ImportControlNumber must not be completed.',
                notEmpty).onSection("ABG").notOnCategory(["101", "103", "105", "106"]),
              // Note: Display rule also overriden in detailDisplay
              warning("tmp_ieicn3", 408, "If the Flow is OUT and the BoPCategory is 10101 to 10110, the first 3 characters must be INV.",
                notPattern(/^INV.+$/)).onOutflow().onSection("ABG").onCategory("101").notOnCategory("101/11"),
              warning("tmp_ieicn5", 217, "Additional spaces identified in data content (May also not contain a comma (,))",
                notEmpty.and(hasSpaces.or(hasPattern(/\,/)))).onSection("A")
            ]
          },
          // ==============================================================================
          // TEMPORARY LIBRA-1971
          // ==============================================================================

          // ==============================================================================
          // TEMPORARY LIBRA-1971
          // ==============================================================================
          // {
          //   field: "TransportDocumentNumber",
          //   rules: [
          //     failure("nm_ietdn1", 412, "Must be completed if the Flow is OUT and the BoPCategory is 10301 to 10310.",
          //       isEmpty).onOutflow().onCategory(['103']).notOnCategory(['103/11']).onSection("A"),
          //   ]
          // },
          {
            field: "TransportDocumentNumber",
            rules: [
              warning("tmp_ietdn1", 412, "Must be completed if the Flow is OUT and the BoPCategory is 10301 to 10310.",
                isEmpty).onOutflow().onCategory(['103']).notOnCategory(['103/11']).onSection("A"),
              warning("tmp_ietdn2", 413, "For any BoPCategory other than 10301 to 10311or 10500 or 10600, the TransportDocumentNumber must not be completed.",
                notEmpty).onSection("ABG").notOnCategory(["103", "105", "106"]),
            ]
          },
          // ==============================================================================
          // TEMPORARY LIBRA-1971
          // ==============================================================================

          // ==============================================================================
          // TEMPORARY LIBRA-1971
          // ==============================================================================
          // {
          //   field: "UCR",
          //   minLen: 2,
          //   maxLen: 35,
          //   rules: [
          //     failure("nm_ieucr1", 414, "If UCR contains a value and the Flow is IN, the minimum characters is 2 but not exceeding 35 characters and is in the following format: nNA12345678a...35a where n = last digit of the year NA = Fixed character 12345678 = Valid Customs Client Numbera = unique alpha numeric consignment number.",
          //       notEmpty.and(isTooShort(12).or(isTooLong(35).or(notPattern(/^\dNA\d{8,13}[a-zA-Z0-9]{1,24}$/))))).onInflow().onSection("A"),
          //   ]
          // },

          {
            field: "UCR",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("tmp_ieucr1", 414, "If UCR contains a value and the Flow is IN, the minimum characters is 2 but not exceeding 35 characters and is in the following format: nNA12345678a...35a where n = last digit of the year NA = Fixed character 12345678 = Valid Customs Client Number and a = unique alpha numeric consignment number.",
                notEmpty.and(isTooShort(12).or(isTooLong(35).or(notPattern(/^\dNA\d{8,13}[a-zA-Z0-9]{1,24}$/))))).onInflow().onSection("A"),
              warning("tmp_ieucr2", 415, "Must be completed if the Flow is IN and the BoPCategory is 10101 to 10110 or 10301 to 10310 or 10500 or 10600",
                isEmpty).onSection("ABG").onInflow().notOnCategory(['101/11', '103/11']).onCategory(['101', '103', '105', '106']),
              warning("tmp_ieucr3", 416, "For any BoPCategory other than 10101 to 10110 or 10301 to 10310 or 10500 or 10600, the UCR must not be completed.",
                notEmpty).onSection("ABG").onInflow().notOnCategory(["101/01", "101/02", "101/03", "101/04", "101/05", "101/06", "101/07", "101/08", "101/09", "101/10", "103/01", "103/02", "103/03", "103/04", "103/05", "103/06", "103/07", "103/08", "103/09", "103/10", "105", "106"]),
            ]
          },
          // ==============================================================================
          // TEMPORARY LIBRA-1971
          // ==============================================================================

          // ==============================================================================
          // TEMPORARY LIBRA-1971
          // ==============================================================================
          // {
          //   field: "MRNNotOnIVS",
          //   rules: [
          //      Note: No validation rule for error code 219 and 226

          //     failure("nm_iemrn1", 203, "The value must only be Y or N",
          //       notEmpty.and(notValueIn(["Y", "N"]))).onSection("A")
          //   ]
          //  }

          {
            field: "MRNNotOnIVS",
            rules: [
              warning("tmp_iemrn1", 203, "The value must only be Y or N",
                notEmpty.and(notValueIn(["Y", "N"]))).onSection("A")
            ]
           }
          // ==============================================================================
          // TEMPORARY LIBRA-1971
          // ==============================================================================
        ]
      };
    }
    return stdImportExport;
  }
});