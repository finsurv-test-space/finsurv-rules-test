define(function() {
  return function(testBase) {
      with (testBase) {

        var test_cases = [

//================================================================================================================================================
//  # LENGTH CHECKS (1/6)
//================================================================================================================================================

      // field: "ReportingQualifier",
          assertSuccess("ReportingQualifier.minLen", {ReportingQualifier: 'BOPCUS'}),
          assertSuccess("ReportingQualifier.maxLen", {ReportingQualifier: 'BOPCUS'}),
          assertFailure("ReportingQualifier.minLen", {ReportingQualifier: 'BOPCU'}),
          assertFailure("ReportingQualifier.maxLen", {ReportingQualifier: '12345678901234567890123456'}),
           
      // field: "ImportControlNumber",
           assertSuccess("ImportControlNumber.maxLen", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: 'PEZ2010302281234567'}]}]
          }),
          assertSuccess("ImportControlNumber.maxLen", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: 'WOR201302281234567'}]}]
          }),
          assertFailure("ImportControlNumber.maxLen", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: '111111111111111111111111111111111111'}]}] //Too long
          }),
          assertNoRule("ImportControlNumber.maxLen", {
            ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: 'XXX301302301234567'}]}]
          }),

//================================================================================================================================================
//  # TRANSACTION (2/6)
//================================================================================================================================================
      
      // field: "NonResident",
          // Non Resdent Entity Element must be completed
          assertSuccess("nm_nr1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: "Synthesis"}}),
          assertSuccess("nm_nr1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: {}}}),
          assertFailure("nm_nr1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {}}),

      // field: "Resident.Entity.EntityName",
          // If Resident EntityCustomer is used, must be completed
          assertSuccess("nm_relen1", {ReportingQualifier: "BOPCARD RESIDENT", Resident: {Entity: {EntityName: "slkdjf"}}}),
          assertNoRule("nm_relen1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            Resident: {Entity: {EntityName: "slkdjf"}}
          }),
          assertFailure("nm_relen1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            Resident: {Entity: {TradingName: "slkdjf"}}
          }),

          // The words specified under Non-resident or Resident ExceptionName, must not be used
          // NOTE: Needs to make provision for STRATE
          assertFailure("nm_relen6", {
            ReportingQualifier: "BOPCARD RESIDENT", 
            Resident: {Entity: {EntityName: 'MUTUAL PARTY'}}
          }),
          assertSuccess("nm_relen6", {
            ReportingQualifier: "BOPCARD RESIDENT", 
            Resident: {Entity: {EntityName: 'SOMETHING ELSE'}}
          }),
          assertNoRule("nm_relen6", {
            ReportingQualifier: "BOPCARD NON RESIDENT", 
            Resident: {Entity: {EntityName: 'MUTUAL PARTY'}}
          }),

      // field: "ReplacementOriginalReference",
          // Additional spaces identified in data content
          assertSuccess("nm_repor3", {
            ReportingQualifier: 'BOPCUS',
            ReplacementOriginalReference: ""
          }),
          assertFailure("nm_repor3", {
            ReportingQualifier: 'BOPCUS',
            ReplacementOriginalReference: "Space here"
          }),
          assertFailure("nm_repor3", {
            ReportingQualifier: 'BOPCUS',
            ReplacementOriginalReference: "BothCharacter!and Space"
          }),
          assertSuccess("nm_repor3", {
            ReportingQualifier: 'BOPCUS',
            ReplacementOriginalReference: "NoSpacehere"
          }),

      // field: "NonResident.Individual.PassportNumber",
          // Additional spaces identified in data content
          assertSuccess("nm_nrpn2", {
            ReportingQualifier: 'BOPCUS', NonResident: {Individual: {PassportNumber: ''}}
          }),
          assertFailure("nm_nrpn2", {
            ReportingQualifier: 'BOPCUS', NonResident: {Individual: {PassportNumber: 'Space here'}}
          }),
          assertFailure("nm_nrpn2", {
            ReportingQualifier: 'BOPCUS', NonResident: {Individual: {PassportNumber: 'BothCharacter!and Space'}}
          }),
          assertSuccess("nm_nrpn2", {
            ReportingQualifier: 'BOPCUS', NonResident: {Individual: {PassportNumber: 'NoSpacehere'}}
          }),

      // field: "NonResident.Entity.CardMerchantName",
          // Additional spaces identified in data content
          assertSuccess("nm_nrcmn3", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {EntityName: "", CardMerchantName: "", AccountIdentifier: ""}}
          }),
          assertFailure("nm_nrcmn3", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {EntityName: "", CardMerchantName: "Space here", AccountIdentifier: ""}}
          }),
          assertFailure("nm_nrcmn3", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {EntityName: "", CardMerchantName: "BothCharacter!and Space", AccountIdentifier: ""}}
          }),
          assertSuccess("nm_nrcmn3", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {EntityName: "", CardMerchantName: "NoSpacehere", AccountIdentifier: ""}}
          }),

      // field: "Resident.Entity.RegistrationNumber",
          // Additional spaces identified in data content
          assertSuccess("nm_rern4", {
            ReportingQualifier: "BOPCUS",
            Resident: {Entity: {EntityName: "company3", RegistrationNumber: ""}}
          }),
          assertFailure("nm_rern4", {
            ReportingQualifier: "BOPCUS",
            Resident: {Entity: {EntityName: "company3", RegistrationNumber: "Space here"}}
          }),
          assertFailure("nm_rern4", {
            ReportingQualifier: "BOPCUS",
            Resident: {Entity: {EntityName: "company3", RegistrationNumber: "BothCharacter!and Space"}}
          }),
          assertSuccess("nm_rern4", {
            ReportingQualifier: "BOPCUS",
            Resident: {Entity: {EntityName: "company3", RegistrationNumber: "NoSpacehere"}}
          }),

      // field: "Resident.Entity.TradingName",
          // If Resident EntityCustomer is used, it may be completed
          assertSuccess("nm_retn1", {ReportingQualifier: "BOPCUS", Resident: {Entity: {TradingName: "tradingName"}}}),
          assertNoRule("nm_retn1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            Resident: {Entity: {TradingName: "tradingName"}}
          }),
          assertFailure("nm_retn1", {ReportingQualifier: "BOPCARD RESIDENT", Resident: {Entity: {EntityName: "tradingName"}}}),

      // field: ["NonResident.Individual.Address.Country", "NonResident.Entity.Address.Country"],
          // SWIFT country code may not be {{Locale}}
          assertSuccess("nm_nrictry3:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {Country: "AD"}}}
          }),
          assertFailure("nm_nrictry3:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {Country: "NA"}}}
          }),
          assertNoRule("nm_nrictry3:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Individual: {Address: {Country: "AD"}}}
          }),
          assertSuccess("nm_nrictry3:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {Country: "AD"}}}
          }),
          assertFailure("nm_nrictry3:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {Country: "NA"}}}
          }),
          assertNoRule("nm_nrictry3:2", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Entity: {Address: {Country: "AD"}}}
          }),
          
      // field: ["NonResident.Individual.Address.AddressLine1", "NonResident.Entity.Address.AddressLine1"],
          // Must not be completed
          assertSuccess("nm_nrial11:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Individual: {Address: {AddressLine1: ""}}}
          }),
          assertFailure("nm_nrial11:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Individual: {Address: {AddressLine1: "testAddressLine"}}}
          }),
          assertNoRule("nm_nrial11:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {AddressLine1: "testAddressLine"}}}
          }),
          assertSuccess("nm_nrial11:2", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {Address: {AddressLine1: ""}}}
          }),
          assertFailure("nm_nrial11:2", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Entity: {Address: {AddressLine1: "testAddressLine"}}}
          }),
          assertNoRule("nm_nrial11:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {AddressLine1: "testAddressLine"}}}
          }),

      // field: ["NonResident.Individual.Address.AddressLine2", "NonResident.Entity.Address.AddressLine2"],
          // Must not be completed  
          assertSuccess("nm_nrial21:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Individual: {Address: {AddressLine2: ""}}}
          }),
          assertFailure("nm_nrial21:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Individual: {Address: {AddressLine2: "testAddressLine2"}}}
          }),
          assertNoRule("nm_nrial21:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {AddressLine2: "testAddressLine2"}}}
          }),
          assertSuccess("nm_nrial21:2", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {Address: {AddressLine2: ""}}}
          }),
          assertFailure("nm_nrial11:2", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Entity: {Address: {AddressLine1: "testAddressLine2"}}}
          }),
          assertNoRule("nm_nrial21:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {AddressLine2: "testAddressLine2"}}}
          }),

      // field: ["NonResident.Individual.Address.Suburb", "NonResident.Entity.Address.Suburb"],
          // Must not be completed
          assertSuccess("nm_nrial31:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Individual: {Address: {Suburb: ""}}}
          }),
          assertFailure("nm_nrial31:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Individual: {Address: {Suburb: "testSuburb"}}}
          }),
          assertNoRule("nm_nrial31:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {Suburb: "testSuburb"}}}
          }),
          assertSuccess("nm_nrial31:2", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {Address: {Suburb: ""}}}
          }),
          assertFailure("nm_nrial31:2", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Entity: {Address: {Suburb: "testSuburb"}}}
          }),
          assertNoRule("nm_nrial31:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {Suburb: "testSuburb"}}}
          }),

      // field: ["NonResident.Individual.Address.City", "NonResident.Entity.Address.City"],
          // Must not be completed 
          assertSuccess("nm_nric2:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Individual: {Address: {City: ""}}}
          }),
          assertFailure("nm_nric2:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Individual: {Address: {City: "testCity"}}}
          }),
          assertNoRule("nm_nric2:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {City: "testCity"}}}
          }),
          assertSuccess("nm_nric2:2", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {Address: {City: ""}}}
          }),
          assertFailure("nm_nric2:2", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Entity: {Address: {City: "testCity"}}}
          }),
          assertNoRule("nm_nric2:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {City: "testCity"}}}
          }),

      // field: ["NonResident.Individual.Address.State", "NonResident.Entity.Address.State"],
          // Must not be completed
          assertSuccess("nm_nris2:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Individual: {Address: {State: ""}}}
          }),
          assertFailure("nm_nris2:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Individual: {Address: {State: "testState"}}}
          }),
          assertNoRule("nm_nris2:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {State: "testState"}}}
          }),
          assertSuccess("nm_nris2:2", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {Address: {State: ""}}}
          }),
          assertFailure("nm_nris2:2", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Entity: {Address: {State: "testState"}}}
          }),
          assertNoRule("nm_nris2:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {State: "testState"}}}
          }),

      // field: ["NonResident.Individual.Address.PostalCode", "NonResident.Entity.Address.PostalCode"],
          // Must not be completed
          assertSuccess("nm_nriz2:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Individual: {Address: {PostalCode: ""}}}
          }),
          assertFailure("nm_nriz2:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Individual: {Address: {PostalCode: "testPostalCode"}}}
          }),
          assertNoRule("nm_nriz2:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {PostalCode: "testPostalCode"}}}
          }),
          assertSuccess("nm_nriz2:2", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {Address: {PostalCode: ""}}}
          }),
          assertFailure("nm_nriz2:2", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Entity: {Address: {PostalCode: "testPostalCode"}}}
          }),
          assertNoRule("nm_nriz2:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {PostalCode: "testPostalCode"}}}
          }),

      // field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
          //NonResident.Individual.AccountIdentifier - If the Flow is OUT must contain a value of NON RESIDENT OTHER or NON RESIDENT NAD or NON RESIDENT FCA or FCA RESIDENT or VOSTRO or CASH or FCA RESIDENT or RES FOREIGN BANK ACCOUNT
          assertSuccess("nm_nriaid1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {AccountIdentifier: "CASH"}}
          }),
          assertSuccess("nm_nriaid1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {AccountIdentifier: "RES FOREIGN BANK ACCOUNT"}}
          }),
          assertFailure("nm_nriaid1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {AccountIdentifier: "MASTER SEND"}}
          }),
          assertNoRule("nm_nriaid1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Individual: {AccountIdentifier: "CASH"}}
          }),

          //NonResident.Entity.AccountIdentifier - If the Flow is OUT must contain a value of NON RESIDENT OTHER or NON RESIDENT NAD or NON RESIDENT FCA or FCA RESIDENT or VOSTRO or CASH or FCA RESIDENT or RES FOREIGN BANK ACCOUNT
          assertSuccess("nm_nreaid1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {AccountIdentifier: "CASH"}}
          }),
          assertSuccess("nm_nreaid1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {AccountIdentifier: "RES FOREIGN BANK ACCOUNT"}}
          }),
          assertFailure("nm_nreaid1", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {AccountIdentifier: "MASTER SEND"}}}),
          assertNoRule("nm_nreaid1:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {AccountIdentifier: "CASH"}}
          }),

          //TODO: Make sure these rules work
          assertSuccess("nm_nreaid2", {
            ReportingQualifier: "BOPCUS",
            Flow: "OUT",
            NonResident: {Individual: {AccountIdentifier: "FCA RESIDENT"}},
            MonetaryAmount: [{CategoryCode: '513'}]
          }),

          assertSuccess("nm_nreaid2", {
            ReportingQualifier: "BOPCUS",
            Flow: "OUT",
            NonResident: {Individual: {}},
            MonetaryAmount: [{CategoryCode: '513'}]
          }),

          assertFailure("nm_nreaid2", {
            ReportingQualifier: "BOPCUS",
            Flow: "OUT",
            NonResident: {Entity: {AccountIdentifier: "FCA RESIDENT"}},
            MonetaryAmount: [{CategoryCode: '513'}]
          }),

          assertSuccess("nm_nreaid2", {
            ReportingQualifier: "BOPCUS",
            Flow: "OUT",
            NonResident: {Entity: {}},
            MonetaryAmount: [{CategoryCode: '513'}]
          }),

      // field: ["NonResident.Individual.AccountNumber", "NonResident.Entity.AccountNumber"],
          // If the Flow is OUT must be completed if the AccountIdentifier is not CASH
          assertSuccess("nm_nrian1:1", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Individual:{AccountIdentifier: "FCA RESIDENT", AccountNumber: "1235498"}}}),
          assertSuccess("nm_nrian1:1", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Individual:{AccountIdentifier: "CASH", AccountNumber: ""}}}),
          assertFailure("nm_nrian1:1", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Individual:{AccountIdentifier: "FCA RESIDENT", AccountNumber: ""}}}),
          assertSuccess("nm_nrian1:2", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Entity:{AccountIdentifier: "FCA RESIDENT", AccountNumber: "1235498"}}}),
          assertSuccess("nm_nrian1:2", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Entity:{AccountIdentifier: "CASH", AccountNumber: ""}}}),
          assertFailure("nm_nrian1:2", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Entity:{AccountIdentifier: "FCA RESIDENT", AccountNumber: ""}}}),

      // field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],
          // Must be completed if Flow is OUT and BoPCategory is 10101 to 10110 or 10500 or 10600
          assertSuccess('nm_ccn2:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: '1234'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertFailure('nm_ccn2:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: ''}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertNoRule('nm_ccn2:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: ''}},
            MonetaryAmount: [{CategoryCode: '107', CategorySubCode: '11'},
              {CategoryCode: '107', CategorySubCode: '10'}]
          }),
          assertSuccess('nm_ccn2:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: '1234'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertFailure('nm_ccn2:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: ''}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '103', CategorySubCode: '10'}]
          }),
          assertNoRule('nm_ccn2:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: ''}},
            MonetaryAmount: [{CategoryCode: '107', CategorySubCode: '11'},
              {CategoryCode: '107', CategorySubCode: '11'}]
          }),

          // If the FLOW is IN and the BoPCategory is 10101 to 10110 or 10301 to 10310 or 10500 or 10600, the CustomsClientNumber must contain a valid Customs client number.
          assertFailure('nm_ccn3:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {CustomsClientNumber: '1'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertSuccess('nm_ccn3:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {CustomsClientNumber: '123'}}, 
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertNoRule('nm_ccn3:1', {
            ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'IN', Resident: {Individual: {CustomsClientNumber: '123'}}, 
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertNoRule('nm_ccn3:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {CustomsClientNumber: '123'}},
            MonetaryAmount: [{CategoryCode: '107', CategorySubCode: '11'},
              {CategoryCode: '107', CategorySubCode: '10'}]
          }),

          assertFailure('nm_ccn3:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {CustomsClientNumber: '1'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertSuccess('nm_ccn3:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {CustomsClientNumber: '123'}}, 
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertNoRule('nm_ccn3:2', {
            ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'IN', Resident: {Entity: {CustomsClientNumber: '123'}}, 
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertNoRule('nm_ccn3:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {CustomsClientNumber: '123'}},
            MonetaryAmount: [{CategoryCode: '107', CategorySubCode: '11'},
              {CategoryCode: '107', CategorySubCode: '10'}]
          }),

          // If the FLOW is OUT and BoPCategory is 10101 to 10110 or 10301 to 10310 or 10500 or 10600, the CustomsClientNumber must contain a valid Customs Client Number
          assertFailure('nm_ccn4:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: '1'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertFailure('nm_ccn4:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: 'string'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertSuccess('nm_ccn4:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: '123'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertNoRule('nm_ccn4:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {CustomsClientNumber: '123'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),

          assertFailure('nm_ccn4:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: '1'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertFailure('nm_ccn4:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: 'string'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertSuccess('nm_ccn4:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: '123'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertNoRule('nm_ccn4:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {CustomsClientNumber: '123'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),

          // Must not be completed
          assertFailure('nm_ccn5:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {CustomsClientNumber: 'abc'}},
          }),
          assertSuccess('nm_ccn5:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {CustomsClientNumber: ''}},
          }),
          assertNoRule('nm_ccn5:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {CustomsClientNumber: ''}},
          }),
          assertFailure('nm_ccn5:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {CustomsClientNumber: 'abc'}},
          }),
          assertSuccess('nm_ccn5:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {CustomsClientNumber: ''}},
          }),
          assertNoRule('nm_ccn5:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {CustomsClientNumber: ''}},
          }),

           // This rule is only for SA, Namibia does not need 70707070 functionality
          // assertSuccess('nm_ccn6:1', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: '12345678'}},
          //   MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '101', CategorySubCode: '10'}]
          // }),
          // assertFailure('nm_ccn6:1', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: '70707070'}},
          //   MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '103', CategorySubCode: '10'}]
          // }),


      // Resident.Individual.TAXClearanceCertificateIndicator, Resident.Entity.TaxClearanceCertificateIndicator
          // Must not be completed
          assertSuccess('nm_tcci2:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT'
          }),
          assertFailure('nm_tcci2:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {
              Individual: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: '1234'
              }
            }
          }),
          assertSuccess('nm_tcci2:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT'
          }),
          assertFailure('nm_tcci2:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {
              Entity: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: '1234'
              }
            }
          }),

      // field: ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1"],
          // Resident.Individual.StreetAddress.AddressLine1 - Physical address must be completed
          assertSuccess('nm_a1_1:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {AddressLine1: '12 Foo Street'}}}
          }),
          assertFailure('nm_a1_1:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {AddressLine1: ''}}}
          }),
          assertNoRule('nm_a1_1:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {AddressLine1: ''}}}
          }),
          // Resident.Individual.StreetAddress.AddressLine1 - Physical address must be completed
          assertSuccess('nm_a1_1:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {AddressLine1: '12 Foo Street'}}}
          }),
          assertFailure('nm_a1_1:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {AddressLine1: ''}}}
          }),
          assertNoRule('nm_a1_1:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {AddressLine1: ''}}}
          }),

      // field: ["Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb"],
          // Resident.Individual.StreetAddress.Suburb - Physical StreetSuburb must be completed
          assertSuccess('nm_s1:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {Suburb: 'MoeggoeVille'}}}
          }),
          assertFailure('nm_s1:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {Suburb: ''}}}
          }),
          assertNoRule('nm_s1:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {Suburb: ''}}}
          }),
          // Resident.Entity.StreetAddress.Suburb - Physical StreetSuburb must be completed
          assertSuccess('nm_s1:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {Suburb: 'MoeggoeVille'}}}
          }),
          assertFailure('nm_s1:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {Suburb: ''}}}
          }),
          assertNoRule('nm_s1:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {Suburb: ''}}}
          }),

      // field: ["Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City"],
          // Resident.Individual.StreetAddress.City - Physical city or town must be completed
          assertSuccess('nm_c1:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {City: 'TestCity'}}}
          }),
          assertFailure('nm_c1:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {City: ''}}}
          }),
          assertNoRule('nm_c1:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {City: ''}}}
          }),
          // Resident.Entity.StreetAddress.City - Physical city or town must be completed
          assertSuccess('nm_c1:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {City: 'TestCity'}}}
          }),
          assertFailure('nm_c1:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {City: ''}}}
          }),
          assertNoRule('nm_c1:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {City: ''}}}
          }),

      // field: [ "Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City"],
          // Resident.Individual.PostalAddress.City - City or town must be completed
          assertSuccess('nm_c2:1', {ReportingQualifier: 'BOPCUS', Resident: {Individual: {PostalAddress: {City: 'TestCity'}}}}),
          assertFailure('nm_c2:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {PostalAddress: {City: ''}}}
          }),
          assertNoRule('nm_c2:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {PostalAddress: {City: ''}}}
          }),
          // Resident.Entity.PostalAddress.City - City or town must be completed
          assertSuccess('nm_c2:2', {ReportingQualifier: 'BOPCUS', Resident: {Entity: {PostalAddress: {City: 'TestCity'}}}}),
          assertFailure('nm_c2:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {PostalAddress: {City: ''}}}
          }),
          assertNoRule('nm_c2:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {PostalAddress: {City: ''}}}
          }),

      // field: [ "Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1",
      //   "Resident.Individual.StreetAddress.AddressLine2", "Resident.Entity.StreetAddress.AddressLine2",
      //   "Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb",
      //   "Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City",
      //   "Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province",
      //   "Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1",
      //   "Resident.Individual.PostalAddress.AddressLine2", "Resident.Entity.PostalAddress.AddressLine2",
      //   "Resident.Individual.PostalAddress.Suburb", "Resident.Entity.PostalAddress.Suburb",
      //   "Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City",
      //   "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
          // "Resident.Individual.StreetAddress.AddressLine1" - Must not be completed
          assertSuccess('nm_s3:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {AddressLine1: ''}}}
          }),
          assertFailure('nm_s3:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {AddressLine1: 'TestAddressLine1'}}}
          }),
          assertNoRule('nm_s3:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {StreetAddress: {AddressLine1: 'TestAddressLine1'}}}
          }),
          // "Resident.Entity.StreetAddress.AddressLine1" - Must not be completed
          assertSuccess('nm_s3:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {AddressLine1: ''}}}
          }),
          assertFailure('nm_s3:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {AddressLine1: 'TestAddressLine1'}}}
          }),
          assertNoRule('nm_s3:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {StreetAddress: {AddressLine1: 'TestAddressLine1'}}}
          }),
          // "Resident.Individual.StreetAddress.AddressLine2" - Must not be completed
          assertSuccess('nm_s3:3', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {AddressLine2: ''}}}
          }),
          assertFailure('nm_s3:3', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {AddressLine2: 'TestAddressLine2'}}}
          }),
          assertNoRule('nm_s3:3', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {StreetAddress: {AddressLine2: 'TestAddressLine2'}}}
          }),
          // "Resident.Entity.StreetAddress.AddressLine2" - Must not be completed
          assertSuccess('nm_s3:4', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {AddressLine2: ''}}}
          }),
          assertFailure('nm_s3:4', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {AddressLine2: 'TestAddressLine2'}}}
          }),
          assertNoRule('nm_s3:4', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {StreetAddress: {AddressLine2: 'TestAddressLine2'}}}
          }),
          // "Resident.Individual.StreetAddress.Suburb" - Must not be completed
          assertSuccess('nm_s3:5', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {Suburb: ''}}}
          }),
          assertFailure('nm_s3:5', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {Suburb: 'TestSuburb'}}}
          }),
          assertNoRule('nm_s3:5', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {StreetAddress: {Suburb: 'TestSuburb'}}}
          }),
          // "Resident.Entity.StreetAddress.Suburb" - Must not be completed
          assertSuccess('nm_s3:6', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {Suburb: ''}}}
          }),
          assertFailure('nm_s3:6', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {Suburb: 'TestSuburb'}}}
          }),
          assertNoRule('nm_s3:6', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {StreetAddress: {Suburb: 'TestSuburb'}}}
          }),

        //Resident.Individual.ForeignIDCountry
        assertSuccess("rifidc1", {
          ReportingQualifier: "BOPCUS",
          Resident: {
            Individual: {
              ForeignIDNumber: "1234567890123",
              ForeignIDCountry: "ZA"
            }
          }
        }),
        assertSuccess("rifidc1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: {
            Individual: {
              ForeignIDNumber: "1234567890123",
              ForeignIDCountry: "ZA"
            }
          }
        }),
        assertFailure("rifidc1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { ForeignIDNumber: "1234567890123" } }
        }),
        assertFailure("rifidc1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { ForeignIDNumber: "1234567890123" } }
        }),



      // field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
          // Must not be completed
          assertFailure('nm_tccr2:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: '1234'
              }
            }
          }),
          assertSuccess('nm_tccr2:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: ''
              }
            }
          }),
          assertNoRule('nm_tccr2:1', {
            ReportingQualifier: 'BOPDIR',
            Resident: {
              Individual: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: ''
              }
            }
          }),
          assertFailure('nm_tccr2:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Entity: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: '1234'
              }
            }
          }),
          assertSuccess('nm_tccr2:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Entity: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: ''
              }
            }
          }),
          assertNoRule('nm_tccr2:2', {
            ReportingQualifier: 'BOPDIR',
            Resident: {
              Entity: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: ''
              }
            }
          }),

      // field: [ "Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1"],
          // Must be completed
          assertSuccess('nm_a1_3:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {PostalAddress: {AddressLine1: '12 Foo Street'}}}
          }),
          assertFailure('nm_a1_3:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {PostalAddress: {AddressLine1: ''}}}
          }),
          assertFailure('nm_a1_3:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {PostalAddress: {AddressLine1: ''}}}
          }),
          assertSuccess('nm_a1_3:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {PostalAddress: {AddressLine1: '12 Foo Street'}}}
          }),
          assertFailure('nm_a1_3:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {PostalAddress: {AddressLine1: ''}}}
          }),
          assertFailure('nm_a1_3:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {PostalAddress: {AddressLine1: ''}}}
          }),

      // field: ["Resident.Individual.AccountNumber", "Resident.Entity.AccountNumber"],
          // Must be completed if the Flow is OUT and the AccountIdentifier under AdditionalCustomerData is not CASH or EFT or CARD PAYMENT
          assertFailure('nm_accno4:1', {
            ReportingQualifier: "BOPCUS",
            Flow: 'OUT',
            Resident: {Individual: {AccountNumber: "", AccountIdentifier: "CREDIT CARD"}}
          }),
          assertSuccess('nm_accno4:1', {
            ReportingQualifier: "BOPCUS",
            Flow: 'OUT',
            Resident: {
              Individual: {
                AccountNumber: "234234",
                AccountIdentifier: "CREDIT CARD"
              }
            }
          }),
          assertNoRule('nm_accno4:1', {
            ReportingQualifier: "BOPCUS",
            Flow: 'IN',
            Resident: {Individual: {AccountNumber: "1234", AccountIdentifier: "CASH"}}
          }),
          assertFailure('nm_accno4:2', {
            ReportingQualifier: "BOPCUS",
            Flow: 'OUT',
            Resident: {Entity: {AccountNumber: "", AccountIdentifier: "CREDIT CARD"}}
          }),
          assertSuccess('nm_accno4:2', {
            ReportingQualifier: "BOPCUS",
            Flow: 'OUT',
            Resident: {
              Entity: {
                AccountNumber: "234234",
                AccountIdentifier: "CREDIT CARD"
              }
            }
          }),
          assertNoRule('nm_accno4:2', {
            ReportingQualifier: "BOPCUS",
            Flow: 'IN',
            Resident: {Entity: {AccountNumber: "", AccountIdentifier: "CASH"}}
          }),

      // field: ["Resident.Individual.AccountName", "Resident.Entity.AccountName"],
          // Must be completed except if the AccountIdentifier is CASH or EFT or CARD PAYMENT
          assertSuccess("nm_an2:1", {ReportingQualifier: "BOPCUS", Resident: {Individual: {AccountName: ""}}}),
          assertFailure("nm_an2:1", {ReportingQualifier: "BOPCUS", Resident: {Individual: {AccountIdentifier: "CASH", AccountName: "slkfj"}}}),
          assertNoRule("nm_an2:1", {ReportingQualifier: "BOPCUS", Resident: {Individual: {AccountIdentifier: "RESIDENT OTHER", AccountName: "slkfj"}}}),
          assertSuccess("nm_an2:2", {ReportingQualifier: "BOPCUS", Resident: {Entity: {AccountName: ""}}}),
          assertFailure("nm_an2:2", {ReportingQualifier: "BOPCUS", Resident: {Entity: {AccountIdentifier: "CASH", AccountName: "slkfj"}}}),
          assertNoRule("nm_an2:2", {ReportingQualifier: "BOPCUS", Resident: {Entity: {AccountIdentifier: "RESIDENT OTHER", AccountName: "slkfj"}}}),

      // field: "TrnReference",
          // Additional spaces identified in data content
          assertSuccess("nm_tref2", {TrnReference: 'trnref'}),
          assertFailure("nm_tref2", {TrnReference: 'trn  ref'}),
          assertFailure("nm_tref2", {TrnReference: ' trnref'}),
          assertFailure("nm_tref2", {TrnReference: 'trnref '}),


//================================================================================================================================================
//  # MONEY (3/6)
//================================================================================================================================================

      // field: "SequenceNumber",
          // Must contain a sequential SequenceNumber entries 
          assertSuccess('nm_mseq1', {
            ReportingQualifier: 'BOPCARD',
            MonetaryAmount: [{SequenceNumber: "1"}, {SequenceNumber: "2"}]
          }),
          assertFailure('nm_mseq1', {
            ReportingQualifier: 'BOPCARD',
            MonetaryAmount: [{SequenceNumber: "2"}]
          }),
     
      // field: "{{LocalValue}}",
          // If DomesticCurrencyCode is NAD and the ForeignCurrencyCode is ZAR, the DomesticValue must be equal to ForeignValue.
          assertSuccess('nm_nvl1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            FlowCurrency: 'ZAR',
            MonetaryAmount: [{
              DomesticValue:8.00,
              ForeignValue: 8}]
          }),
          assertSuccess('nm_nvl1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            FlowCurrency: 'ZAR',
            MonetaryAmount: [{
              DomesticValue:8.20,
              ForeignValue: 8.2}]
          }),
          assertSuccess('nm_nvl1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            FlowCurrency: 'ZAR',
            MonetaryAmount: [{
              DomesticValue:8.11,
              ForeignValue: 8.11}]
          }),
          assertFailure('nm_nvl1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            FlowCurrency: 'ZAR',
            MonetaryAmount: [{
              DomesticValue:8.11,
              ForeignValue: 9.11}]
          }),
          assertNoRule('nm_nvl1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            FlowCurrency: 'USD',
            MonetaryAmount: [{
              DomesticValue:8.11,
              ForeignValue: 8.11}]
          }),

          // If DomesticCurrencyCode is NAD and the ForeignCurrencyCode is the same, the DomesticValue must be equal to ForeignValue.
          // assertSuccess('nm_nvl2', {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   FlowCurrency: 'NAD',
          //   MonetaryAmount: [{
          //     DomesticValue:8.11,
          //     ForeignValue: 8.11}]
          // }),
          // assertFailure('nm_nvl2', {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   FlowCurrency: 'NAD',
          //   MonetaryAmount: [{
          //     DomesticValue:8.11,
          //     ForeignValue: 9.11}]
          // }),
          // assertNoRule('nm_nvl2', {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   FlowCurrency: 'USD',
          //   MonetaryAmount: [{
          //     DomesticValue:8.11,
          //     ForeignValue: 8.11}]
          // }),

          // If BoPCategory 10700 is used, the value must not exceed {{LocalCurrencySymbol}}500.00
          assertSuccess('nm_nvl3', {
            ReportingQualifier: 'BOPCUS',
            FlowCurrency: 'NAD',
            MonetaryAmount: [{
              CategoryCode: '107',
              DomesticValue:499}]
          }),
          assertSuccess('nm_nvl3', {
            ReportingQualifier: 'BOPCUS',
            FlowCurrency: 'NAD',
            MonetaryAmount: [{
              CategoryCode: '107',
              DomesticValue:500}]
          }),
          assertFailure('nm_nvl3', {
            ReportingQualifier: 'BOPCUS',
            FlowCurrency: 'NAD',
            MonetaryAmount: [{
              CategoryCode: '107',
              DomesticValue:501}]
          }),
          assertNoRule('nm_nvl3', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            FlowCurrency: 'NAD',
            MonetaryAmount: [{
              CategoryCode: '107',
              DomesticValue:501}]
          }),

          // Must not equal ForeignValue
          assertSuccess('nm_nvl4', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            FlowCurrency: 'ZAR',
            MonetaryAmount: [{
              DomesticValue:8.11,
              ForeignValue: 8.11}]
          }),
          assertFailure('nm_nvl4', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            FlowCurrency: 'USD',
            MonetaryAmount: [{
              DomesticValue:8.11,
              ForeignValue: 8.11}]
          }),
          assertNoRule('nm_nvl4', {
            ReportingQualifier: 'NON REPORTABLE',
            FlowCurrency: 'USD',
            MonetaryAmount: [{
              DomesticValue:8.11,
              ForeignValue: 8.11}]
          }),
         
      // field: "DomesticCurrencyCode",
          // Invalid SWIFT currency code.
          assertSuccess("nm_ncc2", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{DomesticCurrencyCode: 'NAD'}]}),
          assertSuccess("nm_ncc2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{DomesticCurrencyCode: 'NAD'}]}),
          assertFailure("nm_ncc2", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{DomesticCurrencyCode: 'ABC'}]}),
          assertFailure("nm_ncc2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{DomesticCurrencyCode: 'ABC'}]}),
          
          // SWIFT currecy code must only be NAD
          assertSuccess("nm_ncc3", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{DomesticCurrencyCode: 'NAD'}]}),
          assertSuccess("nm_ncc3", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{DomesticCurrencyCode: 'NAD'}]}),
          assertFailure("nm_ncc3", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{DomesticCurrencyCode: 'ZAR'}]}),
          assertFailure("nm_ncc3", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{DomesticCurrencyCode: 'ZAR'}]}),
          
          // Must not be completed
          assertSuccess("nm_ncc4", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            LocalCurrency: 'NAD',
            ForeignCurrencyCode:'ZAR',
            MonetaryAmount: [{
              NADValue:0.00,
              ForeignValue: 0.00}]
          }),
          assertFailure("nm_ncc4", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            LocalCurrency: 'NAD',
            ForeignCurrencyCode:'ZAR',
            MonetaryAmount: [{
              DomesticCurrencyCode: 'COMPLETED',
              NADValue:0.20,
              ForeignValue: 0.00}]
          }),

      // field: "LoanRefNumber",
          // If the BoPCategory 80100, or 80200, or 80300, or 80400, or 81000 or 81500 or 81600 or 81700 or 81800 or 81900 is used, must be completed.
          assertSuccess("nm_mlrn1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '801', LoanRefNumber: 'XYZ'}]
          }),
          assertFailure("nm_mlrn1", {
            ReportingQualifier: 'BOPCUS', 
            MonetaryAmount: [{CategoryCode: '801'}]}),

          // If the BoPCategory 10600 or 30901 or 30902 or 30903 or 30904 or 30905 or 30906 or 30907, it must be completed.
          assertSuccess("nm_mlrn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '106',
              LoanRefNumber: 'test'
            }]
          }),
          assertFailure("nm_mlrn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '106',
              LoanRefNumber: ''
            }]
          }),
          assertNoRule("nm_mlrn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '08',
              LoanRefNumber: ''
            }]
          }),
          assertNoRule("nm_mlrn2", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{
              CategoryCode: '106',
              LoanRefNumber: 'loanref'
            }]
          }),

          // For any other BoPCategory other than 801, or 802, or 803 or 804 or 810 or 815 or 816 or 817 or 818 or 819 or 106 or 309, it must not be completed
          assertSuccess("nm_mlrn3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '80100', LoanRefNumber: ''}]
          }),
          assertNoRule("nm_mlrn3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '80500', LoanRefNumber: ''}]
          }),
          assertFailure("nm_mlrn3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '80100', LoanRefNumber: 'XYZ'}]
          }),

          // Invalid loan reference number
          assertSuccess("nm_mlrn12", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              LoanRefNumber: '123'
            }]
          }),
          assertFailure("nm_mlrn12", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              LoanRefNumber: '123a'
            }]
          }),     

      // field: "LoanInterestRate",
         //If the BoPCategory 30901 to 30907 is used, must be completed reflecting the percentage interest paid.
          assertSuccess("nm_mlir1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '309',CategorySubCode: '01', LoanInterestRate: '0.0'}]
          }),
          assertFailure("nm_mlir1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '309',CategorySubCode: '01'}]
          }),
          assertNoRule("nm_mlir1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '309',CategorySubCode: '08', LoanInterestRate: '0.0'}]
          }),

          //If the BoPCategory 30901 to 30907 must be completed in the format 0.00
          assertSuccess("nm_mlir2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '01',
              LoanInterestRate: '5.12'
            }]
          }),
          assertFailure("nm_mlir2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '01',
              LoanInterestRate: '5'
            }]
          }),
          assertFailure("nm_mlir2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '01',
              LoanInterestRate: '12.'
            }]
          }),
          assertFailure("nm_mlir2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '01',
              LoanInterestRate: '.14'
            }]
          }),

          // Except for BoPCategory 30901, 30902, 30903, 30904, 30905, 30906, 30907 it must not be completed
          assertSuccess("nm_mlir3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '01',
              LoanInterestRate: '20'
            }]
          }),
          assertFailure("nm_mlir3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '01',
              LoanInterestRate: ''
            }]
          }),
          assertNoRule("nm_mlir3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '08',
              LoanInterestRate: '20'
            }]
          }),

          // Must not be completed
          assertSuccess("nm_mlir4", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mlir4", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LoanInterestRate: '5.12'}]}),
          assertNoRule("nm_mlir4", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{LoanInterestRate: '5.12'}]}),

      // field: "LocationCountry",
          // Must be completed
          assertSuccess("nm_mlc1", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{LocationCountry: 'US'}]}),
          assertFailure("nm_mlc1", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{LocationCountry: ''}]}), 
          assertSuccess("nm_mlc1", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LocationCountry: 'US'}]}),
          assertFailure("nm_mlc1", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LocationCountry: ''}]}),
          
          // //SWIFT country code may not be NA
          assertSuccess("nm_mlc3", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{LocationCountry: 'US'}]}),
          assertFailure("nm_mlc3", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{LocationCountry: 'NA'}]}),
          assertSuccess("nm_mlc3", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LocationCountry: 'US'}]}),
          assertFailure("nm_mlc3", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LocationCountry: 'NA'}]}),
          assertNoRule("nm_mlc3", {ReportingQualifier: 'NON RESIDENT RAND', MonetaryAmount: [{LocationCountry: 'NA'}]}),

          //TODO: Make sure this works

          assertFailure("nm_mlc4", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '03', LocationCountry: 'EU' }]
          }),

          assertNoRule("nm_mlc4", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ CategoryCode: '513', LocationCountry: 'EU' }]
          }),

          assertNoRule("nm_mlc4", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ CategoryCode: '517', LocationCountry: 'EU' }]
          }),

      // field: "BOPDIRTrnReference",
          // Must not be completed
          assertSuccess("nm_mdirtr1", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}]}),
          assertFailure("nm_mdirtr1", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{BOPDIRTrnReference: 'XYZ'}]}),
          assertNoRule("nm_mdirtr1", {ReportingQualifier: 'INTERBANK', MonetaryAmount: [{BOPDIRTrnReference: 'XYZ'}]}),

      // field: "BOPDIR{{DealerPrefix}}Code",
          // Must not be completed
          assertSuccess("nm_mdircd2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mdircd2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{BOPDIRRECode: 'XYZ'}]}),
          assertNoRule("nm_mdircd2", {ReportingQualifier: 'INTERBANK', MonetaryAmount:[{BOPDIRRECode: 'XYZ'}]}),

      // field: "ThirdParty.StreetAddress.Province",
          // Must be a valid region
          assertSuccess("nm_mtpsap1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {StreetAddress: {Province: 'CAPRIVI'}}}]
          }),
          assertNoRule("nm_mtpsap1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {StreetAddress: {Province: ''}}}]
          }),
          assertFailure("nm_mtpsap1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {StreetAddress: {Province: 'INVALIDREGION'}}}]
          }),

          // Must not be completed
          assertSuccess("nm_mtpsap2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtpsap2", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{ThirdParty: {StreetAddress: {Province: 'PROVINCE'}}}]
          }),
          assertNoRule("nm_mtpsap2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {StreetAddress: {Province: 'PROVINCE'}}}]
          }),

      // field: "ThirdParty.PostalAddress.Province",
          // Must be a valid region
          assertSuccess("nm_mtppap1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {PostalAddress: {Province: 'CAPRIVI'}}}]
          }),
          assertNoRule("nm_mtppap1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {PostalAddress: {Province: ''}}}]
          }),
          assertFailure("nm_mtppap1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {PostalAddress: {Province: 'INVALIDREGION'}}}]
          }),

          // Must not be completed
          assertSuccess("nm_mtppap2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtppap2", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{ThirdParty: {PostalAddress: {Province: 'PROVINCE'}}}]
          }),
          assertNoRule("nm_mtppap2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {PostalAddress: {Province: 'PROVINCE'}}}]
          }),

      // field: "ThirdParty.Individual.Surname",
          // Must not be completed
          assertSuccess("nm_mtpisn7", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtpisn7", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{ThirdParty: {Individual: {Surname: 'JONES'}}}]
          }),
          assertNoRule("nm_mtpisn7", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {Individual: {Surname: 'JONES'}}}]
          }),

      // field: "ThirdParty.Individual.Name",
          // If IndividualThirdPartySurname contains a value, the IndividualThirdPartyName must be completed       
          assertSuccess("nm_mtpinm1", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{ThirdParty: {Individual: {Name: 'ABC', Surname: 'XYZ'}}}]
          }),
          assertFailure("nm_mtpinm1", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{ThirdParty: {Individual: {Name: '', Surname: 'XYZ'}}}]
          }),
          assertNoRule("nm_mtpinm1", {
            ReportingQualifier: 'NON RESIDENT RAND',
            MonetaryAmount: [{ThirdParty: {Individual: {Name: '', Surname: 'XYZ'}}}]
          }),

      // field: "ThirdParty.Individual.Gender",
          // If IndividualThirdPartySurname contains a value, the IndividualThirdPartyGender must be completed
          assertSuccess("nm_mtpig1", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{ThirdParty: {Individual: {Gender: 'M', Surname: 'XYZ'}}}]
          }),
          assertFailure("nm_mtpig1", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{ThirdParty: {Individual: {Gender: '', Surname: 'XYZ'}}}]
          }),
          assertNoRule("nm_mtpig1", {
            ReportingQualifier: 'NON RESIDENT RAND',
            MonetaryAmount: [{ThirdParty: {Individual: {Gender: '', Surname: 'XYZ'}}}]
          }),

      // field: "ThirdParty.Individual.IDNumber",
          // If IndividualThirdPartySurname contains a value, either IndividualThirdPartyIDNumber or IndividualThirdPartyTempResPermitNumber must be completed
          assertSuccess("nm_mtpiid1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {Individual: {IDNumber: '6811035039084', Surname: 'XYZ'}}}]
          }),
          assertSuccess("nm_mtpiid1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {Individual: {TempResPermitNumber: '123', Surname: 'XYZ'}}}]
          }),
          assertSuccess("nm_mtpiid1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {Individual: {}}}]
          }),
          assertFailure("nm_mtpiid1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {Individual: {Surname: 'XYZ'}}}]
          }),
          assertFailure("nm_mtpiid1", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{ThirdParty: {Individual: {Surname: 'XYZ'}}}]
          }),
          assertNoRule("nm_mtpiid1", {
            ReportingQualifier: 'NON RESIDENT RAND',
            MonetaryAmount: [{ThirdParty: {Individual: {Surname: 'XYZ'}}}]
          }),

      // field: "ThirdParty.Individual.DateOfBirth",
          // If IndividualThirdPartySurname contains a value, the IndividualThirdPartyDateOfBirth must be completed
          assertSuccess("nm_mtpibd1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {Individual: {DateOfBirth: "1968-11-03", Surname: 'XYZ'}}}]
          }),
          assertFailure("nm_mtpibd1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {Individual: {Surname: 'XYZ'}}}]
          }),
          assertFailure("nm_mtpibd1", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{ThirdParty: {Individual: {Surname: 'XYZ'}}}]
          }),
          assertNoRule("nm_mtpibd1", {
            ReportingQualifier: 'NON RESIDENT RAND',
            MonetaryAmount: [{ThirdParty: {Individual: {Surname: 'XYZ'}}}]
          }),

      // field: "ThirdParty.Individual.TempResPermitNumber",
          // If IndividualThirdPartySurname contains a value, either IndividualThirdPartyIDNumber or IndividualThirdPartyTempResPermit Number must be completed
          assertSuccess("nm_mtpitp1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {Individual: {IDNumber: '6811035039084', Surname: 'XYZ'}}}]
          }),
          assertSuccess("nm_mtpitp1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {Individual: {TempResPermitNumber: '123', Surname: 'XYZ'}}}]
          }),
          assertSuccess("nm_mtpitp1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {Individual: {}}}]
          }),
          assertFailure("nm_mtpitp1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {Individual: {Surname: 'XYZ'}}}]
          }),
          assertFailure("nm_mtpitp1", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{ThirdParty: {Individual: {Surname: 'XYZ'}}}]
          }),
          assertNoRule("nm_mtpitp1", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{ThirdParty: {Individual: {Surname: 'XYZ'}}}]
          }),

      // field: "ThirdParty.Entity.Name", 
          // field: "ThirdParty.Entity.Name",
           assertSuccess("nm_mtpenm2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
           assertFailure("nm_mtpenm2", {
             ReportingQualifier: 'BOPCARD RESIDENT',
             MonetaryAmount: [{ThirdParty: {Entity: {Name: 'XYZ'}}}]
           }),
           assertNoRule("nm_mtpenm2", {
             ReportingQualifier: 'BOPCUS',
             MonetaryAmount: [{ThirdParty: {Entity: {Name: 'XYZ'}}}]
           }),

      //  field: "ThirdParty.Entity.RegistrationNumber",
          // ThirdParty.Entity.RegistrationNumber must be completed
          assertSuccess("nm_mtpern1", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtpern1", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{ThirdParty: {Entity: {RegistrationNumber: '123'}}}]
          }),
          assertFailure("nm_mtpern1", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{ThirdParty: {Entity: {RegistrationNumber: '123'}}}]
          }),

      // field: "ThirdParty.StreetAddress.AddressLine1",
          // Must not be completed
          assertSuccess("nm_mtpsal12", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtpsal12", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{ThirdParty: {StreetAddress: {AddressLine1: 'ADDR'}}}]
          }),
          assertNoRule("nm_mtpsal12", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {StreetAddress: {AddressLine1: 'ADDR'}}}]
          }),
 
      //  field: "ThirdParty.StreetAddress.AddressLine2",
          // Must not be completed
          assertSuccess("nm_mtpsal21", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtpsal21", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{ThirdParty: {StreetAddress: {AddressLine2: 'ADDR'}}}]
          }),
           assertNoRule("nm_mtpsal21", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {StreetAddress: {AddressLine2: 'ADDR'}}}]
          }),

        // field: "ThirdParty.StreetAddress.Suburb",
            // Must not be completed
            assertSuccess("nm_mtpsas2", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}]}),
            assertFailure("nm_mtpsas2", {
              ReportingQualifier: 'BOPCARD RESIDENT',
              MonetaryAmount: [{ThirdParty: {StreetAddress: {Suburb: 'SUB'}}}]
            }),
            assertNoRule("nm_mtpsas2", {
              ReportingQualifier: 'BOPCUS',
              MonetaryAmount: [{ThirdParty: {StreetAddress: {Suburb: 'SUB'}}}]
            }),

        // field: "ThirdParty.StreetAddress.City",
            // Must not be completed
            assertSuccess("nm_mtpsac2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
            assertFailure("nm_mtpsac2", {
              ReportingQualifier: 'BOPCARD NON RESIDENT',
              MonetaryAmount: [{ThirdParty: {StreetAddress: {City: 'CITY'}}}]
            }),
            assertFailure("nm_mtpsac2", {
              ReportingQualifier: 'BOPCARD NON RESIDENT',
              MonetaryAmount: [{ThirdParty: {StreetAddress: {City: 'CITY'}}}]
            }),

        // field: "ThirdParty.StreetAddress.PostalCode",
            // Must not be completed
            assertSuccess("nm_mtpsaz2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
            assertFailure("nm_mtpsaz2", {
              ReportingQualifier: 'BOPCARD NON RESIDENT',
              MonetaryAmount: [{ThirdParty: {StreetAddress: {PostalCode: '2192'}}}]
            }),
            assertNoRule("nm_mtpsaz2", {
              ReportingQualifier: 'BOPCUS',
              MonetaryAmount: [{ThirdParty: {StreetAddress: {PostalCode: '2192'}}}]
            }),

        // field: "ThirdParty.PostalAddress.AddressLine1",
            // Must not be completed
            assertSuccess("nm_mtppal12", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
            assertFailure("nm_mtppal12", {
              ReportingQualifier: 'BOPCARD NON RESIDENT',
              MonetaryAmount: [{ThirdParty: {PostalAddress: {AddressLine1: 'ADDR'}}}]
            }),
            assertNoRule("nm_mtppal12", {
              ReportingQualifier: 'BOPCUS',
              MonetaryAmount: [{ThirdParty: {PostalAddress: {AddressLine1: 'ADDR'}}}]
            }),

            
          // ThirdParty.PostalAddress.AddressLine2 must not be completed
          assertSuccess("nm_mtppal21", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtppal21", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{ThirdParty: {PostalAddress: {AddressLine2: 'ADDR'}}}]
          }),
          assertNoRule("nm_mtppal21", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {PostalAddress: {AddressLine2: 'ADDR'}}}]
          }),

          // ThirdParty.PostalAddress.Suburb Must not be completed
          assertSuccess("nm_mtppas2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtppas2", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{ThirdParty: {PostalAddress: {Suburb: 'SUB'}}}]
          }),
          assertNoRule("nm_mtppas2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {PostalAddress: {Suburb: 'SUB'}}}]
          }),

          // ThirdParty.PostalAddress.City Must not be completed
          assertSuccess("nm_mtppac2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtppac2", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{ThirdParty: {PostalAddress: {City: 'CITY'}}}]
          }),
          assertNoRule("nm_mtppac2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {PostalAddress: {City: 'CITY'}}}]
          }),

      // field: "ThirdParty.PostalAddress.PostalCode",
          // Must not be completed
          assertSuccess("nm_mtppaz2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtppaz2", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{ThirdParty: {PostalAddress: {PostalCode: '2192'}}}]
          }),
          assertNoRule("nm_mtppaz2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {PostalAddress: {PostalCode: '2192'}}}]
          }),

      // field: "ThirdParty.ContactDetails.ContactSurname",
          // Must not be completed
          assertSuccess("nm_mtpcds2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtpcds2", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{ThirdParty: {ContactDetails: {ContactSurname: 'ABC'}}}]
          }),
          assertNoRule("nm_mtpcds2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {ContactDetails: {ContactSurname: 'ABC'}}}]
          }),

      // field: "ThirdParty.ContactDetails.ContactName",
          // Must not be completed
          assertSuccess("nm_mtpcdn2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtpcdn2", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{ThirdParty: {ContactDetails: {ContactName: 'ABC'}}}]
          }),
          assertNoRule("nm_mtpcdn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {ContactDetails: {ContactName: 'ABC'}}}]
          }),

      // field: "ThirdParty.ContactDetails.Email",
          // May not be completed
          assertSuccess("nm_mtpcde2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtpcde2", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{ThirdParty: {ContactDetails: {Email: 'a@b.com'}}}]
          }),
          assertNoRule("nm_mtpcde2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {ContactDetails: {Email: 'a@b.com'}}}]
          }),

      // field: "ThirdParty.ContactDetails.Fax",
          // Must not be completed
          assertSuccess("nm_mtpcdf2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtpcdf2", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{ThirdParty: {ContactDetails: {Fax: '0115559999'}}}]
          }),
          assertNoRule("nm_mtpcdf2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {ContactDetails: {Fax: '0115559999'}}}]
          }),

      // field: "ThirdParty.ContactDetails.Telephone",
          // Must not be completed
          assertSuccess("nm_mtpcdt2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtpcdt2", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{ThirdParty: {ContactDetails: {Telephone: '0115559999'}}}]
          }),
          assertNoRule("nm_mtpcdt2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {ContactDetails: {Telephone: '0115559999'}}}]
          }),

      // field: "LoanTenor",
          // Must not be completed
          assertSuccess("nm_mlt3", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mlt3", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LoanTenor: 'ON DEMAND'}]}),
          assertNoRule("nm_mlt3", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{LoanTenor: 'ON DEMAND'}]}),

      // field: {{Regulator}}Auth.RulingsSection
          // Must not be completed
          assertSuccess("nm_mars1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CBAuth: {RulingsSection: ''}}]
          }),
          assertFailure("nm_mars1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CBAuth: {RulingsSection: 'rulingsSection'}}]
          }),

      // field: {{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber
          // Must not be completed
          assertSuccess("nm_maian2", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}]}),
          assertFailure("nm_maian2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CBAuth: {REInternalAuthNumber: 'XYZ'}}]
          }),
          assertNoRule("nm_maian2", {
            ReportingQualifier: 'NON REPORTABLE',
            MonetaryAmount: [{CBAuth: {REInternalAuthNumber: 'XYZ'}}]
          }),

      // field: {{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate
          // Must not be completed
          assertSuccess("nm_maiad2", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}]}),
          assertFailure("nm_maiad2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CBAuth: {REInternalAuthNumberDate: "2013-01-01"}}]
          }),
          assertNoRule("nm_maiad2", {
            ReportingQualifier: 'NON REPORTABLE',
            MonetaryAmount: [{CBAuth: {REInternalAuthNumberDate: "2013-01-01"}}]
          }),

      // field: {{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber
          // Must not be completed
          assertSuccess("nm_masan3", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}]}),
          assertFailure("nm_masan3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CBAuth: {CBAuthAppNumber: 'XYZ'}}]
          }),
          assertNoRule("nm_masan3", {
            ReportingQualifier: 'NON REPORTABLE',
            MonetaryAmount: [{CBAuth: {CBAuthAppNumber: 'XYZ'}}]
          }),

      // field: {{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber
          // Must not be complete
          assertSuccess("nm_masar3", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}]}),
          assertFailure("nm_masar3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CBAuth: {CBAuthRefNumber: 'ABC'}}]
          }),
          assertNoRule("nm_masar3", {
            ReportingQualifier: 'NON REPORTABLE',
            MonetaryAmount: [{CBAuth: {CBAuthRefNumber: 'ABC'}}]
          }),

      // field: AdHocRequirement.Subject
          // Must not be completed
          assertSuccess("nm_madhs2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_madhs2", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{AdHocRequirement: {Subject: 'INVALIDIDNUMBER'}}]
          }),
          assertNoRule("nm_madhs2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{AdHocRequirement: {Subject: 'INVALIDIDNUMBER'}}]
          }),
          
      // field: AdHocRequirement.Description
          // Must not be completed
          assertSuccess("nm_madhd3", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_madhd3", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{AdHocRequirement: {Description: 'NONE'}}]
          }),
          assertNoRule("nm_madhd3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{AdHocRequirement: {Description: 'NONE'}}]
          }),
          //If Subject is completed, Description may not be empty (LIBRA-2012)
          assertSuccess("madhd1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{AdHocRequirement: {Description: 'NONE', Subject: 'SOMETHING'}}]
          }),
          assertFailure("madhd1", {
            ReportingQualifier: 'BOPCUS', 
            MonetaryAmount: [{AdHocRequirement: {Subject: 'SOMETHING'}}]
          }),
          assertNoRule("madhd1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{AdHocRequirement: {Description: 'NONE'}}]
          }),

      // field: "ThirdParty.CustomsClientNumber",
          // Invalid CustomsClientNumber
          assertSuccess("nm_mtpccn1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '12345'}}]
          }),
          assertSuccess("nm_mtpccn1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '111111111111111'}}] //15 Digits
          }),
          assertFailure("nm_mtpccn1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '1111111111111111'}}] //16 Digits
          }),
          assertFailure("nm_mtpccn1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: 'a12345'}}]
          }),
          assertNoRule("nm_mtpccn1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: ''}}]
          }),

          // If it contains a value, the CustomsClientNumber must contain a valid Customs Client number
          assertSuccess("nm_mtpccn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '12345'}}]
          }),
          assertSuccess("nm_mtpccn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '111111111111111'}}] //15 Digits
          }),
          assertFailure("nm_mtpccn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '1111111111111111'}}] //16 Digits
          }),
          assertFailure("nm_mtpccn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: 'a12345'}}]
          }),
          assertNoRule("nm_mtpccn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: ''}}]
          }),
          // This rule is only for SA, Namibia does not need 70707070 functionality
          // assertSuccess('nm_mtpccn2b:1', {
          //   ReportingQualifier: 'BOPCUS',
          //   MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '12345678'}}]
          // }),
          // assertNoRule('nm_mtpccn2b:2', {
          //   ReportingQualifier: 'BOPCUS',
          //   MonetaryAmount: [{ThirdParty: {CustomsClientNumber: ''}}]
          // }),
          // assertFailure('nm_mtpccn2b:3', {
          //   ReportingQualifier: 'BOPCUS',
          //   MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '70707070'}}]
          // }),


          // Must not be completed
          assertSuccess("nm_mtpccn3", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtpccn3", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '123'}}]
          }),
          assertNoRule("nm_mtpccn3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '123'}}]
          }),

      // field: "ThirdParty.TaxNumber",
          // Must not be completed
           assertSuccess("nm_mtptx2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
           assertFailure("nm_mtptx2", {
             ReportingQualifier: 'BOPCARD RESIDENT',
             MonetaryAmount: [{ThirdParty: {TaxNumber: '123'}}]
           }),
           assertNoRule("nm_mtptx2", {
             ReportingQualifier: 'BOPCUS',
             MonetaryAmount: [{ThirdParty: {TaxNumber: '123'}}]
           }),

      // field: "ThirdParty.VATNumber",
          // Must not be completed
          assertSuccess("nm_mtpvn1", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtpvn1", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{ThirdParty: {VATNumber: '123'}}]
          }),
          assertNoRule("nm_mtpvn1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {VATNumber: '123'}}]
          }),

      // field: "SWIFTDetails",
          // Must not be completed
          assertSuccess("nm_mswd1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            MonetaryAmount: [{SWIFTDetails: ''}]
          }),
          assertSuccess("nm_mswd1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            MonetaryAmount: [{SWIFTDetails: ''}]
          }),
          assertFailure("nm_mswd1", {
            ReportingQualifier: "BOPCUS",
            MonetaryAmount: [{SWIFTDetails: '12'}]
          }),
          assertNoRule("nm_mswd1", {
            ReportingQualifier: "NON REPORTABLE",
            MonetaryAmount: [{SWIFTDetails: '12'}]
          }),

      // field: "ElectronicCommerceIndicator",
          // Must not be completed
          assertSuccess("nm_mcrdec2", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ElectronicCommerceIndicator: ""}]}),
          assertFailure("nm_mcrdec2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ElectronicCommerceIndicator: "Foo"}]
          }),
          assertNoRule("nm_mcrdec2", {
            ReportingQualifier: 'NON REPORTABLE',
            MonetaryAmount: [{ElectronicCommerceIndicator: "Foo"}]
          }),

      // field: "POSEntryMode",
          // Must not be completed
          assertSuccess("nm_mcrdem1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{POSEntryMode: ""}]
          }),
          assertFailure("nm_mcrdem1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{POSEntryMode: "Foo"}]
          }),
          assertNoRule("nm_mcrdem1", {
            ReportingQualifier: 'NON REPORTABLE',
            MonetaryAmount: [{POSEntryMode: "Foo"}]
          }),

      // field: "CardFraudulentTransactionIndicator",
          // Must not be completed
          assertSuccess("nm_mcrdft1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CardFraudulentTransactionIndicator: ''}]
          }),
          assertFailure("nm_mcrdft1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CardFraudulentTransactionIndicator: 'ABC'}]
          }),
          assertNoRule("nm_mcrdft1", {
            ReportingQualifier: 'NON REPORTABLE',
            MonetaryAmount: [{CardFraudulentTransactionIndicator: 'ABC'}]
          }),

      // field: "ImportExport",
          // ImportExportData Element must not be completed
          assertSuccess("nm_mtie2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("nm_mtie2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{ImportExport: [{}]}]}),
          assertNoRule("nm_mtie2", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ImportExport: [{}]}]}),

      // field: "ReversalTrnRefNumber",
          // Additional spaces identified in data content
          assertSuccess("nm_mrtrn4", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ReversalTrnRefNumber: 'XYZ'}]}),
          assertFailure("nm_mrtrn4", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ReversalTrnRefNumber: ' XYZ'}]}),
          assertFailure("nm_mrtrn4", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ReversalTrnRefNumber: 'X YZ'}]}),
          assertNoRule("nm_mrtrn4", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{ReversalTrnRefNumber: ' XYZ'}]}),

      // field: "MoneyTransferAgentIndicator",
          // If the MoneyTransferAgentIndicator is TRAVEL CARD or TRAVELLERS CHEQUE, the category can only be 252, 255, 256 or 530/05
          assertFailure("nm_mtai1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '401',
              MoneyTransferAgentIndicator: "TRAVEL CARD"
            }]
            }),
            assertSuccess("nm_mtai1", {
              ReportingQualifier: 'BOPCUS',
              MonetaryAmount: [{
                CategoryCode: '401',
                MoneyTransferAgentIndicator: "CARD"
              }]
            }),
            assertNoRule("nm_mtai1", {
              ReportingQualifier: 'BOPCUS',
              MonetaryAmount: [{
                CategoryCode: '255',
                MoneyTransferAgentIndicator: "TRAVEL CARD"
              }]
            }),

//================================================================================================================================================
//  # IMPORT/EXPORT (4/6)
//================================================================================================================================================
      // field: "ImportControlNumber",
          //Must be completed if the Flow is OUT and the BoPCategory is 10101 to 10110 or 10301 to 10310 or 10500 or 10600.
          // assertSuccess("nm_ieicn1", {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          //   MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '04', ImportExport: [{ImportControlNumber: 'XYZ'}]}]
          // }),
          // assertFailure("nm_ieicn1", {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          //   MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '04', ImportExport: [{ImportControlNumber: ''}]}]
          // }),

          // // Additional spaces identified in data content (May also not contain a comma (,))
          // assertFailure("nm_ieicn5", {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          //   MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: 'Test Space'}]}]
          // }),
          // assertFailure("nm_ieicn5", {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          //   MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: 'Test,Comma'}]}]
          // }),
          // assertSuccess("nm_ieicn5", {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          //   MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: 'TestBoth'}]}]
          // }),

      // field: ["Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province", 
      // "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
          // Must be a valid province
          assertSuccess('nm_p1:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {StreetAddress: {Province: 'CAPRIVI'}}}
          }),
          assertFailure('nm_p1:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {StreetAddress: {Province: 'INVALIDREGION'}}}
          }),
          assertNoRule('nm_p1:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {StreetAddress: {Province: ''}}}
          }),
          assertSuccess('nm_p1:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {StreetAddress: {Province: 'CAPRIVI'}}}
          }),
          assertFailure('nm_p1:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {StreetAddress: {Province: 'INVALIDREGION'}}}
          }),
          assertNoRule('nm_p1:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {StreetAddress: {Province: ''}}}
          }),
          assertSuccess('nm_p1:3', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {PostalAddress: {Province: 'CAPRIVI'}}}
          }),
          assertFailure('nm_p1:3', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {PostalAddress: {Province: 'INVALIDREGION'}}}
          }),
          assertNoRule('nm_p1:3', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {PostalAddress: {Province: ''}}}
          }),
          assertSuccess('nm_p1:4', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {PostalAddress: {Province: 'CAPRIVI'}}}
          }),
          assertFailure('nm_p1:4', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {PostalAddress: {Province: 'INVALIDREGION'}}}
          }),
          assertNoRule('nm_p1:4', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {PostalAddress: {Province: ''}}}
          }),

          // Region must be completed
          assertSuccess('nm_p2:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {StreetAddress: {Province: 'CHAPRIVO'}}}
          }),
          assertFailure('nm_p2:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {StreetAddress: {Province: ''}}}
          }),

          // Resident.Entity.StreetAddress.Province
          assertSuccess('nm_p2:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {StreetAddress: {Province: 'CAPRIVI'}}}
          }),
          assertFailure('nm_p2:2', {
             ReportingQualifier: 'BOPCUS',
             Resident: {Entity: {StreetAddress: {Province: ''}}}}),
            
      // field: "MRNNotOnIVS",
          // The value must only be Y or N
          // assertSuccess("nm_iemrn1", {
          //   ReportingQualifier: 'BOPCUS',
          //   MonetaryAmount: [{ImportExport: [{MRNNotOnIVS: 'Y'}]}]
          // }),
          // assertSuccess("nm_iemrn1", {
          //   ReportingQualifier: 'BOPCUS',
          //   MonetaryAmount: [{ImportExport: [{MRNNotOnIVS: 'N'}]}]
          // }),
          // assertFailure("nm_iemrn1", {
          //   ReportingQualifier: 'BOPCUS',
          //   MonetaryAmount: [{ImportExport: [{MRNNotOnIVS: 'M'}]}]
          // }),
          // assertSuccess("nm_iemrn1", {
          //   ReportingQualifier: 'BOPCUS',
          //   MonetaryAmount: [{ImportExport: [{MRNNotOnIVS: ''}]}]
          // }),  
          
          // field: "TransportDocumentNumber",
          // Must be completed if the Flow is OUT and the BoPCategory is 10301 to 10310.
          // assertSuccess("nm_ietdn1", {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          //   MonetaryAmount: [{
          //     CategoryCode: '103',
          //     CategorySubCode: '04',
          //     ImportExport: [{TransportDocumentNumber: 'TransportDocumentNumber'}]
          //   }]
          // }),
          // assertFailure("nm_ietdn1", {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          //   MonetaryAmount: [{
          //     CategoryCode: '103',
          //     CategorySubCode: '04',
          //     ImportExport: [{TransportDocumentNumber: ''}]
          //   }]
          // }),
          // assertNoRule("nm_ietdn1", {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          //   MonetaryAmount: [{
          //     CategoryCode: '103',
          //     CategorySubCode: '11',
          //     ImportExport: [{TransportDocumentNumber: ''}]
          //   }]
          // }),
          
          // field: "UCR",
          // If UCR contains a value and the Flow is IN, the minimum characters is 2 but not exceeding 35 characters and is in the following format: nNA12345678a...35a where n = last digit of the year NA = Fixed character 12345678 = Valid Customs Client Numbera = unique alpha numeric consignment number
          // assertSuccess("nm_ieucr1", {
          //   ReportingQualifier: 'BOPCUS',
          //   Flow: 'IN',
          //   MonetaryAmount: [{ImportExport: [{UCR: '2NA001234560500010596'}]}]
          // }),
          // assertFailure("nm_ieucr1", {
          //   ReportingQualifier: 'BOPCUS',
          //   Flow: 'IN',
          //   MonetaryAmount: [{ImportExport: [{UCR: '2ZA001234560500010596'}]}]
          // }),
          // assertNoRule("nm_ieucr1", {
          //   ReportingQualifier: 'BOPCUS',
          //   Flow: 'OUT',
          //   MonetaryAmount: [{ImportExport: [{UCR: '2ZA001234560500010596'}]}]
          // }),

//================================================================================================================================================
//  # NOT FROM THIS CHANNEL (5/6)
//================================================================================================================================================
      // TotalForeignValue
          assertSuccess("tfv1", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'NAD', TotalForeignValue: '101.01',
            MonetaryAmount: [{DomesticValue: '50.00'}, {DomesticValue: '51.01'}]
          }),
          assertFailure("tfv1", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'NAD', TotalForeignValue: '101.01',
            MonetaryAmount: [{DomesticValue: '50.00'}, {DomesticValue: '49.99'}]
          }),
          assertSuccess("tfv1", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'NAD',
            MonetaryAmount: [{DomesticValue: '50.00'}, {DomesticValue: '49.99'}]
          }),

          assertSuccess("tfv2", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD', TotalForeignValue: '101.01',
            MonetaryAmount: [{ForeignValue: '50.00'}, {ForeignValue: '51.01'}]
          }),
          assertFailure("tfv2", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD', TotalForeignValue: '101.01',
            MonetaryAmount: [{ForeignValue: '50.00'}, {ForeignValue: '49.99'}]
          }),
          assertSuccess("tfv2", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD',
            MonetaryAmount: [{ForeignValue: '50.00'}, {ForeignValue: '49.99'}]
          }),

          // assertSuccess("tfv3", {
          //   ReportingQualifier: 'BOPCARD NON RESIDENT', FlowCurrency: 'NAD', TotalForeignValue: '101.01',
          //   MonetaryAmount: [
          //     {ForeignCardHoldersPurchasesDomesticValue: '40.00', ForeignCardHoldersCashWithdrawalsDomesticValue: '10.00'},
          //     {ForeignCardHoldersCashWithdrawalsDomesticValue: '51.01'}
          //   ]
          // }),
          // assertFailure("tfv3", {
          //   ReportingQualifier: 'BOPCARD NON RESIDENT', FlowCurrency: 'NAD', TotalForeignValue: '101.01',
          //   MonetaryAmount: [{ForeignCardHoldersPurchasesDomesticValue: '50.00'}, {ForeignCardHoldersPurchasesDomesticValue: '49.99'}]
          // }),
          // assertSuccess("tfv3", {
          //   ReportingQualifier: 'BOPCARD NON RESIDENT', FlowCurrency: 'NAD',
          //   MonetaryAmount: [{ForeignCardHoldersPurchasesDomesticValue: '50.00'}, {ForeignCardHoldersPurchasesDomesticValue: '49.99'}]
          // }),
          //The TotalForeignValue must be completed and must be greater than 0.00
          // assertSuccess("tfv4", {
          //   ReportingQualifier: 'BOPCUS', TotalForeignValue: '101.01'
          // }),
          // assertFailure("tfv4", {
          //   ReportingQualifier: 'BOPCUS', TotalForeignValue: '0.00'
          // }),
          // assertFailure("tfv4", {
          //   ReportingQualifier: 'BOPCUS'
          // }),
          // assertNoRule("tfv4", {
          //   ReportingQualifier: 'BOPCARD NON RESIDENT', TotalForeignValue: '0.00'
          // }),

          
//================================================================================================================================================
//  # Removed (6/6)
//================================================================================================================================================
          //NonResident.Entity.CardMerchantName
          // assertSuccess("b_nrle1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: {CardMerchantName: "My Card"}}}),
          // assertSuccess("b_nrle1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: {CardMerchantName: {}}}}),
          // assertFailure("b_nrle1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: {}}}),

          // //NonResident.Entity.LegalEntityName
          // assertSuccess("b_nrlen1", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {EntityName: 'Synthesis'}}}),
          // assertNoRule("b_nrlen1", {ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: {Entity: {EntityName: 'Synthesis'}}}),
          // assertSuccess("b_nrlen1", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {LegalName: {}}}}),
          // assertFailure("b_nrlen1", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {LegalName: "Test"}}}),

          // assertSuccess("b_nrlen1", {ReportingQualifier: "BOPCUS", Resident: {Entity: {ExceptionName: {}}}}),
          // assertFailure("b_nrlen1", {ReportingQualifier: "BOPCUS", Resident: {Entity: {ExceptionName: ""}}}),

          // Additional.NonResidentData.Country
          // assertSuccess("nm_nrictry1",{ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: {Individual: {Address: {}}}}),
          // assertFailure("nm_nrictry1",{ReportingQualifier: "BOPCARD NON RESIDENT", NonResident:{Individual: {Address:{Country: "South Africa"}}}}),

          //IndividualCustomer.IDNumber
          // assertFailure("nm_icid1",{ReportingQualifier: "BOPCUS", Resident: {Individual:{}}}),
          // assertFailure("nm_icid1",{ReportingQualifier: "BOPCUS", Resident: {Individual:{IDNumber: {}}}}),
          // assertSuccess("nm_icid1",{ReportingQualifier: "BOPCUS", Resident: {Individual:{IDNumber: "9202265116080"}}}),

          // assertFailure("nm_icid1",{ReportingQualifier: "BOPCARD RESIDENT", Resident: {Individual:{}}}),
          // assertFailure("nm_icid1",{ReportingQualifier: "BOPCARD RESIDENT", Resident: {Individual:{IDNumber: {}}}}),
          // assertSuccess("nm_icid1",{ReportingQualifier: "BOPCARD RESIDENT", Resident: {Individual:{IDNumber: "9202265116080"}}}),

          //AdditionalCustomerData.CustomsClientNumber
          // assertSuccess('b_ccn1:1', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: '1234'}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '101', CategorySubCode: '10'}]
          // }),
          // assertFailure('b_ccn1:1', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: ''}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '103', CategorySubCode: '10'}
          //   ]
          // }),
          // assertFailure('b_ccn1:1', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '103', CategorySubCode: '10'}
          //   ]
          // }),
          // assertNoRule('b_ccn1:1', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {CustomsClientNumber: '1234'}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'}
          //   ]
          // }),

          // assertSuccess('b_ccn1:2', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: '1234'}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '101', CategorySubCode: '10'}]
          // }),
          // assertFailure('b_ccn1:2', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: ''}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '103', CategorySubCode: '10'}
          //   ]
          // }),
          // assertFailure('b_ccn1:2', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '103', CategorySubCode: '10'}
          //   ]
          // }),
          // assertNoRule('b_ccn1:2', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {CustomsClientNumber: '1234'}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'}
          //   ]
          // }),
           // //Must be completed
          // assertFailure('nm_ncc1', {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   MonetaryAmount: [{LocalCurrency: 0.00}]
          // }),
          // assertFailure('nm_ncc1', {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   MonetaryAmount: [{}]
          // }),
          // assertFailure('nm_ncc1', {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   MonetaryAmount: [{LocalCurrency: {}}]
          // }),
          // assertFailure('nm_ncc1', {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   MonetaryAmount: [{LocalCurrency: ''}]
          // }),
          // Must not be completed.
          // assertSuccess("nm_ncc5", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LocalCurrency: ''}]}),
          // assertSuccess("nm_ncc5", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}]}),
          // assertFailure("nm_ncc5", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LocalCurrency: 'NAD'}]}),
        ]   
      }
    return testBase;
  }
})
