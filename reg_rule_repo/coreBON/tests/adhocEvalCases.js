define(function() {
  return function(testBase) {
    var test_cases;
    with (testBase) {
      test_cases = [
        assertAssumption("CUS Rec RESIDENT LOCAL_ACC receiving NAD from ABSANAJJX",
         "ABSANAJJX", null, null, "NAD", null, "SBNMNAXX", resStatus.RESIDENT, accType.LOCAL_ACC, null,
          { decision: dec.DoNotReport }),
      ];
    }
    return testBase;
  }
})