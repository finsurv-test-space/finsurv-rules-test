define(function () {
  return function (predef) {
    var filterLookupRules;
    with (predef) {
      filterLookupRules = {
        filterLookupTrans: {
          ruleset: "Reporting Transaction Lookup Filter Rules",
          scope: "transaction",
          fields: [
            {
              field: "OriginatingCountry",
              display: [
                // limitValue([getMap("Locale")]).onOutflow().onSection("ABG"),
                // excludeValue([getMap("Locale")]).onInflow().onSection("ABG")
              ]
            },
            {
              field: "ReceivingCountry",
              display: [
                // limitValue([getMap("Locale")]).onInflow().onSection("ABG"),
                // excludeValue([getMap("Locale")]).onOutflow().onSection("ABG")
              ]
            },
            {
              field: "LocationCountry",
              display: [
                // excludeValue([getMap("Locale")], not(hasTransactionFieldValue("NonResident.Exception.ExceptionName", "IHQ"))).onSection("ABG"),
                // excludeValue(['EU']).onSection("ABG").notOnCategory("513")
              ]
            },
            {
              field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
              display: [
                excludeValue(["VISA NET", "MASTER SEND"])
              ]
            },
            {
              field: ["NonResident.Individual.Address.Country", "NonResident.Entity.Address.Country"],
              display: [
                excludeValue([getMap("Locale")]).onInflow().onSection("ABCDG"),
                excludeValue([getMap("Locale")], notTransactionFieldValue("Resident.Individual.ForeignIDCountry", ["ZA", "LS", "SZ"])).onSection("E"),
                excludeValue(["EU"]).onOutflow().onSection("A").notOnCategory("513"),
                excludeValue(["EU"]).onInflow().onSection("A").notOnCategory("517")
              ]
            },
            {
              field: ["Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province", "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
              display: [
                //override...
                         ]
            },
            // below rules are causing issues for eval enrichment...
            {
             field  : "NonResident.Exception.ExceptionName",
             display: [
              limitValue(["MUTUAL PARTY"]).onSection("A").onCategory(["252", "255", "256", "530/05"]),
              //  excludeValue(["MUTUAL PARTY"]).onSection("A").notOnCategory(["200", "252", "255", "256", "530/05"]),
              //  excludeValue(["BULK INTEREST"]).onSection("A").notOnCategory(["300", "309/08"]),
              //  excludeValue(["BULK VAT REFUNDS"]).onSection("A").notOnCategory(["400", "411/02"]),
              //  excludeValue(["BULK BANK CHARGES"]).onSection("A").notOnCategory(["200", "275"]),
              //  excludeValue(["BULK PENSIONS"]).onSection("A").notOnCategory(["400", "407"]),
              //   excludeValue(["MUTUAL PARTY"]).onCategory(["200", "252", "255", "256", "530/05"]),
             ]
            },
            {
              field: "Resident.Exception.ExceptionName",
              display: [
                limitValue(["MUTUAL PARTY"]).onSection("A").onCategory(["250", "251"]),
                // excludeValue(["MUTUAL PARTY"]).onSection("A").notOnCategory(["200","250", "251"]),
                // excludeValue(["BULK PENSIONS"]).onSection("A").notOnCategory(["400", "407"]),
                // excludeValue(["BULK INTEREST"]).onSection("A").notOnCategory(["309/08", "300"]),
                // excludeValue(["BULK DIVIDENDS"]).onSection("A").notOnCategory(["301", "300"]),
                // excludeValue(["BULK BANK CHARGES"]).onSection("A").notOnCategory(["275", "200"]),
              ]
            },
          ]
        },

        filterLookupMoney: {
          ruleset: "Reporting Monetary Lookup Filter Rules",
          scope: "money",
          fields: [
            {
               field  : "CompoundCategoryCode",
               display: [
                   excludeValue(['ZZ2']).onSection("AEF"),
                   excludeValue(['ZZ1']).onSection("EF")
               ]
              }
          ]
        },

        filterLookupImportExport: {
          ruleset: "Reporting Import Export Lookup Filter Rules",
          scope: "importexport",
          fields: []
        }
      }
    }

    return filterLookupRules;
  }
});


