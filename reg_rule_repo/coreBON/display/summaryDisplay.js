define(function () {
  return function (predef) {

    var summaryTrans, summaryMoney, summaryImportExport;
    with (predef) {
      summaryTrans = {
        ruleset: "Summary StdBank Transaction Display Rules",
        scope: "transaction",
        fields: []
      };

      summaryMoney = {
        ruleset: "Summary StdBank Money Display Rules",
        scope: "money",
        fields: [
          {
            field: "Description",
            displayOnly: true,
            display: [
              appendValue("%s", categoryDescription)
            ]
          },
          {
            field: "Category",
            display: [
              setValue(),
              appendValue("%s", function (context) {
                var cat = context.lookups.getCategory(context.flow,
                  context.getMoneyField(context.currentMoneyInstance, 'CategoryCode'),
                  context.getMoneyField(context.currentMoneyInstance, 'CategorySubCode'));
                if (cat)
                  return cat.code;
                return "";
              })
            ]
          },
          {
            field: "CategoryCode",
            display: [
              setValue('ZZ1', null, isEmpty).onSection("C")
            ]
          },
          {
            field: "IE_PaymentValueHeading",
            displayOnly: true,
            display: [
              setLabel("Payment Value")
            ]
          },
        ]
      };

      summaryImportExport = {
        ruleset: "Summary StdBank Import Export Display Rules",
        scope: "importexport",
        fields: []
      };


    }

    return {
      summaryTrans: summaryTrans,
      summaryMoney: summaryMoney,
      summaryImportExport: summaryImportExport
    }
  }
})
;
