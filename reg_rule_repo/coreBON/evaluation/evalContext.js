define(function () {
  return function (evaluationEx) {
    return {
      localCountryRegex: "[Nn][Aa]",
      localCurrencyRegex: "[Nn][Aa][Dd]",
      whoAmIRegex: "SBNMNA.*",
      onshoreRegex: "....NA.*",
      onshoreADRegex: [
        "ABSANAJJ.*", // ABSA BANK
        "ALBRNAJJ.*", // ALBARAKA BANK
        "BKCHNAJJ.*", // BANK OF CHINA
        "BIDBNAJJ.*", // BIDVEST
        "BNPANAJJ.*", // BNP PARIBAS JHB
        "CABLNAJJ.*", // CAPITEC
        "CITINAJX.*", // CITIBANK JHB
        "FIRNNAJJ.*", // FIRSTRAND BANK
        "HOBLNAJJ.*", // HABIB OVERSEAS BANK JHB
        "HBZHNAJJ.*", // HBZ BANK
        "IVESNAJJ.*", // INVESTEC
        "MGTCNAJJ.*", // JP MORGAN JHB
        "LISANAJJ.*", // MERCANTILE BANK
        "NEDSNAJJ.*", // NEDBANK
        "SASFNAJJ.*", // SASFIN
        "SOGENAJJ.*", // SOCIETY GENERAL JHB
        "SCBLNAJJ.*", // STANCHART BANK JHB
        "SBINNAJJ.*", // STATE BANK OF INDIA
        "BATHNAJJ.*", // BANK OF ATHENS
        "SBSANAJJ.*"], // STANDARD BANK
      CMARegex: [
        "....ZA.*",  // South Africa
        "....SZ.*",  // Swaziland
        "....LS.*"], // Lesotho
      ZZ1Reportability : false
    };
  }
})
