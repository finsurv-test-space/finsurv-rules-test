define(function () {
  return function (evaluationEx) {
    var scenarios;
    with (evaluationEx) {
      scenarios = [
        {
          scenario: "CustomerPayment Under CMA Limit",
          //knownSide: drcr.DR,
          //resStatus: resStatus.RESIDENT,
          //accType: [accType.LOCAL_ACC, accType.CASH_ZAR, accType.CASH_CURR],
          match: drThisBank.and(not(crThisBank)).and(crCMABank).and(hasValue("DomesticAmount", "<", "5000000")).and(hasValue("DomesticAmount", ">", "0")),
          rules: [
            decide( {
                       manualSection: "Local Bank to CMA bank under CMA limit",
                       reportable: rep.NONREPORTABLE
                    })
          ]
        },
        {
          scenario: "CustomerReceipt Under CMA Limit",
          //knownSide: drcr.DR,
          //resStatus: resStatus.RESIDENT,
          //accType: [accType.LOCAL_ACC, accType.CASH_ZAR, accType.CASH_CURR],
          match: crThisBank.and(not(drThisBank)).and(drCMABank).and(hasValue("DomesticAmount", "<", "5000000")).and(hasValue("DomesticAmount", ">", "0")),
          rules: [
            decide( {
                       manualSection: "CMA bank to Local Bank under CMA limit",
                       reportable: rep.NONREPORTABLE
                    })
          ]
        },
        {
          scenario: "BankPayment",
          match: drThisBank.and(not(crThisBank)).and(drNoResStatus).and(drHasAccType([accType.NOSTRO])),
          rules: [
            decideCr( {
                         manualSection: "All interbank transactions made to foreign banks or foreign currency transactions are reportable",
                         reportable: rep.ZZ1REPORTABLE,
                         flow: flowDir.OUT,
                         reportingSide: drcr.DR,
                         resSide: drcr.DR,
                         resAccountType: at.RE_OTH,
                         nonResSide: drcr.CR,
                         nonResAccountType: at.NR_OTH
                       },
                       offshoreBank.or(transferCURR)),
            decideCr( {
                         manualSection: "Banking payments made to local banks in local currencies are not reportable",
                         reportable: rep.NONREPORTABLE
                       },
                       localBank.and(transferLOCAL))
          ]
        },
        {
          scenario: "BankReceipt",
          match: crThisBank.and(not(drThisBank)).and(crNoResStatus).and(crHasAccType([accType.NOSTRO])),
          rules: [
            decideDr( {
                         manualSection: "All interbank transactions received from foreign banks or in foreign currency are reportable",
                         reportable: rep.ZZ1REPORTABLE,
                         flow: flowDir.IN,
                         reportingSide: drcr.CR,
                         resSide: drcr.CR,
                         resAccountType: at.RE_OTH,
                         nonResSide: drcr.DR,
                         nonResAccountType: at.NR_OTH
                       },
                       offshoreBank.or(transferCURR)),
            decideDr( {
                         manualSection: "Banking payments received from local banks in local currencies are not reportable",
                         reportable: rep.NONREPORTABLE
                       },
                       localBank.and(transferLOCAL))
          ]
        },
        {
          scenario: "InwardIntermediaryBank", // Overseas Bank to other Bank (via us)
          match: not(drThisBank).and(not(crThisBank)).
                   and(not(drLocalBank)).
                   and(drHasAccType([accType.NOSTRO, accType.VOSTRO])).
                   and(crHasAccType([accType.NOSTRO, accType.VOSTRO])),
          rules: [
            decideCr( {
                         manualSection: "Offshore Bank to Local Bank (via us). Instruct beneficiary bank to report appropriately based on type of customer in field 72",
                         reportable: rep.ZZ1REPORTABLE,
                         flow: flowDir.IN,
                         reportingSide: drcr.DR,
                         resSide: drcr.CR,
                         resAccountType: at.RE_OTH,
                         nonResSide: drcr.DR,
                         nonResAccountType: at.NR_OTH
                       },
                       localBank),
            decideCr( {
                         manualSection: "Offshore Bank to Offshore Bank (via us). Nothing to report",
                         reportable: rep.NONREPORTABLE
                       },
                       offshoreBank),
          ]
        },
        {
          scenario: "OutwardIntermediaryBank", // Local Bank to any other Bank  (via us)
          match: not(drThisBank).and(not(crThisBank)).
                   and(drLocalBank).
                   and(drHasAccType([accType.NOSTRO, accType.VOSTRO])).
                   and(crHasAccType([accType.NOSTRO, accType.VOSTRO])),
          rules: [
            decideCr( {
                         manualSection: "Local Bank to offshore. Ordering institution is required to do any of the reporting",
                         reportable: rep.NONREPORTABLE
                       },
                       offshoreBank),
            decideCr( {
                         manualSection: "Local Bank to local Bank requires no reporting",
                         reportable: rep.NONREPORTABLE
                       },
                       localBank)
          ]
        },
        {
          scenario: "CustomerOnUs",
          match: drThisBank.and(crThisBank).
                   and(drHasResStatus([resStatus.RESIDENT, resStatus.NONRES, resStatus.HOLDCO, resStatus.IHQ])).
                   and(crHasResStatus([resStatus.RESIDENT, resStatus.NONRES, resStatus.HOLDCO, resStatus.IHQ])),
          rules: [
            useBoth()
          ]
        },
        {
          scenario: "CustomerPayment",
          //knownSide: drcr.DR,
          //resStatus: resStatus.RESIDENT,
          //accType: [accType.LOCAL_ACC, accType.CASH_ZAR, accType.CASH_CURR],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.RESIDENT)).
                   and(drHasAccType([accType.LOCAL_ACC, accType.CASH_LOCAL, accType.CASH_CURR])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.NONRES, accType.FCA, localBank.and(transferCURR).and(hasResStatus(resStatus.NONRES))),
            useCr(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR).and(not(hasResStatus(resStatus.NONRES)))),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.RESIDENT,
//          accType: [accType.CFC],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.RESIDENT)).
                   and(drHasAccType([accType.CFC])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.NONRES, accType.FCA, localBank.and(transferCURR).and(hasResStatus(resStatus.NONRES))),
            useCr(resStatus.RESIDENT, accType.CFC, localBank.and(transferCURR).and(not(hasResStatus(resStatus.NONRES)))),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.RESIDENT,
//          accType: [accType.NOSTRO],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.RESIDENT)).
                   and(drHasAccType([accType.NOSTRO])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.RESIDENT,
//          accType: [accType.FCA],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.RESIDENT)).
                   and(drHasAccType([accType.FCA])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.HOLDCO, accType.FCA, entity.and(localBank.and(transferCURR))),
            useCr(resStatus.RESIDENT, accType.FCA, individual.and(localBank.and(transferCURR))),
            useCr(resStatus.NONRES, accType.FCA, localBank.and(transferCURR).and(hasResStatus(resStatus.NONRES))),
            useCr(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR).and(not(hasResStatus(resStatus.NONRES)))),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.RESIDENT,
//          accType: [accType.VOSTRO],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.RESIDENT)).
                   and(drHasAccType([accType.VOSTRO])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.RESIDENT,
//          accType: [accType.CASH_CURR, accType.CASH_LOCAL],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.RESIDENT)).
                   and(drHasAccType([accType.CASH_CURR, accType.CASH_LOCAL])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.IHQ,
//          accType: [accType.LOCAL_ACC, accType.CASH_ZAR, accType.CASH_CURR],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.IHQ)).
                   and(drHasAccType([accType.LOCAL_ACC, accType.CASH_ZAR, accType.CASH_CURR])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.IHQ,
//          accType: [accType.CFC],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.IHQ)).
                   and(drHasAccType([accType.CFC])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.RESIDENT, accType.CFC, localBank.and(transferCURR)),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.IHQ,
//          accType: [accType.FCA],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.IHQ)).
                   and(drHasAccType([accType.FCA])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.HOLDCO, accType.FCA, entity.and(localBank.and(transferCURR))),
            useCr(resStatus.RESIDENT, accType.FCA, individual.and(localBank.and(transferCURR))),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.NONRES,
//          accType: [accType.LOCAL_ACC],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.NONRES)).
                   and(drHasAccType([accType.LOCAL_ACC])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.NONRES,
//          accType: [accType.CFC, accType.FCA],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.NONRES)).
                   and(drHasAccType([accType.CFC, accType.FCA])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.NONRES,
//          accType: [accType.NOSTRO],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.NONRES)).
                   and(drHasAccType([accType.NOSTRO])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.NONRES,
//          accType: [accType.VOSTRO],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.NONRES)).
                   and(drHasAccType([accType.VOSTRO])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.NONRES,
//          accType: [accType.CASH_LOCAL, accType.CASH_CURR],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.NONRES)).
                   and(drHasAccType([accType.CASH_LOCAL, accType.CASH_CURR])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.HOLDCO,
//          accType: [accType.LOCAL_ACC],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.HOLDCO)).
                   and(drHasAccType([accType.LOCAL_ACC])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.HOLDCO,
//          accType: [accType.FCA],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.HOLDCO)).
                   and(drHasAccType([accType.FCA])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerPayment",
//          knownSide: drcr.DR,
//          resStatus: resStatus.HOLDCO,
//          accType: [accType.VOSTRO],
          match: drThisBank.and(not(crThisBank)).
                   and(drHasResStatus(resStatus.HOLDCO)).
                   and(drHasAccType([accType.VOSTRO])),
          rules: [
            useCr(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            useCr(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            useCr(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          scenario: "CustomerReceipt",
//          knownSide: drcr.CR,
//          resStatus: resStatus.RESIDENT,
//          accType: [accType.LOCAL_ACC],
          match: crThisBank.and(not(drThisBank)).
                   and(crHasResStatus(resStatus.RESIDENT)).
                   and(crHasAccType([accType.LOCAL_ACC])),
          rules: [
            useDr(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            useDr(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            useDr(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB").and(field72("NTNRC"))))),
            useDr(resStatus.NONRES, accType.FCA, localBankAD.and(transferLOCAL).and(field72("NTNRC"))),
            useDr(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            useDr(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          scenario: "CustomerReceipt",
//          knownSide: drcr.CR,
//          resStatus: resStatus.RESIDENT,
//          accType: [accType.FCA],
          match: crThisBank.and(not(drThisBank)).
                   and(crHasResStatus(resStatus.RESIDENT)).
                   and(crHasAccType([accType.FCA])),
          rules: [
            useDr(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            useDr(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            useDr(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            decideDr({
                        manualSection: "B and T Section B.1 (A) - page 1",
                        reportable: rep.ZZ1REPORTABLE,
                        flow: flowDir.OUT,
                        reportingSide: drcr.CR,
                        nonResSide: drcr.DR,
                        nonResAccountType: at.NR_NAD,
                        resSide: drcr.CR,
                        resAccountType: at.RE_FCA
                      }, localBankOther.and(transferLOCAL)),
            useDr(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            useDr(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            useDr(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          scenario: "CustomerReceipt",
//          knownSide: drcr.CR,
//          resStatus: resStatus.RESIDENT,
//          accType: [accType.CASH_LOCAL, accType.CASH_CURR],
          match: crThisBank.and(not(drThisBank)).
                   and(crHasResStatus(resStatus.RESIDENT)).
                   and(crHasAccType([accType.CASH_LOCAL, accType.CASH_CURR])),
          rules: [
            useDr(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            useDr(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            useDr(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            useDr(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            useDr(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          scenario: "CustomerReceipt",
//          knownSide: drcr.CR,
//          resStatus: resStatus.RESIDENT,
//          accType: [accType.VOSTRO],
          match: crThisBank.and(not(drThisBank)).
                   and(crHasResStatus(resStatus.RESIDENT)).
                   and(crHasAccType([accType.VOSTRO])),
          rules: [
            useDr(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            useDr(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            useDr(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            useDr(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            useDr(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          scenario: "CustomerReceipt",
//          knownSide: drcr.CR,
//          resStatus: resStatus.NONRES,
//          accType: [accType.LOCAL_ACC, accType.FCA],
          match: crThisBank.and(not(drThisBank)).
                   and(crHasResStatus(resStatus.NONRES)).
                   and(crHasAccType([accType.LOCAL_ACC, accType.FCA])),
          rules: [
            useDr(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            useDr(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            useDr(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            useDr(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            useDr(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          scenario: "CustomerReceipt",
//          knownSide: drcr.CR,
//          resStatus: resStatus.NONRES,
//          accType: [accType.CASH_LOCAL, accType.CASH_CURR],
          match: crThisBank.and(not(drThisBank)).
                   and(crHasResStatus(resStatus.NONRES)).
                   and(crHasAccType([accType.CASH_LOCAL, accType.CASH_CURR])),
          rules: [
            useDr(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            useDr(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            useDr(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            useDr(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            useDr(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          scenario: "CustomerReceipt",
//          knownSide: drcr.CR,
//          resStatus: resStatus.HOLDCO,
//          accType: [accType.LOCAL_ACC, accType.FCA],
          match: crThisBank.and(not(drThisBank)).
                   and(crHasResStatus(resStatus.HOLDCO)).
                   and(crHasAccType([accType.LOCAL_ACC, accType.FCA])),
          rules: [
            useDr(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            useDr(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            useDr(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            useDr(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            useDr(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          scenario: "CustomerReceipt",
//          knownSide: drcr.CR,
//          resStatus: resStatus.RESIDENT,
//          accType: [accType.CFC, accType.NOSTRO],
          match: crThisBank.and(not(drThisBank)).
                   and(crHasResStatus(resStatus.RESIDENT)).
                   and(crHasAccType([accType.CFC, accType.NOSTRO])),
          rules: [
            useDr(resStatus.NONRES, accType.FCA, localBankOther.and(transferCURR).and(field72("TRANSFER FROM ABROAD").or(field72("RETURN OF FUNDS")))),
            useDr(resStatus.RESIDENT, accType.CFC, localBankOther.and(transferCURR).and(not(field72("TRANSFER FROM ABROAD").or(field72("RETURN OF FUNDS"))))),
            useDr(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC").or(field72("TRANSFER FROM ABROAD").or(field72("RETURN OF FUNDS"))))),
            useDr(resStatus.RESIDENT, accType.CFC, localBankAD.and(transferCURR).and(not(field72("NTNRC").or(field72("TRANSFER FROM ABROAD").or(field72("RETURN OF FUNDS")))))),
            useDr(resStatus.NONRES, accType.LOCAL_ACC, localBankOther.and(transferLOCAL).and(field72("TRANSFER FROM ABROAD").or(field72("RETURN OF FUNDS")))),
            useDr(resStatus.RESIDENT, accType.CFC, localBankOther.and(transferLOCAL).and(not(field72("TRANSFER FROM ABROAD").or(field72("RETURN OF FUNDS"))))),
            useDr(resStatus.RESIDENT, accType.CFC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            useDr(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            useDr(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          scenario: "CustomerReceipt",
//          knownSide: drcr.CR,
//          resStatus: resStatus.NONRES,
//          accType: [accType.CFC, accType.NOSTRO],
          match: crThisBank.and(not(drThisBank)).
                   and(crHasResStatus(resStatus.NONRES)).
                   and(crHasAccType([accType.CFC, accType.NOSTRO])),
          rules: [
            useDr(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            useDr(resStatus.NONRES, accType.CFC, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            useDr(resStatus.RESIDENT, accType.CFC, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            useDr(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            useDr(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          scenario: "CustomerReceipt",
//          knownSide: drcr.CR,
//          resStatus: resStatus.NONRES,
//          accType: [accType.VOSTRO],
          match: crThisBank.and(not(drThisBank)).
                   and(crHasResStatus(resStatus.NONRES)).
                   and(crHasAccType([accType.VOSTRO])),
          rules: [
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            useDr(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            useDr(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            useDr(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRB")))),
            useDr(resStatus.NONRES, accType.VOSTRO, localBankAD.and(field72("NTNRB"))),
            useDr(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        }
      ]
    }
    return scenarios;
  }
})