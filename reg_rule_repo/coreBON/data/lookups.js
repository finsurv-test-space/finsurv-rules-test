define({
  ReportingQualifier: ["BOPCUS", "NON REPORTABLE", "BOPCARD RESIDENT", "BOPCARD NON RESIDENT"],
  moneyTransferAgents: ["AD", "ADLA", "CARD", "MONEYGRAM", "WESTERNUNION", "MUKURU", "TRAVEL CARD", "TRAVELLERS CHEQUE"],
  provinces: ["CAPRIVI", "ERONGO", "HARDAP", "!KARAS", "KAVANGO EAST", "KAVANGO WEST", "KHOMAS", "KUNENE", "OHANGWENA", "OMAHEKE", "OMUSATI", "OSHANA", "OSHIKOTO", "OTJOZONDJUPA", "ZAMBEZI"],
  nonResidentExceptions: ["MUTUAL PARTY", "BULK INTEREST", "BULK VAT REFUNDS", "BULK BANK CHARGES", "BULK PENSIONS"],
  residentExceptions: ["MUTUAL PARTY", "BULK PENSIONS", "BULK INTEREST", "BULK DIVIDENDS", "BULK BANK CHARGES"],
  accountIdentifiersNonResident: ["NON RESIDENT OTHER", "NON RESIDENT NAD", "NON RESIDENT FCA", "CASH", "FCA RESIDENT", "RES FOREIGN BANK ACCOUNT", "VOSTRO"],
  accountIdentifiersResident   : ["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "EFT", "CARD PAYMENT", "DEBIT CARD", "CREDIT CARD", "VOSTRO"],
  industrialClassifications    : [
    {code: "01", description: "AGRICULTURE, HUNTING, FORESTRY AND FISHING"},
    {code: "02", description: "MINING AND QUARRYING"},
    {code: "03", description: "MANUFACTURING"},
    {code: "04", description: "ELECTRICITY, GAS AND WATER SUPPLY"},
    {code: "05", description: "CONSTRUCTION"},
    {code: "06", description: "WHOLESALE, RETAIL TRADE, REPAIR, HOTEL, RESTAURANT"},
    {code: "07", description: "TRANSPORT, STORAGE AND COMMUNICATION"},
    {code: "08", description: "FINANCIAL SERVICES"},
    {code: "09", description: "COMMUNITY, SOCIAL AND PERSONAL SERVICES"},
    {code: "10", description: "OTHER ACTIVITIES NOT ADEQUATELY DEFINED"}
  ],
  customsOffices: [],
  currencies                   : [
    {code: 'ADP', name: 'Andorran Peseta'},
    {code: 'AED', name: 'Arab Emirates Dirham'},
    {code: 'AFA', name: 'Afghani'},
    {code: 'AFN', name: 'Afghani'},
    {code: 'ALL', name: 'Lek'},
    {code: 'AMD', name: 'Armenian Dram'},
    {code: 'ANG', name: 'Netherlans Antillan Guilder'},
    {code: 'AON', name: 'New Kwanza'},
    {code: 'AOR', name: 'Kwanza Readjustado'},
    {code: 'ARS', name: 'Argentine Peso'},
    {code: 'ATS', name: 'Austrian Schilling'},
    {code: 'AUD', name: 'Australian Dollar'},
    {code: 'AWG', name: 'Aruban Guilder'},
    {code: 'AZM', name: 'Azerbaijanian Manat'},
    {code: 'BAM', name: 'Convertible Marks'},
    {code: 'BBD', name: 'Barbados Dollar'},
    {code: 'BDT', name: 'Taka'},
    {code: 'BEF', name: 'Belgian Franc'},
    {code: 'BGL', name: 'Lev'},
    {code: 'BGN', name: 'Bulgarian Lev'},
    {code: 'BHD', name: 'Bahraini'},
    {code: 'BIF', name: 'Burundi Franc'},
    {code: 'BMD', name: 'Bermudian Dollat'},
    {code: 'BND', name: 'Brunei Dollar'},
    {code: 'BOB', name: 'Boliviano'},
    {code: 'BOV', name: 'Mvdol'},
    {code: 'BRL', name: 'Brazilian Real'},
    {code: 'BSD', name: 'Bahamian Dollar'},
    {code: 'BTN', name: 'Ngultrum'},
    {code: 'BWP', name: 'Botswana Pula'},
    {code: 'BYB', name: 'Belarussian Ruble'},
    {code: 'BYR', name: 'Belarussian Ruble'},
    {code: 'BZD', name: 'Belize Dollar'},
    {code: 'CAD', name: 'Canadian Dollar'},
    {code: 'CDF', name: 'Frank Congalais'},
    {code: 'CHF', name: 'Swiss Frank'},
    {code: 'CLF', name: 'Unidades De Fomento'},
    {code: 'CLP', name: 'Chillean Peso'},
    {code: 'CNH', name: 'Offshore Chinese Renminbi'},
    {code: 'CNY', name: 'Yuan Renminbi'},
    {code: 'COP', name: 'Colombian Peso'},
    {code: 'CRC', name: 'Costa Rican Colon'},
    {code: 'CSD', name: 'Serbian Dinar'},
    {code: 'CUP', name: 'Cuban Peso'},
    {code: 'CVE', name: 'Cape Verde Escudo'},
    {code: 'CYP', name: 'Cyprus Pound'},
    {code: 'CZK', name: 'Czech Koruna'},
    {code: 'DEM', name: 'German Deutsche Mark'},
    {code: 'DJF', name: 'Djibouti Franc'},
    {code: 'DKK', name: 'Danish Krone'},
    {code: 'DOP', name: 'Dominican Peso'},
    {code: 'DZD', name: 'Algerian Dinar'},
    {code: 'ECS', name: 'Sucre'},
    {code: 'ECV', name: 'Unidad De Valor Constante'},
    {code: 'EEK', name: 'Kroon'},
    {code: 'EGP', name: 'Egyptian Pound'},
    {code: 'ERN', name: 'Nafka'},
    {code: 'ESP', name: 'Spanish Peseta'},
    {code: 'ETB', name: 'Ethiopian Birr'},
    {code: 'EUR', name: 'Euro'},
    {code: 'FIM', name: 'Finnish Markka'},
    {code: 'FJD', name: 'Fiji Dollar'},
    {code: 'FKP', name: 'Falkland Islands Pound'},
    {code: 'FRF', name: 'French Frank'},
    {code: 'GBP', name: 'British Pound Sterling'},
    {code: 'GEL', name: 'Lari'},
    {code: 'GHC', name: 'Cedi'},
    {code: 'GHS', name: 'Ghana Cedi'},
    {code: 'GIP', name: 'Gibraltar Pound'},
    {code: 'GMD', name: 'Dalasi'},
    {code: 'GNF', name: 'Guinea Franc'},
    {code: 'GRD', name: 'Greek Drachma'},
    {code: 'GTQ', name: 'Quetzal'},
    {code: 'GYD', name: 'Guyana Dollor'},
    {code: 'GWP', name: 'Guinea-Bissau Peso'},
    {code: 'HKD', name: 'Hong Kong Dollar'},
    {code: 'HNL', name: 'Lempira'},
    {code: 'HRK', name: 'Kuna'},
    {code: 'HTG', name: 'Gourde'},
    {code: 'HUF', name: 'Hungarian Forint'},
    {code: 'IDR', name: 'Rupiah'},
    {code: 'IEP', name: 'Irish Pound'},
    {code: 'ILS', name: 'New Israeli Shekel'},
    {code: 'INR', name: 'Indian Rupee'},
    {code: 'IQD', name: 'Iraqi Dinar'},
    {code: 'IRR', name: 'Iranian Rial'},
    {code: 'ISK', name: 'Iceland Krona'},
    {code: 'ITL', name: 'Italian Lira'},
    {code: 'JMD', name: 'Jamaican Dollar'},
    {code: 'JOD', name: 'Jordian Dinar'},
    {code: 'JPY', name: 'Japanese Yen'},
    {code: 'KES', name: 'Kenyan Shilling'},
    {code: 'KGS', name: 'Som'},
    {code: 'KHR', name: 'Riel'},
    {code: 'KMF', name: 'Comoro Franc'},
    {code: 'KPW', name: 'North Korean Won'},
    {code: 'KRW', name: 'Won'},
    {code: 'KWD', name: 'Kuwaiti Dinr'},
    {code: 'KYD', name: 'Cayman Islands Dollar'},
    {code: 'KZT', name: 'Tenge'},
    {code: 'LAK', name: 'Kip'},
    {code: 'LBP', name: 'Lebanese Pound'},
    {code: 'LKR', name: 'Sri Lanka Rupee'},
    {code: 'LRD', name: 'Liberian Dollar'},
    {code: 'LSL', name: 'Lesotho Loti'},
    {code: 'LTL', name: 'Lithuanian Litas'},
    {code: 'LUF', name: 'Luxembourg Franc'},
    {code: 'LVL', name: 'Latvian Lats'},
    {code: 'LYD', name: 'Libyan Dinar'},
    {code: 'MAD', name: 'Moroccan Dirham'},
    {code: 'MDL', name: 'Moldovan Leu'},
    {code: 'MGA', name: 'Madagascar Ariary'},
    {code: 'MGF', name: 'Malagasy Franc'},
    {code: 'MKD', name: 'Denar'},
    {code: 'MMK', name: 'Kyat'},
    {code: 'MNT', name: 'Tugrik'},
    {code: 'MOP', name: 'Pataca'},
    {code: 'MRO', name: 'Ouguiya'},
    {code: 'MTL', name: 'Maltese Lira'},
    {code: 'MUR', name: 'Mauritius Rupee'},
    {code: 'MVR', name: 'Rufiyaa'},
    {code: 'MWK', name: 'Malawi Kwacha'},
    {code: 'MXN', name: 'Mexican Peso'},
    {code: 'MXV', name: 'Mexican Unidad De Inversion'},
    {code: 'MYR', name: 'Malaysian Ringgit'},
    {code: 'MZN', name: 'New Mozambican Metical'},
    {code: 'MZM', name: 'Metical'},
    {code: 'NAD', name: 'Namibian Dollar'},
    {code: 'NGN', name: 'Naira'},
    {code: 'NIO', name: 'Cordoba Oro'},
    {code: 'NLG', name: 'Dutch Guilder'},
    {code: 'NOK', name: 'Norwegian Krone'},
    {code: 'NPR', name: 'Nepalese Rupee'},
    {code: 'NZD', name: 'New Zealand Dollar'},
    {code: 'OMR', name: 'Rial Omani'},
    {code: 'PAB', name: 'Balboa'},
    {code: 'PEN', name: 'Nuevo Sol'},
    {code: 'PGK', name: 'Kina'},
    {code: 'PHP', name: 'Philippine Peso'},
    {code: 'PKR', name: 'Pakistan Rupee'},
    {code: 'PLN', name: 'Polish Zloty'},
    {code: 'PTE', name: 'Portuguese Escudo'},
    {code: 'PYG', name: 'Guarani'},
    {code: 'QAR', name: 'Qatari Rial'},
    {code: 'RMB', name: 'Chinese Renminbi'},
    {code: 'ROL', name: 'Leu'},
    {code: 'RSD', name: 'Serbian Dinar'},
    {code: 'RUB', name: 'Russian Ruble (New)'},
    {code: 'RUR', name: 'Russian Rubble (Old)'},
    {code: 'RWF', name: 'Rwanda Franc'},
    {code: 'SAR', name: 'Saudi Riyal'},
    {code: 'SBD', name: 'Soloman Island Dollar'},
    {code: 'SCR', name: 'Seychelles Rupee'},
    {code: 'SDD', name: 'Sudanese Dinar'},
    {code: 'SEK', name: 'Swedish Krona'},
    {code: 'SGD', name: 'Singapore Dollar'},
    {code: 'SHP', name: 'St Helena Pound'},
    {code: 'SIT', name: 'Tolar'},
    {code: 'SKK', name: 'Slovak Koruna'},
    {code: 'SLL', name: 'Leone'},
    {code: 'SOS', name: 'Somali Shilling'},
    {code: 'SRD', name: 'Suriname Dollar'},
    {code: 'SRG', name: 'Suriname Guilder'},
    {code: 'STD', name: 'Dobra'},
    {code: 'SVC', name: 'El Salvador Colon'},
    {code: 'SYP', name: 'Syrian Pound'},
    {code: 'SZL', name: 'Swaziland Lilangeni'},
    {code: 'THB', name: 'Thai Baht'},
    {code: 'TJR', name: 'Tajik Ruble'},
    {code: 'TJS', name: 'Somoni'},
    {code: 'TMM', name: 'Manat'},
    {code: 'TND', name: 'Tunisian Dinar'},
    {code: 'TOP', name: 'Pa Anga'},
    {code: 'TPE', name: 'Timor Escudo'},
    {code: 'TRL', name: 'Turkish Lira'},
    {code: 'TRY', name: 'Turkish New Lira'},
    {code: 'TTD', name: 'Trinidad And Tobago Dollar'},
    {code: 'TWD', name: 'Taiwan Dollar'},
    {code: 'TZS', name: 'Tanzanian Shilling'},
    {code: 'UAH', name: 'Hryvnia'},
    {code: 'UGX', name: 'Uganda Shilling'},
    {code: 'USD', name: 'US Dollar'},
    {code: 'USN', name: 'US Dollar Next Day Funds'},
    {code: 'UYU', name: 'Peso Uruguayo'},
    {code: 'UZS', name: 'Uzbekistan Sum'},
    {code: 'VEB', name: 'Bolivar'},
    {code: 'VEF', name: 'Bolivar Fuerte'},
    {code: 'VND', name: 'Dong'},
    {code: 'VUV', name: 'Vatu'},
    {code: 'WST', name: 'Tala'},
    {code: 'XAF', name: 'CFA Franc BEAC'},
    {code: 'XCD', name: 'East Caribbean Dollar'},
    {code: 'XOF', name: 'CFA Franc BCEAO'},
    {code: 'XPF', name: 'CFP Franc'},
    {code: 'YER', name: 'Yemeni Rial'},
    {code: 'ZAR', name: 'South African Rand'},
    {code: 'ZMK', name: 'Zambian Kwacha'},
    {code: 'ZWD', name: 'Zimbabwe Dollar'},
  ],
  countries                    : [
    {code: 'AD', name: 'Andorra'},
    {code: 'AE', name: 'United Arab Emirates'},
    {code: 'AF', name: 'Afghanistan'},
    {code: 'AG', name: 'Antigua and Barbuda'},
    {code: 'AI', name: 'Anguilla'},
    {code: 'AL', name: 'Albania'},
    {code: 'AM', name: 'Armenia'},
    {code: 'AN', name: 'Netherlands Antilles'},
    {code: 'AO', name: 'Angola'},
    {code: 'AQ', name: 'Antarctica'},
    {code: 'AR', name: 'Argentina'},
    {code: 'AS', name: 'American Samoa'},
    {code: 'AT', name: 'Austria'},
    {code: 'AU', name: 'Australia'},
    {code: 'AW', name: 'Aruba'},
    {code: 'AX', name: 'Åland Islands'},
    {code: 'AZ', name: 'Azerbaijan'},
    {code: 'BA', name: 'Bosnia and Herzegovina'},
    {code: 'BB', name: 'Barbados'},
    {code: 'BD', name: 'Bangladesh'},
    {code: 'BE', name: 'Belgium'},
    {code: 'BF', name: 'Burkina Faso'},
    {code: 'BG', name: 'Bulgaria'},
    {code: 'BH', name: 'Bahrain'},
    {code: 'BI', name: 'Burundi'},
    {code: 'BJ', name: 'Benin'},
    {code: 'BL', name: 'Saint Barthélemy'},
    {code: 'BM', name: 'Bermuda'},
    {code: 'BN', name: 'Brunei Darussalam'},
    {code: 'BO', name: 'Bolivia, Plurinational State of'},
    {code: 'BR', name: 'Brazil'},
    {code: 'BS', name: 'Bahamas'},
    {code: 'BT', name: 'Bhutan'},
    {code: 'BV', name: 'Bouvet Island'},
    {code: 'BW', name: 'Botswana'},
    {code: 'BY', name: 'Belarus'},
    {code: 'BZ', name: 'Belize'},
    {code: 'CA', name: 'Canada'},
    {code: 'CC', name: 'Cocos (Keeling) Islands'},
    {code: 'CD', name: 'Congo, the Democratic Republic of the'},
    {code: 'CF', name: 'Central African Republic'},
    {code: 'CG', name: 'Congo'},
    {code: 'CH', name: 'Switzerland'},
    {code: 'CI', name: "Côte d'Ivoire"},
    {code: 'CK', name: 'Cook Islands'},
    {code: 'CL', name: 'Chile'},
    {code: 'CM', name: 'Cameroon'},
    {code: 'CN', name: 'China'},
    {code: 'CO', name: 'Colombia'},
    {code: 'CR', name: 'Costa Rica'},
    {code: 'CS', name: 'Former Czechoslovak (Serbia and Montenegro)'},
    {code: 'CU', name: 'Cuba'},
    {code: 'CV', name: 'Cape Verde'},
    {code: 'CX', name: 'Christmas Island'},
    {code: 'CY', name: 'Cyprus'},
    {code: 'CZ', name: 'Czech Republic'},
    {code: 'DE', name: 'Germany'},
    {code: 'DJ', name: 'Djibouti'},
    {code: 'DK', name: 'Denmark'},
    {code: 'DM', name: 'Dominica'},
    {code: 'DO', name: 'Dominican Republic'},
    {code: 'DZ', name: 'Algeria'},
    {code: 'EC', name: 'Ecuador'},
    {code: 'EE', name: 'Estonia'},
    {code: 'EG', name: 'Egypt'},
    {code: 'EH', name: 'Western Sahara'},
    {code: 'ER', name: 'Eritrea'},
    {code: 'ES', name: 'Spain'},
    {code: 'ET', name: 'Ethiopia'},
    {code: 'FI', name: 'Finland'},
    {code: 'FJ', name: 'Fiji'},
    {code: 'FK', name: 'Falkland Islands (Malvinas)'},
    {code: 'FM', name: 'Micronesia, Federated States of'},
    {code: 'FO', name: 'Faroe Islands'},
    {code: 'FR', name: 'France'},
    {code: 'GA', name: 'Gabon'},
    {code: 'GB', name: 'United Kingdom'},
    {code: 'GD', name: 'Grenada'},
    {code: 'GE', name: 'Georgia'},
    {code: 'GF', name: 'French Guiana'},
    {code: 'GG', name: 'Guernsey'},
    {code: 'GH', name: 'Ghana'},
    {code: 'GI', name: 'Gibraltar'},
    {code: 'GL', name: 'Greenland'},
    {code: 'GM', name: 'Gambia'},
    {code: 'GN', name: 'Guinea'},
    {code: 'GP', name: 'Guadeloupe'},
    {code: 'GQ', name: 'Equatorial Guinea'},
    {code: 'GR', name: 'Greece'},
    {code: 'GS', name: 'South Georgia and the South Sandwich Islands'},
    {code: 'GT', name: 'Guatemala'},
    {code: 'GU', name: 'Guam'},
    {code: 'GW', name: 'Guinea-Bissau'},
    {code: 'GY', name: 'Guyana'},
    {code: 'HK', name: 'Hong Kong'},
    {code: 'HM', name: 'Heard Island and McDonald Islands'},
    {code: 'HN', name: 'Honduras'},
    {code: 'HR', name: 'Croatia'},
    {code: 'HT', name: 'Haiti'},
    {code: 'HU', name: 'Hungary'},
    {code: 'ID', name: 'Indonesia'},
    {code: 'IE', name: 'Ireland'},
    {code: 'IL', name: 'Israel'},
    {code: 'IM', name: 'Isle of Man'},
    {code: 'IN', name: 'India'},
    {code: 'IO', name: 'British Indian Ocean Territory'},
    {code: 'IQ', name: 'Iraq'},
    {code: 'IR', name: 'Iran, Islamic Republic of'},
    {code: 'IS', name: 'Iceland'},
    {code: 'IT', name: 'Italy'},
    {code: 'JE', name: 'Jersey'},
    {code: 'JM', name: 'Jamaica'},
    {code: 'JO', name: 'Jordan'},
    {code: 'JP', name: 'Japan'},
    {code: 'KE', name: 'Kenya'},
    {code: 'KG', name: 'Kyrgyzstan'},
    {code: 'KH', name: 'Cambodia'},
    {code: 'KI', name: 'Kiribati'},
    {code: 'KM', name: 'Comoros'},
    {code: 'KN', name: 'Saint Kitts and Nevis'},
    {code: 'KP', name: "Korea, Democratic Peoples Republic of"},
    {code: 'KR', name: 'Korea, Republic of'},
    {code: 'KW', name: 'Kuwait'},
    {code: 'KY', name: 'Cayman Islands'},
    {code: 'KZ', name: 'Kazakhstan'},
    {code: 'LA', name: "Lao People's Democratic Republic"},
    {code: 'LB', name: 'Lebanon'},
    {code: 'LC', name: 'Saint Lucia'},
    {code: 'LI', name: 'Liechtenstein'},
    {code: 'LK', name: 'Sri Lanka'},
    {code: 'LR', name: 'Liberia'},
    {code: 'LS', name: 'Lesotho'},
    {code: 'LT', name: 'Lithuania'},
    {code: 'LU', name: 'Luxembourg'},
    {code: 'LV', name: 'Latvia'},
    {code: 'LY', name: 'Libya'},
    {code: 'MA', name: 'Morocco'},
    {code: 'MC', name: 'Monaco'},
    {code: 'MD', name: 'Moldova, Republic of'},
    {code: 'ME', name: 'Montenegro'},
    {code: 'MG', name: 'Madagascar'},
    {code: 'MH', name: 'Marshall Islands'},
    {code: 'MK', name: 'Macedonia, The Former Yugoslav Republic of'},
    {code: 'ML', name: 'Mali'},
    {code: 'MM', name: 'Myanmar'},
    {code: 'MN', name: 'Mongolia'},
    {code: 'MO', name: 'Macao'},
    {code: 'MP', name: 'Northern Mariana Islands'},
    {code: 'MQ', name: 'Martinique'},
    {code: 'MR', name: 'Mauritania'},
    {code: 'MS', name: 'Montserrat'},
    {code: 'MT', name: 'Malta'},
    {code: 'MU', name: 'Mauritius'},
    {code: 'MV', name: 'Maldives'},
    {code: 'MW', name: 'Malawi'},
    {code: 'MX', name: 'Mexico'},
    {code: 'MY', name: 'Malaysia'},
    {code: 'MZ', name: 'Mozambique'},
    {code: 'NA', name: 'Namibia'},
    {code: 'NC', name: 'New Caledonia'},
    {code: 'NE', name: 'Niger'},
    {code: 'NF', name: 'Norfolk Island'},
    {code: 'NG', name: 'Nigeria'},
    {code: 'NI', name: 'Nicaragua'},
    {code: 'NL', name: 'Netherlands'},
    {code: 'NO', name: 'Norway'},
    {code: 'NP', name: 'Nepal'},
    {code: 'NR', name: 'Nauru'},
    {code: 'NU', name: 'Niue'},
    {code: 'NZ', name: 'New Zealand'},
    {code: 'OM', name: 'Oman'},
    {code: 'PA', name: 'Panama'},
    {code: 'PB', name: 'Panama (VISA Card)'},
    {code: 'PE', name: 'Peru'},
    {code: 'PF', name: 'French Polynesia'},
    {code: 'PG', name: 'Papua New Guinea'},
    {code: 'PH', name: 'Philippines'},
    {code: 'PK', name: 'Pakistan'},
    {code: 'PL', name: 'Poland'},
    {code: 'PM', name: 'Saint Pierre and Miquelon'},
    {code: 'PN', name: 'Pitcairn'},
    {code: 'PR', name: 'Puerto Rico'},
    {code: 'PS', name: 'Palestine, State of'},
    {code: 'PT', name: 'Portugal'},
    {code: 'PW', name: 'Palau'},
    {code: 'PY', name: 'Paraguay'},
    {code: 'PZ', name: 'Panama Canal Zone'},
    {code: 'QA', name: 'Qatar'},
    {code: 'QZ', name: 'Kosovo'},
    {code: 'RE', name: 'Réunion'},
    {code: 'RO', name: 'Romania'},
    {code: 'RS', name: 'Serbia'},
    {code: 'RU', name: 'Russian Federation'},
    {code: 'RW', name: 'Rwanda'},
    {code: 'SA', name: 'Saudi Arabia'},
    {code: 'SB', name: 'Solomon Islands'},
    {code: 'SC', name: 'Seychelles'},
    {code: 'SD', name: 'Sudan'},
    {code: 'SE', name: 'Sweden'},
    {code: 'SG', name: 'Singapore'},
    {code: 'SH', name: 'Saint Helena, Ascension and Tristan da Cunha'},
    {code: 'SI', name: 'Slovenia'},
    {code: 'SJ', name: 'Svalbard and Jan Mayen'},
    {code: 'SK', name: 'Slovakia'},
    {code: 'SL', name: 'Sierra Leone'},
    {code: 'SM', name: 'San Marino'},
    {code: 'SN', name: 'Senegal'},
    {code: 'SO', name: 'Somalia'},
    {code: 'SR', name: 'Suriname'},
    {code: 'ST', name: 'Sao Tome and Principe'},
    {code: 'SV', name: 'El Salvador'},
    {code: 'SY', name: 'Syrian Arab Republic'},
    {code: 'SZ', name: 'Swaziland'},
    {code: 'TC', name: 'Turks and Caicos Islands'},
    {code: 'TD', name: 'Chad'},
    {code: 'TF', name: 'French Southern Territories'},
    {code: 'TG', name: 'Togo'},
    {code: 'TH', name: 'Thailand'},
    {code: 'TJ', name: 'Tajikistan'},
    {code: 'TK', name: 'Tokelau'},
    {code: 'TL', name: 'Timor-Leste'},
    {code: 'TM', name: 'Turkmenistan'},
    {code: 'TN', name: 'Tunisia'},
    {code: 'TO', name: 'Tonga'},
    {code: 'TP', name: 'East Timor'},
    {code: 'TR', name: 'Turkey'},
    {code: 'TT', name: 'Trinidad and Tobago'},
    {code: 'TV', name: 'Tuvalu'},
    {code: 'TW', name: 'Taiwan, Province of China'},
    {code: 'TZ', name: 'Tanzania, United Republic of'},
    {code: 'UA', name: 'Ukraine'},
    {code: 'UG', name: 'Uganda'},
    {code: 'UM', name: 'United States Minor Outlying Islands'},
    {code: 'US', name: 'United States'},
    {code: 'UY', name: 'Uruguay'},
    {code: 'UZ', name: 'Uzbekistan'},
    {code: 'VA', name: 'Holy See (Vatican City State)'},
    {code: 'VC', name: 'Saint Vincent and the Grenadines'},
    {code: 'VE', name: 'Venezuela, Bolivarian Republic of'},
    {code: 'VG', name: 'Virgin Islands, British'},
    {code: 'VI', name: 'Virgin Islands, U.S.'},
    {code: 'VN', name: 'Vietnam'},
    {code: 'VU', name: 'Vanuatu'},
    {code: 'WF', name: 'Wallis and Futuna'},
    {code: 'WS', name: 'Samoa'},
    {code: 'XC', name: 'Leeward & Windward Islands'},
    {code: 'YE', name: 'Yemen'},
    {code: 'YT', name: 'Mayotte'},
    {code: 'ZA', name: 'South Africa'},
    {code: 'ZM', name: 'Zambia'},
    {code: 'ZW', name: 'Zimbabwe'}
  ],
  adhocSubjects: ["INVALIDIDNUMBER"],
  categories: [
    {
     flow: "IN", 
     code: "100", 
     section: "Merchandise", 
     description: "Adjustments / Reversals / Refunds applicable to merchandise"
    },
    {
     flow: "IN", 
     code: "101/01", 
     section: "Merchandise", 
     description: "Export advance payment - (excluding capital goods, gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal, iron ore and goods exported via the Namibian Post Office)"
    },
    {
     flow: "IN", 
     code: "101/02", 
     section: "Merchandise", 
     description: "Export advance payment - capital goods"
    },
    {
     flow: "IN", 
     code: "101/03", 
     section: "Merchandise", 
     description: "Export advance payment - gold"
    },
    {
     flow: "IN", 
     code: "101/04", 
     section: "Merchandise", 
     description: "Export advance payment - platinum"
    },
    {
     flow: "IN", 
     code: "101/05", 
     section: "Merchandise", 
     description: "Export advance payment - crude oil"
    },
    {
     flow: "IN", 
     code: "101/06", 
     section: "Merchandise", 
     description: "Export advance payment - refined petroleum products"
    },
    {
     flow: "IN", 
     code: "101/07", 
     section: "Merchandise", 
     description: "Export advance payment - diamonds"
    },
    {
     flow: "IN", 
     code: "101/08", 
     section: "Merchandise", 
     description: "Export advance payment - steel"
    },
    {
     flow: "IN", 
     code: "101/09", 
     section: "Merchandise", 
     description: "Export advance payment - coal"
    },
    {
     flow: "IN", 
     code: "101/10", 
     section: "Merchandise", 
     description: "Export advance payment - iron ore"
    },
    {
     flow: "IN", 
     code: "101/11", 
     section: "Merchandise", 
     description: "Export advance payment - goods exported via the Namibian Post Office"
    },
    {
     flow: "IN", 
     code: "102/02", 
     section: "Merchandise", 
     description: "Export advance payment - capital good"
    },
    {
     flow: "IN", 
     code: "102/03", 
     section: "Merchandise", 
     description: "Export advance payment -– gold"
    },
    {
     flow: "IN", 
     code: "102/04", 
     section: "Merchandise", 
     description: "Export advance payment -– platinum"
    },
    {
     flow: "IN", 
     code: "102/05", 
     section: "Merchandise", 
     description: "Export advance payment - crude oil"
    },
    {
     flow: "IN", 
     code: "102/06", 
     section: "Merchandise", 
     description: "Export advance payment - refined petroleum products"
    },
    {
     flow: "IN", 
     code: "102/07", 
     section: "Merchandise", 
     description: "Export advance payment -– diamonds"
    },
    {
     flow: "IN", 
     code: "102/08", 
     section: "Merchandise", 
     description: "Export advance payment- steel"
    },
    {
     flow: "IN", 
     code: "102/09", 
     section: "Merchandise", 
     description: "Export advance payment -– coal"
    },
    {
     flow: "IN", 
     code: "102/10", 
     section: "Merchandise", 
     description: "Export advance payment - iron ore"
    },
    {
     flow: "IN", 
     code: "102/11", 
     section: "Merchandise", 
     description: "Export advance payment - goods imported via the Namibian Post Office"
    },
    {
     flow: "IN", 
     code: "103/01", 
     section: "Merchandise", 
     description: "Export payments - (excluding capital goods, gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal, iron ore and goods exported via the Namibian Post Office)"
    },
    {
     flow: "IN", 
     code: "103/02", 
     section: "Merchandise", 
     description: "Export payment - capital goods"
    },
    {
     flow: "IN", 
     code: "103/03", 
     section: "Merchandise", 
     description: "Export payment - gold"
    },
    {
     flow: "IN", 
     code: "103/04", 
     section: "Merchandise", 
     description: "Export payment - platinum"
    },
    {
     flow: "IN", 
     code: "103/05", 
     section: "Merchandise", 
     description: "Export payment - crude oil"
    },
    {
     flow: "IN", 
     code: "103/06", 
     section: "Merchandise", 
     description: "Export payment - refined petroleum products"
    },
    {
     flow: "IN", 
     code: "103/07", 
     section: "Merchandise", 
     description: "Export payment - diamonds"
    },
    {
     flow: "IN", 
     code: "103/08", 
     section: "Merchandise", 
     description: "Export payment - steel"
    },
    {
     flow: "IN", 
     code: "103/09", 
     section: "Merchandise", 
     description: "Export payment - coal"
    },
    {
     flow: "IN", 
     code: "103/10", 
     section: "Merchandise", 
     description: "Export payment - iron ore"
    },
    {
     flow: "IN", 
     code: "103/11", 
     section: "Merchandise", 
     description: "Export payment - goods exported via the Namibian Post Office"
    },
    {
     flow: "IN", 
     code: "105", 
     section: "Merchandise", 
     description: "Consumables acquired in port"
    },
    {
     flow: "IN", 
     code: "106", 
     section: "Merchandise", 
     description: "Trade finance repayments in respect of exports"
    },
    {
     flow: "IN", 
     code: "107", 
     section: "Merchandise", 
     description: "Export proceeds where the Customs value of the shipment is less than NAD 500.00."
    },
    {
     flow: "IN", 
     code: "108", 
     section: "Merchandise", 
     description: "Export payments where goods were declared as part of passenger baggage and no UCR is available"
    },
    {
     flow: "IN", 
     code: "109/01", 
     section: "Merchandise", 
     description: "Goods purchased by non-residents where no physical export will take place, excluding gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal, iron ore and merchanting transactions"
    },
    {
     flow: "IN", 
     code: "109/02", 
     section: "Merchandise", 
     description: "Proceeds for gold purchased by non-residents where no physical export will take place, excluding merchanting transactions"
    },
    {
     flow: "IN", 
     code: "109/03", 
     section: "Merchandise", 
     description: "Proceeds for platinum purchased by non-residents where no physical export will take place, excluding merchanting transactions"
    },
    {
     flow: "IN", 
     code: "109/04", 
     section: "Merchandise", 
     description: "Proceeds for crude oil purchased by non-residents where no physical export will take place, excluding merchanting transactions"
    },
    {
     flow: "IN", 
     code: "109/05", 
     section: "Merchandise", 
     description: "Proceeds for refined petroleum products purchased by non-residents where no physical export will take place, excluding merchanting transactions"
    },
    {
     flow: "IN", 
     code: "109/06", 
     section: "Merchandise", 
     description: "Proceeds for diamonds purchased by non-residents where no physical export will take place, excluding merchanting transactions"
    },
    {
     flow: "IN", 
     code: "109/07", 
     section: "Merchandise", 
     description: "Proceeds for steel purchased by non-residents where no physical export willtake place, excluding merchanting transactions"
    },
    {
     flow: "IN", 
     code: "109/08", 
     section: "Merchandise", 
     description: "Proceeds for coal purchased by non-residents where no physical export will take place, excluding merchanting transactions"
    },
    {
     flow: "IN", 
     code: "109/09", 
     section: "Merchandise", 
     description: "Proceeds for iron ore purchased by non-residents where no physical export will take place, excluding merchanting transactions"
    },
    {
     flow: "IN", 
     code: "110", 
     section: "Merchandise", 
     description: "Merchanting transaction"
    },
    {
     flow: "IN", 
     code: "200", 
     section: "Intellectual property and other services", 
     description: "Adjustments / Reversals / Refunds applicable to intellectual property and service related items"
    },
    {
     flow: "IN", 
     code: "201", 
     section: "Intellectual property and other services", 
     description: "Rights assigned for licences to reproduce and/or distribute"
    },
    {
     flow: "IN", 
     code: "202", 
     section: "Intellectual property and other services", 
     description: "Rights assigned for using patents and inventions (licensing)"
    },
    {
     flow: "IN", 
     code: "203", 
     section: "Intellectual property and other services", 
     description: "Rights assigned for using patterns and designs (including industrial processes)"
    },
    {
     flow: "IN", 
     code: "204", 
     section: "Intellectual property and other services", 
     description: "Rights assigned for using copyrights"
    },
    {
     flow: "IN", 
     code: "205", 
     section: "Intellectual property and other services", 
     description: "Rights assigned for using franchises and trademarks"
    },
    {
     flow: "IN", 
     code: "210", 
     section: "Intellectual property and other services", 
     description: "Disposal of patents and inventions"
    },
    {
     flow: "IN", 
     code: "211", 
     section: "Intellectual property and other services", 
     description: "Disposal of patterns and designs (including industrial processes)"
    },
    {
     flow: "IN", 
     code: "212", 
     section: "Intellectual property and other services", 
     description: "Disposal of copyrights"
    },
    {
     flow: "IN", 
     code: "213", 
     section: "Intellectual property and other services", 
     description: "Disposal of franchises and trademarks"
    },
    {
     flow: "IN", 
     code: "220", 
     section: "Intellectual property and other services", 
     description: "Proceeds received for research and development services"
    },
    {
     flow: "IN", 
     code: "221", 
     section: "Intellectual property and other services", 
     description: "Funding received for research and development"
    },
    {
     flow: "IN", 
     code: "225", 
     section: "Intellectual property and other services", 
     description: "Sales of original manuscripts, sound recordings and films"
    },
    {
     flow: "IN", 
     code: "226", 
     section: "Intellectual property and other services", 
     description: "Receipt of funds relating to the production of motionpictures, radio and television programs and musical recordings"
    },
    {
     flow: "IN", 
     code: "230", 
     section: "Intellectual property and other services", 
     description: "The outright selling of ownership rights of software"
    },
    {
     flow: "IN", 
     code: "231", 
     section: "Intellectual property and other services", 
     description: "Computer-related services including maintenance, repair and consultancy"
    },
    {
     flow: "IN", 
     code: "232", 
     section: "Intellectual property and other services", 
     description: "Commercial sales of customised software and related licenses for use by customers"
    },
    {
     flow: "IN", 
     code: "233", 
     section: "Intellectual property and other services", 
     description: "Commercial sales of non-customised software on physical media with periodic licence to use"
    },
    {
     flow: "IN", 
     code: "234", 
     section: "Intellectual property and other services", 
     description: "Commercial sales of non-customised software provided on physical media with right to perpetual (ongoing) use"
    },
    {
     flow: "IN", 
     code: "235", 
     section: "Intellectual property and other services", 
     description: "Commercial sales of non-customised software provided for downloading or electronically made available with periodic license"
    },
    {
     flow: "IN", 
     code: "236", 
     section: "Intellectual property and other services", 
     description: "Commercial sales of non-customised software provided for downloading or electronically made available with single payment"
    },
    {
     flow: "IN", 
     code: "240/01", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on materials (excluding gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal and iron ore)"
    },
    {
     flow: "IN", 
     code: "240/02", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on gold"
    },
    {
     flow: "IN", 
     code: "240/03", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on platinum"
    },
    {
     flow: "IN", 
     code: "240/04", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on crude oil"
    },
    {
     flow: "IN", 
     code: "240/05", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on refined petroleum products"
    },
    {
     flow: "IN", 
     code: "240/06", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on diamonds"
    },
    {
     flow: "IN", 
     code: "240/07", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on steel"
    },
    {
     flow: "IN", 
     code: "240/08", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on coal"
    },
    {
     flow: "IN", 
     code: "240/09", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on iron ore"
    },
    {
     flow: "IN", 
     code: "241", 
     section: "Intellectual property and other services", 
     description: "Repairs and maintenance on machinery and equipment"
    },
    {
     flow: "IN", 
     code: "242", 
     section: "Intellectual property and other services", 
     description: "Architectural, engineering and other technical services"
    },
    {
     flow: "IN", 
     code: "243", 
     section: "Intellectual property and other services", 
     description: "Agricultural, mining, waste treatment and depollution services"
    },
    {
     flow: "IN", 
     code: "250", 
     section: "Intellectual property and other services", 
     description: "Travel services for non-residents - business travel"
    },
    {
     flow: "IN", 
     code: "251", 
     section: "Intellectual property and other services", 
     description: "Travel services for non-residents - holiday travel"
    },
    {
     flow: "IN", 
     code: "252", 
     section: "Intellectual property and other services", 
     description: "Foreign exchange accepted by residents from non-residents"
    },
    {
     flow: "IN", 
     code: "255", 
     section: "Intellectual property and other services", 
     description: "Travel services for residents - business travel"
    },
    {
     flow: "IN", 
     code: "256", 
     section: "Intellectual property and other services", 
     description: "Travel services for residents - holiday travel"
    },
    {
     flow: "IN", 
     code: "260", 
     section: "Intellectual property and other services", 
     description: "Proceeds for travel services in respect of third parties - business travel"
    },
    {
     flow: "IN", 
     code: "261", 
     section: "Intellectual property and other services", 
     description: "Proceeds for travel services in respect of third parties - holiday travel"
    },
    {
     flow: "IN", 
     code: "265", 
     section: "Intellectual property and other services", 
     description: "Proceeds for telecommunication services"
    },
    {
     flow: "IN", 
     code: "266", 
     section: "Intellectual property and other services", 
     description: "Proceeds for information services including data, news related and news agency fees"
    },
    {
     flow: "IN", 
     code: "270/01", 
     section: "Intellectual property and other services", 
     description: "Proceeds for passenger services - road"
    },
    {
     flow: "IN", 
     code: "270/02", 
     section: "Intellectual property and other services", 
     description: "Proceeds for passenger services - rail"
    },
    {
     flow: "IN", 
     code: "270/03", 
     section: "Intellectual property and other services", 
     description: "Proceeds for passenger services - sea"
    },
    {
     flow: "IN", 
     code: "270/04", 
     section: "Intellectual property and other services", 
     description: "Proceeds for passenger services - air"
    },
    {
     flow: "IN", 
     code: "271/01", 
     section: "Intellectual property and other services", 
     description: "Proceeds for freight services - road"
    },
    {
     flow: "IN", 
     code: "271/02", 
     section: "Intellectual property and other services", 
     description: "Proceeds for freight services - rail"
    },
    {
     flow: "IN", 
     code: "271/03", 
     section: "Intellectual property and other services", 
     description: "Proceeds for freight services - sea"
    },
    {
     flow: "IN", 
     code: "271/04", 
     section: "Intellectual property and other services", 
     description: "Proceeds for freight services - air"
    },
    {
     flow: "IN", 
     code: "272/01", 
     section: "Intellectual property and other services", 
     description: "Proceeds for other transport services - road"
    },
    {
     flow: "IN", 
     code: "272/02", 
     section: "Intellectual property and other services", 
     description: "Proceeds for other transport services - rail"
    },
    {
     flow: "IN", 
     code: "272/03", 
     section: "Intellectual property and other services", 
     description: "Proceeds for other transport services - sea"
    },
    {
     flow: "IN", 
     code: "272/04", 
     section: "Intellectual property and other services", 
     description: "Proceeds for other transport services - air"
    },
    {
     flow: "IN", 
     code: "273/01", 
     section: "Intellectual property and other services", 
     description: "Proceeds for postal and courier services - road"
    },
    {
     flow: "IN", 
     code: "273/02", 
     section: "Intellectual property and other services", 
     description: "Proceeds for postal and courier services - rail"
    },
    {
     flow: "IN", 
     code: "273/03", 
     section: "Intellectual property and other services", 
     description: "Proceeds for postal and courier services - sea"
    },
    {
     flow: "IN", 
     code: "273/04", 
     section: "Intellectual property and other services", 
     description: "Proceeds for postal and courier services - air"
    },
    {
     flow: "IN", 
     code: "275", 
     section: "Intellectual property and other services", 
     description: "Commission and fees"
    },
    {
     flow: "IN", 
     code: "276", 
     section: "Intellectual property and other services", 
     description: "Proceeds for financial services charged for advice provided"
    },
    {
     flow: "IN", 
     code: "280", 
     section: "Intellectual property and other services", 
     description: "Proceeds for construction services"
    },
    {
     flow: "IN", 
     code: "281", 
     section: "Intellectual property and other services", 
     description: "Proceeds for government services"
    },
    {
     flow: "IN", 
     code: "282", 
     section: "Intellectual property and other services", 
     description: "Diplomatic transfers"
    },
    {
     flow: "IN", 
     code: "285", 
     section: "Intellectual property and other services", 
     description: "Tuition fees"
    },
    {
     flow: "IN", 
     code: "287", 
     section: "Intellectual property and other services", 
     description: "Proceeds for legal services"
    },
    {
     flow: "IN", 
     code: "288", 
     section: "Intellectual property and other services", 
     description: "Proceeds for accounting services"
    },
    {
     flow: "IN", 
     code: "289", 
     section: "Intellectual property and other services", 
     description: "Proceeds for management consulting services"
    },
    {
     flow: "IN", 
     code: "290", 
     section: "Intellectual property and other services", 
     description: "Proceeds for public relation services"
    },
    {
     flow: "IN", 
     code: "291", 
     section: "Intellectual property and other services", 
     description: "Proceeds for advertising & market research services"
    },
    {
     flow: "IN", 
     code: "292", 
     section: "Intellectual property and other services", 
     description: "Proceeds for managerial services"
    },
    {
     flow: "IN", 
     code: "293", 
     section: "Intellectual property and other services", 
     description: "Proceeds for medical and dental services"
    },
    {
     flow: "IN", 
     code: "294", 
     section: "Intellectual property and other services", 
     description: "Proceeds for educational services"
    },
    {
     flow: "IN", 
     code: "295", 
     section: "Intellectual property and other services", 
     description: "Operational leasing"
    },
    {
     flow: "IN", 
     code: "296", 
     section: "Intellectual property and other services", 
     description: "Proceeds for cultural and recreational services"
    },
    {
     flow: "IN", 
     code: "297", 
     section: "Intellectual property and other services", 
     description: "Proceeds for other business services not included elsewhere"
    },
    {
     flow: "IN", 
     code: "300", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Adjustments / Reversals / Refunds related to income and yields on financial assets"
    },
    {
     flow: "IN", 
     code: "301", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Dividends"
    },
    {
     flow: "IN", 
     code: "302", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Branch profits"
    },
    {
     flow: "IN", 
     code: "303", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Compensation paid by a non-resident to a resident employee temporarily abroad (excluding remittances)"
    },
    {
     flow: "IN", 
     code: "304", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Compensation paid by a non-resident to a non-resident employee in Namibia (excluding remittances)"
    },
    {
     flow: "IN", 
     code: "305", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Compensation paid by a non-resident to a migrant worker employee (excluding remittances)"
    },
    {
     flow: "IN", 
     code: "306", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Compensation paid by a non-resident to a foreign national contract worker employee (excluding remittances)"
    },
    {
     flow: "IN", 
     code: "307", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Commission or brokerage"
    },
    {
     flow: "IN", 
     code: "308", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Rental"
    },
    {
     flow: "IN", 
     code: "309/01", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Interest received from a resident temporarily abroad in respect of loans"
    },
    {
     flow: "IN", 
     code: "309/02", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Interest received from a non-resident in respect of individual loans"
    },
    {
     flow: "IN", 
     code: "309/03", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Interest received from a non-resident in respect of study loans"
    },
    {
     flow: "IN", 
     code: "309/04", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Interest received from a non-resident in respect of shareholders\u2019 loans"
    },
    {
     flow: "IN", 
     code: "309/05", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Interest received from a non-resident in respect of third party loans"
    },
    {
     flow: "IN", 
     code: "309/06", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Interest received from a non-resident in respect of trade finance loans"
    },
    {
     flow: "IN", 
     code: "309/07", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Interest received from a non-resident in respect of a bond"
    },
    {
     flow: "IN", 
     code: "309/08", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Interest received not in respect of loans"
    },
    {
     flow: "IN", 
     code: "312/01", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Income in respect of inward listed securities derivatives individual"
    },
    {
     flow: "IN", 
     code: "312/02", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Income in respect of inward listed securities derivatives corporate"
    },
    {
     flow: "IN", 
     code: "312/03", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Income in respect of inward listed securities derivatives bank"
    },
    {
     flow: "IN", 
     code: "312/04", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Income in respect of inward listed securities derivatives institution"
    },
    {
     flow: "IN", 
     code: "313", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Income earned abroad by a resident on an individual investment"
    },
    {
     flow: "IN", 
     code: "400", 
     section: "Transfers of a Current Nature", 
     description: "Adjustments / Reversals / Refunds related to transfers of a current nature"
    },
    {
     flow: "IN", 
     code: "401", 
     section: "Transfers of a Current Nature", 
     description: "Gifts"
    },
    {
     flow: "IN", 
     code: "402", 
     section: "Transfers of a Current Nature", 
     description: "Annual contributions"
    },
    {
     flow: "IN", 
     code: "403", 
     section: "Transfers of a Current Nature", 
     description: "Contributions in respect of social security schemes"
    },
    {
     flow: "IN", 
     code: "404", 
     section: "Transfers of a Current Nature", 
     description: "Contributions in respect of charitable, religious and cultural (excluding research and development)"
    },
    {
     flow: "IN", 
     code: "405", 
     section: "Transfers of a Current Nature", 
     description: "Other donations / aid to Government (excluding research and development)"
    },
    {
     flow: "IN", 
     code: "406", 
     section: "Transfers of a Current Nature", 
     description: "Other donations / aid to private sector (excluding research and development)"
    },
    {
     flow: "IN", 
     code: "407", 
     section: "Transfers of a Current Nature", 
     description: "Pensions"
    },
    {
     flow: "IN", 
     code: "408", 
     section: "Transfers of a Current Nature", 
     description: "Annuities (pension related)"
    },
    {
     flow: "IN", 
     code: "409", 
     section: "Transfers of a Current Nature", 
     description: "Inheritances"
    },
    {
     flow: "IN", 
     code: "410", 
     section: "Transfers of a Current Nature", 
     description: "Alimony"
    },
    {
     flow: "IN", 
     code: "411/01", 
     section: "Transfers of a Current Nature", 
     description: "Tax - Income tax"
    },
    {
     flow: "IN", 
     code: "411/02", 
     section: "Transfers of a Current Nature", 
     description: "Tax - VAT refunds"
    },
    {
     flow: "IN", 
     code: "411/03", 
     section: "Transfers of a Current Nature", 
     description: "Tax - Other"
    },
    {
     flow: "IN", 
     code: "412", 
     section: "Transfers of a Current Nature", 
     description: "Insurance premiums (non-life/short term)"
    },
    {
     flow: "IN", 
     code: "413", 
     section: "Transfers of a Current Nature", 
     description: "Insurance claims (non-life/short term)"
    },
    {
     flow: "IN", 
     code: "414", 
     section: "Transfers of a Current Nature", 
     description: "Insurance premiums (life)"
    },
    {
     flow: "IN", 
     code: "415", 
     section: "Transfers of a Current Nature", 
     description: "Insurance claims (life)"
    },
    {
     flow: "IN", 
     code: "416", 
     section: "Transfers of a Current Nature", 
     description: "Migrant worker remittances (excluding compensation)"
    },
    {
     flow: "IN", 
     code: "417", 
     section: "Transfers of a Current Nature", 
     description: "Foreign national contract worker remittances (excluding compensation)"
    },
    {
     flow: "IN", 
     code: "500", 
     section: "Transfers of a Capital Nature", 
     description: "Adjustments / Reversals / Refunds related to capital transfers and immigrants"
    },
    {
     flow: "IN", 
     code: "501", 
     section: "Transfers of a Capital Nature", 
     description: "Donations to the Namibian Government for fixed assets"
    },
    {
     flow: "IN", 
     code: "502", 
     section: "Transfers of a Capital Nature", 
     description: "Donations to corporate entities - fixed assets"
    },
    {
     flow: "IN", 
     code: "503", 
     section: "Transfers of a Capital Nature", 
     description: "Investment into property by a non-resident corporate entity"
    },
    {
     flow: "IN", 
     code: "504", 
     section: "Transfers of a Capital Nature", 
     description: "Disinvestment of property by a resident corporate entity"
    },
    {
     flow: "IN", 
     code: "510/01", 
     section: "Transfers of a Capital Nature", 
     description: "Investment into property by a non-resident individual"
    },
    {
     flow: "IN", 
     code: "510/02", 
     section: "Transfers of a Capital Nature", 
     description: "Investment by a non-resident individual - other"
    },
    {
     flow: "IN", 
     code: "511/01", 
     section: "Transfers of a Capital Nature", 
     description: "Disinvestment of capital by a resident individual - Shares"
    },
    {
     flow: "IN", 
     code: "511/02", 
     section: "Transfers of a Capital Nature", 
     description: "Disinvestment of capital by a resident individual - Bonds"
    },
    {
     flow: "IN", 
     code: "511/03", 
     section: "Transfers of a Capital Nature", 
     description: "Disinvestment of capital by a resident individual - Money market instruments"
    },
    {
     flow: "IN", 
     code: "511/04", 
     section: "Transfers of a Capital Nature", 
     description: "Disinvestment of capital by a resident individual - Deposits with a foreign bank"
    },
    {
     flow: "IN", 
     code: "511/05", 
     section: "Transfers of a Capital Nature", 
     description: "Disinvestment of capital by a resident individual - Mutual funds / collective investment schemes"
    },
    {
     flow: "IN", 
     code: "511/06", 
     section: "Transfers of a Capital Nature", 
     description: "Disinvestment of capital by a resident individual - Property"
    },
    {
     flow: "IN", 
     code: "511/07", 
     section: "Transfers of a Capital Nature", 
     description: "Disinvestment of capital by a resident individual - Other"
    },
    {
      flow       : 'IN',
      code       : '517',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers by Namibian resident individuals',
      description: 'Repatriation, on instruction by the Exchange Control Department, of a foreign investment by a resident individual originating from an account conducted in foreign currency held at an Authorised Dealer in Namibia.',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Repatriation', 'Financial Surveillance Department', 'Instruction', 'foreign investment', 'fixed assets', 'capital transfers', 'immigrants', 'resident', 'individual', 'foreign currency', 'authorised dealer in south africa']
    },
    {
     flow: "IN", 
     code: "530/01", 
     section: "Transfers of a Capital Nature", 
     description: "Immigration foreign capital allowance - fixed property"
    },
    {
     flow: "IN", 
     code: "600", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Adjustments / Reversals / Refunds related to financial investments/disinvestments and prudential investments"
    },
    {
     flow: "IN", 
     code: "601/01", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment in listed shares by a non-resident"
    },
    {
     flow: "IN", 
     code: "601/02", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment in non-listed shares by a non-resident"
    },
    {
     flow: "IN", 
     code: "602", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment into money market instruments by a non-resident"
    },
    {
     flow: "IN", 
     code: "603/01", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment into listed bonds by a non-resident (excluding loans)"
    },
    {
     flow: "IN", 
     code: "603/02", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment into non-listed bonds by a non-resident (excluding loans"
    },
    {
     flow: "IN", 
     code: "605/01", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment of shares by resident - Agriculture, hunting, forestry and fishing"
    },
    {
     flow: "IN", 
     code: "605/02", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment of shares by resident - Mining, quarrying and exploration"
    },
    {
     flow: "IN", 
     code: "605/03", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment of shares by resident - Manufacturing"
    },
    {
     flow: "IN", 
     code: "605/04", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment of shares by resident - Electricity, gas and water supply"
    },
    {
     flow: "IN", 
     code: "605/05", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment of shares by resident - Construction"
    },
    {
     flow: "IN", 
     code: "605/06", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment of shares by resident - Wholesale, retail, repairs, hotel and restaurants"
    },
    {
     flow: "IN", 
     code: "605/07", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment of shares by resident - Transport and communication"
    },
    {
     flow: "IN", 
     code: "605/08", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment of shares by resident - Financial services"
    },
    {
     flow: "IN", 
     code: "605/09", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment of shares by resident - Community, social and personal services"
    },
    {
     flow: "IN", 
     code: "610/01", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities equity individual buy back"
    },
    {
     flow: "IN", 
     code: "610/02", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities equity corporate buy back"
    },
    {
     flow: "IN", 
     code: "610/03", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities equity bank buy back"
    },
    {
     flow: "IN", 
     code: "610/04", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities equity institution buy back"
    },
    {
     flow: "IN", 
     code: "611/01", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities debt individual redemption"
    },
    {
     flow: "IN", 
     code: "611/02", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities debt corporate redemption"
    },
    {
     flow: "IN", 
     code: "611/03", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities debt bank redemption"
    },
    {
     flow: "IN", 
     code: "611/04", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities debt institution redemption"
    },
    {
     flow: "IN", 
     code: "612/01", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities derivatives individual proceeds"
    },
    {
     flow: "IN", 
     code: "612/02", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities derivatives corporate proceeds"
    },
    {
     flow: "IN", 
     code: "612/03", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities derivatives bank proceeds"
    },
    {
     flow: "IN", 
     code: "612/04", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities derivatives institution proceeds"
    },
    {
     flow: "IN", 
     code: "615/01", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment by resident institutional investor - Asset Manager"
    },
    {
     flow: "IN", 
     code: "615/02", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment by resident institutional investor - Collective Investment Scheme"
    },
    {
     flow: "IN", 
     code: "615/03", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment by resident institutional investor - Retirement Fund"
    },
    {
     flow: "IN", 
     code: "615/04", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment by resident institutional investor - Life Linked"
    },
    {
     flow: "IN", 
     code: "615/05", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment by resident institutional investor - Life Non Linked"
    },
    {
     flow: "IN", 
     code: "616", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Bank prudential disinvestment"
    },
    {
     flow: "IN", 
     code: "700", 
     section: "Derivatives", 
     description: "Adjustments / Reversals / Refunds related to derivatives"
    },
    {
     flow: "IN", 
     code: "701/01", 
     section: "Derivatives", 
     description: "Options - listed"
    },
    {
     flow: "IN", 
     code: "701/02", 
     section: "Derivatives", 
     description: "Options - unlisted"
    },
    {
     flow: "IN", 
     code: "702/01", 
     section: "Derivatives", 
     description: "Futures - listed"
    },
    {
     flow: "IN", 
     code: "702/02", 
     section: "Derivatives", 
     description: "Futures - unlisted"
    },
    {
     flow: "IN", 
     code: "703/01", 
     section: "Derivatives", 
     description: "Warrants - listed"
    },
    {
     flow: "IN", 
     code: "703/02", 
     section: "Derivatives", 
     description: "Warrants - unlisted"
    },
    {
     flow: "IN", 
     code: "704/01", 
     section: "Derivatives", 
     description: "Gold hedging - listed"
    },
    {
     flow: "IN", 
     code: "704/02", 
     section: "Derivatives", 
     description: "Gold hedging - unlisted"
    },
    {
     flow: "IN", 
     code: "705/01", 
     section: "Derivatives", 
     description: "Derivative not specified above - listed"
    },
    {
     flow: "IN", 
     code: "705/02", 
     section: "Derivatives", 
     description: "Derivative not specified above - unlisted"
    },
    {
     flow: "IN", 
     code: "800", 
     section: "Loan and Miscellaneous payments", 
     description: "Adjustments / Reversals / Refunds related to loan and miscellaneous payments"
    },
    {
     flow: "IN", 
     code: "801", 
     section: "Loan and Miscellaneous payments", 
     description: "Trade finance loan drawn down in Namibia"
    },
    {
     flow: "IN", 
     code: "802", 
     section: "Loan and Miscellaneous payments", 
     description: "International Bond drawn down"
    },
    {
     flow: "IN", 
     code: "803", 
     section: "Loan and Miscellaneous payments", 
     description: "Loan made to a resident by a non-resident shareholder"
    },
    {
     flow: "IN", 
     code: "804", 
     section: "Loan and Miscellaneous payments", 
     description: "Loan made to a resident by a non-resident third party"
    },
    {
     flow: "IN", 
     code: "810", 
     section: "Loan and Miscellaneous payments", 
     description: "Repayment by a resident temporarily abroad of a loan granted by a resident"
    },
    {
     flow: "IN", 
     code: "815", 
     section: "Loan and Miscellaneous payments", 
     description: "Repayment of an individual loan to a resident"
    },
    {
     flow: "IN", 
     code: "816", 
     section: "Loan and Miscellaneous payments", 
     description: "Repayment of a study loan to a resident"
    },
    {
     flow: "IN", 
     code: "817", 
     section: "Loan and Miscellaneous payments", 
     description: "Repayment of a shareholders loan to a resident"
    },
    {
     flow: "IN", 
     code: "818", 
     section: "Loan and Miscellaneous payments", 
     description: "Repayment of a third party loan to a resident (excluding shareholders)"
    },
    {
     flow: "IN", 
     code: "819", 
     section: "Loan and Miscellaneous payments", 
     description: "Repayment of a trade finance loan to a resident"
    },
    {
     flow: "IN", 
     code: "830", 
     section: "Loan and Miscellaneous payments", 
     description: "Details of payments not classified"
    },
    {
     flow: "IN", 
     code: "833", 
     section: "Loan and Miscellaneous payments", 
     description: "Credit/Debit card company settlement as well as money remitter settlements"
    },
    {
     flow: "IN", 
     code: "ZZ1", 
     section: "NoN-Reportable Transactions", 
     description: "Non reportable"
    },
    {
     flow: "OUT", 
     code: "100", 
     section: "Merchandise", 
     description: "Adjustments / Reversals / Refunds applicable to merchandise"
    },
    {
     flow: "OUT", 
     code: "101/01", 
     section: "Merchandise", 
     description: "Import advance payment (excluding capital goods, gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal, iron ore and goods imported via the Namibian Post Office)"
    },
    {
     flow: "OUT", 
     code: "101/02", 
     section: "Merchandise", 
     description: "Import advance payment - capital goods"
    },
    {
     flow: "OUT", 
     code: "101/03", 
     section: "Merchandise", 
     description: "Import advance payment - gold"
    },
    {
     flow: "OUT", 
     code: "101/04", 
     section: "Merchandise", 
     description: "Import advance payment - platinum"
    },
    {
     flow: "OUT", 
     code: "101/05", 
     section: "Merchandise", 
     description: "Import advance payment - crude oil"
    },
    {
     flow: "OUT", 
     code: "101/06", 
     section: "Merchandise", 
     description: "Import advance payment - refined petroleum products"
    },
    {
     flow: "OUT", 
     code: "101/07", 
     section: "Merchandise", 
     description: "Import advance payment - diamonds"
    },
    {
     flow: "OUT", 
     code: "101/08", 
     section: "Merchandise", 
     description: "Import advance payment - steel"
    },
    {
     flow: "OUT", 
     code: "101/09", 
     section: "Merchandise", 
     description: "Import advance payment - coal"
    },
    {
     flow: "OUT", 
     code: "101/10", 
     section: "Merchandise", 
     description: "Import advance payment - iron ore"
    },
    {
     flow: "OUT", 
     code: "101/11", 
     section: "Merchandise", 
     description: "Import advance payment - goods imported via the Namibian Post Office"
    },
    {
     flow: "OUT", 
     code: "103/01", 
     section: "Merchandise", 
     description: "Import payment (excluding capital goods, gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal, iron ore and goods imported via the Namibian Post Office)"
    },
    {
     flow: "OUT", 
     code: "103/02", 
     section: "Merchandise", 
     description: "Import payment - capital goods"
    },
    {
     flow: "OUT", 
     code: "103/03", 
     section: "Merchandise", 
     description: "Import payment - gold"
    },
    {
     flow: "OUT", 
     code: "103/04", 
     section: "Merchandise", 
     description: "Import payment - platinum"
    },
    {
     flow: "OUT", 
     code: "103/05", 
     section: "Merchandise", 
     description: "Import payment - crude oil"
    },
    {
     flow: "OUT", 
     code: "103/06", 
     section: "Merchandise", 
     description: "Import payment - refined petroleum products"
    },
    {
     flow: "OUT", 
     code: "103/07", 
     section: "Merchandise", 
     description: "Import payment - diamonds"
    },
    {
     flow: "OUT", 
     code: "103/08", 
     section: "Merchandise", 
     description: "Import payment - steel"
    },
    {
     flow: "OUT", 
     code: "103/09", 
     section: "Merchandise", 
     description: "Import payment - coal"
    },
    {
     flow: "OUT", 
     code: "103/10", 
     section: "Merchandise", 
     description: "Import payment - iron ore"
    },
    {
     flow: "OUT", 
     code: "103/11", 
     section: "Merchandise", 
     description: "Import payment - goods imported via the Namibian Post Office"
    },
    {
     flow: "OUT", 
     code: "104/01", 
     section: "Merchandise", 
     description: "Import payment - (excluding capital goods, gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal, iron ore and goods imported via the Namibian post office)"
    },
    {
     flow: "OUT", 
     code: "104/02", 
     section: "Merchandise", 
     description: "Import payment - capital goods"
    },
    {
     flow: "OUT", 
     code: "104/03", 
     section: "Merchandise", 
     description: "Import payment - gold"
    },
    {
     flow: "OUT", 
     code: "104/04", 
     section: "Merchandise", 
     description: "Import payment - platinum"
    },
    {
     flow: "OUT", 
     code: "104/05", 
     section: "Merchandise", 
     description: "Import payment - crude oil"
    },
    {
     flow: "OUT", 
     code: "104/06", 
     section: "Merchandise", 
     description: "Import payment - refined petroleum products"
    },
    {
     flow: "OUT", 
     code: "104/07", 
     section: "Merchandise", 
     description: "Import payment - diamonds"
    },
    {
     flow: "OUT", 
     code: "104/08", 
     section: "Merchandise", 
     description: "Import payment - steel"
    },
    {
     flow: "OUT", 
     code: "104/09", 
     section: "Merchandise", 
     description: "Import payment - coal"
    },
    {
     flow: "OUT", 
     code: "104/10", 
     section: "Merchandise", 
     description: "Import payment - iron ore"
    },
    {
     flow: "OUT", 
     code: "104/11", 
     section: "Merchandise", 
     description: "Import payment - goods imported via the Namibian Post Office"
    },
    {
     flow: "OUT", 
     code: "105", 
     section: "Merchandise", 
     description: "Consumables acquired in port"
    },
    {
     flow: "OUT", 
     code: "106", 
     section: "Merchandise", 
     description: "Repayment of trade finance for imports"
    },
    {
     flow: "OUT", 
     code: "107", 
     section: "Merchandise", 
     description: "Import payments where the Customs value of the shipment is less than NAD 500.00."
    },
    {
     flow: "OUT", 
     code: "108", 
     section: "Merchandise", 
     description: "Import payments where goods were declared as part of passenger baggage and no MRN is available"
    },
    {
     flow: "OUT", 
     code: "109/01", 
     section: "Merchandise", 
     description: "Goods purchased from non-residents where no physical import will take place, excluding gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal, iron ore, merchanting transactions"
    },
    {
     flow: "OUT", 
     code: "109/02", 
     section: "Merchandise", 
     description: "Payments for gold purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
    },
    {
     flow: "OUT", 
     code: "109/03", 
     section: "Merchandise", 
     description: "Payments for platinum purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
    },
    {
     flow: "OUT", 
     code: "109/04", 
     section: "Merchandise", 
     description: "Payments for crude oil purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
    },
    {
     flow: "OUT", 
     code: "109/05", 
     section: "Merchandise", 
     description: "Payments for refined petroleum products purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
    },
    {
     flow: "OUT", 
     code: "109/06", 
     section: "Merchandise", 
     description: "Payments for diamonds purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
    },
    {
     flow: "OUT", 
     code: "109/07", 
     section: "Merchandise", 
     description: "Payments for steel purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
    },
    {
     flow: "OUT", 
     code: "109/08", 
     section: "Merchandise", 
     description: "Payments for coal purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
    },
    {
     flow: "OUT", 
     code: "109/09", 
     section: "Merchandise", 
     description: "Payments for iron ore purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
    },
    {
     flow: "OUT", 
     code: "110", 
     section: "Merchandise", 
     description: "Merchanting transaction"
    },
    {
     flow: "OUT", 
     code: "200", 
     section: "Intellectual property and other services", 
     description: "Adjustments / Reversals / Refunds applicable to intellectual property and service related items"
    },
    {
     flow: "OUT", 
     code: "201", 
     section: "Intellectual property and other services", 
     description: "Rights obtained for licences to reproduce and/or distribute"
    },
    {
     flow: "OUT", 
     code: "202", 
     section: "Intellectual property and other services", 
     description: "Rights obtained for using patents and inventions (licensing)"
    },
    {
     flow: "OUT", 
     code: "203", 
     section: "Intellectual property and other services", 
     description: "Rights obtained for using patterns and designs (including industrial processes)"
    },
    {
     flow: "OUT", 
     code: "204", 
     section: "Intellectual property and other services", 
     description: "Rights obtained for using copyrights"
    },
    {
     flow: "OUT", 
     code: "205", 
     section: "Intellectual property and other services", 
     description: "Rights obtained for using franchises and trademarks"
    },
    {
     flow: "OUT", 
     code: "210", 
     section: "Intellectual property and other services", 
     description: "Acquisition of patents and inventions"
    },
    {
     flow: "OUT", 
     code: "211", 
     section: "Intellectual property and other services", 
     description: "Acquisition of patterns and designs (including industrial processes)"
    },
    {
     flow: "OUT", 
     code: "212", 
     section: "Intellectual property and other services", 
     description: "Acquisition of copyrights"
    },
    {
     flow: "OUT", 
     code: "213", 
     section: "Intellectual property and other services", 
     description: "Acquisition of franchises and trademarks"
    },
    {
     flow: "OUT", 
     code: "220", 
     section: "Intellectual property and other services", 
     description: "Payments for research and development services"
    },
    {
     flow: "OUT", 
     code: "221", 
     section: "Intellectual property and other services", 
     description: "Funding for research and development"
    },
    {
     flow: "OUT", 
     code: "225", 
     section: "Intellectual property and other services", 
     description: "Acquisition of original manuscripts, sound recordings and films"
    },
    {
     flow: "OUT", 
     code: "226", 
     section: "Intellectual property and other services", 
     description: "Payment relating to the production of motion pictures, radio and television programs and musical recordings"
    },
    {
     flow: "OUT", 
     code: "230", 
     section: "Intellectual property and other services", 
     description: "The outright purchasing of ownership rights of software"
    },
    {
     flow: "OUT", 
     code: "231", 
     section: "Intellectual property and other services", 
     description: "Computer-related services including maintenance, repair and consultancy"
    },
    {
     flow: "OUT", 
     code: "232", 
     section: "Intellectual property and other services", 
     description: "Commercial purchases of customised softwareand related licenses to use"
    },
    {
     flow: "OUT", 
     code: "233", 
     section: "Intellectual property and other services", 
     description: "Commercial purchases of non-customised software on physical media with periodic licence to use"
    },
    {
     flow: "OUT", 
     code: "234", 
     section: "Intellectual property and other services", 
     description: "Commercial purchases of non-customised software provided on physical media with right to perpetual (ongoing) use"
    },
    {
     flow: "OUT", 
     code: "235", 
     section: "Intellectual property and other services", 
     description: "Commercial purchases of non-customised software downloaded or electronically acquired with periodic license"
    },
    {
     flow: "OUT", 
     code: "236", 
     section: "Intellectual property and other services", 
     description: "Commercial purchases of non-customised software downloaded or electronically acquired with single payment"
    },
    {
     flow: "OUT", 
     code: "240/01", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on materials (excluding gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal and iron ore)"
    },
    {
     flow: "OUT", 
     code: "240/02", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on gold"
    },
    {
     flow: "OUT", 
     code: "240/03", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on platinum"
    },
    {
     flow: "OUT", 
     code: "240/04", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on crude oil"
    },
    {
     flow: "OUT", 
     code: "240/05", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on refined petroleum products"
    },
    {
     flow: "OUT", 
     code: "240/06", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on diamonds"
    },
    {
     flow: "OUT", 
     code: "240/07", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on steel"
    },
    {
     flow: "OUT", 
     code: "240/08", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on coal"
    },
    {
     flow: "OUT", 
     code: "240/09", 
     section: "Intellectual property and other services", 
     description: "Fees for processing - processing done on iron ore"
    },
    {
     flow: "OUT", 
     code: "241", 
     section: "Intellectual property and other services", 
     description: "Repairs and maintenance onmachinery and equipment"
    },
    {
     flow: "OUT", 
     code: "242", 
     section: "Intellectual property and other services", 
     description: "Architectural, engineering and other technical services"
    },
    {
     flow: "OUT", 
     code: "243", 
     section: "Intellectual property and other services", 
     description: "Agricultural, mining, waste treatment and depollution services"
    },
    {
     flow: "OUT", 
     code: "250", 
     section: "Intellectual property and other services", 
     description: "Travel services for non-residents - business travel"
    },
    {
     flow: "OUT", 
     code: "251", 
     section: "Intellectual property and other services", 
     description: "Travel services for non-residents - holiday travel"
    },
    {
     flow: "OUT", 
     code: "255", 
     section: "Intellectual property and other services", 
     description: "Travel services for residents - business travel"
    },
    {
     flow: "OUT", 
     code: "256", 
     section: "Intellectual property and other services", 
     description: "Travel services for residents - holiday travel"
    },
    {
     flow: "OUT", 
     code: "260", 
     section: "Intellectual property and other services", 
     description: "Payment for travel services in respect of third parties - business travel"
    },
    {
     flow: "OUT", 
     code: "261", 
     section: "Intellectual property and other services", 
     description: "Payment for travel services in respect of third parties - holiday travel"
    },
    {
     flow: "OUT", 
     code: "265", 
     section: "Intellectual property and other services", 
     description: "Payment for telecommunication services"
    },
    {
     flow: "OUT", 
     code: "266", 
     section: "Intellectual property and other services", 
     description: "Payment for information services including data, news related and news agency fees"
    },
    {
     flow: "OUT", 
     code: "270/01", 
     section: "Intellectual property and other services", 
     description: "Payment for passenger services - road"
    },
    {
     flow: "OUT", 
     code: "270/02", 
     section: "Intellectual property and other services", 
     description: "Payment for passenger services - rail"
    },
    {
     flow: "OUT", 
     code: "270/03", 
     section: "Intellectual property and other services", 
     description: "Payment for passenger services - sea"
    },
    {
     flow: "OUT", 
     code: "270/04", 
     section: "Intellectual property and other services", 
     description: "Payment for passenger services - air"
    },
    {
     flow: "OUT", 
     code: "271/01", 
     section: "Intellectual property and other services", 
     description: "Payment for freight services - road"
    },
    {
     flow: "OUT", 
     code: "271/02", 
     section: "Intellectual property and other services", 
     description: "Payment for freight services - rail"
    },
    {
     flow: "OUT", 
     code: "271/03", 
     section: "Intellectual property and other services", 
     description: "Payment for freight services - sea"
    },
    {
     flow: "OUT", 
     code: "271/04", 
     section: "Intellectual property and other services", 
     description: "Payment for freight services - air"
    },
    {
     flow: "OUT", 
     code: "272/01", 
     section: "Intellectual property and other services", 
     description: "Payment for other transport services - road"
    },
    {
     flow: "OUT", 
     code: "272/02", 
     section: "Intellectual property and other services", 
     description: "Payment for other transport services - rail"
    },
    {
     flow: "OUT", 
     code: "272/03", 
     section: "Intellectual property and other services", 
     description: "Payment for other transport services - sea"
    },
    {
     flow: "OUT", 
     code: "272/04", 
     section: "Intellectual property and other services", 
     description: "Payment for other transport services - air"
    },
    {
     flow: "OUT", 
     code: "273/01", 
     section: "Intellectual property and other services", 
     description: "Payment for postal and courier services - road"
    },
    {
     flow: "OUT", 
     code: "273/02", 
     section: "Intellectual property and other services", 
     description: "Payment for postal and courier services - rail"
    },
    {
     flow: "OUT", 
     code: "273/03", 
     section: "Intellectual property and other services", 
     description: "Payment for postal and courier services - sea"
    },
    {
     flow: "OUT", 
     code: "273/04", 
     section: "Intellectual property and other services", 
     description: "Payment for postal and courier services - air"
    },
    {
     flow: "OUT", 
     code: "275", 
     section: "Intellectual property and other services", 
     description: "Commission and fees"
    },
    {
     flow: "OUT", 
     code: "276", 
     section: "Intellectual property and other services", 
     description: "Financial service fees charged for advice provided"
    },
    {
     flow: "OUT", 
     code: "280", 
     section: "Intellectual property and other services", 
     description: "Payment for construction services"
    },
    {
     flow: "OUT", 
     code: "281", 
     section: "Intellectual property and other services", 
     description: "Payment for government services"
    },
    {
     flow: "OUT", 
     code: "282", 
     section: "Intellectual property and other services", 
     description: "Diplomatic transfers"
    },
    {
     flow: "OUT", 
     code: "285", 
     section: "Intellectual property and other services", 
     description: "Tuition fees"
    },
    {
     flow: "OUT", 
     code: "287", 
     section: "Intellectual property and other services", 
     description: "Payment for legal services"
    },
    {
     flow: "OUT", 
     code: "288", 
     section: "Intellectual property and other services", 
     description: "Payment for accounting services"
    },
    {
     flow: "OUT", 
     code: "289", 
     section: "Intellectual property and other services", 
     description: "Payment for management consulting services"
    },
    {
     flow: "OUT", 
     code: "290", 
     section: "Intellectual property and other services", 
     description: "Payment for public relation services"
    },
    {
     flow: "OUT", 
     code: "291", 
     section: "Intellectual property and other services", 
     description: "Payment for advertising & market research services"
    },
    {
     flow: "OUT", 
     code: "292", 
     section: "Intellectual property and other services", 
     description: "Payment for managerial services"
    },
    {
     flow: "OUT", 
     code: "293", 
     section: "Intellectual property and other services", 
     description: "Payment for medical and dental services"
    },
    {
     flow: "OUT", 
     code: "294", 
     section: "Intellectual property and other services", 
     description: "Payment for educational services"
    },
    {
     flow: "OUT", 
     code: "295", 
     section: "Intellectual property and other services", 
     description: "Operational leasing"
    },
    {
     flow: "OUT", 
     code: "296", 
     section: "Intellectual property and other services", 
     description: "Payment for cultural and recreational services"
    },
    {
     flow: "OUT", 
     code: "297", 
     section: "Intellectual property and other services", 
     description: "Payment for other business services not included elsewhere"
    },
    {
     flow: "OUT", 
     code: "300", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Adjustments / Reversals / Refunds related to income and yields on financial assets"
    },
    {
     flow: "OUT", 
     code: "301", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Dividends"
    },
    {
     flow: "OUT", 
     code: "302", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Branch profits"
    },
    {
     flow: "OUT", 
     code: "303", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Compensation paid by a resident to a resident employee temporarily abroad (excluding remittances)"
    },
    {
     flow: "OUT", 
     code: "304", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Compensation paid by a resident to a non-resident employee (excluding remittances)"
    },
    {
     flow: "OUT", 
     code: "305", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Compensation paid by a resident to a migrant worker employee (excluding remittances)"
    },
    {
     flow: "OUT", 
     code: "306", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Compensation paid by a resident to a foreign national contract worker employee (excluding remittances)"
    },
    {
     flow: "OUT", 
     code: "307", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Commission or brokerage"
    },
    {
     flow: "OUT", 
     code: "308", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Rental"
    },
    {
     flow: "OUT", 
     code: "309/04", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Interest paid to a non-resident in respect of shareholders loans"
    },
    {
     flow: "OUT", 
     code: "309/05", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Interest paid to a non-resident in respect of third party loans"
    },
    {
     flow: "OUT", 
     code: "309/06", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Interest paid to a non-resident in respect of trade finance loans"
    },
    {
     flow: "OUT", 
     code: "309/07", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Interest paid to a non-resident in respect of a bond"
    },
    {
     flow: "OUT", 
     code: "309/08", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Interest paid not in respect of loans"
    },
    {
     flow: "OUT", 
     code: "312/01", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Fee in respect of inward listed securities derivatives individual"
    },
    {
     flow: "OUT", 
     code: "312/02", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Fee in respect of inward listed securities derivatives corporate"
    },
    {
     flow: "OUT", 
     code: "312/03", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Fee in respect of inward listed securities derivatives bank"
    },
    {
     flow: "OUT", 
     code: "312/04", 
     section: "Transactions relating to income and yields on financial assets", 
     description: "Fee in respect of inward listed securities derivatives institution"
    },
    {
     flow: "OUT", 
     code: "400", 
     section: "Transfers of a Current Nature", 
     description: "Adjustments / Reversals / Refunds related to transfers of a current nature"
    },
    {
     flow: "OUT", 
     code: "401", 
     section: "Transfers of a Current Nature", 
     description: "Gifts"
    },
    {
     flow: "OUT", 
     code: "402", 
     section: "Transfers of a Current Nature", 
     description: "Annual contributions"
    },
    {
     flow: "OUT", 
     code: "403", 
     section: "Transfers of a Current Nature", 
     description: "Contributions in respect of social security schemes"
    },
    {
     flow: "OUT", 
     code: "404", 
     section: "Transfers of a Current Nature", 
     description: "Contributions in respect of foreign charitable,religious and cultural (excluding research and development)"
    },
    {
     flow: "OUT", 
     code: "405", 
     section: "Transfers of a Current Nature", 
     description: "Other donations / aid to a foreign Government (excluding research and development)"
    },
    {
     flow: "OUT", 
     code: "406", 
     section: "Transfers of a Current Nature", 
     description: "Other donations / aid to a foreign private sector (excluding research and development)"
    },
    {
     flow: "OUT", 
     code: "407", 
     section: "Transfers of a Current Nature", 
     description: "Pensions"
    },
    {
     flow: "OUT", 
     code: "408", 
     section: "Transfers of a Current Nature", 
     description: "Annuities (pension related)"
    },
    {
     flow: "OUT", 
     code: "409", 
     section: "Transfers of a Current Nature", 
     description: "Inheritances"
    },
    {
     flow: "OUT", 
     code: "410", 
     section: "Transfers of a Current Nature", 
     description: "Alimony"
    },
    {
     flow: "OUT", 
     code: "411/01", 
     section: "Transfers of a Current Nature", 
     description: "Tax - Income tax"
    },
    {
     flow: "OUT", 
     code: "411/02", 
     section: "Transfers of a Current Nature", 
     description: "Tax - VAT refunds"
    },
    {
     flow: "OUT", 
     code: "411/03", 
     section: "Transfers of a Current Nature", 
     description: "Tax - Other"
    },
    {
     flow: "OUT", 
     code: "412", 
     section: "Transfers of a Current Nature", 
     description: "Insurance premiums (non-life/short term)"
    },
    {
     flow: "OUT", 
     code: "413", 
     section: "Transfers of a Current Nature", 
     description: "Insurance claims (non-life/short term)"
    },
    {
     flow: "OUT", 
     code: "414", 
     section: "Transfers of a Current Nature", 
     description: "Insurance premiums (life)"
    },
    {
     flow: "OUT", 
     code: "415", 
     section: "Transfers of a Current Nature", 
     description: "Insurance claims (life)"
    },
    {
     flow: "OUT", 
     code: "416", 
     section: "Transfers of a Current Nature", 
     description: "Migrant worker remittances (excluding compensation)"
    },
    {
     flow: "OUT", 
     code: "417", 
     section: "Transfers of a Current Nature", 
     description: "Foreign national contract worker remittances (excluding compensation)"
    },
    {
     flow: "OUT", 
     code: "500", 
     section: "Transfers of a Capital Nature", 
     description: "Adjustments / Reversals / Refunds related to capital transfers and emigrants"
    },
    {
     flow: "OUT", 
     code: "501", 
     section: "Transfers of a Capital Nature", 
     description: "Donations by the Namibian Government for fixed assets"
    },
    {
     flow: "OUT", 
     code: "502", 
     section: "Transfers of a Capital Nature", 
     description: "Donations by corporate entities for fixed assets"
    },
    {
     flow: "OUT", 
     code: "503", 
     section: "Transfers of a Capital Nature", 
     description: "Disinvestment of property by a non-resident corporate entity"
    },
    {
     flow: "OUT", 
     code: "504", 
     section: "Transfers of a Capital Nature", 
     description: "Investment into property by a resident corporate entity"
    },
    {
     flow: "OUT", 
     code: "510/01", 
     section: "Transfers of a Capital Nature", 
     description: "Disinvestment of property by a non-resident individual"
    },
    {
     flow: "OUT", 
     code: "510/02", 
     section: "Transfers of a Capital Nature", 
     description: "Disinvestment by a non-resident individual - other"
    },
    {
     flow: "OUT", 
     code: "511/01", 
     section: "Transfers of a Capital Nature", 
     description: "Investment by a resident individual not related to the investment allowance - Shares"
    },
    {
     flow: "OUT", 
     code: "511/02", 
     section: "Transfers of a Capital Nature", 
     description: "Investment by a resident individual not related to the investment allowance - Bonds"
    },
    {
     flow: "OUT", 
     code: "511/03", 
     section: "Transfers of a Capital Nature", 
     description: "Investment by a resident individual not related to the investment allowance - Money market instruments"
    },
    {
     flow: "OUT", 
     code: "511/04", 
     section: "Transfers of a Capital Nature", 
     description: "Investment by a resident individual not related to the investment allowance - Deposits with a foreign bank"
    },
    {
     flow: "OUT", 
     code: "511/05", 
     section: "Transfers of a Capital Nature", 
     description: "Investment by a resident individual not related to the investment allowance - Mutual funds / collective investment schemes"
    },
    {
     flow: "OUT", 
     code: "511/06", 
     section: "Transfers of a Capital Nature", 
     description: "Investment by a resident individual not related to the investment allowance - Property"
    },
    {
     flow: "OUT", 
     code: "511/07", 
     section: "Transfers of a Capital Nature", 
     description: "Investment by a resident individual not related to the investment allowance - Other"
    },
    {
     flow: "OUT", 
     code: "511/01", 
     section: "Transfers of a Capital Nature", 
     description: "Investment by a resident individual not related to the investment allowance - Shares"
    },
    {
     flow: "OUT", 
     code: "512/01", 
     section: "Transfers of a Capital Nature", 
     description: "Foreign investment by a resident individual in respect of the investment allowance - Shares"
    },
    {
     flow: "OUT", 
     code: "512/02", 
     section: "Transfers of a Capital Nature", 
     description: "Foreign investment by a resident individual in respect of the investment allowance - Bonds"
    },
    {
     flow: "OUT", 
     code: "512/03", 
     section: "Transfers of a Capital Nature", 
     description: "Foreign investment by a resident individual in respect of the investment allowance - Money market instruments"
    },
    {
     flow: "OUT", 
     code: "512/04", 
     section: "Transfers of a Capital Nature", 
     description: "Foreign investment by a resident individual in respect of the investment allowance - Deposits with a foreign bank"
    },
    {
     flow: "OUT", 
     code: "512/05", 
     section: "Transfers of a Capital Nature", 
     description: "Foreign investment by a resident individual in respect of the investment allowance - Mutual funds / collective investment schemes"
    },
    {
     flow: "OUT", 
     code: "512/06", 
     section: "Transfers of a Capital Nature", 
     description: "Foreign investment by a resident individual in respect of the investment allowance - Property"
    },
    {
     flow: "OUT", 
     code: "512/07", 
     section: "Transfers of a Capital Nature", 
     description: "Foreign investment by a resident individual in respect of the investment allowance - Other"
    },
    {
     flow: "OUT", 
     code: "513", 
     section: "Transfers of a Capital Nature", 
     description: "Investment by a resident individual originating from a local source into an account conducted in foreign currency held at an Authorised Dealer in Namibia"
    },
    {
     flow: "OUT", 
     code: "530/01", 
     section: "Transfers of a Capital Nature", 
     description: "Emigration foreign capital allowance - fixed property"
    },
    {
     flow: "OUT", 
     code: "530/02", 
     section: "Transfers of a Capital Nature", 
     description: "Emigration foreign capital allowance - listed investments"
    },
    {
     flow: "OUT", 
     code: "530/03", 
     section: "Transfers of a Capital Nature", 
     description: "Emigration foreign capital allowance - unlisted investments"
    },
    {
     flow: "OUT", 
     code: "530/04", 
     section: "Transfers of a Capital Nature", 
     description: "Emigration foreign capital allowance - insurance policies"
    },
    {
     flow: "OUT", 
     code: "530/05", 
     section: "Transfers of a Capital Nature", 
     description: "Emigration foreign capital allowance - cash"
    },
    {
     flow: "OUT", 
     code: "530/06", 
     section: "Transfers of a Capital Nature", 
     description: "Emigration foreign capital allowance - debtors"
    },
    {
     flow: "OUT", 
     code: "530/07", 
     section: "Transfers of a Capital Nature", 
     description: "Emigration foreign capital allowance - capital distribution from trusts"
    },
    {
     flow: "OUT", 
     code: "530/08", 
     section: "Transfers of a Capital Nature", 
     description: "Emigration foreign capital allowance -other assets"
    },
    {
     flow: "OUT", 
     code: "600", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Adjustments / Reversals / Refunds related to financial investments/disinvestments and prudential investments"
    },
    {
     flow: "OUT", 
     code: "601/01", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Listed shares - sale proceeds paid to a non-resident"
    },
    {
     flow: "OUT", 
     code: "601/02", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Non-listed shares - sale proceeds paid to a non-resident"
    },
    {
     flow: "OUT", 
     code: "602", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment of money market instruments by a non-resident"
    },
    {
     flow: "OUT", 
     code: "603/01", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment of listed bonds by a non-resident (excluding loans)"
    },
    {
     flow: "OUT", 
     code: "603/02", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Disinvestment of non-listed bonds by a non-resident (excluding loans)"
    },
    {
     flow: "OUT", 
     code: "605/01", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment into shares by a resident entity - Agriculture, hunting, forestry and fishing"
    },
    {
     flow: "OUT", 
     code: "605/02", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment into shares by a resident entity - Mining, quarrying and exploration"
    },
    {
     flow: "OUT", 
     code: "605/03", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment into shares by a resident entity - Manufacturing"
    },
    {
     flow: "OUT", 
     code: "605/04", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment into shares by a resident entity - Electricity, gas and water supply"
    },
    {
     flow: "OUT", 
     code: "605/05", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment into shares by a resident entity - Construction"
    },
    {
     flow: "OUT", 
     code: "605/06", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment into shares by a resident entity - Wholesale, retail, repairs, hotel and restaurants"
    },
    {
     flow: "OUT", 
     code: "605/07", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment into shares by a resident entity - Transport and communication"
    },
    {
     flow: "OUT", 
     code: "605/08", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment into shares by a resident entity - Financial services"
    },
    {
     flow: "OUT", 
     code: "605/09", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment into shares by a resident entity - Community, social and personal services"
    },
    {
     flow: "OUT", 
     code: "610/01", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities equity individual"
    },
    {
     flow: "OUT", 
     code: "610/02", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities equity corporate"
    },
    {
     flow: "OUT", 
     code: "610/03", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities equity bank"
    },
    {
     flow: "OUT", 
     code: "610/04", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities equity institution"
    },
    {
     flow: "OUT", 
     code: "611/01", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities debt individual"
    },
    {
     flow: "OUT", 
     code: "611/02", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities debt corporate"
    },
    {
     flow: "OUT", 
     code: "611/03", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities debt bank"
    },
    {
     flow: "OUT", 
     code: "611/04", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities debt institution"
    },
    {
     flow: "OUT", 
     code: "612/01", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities derivatives individual"
    },
    {
     flow: "OUT", 
     code: "612/02", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities derivatives corporate"
    },
    {
     flow: "OUT", 
     code: "612/03", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities derivatives bank"
    },
    {
     flow: "OUT", 
     code: "612/04", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Inward listed securities derivatives institution"
    },
    {
     flow: "OUT", 
     code: "615/01", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment by resident institutional investor - Asset Manager"
    },
    {
     flow: "OUT", 
     code: "615/02", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment by resident institutional investor - Collective Investment Scheme"
    },
    {
     flow: "OUT", 
     code: "615/03", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment by resident institutional investor - Retirement Fund"
    },
    {
     flow: "OUT", 
     code: "615/04", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment by resident institutional investor - Life Linked"
    },
    {
     flow: "OUT", 
     code: "615/05", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Investment by resident institutional investor - Life Non Linked"
    },
    {
     flow: "OUT", 
     code: "616", 
     section: "Financial investments/disinvestments and Prudential investments", 
     description: "Bank prudential investment"
    },
    {
     flow: "OUT", 
     code: "700", 
     section: "Derivatives", 
     description: "Adjustments / Reversals / Refunds related to derivatives"
    },
    {
     flow: "OUT", 
     code: "701/01", 
     section: "Derivatives", 
     description: "Options - listed"
    },
    {
     flow: "OUT", 
     code: "701/02", 
     section: "Derivatives", 
     description: "Options - unlisted"
    },
    {
     flow: "OUT", 
     code: "702/01", 
     section: "Derivatives", 
     description: "Futures - listed"
    },
    {
     flow: "OUT", 
     code: "702/02", 
     section: "Derivatives", 
     description: "Futures - unlisted"
    },
    {
     flow: "OUT", 
     code: "703/01", 
     section: "Derivatives", 
     description: "Warrants - listed"
    },
    {
     flow: "OUT", 
     code: "703/02", 
     section: "Derivatives", 
     description: "Warrants - unlisted"
    },
    {
     flow: "OUT", 
     code: "704/01", 
     section: "Derivatives", 
     description: "Gold hedging - listed"
    },
    {
     flow: "OUT", 
     code: "704/02", 
     section: "Derivatives", 
     description: "Gold hedging - unlisted"
    },
    {
     flow: "OUT", 
     code: "705/01", 
     section: "Derivatives", 
     description: "Derivative not specified above - listed"
    },
    {
     flow: "OUT", 
     code: "705/02", 
     section: "Derivatives", 
     description: "Derivative not specified above - unlisted"
    },
    {
     flow: "OUT", 
     code: "800", 
     section: "Loan and Miscellaneous payments", 
     description: "Adjustments / Reversals / Refunds related to loan and miscellaneous payments"
    },
    {
     flow: "OUT", 
     code: "801", 
     section: "Loan and Miscellaneous payments", 
     description: "Repayment of trade finance drawn down in Namibia"
    },
    {
     flow: "OUT", 
     code: "802", 
     section: "Loan and Miscellaneous payments", 
     description: "Repayment of an international Bond drawn down"
    },
    {
     flow: "OUT", 
     code: "803", 
     section: "Loan and Miscellaneous payments", 
     description: "Repayment by a resident of a loan received from a non-resident shareholder"
    },
    {
     flow: "OUT", 
     code: "804", 
     section: "Loan and Miscellaneous payments", 
     description: "Repayment by a resident of a loan received from a non-resident third party"
    },
    {
     flow: "OUT", 
     code: "810", 
     section: "Loan and Miscellaneous payments", 
     description: "Loan made by a resident to a resident temporarily abroad"
    },
    {
     flow: "OUT", 
     code: "815", 
     section: "Loan and Miscellaneous payments", 
     description: "Individual loan to a non-resident"
    },
    {
     flow: "OUT", 
     code: "816", 
     section: "Loan and Miscellaneous payments", 
     description: "Study loan to a non-resident"
    },
    {
     flow: "OUT", 
     code: "817", 
     section: "Loan and Miscellaneous payments", 
     description: "Shareholders loan to a non-resident"
    },
    {
     flow: "OUT", 
     code: "818", 
     section: "Loan and Miscellaneous payments", 
     description: "Third party loan to a non-resident (excluding shareholders)"
    },
    {
     flow: "OUT", 
     code: "819", 
     section: "Loan and Miscellaneous payments", 
     description: "Trade finance to a non-resident"
    },
    {
     flow: "OUT", 
     code: "830", 
     section: "Loan and Miscellaneous payments", 
     description: "Details of payments not classified"
    },
    {
     flow: "OUT", 
     code: "831", 
     section: "Loan and Miscellaneous payments", 
     description: "NAD collections for the credit of vostro accounts"
    },
    {
     flow: "OUT", 
     code: "833", 
     section: "Loan and Miscellaneous payments", 
     description: "Credit/Debit card company settlement as well as money remitter settlements"
    },
    {
     flow: "OUT", 
     code: "ZZ1", 
     section: "Non-Reportable Transactions", 
     description: "Non reportable"
    }
   ],
})


