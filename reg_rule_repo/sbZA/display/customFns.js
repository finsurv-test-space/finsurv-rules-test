define(function (require) {
  return function (app, config) {

		var superInit = function () { };

    if (config.initializationFn) superInit = config.initializationFn;

		var _config = {
      initializationFn: function (model, scope) {
				superInit(model, scope);
				var issuerCache = {
					AuthAppNumber: '',
					AuthRefNumber: '',
					AuthDate: '',
					InternalAuthNumber: '',
					InternalAuthNumberDate: ''
				};
				var loanRateSet = false;

				scope.ChangeIssuer = function(model, authIssuer) {

					var regAuth = model.data[scope.map('Regulator') + "Auth"];
					var regPref = scope.map('RegulatorPrefix');
					var dealerPref = scope.map('DealerPrefix');

					if(!authIssuer)
					{
						issuerCache["AuthAppNumber"] = regAuth[regPref + 'AuthAppNumber'] ? regAuth[regPref + 'AuthAppNumber'] : issuerCache["AuthAppNumber"];
						issuerCache["AuthRefNumber"] = regAuth[regPref + 'AuthRefNumber'] ? regAuth[regPref + 'AuthRefNumber'] : issuerCache["AuthRefNumber"];
						issuerCache["AuthDate"] = regAuth[regPref + 'AuthDate'] ? regAuth[regPref + 'AuthDate'] : issuerCache["AuthDate"];
						issuerCache["InternalAuthNumber"] =	regAuth[dealerPref + 'InternalAuthNumber'] ? regAuth[dealerPref + 'InternalAuthNumber'] : issuerCache["InternalAuthNumber"];
						issuerCache["InternalAuthNumberDate"] = regAuth[dealerPref + 'InternalAuthNumberDate'] ? regAuth[dealerPref + 'InternalAuthNumberDate'] : issuerCache["InternalAuthNumberDate"];

						regAuth[dealerPref + 'InternalAuthNumber'] = '';
						regAuth[dealerPref + 'InternalAuthNumberDate'] = '';
						regAuth[regPref + 'AuthAppNumber'] = '';
						regAuth[regPref + 'AuthRefNumber'] = '';
						regAuth[regPref + 'AuthDate'] = '';

						return;
					}

					if(authIssuer.value == "THIS BANK" || authIssuer.value == "OTHER BANK") {
						if(!regAuth[dealerPref + 'InternalAuthNumber'] && !regAuth[dealerPref + 'InternalAuthNumberDate'])
						{
							issuerCache["AuthAppNumber"] = regAuth[regPref + 'AuthAppNumber'] ? regAuth[regPref + 'AuthAppNumber'] : issuerCache["AuthAppNumber"];
							issuerCache["AuthRefNumber"] = regAuth[regPref + 'AuthRefNumber'] ? regAuth[regPref + 'AuthRefNumber'] : issuerCache["AuthRefNumber"];
							issuerCache["AuthDate"] = regAuth[regPref + 'AuthDate'] ? regAuth[regPref + 'AuthDate'] : issuerCache["AuthDate"];
	
							regAuth[dealerPref + 'InternalAuthNumber'] = issuerCache["InternalAuthNumber"];
							regAuth[dealerPref + 'InternalAuthNumberDate'] = issuerCache["InternalAuthNumberDate"];
						}						
					} else if(authIssuer.value == "REGULATOR") {
						issuerCache["InternalAuthNumber"] =	regAuth[dealerPref + 'InternalAuthNumber'] ? regAuth[dealerPref + 'InternalAuthNumber'] : issuerCache["InternalAuthNumber"];
						issuerCache["InternalAuthNumberDate"] = regAuth[dealerPref + 'InternalAuthNumberDate'] ? regAuth[dealerPref + 'InternalAuthNumberDate'] : issuerCache["InternalAuthNumberDate"];

						regAuth[regPref + 'AuthAppNumber'] = issuerCache["AuthAppNumber"];
						regAuth[regPref + 'AuthRefNumber'] = issuerCache["AuthRefNumber"];
						regAuth[regPref + 'AuthDate'] = issuerCache["AuthDate"];
					}

					scope.setIgnoreArmCall();
				}

				scope.setLoanRefFields = function () {

					if(!loanRateSet) {

						var money = scope.data.MonetaryAmount;
						// var r = /^(?:(?<TERM>\d+)\s)?(?:(?<BASE>(?:[A-Z]{4,8}(?=\s(?=PLUS|MINUS)))|FIXED)\s)(?:(?<!FIXED\s)(?<PLUSMINUS>PLUS|MINUS)\s)?(?<RATE>\d+(?:\.\d{1,5})?)(?:%)?$/;

						useAlternate = true; /*To simply run IE compatible code - make this true*/
						var regEx = null;
								/* MSIE DOES NOT SUPPORT A SUBSTANTIAL SET OF STANDARD FEATURES FOR REGEX - INCLUDING NAMED CAPTURES.
								https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions#Browser_compatibility  
								USE ALTERNATE MECHANISM...*/
						/*
						if (!useAlternate){}
							try {
								regEx = /^(?:(?<TERM>\d+)\s)?(?:(?<BASE>[A-Z]{4,8})\s)(?:(?<PLUSMINUS>PLUS|MINUS)\s)?(?<RATE>\d+(?:\.\d{1,5})?)(?:%)?$/;
							} catch (regExCeption){
								console.log("...Browser doesn't seem to support Regex with: Named Capture, Non-capture, grouping  ...or other more basic functions of standard Regex. \n A modern, mainstream browser is highly recommended for security and performance reasons.");
								useAlternate = true;
							}
						}
						*/

						for(var i = 0; i < money.length; i++) {

							var loanInterestRate = scope.data.MonetaryAmount[i].LoanInterestRate.val;

							if(loanInterestRate) {
								var loanTerm = scope.data.MonetaryAmount[i].LoanInterest.Term.val;
								var loanBase = scope.data.MonetaryAmount[i].LoanInterest.BaseRate.val;
								var loanSign = scope.data.MonetaryAmount[i].LoanInterest.PlusMinus.val;
								var loanRate = scope.data.MonetaryAmount[i].LoanInterest.Rate.val;
				
								if(!loanTerm && !loanBase && !loanSign && !loanRate) {
									/* COMMENTED OUT NON-MSIE COMPATIBLE CODE FOR THE TIME BEING */
									/*
									if (regEx && !useAlternate){
										try{
											var matches = loanInterestRate.match(regEx);

											if(matches) {

												scope.data.MonetaryAmount[i].LoanInterest.Term.val = matches.groups.TERM;
												scope.data.MonetaryAmount[i].LoanInterest.BaseRate.val = matches.groups.BASE;
												scope.data.MonetaryAmount[i].LoanInterest.PlusMinus.val = matches.groups.PLUSMINUS;
												scope.data.MonetaryAmount[i].LoanInterest.Rate.val = matches.groups.RATE;
											}
										} catch (exception) {
											console.log("Exception raised when attempting clean code parsing of provided LoanInterestRate field: "+JSON.stringify(exception)+"\n...attempting alternate convoluted parse of LoanInterestRate field...");
											useAlternate = true;
										}
									} else {
										useAlternate = true;
									}
									*/

									/*Alternate mechanism for parsing the LoanInterestRate field value - unclean*/
									if (useAlternate){
										try{
											termRegEx = /^(\d+)(?:\s+[A-Z]+(?:\s.*)?)$/;
											baseRegEx = /^(?:\d+\s+)?([A-Z]+)(?:\s.*)?$/;
											plusMinusRegEx = /^(?:\d+\s+)?(?:[A-Z]+\s+)(PLUS|MINUS)(?:\s.*)?$/;
											rateRegEx = /^(?:\d+\s+)?(?:[A-Z]+\s+)?(?:(?:PLUS|MINUS)\s+)?((?:\d+(?:\.\d+)?)|(?:\.\d+))$/;

											matches = loanInterestRate.match(termRegEx);
											if(matches && matches[1]) {
												scope.data.MonetaryAmount[i].LoanInterest.Term.val = matches[1];
											}
											matches = loanInterestRate.match(baseRegEx);
											if(matches && matches[1]) {
												scope.data.MonetaryAmount[i].LoanInterest.BaseRate.val = matches[1];
											}
											matches = loanInterestRate.match(plusMinusRegEx);
											if(matches && matches[1]) {
												scope.data.MonetaryAmount[i].LoanInterest.PlusMinus.val = matches[1];
											}
											matches = loanInterestRate.match(rateRegEx);
											if(matches && matches[1]) {
												scope.data.MonetaryAmount[i].LoanInterest.Rate.val = matches[1];
											}
										} catch (err){
											console.log("Unable to parse provided LoanInterestRate field value: "+loanInterestRate + "\nError Raised: "+JSON.stringify(err));
										}
									}
								}
							}
						}
						loanRateSet = true;
					}
					return true;
				}

				scope.clickedYesButton = function() {
					d.transaction.MonetaryAmount[scope.current.moneyInd].ignoreARMExternalCall = 'true';
					a.validate();					
				}

				scope.clickedNoButton = function() {
					d.transaction.MonetaryAmount[scope.current.moneyInd].armExtNoSelected = 'true';
				}

				scope.setIgnoreArmCall = function() {
					if(d.transaction.MonetaryAmount[scope.current.moneyInd].ignoreARMExternalCall)
						d.transaction.MonetaryAmount[scope.current.moneyInd].ignoreARMExternalCall = undefined;

					setTimeout(function(){
						d.transaction.MonetaryAmount[scope.current.moneyInd].armExtNoSelected = 'false';
					}, 2000);
				}
				
				scope.armExternalValidation = function() {
					var raisedEvents = a.getRaisedEvents();
					var shouldShow = false;
					
					var AuthNumberField = '' + scope.map('Regulator') + 'Auth' + '.' + scope.map('DealerPrefix') + 'InternalAuthNumber';
					var AuthAppNumber = '' + scope.map('Regulator') + 'Auth' + '.' + scope.map('RegulatorPrefix') + 'AuthAppNumber';

					for(var i = 0; i < raisedEvents.length; i++) {
						if(raisedEvents[i].code == '413' && (raisedEvents[i].field == AuthNumberField || raisedEvents[i].field == AuthAppNumber)) {
							shouldShow = true;
						}
					}

					return shouldShow;
				}

				scope.composeLoanInterestRate = function (model) {
					var fullString = '';
					/* Named groups not supported in MSIE */
					/*
					var regEx = /^(?<UNITS>\d{1,2})?((?:\.)(?<DECIMALS>\d{1,})?)?$/
					*/
					var regEx = /^(\d{1,2})?((?:\.)(\d{1,})?)?$/ /* Alternate code hack */

					var loanTerm = model.data.LoanInterest.Term;
					var loanBase = model.data.LoanInterest.BaseRate;
					var loanSign = model.data.LoanInterest.PlusMinus;
					var loanRate = model.data.LoanInterest.Rate;

					if(loanBase == "FIXED") {
						loanTerm = '';
						loanSign = '';
					}
					else if(loanBase == "PRIME") {
						loanTerm = '';
					}

					fullString += loanTerm ? (loanTerm + ' ') : '';
					fullString += loanBase ? (loanBase + ' ') : '';
					fullString += loanSign ? (loanSign + ' ') : '';

					var newLoanRate;
					if (loanRate){
						newLoanRate = loanRate.match(regEx);
						/*  Removed Named groups regex method - not supported by MSIE  */
						/*
						if(newLoanRate && newLoanRate.groups && (newLoanRate.groups.UNITS || newLoanRate.groups.DECIMALS)) {
							loanRate = (newLoanRate.groups.UNITS ? newLoanRate.groups.UNITS : "0") + "." 
							+ (newLoanRate.groups.DECIMALS ? (newLoanRate.groups.DECIMALS.length > 2 ? 
							newLoanRate.groups.DECIMALS.substr(0, 2) : (newLoanRate.groups.DECIMALS.length == 1 
							? newLoanRate.groups.DECIMALS + "0" :  newLoanRate.groups.DECIMALS + "")) : "00");
						*/
						/* ALTERNATE HACK for MSIE  */
						if(newLoanRate && newLoanRate.length && (newLoanRate[1] || newLoanRate[3])) {
							loanRate = (newLoanRate[1] ? newLoanRate[1] : "0") + "." 
							+ (
								newLoanRate[3] ? 
									(newLoanRate[3].length > 2 ? 
										newLoanRate[3].substr(0, 2) : 
										(newLoanRate[3].length == 1 ?
											newLoanRate[3] + "0" :  
											newLoanRate[3] + ""
										)
									) : 
								"00"
							);
						} else {
							if (loanSign || loanBase == 'FIXED'){
								loanRate = '0.00';
							} else {
								loanRate = '';
							}
						}
					}

					

					fullString += loanRate ? loanRate : '';

					model.data.LoanInterestRate = fullString.trim();
				}
			}
    }

    Object.assign(config, _config);

    return config;

  }
})