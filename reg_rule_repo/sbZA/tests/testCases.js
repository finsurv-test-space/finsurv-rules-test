define(function() {
  return function(testBase) {
      with (testBase) {
        var mappings = {
          LocalCurrencySymbol: "R",
          LocalCurrencyName: "Rand",
          LocalCurrency: "ZAR",
          LocalValue: "RandValue",
          Regulator: "Regulator",
          DealerPrefix: "RE",
          RegulatorPrefix: "CB"
        };

        setMappings(mappings, true);
        var test_cases = [

					//Money: RegulatorAuth.AuthFacilitator
					//Must be completed if the Flow is OUT and no data is supplied under RulingsSection
					assertSuccess("auf1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{CategoryCode: '101', RegulatorAuth: {AuthFacilitator: 'THIS BANK'}}]
					}),
					assertSuccess("auf1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{CategoryCode: '101', RegulatorAuth: {RulingsSection: 'XYZ'}}]
					}),
					assertNoRule("auf1", {ReportingQualifier: 'BOPCUS', Flow: 'OUT', MonetaryAmount: [{CategoryCode: '100'}]}),
					assertFailure("auf1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{CategoryCode: '101', RegulatorAuth: {}}]
					}),
					assertFailure("auf1", {ReportingQualifier: 'BOPCUS', Flow: 'OUT', MonetaryAmount: [{CategoryCode: '101'}]}),

					//RegistrationNumber is registered as an IHQ entity or the Subject is IHQnnn
					assertSuccess("auf2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234567/07"}},
						MonetaryAmount: [{
							RegulatorAuth: {AuthFacilitator: 'THIS BANK'}
						}]
					}),
					assertSuccess("auf2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234568/07"}},
						MonetaryAmount: [{
							RegulatorAuth: {AuthFacilitator: 'OTHER BANK'}
						}]
					}),
					assertFailure("auf2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234568/07"}},
						MonetaryAmount: [{
							RegulatorAuth: {AuthFacilitator: ''}
						}]
					}),
					assertSuccess("auf2", {
						ReportingQualifier: 'BOPCUS',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'IHQ123'},
							RegulatorAuth: {AuthFacilitator: 'OTHER BANK'}
						}]
					}),
					assertFailure("auf2", {
						ReportingQualifier: 'BOPCUS',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'IHQ123'},
							RegulatorAuth: {AuthFacilitator: ''}
						}]
					}),
					assertNoRule("auf2", {ReportingQualifier: "NON REPORTABLE"}),
				
					//If the Flow is IN and the Subject is SETOFF, it must be completed
					assertSuccess("auf3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							RegulatorAuth: {AuthFacilitator: 'THIS BANK'}
						}]
					}),
					assertSuccess("auf3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							RegulatorAuth: {AuthFacilitator: 'OTHER BANK'}
						}]
					}),
					assertFailure("auf3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							RegulatorAuth: {AuthFacilitator: ''}
						}]
					}),
					assertFailure("auf3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{AdHocRequirement: {Subject: 'SETOFF'}, RegulatorAuth: {}}]
					}),

					//May not be completed
					assertSuccess("auf4", {ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}]}),
					assertFailure("auf4", {
						ReportingQualifier: 'NON REPORTABLE',
						MonetaryAmount: [{RegulatorAuth: {AuthFacilitator: 'XYZ'}}]
					}),

					//Money: RegulatorAuth.AuthIssuer
					//Must be complete if AuthFacilitator is
					assertSuccess("aui1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{
								CategoryCode: '101', 
								RegulatorAuth: {
									AuthFacilitator: 'THIS BANK', 
									AuthIssuer: "THIS BANK"
								}
							}]
					}),
					assertSuccess("aui1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{
								CategoryCode: '101', 
								RegulatorAuth: {
									AuthFacilitator: 'OTHER BANK', 
									AuthIssuer: "OTHER BANK"
								}
							}]
					}),
					assertSuccess("aui1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{
								CategoryCode: '101', 
								RegulatorAuth: {
									AuthFacilitator: 'THIS BANK', 
									AuthIssuer: "REGULATOR"
								}
							}]
					}),
					assertFailure("aui1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{
								CategoryCode: '101', 
								RegulatorAuth: {
									AuthFacilitator: 'THIS BANK', 
									AuthIssuer: ""
								}
							}]
					}),
					assertFailure("aui1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{
								CategoryCode: '101', 
								RegulatorAuth: {
									AuthFacilitator: 'THIS BANK', 
									AuthIssuer: ""
								}
							}]
					}),
					assertFailure("aui1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{
								CategoryCode: '101', 
								RegulatorAuth: {
									AuthFacilitator: 'OTHER BANK'
								}
							}]
					}),

					//RegistrationNumber is registered as an IHQ entity or the Subject is IHQnnn
					assertSuccess("aui2", {
						ReportingQualifier: 'BOPCUS',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'IHQ123'},
							RegulatorAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'REGULATOR'
							}
						}]
					}),
					assertSuccess("aui2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234567/07"}},
						MonetaryAmount: [{
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: "REGULATOR"
							}
						}]
					}),
					assertSuccess("aui2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234568/07"}},
						MonetaryAmount: [{
							RegulatorAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'REGULATOR'
							}
						}]
					}),
					assertFailure("aui2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234568/07"}},
						MonetaryAmount: [{
							RegulatorAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'OTHER BANK'
							}
						}]
					}),
					assertFailure("aui2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234568/07"}},
						MonetaryAmount: [{
							RegulatorAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'THIS BANK'
							}
						}]
					}),
					assertFailure("aui2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234568/07"}},
						MonetaryAmount: [{
							RegulatorAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'OTHER BANK'
							}
						}]
					}),
					assertSuccess("aui2", {
						ReportingQualifier: 'BOPCUS',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'IHQ123'},
							RegulatorAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'REGULATOR'
							}
						}]
					}),
					assertFailure("aui2", {
						ReportingQualifier: 'BOPCUS',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'IHQ123'},
							RegulatorAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'THIS BANK'
							}
						}]
					}),
					assertFailure("aui2", {
						ReportingQualifier: 'BOPCUS',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'IHQ123'},
							RegulatorAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'OTHER BANK'
							}
						}]
					}),
					assertFailure("aui2", {
						ReportingQualifier: 'BOPCUS',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'IHQ123'},
							RegulatorAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'THIS BANK'
							}
						}]
					}),

					//If the Flow is IN and the Subject is SETOFF, it must be completed
					assertSuccess("aui3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR'
							}
						}]
					}),
					assertSuccess("aui3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR'
							}
						}]
					}),
					assertFailure("aui3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK'
							}
						}]
					}),
					assertFailure("aui3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'THIS BANK'
							}
						}]
					}),
					assertFailure("aui3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK'
							}
						}]
					}),
					assertFailure("aui3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'}, 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK'
							}
						}]
					}),

					 // Money: RegulatorAuth.REInternalAuthNumber
          // Must be completed if the AuthIssuer is 'OTHER BANK' OR 'THIS BANK'
					assertSuccess("za_maian3", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK',
								REInternalAuthNumber: '12345678'
							}
						}]
					}),
					assertSuccess("za_maian3", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'THIS BANK',
								REInternalAuthNumber: '12345678'
							}
						}]
					}),
					assertFailure("za_maian3", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'THIS BANK',
								REInternalAuthNumber: ''
							}
						}]
					}),
					assertFailure("za_maian3", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK',
								REInternalAuthNumber: ''
							}
						}]
					}),
					
					// Must be 8 characters in length
					assertSuccess("za_maian4", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK',
								REInternalAuthNumber: '12345678'
							}
						}]
					}),
					assertSuccess("za_maian4", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'THIS BANK',
								REInternalAuthNumber: '12345678'
							}
						}]
					}),

					assertFailure("za_maian4", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'THIS BANK',
								REInternalAuthNumber: '123'
							}
						}]
					}),
					assertFailure("za_maian4", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK',
								REInternalAuthNumber: 'XYZ'
							}
						}]
          }),

					// Money: RegulatorAuth.CBAuthAppNumber
          // Must be completed if the AuthIssuer is 'REGULATOR'
					assertSuccess("za_masan5", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
								CBAuthAppNumber: '12345678'
							}
						}]
					}),
					assertFailure("za_masan5", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
								CBAuthAppNumber: ''
							}
						}]
					}),
					assertFailure("za_masan5", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
							}
						}]
					}),
					
					// Must be 8 characters in length
					assertSuccess("za_masan6", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
								CBAuthAppNumber: '12345678'
							}
						}]
					}),
					assertFailure("za_masan6", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
								CBAuthAppNumber: '123'
							}
						}]
					}),
					assertFailure("za_masan6", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
								CBAuthAppNumber: 'ABCDEFGH'
							}
						}]
          }),


					// Money: RegulatorAuth.CBAuthDate
          // Must be completed if the AuthIssuer is 'REGULATOR'
					assertSuccess("sad", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
								CBAuthDate: '2020-06-05'
							}
						}]
					}),
					assertSuccess("sad", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'REGULATOR',
								CBAuthDate: ''
							}
						}]
					}),
					assertSuccess("sad", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK',
								CBAuthDate: ''
							}
						}]
					}),
					assertFailure("sad", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
								CBAuthDate: ''
							}
						}]
					}),
					assertFailure("sad", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
							}
						}]
					}),

					assertCustomNoValidation("sb_arm1", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: "THIS BANK",
								AuthIssuer: "THIS BANK",
								REInternalAuthNumber: "12345678",
								REInternalAuthNumberDate: "2012-02-18",
						}
						}]
					}, {"SourceSystem": "ITAP"}),

					assertCustomValidation("sb_arm1", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: "THIS BANK",
								AuthIssuer: "THIS BANK",
								REInternalAuthNumber: "12345678",
								REInternalAuthNumberDate: "2012-02-18",
						}
						}]
					}, {"SourceSystem": ""}),

					assertValidation("sb_arm1", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: "THIS BANK",
								AuthIssuer: "THIS BANK",
								REInternalAuthNumber: "12345678",
								REInternalAuthNumberDate: "2012-02-18",
						}
						}]
					}),

					assertValidation("sb_arm1", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101',
							ignoreARMExternalCall: "",
							RegulatorAuth: {
								AuthFacilitator: "THIS BANK",
								AuthIssuer: "THIS BANK",
								REInternalAuthNumber: "12345678",
								REInternalAuthNumberDate: "2012-02-18"
							}
						}]
					}),

					assertNoValidation("sb_arm1", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101',
							ignoreARMExternalCall: "True",
							RegulatorAuth: {
								AuthFacilitator: "THIS BANK",
								AuthIssuer: "THIS BANK",
								REInternalAuthNumber: "12345678",
								REInternalAuthNumberDate: "2012-02-18"
							}
						}]
					}),

					assertCustomNoValidation("sb_arm2", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: "THIS BANK",
								AuthIssuer: "REGULATOR",
								CBAuthAppNumber: "12345678",
								CBAuthRefNumber: "123",
						}
						}]
					}, {"SourceSystem": "ITAP"}),

					assertCustomValidation("sb_arm2", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: "THIS BANK",
								AuthIssuer: "REGULATOR",
								CBAuthAppNumber: "12345678",
								CBAuthRefNumber: "123",
								CBAuthDate: "2012-02-18"
						}
						}]
					}, {"SourceSystem": ""}),

					assertValidation("sb_arm2", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							RegulatorAuth: {
								AuthFacilitator: "THIS BANK",
								AuthIssuer: "REGULATOR",
								CBAuthAppNumber: "12345678",
								CBAuthRefNumber: "123",
								CBAuthDate: "2012-02-18"
						}
						}]
					}),

					assertValidation("sb_arm2", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101',
							ignoreARMExternalCall: "",
							RegulatorAuth: {
								AuthFacilitator: "THIS BANK",
								AuthIssuer: "REGULATOR",
								CBAuthAppNumber: "12345678",
								CBAuthRefNumber: "123",
								CBAuthDate: "2012-02-18"
						}
						}]
					}),

					assertNoValidation("sb_arm2", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101',
							ignoreARMExternalCall: "True", 
							RegulatorAuth: {
								AuthFacilitator: "THIS BANK",
								AuthIssuer: "REGULATOR",
								CBAuthAppNumber: "12345678",
								CBAuthRefNumber: "123",
								CBAuthDate: "2012-02-18"
						}
						}]
					}),

				]
			}
			return testBase;
		}
	})