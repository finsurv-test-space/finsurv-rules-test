define(function() {
  return function(predef) {
    var display;

    with (predef) {
      display = {
        detailTrans: {
          ruleset: "Feature No Edit Resident Transaction Display Rules",
          scope: "transaction",
          fields: [
            {
              field: [
                "Resident.Individual.Surname",
                "Resident.Individual.Name",
                "Resident.Individual.Gender",
                "Resident.Individual.DateOfBirth",
                "Resident.Individual.IDNumber",
                "Resident.Individual.TempResPermitNumber",
                "Resident.Individual.ForeignIDNumber",
                "Resident.Individual.ForeignIDCountry",
                "Resident.Individual.PassportNumber",
                "Resident.Individual.PassportCountry",
                "Resident.Individual.TaxNumber",
                "Resident.Individual.VATNumber",
                "Resident.Exception.ExceptionName",
                "Resident.Exception.Country",
                "Resident.Entity.EntityName",
                "Resident.Entity.TradingName",
                "Resident.Entity.RegistrationNumber",
                "Resident.Entity.InstitutionalSector",
                "Resident.Entity.IndustrialClassification",
                "Resident.Entity.TaxNumber",
                "Resident.Entity.VATNumber",
                "Resident.Individual.BeneficiaryID1",
                "Resident.Individual.BeneficiaryID2",
                "Resident.Individual.BeneficiaryID3",
                "Resident.Individual.BeneficiaryID4",
                "Resident.Individual.ContactDetails.ContactName",
                "Resident.Individual.ContactDetails.ContactSurname",
                "Resident.Individual.ContactDetails.Email",
                "Resident.Individual.ContactDetails.Fax",
                "Resident.Individual.ContactDetails.Telephone",
                "Resident.Individual.StreetAddress.AddressLine1",
                "Resident.Individual.StreetAddress.AddressLine2",
                "Resident.Individual.StreetAddress.Suburb",
                "Resident.Individual.StreetAddress.City",
                "Resident.Individual.StreetAddress.Province",
                "Resident.Individual.StreetAddress.PostalCode",
                "Resident.Individual.PostalAddress.AddressLine1",
                "Resident.Individual.PostalAddress.AddressLine2",
                "Resident.Individual.PostalAddress.Suburb",
                "Resident.Individual.PostalAddress.City",
                "Resident.Individual.PostalAddress.Province",
                "Resident.Individual.PostalAddress.PostalCode",

                "Resident.Entity.ContactDetails.ContactName",
                "Resident.Entity.ContactDetails.ContactSurname",
                "Resident.Entity.ContactDetails.Email",
                "Resident.Entity.ContactDetails.Fax",
                "Resident.Entity.ContactDetails.Telephone",
                "Resident.Entity.StreetAddress.AddressLine1",
                "Resident.Entity.StreetAddress.AddressLine2",
                "Resident.Entity.StreetAddress.Suburb",
                "Resident.Entity.StreetAddress.City",
                "Resident.Entity.StreetAddress.Province",
                "Resident.Entity.StreetAddress.PostalCode",
                "Resident.Entity.PostalAddress.AddressLine1",
                "Resident.Entity.PostalAddress.AddressLine2",
                "Resident.Entity.PostalAddress.Suburb",
                "Resident.Entity.PostalAddress.City",
                "Resident.Entity.PostalAddress.Province",
                "Resident.Entity.PostalAddress.PostalCode",

                "Resident.Individual.AccountName",
                "Resident.Individual.AccountIdentifier",
                "Resident.Individual.AccountNumber",
                "Resident.Entity.AccountName",
                "Resident.Entity.AccountIdentifier",
                "Resident.Entity.AccountNumber",
              ],
              display: [
                extend(),
                disable(hasCustomValue('disableResident',true)),
                disable(hasCustomValue('disableResident','true'))
              ]
            }
          ]
        },
        detailMoney: {
          ruleset: "Feature No Edit Resident Money Display Rules",
          scope: "money",
          fields: []
        },
        detailImportExport: {
          ruleset: "Feature No Edit Resident Import/Export Display Rules",
          scope: "importexport",
          fields: []
        }
      };
    }

    return display;
  };
});
