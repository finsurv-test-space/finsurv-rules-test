define(function () {
  return function (predef) {
    var stdMoney;
    with (predef) {

      stdMoney = {
        ruleset: "Standard Money Rules",
        scope: "money",
        validations: [
          {
            field: "SequenceNumber",
            rules: [
              failure("mseq1", 547, "Must be completed",
                isEmpty)
            ]
          },
          {
            field: "MoneyTransferAgentIndicator",
            minLen: 2,
            maxLen: 35,
            rules: [
              // NOTE: Updated this message
              failure("mta1", 351, "Must contain one of the following values: AD, or ADLA, CARD or MONEYGRAM, or WESTERNUNION, or PAYPAL, or EXCHANGE4FREE, or MUKURU, or MONEYTRANS or XPRESSMONEY or ZMT or ESKOM or SANLAM or MOMENTUM or TOURVEST or TOWER or IMALI or TRAVELEX or INTERAFRICA or GLOBAL or SIKHONA or FOREXWORLD or ACE or AYOBA or MASTERCURRENCY or INTERCHANGE or HELLO PAISA or TRAVEL CARD or TRAVELLERS CHEQUE or SOUTH EAST or MAMA MONEY or SHOPRITE or DAYTONA or FLASH or PEP or AFROCOIN or ECONET or PAYMENT PARTNER.",
                notEmpty.and(notMoneyTransferAgent)).onSection("ABCD"),
              failure("mta2", 351, "Must contain the value BOPDIR",
                notEmpty.and(notValueIn("BOPDIR"))).onSection("G"),
              failure("mta3", 213, "If the CategoryCode is 833, the MoneyTransferAgentIndicator may not be AD or ADLA",
                notEmpty.and(hasValueIn(["AD", "ADLA"]))).onSection("AB").onCategory("833"),
              failure("mta4", 351, "Must contain the value CARD",
                notEmpty.and(notValueIn("CARD"))).onSection("EF"),
              failure("mta5", 350, "Must be completed",
                isEmpty),
              failure("mta6", "S13", "This dealer is an AD and therefore may not specify ADLA",
                notEmpty.and(dealerTypeAD).and(hasValue("ADLA"))).onSection("ABCD"),
              failure("mta7", "S833", "For BOPCUS transactions, CARD may be used as MTA Indicator provided that the Bop Category is 833.",
                  notEmpty.and(hasValueIn(["CARD"]))).onSection("A").notOnCategory("833"),
              failure("mta8", "S833", "Should not contain the value BOPDIR for AD on sections other than G",
                notEmpty.and(hasValueIn(["BOPDIR"])).and(dealerTypeAD)).onSection("ABCDEF"),
            ]
          },
          {
            field: "{{LocalValue}}",
            rules: [
              failure("mrv1", 348, "At least one of {{LocalValue}} or ForeignValue must be present",
                isEmpty.or(not(isGreaterThan(0.00))).and(notMoneyField("ForeignValue").or(hasMoneyFieldValue("ForeignValue", "0")))).onSection("ABCDEG"),
              failure("mrv2", 352, "Must not contain a negative value",
                notEmpty.and(isNegative)).onSection("ABCDEG"),
              failure("mrv3", 353, "May not equal ForeignValue except if ForeignCurrencyCode is LSL, NAD, SZL, DKK, NOK, SEK, CNY, BWP, UAH, ZAR, HKD, RTG or ZMW",
                equalsMoneyField("ForeignValue").and(notCurrencyIn(["LSL", "NAD", "SZL", "DKK", "NOK", "SEK",
                  "CNY", "BWP", "UAH", "ZAR", "HKD", "RTG", "ZMW"]))).onSection("ABCDEG"),
              failure("mrv4", 354, "If a ForeignValue is completed and a {{LocalValue}} is reported, the reported {{LocalValue}} must be within a 15% variance with the applicable mid-rate completed by the {{Regulator}}. MWK and ZWD are excluded from this validation",
                notEmpty.and(hasMoneyField("ForeignValue").and(notCurrencyIn(["MWK", "ZWD", map("LocalCurrency")]).and(outOfMidRateVariance)))).onSection("ABCDEG"),
              failure("mrv5", 361, "Must not be completed",
                notEmpty).onSection("F"),
              warning("mrv6", 515, "Category 101/11 or 102/11 or 103/11 or 104/11 is used and the value exceeds {{LocalValue}} of {{LocalCurrencySymbol}}100,000.00",
                notEmpty.and(isGreaterThan(100000))).onSection("AB").onCategory(["101/11", "102/11", "103/11", "104/11"]),
              failure("mrv7", 516, "If category 107 is used, the value may not exceed {{LocalValue}} of {{LocalCurrencySymbol}}1,000.00",
                notEmpty.and(isGreaterThan(1000))).onSection("AB").onCategory("107"),
              failure("mrv8", 343, "Must not exceed 20 digits",
                isTooLong(20)).onSection("ABCDEG"),
              // NOTE: Changed message
              failure("mrv9", 356, "If the flow is OUT and the Subject is REMITTANCE DISPENSATION, the {{LocalValueName}} value must be completed",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A"),
              // NOTE: 570 checks are covered by Subject (REMITTANCE DISPENSATION) and RulingsSection checks. You can't change RandValue (total) and therefore no point raising error on this field
              //failure("mrv10", 356, "If the flow is OUT and the Subject is REMITTANCE DISPENSATION then a only maximum {{LocalValueName}} value of {{LocalCurrencySymbol}}3,000 is allowed",
              //  notEmpty.and(hasSumLocalValue('>', "3000")).and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A")
              failure("mrv11", 353, "Both the {{LocalValue}} and ForeignValue for RTG transactions must be supplied, since the RTG rate is not available.",
                isEmpty.or(notMoneyField("ForeignValue").or(hasMoneyFieldValue("ForeignValue", "0"))).and(isCurrencyIn("RTG"))).onSection("ABCDEG"),
              failure("mrv12", 353, "Must equal ForeignValue if ForeignCurrencyCode is same as DomesticCurrencyCode, and both Domestic and Foreign values are supplied.",
                  notEmpty.and(hasMoneyField("ForeignValue")).and(hasTransactionFieldValue("FlowCurrency",map("LocalCurrency"))).and(not(equalsMoneyField("ForeignValue"))))
            ]
          },
          {
            field: "ForeignValue",
            rules: [
              failure("mfv1", 348, "At least one of {{LocalValue}} or ForeignValue must be present",
                isEmpty.and(notMoneyField(map('LocalValue')))).onSection("ABCDEG"),
              failure("mfv2", 357, "Must not contain a negative value",
                isNegative).onSection("ABCDEG"),
              failure("mfv3", 355, "If FlowCurrency is not {{LocalCurrency}}, ForeignValue must be completed",
                isEmpty.or(not(isGreaterThan(0.00))).and(notCurrencyIn(map("LocalCurrency")))).onSection("ABCDEG"),
              failure("mfv4", 362, "Must not be completed",
                notEmpty).onSection("F"),
              failure("mfv5", 343, "Must not exceed 20 digits",
                isTooLong(20)).onSection("ABCDEG"),
              // NOTE: Removed category D
              failure("mfv6", 355, "If CFC RESIDENT, ForeignValue must be completed",
                isEmpty.or(not(isGreaterThan(0.00))).and(hasResidentFieldValue("AccountIdentifier", "CFC RESIDENT"))).onSection("ABC"),
              // failure("mfv7", 353, "Both the {{LocalValue}} and ForeignValue for RTG transactions must be supplied, since the RTG rate is not available.",
              //   isEmpty.or(notMoneyField(map('LocalValue'))).and(isCurrencyIn("RTG"))).onSection("ABCDEG")
            ]
          },
          {
            field: "CategoryCode",
            len: 3,
            rules: [
              failure("mcc1", 366, "Must be completed",
                isEmpty).onSection("ABG"),
              failure("mcc2", 367, "May not be completed",
                notEmpty).onSection("DEF"),
              // Note: Changed message
              failure("mcc3", 368, "Invalid BoPcategory",
                notEmpty.and(hasInvalidCategory)).onSection("ABG"),
              failure("mcc4", "S10", "For NON REPORTABLE transactions this must be set to ZZ1",
                notEmpty.and(notValue("ZZ1"))).onSection("C"),
              failure("mcc5", 292, "If the Flow is IN and category 303, 304, 305, 306, 416 or 417 is used, the Entity element under Resident is completed, the ThirdParty Individual attributes must be completed.",
                hasTransactionField("Resident.Entity").and(notMoneyField("ThirdParty.Individual.Surname"))).onInflow().onCategory(["303", "304", "305", "306", "416", "417"]).onSection("AB"),
              failure("mcc6", 416, "If CategoryCode 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, the third party individual and address details must be completed",
                hasTransactionField("Resident.Entity").and(
                  notMoneyField("ThirdParty.Individual.Surname").or(notMoneyField("ThirdParty.StreetAddress.AddressLine1")).or(notMoneyField("ThirdParty.PostalAddress.AddressLine1"))
                )).onOutflow().onSection("A").onCategory(["512", "513"]),
              failure("mcc7", 416, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, the third party individual and address details must be completed",
                hasTransactionField("Resident.Entity").and(
                  notMoneyField("ThirdParty.Individual.Surname").or(notMoneyField("ThirdParty.StreetAddress.AddressLine1")).or(notMoneyField("ThirdParty.PostalAddress.AddressLine1"))
                )).onInflow().onSection("A").onCategory(["511", "516"]),
              failure('mcc8', 323, 'If the Flow is OUT then the categories 102/01 to 102/10 or 104/01 to 104/10 can only be used for import undertaking customers',
                notEmpty.and(not(hasMoneyField("ThirdParty.CustomsClientNumber").and(evalMoneyField("ThirdParty.CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))).
                  and(not(evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn'))))
                )).onOutflow().onSection("AB").onCategory(['102', '104']).notOnCategory(['102/11', '104/11']),
              // LIBRA-947
              failure('mcc9', 323, 'If the Flow is OUT then the categories 101/01 to 101/10 or 103/01 to 103/10 may not be used for import undertaking customers',
                notEmpty.and(hasMoneyField("ThirdParty.CustomsClientNumber").and(evalMoneyField("ThirdParty.CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))).
                  or(not(hasMoneyField("ThirdParty.CustomsClientNumber")).and(evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))
                  )).onOutflow().onSection("AB").onCategory(['101', '103']).notOnCategory(['101/11', '103/11']),
              failure("mcc11", "S11", "For BOPCUS transactions this can not be set to ZZ1",
                notEmpty.and(hasValue("ZZ1"))).onSection("A"),
            ]
          },
          {
            field: "CategorySubCode",
            rules: [
              failure("msc1", 369, "Invalid SubBoPCategory",
                notEmpty.and(hasInvalidSubCategory.and(notValue("*")))).onSection("ABG"),
              // Note: Removed category F and changed message
              failure("msc2", 370, "Must not be completed",
                notEmpty).onSection("CDE"),
              // NOTE: This message is not in the SARB Spec
              failure("msc3", 368, "This field contains the old category code. Please select a new Finsurv category",
                notEmpty.and(hasValue("*"))).onSection("ABG")
            ]
          },
          {
            field: "SWIFTDetails",
            minLen: 2,
            maxLen: 100,
            rules: [
              failure("mswd1", 293, "Must not be completed",
                notEmpty).onSection("EF")
            ]

          },
          {
            field: "StrateRefNumber",
            minLen: 1,
            maxLen: 30,
            rules: [
              failure("msrn1", 371, "Should be completed with the wording ON MARKET or OFF MARKET if the category is 601/01 or 603/01",
                isEmpty).onSection("AB").onCategory(["601/01", "603/01"]),
              failure("msrn2", 371, "Should be completed with the wording ON MARKET or OFF MARKET if the Resident ExceptionName is STRATE",
                isEmpty.and(hasResException("STRATE"))).onSection("AB"),
              // NOTE: Removed section G
              failure("msrn3", 372, "Must not be completed",
                notEmpty).onSection("CDEF"),
              failure("msrn4", 565, "Should only contain ON MARKET or OFF MARKET",
                notEmpty.and(notValueIn(["ON MARKET", "OFF MARKET"]))).onSection("AB")
            ]
          },
          {
            field: ["Travel", "Travel.Surname", "Travel.Name", "Travel.IDNumber", "Travel.DateOfBirth", "Travel.TempResPermitNumber"],
            rules: [
              deprecated("mtvl1", "S11", "The Travel field can no longer be used for new Finsurv messages. Use ThirdParty element instead",
                notEmpty)
            ]
          },
          {
            field: "LoanRefNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("mlrn1", 373, "If category 801, or 802, or 803 or 804 is used, must be completed",
                isEmpty).onSection("AB").onCategory(["801", "802", "803", "804"]),
              failure("mlrn2", 374, "If CategoryCode is 801, or 802, or 803 or 804 and LocationCountry is LS and the LoanRefNumber must be 99012301230123",
                notValue("99012301230123").and(hasMoneyFieldValue("LocationCountry", "LS"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
              failure("mlrn3", 374, "If CategoryCode is 801, or 802, or 803 or 804 and LocationCountry is SZ and the LoanRefNumber must be 99456745674567",
                notValue("99456745674567").and(hasMoneyFieldValue("LocationCountry", "SZ"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
              failure("mlrn4", 374, "If CategoryCode is 801, or 802, or 803 or 804 and LocationCountry is NA and the LoanRefNumber must be 99789078907890",
                notValue("99789078907890").and(hasMoneyFieldValue("LocationCountry", "NA"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
              failure("mlrn5", 374, "If CategoryCode and CategorySubCode is 106 or 309/04 or 309/05 or 309/06 or 309/07 is used and the Flow is OUT, and the LocationCountry is LS and the LoanRefNumber must be 99012301230123",
                notValue("99012301230123").and(hasMoneyFieldValue("LocationCountry", "LS"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              failure("mlrn6", 374, "If CategoryCode and CategorySubCode is 106 or 309/04 or 309/05 or 309/06 or 309/07 is used and the Flow is OUT, and the LocationCountry is SZ and the LoanRefNumber must be 99456745674567",
                notValue("99456745674567").and(hasMoneyFieldValue("LocationCountry", "SZ"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              failure("mlrn7", 374, "If CategoryCode and CategorySubCode is 106 or 309/04 or 309/05 or 309/06 or 309/07 is used and the Flow is OUT, and the LocationCountry is NA and the LoanRefNumber must be 99789078907890",
                notValue("99789078907890").and(hasMoneyFieldValue("LocationCountry", "NA"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              failure("mlrn8", 373, "If the Flow is OUT, and category is 106 or 309/04 or 309/05 or 309/06 or 309/07, it must be completed",
                isEmpty).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              failure("mlrn9", 375, "If the Flow is OUT and CategoryCode is 810 or 815 or 816 or 817 or 818 or 819 is used, it may not be completed",
                notEmpty).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
              // NOTE: Removed category F on mlrn10
              failure("mlrn10", 375, "May not be completed",
                notEmpty).onSection("CDE"),
              failure("mlrn11", 375, "For any other category other than 801, 802, 803, 804, 106, 309/04, 309/05, 309/06 or 309/07 this must not be completed",
                notEmpty).onSection("AB").notOnCategory(["801", "802", "803", "804", "106", "309/04", "309/05", "309/06", "309/07"]),
              failure("mlrn12", 374, "The LoanRefNumber must contain only characters 0-9",
                notEmpty.and(notPattern(/^\d+$/))).onSection("ABG")
            ]
          },
          {
            field: "LoanTenor",
            minLen: 9,
            maxLen: 10,
            rules: [
              failure("mlt1", 376, "If the Flow is Out and CategoryCode 810 or 815 or 816 or 817 or 818 or 819 is used, must reflect the date of maturity in the format CCYY-MM-DD or ON DEMAND if no date is applicable",
                isEmpty.or(notValue("ON DEMAND").and(notDatePattern))).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
              failure("mlt2", 346, "If the LoanTenor in the format CCYY-MM-DD it must be a future date",
                notEmpty.and(hasDatePattern).and(isDaysInPast(-1))).onSection("ABG"),
              // NOTE: Removed section E and F
              failure("mlt3", 377, "Must not be completed",
                notEmpty).onSection("CD")
            ]
          },
          {
            field: "LoanInterestRate",
            minLen: 1,
            maxLen: 25,
            rules: [
              failure("mlir1", 378, "If the Flow is Out and CategoryCode 810 or 815 or 816 or 817 or 818 or 819 is used, must contain a value of reflecting interest rate percentage of the loan in the proper format. E.g. 0.00, 5.12, BASE PLUS 1.25, BASE MINUS 1.00, 12 LIBOR, 12 LIBOR PLUS 1.25, 6 LIBOR MINUS 0.5, 12 JIBAR, 9 JIBAR PLUS 1.25, 6 JIBAR MINUS 0.5",
                notPattern("^(\\d+(\\.\\d{1,2})?|FIXED \\d+(\\.\\d{1,2})?|BASE PLUS \\d+(\\.\\d{1,2})?|BASE MINUS \\d+(\\.\\d{1,2})?|\\d{1,2} [A-Z]{1,10}|\\d{1,2} [A-Z]{1,10} PLUS \\d+(\\.\\d{1,2})|\\d{1,2} [A-Z]{1,10} MINUS \\d+(\\.\\d{1,2})|[A-Z]{1,10} PLUS \\d+(\\.\\d{1,2})|[A-Z]{1,10} MINUS \\d+(\\.\\d{1,2})|[A-Z]{1,10})$")).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
              // NOTE: Removed section F
              failure("mlir2", 379, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mlir3", 380, "If the Flow is Out and CategoryCode 309/04 to 309/07 is used, must be completed reflecting the percentage interest paid in the format 0.00",
                notPattern("^\\d{1,3}\\.\\d{2}?$")).onOutflow().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
              failure("mlir4", 380, "If the Flow is In and CategoryCode 309/01 to 309/07 is used, must be completed reflecting the percentage interest paid in the format 0.00",
                notPattern("^\\d{1,3}\\.\\d{2}?$")).onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
              warning("mlir5", "S12", "It is unlikely that an interest rate greater than 100% is being charged",
                hasPattern("^\\d{3}\\.\\d{2}?$")).onOutflow().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
              warning("mlir6", "S12", "It is unlikely that an interest rate greater than 100% is being charged",
                hasPattern("^\\d{3}\\.\\d{2}?$")).onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
              failure("mlir7", 379, "May not be completed unless a loan related transaction is being captured",
                notEmpty).onOutflow().onSection("ABG").notOnCategory(["810", "815", "816", "817", "818", "819", "309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
              failure("mlir8", 379, "May not be completed unless a loan related transaction is being captured",
                notEmpty).onInflow().onSection("ABG").notOnCategory(["810", "815", "816", "817", "818", "819", "309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"])
            ]
          },
          {
            field: "{{Regulator}}Auth.RulingsSection",
            minLen: 2,
            maxLen: 30,
            rules: [
              failure("mars1", 381, "Must be completed if the Flow is OUT and no data is supplied under either {{DealerPrefix}}InternalAuthNumber or {{RegulatorPrefix}}AuthAppNumber",
                isEmpty.and(notMoneyField(map("{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber")).and(notMoneyField(map("{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber"))))).onOutflow().onSection("ABG").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              failure("mars2", 382, "Must not be completed",
                notEmpty).onSection("CDEF"),
              failure("mars3", 566, "If the flow is OUT and the Subject is REMITTANCE DISPENSATION then the RulingsSection must be 'Circular 06/2019'",
                hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION").and(notValueIn(["Circular 06/2019", "CIRCULAR 06/2019"]))).onOutflow().onSection("A"),
              failure("mars4", 566, "If the Flow is OUT and the RulingsSection is Circular 06/2019 the RandValue must be equal to or less than 5000.00 and the Subject must be REMITTANCE DISPENSATION.",
                hasValueIn(["Circular 06/2019", "CIRCULAR 06/2019"]).and(hasSumLocalValue('>', "5000").or(not(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))))).onOutflow().onSection("A"),
              // failure("mars6", 566, "If the flow is OUT and the Subject is REMITTANCE DISPENSATION then the RulingsSection must be 'Circular 06/2019'",
              //   notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION").and(hasValueIn(["Circular 06/2019", "CIRCULAR 06/2019"]))).onOutflow().onSection("A"),

              failure("mars5", 566, "If the Flow is IN, the BoPCategory is 400 and the RulingsSection is Circular 06/2019 the RandValue must be equal to or less than 5000.00 and the Subject must be REMITTANCE DISPENSATION.",
                hasValueIn(["Circular 06/2019", "CIRCULAR 06/2019"]).and(hasSumLocalValue('>', "5000").or(not(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))))).onInflow().onCategory("400").onSection("A"),
              // failure("mars4", 566, "If the flow is IN and the Subject is REMITTANCE DISPENSATION (for category 400) then the RulingsSection must be 'Circular 06/2019'",
              //   hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION").and(notValueIn(["Circular 06/2019", "CIRCULAR 06/2019"]))).onInflow().onSection("A"),
              // failure("mars5", 566, "If the flow is IN and the RulingsSection is 'Circular 06/2019', then the Subject must be REMITTANCE DISPENSATION (for category 400)",
              //   notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION").and(hasValueIn(["Circular 06/2019", "CIRCULAR 06/2019"]))).onInflow().onSection("A")

              //NOTE: 570 Rule applied to Ruling Section because the LocalValue can not be changed so no point putting the rule there
              failure("mars6", 570, "If the Flow is OUT and the RandValue is equal or less than 5000.00 and the resident street and postal address attributes are not completed, the RulingsSection must be Circular 06/2019 and the Subject must be REMITTANCE DISPENSATION, except if the Resident ExceptionName is completed",
              notValueIn(["Circular 06/2019", "CIRCULAR 06/2019"]).and(hasSumLocalValue('<=', "5000"))
              .and(not(hasTransactionField("Resident.Exception.ExceptionName"))).and(not(hasResidentField("StreetAddress.AddressLine1")))
              .and(not(hasResidentField("PostalAddress.AddressLine1")))).onOutflow().onSection("A"),
              failure("mars7", 570, "If the Flow is IN, the RandValue is equal or less than 5000.00, the BoPCategory is 400 and the resident street and postal address attributes are not completed, the RulingSection must be 22/2015 and the Subject must be REMITTANCE DISPENSATION, except if the Resident ExceptionName is completed.",
                notValue("22/2015").and(hasSumLocalValue('<=', "5000"))
                .and(not(hasTransactionField("Resident.Exception.ExceptionName"))).and(not(hasResidentField("StreetAddress.AddressLine1")))
                .and(not(hasResidentField("PostalAddress.AddressLine1")))).onInflow().onCategory("400").onSection("A"),
            ]
          },
          {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              failure("maian1", 381, "Must be completed if the Flow is OUT and no data is supplied under either RulingsSection or {{RegulatorPrefix}}AuthAppNumber",
                isEmpty.and(notMoneyField(map("{{Regulator}}Auth.RulingsSection")).and(notMoneyField(map("{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber"))))).onOutflow().onSection("ABG").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              failure("maian2", 382, "May not be completed",
                notEmpty).onSection("CDEF")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
            rules: [
              failure("maiad1", 385, "If {{DealerPrefix}}InternalAuthNumber has a value, it must be completed",
                isEmpty.and(hasMoneyField(map("{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber")))).onSection("ABG"),
              failure("maiad2", 382, "May not be completed",
                notEmpty).onSection("CDEF"),
              failure("maiad3", 215, "Is not in the required date format is CCYY-MM-DD",
                notEmpty.and(notPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/))).onSection("ABG")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              failure("masan1", 381, "Must be completed if the Flow is OUT and no data is supplied under either RulingsSection or {{DealerPrefix}}InternalAuthNumber",
                isEmpty.and(notMoneyField(map("{{Regulator}}Auth.RulingsSection")).and(notMoneyField(map("{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber"))))).onOutflow().onSection("ABG").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              failure("masan2", 386, "If the Flow is IN and the Subject is SETOFF, it must be completed",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SETOFF"))).onInflow().onSection("AB"),
              //TODO: Must be completed if the RegistrationNumber is registered as an IHQ entity
              failure("masan3", 382, "May not be completed",
                notEmpty).onSection("CDEF"),
              failure("masan4", 387, "Must be completed if the RegistrationNumber is registered as an IHQ entity or the Subject is IHQnnn",
                isEmpty.and(evalTransactionField("Resident.Entity.RegistrationNumber", isInLookup('ihqCompanies', 'registrationNumber')).or(evalMoneyField("AdHocRequirement.Subject", hasPattern("^IHQ\\d{3}$"))))).onSection("A")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              failure("masar1", 388, "If {{RegulatorPrefix}}AuthAppNumber has a value, it must be completed",
                isEmpty.and(hasMoneyField(map("{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber")))).onSection("ABG"),
              //failure("masar2", 389, "Must be in the format CCYY-nnnnnnnn where CC is the Century, YY is the year and nnnnnnnn is the e-docs number or any other {{Regulator}} authorisation number allocated by the {{Regulator}} in the reply to the application",
              //        notEmpty.and(notPattern("^(19|20)\\d{2}-\\d+$"))).onSection("ABG"),
              failure("masar3", 382, "May not be completed",
                notEmpty).onSection("CDEF")
            ]
          },
          {
            field: "CannotCategorize",
            minLen: 2,
            maxLen: 100,
            rules: [
              // NOTE: Added section G
              failure("mexc1", 390, "If CategoryCode 830 is used, must be completed",
                isEmpty).onSection("ABG").onCategory("830"),
              failure("mexc2", 392, "Must not be completed",
                notEmpty).onSection("CDEF")
            ]
          },
          {
            field: "AdHocRequirement.Subject",
            minLen: 2,
            maxLen: 30,
            rules: [
              failure("madhs1", 393, "If the Subject contains a value, it must be INVALIDIDNUMBER, AIRPORT, IHQnnn (where nnn is numeric) or SETOFF or ZAMBIAN GRAIN or YES or NO or HOLDCO or SDA",
                notEmpty.and(notValueIn(["INVALIDIDNUMBER", "AIRPORT", "SETOFF", "ZAMBIAN GRAIN", "YES", "NO", "HOLDCO", "SDA", "REMITTANCE DISPENSATION"]).and(notPattern("^IHQ\\d{3}$")))).onSection("AB"),
              // NOTE: Removed section F
              failure("madhs2", 394, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("madhs3", 395, "If the Subject contains the value AIRPORT the Flow must be IN",
                notEmpty.and(hasValue("AIRPORT")).and(notInflow)).onSection("AB"),
              //failure("madhs4", 396, "If the Subject is IHQnnn the AccountIdentifier must be FCA RESIDENT",
              //  notEmpty.and(hasPattern("^IHQ\\d{3}$")).and(notResidentFieldValue("AccountIdentifier", "FCA RESIDENT"))).onSection("AB"),
              //If the Subject contains the value IHQnnn, and the RegistrationNumber under Resident EntityCustomer is equal to a registered IHQ registration number, the value of SUBJECT must be equal to the IHQ code
              //Invalid IHQ number
              failure("madhs5", 400, "If the value is SETOFF, the CategoryCode and SubCategory must be 100 or 101/01 to 101/11 or 102/01 to 102/11 103/01 to 103/11 or 104/01 to 104/11 or 105 or 106 or 107 or 108",
                notEmpty.and(hasValue("SETOFF"))).onSection("AB").notOnCategory(["100", "101", "102", "102", "103", "104", "105", "106", "107", "108"]),
              failure("madhs6", 401, "If the value is SETOFF, the Flow must be IN",
                notEmpty.and(hasValue("SETOFF")).and(notInflow)).onSection("AB"),
              failure("madhs7", 448, "If the Value is ZAMBIAN GRAIN, the CategoryCode must be 101/01 or 109/01 or 110",
                notEmpty.and(hasValue("ZAMBIAN GRAIN"))).onSection("AB").notOnCategory(["101/01", "109/01", "110"]),
              failure("madhs8", 393, "If the Flow is OUT and the CategoryCode is 512/01 to 512/07 or 513 and the Resident Entity element is used, the Subject must be YES or NO",
                hasTransactionField("Resident.Entity").and(notValueIn(["YES", "NO"]))).onOutflow().onSection("A").onCategory(["512", "513"]),
              failure("madhs9", 403, "If Subject is AIRPORT, the CategoryCode must be 830",
                notEmpty.and(hasValue("AIRPORT"))).onSection("AB").notOnCategory("830"),
              failure("madhs10", 403, "If Subject is AIRPORT, the Resident Entity RegistrationNumber must be GOVERNMENT",
                notEmpty.and(hasValue("AIRPORT")).and(notTransactionFieldValue("Resident.Entity.RegistrationNumber", "GOVERNMENT"))).onSection("AB").onCategory("830"),
              failure("madhs11", 403, "If Subject is AIRPORT, the Resident EntityName must be CORPORATION FOR PUBLIC DEPOSITS",
                notEmpty.and(hasValue("AIRPORT")).and(notTransactionFieldValue("Resident.Entity.EntityName", "CORPORATION FOR PUBLIC DEPOSITS"))).onSection("AB").onCategory("830"),
              failure("madhs12", 567, "If the Flow is OUT and the Subject is REMITTANCE DISPENSATION, the {{LocalCurrencyName}} value may not exceed 5,000",
                notEmpty.and(hasValue("REMITTANCE DISPENSATION")).and(hasSumLocalValue('>', "5000"))).onOutflow().onSection("A"),
              failure("madhs13", 393, "Transactions with ReportQualifier 'NON REPORTABLE' may not use REMITTANCE DISPENSATION",
                notEmpty.and(hasValue("REMITTANCE DISPENSATION"))).onSection("C"),
              failure("madhs14", 437, "If NonResident ExceptionName is IHQ, the value must be IHQnnn (where nnn is numeric)",
                hasTransactionFieldValue("NonResident.Exception.ExceptionName", "IHQ").and(isEmpty.or(not(hasPattern(/^IHQ\d{3}$/))))).onSection("A"),
              //failure("madhs15", 566, "If the Flow is OUT and the RulingsSection is Circular 06/2019 the {{LocalValue}} must be equal to or less than 3000.00 and the Subject must be REMITTANCE DISPENSATION. ",
              // evalMoneyField(map("{{Regulator}}Auth.RulingsSection"), hasPattern(/^Circular.*$/)).and(notValue("REMITTANCE DISPENSATION"))).onOutflow().onSection("A"),
              failure("madhs16", 397, "If the RegistrationNumber is equal to a registration number of an IHQ entity as per the IHQ table, the Subject must be IHQnnn related to the IHQ entity. Must be completed if the RegistrationNumber is registered as an IHQ entity or the Subject is IHQnnn",
                evalTransactionField("Resident.Entity.RegistrationNumber", isInLookup('ihqCompanies', 'registrationNumber')).
                  and(not(hasLookupTransactionFieldValue("ihqCompanies", "ihqCode", "registrationNumber", "Resident.Entity.RegistrationNumber")))).onSection("A"),
              failure("madhs17", 399, "Invalid IHQ number",
                notEmpty.and(hasPattern("^IHQ\\d{3}$")).and(not(isInLookup('ihqCompanies', 'ihqCode')))).onSection("A"),
              // failure("madhs18", 437, "If Resident Registration Number is an IHQ, the value must be IHQnnn (where nnn is the number of the associated IHQ company)",
              //   evalTransactionField('Resident.Entity.RegistrationNumber', isInLookup('ihqCompanies', 'registrationNumber')).
              //   and(isEmpty.or(not(hasLookupTransactionFieldValue('ihqCompanies', 'ihqCode', 'registrationNumber', 'Resident.Entity.RegistrationNumber'))))).onSection("A"),
              failure("madhs19", 437, "If Resident Registration Number is not an IHQ, the value can only be be IHQnnn if Non Resident Exception is IHQ",
                not(evalTransactionField('Resident.Entity.RegistrationNumber', isInLookup('ihqCompanies', 'registrationNumber'))).
                  and(notNonResException("IHQ")).
                  and(hasPattern("^IHQ\\d{3}$"))).onSection("A"),
              failure("madhs20", 567, "If the Flow is IN and the Subject is REMITTANCE DISPENSATION on category 400, the {{LocalCurrencyName}} value may not exceed 5,000",
                notEmpty.and(hasValue("REMITTANCE DISPENSATION")).and(hasSumLocalValue('>', "5000"))).onInflow().onCategory("400").onSection("A"),
              failure("madhs21", 567, "If the Flow is IN and the Subject is REMITTANCE DISPENSATION, then category 400 must be used",
                notEmpty.and(hasValue("REMITTANCE DISPENSATION"))).onInflow().notOnCategory("400").onSection("A"),
              failure("madhs22", 570, "If the Flow is OUT and the RulingsSection is 'Circular 06/2019' then the Subject must be 'REMITTANCE DISPENSATION'",
                notValue("REMITTANCE DISPENSATION").and(hasMoneyFieldValue(map("{{Regulator}}Auth.RulingsSection"), ["Circular 06/2019", "CIRCULAR 06/2019"]))).onOutflow().onSection("A")
            ]
          },
          {
            field: "AdHocRequirement.Description",
            minLen: 2,
            maxLen: 100,
            rules: [
              failure("madhd1", 402, "If Subject is used, must be completed",
                isEmpty.and(hasMoneyField("AdHocRequirement.Subject"))).onSection("ABG"),
              failure("madhd2", 454, "If Subject is NO, the Description must be NONE",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "NO").and(notValue("NONE")))).onSection("A"),
              // NOTE: Removed category F
              failure("madhd3", 404, "Must not be completed",
                notEmpty).onSection("CDE")
            ]
          },
          {
            field: "LocationCountry",
            len: 2,
            rules: [
              failure("mlc1", 405, "Must be completed",
                isEmpty).onSection("ABG"),
              failure("mlc2", 238, "Invalid SWIFT country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("ABFG"),
              failure("mlc3", 290, "SWIFT country code may not be {{Locale}} (except if the Non Resident Exception is IHQ)",
                notEmpty.and(hasValue(map("Locale")).and(not(hasNonResException("IHQ"))))).onSection("ABFG"),
              failure("mlc4", 238, "The LocationCountry EU may only be used if the CategoryCode is 513",
                notEmpty.and(hasValue("EU"))).onSection("AB").notOnCategory("513"),
              failure("mlc5", 407, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mlc6", 405, "Must be completed if ForeignCardHoldersPurchases{{LocalCurrencyName}}Value and/or ForeignCardHoldersCashWithdrawals{{LocalCurrencyName}}Value is equal or greater than 0.01",
                isEmpty.and(hasMoneyCardValue)).onSection("F"),
              failure("mlc7", 407, "Must not be completed if ForeignCardHoldersPurchases{{LocalCurrencyName}}Value and ForeignCardHoldersCashWithdrawals{{LocalCurrencyName}}Value is equal to 0.00",
                notEmpty.and(not(hasMoneyCardValue))).onSection("F"),
              failure("mlc8", 398, "Value must be {{Locale}} if the Non Resident Exception is IHQ",
                notEmpty.and(not(hasValue(map("Locale"))).and(hasNonResException("IHQ")))).onSection("A")
            ]
          },
          {
            field: "ReversalTrnRefNumber",
            minLen: 1,
            maxLen: 30,
            rules: [
              failure("mrtrn1", 408, "If CategoryCode 100 or 200 or 300 or 400 or 500 or 600 or 700 or 800 is used, it must be completed",
                isEmpty).onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              failure("mrtrn2", 406, "If ReversalTrnRefNumber has a value, the CardChargeBack must be Y",
                notEmpty.and(notMoneyFieldValue("CardChargeBack", "Y"))).onSection("E"),
              failure("mrtrn3", 285, "May not be used",
                notEmpty).onSection("F"),
              failure("mrtrn4", 219, "Additional spaces identified in data content",
                notEmpty.and(hasAdditionalSpaces)).onSection("ABG"),
              // TODO:  This rule has been updated in the SARB spec
              //        * It is te inverse of mtrn1
              //        * According to the SARB spec it should be completed when the Reporting entity code is 301?
              failure("mrtrn5", 408, "Unless CategoryCode 100 or 200 or 300 or 400 or 500 or 600 or 700 or 800 is used, this must not be completed",
                notEmpty).onSection("AB").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              //TODO: 410 Original transaction and SequenceNumber combination, with an opposite flow, not stored on database
              failure("mrtrn6", "S03", "ReversalTrnRefNumber must not have a value if the Category Code is not a Reversal Code",
                notEmpty.and(not(hasMoneyFieldValue("CategoryCode", ["100", "200", "300", "400", "500", "600", "700", "800"]))))
            ]
          },
          {
            field: "ReversalTrnSeqNumber",
            minLen: 1,
            maxLen: 3,
            rules: [
              failure("mrtrs1", 408, "If CategoryCode 100 or 200 or 300 or 400 or 500 or 600 or 700 or 800 is used, it must be completed",
                isEmpty).onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              // NOTE: Removed '(And not otherwise)' from the message
              failure("mrtrs2", 409, "If the ReversalTrnRefNumber has a value, it must be completed",
                isEmpty.and(hasMoneyField("ReversalTrnRefNumber")).or(notEmpty.and(notMoneyField("ReversalTrnRefNumber")))).onSection("E"),
              failure("mrtrs3", 285, "May not be used",
                notEmpty).onSection("F"),
              failure("mrtrs4", "S02", "ReversalTrnSeqNumber must not have a value if the Category Code is not a Reversal Code",
                notEmpty.and(not(hasMoneyFieldValue("CategoryCode", ["100", "200", "300", "400", "500", "600", "700", "800"]))))
            ]
          },
          {
            field: "BOPDIRTrnReference",
            minLen: 1,
            maxLen: 30,
            rules: [
              // NOTE: Removed section AEF
              failure("mdirtr1", 413, "Must not be completed",
                notEmpty).onSection("BCD")
            ]
          },
          {
            field: "BOPDIR{{DealerPrefix}}Code",
            len: 3,
            rules: [
              // TODO: This rule has completely been changed in the SARB spec
              failure("mdircd1", 415, "If the Reporting Entity Code is not 304 or 305 it may not be completed",
                notEmpty).onSection("ABCDEF"),
              failure("mdircd2", 415, "Must not be completed",
                notEmpty).onSection("BCDE"),
            ]
          },
          {
            field: "ThirdParty.Individual",
            rules: [
              // TODO: This rule has completely been changed in the SARB spec
              failure("mtpi1", 292, "If Flow is OUT and category is 511/01 to 511/07 and Subject is SDA and Resident Entity is used, Third Party Individual details must be provided",
                isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).onOutflow().onCategory("511").onSection("A")

            ]
          },
          {
            field: "ThirdParty.Individual.Surname",
            minLen: 1,
            maxLen: 35,
            rules: [
              failure("mtpisn1", 416, "If CategoryCode 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, it must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onOutflow().onSection("AB").onCategory(["512", "513"]),
              failure("mtpisn2", 416, "If IndividualThirdPartyName contains a value, the IndividualThirdPartySurname must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Name"))).onSection("AB"),
              failure("mtpisn3", 416, "If the category is 256 and the PassportNumber under Individual ResidentCustomerAccountHolder contains no value, IndividualThirdPartySurname must be completed.",
                isEmpty.and(notTransactionField("Resident.Individual.PassportNumber"))).onSection("AB").onCategory("256"),
              failure("mtpisn4", 416, "If the category is 255 or 256 and the EntityCustomer element is completed, IndividualThirdPartySurname must be completed.",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("AB").onCategory(["255", "256"]),
              failure("mtpisn5", 417, "Must not be equal to EntityCustomer LegalEntityName",
                notEmpty.and(matchesTransactionField("Resident.Entity.EntityName"))).onSection("ABE"),
              failure("mtpisn6", 416, "If the SupplementaryCardIndicator is Y, it must be completed",
                isEmpty.and(hasResidentFieldValue("SupplementaryCardIndicator", "Y"))).onSection("E"),
              failure("mtpisn7", 418, "Must not be completed",
                notEmpty).onSection("CD"),
              failure("mtpisn8", 416, "If the Flow is IN and category 303, 304, 305, 306, 416 or 417 is used and Resident Entity is completed, then must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("AB").onCategory(["303", "304", "305", "306", "416", "417"]),
              failure("mtpisn9", 416, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("A").onCategory(["511", "516"]),
              failure("mtpisn10", 418, "If the Subject is REMITTANCE DISPENSATION the Individual ThirdParty Surname must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              failure("mtpisn11", 564, "If the Subject is SDA and the EntityCustomer element is completed in respect of any category the ThirdParty Individual Surname must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).onSection("AB"),
            ]
          },
          {
            field: "ThirdParty.Individual.Name",
            minLen: 1,
            maxLen: 35,
            rules: [
              failure("mtpinm1", 419, "If IndividualThirdPartySurname contains a value, the IndividualThirdPartyName must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname"))).onSection("AB"),
              failure("mtpinm2", 419, "If the category is 256 and the PassportNumber under IndividualCustomer contains no value, IndividualThirdPartyName must be completed.",
                isEmpty.and(notTransactionField("Resident.Individual.PassportNumber"))).onSection("AB").onCategory("256"),
              failure("mtpinm3", 419, "If the category is 255 or 256 and the EntityCustomer element is completed, IndividualThirdPartyName must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("AB").onCategory(["255", "256"]),
              failure("mtpinm4", 417, "Must not be equal to LegalEntityName",
                notEmpty.and(matchesTransactionField("Resident.Entity.EntityName"))).onSection("ABE"),
              failure("mtpinm5", 420, "Must not be completed",
                notEmpty).onSection("CDF"),
              failure("mtpinm6", 419, "If the SupplementaryCardIndicator is Y, it must be completed",
                isEmpty.and(hasResidentFieldValue("SupplementaryCardIndicator", "Y"))).onSection("E"),
              failure("mtpinm7", 420, "If the Subject is REMITTANCE DISPENSATION the Individual ThirdParty Name must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.Individual.Gender",
            len: 1,
            rules: [
              failure("mtpig1", 421, "If IndividualThirdPartySurname contains a value, the IndividualThirdPartyGender must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname"))).onSection("AB"),
              failure("mtpig2", 422, "Invalid gender",
                notEmpty.and(notValueIn(["M", "F"]))).onSection("ABEG"),
              failure("mtpig3", 421, "If the category is 256 and the PassportNumber under IndividualCustomer contains no value, IndividualThirdPartyGender must be completed.",
                isEmpty.and(notTransactionField("Resident.Individual.PassportNumber"))).onSection("AB").onCategory("256"),
              failure("mtpig4", 421, "If the category is 255 and the EntityCustomer element is completed, IndividualThirdPartyGender must be completed.",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("AB").onCategory("255"),
              failure("mtpig5", 423, "Must not be completed",
                notEmpty).onSection("CDF"),
              failure("mtpig6", 421, "If the SupplementaryCardIndicator is Y, IndividualThirdPartyGender must be completed",
                isEmpty.and(hasResidentFieldValue("SupplementaryCardIndicator", "Y"))).onSection("E"),
              failure("mtpig7", 423, "If the Subject is REMITTANCE DISPENSATION the Individual ThirdParty Gender must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              warning("mtpig8", "S11", "The gender should match the ID Number",
                notEmpty.and(hasMoneyField("ThirdParty.Individual.IDNumber").and(notMatchesGenderToIDNumber("ThirdParty.Individual.IDNumber")))).onSection("ABEG")
            ]
          },
          {
            field: "ThirdParty.Individual.IDNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("mtpiid1", 424, "If IndividualThirdPartySurname contains a value, either IndividualThirdPartyIDNumber or IndividualThirdPartyTempResPermitNumber must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname")).and(notMoneyField("ThirdParty.Individual.TempResPermitNumber"))).onSection("AB"),
              failure("mtpiid2", 425, "If category 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onOutflow().onSection("AB").onCategory(["512", "513"]),
              failure("mtpiid3", 426, "This is an Invalid ID number (Note, if the ID number does not comply to the algorithm, the Subject must be INVALIDIDNUMBER to accept an invalid ID number)",
                notEmpty.and(notValidRSAID).and(notMoneyFieldValue("AdHocRequirement.Subject", "INVALIDIDNUMBER"))).onSection("ABEG"),
              failure("mtpiid4", 427, "Must not be completed",
                notEmpty).onSection("CDF"),
              failure("mtpiid5", 424, "If the SupplementaryCardIndicator is Y, either IndividualThirdPartyIDNumber or IndividualThirdPartyTempResPermit Number must be completed",
                isEmpty.and(hasResidentFieldValue("SupplementaryCardIndicator", "Y")).and(notMoneyField("ThirdParty.Individual.TempResPermitNumber"))).onSection("E"),
              failure("mtpiid6", 553, "Must not be equal to IDNumber under Resident IndividualCustomer",
                notEmpty.and(matchesTransactionField("Resident.Individual.IDNumber"))).onSection("ABEG"),
              failure("mtpiid7", 427, "If the Subject is REMITTANCE DISPENSATION the Individual ThirdParty IDNumber must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              failure("mtpiid8", 564, "If the Subject is SDA and the EntityCustomer element is completed in respect of any category the ThirdParty IDNumber must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).onSection("AB"),
              failure("mtpiid9", 425, "If category or 511/01 to 511/07 or 516 is used and flow is IN in cases where the Entity Customer element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("AB").onCategory(["511", "516"])
            ]
          },
          {
            field: "ThirdParty.Individual.DateOfBirth",
            rules: [
              failure("mtpibd1", 428, "If IndividualThirdPartySurname contains a value, the IndividualThirdPartyDateOfBirth must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname"))).onSection("AB"),
              failure("mtpibd2", 428, "If the category is 256 and the PassportNumber under Resident Individual contains no value, this must be completed",
                isEmpty.and(notTransactionField("Resident.Individual.PassportNumber"))).onSection("AB").onCategory("256"),
              failure("mtpibd3", 428, "If the category is 255 and the Resident Entity is provided, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("AB").onCategory("255"),
              failure("mtpibd4", 429, "Must not be completed",
                notEmpty).onSection("CDF"),
              failure("mtpibd5", 428, "If the SupplementaryCardIndicator is Y, IndividualThirdPartyDateOfBirth must be completed",
                isEmpty.and(hasResidentFieldValue("SupplementaryCardIndicator", "Y"))).onSection("E"),
              failure("mtpibd6", 428, "Must be in a valid date format: YYYY-MM-DD",
                notEmpty.and(notPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/).or(isDaysInFuture(0)))).onSection("ABEG"),
              failure("mtpibd7", 429, "If the Subject is REMITTANCE DISPENSATION the Individual ThirdParty DateOfBirth must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              warning("mtpibd8", "S11", "The date of birth should match the ID Number",
                notEmpty.and(hasMoneyField("ThirdParty.Individual.IDNumber").and(notMatchThirdPartyDateToIDNumber))).onSection("ABEG")

            ]
          },
          {
            field: "ThirdParty.Individual.TempResPermitNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("mtpitp1", 424, "If IndividualThirdPartySurname contains a value, either IndividualThirdPartyIDNumber or IndividualThirdPartyTempResPermit Number must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname")).and(notMoneyField("ThirdParty.Individual.IDNumber"))).onSection("AB"),
              failure("mtpitp2", 299, "If category is 511/01 to 511/08 or 512/01 to 512/07 or 513 or 516 is used, this may not be completed",
                notEmpty).onSection("AB").onCategory(["511", "512", "513", "516"]),
              failure("mtpitp3", 424, "If the SupplementaryCardIndicator is Y, either IndividualThirdPartyIDNumber or IndividualThirdPartyTempResPermit Number must be completed",
                isEmpty.and(hasResidentFieldValue("SupplementaryCardIndicator", "Y")).and(notMoneyField("ThirdParty.Individual.IDNumber"))).onSection("E"),
              failure("mtpitp4", 430, "May not be completed",
                notEmpty).onSection("CDF"),
              failure("mtpitp5", 554, "Must not be equal to TempResPermitNumber under Resident IndividualCustomer",
                notEmpty.and(matchesTransactionField("Resident.Individual.TempResPermitNumber"))).onSection("ABEG"),
              failure("mtpitp6", 430, "If the Subject is REMITTANCE DISPENSATION the Individual ThirdParty TempResPermitNumber must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              failure("mtpitp7", 282, "If IndividualThirdPartySurname contains a value and the BoPCategory is 250 and the Flow is OUT, either IndividualThirdPartyTempResPermit Number or IndividualThirdPartyPassportNumber must be completed. (This rule caters for non-residents, but excluding contract workers, traveling o.b.o. of a SA entity)",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname")).and(notMoneyField("ThirdParty.Individual.PassportNumber"))).onOutflow().onCategory("250").onSection("AB")
            ]
          },
          {
            field: "ThirdParty.Individual.PassportNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("mtpipn1", 431, "If the BoPCategory is 256 and the PassportNumber under IndividualCustomer contains no value, IndividualThirdPartyPassportNumber must be completed.",
                isEmpty.and(notTransactionField("Resident.Individual.PassportNumber"))).onSection("AB").onCategory("256"),
              failure("mtpipn2", 431, "If the category is 255 and the EntityCustomer element is completed, IndividualThirdPartyPassportNumber must be completed.",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("AB").onCategory("255"),
              failure("mtpipn3", 432, "Must not be completed",
                notEmpty).onSection("CDEFG"),
              failure("mtpipn4", 555, "Must not be equal to PassportNumber under Resident IndividualCustomer",
                notEmpty.and(matchesTransactionField("Resident.Individual.PassportNumber"))).onSection("AB"),
              failure("mtpipn5", 432, "If the Subject is REMITTANCE DISPENSATION the Individual ThirdParty PassportNumber must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              failure("mtpipn6", 282, "If Individual ThirdParty Surname contains a value and the category is 250 and the Flow is OUT, either ThirdParty TempResPermit Number or ThirdParty PassportNumber must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname")).and(notMoneyField("ThirdParty.Individual.TempResPermitNumber"))).onOutflow().onCategory("250").onSection("AB")
            ]
          },
          {
            field: "ThirdParty.Individual.PassportCountry",
            len: 2,
            rules: [
              failure("mtpipc1", 433, "If the IndividualThirdPartyPassportNumber contains a value, IndividualThirdPartyPassportCountry must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.PassportNumber"))).onSection("AB"),
              failure("mtpipc2", 434, "Must not be completed",
                notEmpty).onSection("CDEFG"),
              failure("mtpipc3", 238, "Invalid country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("AB"),
              failure("mtpipc4", 434, "If the Subject is REMITTANCE DISPENSATION the Individual ThirdParty PassportCountry must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.Entity.Name",
            minLen: 1,
            maxLen: 50,
            rules: [
              //failure("mtpenm1", "F01", "In the case of Business travel, category 255, this attribute must be used", // See wording of field
              //        isEmpty).onSection("AB").onCategory("255"),
              // NOTE: Removed section F
              failure("mtpenm2", 435, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtpenm3", 435, "If the Subject is REMITTANCE DISPENSATION the Entity ThirdParty Name must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.Entity.RegistrationNumber",
            minLen: 2,
            maxLen: 30,
            rules: [
              // NOTE: Removed section F
              failure("mtpern1", 436, "Must not be completed",
                notEmpty).onSection("CDE"),
              // NOTE: Removed section E
              failure("mtpern2", 556, "Must not be equal to RegistrationNumber under Resident EntityCustomer",
                notEmpty.and(matchesTransactionField("Resident.Entity.RegistrationNumber"))).onSection("ABG"),
              failure("mtpern3", 436, "If the Subject is REMITTANCE DISPENSATION the Entity ThirdParty RegistrationNumber must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.CustomsClientNumber",
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("mtpccn1", 322, "CustomsClientNumber must be numeric and contain between 8 and 13 digits",
                notEmpty.and(notPattern(/^\d{8,13}$/))).onSection("ABG"),
              failure("mtpccn2", 322, "The value 70707070 implies an unknown customs client number",
                notEmpty.and(hasValue("70707070"))).onSection("ABG"),
              failure("mtpccn3", 321, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtpccn4", 321, "If the Subject is REMITTANCE DISPENSATION the Entity ThirdParty CustomsClientNumber must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              warning('mtpccn5', 322, 'CustomsClientNumber should pass one of the validations for either CCN, ID Number or Tax Number',
                notEmpty.and(hasPattern(/^\d{8,13}$/)).and(not(isValidCCN.or(isValidRSAID.or(isValidZATaxNumber)))))

            ]
          },
          {
            field: "ThirdParty.TaxNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              failure("mtptx1", 439, "If category 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onOutflow().onSection("AB").onCategory(["512", "513"]),
              // NOTE: Removed section E and F
              failure("mtptx2", 438, "Must not be completed",
                notEmpty).onSection("CD"),
              // NOTE: Removed section E
              failure("mtptx3", 557, "Must not be equal to TaxNumber under AdditionalCustomerData",
                notEmpty.and(matchesTransactionField("Resident.Individual.TaxNumber").or(matchesTransactionField("Resident.Entity.TaxNumber")))).onSection("ABG"),
              failure("mtptx4", 439, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("AB").onCategory(["511", "516"]),
              failure("mtptx5", 438, "If the Subject is REMITTANCE DISPENSATION the Entity ThirdParty TaxNumber must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.VATNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              // NOTE: Removed section E and F
              failure("mtpvn1", 569, "Must not be completed",
                notEmpty).onSection("CD"),
              failure("mtpvn2", 558, "Must not be equal to VATNumber under Resident",
                notEmpty.and(matchesTransactionField("Resident.Individual.VATNumber").or(matchesTransactionField("Resident.Entity.VATNumber")))).onSection("ABEG"),
              failure("mtpvn3", 569, "If the Subject is REMITTANCE DISPENSATION the Entity ThirdParty VATNumber must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.StreetAddress.AddressLine1",
            minLen: 2,
            maxLen: 70,
            rules: [
              failure("mtpsal11", 456, "If category 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onOutflow().onSection("A").onCategory(["512", "513"]),
              failure("mtpsal12", 441, "Must not be completed",
                notEmpty).onSection("CD"),
              failure("mtpsal13", 456, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("A").onCategory(["511", "516"]),
              failure("mtpsal14", 441, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Street AddressLine1 must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.StreetAddress.AddressLine2",
            minLen: 2,
            maxLen: 70,
            rules: [
              // NOTE: Removed section F
              failure("mtpsal21", 442, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtpsal22", 442, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Street AddressLine2 must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.StreetAddress.Suburb",
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("mtpsas1", 458, "If ThirdParty StreetAddress Line1 has a value, this must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.StreetAddress.AddressLine1"))).onSection("AB"),
              failure("mtpsas2", 443, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtpsas3", 443, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Street Suburb must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.StreetAddress.City",
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("mtpsac1", 459, "If ThirdParty StreetAddress Line1 has a value, this must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.StreetAddress.AddressLine1"))).onSection("AB"),
              // NOTE: Removed section F
              failure("mtpsac2", 444, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtpsac3", 444, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Street City must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.StreetAddress.Province",
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("mtpsap1", 485, "If ThirdParty StreetAddress Line1 has a value, this must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.StreetAddress.AddressLine1"))).onSection("AB"),
              // NOTE: Removed section F
              failure("mtpsap2", 445, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtpsap3", 336, "Must be a valid province",
                notEmpty.and(notValidProvince)).onSection("ABG"),
              failure("mtpsap4", 445, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Street Province must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.StreetAddress.PostalCode",
            minLen: 2,
            maxLen: 10,
            rules: [
              failure("mtpsaz1", 501, "If ThirdParty StreetAddress Line1 has a value, this must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.StreetAddress.AddressLine1"))).onSection("AB"),
              // NOTE: Removed section F
              failure("mtpsaz2", 449, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtpsaz3", 338, "Invalid Postal Code",
                notEmpty.and(notValidPostalCode)).onSection("AB"),
              failure("mtpsaz4", 449, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Street PostalCode must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.PostalAddress.AddressLine1",
            minLen: 2,
            maxLen: 70,
            rules: [
              failure("mtppal11", 512, "If category 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onOutflow().onSection("A").onCategory(["512", "513"]),
              // NOTE: Removed section E and F
              failure("mtppal12", 450, "Must not be completed",
                notEmpty).onSection("CD"),
              failure("mtppal13", 512, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("A").onCategory(["511", "516"]),
              failure("mtppal14", 450, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Postal AddressLine1 must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.PostalAddress.AddressLine2",
            minLen: 2,
            maxLen: 70,
            rules: [
              failure("mtppal21", 451, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtppal22", 451, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Postal AddressLine2 must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.PostalAddress.Suburb",
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("mtppas1", 534, "If ThirdParty PostalAddress Line1 has a value, this must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.PostalAddress.AddressLine1"))).onSection("AB"),
              // NOTE: Removed category F
              failure("mtppas2", 453, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtppas3", 453, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Postal Suburb must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.PostalAddress.City",
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("mtppac1", 535, "If ThirdParty PostalAddress Line1 has a value, this must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.PostalAddress.AddressLine1"))).onSection("AB"),
              // NOTE: Removed section F
              failure("mtppac2", 455, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtppac3", 455, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Postal City must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.PostalAddress.Province",
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("mtppap1", 536, "If ThirdParty PostalAddress Line1 has a value, this must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.PostalAddress.AddressLine1"))).onSection("AB"),
              // NOTE: Removed section F
              failure("mtppap2", 457, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtppap3", 336, "Must be a valid province",
                notEmpty.and(notValidProvince)).onSection("ABG"),
              failure("mtppap4", 457, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Postal Province must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.PostalAddress.PostalCode",
            minLen: 2,
            maxLen: 10,
            rules: [
              failure("mtppaz1", 537, "If ThirdParty PostalAddress Line1 has a value, this must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.PostalAddress.AddressLine1"))).onSection("AB"),
              // NOTE: Removed section F
              failure("mtppaz2", 262, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtppaz3", 338, "Invalid Postal Code",
                notEmpty.and(notValidPostalCode)).onSection("ABG"),
              failure("mtppaz4", 262, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Postal Code must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.ContactDetails.ContactSurname",
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("mtpcds1", 460, "If Individual ThirdParty Surname or Entity ThirdParty Name has a value, this must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname").or(hasMoneyField("ThirdParty.Entity.Name")))).onSection("ABG"),
              // NOTE: Removed section F
              failure("mtpcds2", 461, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtpcds3", 461, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Contact Surname must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.ContactDetails.ContactName",
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("mtpcdn1", 462, "If Individual ThirdParty Surname or Entity ThirdParty Name has a value, this must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname").or(hasMoneyField("ThirdParty.Entity.Name")))).onSection("ABG"),
              // NOTE: Removed category F
              failure("mtpcdn2", 463, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtpcdn3", 463, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Contact Name must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.ContactDetails.Email",
            minLen: 2,
            maxLen: 120,
            rules: [
              failure("mtpcde1", 464, "If Individual ThirdParty Surname or Entity ThirdParty Name has a value, either this or ThirdParty Fax or ThirdParty Telephone be must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname").or(hasMoneyField("ThirdParty.Entity.Name"))).and(notMoneyField("ThirdParty.ContactDetails.Fax")).and(notMoneyField("ThirdParty.ContactDetails.Telephone"))).onSection("ABG"),
              // NOTE: Removed category E and F
              failure("mtpcde2", 465, "May not be completed",
                notEmpty).onSection("CD"),
              failure("mtpcde3", 465, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Contact Email must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              failure("cnte4", "E01", "This is not a valid email address",
                notEmpty.and(notValidEmail))

            ]
          },
          {
            field: "ThirdParty.ContactDetails.Fax",
            minLen: 2,
            maxLen: 15,
            rules: [
              failure("mtpcdf1", 464, "If Individual ThirdParty Surname or Entity ThirdParty Name has a value, either this or ThirdParty Email or ThirdParty Telephone be must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname").or(hasMoneyField("ThirdParty.Entity.Name"))).and(notMoneyField("ThirdParty.ContactDetails.Email")).and(notMoneyField("ThirdParty.ContactDetails.Telephone"))).onSection("ABG"),
              // NOTE: Removed section F
              failure("mtpcdf2", 465, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtpcdf3", 465, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Contact Fax must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.ContactDetails.Telephone",
            minLen: 2,
            maxLen: 15,
            rules: [
              failure("mtpcdt1", 464, "If Individual ThirdParty Surname or Entity ThirdParty Name has a value, either this or ThirdParty Fax or ThirdParty Email be must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname").or(hasMoneyField("ThirdParty.Entity.Name"))).and(notMoneyField("ThirdParty.ContactDetails.Fax")).and(notMoneyField("ThirdParty.ContactDetails.Email"))).onSection("ABG"),
              failure("mtpcdt2", 465, "Must not be completed",
                notEmpty).onSection("CDE"),
              failure("mtpcdt3", 465, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Contact Telephone must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "CardChargeBack",
            rules: [
              failure("mcrdcb1", 466, "Must not be completed",
                notEmpty).onSection("ABCDFG"),
              failure("mcrdcb2", 477, "Must be set to either Y or N (blank assumed as N)",
                notEmpty.and(notValueIn(['Y', 'N']))).onSection("E"),
              failure("mcrdcb3", 383, "If CardChargeBack is Y, the Flow must be IN or the Non Resident Account Identifier must be VISA NET or MASTER SEND",
                notEmpty.and(hasValue("Y")).and(notNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onOutflow().onSection("E")
            ]
          },
          {
            field: "CardIndicator",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("mcrdci1", 478, "Must not be completed",
                notEmpty).onSection("ABCDG"),
              failure("mcrdci2", 479, "Must contain AMEX or DINERS or ELECTRON or MAESTRO or MASTER or VISA or BOCEXPRESS",
                notValidCardType).onSection("EF")
            ]
          },
          {
            field: "ElectronicCommerceIndicator",
            minLen: 1,
            maxLen: 2,
            rules: [
              failure("mcrdec1", 538, "Must be completed except if the Non Resident AccountIdentifier is VISA NET or MASTER SEND",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onSection("E"),
              failure("mcrdec2", 480, "Must not be completed",
                notEmpty).onSection("ABCDFG"),
              failure("mcrdec3", 539, "The {{Regulator}} mandates that only certain codes are applicable",
                notEmpty.and(notValidECI)).onSection("E")
              //wut the actual F? - warning("", "TBA", "If the Flow is OUT, and the MerchantCode is 7995, 7800, 7801 or 7802 and the CardIndicator is,……., the transaction is foreign gambling")
            ]
          },
          {
            field: "POSEntryMode",
            len: 2,
            rules: [
              failure("mcrdem1", 480, "Must not be completed",
                notEmpty).onSection("ABCDFG"),
              failure("mcrdem2", 481, "Must be completed except if the Non Resident AccountIdentifier is VISA NET or MASTER SEND",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onSection("E"),
              failure("mcrdem3", 482, "The {{Regulator}} mandates that only certain codes are applicable",
                notEmpty.and(notValidPOSEntryMode)).onSection("E"),
              warning("mcrdem4", 364, "This transaction is regarded as online foreign lottery and gambling",
                notEmpty.and(isForeignGambling)).onSection("E")
            ]
          },
          {
            field: "CardFraudulentTransactionIndicator",
            rules: [
              failure("mcrdft1", 483, "Must not be completed",
                notEmpty).onSection("ABCDFG"),
              failure("mcrdft2", 484, "Must contain a value Y or N",
                isEmpty.or(notValueIn(["Y", "N"]))).onSection("E")
            ]
          },
          {
            field: "ForeignCardHoldersPurchases{{LocalValue}}",
            rules: [
              failure("mcrdfp1", 486, "Must not be completed",
                notEmpty).onSection("ABCDEG"),
              failure("mcrdfp2", 487, "Must be completed",
                isEmpty).onSection("F"),
              failure("mcrdfp3", 352, "Must not contain a negative value",
                notEmpty.and(isNegative)).onSection("F")
            ]
          },
          {
            field: "ForeignCardHoldersCashWithdrawals{{LocalValue}}",
            rules: [
              failure("mcrdfw1", 488, "Must not be completed",
                notEmpty).onSection("ABCDEG"),
              failure("mcrdfw2", 489, "Must be completed",
                isEmpty).onSection("F"),
              failure("mcrdfw3", 352, "Must not contain a negative value",
                notEmpty.and(isNegative)).onSection("F")
            ]
          },
          {
            field: "ImportExport",
            rules: [
              failure("mtie1", 490, "If Inflow and the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, the ImportExport element must be completed",
                emptyImportExport.and(
                  not(hasMoneyFieldValue("CategoryCode", "106").
                    and(
                      hasMoneyField("ThirdParty.CustomsClientNumber").and(evalMoneyField("ThirdParty.CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))
                    )
                  ).and(
                    not(hasMoneyFieldValue("CategoryCode", "106").
                      and(
                        not(hasMoneyField("ThirdParty.CustomsClientNumber")).and(evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))
                      )
                    )
                  )
                )).onInflow().onSection("AB").onCategory(["101", "103", "105", "106"]).notOnCategory(["101/11", "103/11"]),
              failure("mtie2", 491, "ImportExportData Element must not be completed",
                notEmptyImportExport).onSection("CD"),
              failure("mtie3", 529, "Total PaymentValue of all ImportExport entries may not exceed a 1% variance with the {{LocalValue}} or ForeignValue",
                notChecksumImportExport).onSection("ABG"),
              failure("mtie4", 490, "If outflow and the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, the ImportExport element must be completed unless a) the Subject is SDA or REMITTANCE DISPENSATION or b) category 106 and import undertaking client",
                emptyImportExport.and(notMoneyFieldValue("AdHocRequirement.Subject", ["SDA", "REMITTANCE DISPENSATION"]).and(
                  not(hasMoneyFieldValue("CategoryCode", "106").
                    and(
                      hasMoneyField("ThirdParty.CustomsClientNumber").and(evalMoneyField("ThirdParty.CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))
                    )
                  ).and(
                    not(hasMoneyFieldValue("CategoryCode", "106").
                      and(
                        not(hasMoneyField("ThirdParty.CustomsClientNumber")).and(evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))
                      )
                    )
                  )
                ))).onOutflow().onSection("A").onCategory(["101", "103", "105", "106"]).notOnCategory(["101/11", "103/11"]),
              failure("mtie5", 491, "For any category other than 101/01 to 101/11 or 103/01 to 103/11 or 105 or 106, the ImportExport element must not be completed",
                notEmptyImportExport).onSection("ABG").notOnCategory(["101", "103", "105", "106"]),
              failure('mtie6', 494, 'Must contain a sequential SubSequence entries that must start with the value 1',
                notValidSubSequenceNumbers)
            ]
          }
        ]
      };


    }
    return stdMoney;
  }
});
