define(function () {
  return function (predef) {
    var stdTrans;
    with (predef) {

      stdTrans = {
        ruleset: "Standard Transaction Rules",
        scope: "transaction",
        validations: [
          {
            field: "ReplacementTransaction",
            rules: [
              failure("repyn1", 543, "ReplacementTransaction must be completed",
                isEmpty.and(hasTransactionField("ReplacementOriginalReference"))),
              failure("repyn2", 209, "ReplacementTransaction must contain a value Y or N",
                notEmpty.and(notValueIn(["Y", "N"])))
            ]
          },
          {
            field: "ReplacementOriginalReference",
            rules: [
              failure("repor1", 210, "If ReplacementTransaction is Y, must be completed",
                isEmpty.and(hasTransactionFieldValue("ReplacementTransaction", "Y"))),
              failure("repor2", 211, "Must not be completed if ReplacementTransactionIndicator is N",
                notEmpty.and(hasTransactionFieldValue("ReplacementTransaction", "N"))),
              failure("repor3", 219, "Additional spaces identified in data content",
                notEmpty.and(hasAdditionalSpaces))
            ]
          },
          {
            field: "ReportingQualifier",
            minLen: 6,
            maxLen: 25,
            rules: [
              failure("repq1", 541, "Must be completed", isEmpty),
              failure("repq2", 207, "Must contain a valid value",
                notEmpty.and(notReportingQualifier))
            ]
          },
          {
            field: "Flow",
            rules: [
              failure("flow1", 542, "Must be completed", isEmpty),
              failure("flow2", 208, "Must contain the value IN or OUT",
                notEmpty.and(notValueIn(["IN", "OUT"]))).onSection("ABCDEG"),
              failure("flow3", 208, "Must only contain the value IN",
                notEmpty.and(notValue("IN"))).onSection("F"),
              failure("flow4", "F01", "The flow direction of the report data must match the flow on the account entry. Please ask IT support to investigate",
                notEmpty.and(notAccountFLow)).onSection("ABDEFG"),
              failure("flow5", "F01", "The flow direction of the report data must match the flow on the account entry unless this is a VOSTRO related transaction. Please ask IT support to investigate",
                notEmpty.and(notAccountFLow.and(notResException("VOSTRO NON REPORTABLE").and(notNonResException("VOSTRO NON REPORTABLE"))))).onSection("C")
            ]
          },
          {
            field: "ValueDate",
            rules: [
              failure("vd1", 544, "Must be completed", isEmpty),
              failure("vd2", 216, "May not exceed today's date plus 10 days", notEmpty.and(isDaysInFuture(10))),
              warning("vd3", 217, "Old transaction if the ValueDate is today's date less 3 days", notEmpty.and(isDaysInPast(4))),
              failure("vd4", 218, "ValueDate must be equal to or after the 2013-08-19",
                notEmpty.and(isBeforeGoLive)),
              failure("vd5", 214, "Date format incorrect (Date format is CCYY-MM-DD)",
                notEmpty.and(notDatePattern))
            ]
          },
          {
            field: "FlowCurrency",
            rules: [
              failure("fcurr1", "S01", "Must be completed", isEmpty),
              failure("fcurr2", 360, "Invalid SWIFT currency code", notEmpty.and(hasInvalidSWIFTCurrency))
            ]
          },
          {
            field: "TotalForeignValue",
            rules: [
              failure("tfv1", "244", "If the FlowCurrency is {{LocalCurrency}} then the sum of the {{LocalCurrencyName}} Monetary Amounts must add up to the TotalForeignValue",
                notEmpty.and(isCurrencyIn(map("LocalCurrency")).and(notSumLocalValue))).onSection("ABCDEG"),
              failure("tfv2", "244", "If the FlowCurrency is not {{LocalCurrency}} then the sum of the Foreign Monetary Amounts must add up to the TotalForeignValue",
                notEmpty.and(notCurrencyIn(map("LocalCurrency")).and(notSumForeignValue))).onSection("ABCDEG"),
              failure("tfv3", "551", "ForeignCardHoldersPurchases{{LocalValue}} + ForeignCardHoldersCashWithdrawals{{LocalValue}} must equal TotalForeignValue",
                notEmpty.and(notSumCardValue)).onSection("F"),
              failure("tfv4", "245", "The TotalForeignValue must be completed and must be greater than 0.00",
                isEmpty.or(not(isGreaterThan(0.00)))).onSection("ABCDEG")
            ]
          },
          {
            field: "TrnReference",
            minLen: 1,
            maxLen: 30,
            rules: [
              failure("tref1", 545, "Must be completed", isEmpty),
              failure("tref2", 219, "Additional spaces identified in data content",
                notEmpty.and(hasAdditionalSpaces))
            ]
          },
          {
            field: "OriginatingBank",
            minLen: 2,
            maxLen: 50,
            rules: [
              failure("obank1", 228, "If Flow is OUT, must be completed",
                isEmpty).onOutflow().onSection("ABCDG"),
              failure("obank2", 229, "May not be completed",
                notEmpty).onSection("EF"),
              failure("obank3", 230, "If Flow is IN, must be completed except if the Non-Resident AccountIdentifier is CASH",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", "CASH"))).onInflow().onSection("CDG"),
              failure("obank4", 229, "If category 250 or 251, is used and the Flow is IN and the Resident ExceptionName is MUTUAL PARTY, the OriginatingBank may not be completed",
                notEmpty.and(hasResException("MUTUAL PARTY"))).onInflow().onSection("AB").onCategory(["250", "251"]),
              failure("obank5", 229, "If category 252, 255 or 256 is used and the Flow is IN and the NonResident ExceptionName is MUTUAL PARTY, the OriginatingBank may not be completed",
                notEmpty.and(hasNonResException("MUTUAL PARTY"))).onInflow().onSection("AB").onCategory(["252", "255", "256"]),
              failure("obank6", 230, "If OriginatingCountry is completed, must be completed",
                isEmpty.and(hasTransactionField("OriginatingCountry"))).onSection("ABCDG"),
              failure("obank7", 230, "If Flow is IN, must be completed except if the Non-Resident AccountIdentifier is CASH",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", "CASH"))).onInflow().onSection("AB").notOnCategory(["250", "251", "252", "255", "256"]),
              failure("obank8", 230, "If Flow is IN, must be completed except if the Non-Resident AccountIdentifier is CASH or Resident ExceptionName is MUTUAL PARTY",
                isEmpty.and(notResException("MUTUAL PARTY").and(notNonResidentFieldValue("AccountIdentifier", "CASH")))).onInflow().onSection("AB").onCategory(["250", "251"]),
              failure("obank9", 230, "If Flow is IN, must be completed except if the Non-Resident AccountIdentifier is CASH or NonResident ExceptionName is MUTUAL PARTY",
                isEmpty.and(notNonResException("MUTUAL PARTY").and(notNonResidentFieldValue("AccountIdentifier", "CASH")))).onInflow().onSection("AB").onCategory(["252", "255", "256"])
            ]
          },
          {
            field: "OriginatingCountry",
            rules: [
              failure("ocntry1", 231, "If OriginatingBank is completed, must be completed",
                isEmpty.and(hasTransactionField("OriginatingBank"))).onSection("ABCDG"),
              failure("ocntry2", 229, "May not be completed",
                notEmpty).onSection("EF"),
              failure("ocntry3", 233, "If Flow is OUT, SWIFT country code must be {{Locale}}",
                notEmpty.and(notValue(map("Locale")))).onOutflow().onSection("ABG"),
              failure("ocntry4", 234, "If Flow is IN, SWIFT country code may not be {{Locale}}",
                notEmpty.and(hasValue(map("Locale")))).onInflow().onSection("ABG"),
              failure("ocntry5", 238, "Invalid SWIFT country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("ABCDG"),
              failure("ocntry6", 229, "If category 250 or 251, is used and the Flow is IN and the Resident ExceptionName is MUTUAL PARTY, the OriginatingCountry may not be completed",
                notEmpty.and(hasResException("MUTUAL PARTY"))).onInflow().onSection("AB").onCategory(["250", "251"]),
              failure("ocntry7", 229, "If category 252, 255 or 256 is used and the Flow is IN and the NonResident ExceptionName is MUTUAL PARTY, the OriginatingCountry may not be completed",
                notEmpty.and(hasNonResException("MUTUAL PARTY"))).onInflow().onSection("AB").onCategory(["252", "255", "256"])
            ]
          },
          {
            field: "CorrespondentBank",
            minLen: 2,
            maxLen: 50,
            rules: [
              failure("cbank1", 235, "If CorrespondentCountry is completed, must be completed",
                isEmpty.and(hasTransactionField("CorrespondentCountry"))).onSection("ABCDG"),
              failure("cbank2", 236, "May not be completed",
                notEmpty).onSection("EF")
            ]
          },
          {
            field: "CorrespondentCountry",
            rules: [
              failure("ccntry1", 237, "If CorrespondentBank is completed, must be completed",
                isEmpty.and(hasTransactionField("CorrespondentBank"))).onSection("ABCDG"),
              failure("ccntry2", 236, "May not be completed",
                notEmpty).onSection("EF"),
                //ToDo: This code needs to be 238
              failure("ccntry3", 233, "Invalid SWIFT country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("ABCDG")
            ]
          },
          {
            field: "ReceivingBank",
            minLen: 2,
            maxLen: 50,
            rules: [
              failure("rbank1", 239, "If Flow is IN, must be completed",
                isEmpty).onInflow().onSection("ABCDG"),
              failure("rbank2", 240, "Must not be completed",
                notEmpty).onSection("EF"),
              failure("rbank3", 241, "If ReceivingCountry is completed, must be completed",
                isEmpty.and(hasTransactionField("ReceivingCountry"))).onSection("ABCDG"),
              failure("rbank4", 241, "If Flow is OUT, must be completed except if the Non-Resident AccountIdentifier is CASH, VISA NET or MASTER SEND",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["CASH", "VISA NET", "MASTER SEND"]))).onOutflow().onSection("CDG"),
              failure("rbank5", 240, "If category 200, 250 or 251 is used and the Flow is OUT and the Resident ExceptionName is MUTUAL PARTY and the Non Resident AccountIdentifier is CASH, the ReceivingBank may not be completed",
                notEmpty.and(hasResException("MUTUAL PARTY")).and(hasNonResidentFieldValue("AccountIdentifier", "CASH"))).onOutflow().onSection("AB").onCategory(["200", "250", "251"]),
              failure("rbank6", 240, "If category 255, 256 or 530/05 is used and the Flow is OUT and the NonResident ExceptionName is MUTUAL PARTY, the ReceivingBank may not be completed",
                notEmpty.and(hasNonResException("MUTUAL PARTY"))).onOutflow().onSection("AB").onCategory(["200", "255", "256", "530/05"]),
              failure("rbank7", 241, "If Flow is OUT, must be completed except if the Non-Resident AccountIdentifier is CASH, VISA NET or MASTER SEND",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["CASH", "VISA NET", "MASTER SEND"]))).onOutflow().onSection("AB").notOnCategory(["200", "250", "251", "255", "256", "530/05"]),
              failure("rbank8", 241, "If Flow is OUT, must be completed except if the Non-Resident AccountIdentifier is CASH, VISA NET or MASTER SEND",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["CASH", "VISA NET", "MASTER SEND"])).and(notResException("MUTUAL PARTY"))).onOutflow().onSection("AB").onCategory(["200", "250", "251"]),
              failure("rbank9", 241, "If Flow is OUT, must be completed except if the Non-Resident AccountIdentifier is CASH, VISA NET or MASTER SEND",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["CASH", "VISA NET", "MASTER SEND"])).and(notNonResException("MUTUAL PARTY"))).onOutflow().onSection("AB").onCategory(["255", "256", "530/05"])
            ]
          },
          {
            field: "ReceivingCountry",
            rules: [
              failure("rcntry1", 242, "If ReceivingBank is completed must be completed",
                isEmpty.and(hasTransactionField("ReceivingBank"))).onSection("ABCDG"),
              failure("rcntry2", 240, "Must not be completed",
                notEmpty).onSection("EF"),
              failure("rcntry3", 238, "Invalid SWIFT country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("ABCDG"),
              failure("rcntry4", 243, "If Flow is IN, SWIFT country code must be {{Locale}}",
                notValue(map("Locale"))).onInflow().onSection("ABG"),
              failure("rcntry5", 331, "If Flow is OUT, SWIFT country code may not be {{Locale}}",
                notEmpty.and(hasValue(map("Locale")))).onOutflow().onSection("ABG"),
              failure("rcntry6", 240, "If category 200, 250 or 251 is used and the Flow is OUT and the Resident ExceptionName is MUTUAL PARTY, the ReceivingCountry may not be completed",
                notEmpty.and(hasResException("MUTUAL PARTY"))).onOutflow().onSection("AB").onCategory(["200", "250", "251"]),
              failure("rcntry7", 240, "If category 255, 256 or 530/05 is used and the Flow is OUT and the NonResident ExceptionName is MUTUAL PARTY, the ReceivingCountry may not be completed",
                notEmpty.and(hasNonResException("MUTUAL PARTY"))).onOutflow().onSection("AB").onCategory(["255", "256", "530/05"])
            ]
          },
          {
            field: "NonResident",
            rules: [
              failure("nr1", 246, "Must contain one of Individual, Entity, or Exception elements",
                isMissingField(["Individual", "Entity", "Exception"])).onSection("ACDG"),
              failure("nr2", 247, "Must contain only one of Individual or Entity elements",
                isMissingField(["Individual", "Entity"])).onSection("BE"),
              failure("nr3", 248, "If the Reporting Entity Code is 304 or 305 only Non Resident Entity element may be completed",
                isMissingField("Entity")).onSection("G").onCategory(["304", "305"]),
              failure("nr4", 248, "Non Resident Entity element must be completed unless the Non Resident Account Identifier is VISA NET or MASTER SEND",
                isMissingField("Entity").and(notNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onSection("E"),
              failure("nr5", 250, "NonResident and AdditionalNonResidentData elements must not be completed",
                notEmpty).onSection("F"),
              failure("nr6", 253, "If category 250 or 251 is used, Non Resident Entity, or NonResident Exception element may not be completed",
                isMissingField("Individual")).onSection("A").onCategory(["250", "251"])
            ]
          },
          {
            field: "NonResident.Individual.Surname",
            minLen: 1,
            maxLen: 35,
            rules: [
              failure("nrsn1", 254, "If NonResident Individual is completed, must be completed",
                isEmpty).onSection("ABCDEG"),
              failure("nrsn2", 255, "The words specified under Non-resident or Resident ExceptionName, excluding STRATE, must not be used.",
              notEmpty.and(notValue('STRATE').and(isExceptionName))).onSection("ABCDG"),
              failure("nrsn3", 254, "If the Flow is OUT and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then the Surname must have a value",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A"),
              failure("nrsn4", 254, "If the Flow is IN and the category is 400 and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then the Surname must have a value",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onCategory('400').onSection("A")
            ]
          },
          {
            field: "NonResident.Individual.Name",
            minLen: 1,
            maxLen: 50,
            rules: [
              failure("nrnm1", 256, "If Flow is OUT, and NonResident Individual is completed, must be completed",
                isEmpty).onOutflow().onSection("ABCDG"),
              failure("nrnm2", 256, "If the Flow is OUT and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then the Name must have a value",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A"),
              failure("nrnm3", 254, "If the Flow is IN and the category is 400 and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then the Name must have a value",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onCategory('400').onSection("A"),
              failure("nrnm4", 255, "The words specified under Non-resident or Resident ExceptionName, excluding STRATE, must not be used.",
                notEmpty.and(notValue('STRATE').and(isExceptionName))).onSection("ABCDG"),
            ]
          },
          {
            field: "NonResident.Individual.Gender",
            rules: [
              failure("nrgn1", 295, "Invalid gender value",
                notEmpty.and(notValueIn(["M", "F"]))).onOutflow().onSection("ABCDG")
            ]
          },
          {
            field: "NonResident.Individual.PassportNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("nrpn1", 258, "If category 250 or 251 is completed, must be completed",
                isEmpty).onSection("AB").onCategory(["250", "251"]),
              failure("nrpn2", 219, "Additional spaces identified in data content",
                notEmpty.and(hasAdditionalSpaces)).onSection("AB")
            ]
          },
          {
            field: "NonResident.Individual.PassportCountry",
            rules: [
              failure("nrpc1", 259, "If category 250 or 251 is completed, must be completed",
                isEmpty).onSection("AB").onCategory(["250", "251"]),
              failure("nrpc2", 238, "Invalid CountryCode",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("AB")
            ]
          },
          {
            field: "NonResident.Entity.EntityName",
            minLen: 2,
            maxLen: 70,
            rules: [
              failure("nrlen1", 260, "If NonResident Entity is completed, must be completed",
                isEmpty).onSection('ABCDG'),
              failure("nrlen2", 559, "Must not be completed (other than for VISA NET or MASTER SEND)",
                notEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onSection("E"),
              failure("nrlen3", 255, "The words specified under Non-resident or Resident ExceptionName, excluding STRATE, must not be used.",
                notEmpty.and(notValue('STRATE').and(isExceptionName))).onSection("ABCDG"),
              failure("nrlen4", 261, "If the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then an Entity must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              failure("nrlen5", 559, "This field or CardMerchantName must be completed (Only for VISA NET or MASTER SEND)",
                isEmpty.and(notTransactionField("NonResident.Entity.CardMerchantName")).and(hasNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onSection("E")
            ]
          },
          {
            field: "NonResident.Entity.CardMerchantName",
            minLen: 2,
            maxLen: 70,
            rules: [
              failure("nrcmn1", 559, "Must be completed (other than for VISA NET or MASTER SEND)",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onSection("E"),
              failure("nrcmn2", 263, "May not be completed", notEmpty).onSection("ABCDG"),
              failure("nrcmn3", 219, "Additional spaces identified in data content",
                notEmpty.and(hasAdditionalSpaces)).onSection("E"),
              failure("nrcmn4", 559, "This field or EntityName must be completed",
                isEmpty.and(notTransactionField("NonResident.Entity.EntityName")).and(hasNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onSection("E")
            ]
          },
          {
            // TODO: 266 Invalid CardMerchantCode not present
            field: "NonResident.Entity.CardMerchantCode",
            minLen: 4,
            maxLen: 6,
            rules: [
              failure("nrcmc1", 264, "Must be completed",
                isEmpty).onSection("E"),
              failure("nrcmc2", 265, "Must not be completed", notEmpty).onSection("ABCDG"),
              failure("nrcmc3", 266, "Invalid CardMerchantCode", notEmpty.and(not(hasPattern(/^[0-9]*$/)))).onSection("E"),
            ]
          },
          {
            field: "NonResident.Exception",
            rules: [
              failure("nrex1", 268, "If it has a value, ResidentCustomerAccountHolder Exception must not be completed",
                notEmpty.and(hasTransactionField("Resident.Exception"))).onSection("A"),
              failure("nrex2", 269, "Must not be completed",
                notEmpty).onSection("BEFG")
            ]
          },
          {
            field: "NonResident.Exception.ExceptionName",
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("nrexn1", 267, "May only contain one of the specified values",
                notNonResExceptionName).onSection("ACD"),
              failure("nrexn2", 270, "If the ExceptionName is MUTUAL PARTY, the BoPCategory must only be 200, 252, 255, 256 or 530/05",
                hasValue("MUTUAL PARTY")).onSection("A").notOnCategory(["200", "252", "255", "256", "530/05"]),
              failure("nrexn3", 270, "If BoPCategory is 252 and the Flow is IN, must only contain the value MUTUAL PARTY under NonResident ExceptionName",
                notEmpty.and(notValue("MUTUAL PARTY"))).onInflow().onSection("A").onCategory("252"),
              failure("nrexn4", 270, "Must only be completed if BoPCategory is 300 and the original transaction was reported with BoPCategory and SubBoPCategory 309/08 with a Non Resident ExceptionName BULK INTEREST",
                notEmpty.and(hasValue("BULK INTEREST"))).onSection("A").notOnCategory(["300", "309/08"]),
              failure("nrexn5", 270, "ExceptionName of BULK VAT REFUNDS may only be used for category 400 or 411/02",
                notEmpty.and(hasValue("BULK VAT REFUNDS"))).onSection("A").notOnCategory(["400", "411/02"]),
              failure("nrexn6", 270, "ExceptionName of BULK BANK CHARGES may only be used for category 200 or 275",
                notEmpty.and(hasValue("BULK BANK CHARGES"))).onSection("A").notOnCategory(["200", "275"]),
              failure("nrexn7", 270, "ExceptionName of BULK PENSIONS may only be used for category 400 or 407",
                notEmpty.and(hasValue("BULK PENSIONS"))).onSection("A").notOnCategory(["400", "407"]),
              //failure("nrexn8", 437, "If value is IHQ, the Subject under MonetaryDetails must be IHQnnn",
              //        notEmpty.and(hasValue("IHQ")).and(notTransactionFieldValue("MonetaryAmount.AdHocRequirement.Subject", "IHQnnn"))).onSection("A"),
              failure("nrexn9", 270, "ExceptionName of STRATE may only be used for category 601/01 or 603/01",
                notEmpty.and(hasValue("STRATE"))).onSection("A").notOnCategory(["601/01", "603/01"]),
              failure("nrexn10", 269, "May not be used. MUTUAL PARTY is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("MUTUAL PARTY"))).onSection("BCDEFG"),
              failure("nrexn11", 269, "May not be used. BULK INTEREST is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK INTEREST"))).onSection("BCDEFG"),
              failure("nrexn12", 269, "May not be used. BULK VAT REFUNDS is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK VAT REFUNDS"))).onSection("BCDEFG"),
              failure("nrexn13", 269, "May not be used. BULK BANK CHARGES is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK BANK CHARGES"))).onSection("BCDEFG"),
              failure("nrexn14", 269, "May not be used. BULK PENSIONS is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK PENSIONS"))).onSection("BCDEFG"),
              failure("nrexn15", 269, "May not be used. STRATE is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("STRATE"))).onSection("BCDEFG"),
              failure("nrexn16", 269, "May not be used. FCA RESIDENT NON REPORTABLE is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("FCA RESIDENT NON REPORTABLE"))).onSection("ABDEFG"),
              failure("nrexn17", 269, "May not be used. CFC RESIDENT NON REPORTABLE is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("CFC RESIDENT NON REPORTABLE"))).onSection("ABDEFG"),
              failure("nrexn18", 269, "May not be used. VOSTRO NON REPORTABLE is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("VOSTRO NON REPORTABLE"))).onSection("ABDEFG"),
              failure("nrexn19", 269, "May not be used. VOSTRO INTERBANK is only applicable for INTERBANK and NON REPORTABLE transactions",
                notEmpty.and(hasValue("VOSTRO INTERBANK"))).onSection("ABEFG"),
              failure("nrexn20", 269, "May not be used. NOSTRO INTERBANK is only applicable for INTERBANK and NON REPORTABLE transactions",
                notEmpty.and(hasValue("NOSTRO INTERBANK"))).onSection("ABEFG"),
              failure("nrexn21", 269, "May not be used. NOSTRO NON REPORTABLE is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("NOSTRO NON REPORTABLE"))).onSection("ABDEFG"),
              failure("nrexn22", 269, "May not be used. RTGS NON REPORTABLE is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("RTGS NON REPORTABLE"))).onSection("ABDEFG"),
              failure("nrexn23", 269, "If the Subject under the MonetaryDetails is REMITTANCE DISPENSATION and category 256 then MUTUAL PARTY may not be used",
                hasValue("MUTUAL PARTY").and(hasAnyMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A").onCategory("256"),
              failure("nrexn24", 437, "If value is IHQ, the Subject under MonetaryDetails must be IHQnnn",
                notEmpty.and(hasValue("IHQ").and(evalMoneyField("AdHocRequirement.Subject", notPattern(/^IHQ\d{3}$/))))).onSection("A"),
              failure("nrexn25", 269, "May not be used. 'IHQ' is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("IHQ"))).onSection("BCDEFG"),
              failure("nrexn26", 270, "If the MoneyTransferAgentIndicator is TRAVEL CARD or TRAVELLERS CHEQUE, the category can only be 252, 255, 256 or 530/05",
                hasValue("MUTUAL PARTY").and(hasAnyMoneyFieldValue("MoneyTransferAgentIndicator", ["TRAVEL CARD", "TRAVELLERS CHEQUE"]))).onSection("A").notOnCategory(["252", "255", "256", "530/05"]),
              failure("nrexn27", 446, "If value is IHQ, the RegistrationNumber under EntityCustomer of the particular IHQnnn must not be equal to the registered IHQ registration number",
                notEmpty.and(hasValue("IHQ")).and(evalTransactionField("Resident.Entity.RegistrationNumber", isInLookup("ihqCompanies", "registrationNumber")))).onSection("A"),
              failure("nrexn28", 447, "If value is IHQ, the Resident LegalEntityName of the particular IHQnnn must not be equal to the registered IHQ name",
                  notEmpty.and(hasValue("IHQ")).and(evalTransactionField("Resident.Entity.EntityName",isInLookup("ihqCompanies", "companyName")))).onSection("A")
  
            ]
          },
          {
            field: "NonResident.Exception.AccountIdentifier",
            rules: [
              deprecated("nrexai", "S03", "This fields is not used for finsurv submissions",
                notEmpty)
            ]
          },
          {
            field: "NonResident.Exception.AccountNumber",
            rules: [
              deprecated("nrexan", "S04", "This fields is not used for finsurv submissions",
                notEmpty)
            ]
          },
          {
            field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("nriaid1", 272, "If the Flow is OUT must contain a value of NON RESIDENT OTHER, NON RESIDENT RAND, NON RESIDENT FCA, CASH, FCA RESIDENT, RES FOREIGN BANK ACCOUNT, VOSTRO, VISA NET or MASTER SEND",
                notValueIn(["NON RESIDENT OTHER", "NON RESIDENT RAND", "NON RESIDENT FCA", "CASH", "FCA RESIDENT", "RES FOREIGN BANK ACCOUNT", "VOSTRO", "VISA NET", "MASTER SEND"])).onOutflow().onSection("ACDG"),
              failure("nriaid2", 273, "Must contain a value of NON RESIDENT RAND, VISA NET or MASTER SEND", notValueIn(["NON RESIDENT RAND", "VISA NET", "MASTER SEND"])).onSection("B"),
              failure("nriaid3", 272, "Must contain a value VISA NET or MASTER SEND", notEmpty.and(notValueIn(["VISA NET", "MASTER SEND", "CARD DIRECT"]))).onSection("E"),
              failure("nriaid4", 252, "If the AccountIdentifier is NON RESIDENT OTHER or NON RESIDENT RAND or NON RESIDENT FCA or FCA RESIDENT or RES FOREIGN BANK ACCOUNT, NonResident Exception may not be completed",
                hasValueIn(["NON RESIDENT OTHER", "NON RESIDENT RAND", "NON RESIDENT FCA", "FCA RESIDENT", "RES FOREIGN BANK ACCOUNT"]).and(hasTransactionField("NonResident.Exception"))).onSection("ABEG"),

              failure("nriaid5", 274, "If the AccountIdentifier is RES FOREIGN BANK ACCOUNT and the Flow is IN and the category is 255 or 256 or 810 or 416, the Non Resident Individual element must be completed.",
                hasValue("RES FOREIGN BANK ACCOUNT").and(notTransactionField("NonResident.Individual"))).onInflow().onCategory(["255", "256", "810", "416"]).onSection("A"),

              failure("nriaid6", 274, "If the AccountIdentifier is RES FOREIGN BANK ACCOUNT and the Flow is OUT and the category is 255 or 256 or 810, the non resident Individual element must be completed.",
                hasValue("RES FOREIGN BANK ACCOUNT").and(notTransactionField("NonResident.Individual"))).onOutflow().onCategory(["255", "256", "810"]).onSection("A"),
              ignore("nriaid7", 272, "If the Flow is OUT must contain a value of NON RESIDENT OTHER, NON RESIDENT RAND, NON RESIDENT FCA, CASH, FCA RESIDENT, RES FOREIGN BANK ACCOUNT, VOSTRO, VISA NET or MASTER SEND", // duplicate of nriaid1
                notValueIn(["NON RESIDENT OTHER", "NON RESIDENT RAND", "NON RESIDENT FCA", "CASH", "FCA RESIDENT", "RES FOREIGN BANK ACCOUNT", "VOSTRO", "VISA NET", "MASTER SEND"])).onOutflow().onSection("ACDG"),
              failure("nriaid9", 276, "If AccountIdentifier is FCA RESIDENT and the Flow is OUT the category 513 must be completed",
                hasValue("FCA RESIDENT")).onOutflow().onSection("A").notOnCategory("513"),
              failure("nriaid10", 276, "If AccountIdentifier is FCA RESIDENT and the Flow is IN the category 517 must be completed",
                hasValue("FCA RESIDENT")).onInflow().onSection("A").notOnCategory("517"),
              ignore("nriaid11"), // 219 deleted in 21040123
              warning("nriaid12", 272, "The value CARD DIRECT is being deprecated (VISA NET and MASTER SEND to be implemented by 2016-05-15)", hasValue("CARD DIRECT")).onSection("E"),
              failure("nriaid13", 252, "May not be completed",
                notEmpty).onSection("F"),
              failure("nriaid14", 274, "If the AccountIdentifier is FCA RESIDENT and the Flow is OUT and the category is 513, Non Resident Individual must be completed",
                hasValue("FCA RESIDENT").and(hasTransactionField("NonResident.Entity"))).onOutflow().onSection("A").onCategory("513")
            ]
          },
          {
            field: ["NonResident.Individual.AccountNumber", "NonResident.Entity.AccountNumber"],
            minLen: 2,
            maxLen: 40,
            rules: [
              failure("nrian1", 279, "If the Flow is OUT must be completed if the AccountIdentifier is not CASH or NON RESIDENT RAND",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["CASH", "NON RESIDENT RAND"]))).onOutflow().onSection("ABCDG"),
              failure("nrian2", 280, "Must not be equal to AccountNumber under Resident element",
                notEmpty.and(matchesResidentField("AccountNumber"))).onSection("ABCDG"),
              failure("nrian3", 533, "May not contain invalid characters like ' or &",
                notEmpty.and(hasPattern(/['&]/))).onSection("ABCDG"),
              failure("nrian4", 252, "Must not be completed",
                notEmpty).onSection("F")
            ]
          },
          {
            field: ["NonResident.Individual.Address.AddressLine1", "NonResident.Entity.Address.AddressLine1"],
            minLen: 2,
            maxLen: 50,
            rules: [
              // NOTE: Removed section F
              failure("nrial11", 281, "Must not be completed", notEmpty).onSection("E")
            ]
          },
          {
            field: ["NonResident.Individual.Address.AddressLine2", "NonResident.Entity.Address.AddressLine2"],
            minLen: 2,
            maxLen: 50,
            rules: [
              // NOTE: Removed section F
              failure("nrial21", 281, "Must not be completed", notEmpty).onSection("E")
            ]
          },
          {
            field: ["NonResident.Individual.Address.Suburb", "NonResident.Entity.Address.Suburb"],
            minLen: 2,
            maxLen: 50,
            rules: [
              // NOTE: Removed section F
              failure("nrial31", 281, "Must not be completed", notEmpty).onSection("E")
            ]
          },
          {
            field: ["NonResident.Individual.Address.City", "NonResident.Entity.Address.City"],
            minLen: 2,
            maxLen: 35,
            rules: [
              ignore("nric1").onSection("ABCDG"),
              // NOTE: Removed section F
              failure("nric2", 284, "Must not be completed", notEmpty).onSection("E")
            ]
          },
          {
            field: ["NonResident.Individual.Address.State", "NonResident.Entity.Address.State"],
            minLen: 2,
            maxLen: 35,
            rules: [
              ignore("nris1").onSection("ABCDG"),
              failure("nris2", 286, "Must not be completed",
                notEmpty).onSection("E")
            ]
          },
          {
            field: ["NonResident.Individual.Address.PostalCode", "NonResident.Entity.Address.PostalCode"],
            minLen: 2,
            maxLen: 10,
            rules: [
              ignore("nriz1").onSection("ABCDG"),
              failure("nriz2", 288, "Must not be completed",
                notEmpty).onSection("E"),
              failure("nriz3", "L04", "Length is too long",
                notEmpty.and(isTooLong(10))).onSection("ABCDG")
            ]
          },
          {
            field: ["NonResident.Individual.Address.Country", "NonResident.Entity.Address.Country"],
            len: 2,
            rules: [
              failure("nrictry1", 289, "Must be completed",
                isEmpty).onSection("ABCDEG"),
              failure("nrictry2", 238, "Invalid SWIFT country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("ABCDEG"),
              failure("nrictry3", 290, "SWIFT country code may not be {{Locale}}",
                notEmpty.and(hasValue(map("Locale")))).onSection("ABCDG"),
              failure("nrictry4", 238, "SWIFT country code must not be {{Locale}} except if the ForeignIDCountry under IndividualCustomer is NA, LS or SZ",
                notEmpty.and(hasValue(map("Locale")).and(notTransactionFieldValue("Resident.Individual.ForeignIDCountry", ["NA", "LS", "SZ"])))).onSection("E"),
              failure("nrictry5", 238, "EU is not a valid country and may not be used",
                notEmpty.and(hasValue("EU"))).onOutflow().onSection("A").notOnCategory("513"),
              failure("nrictry6", 238, "EU is not a valid country and may not be used",
                notEmpty.and(hasValue("EU"))).onInflow().onSection("A").notOnCategory("517"),
              failure("nrictry7", 238, "For Outflow category 513 the country must be linked to the currency (EU must be used for EUR payments)",
                notEmpty.and(notMatchToCurrency)).onOutflow().onSection("A").onCategory("513"),
              failure("nrictry8", 238, "For Inflow category 517 the country must be linked to the currency (EU must be used for EUR payments)",
                notEmpty.and(notMatchToCurrency)).onInflow().onSection("A").onCategory("517"),
              failure("nrictry9", 252, "Must not be completed",
                notEmpty).onSection("F")
            ]
          },


          //Resident
          {
            field: "Resident",
            rules: [
              failure("rg1", 277, "Must contain one of IndividualCustomer or EntityCustomer or Exception elements",
                notTransactionField("Resident.Individual").and(notTransactionField("Resident.Entity").and(notTransactionField("Resident.Exception")))).onSection("ABCEG"),
              failure("rg2", 291, "If the Flow is OUT and category 255 is used, Resident EntityCustomer element must be completed",
                notTransactionField("Resident.Entity")).onOutflow().onCategory("255").onSection("AB"),
              //ignore("rg3", 293, "If category 256 is used, Resident Individual element must be completed",
              //  notTransactionField("Resident.Individual")).onCategory("256").onSection("AB"),
              // failure("rg4", 292, "If category is 514/01 to 514/07 or 515/01 to 515/07 the Resident Entity may not be used",
              //   hasTransactionField("Resident.Entity")).onCategory(["514", "515"]).onSection("A"),
              failure("rg5", 548, "Resident CustomerAccountHolder and AdditionalCustomer Data elements must not be completed",
                notEmpty).onSection("F"),
              ignore("rg6"), // moved to CategoryCode field mcc5
              //failure("rg7", 292, "If Flow is OUT and category is 511/01 to 511/07 the Resident Entity may not be used",
              //  hasTransactionField("Resident.Entity").and(notMoneyFieldValue("AdHocRequirement.Subject","SDA"))).onOutflow().onCategory("511").onSection("A")
            ]
          },
          {
            field: "Resident.Individual.Surname",
            minLen: 1,
            maxLen: 35,
            rules: [
              failure("risn1", 254, "If Resident Individual is completed, Surname must be completed", isEmpty).onSection("ABCEG"),
              failure("risn2", 255, "The words specified under NonResident or Resident ExceptionName, excluding STRATE, must not be used.",
                notValue('STRATE').and(isExceptionName)).onSection("ABCE"),
              failure("risn3", 254, "If the Flow is OUT and the Subject under the MonetaryDetails element is REMITTANCE DISPENSATION, surname must have a value.",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A"),
              failure('risn4', 254, 'If the Flow is IN and the category is 400 and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION, surname must be completed.',
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onCategory('400').onSection("A"),
              ]
          },
          {
            field: "Resident.Individual.Name",
            minLen: 1,
            maxLen: 50,
            rules: [
              failure("rin1", 256, "If Resident Individual is completed, must be completed", 
                isEmpty.and(hasTransactionField("Resident.Individual"))).onSection("ABCEG"),
              failure("rin2", 256, "If the Flow is OUT and the Subject under the MonetaryDetails element is REMITTANCE DISPENSATION, name must have a value.",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A"),
              failure('rin3', 256, 'If the Flow is IN and the category is 400 and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION, name must be completed.',
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onCategory('400').onSection("A"),
              failure("rin4", 255, "The words specified under NonResident or Resident ExceptionName, excluding STRATE, must not be used.",
                notValue('STRATE').and(isExceptionName)).onSection("ABCE"),
            ]
          },
          {
            field: "Resident.Individual.Gender",
            len: 1,
            rules: [
              failure("rig1", 257, "Must be completed", isEmpty).onSection("ABEG"),
              failure("rig2", 295, "Invalid Gender value", notEmpty.and(notValueIn(["M", "F"]))).onSection("ABEG"),
              warning("rig3", "S11", "The gender should match the ID Number",
                notEmpty.and(evalTransactionField("Resident.Individual.IDNumber",notEmpty.and(isValidRSAID)).and(notMatchesGenderToIDNumber("Resident.Individual.IDNumber")))).onSection("ABEG")
            ]
          },
          {
            field: "Resident.Individual.DateOfBirth",
            rules: [
              failure("ridob1", 296, "Must be completed",
                isEmpty).onSection("ABEG"),
              failure("ridob2", 215, "Date format incorrect (Date format is CCYY-MM-DD)",
                notEmpty.and(notPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/).or(isDaysInFuture(0)))).onSection("ABEG"),
              warning("ridob3", "S11", "The date of birth should match the ID Number",
                notEmpty.and(hasTransactionField("Resident.Individual.IDNumber").and(notMatchResidentDateToIDNumber))).onSection("ABEG")
            ]
          },
          {
            field: "Resident.Individual.IDNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("riidn1", 298, "If BoPCategory and SubBoPCategory is 511/01 to 511/08 or 512/01 to 512/07 or 513 is used, must be completed",
                isEmpty).onSection("AB").onCategory(["511", "512", "513"]),
              failure("riidn2", 298, "If the Flow is OUT and category 401 is used, must be completed",
                isEmpty).onOutflow().onSection("AB").onCategory("401"),
              failure("riidn3", 297, "Invalid ID number if completed. (Note, if the ID number does not comply to the algorithm, the Subject must be INVALIDIDNUMBER to accept an invalid ID number)",
                notEmpty.and(notValidRSAID).and(notMoneyFieldValue("AdHocRequirement.Subject", "INVALIDIDNUMBER"))).onSection("ABEG"),
              failure("riidn4", 294, "If IndividualCustomer is selected, at least one of IDNumber or TempResPermitNumber or ForeignIDNumber must be completed",
                isEmpty.and(notTransactionField("Resident.Individual.TempResPermitNumber").and(notTransactionField("Resident.Individual.ForeignIDNumber")))).onSection("ABE"),
              failure("riidn5", 563, "If the Subject is SDA, IDNumber must be completed",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).onSection("AB")
            ]
          },
          {
            field: "Resident.Individual.TempResPermitNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("ritrpn1", 299, "If CategoryCode 511, 512 or 513 is used, TempResPermitNumber may not be completed",
                notEmpty).onSection("AB").onCategory(["511", "512", "513"]),
              failure("ritrpn2", 299, "If the Flow is OUT and category 401 is used, may not be completed",
                notEmpty).onOutflow().onSection("AB").onCategory("401"),
              failure("ritrpn3", 294, "If IndividualCustomer is selected, at least one of IDNumber or TempResPermitNumber or ForeignIDNumber must be completed",
                isEmpty.and(notTransactionField("Resident.Individual.IDNumber").and(notTransactionField("Resident.Individual.ForeignIDNumber")))).onSection("ABE"),
              failure("ritrpn4", 562, "If the Subject is SDA, TempRespermitNumber must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).onSection("AB")
            ]
          },
          {
            field: "Resident.Individual.ForeignIDNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("rifidn1", 452, "If category is 511/01 to 511/08 or 512/01 to 512/07 or 513 is used, ForeignIDNumber may not be completed",
                notEmpty).onSection("AB").onCategory(["511", "512", "513"]),
              failure("rifidn2", 452, "If the Flow is OUT and category 401 is used, may not be completed",
                notEmpty).onOutflow().onSection("AB").onCategory("401"),
              failure("rifidn3", 294, "If IndividualCustomer is selected, at least one of IDNumber or TempResPermitNumber or ForeignIDNumber must be completed",
                isEmpty.and(notTransactionField("Resident.Individual.IDNumber").and(notTransactionField("Resident.Individual.TempResPermitNumber")))).onSection("ABE"),
              failure("rifidn4", 204, "If the Non Resident AccountIdentifier is NON RESIDENT OTHER or CASH, the Resident IndividualCustomer is completed and the category is 250 or 251, the ForeignIDNumber must be completed",
                isEmpty.and(hasNonResidentFieldValue("AccountIdentifier", ["NON RESIDENT OTHER", "CASH"]))).onSection("A").onCategory(["250", "251"]),
              failure("rifidn5", 562, "If the Subject is SDA, ForeignIDNumber must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).onSection("AB")
            ]
          },
          {
            field: "Resident.Individual.ForeignIDCountry",
            len: 2,
            rules: [
              // NOTE: Removed section E
              failure("rifidc1", 300, "If ForeignIDNumber is completed, must be completed",
                isEmpty.and(hasTransactionField("Resident.Individual.ForeignIDNumber"))).onSection("AB"),
              failure("rifidc2", 238, "Invalid SWIFT country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("ABE"),
              failure("rifidc3", 391, "The ForeignID Country must not be {{Locale}}",
                notEmpty.and(hasValue(map("Locale")))).onSection("ABE")
            ]
          },
          {
            field: "Resident.Individual.PassportNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("ripn1", 301, "If BoPCategory 255 is used and the Flow is OUT, must not be completed (IndividualThirdPartyPassportNumb er must be completed)",
                notEmpty).onOutflow().onSection("AB").onCategory("255"),
              failure("ripn2", 301, "Must not be completed",
                notEmpty).onSection("G"),
              failure("ripn3", 302, "If BoPCategory 256 is used and the PassportNumber is not completed, (account holder is not traveling) the IndividualThirdPartyPassportNumber must be completed.",
                isEmpty.and(not(hasAllMoneyField("ThirdParty.Individual.PassportNumber")))).onSection("AB").onCategory("256")
            ]
          },
          {
            field: "Resident.Individual.PassportCountry",
            len: 2,
            rules: [
              failure("ripc1", 259, "If PassportNumber is completed, must be completed",
                isEmpty.and(hasTransactionField("Resident.Individual.PassportNumber"))).onSection("AB"),
              failure("ripc2", 238, "Invalid SWIFT country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("AB"),
              failure("ripc3", 302, "If BoPCategory 256 is used and the PassportCountry is not completed, (account holder is not traveling) the IndividualThirdPartyPassportCountry must be completed.",
                isEmpty.and(not(hasAllMoneyField("ThirdParty.Individual.PassportCountry")))).onSection("AB").onCategory("256")
            ]
          },
          {
            field: ["Resident.Individual.BeneficiaryID1", "Resident.Individual.BeneficiaryID2", "Resident.Individual.BeneficiaryID3", "Resident.Individual.BeneficiaryID4"],
            rules: [
              deprecated("ribenid", "S05", "This fields is not used for finsurv submissions",
                notEmpty)
            ]
          },
          {
            field: "Resident.Entity.EntityName",
            minLen: 2,
            maxLen: 70,
            rules: [
              failure("relen1", 304, "If Resident EntityCustomer is used, must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("ABC"),
              failure("relen2", 303, "Must not be completed",
                notEmpty).onSection("F"),
              failure("relen3", 261, "If the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then an Entity must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              failure("relen4", 571, "Entity Name must be equal to the registered IHQ name as per the IHQ table if the RegistrationNumber is equal to the IHQ registration number as per the IHQ table",
                notEmpty.and(evalTransactionField("Resident.Entity.RegistrationNumber", isInLookup("ihqCompanies", "registrationNumber"))).and(not(isInLookup("ihqCompanies", "companyName")))).onSection("A"),
              failure("relen5", 447, "If NonResident Exception is IHQ, value must not be same as {{Regulator}}-registered IHQ name",
                notEmpty.and(evalTransactionField("NonResident.Exception.ExceptionName", hasValue("IHQ"))).and(isInLookup("ihqCompanies", "companyName"))).onSection("A"),
              failure("relen6", 255, "The words specified under Non-resident or Resident ExceptionName, excluding STRATE, must not be used.",
                notEmpty.and(notValue('STRATE').and(isExceptionName))).onSection("ABCDEG"),
              // NOTE: Added this rule
              failure("relen7", 303, "If the Reporting Entity Code is 304 or 305, it must not be completed",
                notEmpty).onSection("G").onCategory(["304","305"]),
            ]
          },
          {
            field: "Resident.Entity.TradingName",
            minLen: 2,
            maxLen: 70,
            rules: [
              failure("retn1", 304, "If Resident EntityCustomer is used, must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("ABC")
            ]
          },
          {
            field: "Resident.Entity.RegistrationNumber",
            minLen: 2,
            maxLen: 30,
            rules: [
              failure("rern1", 306, "If Resident EntityCustomer is used, must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("ABEG"),
              failure("rern2", 403, "If Subject is AIRPORT the RegistrationNumber must be GOVERNMENT",
                notValue("GOVERNMENT").and(hasMoneyFieldValue("AdHocRequirement.Subject", "AIRPORT"))).onSection("AB").onCategory("830"),
              failure("rern3", 446, "If NonResident Exception is IHQ, RegistrationNumber must not be same as SARB-registered IHQ registration number",
                notEmpty.and(evalTransactionField("NonResident.Exception.ExceptionName", hasValue("IHQ"))).and(isInLookup("ihqCompanies", "registrationNumber"))).onSection("A"),
              failure("rern4", 219, "Additional spaces identified in data content",
                notEmpty.and(hasAdditionalSpaces)).onSection("ABEG")
            ]
          },
          {
            field: "Resident.Entity.InstitutionalSector",
            len: 2,
            rules: [
              failure("reis1", 308, "Must be completed", isEmpty).onSection("ABE"),
              failure("reis2", 518, "Must not be completed", notEmpty).onSection("F"),
              failure("reis3", 310, "If InstitutionalSectorCode is completed, must be valid", notEmpty.and(notValidInstitutionalSector)).onSection("ABCDEG")
            ]
          },
          {
            field: "Resident.Entity.IndustrialClassification",
            len: 2,
            rules: [
              failure("reic1", 311, "Must be completed", isEmpty).onSection("ABE"),
              failure("reic2", 519, "Must not be completed", notEmpty).onSection("F"),
              // NOTE: Wrong error code, changed 311 to 313
              failure("reic3", 313, "If IndustrialClassification is completed, must be valid", notEmpty.and(notValidIndustrialClassification)).onSection("ABCDEG")
            ]
          },
          {
            field: "Resident.Exception",
            rules: [
              // NOTE: Removed section B
              failure("re1", 315, "Must not contain a value", notEmpty).onSection("EF")
            ]
          },
          {
            field: "Resident.Exception.ExceptionName",
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("ren1", 267, "May only contain one of the specified values",
                notResExceptionName).onSection("ACD"),
              failure("ren2", 316, "If category 250 or 251 is used, may only contain the value MUTUAL PARTY",
                notEmpty.and(notValue("MUTUAL PARTY"))).onSection("A").onCategory(["250", "251"]),
              failure("ren3", 316, "For any BoPCategory other than 200, 250, 251 the value MUTUAL PARTY must not be completed.",
                notEmpty.and(hasValue("MUTUAL PARTY"))).onSection("A").notOnCategory(["200", "250", "251"]),
              failure("ren4", 315, "May not be used. MUTUAL PARTY is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("MUTUAL PARTY"))).onSection("BCDEFG"),
              failure("ren5", 316, "May not be used. BULK PENSIONS is only valid for category 407 or 400",
                notEmpty.and(hasValue("BULK PENSIONS"))).onSection("A").notOnCategory(["400", "407"]),
              failure("ren6", 316, "May only be completed if the category is 100 or 200 or 300 or 400 or 500 or 600 or 700 or 800",
                notEmpty.and(hasValue("UNCLAIMED DRAFTS"))).onSection("A").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              failure("ren7", 315, "May not be completed. FCA NON RESIDENT NON REPORTABLE is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("FCA NON RESIDENT NON REPORTABLE"))).onSection("ABEFG"),
              failure("ren8", 315, "May not be completed. VOSTRO NON REPORTABLE is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("VOSTRO NON REPORTABLE"))).onSection("ABEFG"),
              failure("ren9", 315, "May not be completed. VOSTRO INTERBANK is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("VOSTRO INTERBANK"))).onSection("ABEFG"),
              failure("ren10", 316, "May not be used. BULK INTEREST is only valid for category 309/08 or 300",
                notEmpty.and(hasValue("BULK INTEREST"))).onSection("A").notOnCategory(["309/08", "300"]),
              failure("ren11", 316, "Category may only be 301 or 300",
                notEmpty.and(hasValue("BULK DIVIDENDS"))).onSection("A").notOnCategory(["301", "300"]),
              failure("ren12", 316, "Category may only be 275 or 200",
                notEmpty.and(hasValue("BULK BANK CHARGES"))).onSection("A").notOnCategory(["275", "200"]),
              failure("ren13", 316, "May only be completed if the category is 601/01 or 603/01 or 600",
                notEmpty.and(hasValue("STRATE"))).onSection("A").notOnCategory(["601/01", "603/01", "600"]),
              failure("ren14", 315, "May not be used. RAND CHEQUE is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("RAND CHEQUE"))).onSection("BCDEFG"),
              failure("ren15", 315, "May not be used. BULK PENSIONS is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK PENSIONS"))).onSection("BCDEFG"),
              failure("ren16", 315, "May not be completed except if the NonResident AccountIdentifier is NON RESIDENT RAND, VISA NET or MASTER SEND",
                notEmpty.and(hasValue("NON RESIDENT RAND").and(notNonResidentFieldValue("AccountIdentifier", ["NON RESIDENT RAND", "VISA NET", "MASTER SEND"])))).onSection("ABC"),
              failure("ren17", 315, "May not be used. NON RESIDENT RAND is only applicable for BOPCUS transactions",
                notEmpty.and(hasValue("NON RESIDENT RAND"))).onSection("CDEFG"),
              failure("ren18", 315, "May not be used. UNCLAIMED DRAFTS is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("UNCLAIMED DRAFTS"))).onSection("BCDEFG"),
              failure("ren19", 315, "May not be used. BULK INTEREST is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK INTEREST"))).onSection("BCDEFG"),
              failure("ren20", 315, "May not be used. BULK DIVIDENDS is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK DIVIDENDS"))).onSection("BCDEFG"),
              failure("ren21", 315, "May not be used. BULK BANK CHARGES is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK BANK CHARGES"))).onSection("BCDEFG"),
              failure("ren22", 315, "May not be used. NOSTRO INTERBANK is only applicable for INTERBANK and NON REPORTABLE transactions",
                notEmpty.and(hasValue("NOSTRO INTERBANK"))).onSection("ABEFG"),
              failure("ren23", 315, "May not be used. NOSTRO NON REPORTABLE is only applicable for INTERBANK and NON REPORTABLE transactions",
                notEmpty.and(hasValue("NOSTRO NON REPORTABLE"))).onSection("ABEFG"),
              failure("ren24", 315, "May not be used. RTGS NON REPORTABLE is only applicable for INTERBANK and NON REPORTABLE transactions",
                notEmpty.and(hasValue("RTGS NON REPORTABLE"))).onSection("ABEFG"),
              failure("ren25", 315, "May not be used. STRATE is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("STRATE"))).onSection("BCDEFG"),
              failure("ren26", 315, "For categories 511/01 to 511/08 or 512/01 to 512/07 or 513 the Exception must not be completed",
                notEmpty).onSection("AB").onCategory(["511", "512", "513"]),
              failure("ren27", 315, "If the Subject under the MonetaryDetails is REMITTANCE DISPENSATION and category 251 then MUTUAL PARTY may not be used",
                hasValue("MUTUAL PARTY").and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A").onCategory("251"),
              failure("ren28", 267, "Exception name is case-sensitive and must contain one of the specified values",
                notEmpty.and(notPattern(/^[A-Z][A-Z\s]+[A-Z]$/))),
            ]
          },
          {
            field: "Resident.Exception.Country",
            len: 2,
            rules: [
              failure("rec1", 318, "Must be completed",
                isEmpty).onSection("CD"),
              failure("rec2", 290, "If Resident ExceptionName is VOSTRO NON REPORTABLE or VOSTRO INTERBANK, the Country must not be {{Locale}}",
                notEmpty.and(hasValue(map("Locale"))).and(hasTransactionFieldValue("Resident.Exception.ExceptionName", ["VOSTRO NON REPORTABLE", "VOSTRO INTERBANK"]))).onSection("CD"),
              failure("rec3", 238, "Invalid country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("CD"),
              failure("rec4", 314, "May not be completed",
                notEmpty).onSection("ABEFG")
            ]
          },
          {
            field: "Resident.Exception.AccountIdentifier",
            rules: [
              deprecated("rexai", "S06", "This field is not used for finsurv submissions",
                notEmpty)
            ]
          },
          {
            field: "Resident.Exception.AccountNumber",
            rules: [
              deprecated("rexan", "S07", "This field is not used for finsurv submissions",
                notEmpty)
            ]
          },
          {
            field: ["Resident.Individual", "Resident.Entity"],
            rules: [
              failure("g1", 340, "Must contain at least one of Email, Fax or Telephone",
                notEmpty.and(notResidentField("ContactDetails.Email").and(notResidentField("ContactDetails.Fax").and(notResidentField("ContactDetails.Telephone"))))).onSection("ABEG")
            ]
          },
          {
            field: ["Resident.Individual.AccountName", "Resident.Entity.AccountName"],
            minLen: 2,
            maxLen: 70,
            rules: [
              failure("an1", 319, "Must be completed except if the AccountIdentifier is CASH or EFT or CARD PAYMENT",
                isEmpty.and(notResidentFieldValue("AccountIdentifier", ["CASH", "EFT", "CARD PAYMENT"]))).onSection("ABEG")
            ]
          },
          {
            field: ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("accid1", 272, "Must contain a value of RESIDENT OTHER or CFC RESIDENT or FCA RESIDENT or CASH or EFT or CARD PAYMENT",
                notValueIn(["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "EFT", "CARD PAYMENT"])).onSection("ABG"),
              failure("accid2", 272, "Must contain a value of RESIDENT OTHER or CFC RESIDENT or FCA RESIDENT, CASH or VOSTRO or EFT or CARD PAYMENT",
                notValueIn(["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "VOSTRO", "EFT", "CARD PAYMENT"])).onSection("CD"),
              failure("accid3", 520, "May not be completed", notEmpty).onSection("F"),
              failure("accid4", 272, "Must contain a value DEBIT CARD or CREDIT CARD",
                notValueIn(["DEBIT CARD", "CREDIT CARD"])).onSection("E"),
              failure("accid5", 272, "Invalid AccountIdentifier",
                notValueIn(["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "VOSTRO", "DEBIT CARD", "CREDIT CARD", "EFT", "CARD PAYMENT"])).onSection("ABCDEG"),
              failure("accid6", 355, "If CFC RESIDENT, ForeignValue must be completed",
                hasValue("CFC RESIDENT").and(isCurrencyIn(map("LocalCurrency")))).onSection("ABCD"),
              failure("accid7", 356, "If VOSTRO, {{LocalValue}} must be completed",
                hasValue("VOSTRO").and(notCurrencyIn(map("LocalCurrency")))).onSection("ABCD")
//              failure("accid8", 272, "The Account Identifier cannot be 'FCA RESIDENT' for {{LocalCurrency}} transactions",
//                hasValue("FCA RESIDENT").and(isCurrencyIn(map("LocalCurrency")))).onSection("A")
              //failure("accid9", 568, "If Flow is OUT and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then account identifier must be CASH",
              //    notValue("CASH").and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A")
            ]
          },
          {
            field: "Resident.Entity.AccountIdentifier",
            rules: [
              failure("eaccid1", 521, "If the RegistrationNumber under Resident EntityCustomer is equal to the IHQ registration number as per the IHQ table, the Resident AccountIdentifier must be FCA RESIDENT",
                evalTransactionField("Resident.Entity.RegistrationNumber", isInLookup('ihqCompanies', 'registrationNumber')).
                and(notNonResException("IHQ")).
                and(not(hasValue("FCA RESIDENT")))).onSection("A")
            ]
          },
          {
            field: ["Resident.Individual.AccountNumber", "Resident.Entity.AccountNumber"],

            minLen: 2,
            maxLen: 40,
            rules: [
              failure('accno1', 279, "Must be completed",
                isEmpty).onSection('E'),
              failure('accno2', 522, "Must not be completed",
                notEmpty).onSection('F'),
              failure('accno3', 280, "Must not be equal to AccountNumber under the NonResidentData element",
                notEmpty.and(matchesNonResidentField('AccountNumber'))).onSection('ABCDG'),
              failure('accno4', 279, "Must be completed if the Flow is OUT and the AccountIdentifier under AdditionalCustomerData is not CASH or EFT or CARD PAYMENT",
                isEmpty.and(notResidentFieldValue("AccountIdentifier", ["CASH", "EFT", "CARD PAYMENT"]))).onOutflow().onSection("ABCDG")
            ]
          },
          {
            field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],

            minLen: 2,
            maxLen: 15,
            rules: [
              failure('ccn1', 320, 'Must be completed if Flow is IN and category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106',
                isEmpty).onInflow().onSection("AB").notOnCategory(['101/11', '103/11']).onCategory(['101', '103', '105', '106']),
              failure('ccn2', 320, 'Must be completed if Flow is OUT and category is 101/01 to 101/10 or 102/01 to 102/10 or 103/01 to 103/10 or 104/01 to 104/10 or 105 or 106',
                isEmpty).onOutflow().onSection("AB").notOnCategory(['101/11', '102/11', '103/11', '104/11']).onCategory(['101', '102', '103', '104', '105', '106']),
              failure('ccn3', 322, 'CustomsClientNumber must be numeric and contain between 8 and 13 digits',
                notEmpty.and(notPattern(/^\d{8,13}$/))).onInflow().onSection("AB").notOnCategory(['101/11', '103/11']).onCategory(['101', '103', '105', '106']),
              failure('ccn4', 322, 'CustomsClientNumber must be numeric and contain between 8 and 13 digits',
                notEmpty.and(notPattern(/^\d{8,13}$/))).onOutflow().onSection("AB").notOnCategory(['101/11', '102/11', '103/11', '104/11']).onCategory(['101', '102', '103', '104', '105', '106']),
              warning('ccn5', 322, 'CustomsClientNumber 70707070 should not be regularly used for transactions more than R50,000.00',
                notEmpty.and(hasPattern(/^70707070$/)).and(hasSumLocalValue('>', "50000"))).onSection("AB"),
              failure('ccn6', 321, 'May not be completed',
                notEmpty).onSection("DEF"),
              warning("ccn7", "S09", "Unless the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, this need not be provided and if invalid will cause the SARB to reject transaction",
                notEmpty.and(not(hasAnyMoneyFieldValue("CategoryCode",["101", "103", "105", "106"])))).onInflow().onSection("AB"),
              ignore('ccn8', "S15", 'Check that CustomsClientNumber 70707070 is only used if this transaction is smaller than R50,000.00',
                notEmpty.and(hasPattern(/^70707070$/)).and(hasSumLocalValue('<=', 50000))).onSection("AB"),
              warning("ccn9", "S09", "Unless the category is 101/01 to 101/10 or 102/01 to 102/10 or 103/01 to 103/10 or 104/01 to 104/10 or 105 or 106, this need not be provided and if invalid will cause the SARB to reject transaction",
                notEmpty.and(not(hasAnyMoneyFieldValue("CategoryCode",['101', '102', '103', '104', '105', '106'])))).onOutflow().onSection("AB"),
              warning('ccn10', 322, 'CustomsClientNumber should pass one of the validations for either CCN, ID Number or Tax Number',
                notEmpty.and(hasPattern(/^\d{8,13}$/)).and(not(isValidCCN.or(isValidRSAID.or(isValidZATaxNumber))))).onInflow().onSection("AB").notOnCategory(['101/11', '103/11']).onCategory(['101', '103', '105', '106']),
              warning('ccn11', 322, 'CustomsClientNumber should pass one of the validations for either CCN, ID Number or Tax Number',
                notEmpty.and(hasPattern(/^\d{8,13}$/)).and(not(isValidCCN.or(isValidRSAID.or(isValidZATaxNumber))))).onOutflow().onSection("AB").notOnCategory(['101/11', '102/11', '103/11', '104/11']).onCategory(['101', '102', '103', '104', '105', '106']),
              failure('ccn12', 322,'CustomsClientNumber is 70707070, which implies CCN is not known',
                notEmpty.and(hasPattern(/^70707070$/)))
            ]
          },
          {
            field: "Resident.Individual.TaxNumber",
            minLen: 2,
            maxLen: 30,
            rules: [
              failure('tni1', 324, 'Must be completed if Flow is OUT and category is 512/01 to 512/07 or 513',
                isEmpty).onSection("AB").onCategory(['512', '513']),
              failure('tni2', 523, 'Must not be completed.',
                notEmpty).onSection("F")
            ]
          },
          {
            field: "Resident.Entity.TaxNumber",
            minLen: 2,
            maxLen: 30,
            rules: [
              failure('tne1', 324, 'Must be completed if category is 101/01 to 101/10 or 102/01 to 102/10 or 103/01 to 103/10 or 104/01 to 104/10 or 105 or 106',
                isEmpty).onSection("AB").notOnCategory(['101/11', '102/11', '103/11', '104/11']).onCategory(['101', '102', '103', '104', '105', '106']),
              failure('tne2', 523, 'Must not be completed.',
                notEmpty).onSection("F")
            ]
          },
          {
            field: ["Resident.Individual.TaxNumber", "Resident.Entity.TaxNumber"],
            rules: [
              warning('tn1', 325, 'Incorrect format: Invalid checksum',
                notEmpty.and(not(isValidZATaxNumber))).onSection("AB")
            ]
          },
          {
            field: "Resident.Individual.VATNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              ignore('vni1'), // ignored vni1 so that rules are in line with Resident.Entity.VATNumber
              failure('vni2', 524, 'Must not be completed.',
                notEmpty).onSection("F")
            ]
          },
          {
            field: "Resident.Entity.VATNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure('vne1', 326, 'Must be completed or have "NO VAT NUMBER" if category is 101/01 to 101/10 or 102/01 to 102/10 or 103/01 to 103/10 or 104/01 to 104/10 or 105 or 106',
                isEmpty).onSection("AB").notOnCategory(['101/11', '102/11', '103/11', '104/11']).onCategory(['101', '102', '103', '104', '105', '106']),
              failure('vne2', 524, 'Must not be completed.',
                notEmpty).onSection("F")

            ]
          },
          {
            field: ["Resident.Individual.VATNumber", "Resident.Entity.VATNumber"],
            rules: [
              warning('vn1', 326, 'Incorrect format: Should be "NO VAT NUMBER" or has invalid checksum',
                notEmpty.and(not(hasValue("NO VAT NUMBER").or(isValidZAVATNumber)))).onSection("AB")
            ]
          },
          {
            // TODO: Does not contain all cases as per the spec
            field: ["Resident.Individual.TaxClearanceCertificateIndicator", "Resident.Entity.TaxClearanceCertificateIndicator"],
            len: 1,
            rules: [
              failure('tcci1', 327, 'Must be "Y" or "N" if Flow is OUT and category is 512/01 to 512/07 or 513',
                notValueIn(['Y', 'N'])).onOutflow().onSection("AB").onCategory(['512', '513']),
                // TODO: Should not be on section F              
                failure('tcci2', 525, 'Must not be completed',
                notEmpty).onSection("CDEF")
            ]
          },
          {
            field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
            minLen: 2,
            maxLen: 30,
            rules: [
              failure('tccr1', 249, 'TaxClearanceCertificateIndicator is "Y", so needs to be completed',
                isEmpty.and(hasResidentFieldValue('TaxClearanceCertificateIndicator', 'Y'))).onSection("AB"),
            // TODO: Should not be on section E and F  
              failure('tccr2', 526, 'Must not be completed', notEmpty).onSection("CDEF")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1", "Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1"],
            minLen: 2,
            maxLen: 70,
            rules: [
              failure('a1_1', 332, 'Must be completed', isEmpty).onSection("BEG"),
              failure('a1_2', 527, 'Must not be completed', notEmpty).onSection("DF"),
              failure("a1_3", 332, 'Must be completed (unless REMITTANCE DISPENSATION on a reversal)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onSection("A"),
              failure("a1_4", 332, 'Must be completed (unless REMITTANCE DISPENSATION)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.AddressLine2", "Resident.Entity.StreetAddress.AddressLine2", "Resident.Individual.PostalAddress.AddressLine2", "Resident.Entity.PostalAddress.AddressLine2"],
            minLen: 2,
            maxLen: 70,
            rules: [
              failure('a2_1', 527, 'Must not be completed', notEmpty).onSection("DF")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb", "Resident.Individual.PostalAddress.Suburb", "Resident.Entity.PostalAddress.Suburb"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('s1', 333, 'Must be completed', isEmpty).onSection("BEG"),
              failure('s2', 527, 'May not be completed', notEmpty).onSection("DF"),
              failure('s3', 333, 'Must be completed (unless REMITTANCE DISPENSATION on a reversal)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onSection("A"),
              failure('s4', 333, 'Must be completed (unless REMITTANCE DISPENSATION)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City", "Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('c1', 334, 'Must be completed', isEmpty).onSection("BEG"),
              failure('c2', 527, 'Must not be completed', notEmpty).onSection("DF"),
              failure('c3', 334, 'Must be completed (unless REMITTANCE DISPENSATION on a reversal)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onSection("A"),
              failure('c4', 334, 'Must be completed (unless REMITTANCE DISPENSATION)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.PostalCode", "Resident.Entity.StreetAddress.PostalCode"],
            minLen: 2,
            maxLen: 10,
            rules: [
              failure('spc1', 338, 'Invalid postal code',
                notEmpty.and(notValidPostalCode)).onSection("ABEG"),
              failure('spc2', 527, 'May not be completed',
                notEmpty).onSection("DF"),
              failure('spc3', 338, 'If the Street Province is NAMIBIA or LESOTHO or SWAZILAND then the PostalCode must be 9999',
                hasResidentFieldValue("StreetAddress.Province", ["NAMIBIA", "LESOTHO", "SWAZILAND"]).and(notValue("9999"))).onSection("E"),
              failure('spc4', 338, 'A Postal code of 9999 may not be used for South African province',
                notEmpty.and(hasValue("9999").and(notResidentFieldValue("StreetAddress.Province", ["NAMIBIA", "LESOTHO", "SWAZILAND"])))).onSection("ABEG")
            ]
          },
          {
            field: ["Resident.Individual.PostalAddress.PostalCode", "Resident.Entity.PostalAddress.PostalCode"],
            minLen: 2,
            maxLen: 10,
            rules: [
              failure('pc1', 338, 'Invalid postal code',
                notEmpty.and(notValidPostalCode)).onSection("ABEG"),
              failure('pc2', 337, 'Must be completed',
                isEmpty).onSection("BEG"),
              failure('pc3', 527, 'Must not be completed',
                notEmpty).onSection("DF"),
              failure('pc4', 338, 'If the Street Province is NAMIBIA or LESOTHO or SWAZILAND then the PostalCode must be 9999',
                hasResidentFieldValue("PostalAddress.Province", ["NAMIBIA", "LESOTHO", "SWAZILAND"]).and(notValue("9999"))).onSection("E"),
              failure('pc5', 338, 'A Postal code of 9999 may not be used for South African province',
                notEmpty.and(hasValue("9999").and(notResidentFieldValue("PostalAddress.Province", ["NAMIBIA", "LESOTHO", "SWAZILAND"])))).onSection("ABEG"),
              failure('pc6', 337, 'Must be completed (unless REMITTANCE DISPENSATION on a reversal)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onSection("A"),
              failure('pc7', 337, 'Must be completed (unless REMITTANCE DISPENSATION)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.ContactSurname", "Resident.Entity.ContactDetails.ContactSurname", "Resident.Individual.ContactDetails.ContactName", "Resident.Entity.ContactDetails.ContactName"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('cn1', 339, 'Must be completed',
                isEmpty).onSection("ABE"),
              failure('cn2', 528, 'Must not be completed',
                notEmpty).onSection("DF")
            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.Email",
              "Resident.Entity.ContactDetails.Email"],
            minLen: 2,
            maxLen: 120,
            rules: [
              // NOTE: Changed this message
              failure('cnte1', 340, 'Must contain at least one of Email, Fax or Telephone',
                notResidentField('ContactDetails.Email').and(notResidentField('ContactDetails.Fax')).and(notResidentField('ContactDetails.Telephone'))).onSection("ABEG"),
              failure('cnte2', 528, 'Must not be completed', notEmpty).onSection("DF"),
              failure("cnte3", "E01", "This is not a valid email address",
                  notEmpty.and(notValidEmail))

            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.Fax",
              "Resident.Entity.ContactDetails.Fax",
              "Resident.Individual.ContactDetails.Telephone",
              "Resident.Entity.ContactDetails.Telephone"],
            minLen: 2,
            maxLen: 15,
            rules: [
              failure('cntft1', 340, 'Must contain at least one of Email, Fax or Telephone',
                notResidentField('ContactDetails.Email').and(notResidentField('ContactDetails.Fax')).and(notResidentField('ContactDetails.Telephone'))).onSection("ABEG"),
              failure('cntft2', 528, 'Must not be completed',
                notEmpty).onSection("DF"),
              // NOTE: Changed this message
              failure('cntft3', 341, 'Must be in a 10 to 15 digit format',
                notEmpty.and(notPattern(/^\d{10,15}$/))).onSection("ABEG")
            ]
          },
          {
            field: ["Resident.Individual.CardNumber", "Resident.Entity.CardNumber"],
            minLen: 2,
            maxLen: 20,
            rules: [
              failure('crd1', 342, 'Must be completed', isEmpty).onSection("E"),
              failure('crd2', 344, 'Must not be completed', notEmpty).onSection("ABCDFG")
            ]
          },
          {
            field: ["Resident.Individual.SupplementaryCardIndicator", "Resident.Entity.SupplementaryCardIndicator"],
            len: 1,
            rules: [
              // NOTE: Left more detailed message unchanged
              failure('crdi1', 345, 'Must be set to be either Y or N (blank assumed as N)',
                notEmpty.and(notValueIn(['Y', 'N']))).onSection("E"),
              failure('crdi2', 347, 'Must not be completed',
                notEmpty).onSection("ABCDFG")
            ]
          },
          {
            field: "MonetaryAmount",
            rules: [
              failure('tma1', "S08", 'At least one MonetaryAmount entry must be provided',
                emptyMoneyField),
              // NOTE: Updated the message
              failure('tma2', "349", 'Must contain a sequential number that must start with the value 1 except if the ReplacementTransaction indicator is Y',
                notValidSequenceNumbers)
            ]
          },
          {
            field: "Total{{LocalValue}}",
            rules: [
              warning("tlv1", "DVT", "If the FlowCurrency is not {{LocalCurrency}} then the sum of the Local Monetary Amounts must add up to the Total{{LocalValue}}",
                notEmpty.and(notCurrencyIn(map("LocalCurrency")).and(notSumLocalValue))).onSection("ABCDEG"),
              // failure("tlv2", "DVT", "The Total{{LocalValue}} must be greater than 0.00",
              //   notEmpty.and(not(isGreaterThan(0)))).onSection("ABCDEG")
            ]
          }
        ]
      };


    }
    return stdTrans;
  }
});
