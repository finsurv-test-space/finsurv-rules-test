define(function () {
  return function (predef) {
    var summaryTrans, summaryMoney, summaryImportExport;
    with (predef) {
      summaryTrans = {
        ruleset: "Summary Transaction Display Rules",
        scope: "transaction",
        fields: [
          {
            field: "MainTransferPurpose",
            display: [
              show().onSection("ABDG"),
              hide().onSection("CEF")
            ]
          },
          {
            field: "NonResidentTitle",
            displayOnly: true,
            display: [
              setValue("Beneficiary Details").onOutflow(),
              setValue("Remitter Details").onInflow()
            ]
          },
          {
            field: "Resident.Description",
            displayOnly: true,
            display: [
              appendValue("%s", "Resident.Individual.Name", hasTransactionField("Resident.Individual")),
              appendValue("%s", "Resident.Individual.Surname", hasTransactionField("Resident.Individual")),
              appendValue("%s", "Resident.Entity.EntityName", hasTransactionField("Resident.Entity")),
              appendValue("t/a %s", "Resident.Entity.TradingName", hasTransactionField("Resident.Entity.TradingName")),
              appendValue("%s", "Resident.Exception.ExceptionName", hasTransactionField("Resident.Exception"))
            ]
          },
          {
            field: "Resident.AccountDescription",
            displayOnly: true,
            display: [
              appendValue("%s", "Resident.Individual.AccountIdentifier", hasTransactionField("Resident.Individual")),
              appendValue("[%s]", "Resident.Individual.AccountNumber", hasTransactionField("Resident.Individual")),
              appendValue("%s", "Resident.Individual.AccountName", hasTransactionField("Resident.Individual")),
              appendValue("%s", "Resident.Entity.AccountIdentifier", hasTransactionField("Resident.Entity")),
              appendValue("[%s]", "Resident.Entity.AccountNumber", hasTransactionField("Resident.Entity")),
              appendValue("%s", "Resident.Entity.AccountName", hasTransactionField("Resident.Entity"))
            ]
          },
          {
            field: "Resident.ContactDetailsDescription",
            displayOnly: true,
            display: [
              appendValue("%s", "Resident.Individual.ContactDetails.ContactName", hasTransactionField("Resident.Individual")),
              appendValue("%s", "Resident.Individual.ContactDetails.ContactSurname", hasTransactionField("Resident.Individual")),
              appendValue("email: %s", "Resident.Individual.ContactDetails.Email", hasTransactionField("Resident.Individual.ContactDetails.Email")),
              appendValue("tel: %s", "Resident.Individual.ContactDetails.Telephone", hasTransactionField("Resident.Individual.ContactDetails.Telephone")),
              appendValue("fax: %s", "Resident.Individual.ContactDetails.Fax", hasTransactionField("Resident.Individual.ContactDetails.Fax")),
              appendValue("%s", "Resident.Entity.ContactDetails.ContactName", hasTransactionField("Resident.Entity")),
              appendValue("%s", "Resident.Entity.ContactDetails.ContactSurname", hasTransactionField("Resident.Entity")),
              appendValue("email: %s", "Resident.Entity.ContactDetails.Email", hasTransactionField("Resident.Entity.ContactDetails.Email")),
              appendValue("tel: %s", "Resident.Entity.ContactDetails.Telephone", hasTransactionField("Resident.Entity.ContactDetails.Telephone")),
              appendValue("fax: %s", "Resident.Entity.ContactDetails.Fax", hasTransactionField("Resident.Entity.ContactDetails.Fax"))
            ]
          },
          {
            field: "NonResident.Description",
            displayOnly: true,
            display: [
              appendValue("%s", "NonResident.Individual.Name", hasTransactionField("NonResident.Individual")).onSection("ABCDG"),
              appendValue("%s", "NonResident.Individual.Surname", hasTransactionField("NonResident.Individual")).onSection("ABCDG"),
              appendValue("%s", "NonResident.Entity.EntityName", hasTransactionField("NonResident.Entity")).onSection("ABCDG"),
              appendValue("%s", "NonResident.Entity.CardMerchantName", hasTransactionField("NonResident.Entity.CardMerchantName")).onSection("E"),
              appendValue("%s", "NonResident.Exception.ExceptionName", hasTransactionField("NonResident.Exception")).onSection("ABCDG")
            ]
          },
          {
            field: "NonResident.Account",
            display: [
              show(notTransactionField("NonResident.Exception")),
              hide(hasTransactionField("NonResident.Exception"))
            ]
          },
          {
            field: "NonResident.AccountDescription",
            displayOnly: true,
            display: [
              appendValue("%s", "NonResident.Individual.AccountIdentifier", hasTransactionField("NonResident.Individual")),
              appendValue("[%s]", "NonResident.Individual.AccountNumber", hasTransactionField("NonResident.Individual")),
              appendValue("%s", "NonResident.Entity.AccountIdentifier", hasTransactionField("NonResident.Entity")),
              appendValue("[%s]", "NonResident.Entity.AccountNumber", hasTransactionField("NonResident.Entity"))
            ]
          },
          {
            field: "NonResident.Address",
            display: [
              show(notTransactionField("NonResident.Exception")),
              hide(hasTransactionField("NonResident.Exception"))
            ]
          },
          {
            field: "Resident.Account",
            display: [
              show(notTransactionField("Resident.Exception")),
              hide(hasTransactionField("Resident.Exception"))
            ]
          },
          {
            field: "Resident.ContactDetails",
            display: [
              show(notTransactionField("Resident.Exception")),
              hide(hasTransactionField("Resident.Exception"))
            ]
          },
          {
            field: "resident_table",
            display: [
              show(),
              hide().onSection("F")
            ]
          },
          {
            field: "non_resident_table",
            display: [
              show(),
              hide().onSection("F")
            ]
          }
        ]
      };

      summaryMoney = {
        ruleset: "Summary Money Display Rules",
        scope: "money",
        fields: [
          {
            field: "AddMoneyButton",
            displayOnly: true,
            display: [
              hide(notEmptyMoneyField).onSection("CDEFG")
            ]
          },
          {
            field: "Description",
            displayOnly: true,
            display: [
              appendValue("Loan Ref: %s", "LoanRefNumber").onSection("AB").onCategory(["801", "802", "803", "804"]),
              appendValue("Loan Ref: %s", "LoanRefNumber").onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              appendValue("Loan Tenor: %s", "LoanTenor").onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
              appendValue("Loan Int: %s", "LoanInterestRate").onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
              appendValue("Loan Int: %s", "LoanInterestRate").onOutflow().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
              appendValue("Loan Int: %s", "LoanInterestRate").onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
              appendValue("CCN: %s", "transaction::Resident.Individual.CustomsClientNumber").onInflow().onSection("AB").onCategory(["101", "103", "105", "106"]),
              appendValue("CCN: %s", "transaction::Resident.Individual.CustomsClientNumber").onOutflow().onSection("AB").onCategory(["101", "102", "103", "104", "105", "106"]),
              appendValue("CCN: %s", "transaction::Resident.Entity.CustomsClientNumber").onInflow().onSection("AB").onCategory(["101", "103", "105", "106"]),
              appendValue("CCN: %s", "transaction::Resident.Entity.CustomsClientNumber").onOutflow().onSection("AB").onCategory(["101", "102", "103", "104", "105", "106"]),
              appendValue("CCN2: %s", "ThirdParty.CustomsClientNumber").onSection("AB"),
              appendValue("Ruling: %s", "{{Regulator}}Auth.RulingsSection"),
              appendValue("Reversal: %s", "ReversalTrnRefNumber").onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"])
            ]
          },
          {
            field: "ThirdPartyDescription",
            displayOnly: true,
            display: [
              appendValue("Individual: %s", "ThirdParty.Individual.Surname", notMoneyField("ThirdParty.Entity.Name")),
              appendValue("%s", "ThirdParty.Individual.Name", notMoneyField("ThirdParty.Entity.Name")),
              appendValue("Entity: %s", "ThirdParty.Entity.Name", hasMoneyField("ThirdParty.Entity.Name"))
            ]
          },
          {
            field: "ThirdPartyContact",
            displayOnly: true,
            display: [
              hide(),
              appendValue("%s", "ThirdParty.ContactDetails.ContactSurname", hasMoneyField("ThirdParty.ContactDetails.ContactSurname")),
              appendValue("%s", "ThirdParty.ContactDetails.ContactName", hasMoneyField("ThirdParty.ContactDetails.ContactName")),
              show(hasMoneyField("ThirdParty.Individual.Surname").or(hasMoneyField("ThirdParty.Entity.Name"))),
              show(hasTransactionField("Resident.Entity")).onOutflow().onSection("AB").onCategory(["511", "512", "513"]),
              show().onSection("AB").onCategory(["255", "256"]),
              show(hasResidentFieldValue("SupplementaryCardIndicator", "Y")).onSection("E")
            ]
          },
          {
            field: "ImportExport",
            displayOnly: true,
            display: [
              hide(),
              clearValue(""),
              setValue("multiple", null,
                multipleImportExport),
              setValue("ICN: %s", "importexport::ImportControlNumber",
                singleImportExport).onOutflow().onSection("ABG").onCategory(["101"]),
              setValue("MRN: %s", "importexport::ImportControlNumber",
                singleImportExport).onOutflow().onSection("AB").onCategory(["103", "105", "106"]),
              setValue("UCR: %s", "importexport::UCR",
                singleImportExport).onInflow().onSection("ABG").onCategory(["101", "103", "105", "106"]),
              setValue("Transport DN: %s", "importexport::TransportDocumentNumber",
                singleImportExport).onOutflow().onSection("ABG").onCategory(["103", "105", "106"]),
              show(notEmptyImportExport),
              show().onSection("ABG").onCategory(["101", "103", "105", "106"])
            ]
          },
          {
            field: "IE_PaymentValueHeading",
            displayOnly: true,
            display: [
              setLabel("Allocated Amount")
            ]
          }
        ]
      };

      summaryImportExport = {
        ruleset: "Summary Import Export Display Rules",
        scope: "importexport",
        fields: [
          {
            field: "Description",
            displayOnly: true,
            display: [
              clearValue(""),
              setValue("ICN: %s", "ImportControlNumber").onOutflow().onSection("ABG").onCategory(["101"]),
              setValue("MRN: %s", "ImportControlNumber").onOutflow().onSection("AB").onCategory(["103", "105", "106"]),
              setValue("UCR: %s", "UCR").onInflow().onSection("ABG").onCategory(["101", "103", "105", "106"]),
              setValue("Transport DN: %s", "TransportDocumentNumber").onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
            ]
          }
        ]
      };


    }

    return {
      summaryTrans: summaryTrans,
      summaryMoney: summaryMoney,
      summaryImportExport: summaryImportExport
    }
  }
})
;