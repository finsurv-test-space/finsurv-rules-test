define(function () {
  return function (predef) {
    var filterLookupRules;
    with (predef) {
      filterLookupRules = {
        filterLookupTrans: {
          ruleset: "Reporting Transaction Lookup Filter Rules",
          scope: "transaction",
          fields: [
            {
              field: "OriginatingCountry",
              display: [
                limitValue([getMap("Locale")]).onOutflow().onSection("ABG"),
                excludeValue([getMap("Locale")]).onInflow().onSection("ABG")
              ]
            },
            {
              field: "ReceivingCountry",
              display: [
                limitValue([getMap("Locale")]).onInflow().onSection("ABG"),
                excludeValue([getMap("Locale")]).onOutflow().onSection("ABG")
              ]
            },
            {
              field: "LocationCountry",
              display: [
                excludeValue([getMap("Locale")], not(hasTransactionFieldValue("NonResident.Exception.ExceptionName", "IHQ"))).onSection("ABG"),
                excludeValue(['EU']).onSection("ABG").notOnCategory("513")
              ]
            },
            {
             field  : "NonResident.Exception.ExceptionName",
             display: [
               limitValue(["MUTUAL PARTY"]).onInflow().onSection("A").onCategory("252"),
               //excludeValue(["MUTUAL PARTY"]).onSection("A").notOnCategory(["200", "252", "255", "256", "530/05"]),
               excludeValue(["BULK INTEREST"]).onSection("A").notOnCategory(["300", "309/08"]),
               excludeValue(["BULK VAT REFUNDS"]).onSection("A").notOnCategory(["400", "411/02"]),
               excludeValue(["BULK BANK CHARGES"]).onSection("A").notOnCategory(["200", "275"]),
               excludeValue(["BULK PENSIONS"]).onSection("A").notOnCategory(["400", "407"]),
               excludeValue(["STRATE"]).onSection("A").notOnCategory(["601/01", "603/01"]),
               excludeValue(["MUTUAL PARTY", "BULK INTEREST", "BULK VAT REFUNDS", "BULK BANK CHARGES", "BULK PENSIONS", "STRATE"]).onSection("BCDEFG"),
               excludeValue(["FCA RESIDENT NON REPORTABLE", "CFC RESIDENT NON REPORTABLE", "VOSTRO NON REPORTABLE", "NOSTRO NON REPORTABLE", "RTGS NON REPORTABLE"]).onSection("ABDEFG"),
               excludeValue(["VOSTRO INTERBANK", "NOSTRO INTERBANK"]).onSection("ABEFG")
             ]
            },
            {
              field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
              display: [
                excludeValue(["CARD DIRECT"]).onOutflow().onSection("ACDG"),
                limitValue(["NON RESIDENT RAND"]).onSection("B"),
                limitValue(["CARD DIRECT", "VISA NET", "MASTER SEND"]).onSection("E")
              ]
            },
            {
              field: ["NonResident.Individual.Address.Country", "NonResident.Entity.Address.Country"],
              display: [ // ZA specific
                excludeValue([getMap("Locale")]).onInflow().onSection("ABCDG"),
                //excludeValue(["ZA"], notTransactionFieldValue("Resident.Individual.ForeignIDCountry", ["NA", "LS", "SZ"])).onSection("E"),
                excludeValue(["EU"]).onOutflow().onSection("A").notOnCategory("513"),
                excludeValue(["EU"]).onInflow().onSection("A").notOnCategory("517")
              ]
            },
            {
              field: "Resident.Exception.ExceptionName",
              display: [
                limitValue(["MUTUAL PARTY"]).onSection("A").onCategory(["250", "251"]),
                excludeValue(["MUTUAL PARTY"]).onSection("A").notOnCategory(["250", "251"]),
               // excludeValue(["MUTUAL PARTY", "NON RESIDENT RAND", "RAND CHEQUE", "BULK PENSIONS", "UNCLAIMED DRAFTS", "BULK INTEREST", "BULK DIVIDENDS", "BULK BANK CHARGES", "STRATE"]).onSection("BCDEFG"),
                excludeValue(["BULK PENSIONS"]).onSection("A").notOnCategory(["400", "407"]),
                excludeValue(["UNCLAIMED DRAFTS"]).onSection("A").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
                excludeValue(["FCA NON RESIDENT NON REPORTABLE", "VOSTRO NON REPORTABLE", "VOSTRO INTERBANK", "NOSTRO INTERBANK", "NOSTRO NON REPORTABLE", "RTGS NON REPORTABLE"]).onSection("ABEFG"),
                excludeValue(["BULK INTEREST"]).onSection("A").notOnCategory(["309/08", "300"]),
                excludeValue(["BULK DIVIDENDS"]).onSection("A").notOnCategory(["301", "300"]),
                excludeValue(["BULK BANK CHARGES"]).onSection("A").notOnCategory(["275", "200"]),
                excludeValue(["STRATE"]).onSection("A").notOnCategory(["601/01", "603/01", "600"])
              ]
            },
            {
              field: "Resident.Exception.Country",
              display: [
                excludeValue([getMap("Locale")], hasTransactionFieldValue("Resident.Exception.ExceptionName", ['VOSTRO NON REPORTABLE', 'VOSTRO INTERBANK'])).onSection("CD")
              ]
            },
            {
              field: ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
              display: [
                limitValue(["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "EFT", "CARD PAYMENT"]).onSection("ABG"),
                limitValue(["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "EFT", "CARD PAYMENT", "VOSTRO"]).onSection("CD"),
                limitValue(["DEBIT CARD", "CREDIT CARD"]).onSection("E")
              ]
            }
            // ,
            // {
            //   field: ["Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province", "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
            //   display: [
            //     limitValue(["GAUTENG", "LIMPOPO", "NORTH WEST", "WESTERN CAPE", "EASTERN CAPE", "NORTHERN CAPE", "FREE STATE", "MPUMALANGA", "KWAZULU NATAL"]).onSection("ABG"),
            //     limitValue(["GAUTENG", "LIMPOPO", "NORTH WEST", "WESTERN CAPE", "EASTERN CAPE", "NORTHERN CAPE", "FREE STATE", "MPUMALANGA", "KWAZULU NATAL"], notTransactionField("Resident.Individual")).onSection("E")
            //   ]
            // }
          ]
        },

        filterLookupMoney: {
          ruleset: "Reporting Monetary Lookup Filter Rules",
          scope: "money",
          fields: [
            {
              field: "MoneyTransferAgentIndicator",
              display: [
                limitValue(["CARD"]).onSection("EF"),
                limitValue(["BOPDIR"]).onSection("G"),
                excludeValue(["AD", "ADLA", "BOPDIR"], dealerTypeAD).onSection("ABCD").onCategory("833"),
                excludeValue(["ADLA", "CARD", "BOPDIR"], dealerTypeAD).onSection("ABCD").notOnCategory("833")
              ]
            },
            {
              field: "IECurrencyCode",
              display: [
                filterValue(function(itemVal,context){
                  return (itemVal == context.getTransactionField("FlowCurrency") || itemVal=='ZAR')
                })
              ]
            },
            {
               field  : "CompoundCategoryCode",
               display: [
                 excludeValue(['ZZ1'], not(singleMonetaryAmount))
               ]
            }
            //{
            //  field  : "CompoundCategoryCode",
            //  display: [
            //      //#dummyData 18
            //      excludeValue(['102,104'],
            //          notImportUndertakingClient
            //          .and(notCustomValue("ignoreLUClient", true)))
            //      .onOutflow().onSection("ABG"),
            //
            //      //#dummyData 19
            //    limitValue(['102,104'], importUndertakingClient).onOutflow().onSection("ABG"),
            //
            //      excludeValue(['102,104'],
            //          notImportUndertakingClient
            //          .and(notCustomValue("ignoreLUClient", true)))
            //      .onInflow().onSection("ABG"),
            //
            //    limitValue(['102,104'], importUndertakingClient)
            //        .onInflow().onSection("ABG"),
            //
            //      //#dummyData 14, 20
            //      //excludeValue(['514,515,ZZ1,ZZ2,100,200,201,202,203,204,205,210,211,212,213,220,221,225,226,230,231,232,233,234,235,236,240,240,241,242,243,250,251,255,256,260,261,265,266,270,271,272,273,275,276,280,281,282,285,287,288,289,290,291,292,293,294,295,296,297,300,301,302,303,304,305,306,307,308,309,312,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,500,501,502,503,504,510,511,512,513,530,600,601,602,603,605,610,611,612,615,616,700,701,702,703,704,705,800,801,802,803,804,810,815,816,817,818,819,830,831,833']
            //      limitValue(['100,101,102,103,104,105,106,107,108,109,110']
            //          , hasCustomValue("tradeOnly", true))
            //          .onOutflow()
            //          .onSection("ABG"),
            //
            //      limitValue(['101,103,106,110,242']
            //          , hasCustomValue("tradeOnly", true))
            //          .onInflow()
            //          .onSection("ABG"),
            //
            //      limitValue(['ZZ1']).onSection("C"),
            //
            //      excludeValue(['ZZ1','ZZ2']).onSection("ABDEF")
            //  ]
            //},
            //{
            //  field  : "LocationCountry",
            //  display: [
            //    excludeValue(['ZA']).onSection("ABG"),
            //    excludeValue(['EU']).onSection("ABG").notOnCategory("513")
            //  ]
            //}
          ]
        },

        filterLookupImportExport: {
          ruleset: "Reporting Import Export Lookup Filter Rules",
          scope: "importexport",
          fields: []
        }
      }
    }

    return filterLookupRules;
  }
});


