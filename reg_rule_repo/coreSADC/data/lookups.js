define({
  yesNo                        : ['Y', 'N'],
  gender                       : ['M', 'F'],
  res_types                    : ['Individual', 'Entity', 'Exception'],
  //ReportingQualifier : ["BOPCUS", "NON RESIDENT RAND", "NON REPORTABLE", "INTERBANK", "BOPCARD RESIDENT", "BOPCARD NON RESIDENT", "DIRECT"],
  ReportingQualifier           : ["BOPCUS", "NON REPORTABLE", "BOPCARD RESIDENT", "BOPCARD NON RESIDENT", "NON RESIDENT RAND", "INTERBANK", "BOPDIR"],
  Flow                         : ['IN', 'OUT'],
  currencies                   : [
    {code: 'ADP', name: 'Andorran Peseta'},
    {code: 'AED', name: 'Arab Emirates Dirham'},
    {code: 'AFA', name: 'Afghani'},
    {code: 'AFN', name: 'Afghani'},
    {code: 'ALL', name: 'Lek'},
    {code: 'AMD', name: 'Armenian Dram'},
    {code: 'ANG', name: 'Netherlans Antillan Guilder'},
    {code: 'AOA', name: 'Kwanza'},
    {code: 'AON', name: 'New Kwanza'},
    {code: 'AOR', name: 'Kwanza Readjustado'},
    {code: 'ARS', name: 'Argentine Peso'},
    {code: 'ATS', name: 'Austrian Schilling'},
    {code: 'AUD', name: 'Australian Dollar'},
    {code: 'AWG', name: 'Aruban Guilder'},
    {code: 'AZM', name: 'Azerbaijanian Manat'},
    {code: 'AZN', name: 'Azerbaijanian Manat (ISO CODE INCORRECT)'},
    {code: 'BAM', name: 'Convertible Marks'},
    {code: 'BBD', name: 'Barbados Dollar'},
    {code: 'BDT', name: 'Taka'},
    {code: 'BEF', name: 'Belgian Franc'},
    {code: 'BGL', name: 'Lev'},
    {code: 'BGN', name: 'Bulgarian Lev'},
    {code: 'BHD', name: 'Bahraini'},
    {code: 'BIF', name: 'Burundi Franc'},
    {code: 'BMD', name: 'Bermudian Dollat'},
    {code: 'BND', name: 'Brunei Dollar'},
    {code: 'BOB', name: 'Boliviano'},
    {code: 'BOV', name: 'Mvdol'},
    {code: 'BRL', name: 'Brazilian Real'},
    {code: 'BSD', name: 'Bahamian Dollar'},
    {code: 'BTN', name: 'Ngultrum'},
    {code: 'BWP', name: 'Botswana Pula'},
    {code: 'BYB', name: 'Belarussian Ruble'},
    {code: 'BYR', name: 'Belarussian Ruble'},
    {code: 'BZD', name: 'Belize Dollar'},
    {code: 'CAD', name: 'Canadian Dollar'},
    {code: 'CDF', name: 'Frank Congalais'},
    {code: 'CHF', name: 'Swiss Frank'},
    {code: 'CLF', name: 'Unidades De Fomento'},
    {code: 'CLP', name: 'Chillean Peso'},
    {code: 'CNY', name: 'Yuan Renminbi'},
    {code: 'COP', name: 'Colombian Peso'},
    {code: 'COU', name: 'Unidad De Valor Real'},
    {code: 'CRC', name: 'Costa Rican Colon'},
    {code: 'CSD', name: 'Serbian Dinar'},
    {code: 'CUC', name: 'Peso Convertible'},
    {code: 'CUP', name: 'Cuban Peso'},
    {code: 'CVE', name: 'Cape Verde Escudo'},
    {code: 'CYP', name: 'Cyprus Pound'},
    {code: 'CZK', name: 'Czech Koruna'},
    {code: 'DEM', name: 'German Deutsche Mark'},
    {code: 'DJF', name: 'Djibouti Franc'},
    {code: 'DKK', name: 'Danish Krone'},
    {code: 'DOP', name: 'Dominican Peso'},
    {code: 'DZD', name: 'Algerian Dinar'},
    {code: 'ECS', name: 'Sucre'},
    {code: 'ECV', name: 'Unidad De Valor Constante'},
    {code: 'EEK', name: 'Kroon'},
    {code: 'EGP', name: 'Egyptian Pound'},
    {code: 'ERN', name: 'Nafka'},
    {code: 'ESP', name: 'Spanish Peseta'},
    {code: 'ETB', name: 'Ethiopian Birr'},
    {code: 'EUR', name: 'Euro'},
    {code: 'FIM', name: 'Finnish Markka'},
    {code: 'FJD', name: 'Fiji Dollar'},
    {code: 'FKP', name: 'Falkland Islands Pound'},
    {code: 'FRF', name: 'French Frank'},
    {code: 'GBP', name: 'British Pound Sterling'},
    {code: 'GEL', name: 'Lari'},
    {code: 'GHC', name: 'Cedi'},
    {code: 'GHS', name: 'Ghana Cedi'},
    {code: 'GIP', name: 'Gibraltar Pound'},
    {code: 'GMD', name: 'Dalasi'},
    {code: 'GNF', name: 'Guinea Franc'},
    {code: 'GRD', name: 'Greek Drachma'},
    {code: 'GTQ', name: 'Quetzal'},
    {code: 'GWP', name: 'Guinea-Bissau Peso'},
    {code: 'GYD', name: 'Guyana Dollor'},
    {code: 'HKD', name: 'Hong Kong Dollar'},
    {code: 'HNL', name: 'Lempira'},
    {code: 'HRK', name: 'Kuna'},
    {code: 'HTG', name: 'Gourde'},
    {code: 'HUF', name: 'Hungarian Forint'},
    {code: 'IDR', name: 'Rupiah'},
    {code: 'IEP', name: 'Irish Pound'},
    {code: 'ILS', name: 'New Israeli Shekel'},
    {code: 'INR', name: 'Indian Rupee'},
    {code: 'IQD', name: 'Iraqi Dinar'},
    {code: 'IRR', name: 'Iranian Rial'},
    {code: 'ISK', name: 'Iceland Krona'},
    {code: 'ITL', name: 'Italian Lira'},
    {code: 'JMD', name: 'Jamaican Dollar'},
    {code: 'JOD', name: 'Jordian Dinar'},
    {code: 'JPY', name: 'Japanese Yen'},
    {code: 'KES', name: 'Kenyan Shilling'},
    {code: 'KGS', name: 'Som'},
    {code: 'KHR', name: 'Riel'},
    {code: 'KMF', name: 'Comoro Franc'},
    {code: 'KPW', name: 'North Korean Won'},
    {code: 'KRW', name: 'Won'},
    {code: 'KWD', name: 'Kuwaiti Dinr'},
    {code: 'KYD', name: 'Cayman Islands Dollar'},
    {code: 'KZT', name: 'Tenge'},
    {code: 'LAK', name: 'Kip'},
    {code: 'LBP', name: 'Lebanese Pound'},
    {code: 'LKR', name: 'Sri Lanka Rupee'},
    {code: 'LRD', name: 'Liberian Dollar'},
    {code: 'LSL', name: 'Lesotho Loti'},
    {code: 'LTL', name: 'Lithuanian Litas'},
    {code: 'LUF', name: 'Luxembourg Franc'},
    {code: 'LVL', name: 'Latvian Lats'},
    {code: 'LYD', name: 'Libyan Dinar'},
    {code: 'MAD', name: 'Moroccan Dirham'},
    {code: 'MDL', name: 'Moldovan Leu'},
    {code: 'MGA', name: 'Madagascar Ariary'},
    {code: 'MGF', name: 'Malagasy Franc'},
    {code: 'MKD', name: 'Denar'},
    {code: 'MMK', name: 'Kyat'},
    {code: 'MNT', name: 'Tugrik'},
    {code: 'MOP', name: 'Pataca'},
    {code: 'MRO', name: 'Ouguiya'},
    {code: 'MRU', name: 'Ouguiya'},
    {code: 'MTL', name: 'Maltese Lira'},
    {code: 'MUR', name: 'Mauritius Rupee'},
    {code: 'MVR', name: 'Rufiyaa'},
    {code: 'MWK', name: 'Malawi Kwacha'},
    {code: 'MXN', name: 'Mexican Peso'},
    {code: 'MXV', name: 'Mexican Unidad De Inversion'},
    {code: 'MYR', name: 'Malaysian Ringgit'},
    {code: 'MZM', name: 'Metical'},
    {code: 'MZN', name: 'New Mozambican Metical'},
    {code: 'NAD', name: 'Namibian Dollar'},
    {code: 'NGN', name: 'Naira'},
    {code: 'NIO', name: 'Cordoba Oro'},
    {code: 'NLG', name: 'Dutch Guilder'},
    {code: 'NOK', name: 'Norwegian Krone'},
    {code: 'NPR', name: 'Nepalese Rupee'},
    {code: 'NZD', name: 'New Zealand Dollar'},
    {code: 'OMR', name: 'Rial Omani'},
    {code: 'PAB', name: 'Balboa'},
    {code: 'PEN', name: 'Nuevo Sol'},
    {code: 'PGK', name: 'Kina'},
    {code: 'PHP', name: 'Philippine Peso'},
    {code: 'PKR', name: 'Pakistan Rupee'},
    {code: 'PLN', name: 'Polish Zloty'},
    {code: 'PTE', name: 'Portuguese Escudo'},
    {code: 'PYG', name: 'Guarani'},
    {code: 'QAR', name: 'Qatari Rial'},
    {code: 'RMB', name: 'Chinese Renminbi'},
    {code: 'ROL', name: 'Leu'},
    {code: 'RON', name: 'Romanian Leu'},
    {code: 'RSD', name: 'Serbian Dinar'},
    {code: 'RUB', name: 'Russian Ruble (New)'},
    {code: 'RUR', name: 'Russian Rubble (Old)'},
    {code: 'RWF', name: 'Rwanda Franc'},
    {code: 'SAR', name: 'Saudi Riyal'},
    {code: 'SBD', name: 'Soloman Island Dollar'},
    {code: 'SCR', name: 'Seychelles Rupee'},
    {code: 'SDD', name: 'Sudanese Dinar'},
    {code: 'SDG', name: 'Sudanese Pound'},
    {code: 'SEK', name: 'Swedish Krona'},
    {code: 'SGD', name: 'Singapore Dollar'},
    {code: 'SHP', name: 'St Helena Pound'},
    {code: 'SIT', name: 'Tolar'},
    {code: 'SKK', name: 'Slovak Koruna'},
    {code: 'SLL', name: 'Leone'},
    {code: 'SOS', name: 'Somali Shilling'},
    {code: 'SRD', name: 'Suriname Dollar'},
    {code: 'SRG', name: 'Suriname Guilder'},
    {code: 'SSP', name: 'South Sudanese Pound'},
    {code: 'STD', name: 'Dobra'},
    {code: 'STN', name: 'Dobra'},
    {code: 'SVC', name: 'El Salvador Colon'},
    {code: 'SYP', name: 'Syrian Pound'},
    {code: 'SZL', name: 'Swaziland Lilangeni'},
    {code: 'THB', name: 'Thai Baht'},
    {code: 'TJR', name: 'Tajik Ruble'},
    {code: 'TJS', name: 'Somoni'},
    {code: 'TMM', name: 'Manat'},
    {code: 'TMT', name: 'Turkmenistan New Manat'},
    {code: 'TND', name: 'Tunisian Dinar'},
    {code: 'TOP', name: 'Pa Anga'},
    {code: 'TPE', name: 'Timor Escudo'},
    {code: 'TRL', name: 'Turkish Lira'},
    {code: 'TRY', name: 'Turkish New Lira'},
    {code: 'TTD', name: 'Trinidad And Tobago Dollar'},
    {code: 'TWD', name: 'Taiwan Dollar'},
    {code: 'TZS', name: 'Tanzanian Shilling'},
    {code: 'UAF', name: 'Hryvnia'},
    {code: 'UAH', name: 'Hryvnia'},
    {code: 'UGX', name: 'Uganda Shilling'},
    {code: 'USD', name: 'US Dollar'},
    {code: 'USN', name: 'US Dollar Next Day Funds'},
    {code: 'UYU', name: 'Peso Uruguayo'},
    {code: 'UZS', name: 'Uzbekistan Sum'},
    {code: 'VEB', name: 'Bolivar'},
    {code: 'VEF', name: 'Bolivar Fuerte'},
    {code: 'VND', name: 'Dong'},
    {code: 'VUV', name: 'Vatu'},
    {code: 'WST', name: 'Tala'},
    {code: 'XAF', name: 'CFA Franc BEAC'},
    {code: 'XCD', name: 'East Caribbean Dollar'},
    {code: 'XDR', name: 'SDR (Special Drawing Right)'},
    {code: 'XOF', name: 'CFA Franc BCEAO'},
    {code: 'XPF', name: 'CFP Franc'},
    {code: 'XSU', name: 'Sucre'},
    {code: 'YER', name: 'Yemeni Rial'},
    {code: 'YUM', name: 'New Yugoslavian Dinar'},
    {code: 'ZAR', name: 'South African Rand'},
    {code: 'ZMK', name: 'Zambian Kwacha'},
    {code: 'ZMW', name: 'Zambian Kwacha New'},
    {code: 'ZWD', name: 'Zimbabwe Dollar'},
    {code: 'ZWL', name: 'New Zimbabwe Dollar'},
    {code: 'ZWR', name: 'Zimbabwean Dollar'}
  ],
  countries                    : [
    {code: 'AD', name: 'Andorra'},
    {code: 'AE', name: 'United Arab Emirates'},
    {code: 'AF', name: 'Afghanistan'},
    {code: 'AG', name: 'Antigua and Barbuda'},
    {code: 'AI', name: 'Anguilla'},
    {code: 'AL', name: 'Albania'},
    {code: 'AM', name: 'Armenia'},
    {code: 'AN', name: 'Netherlands Antilles'},
    {code: 'AO', name: 'Angola'},
    {code: 'AQ', name: 'Antarctica'},
    {code: 'AR', name: 'Argentina'},
    {code: 'AS', name: 'American Samoa'},
    {code: 'AT', name: 'Austria'},
    {code: 'AU', name: 'Australia'},
    {code: 'AW', name: 'Aruba'},
    {code: 'AX', name: 'Åland Islands'},
    {code: 'AZ', name: 'Azerbaijan'},
    {code: 'BA', name: 'Bosnia and Herzegovina'},
    {code: 'BB', name: 'Barbados'},
    {code: 'BD', name: 'Bangladesh'},
    {code: 'BE', name: 'Belgium'},
    {code: 'BF', name: 'Burkina Faso'},
    {code: 'BG', name: 'Bulgaria'},
    {code: 'BH', name: 'Bahrain'},
    {code: 'BI', name: 'Burundi'},
    {code: 'BJ', name: 'Benin'},
    {code: 'BL', name: 'Saint Barthélemy'},
    {code: 'BM', name: 'Bermuda'},
    {code: 'BN', name: 'Brunei Darussalam'},
    {code: 'BO', name: 'Bolivia, Plurinational State of'},
    {code: 'BQ', name: 'Bonaire'},
    {code: 'BR', name: 'Brazil'},
    {code: 'BS', name: 'Bahamas'},
    {code: 'BT', name: 'Bhutan'},
    {code: 'BV', name: 'Bouvet Island'},
    {code: 'BW', name: 'Botswana'},
    {code: 'BY', name: 'Belarus'},
    {code: 'BZ', name: 'Belize'},
    {code: 'CA', name: 'Canada'},
    {code: 'CC', name: 'Cocos (Keeling) Islands'},
    {code: 'CD', name: 'Congo, the Democratic Republic of the'},
    {code: 'CF', name: 'Central African Republic'},
    {code: 'CG', name: 'Congo'},
    {code: 'CH', name: 'Switzerland'},
    {code: 'CI', name: "Côte d'Ivoire"},
    {code: 'CK', name: 'Cook Islands'},
    {code: 'CL', name: 'Chile'},
    {code: 'CM', name: 'Cameroon'},
    {code: 'CN', name: 'China'},
    {code: 'CO', name: 'Colombia'},
    {code: 'CR', name: 'Costa Rica'},
    {code: 'CS', name: 'Former Czechoslovak (Serbia and Montenegro)'},
    {code: 'CU', name: 'Cuba'},
    {code: 'CV', name: 'Cape Verde'},
    {code: 'CX', name: 'Christmas Island'},
    {code: 'CY', name: 'Cyprus'},
    {code: 'CZ', name: 'Czech Republic'},
    {code: 'DE', name: 'Germany'},
    {code: 'DJ', name: 'Djibouti'},
    {code: 'DK', name: 'Denmark'},
    {code: 'DM', name: 'Dominica'},
    {code: 'DO', name: 'Dominican Republic'},
    {code: 'DZ', name: 'Algeria'},
    {code: 'EC', name: 'Ecuador'},
    {code: 'EE', name: 'Estonia'},
    {code: 'EG', name: 'Egypt'},
    {code: 'EH', name: 'Western Sahara'},
    {code: 'ER', name: 'Eritrea'},
    {code: 'ES', name: 'Spain'},
    {code: 'ET', name: 'Ethiopia'},
    {code: 'EU', name: 'European Union'},
    {code: 'FI', name: 'Finland'},
    {code: 'FJ', name: 'Fiji'},
    {code: 'FK', name: 'Falkland Islands (Malvinas)'},
    {code: 'FM', name: 'Micronesia, Federated States of'},
    {code: 'FO', name: 'Faroe Islands'},
    {code: 'FR', name: 'France'},
    {code: 'GA', name: 'Gabon'},
    {code: 'GB', name: 'United Kingdom'},
    {code: 'GD', name: 'Grenada'},
    {code: 'GE', name: 'Georgia'},
    {code: 'GF', name: 'French Guiana'},
    {code: 'GG', name: 'Guernsey'},
    {code: 'GH', name: 'Ghana'},
    {code: 'GI', name: 'Gibraltar'},
    {code: 'GL', name: 'Greenland'},
    {code: 'GM', name: 'Gambia'},
    {code: 'GN', name: 'Guinea'},
    {code: 'GP', name: 'Guadeloupe'},
    {code: 'GQ', name: 'Equatorial Guinea'},
    {code: 'GR', name: 'Greece'},
    {code: 'GS', name: 'South Georgia and the South Sandwich Islands'},
    {code: 'GT', name: 'Guatemala'},
    {code: 'GU', name: 'Guam'},
    {code: 'GW', name: 'Guinea-Bissau'},
    {code: 'GY', name: 'Guyana'},
    {code: 'HK', name: 'Hong Kong'},
    {code: 'HM', name: 'Heard Island and McDonald Islands'},
    {code: 'HN', name: 'Honduras'},
    {code: 'HR', name: 'Croatia'},
    {code: 'HT', name: 'Haiti'},
    {code: 'HU', name: 'Hungary'},
    {code: 'ID', name: 'Indonesia'},
    {code: 'IE', name: 'Ireland'},
    {code: 'IL', name: 'Israel'},
    {code: 'IM', name: 'Isle of Man'},
    {code: 'IN', name: 'India'},
    {code: 'IO', name: 'British Indian Ocean Territory'},
    {code: 'IQ', name: 'Iraq'},
    {code: 'IR', name: 'Iran, Islamic Republic of'},
    {code: 'IS', name: 'Iceland'},
    {code: 'IT', name: 'Italy'},
    {code: 'JE', name: 'Jersey'},
    {code: 'JM', name: 'Jamaica'},
    {code: 'JO', name: 'Jordan'},
    {code: 'JP', name: 'Japan'},
    {code: 'KE', name: 'Kenya'},
    {code: 'KG', name: 'Kyrgyzstan'},
    {code: 'KH', name: 'Cambodia'},
    {code: 'KI', name: 'Kiribati'},
    {code: 'KM', name: 'Comoros'},
    {code: 'KN', name: 'Saint Kitts and Nevis'},
    {code: 'KP', name: "Korea, Democratic Peoples Republic of"},
    {code: 'KR', name: 'Korea, Republic of'},
    {code: 'KW', name: 'Kuwait'},
    {code: 'KY', name: 'Cayman Islands'},
    {code: 'KZ', name: 'Kazakhstan'},
    {code: 'LA', name: "Lao People's Democratic Republic"},
    {code: 'LB', name: 'Lebanon'},
    {code: 'LC', name: 'Saint Lucia'},
    {code: 'LI', name: 'Liechtenstein'},
    {code: 'LK', name: 'Sri Lanka'},
    {code: 'LR', name: 'Liberia'},
    {code: 'LS', name: 'Lesotho'},
    {code: 'LT', name: 'Lithuania'},
    {code: 'LU', name: 'Luxembourg'},
    {code: 'LV', name: 'Latvia'},
    {code: 'LY', name: 'Libya'},
    {code: 'MA', name: 'Morocco'},
    {code: 'MC', name: 'Monaco'},
    {code: 'MD', name: 'Moldova, Republic of'},
    {code: 'ME', name: 'Montenegro'},
    {code: 'MF', name: 'Saint Martin (French part)'},
    {code: 'MG', name: 'Madagascar'},
    {code: 'MH', name: 'Marshall Islands'},
    {code: 'MK', name: 'Macedonia, The Former Yugoslav Republic of'},
    {code: 'ML', name: 'Mali'},
    {code: 'MM', name: 'Myanmar'},
    {code: 'MN', name: 'Mongolia'},
    {code: 'MO', name: 'Macao'},
    {code: 'MP', name: 'Northern Mariana Islands'},
    {code: 'MQ', name: 'Martinique'},
    {code: 'MR', name: 'Mauritania'},
    {code: 'MS', name: 'Montserrat'},
    {code: 'MT', name: 'Malta'},
    {code: 'MU', name: 'Mauritius'},
    {code: 'MV', name: 'Maldives'},
    {code: 'MW', name: 'Malawi'},
    {code: 'MX', name: 'Mexico'},
    {code: 'MY', name: 'Malaysia'},
    {code: 'MZ', name: 'Mozambique'},
    {code: 'NA', name: 'Namibia'},
    {code: 'NC', name: 'New Caledonia'},
    {code: 'NE', name: 'Niger'},
    {code: 'NF', name: 'Norfolk Island'},
    {code: 'NG', name: 'Nigeria'},
    {code: 'NI', name: 'Nicaragua'},
    {code: 'NL', name: 'Netherlands'},
    {code: 'NO', name: 'Norway'},
    {code: 'NP', name: 'Nepal'},
    {code: 'NR', name: 'Nauru'},
    {code: 'NT', name: 'Neutral Zone'},
    {code: 'NU', name: 'Niue'},
    {code: 'NZ', name: 'New Zealand'},
    {code: 'OM', name: 'Oman'},
    {code: 'PA', name: 'Panama'},
    {code: 'PB', name: 'Panama (VISA Card)'},
    {code: 'PE', name: 'Peru'},
    {code: 'PF', name: 'French Polynesia'},
    {code: 'PG', name: 'Papua New Guinea'},
    {code: 'PH', name: 'Philippines'},
    {code: 'PK', name: 'Pakistan'},
    {code: 'PL', name: 'Poland'},
    {code: 'PM', name: 'Saint Pierre and Miquelon'},
    {code: 'PN', name: 'Pitcairn'},
    {code: 'PR', name: 'Puerto Rico'},
    {code: 'PS', name: 'Palestine, State of'},
    {code: 'PT', name: 'Portugal'},
    {code: 'PW', name: 'Palau'},
    {code: 'PY', name: 'Paraguay'},
    {code: 'PZ', name: 'Panama Canal Zone'},
    {code: 'QA', name: 'Qatar'},
    {code: 'QZ', name: 'Kosovo'},
    {code: 'RE', name: 'Réunion'},
    {code: 'RO', name: 'Romania'},
    {code: 'RS', name: 'Serbia'},
    {code: 'RU', name: 'Russian Federation'},
    {code: 'RW', name: 'Rwanda'},
    {code: 'SA', name: 'Saudi Arabia'},
    {code: 'SB', name: 'Solomon Islands'},
    {code: 'SC', name: 'Seychelles'},
    {code: 'SD', name: 'Sudan'},
    {code: 'SE', name: 'Sweden'},
    {code: 'SG', name: 'Singapore'},
    {code: 'SH', name: 'Saint Helena, Ascension and Tristan da Cunha'},
    {code: 'SI', name: 'Slovenia'},
    {code: 'SJ', name: 'Svalbard and Jan Mayen'},
    {code: 'SK', name: 'Slovakia'},
    {code: 'SL', name: 'Sierra Leone'},
    {code: 'SM', name: 'San Marino'},
    {code: 'SN', name: 'Senegal'},
    {code: 'SO', name: 'Somalia'},
    {code: 'SR', name: 'Suriname'},
    {code: 'SS', name: 'South Sudan'},
    {code: 'ST', name: 'Sao Tome and Principe'},
    {code: 'SV', name: 'El Salvador'},
    {code: 'SX', name: 'ST Maarten'},
    {code: 'SY', name: 'Syrian Arab Republic'},
    {code: 'SZ', name: 'Swaziland'},
    {code: 'TC', name: 'Turks and Caicos Islands'},
    {code: 'TD', name: 'Chad'},
    {code: 'TF', name: 'French Southern Territories'},
    {code: 'TG', name: 'Togo'},
    {code: 'TH', name: 'Thailand'},
    {code: 'TJ', name: 'Tajikistan'},
    {code: 'TK', name: 'Tokelau'},
    {code: 'TL', name: 'Timor-Leste'},
    {code: 'TM', name: 'Turkmenistan'},
    {code: 'TN', name: 'Tunisia'},
    {code: 'TO', name: 'Tonga'},
    {code: 'TP', name: 'East Timor'},
    {code: 'TR', name: 'Turkey'},
    {code: 'TT', name: 'Trinidad and Tobago'},
    {code: 'TV', name: 'Tuvalu'},
    {code: 'TW', name: 'Taiwan, Province of China'},
    {code: 'TZ', name: 'Tanzania, United Republic of'},
    {code: 'UA', name: 'Ukraine'},
    {code: 'UG', name: 'Uganda'},
    {code: 'UM', name: 'United States Minor Outlying Islands'},
    {code: 'US', name: 'United States'},
    {code: 'UY', name: 'Uruguay'},
    {code: 'UZ', name: 'Uzbekistan'},
    {code: 'VA', name: 'Holy See (Vatican City State)'},
    {code: 'VC', name: 'Saint Vincent and the Grenadines'},
    {code: 'VE', name: 'Venezuela, Bolivarian Republic of'},
    {code: 'VG', name: 'Virgin Islands, British'},
    {code: 'VI', name: 'Virgin Islands, U.S.'},
    {code: 'VN', name: 'Vietnam'},
    {code: 'VU', name: 'Vanuatu'},
    {code: 'WF', name: 'Wallis and Futuna'},
    {code: 'WS', name: 'Samoa'},
    {code: 'XC', name: 'Leeward & Windward Islands'},
    {code: 'XX', name: 'Various - For FDM'},
    {code: 'YE', name: 'Yemen'},
    {code: 'YT', name: 'Mayotte'},
    {code: 'YU', name: 'Yugoslavia'},
    {code: 'ZA', name: 'South Africa'},
    {code: 'ZM', name: 'Zambia'},
    {code: 'ZW', name: 'Zimbabwe'}
  ],
  nonResidentExceptions        : ["MUTUAL PARTY", "BULK INTEREST", "BULK VAT REFUNDS", "BULK BANK CHARGES", "BULK PENSIONS",
    "FCA RESIDENT NON REPORTABLE", "CFC RESIDENT NON REPORTABLE", "VOSTRO NON REPORTABLE",
    "VOSTRO INTERBANK", "STRATE", "NOSTRO INTERBANK", "NOSTRO NON REPORTABLE", "RTGS NON REPORTABLE", "RTGS INTERBANK", "IHQ"],
  residentExceptions           : ["MUTUAL PARTY", "RAND CHEQUE", "BULK PENSIONS", "NON RESIDENT RAND", "UNCLAIMED DRAFTS",
    "BULK INTEREST", "BULK DIVIDENDS", "BULK BANK CHARGES", "FCA NON RESIDENT NON REPORTABLE",
    "VOSTRO NON REPORTABLE", "VOSTRO INTERBANK", "NOSTRO NON REPORTABLE", "RTGS NON REPORTABLE", "RTGS INTERBANK", "NOSTRO INTERBANK", "STRATE"],
  moneyTransferAgents          : ["AD", "ADLA", "CARD", "MONEYGRAM", "WESTERNUNION", "PAYPAL", "EXCHANGE4FREE",
    "MUKURU", "MONEYTRANS", "XPRESSMONEY", "ZMT", "ESKOM", "SANLAM", "MOMENTUM", "TOURVEST", "TOWER", "IMALI", "TRAVELEX",
    "INTERAFRICA", "GLOBAL", "RANDBUREAU", "SIKHONA", "FOREXWORLD", "ACE", "AYOBA", "MASTERCURRENCY",
    "INTERCHANGE", "HELLO PAISA", "TRAVEL CARD", "TRAVELLERS CHEQUE", "SOUTH EAST", "MAMA MONEY", "SHOPRITE", "DAYTONA",
    "AXIS", "HBL", "SILK", "FLASH", "PEP", "AFROCOIN", "ECONET"],
  accountIdentifiersNonResident: ["NON RESIDENT OTHER", "NON RESIDENT RAND", "NON RESIDENT FCA", "CASH", "FCA RESIDENT", "RES FOREIGN BANK ACCOUNT", "VOSTRO", "VISA NET", "MASTER SEND"],
  accountIdentifiersResident   : ["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "EFT", "CARD PAYMENT", "VOSTRO", "DEBIT CARD", "CREDIT CARD"],
  adhocSubjects                : ["INVALIDIDNUMBER", "AIRPORT", "SETOFF", "ZAMBIAN GRAIN", "YES", "NO", "HOLDCO", "SDA", "REMITTANCE"],
  provinces                    : ["GAUTENG", "LIMPOPO", "NORTH WEST", "WESTERN CAPE", "EASTERN CAPE", "NORTHERN CAPE",
    "FREE STATE", "MPUMALANGA", "KWAZULU NATAL", "NAMIBIA", "LESOTHO", "SWAZILAND"],
  cardTypes                    : ["AMEX", "DINERS", "ELECTRON", "MAESTRO", "MASTER", "VISA", "BOCEXPRESS"],
  institutionalSectors         : [
    {code: "01", description: "FINANCIAL CORPORATE"},
    {code: "02", description: "NON FINANCIAL CORPORATE"},
    {code: "03", description: "GENERAL GOVERNMENT"},
    {code: "04", description: "HOUSEHOLD"}],
  industrialClassifications    : [
    {code: "01", description: "Agriculture, hunting, forestry and fishing"},
    {code: "02", description: "Mining and quarrying"},
    {code: "03", description: "Manufacturing"},
    {code: "04", description: "Electricity, gas and water supply"},
    {code: "05", description: "Construction"},
    {
      code       : "06",
      description: "Wholesale and retail trade; repair of motor vehicles, motor cycles and personal and household goods; hotels and restaurants"
    },
    {code: "07", description: "Transport, storage and communication"},
    {code: "08", description: "Financial intermediation, insurance, real estate and business services"},
    {code: "09", description: "Community, social and personal services"},
    {
      code       : "10",
      description: "Private households, exterritorial organisations, representatives of foreign governments and other activities not adequately defined"
    }],
  customsOffices               : [
    {code: "ALX", name: "ALEXANDERBAY"},
    {code: "BBR", name: "BEIT BRIDGE"},
    {code: "BFN", name: "BLOEMFONTEIN"},
    {code: "BIA", name: "BLOEMFONTEIN AIRPORT"},
    {code: "CLP", name: "CALEDONSPOORT"},
    {code: "CTN", name: "CAPE TOWN"},
    {code: "DBN", name: "DURBAN"},
    {code: "DFM", name: "CAPE TOWN INTERNATIONAL AIRPORT"},
    {code: "ELN", name: "EAST LONDON"},
    {code: "FBB", name: "FICKSBURG BRIDGE"},
    {code: "GMR", name: "GERMISTON/ALBERTON"},
    {code: "GOL", name: "GOLELA"},
    {code: "GRB", name: "GROBLERS BRIDGE"},
    {code: "HFV", name: "PEZ INTER AIRPORT"},
    {code: "JHB", name: "JOHANNESBURG"},
    {code: "JPR", name: "JEPPES REEF"},
    {code: "JSA", name: "O R TAMBO INTERNATIONAL AIRPORT"},
    {code: "KBY", name: "KIMBERLEY"},
    {code: "KFN", name: "KOPFONTEIN"},
    {code: "KOM", name: "KOMATIPOORT"},
    {code: "LBA", name: "KING SHAKA INT AIRPORT"},
    {code: "LSA", name: "LANSERIA AIRPORT"},
    {code: "MAF", name: "MAFEKING/MMABATHO"},
    {code: "MAH", name: "MAHAMBA"},
    {code: "MAN", name: "MANANGA"},
    {code: "MOS", name: "MOSSEL BAY"},
    {code: "MSB", name: "MASERU BRIDGE"},
    {code: "NAR", name: "NAKOP (NAROGAS)"},
    {code: "NRS", name: "NERSTON"},
    {code: "NSA", name: "NELSPRUIT AIRPORT"},
    {code: "OUD", name: "OUDTSHOORN"},
    {code: "OSH", name: "OSHOEK"},
    {code: "PEZ", name: "PORT ELIZABETH"},
    {code: "PIA", name: "PILANSBERG AIRPORT"},
    {code: "PMB", name: "PIETERMARITZBURG"},
    {code: "PRL", name: "PAARL"},
    {code: "PSB", name: "PIETERSBURG"},
    {code: "PTA", name: "PRETORIA"},
    {code: "QAC", name: "QACHAS NEK"},
    {code: "RAM", name: "RAMATLABAMA"},
    {code: "RBS", name: "ROBERTSON"},
    {code: "RIA", name: "RICHARDSBAY AIRPORT"},
    {code: "RIC", name: "RICHARDS BAY"},
    {code: "SAL", name: "SALDANHA BAY"},
    {code: "SKH", name: "SKILPADSHEK"},
    {code: "STE", name: "STELLENBOSCH"},
    {code: "UPS", name: "UPINGTON AIRPORT/STATION"},
    {code: "UPT", name: "UPINGTON"},
    {code: "VLD", name: "VIOOLSDRIFT"},
    {code: "VRE", name: "VREDENDAL"},
    {code: "VRH", name: "VAN ROOYENHEK"},
    {code: "WOR", name: "WORCESTER"}
  ],
  categories : [
    {
      flow       : 'IN',
      code       : 'ZZ1',
      section    : 'Special',
      subsection : 'Report',
      description: 'Report this BOPCUS transaction as a NON REPORTABLE'
    },
    {
      flow       : 'IN',
      code       : 'ZZ2',
      section    : 'Special',
      subsection : 'Do Not Report',
      description: 'Do not report this BOPCUS transaction'
    },
    {
      flow       : 'IN',
      code       : '100',
      oldCodes   : '100',
      section    : 'Merchandise',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds applicable to merchandise',
      tags       : ['Merchandise', 'Adjustments', 'Reversals', 'Refunds', 'Undertaking']
    },
    {
      flow       : 'IN',
      code       : '101/01',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Exports : Advance payments',
      description: 'Export advance payment',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'Undertaking']
    },
    {
      flow       : 'IN',
      code       : '101/02',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Exports : Advance payments',
      description: 'Export advance payment - capital goods',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'capital goods', 'Undertaking']
    },
    {
      flow       : 'IN',
      code       : '101/03',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Exports : Advance payments',
      description: 'Export advance payment - gold',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'Gold', 'Undertaking']
    },
    {
      flow       : 'IN',
      code       : '101/04',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Exports : Advance payments',
      description: 'Export advance payment - platinum',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'platinum', 'Undertaking']
    },
    {
      flow       : 'IN',
      code       : '101/05',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Exports : Advance payments',
      description: 'Export advance payment - crude oil',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'crude oil', 'Undertaking']
    },
    {
      flow       : 'IN',
      code       : '101/06',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Exports : Advance payments',
      description: 'Export advance payment - refined petroleum products',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'refined petroleum products', 'Undertaking']
    },
    {
      flow       : 'IN',
      code       : '101/07',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Exports : Advance payments',
      description: 'Export advance payment - diamonds',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'diamonds', 'Undertaking']
    },
    {
      flow       : 'IN',
      code       : '101/08',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Exports : Advance payments',
      description: 'Export advance payment - steel',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'steel', 'Undertaking']
    },
    {
      flow       : 'IN',
      code       : '101/09',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Exports : Advance payments',
      description: 'Export advance payment - coal',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'coal', 'Undertaking']
    },
    {
      flow       : 'IN',
      code       : '101/10',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Exports : Advance payments',
      description: 'Export advance payment - iron ore',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'iron ore', 'Undertaking']
    },
    {
      flow       : 'IN',
      code       : '101/11',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Exports : Advance payments',
      description: 'Export advance payment - goods exported via the South African Post Office',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'Post Office', 'SAPO', 'Mail', 'Postal Service', 'International Delivery', 'Delivery', 'Undertaking']
    },
    {
      flow       : 'IN',
      code       : '103/01',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Exports:',
      description: 'Export payments',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'Payment', 'Undertaking']
    },
    {
      flow       : 'IN',
      code       : '103/02',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Exports:',
      description: 'Export payment - capital goods',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'capital goods', 'Undertaking']
    },
    {
      flow       : 'IN',
      code       : '103/03',
      oldCodes   : '201',
      section    : 'Merchandise',
      subsection : 'Exports:',
      description: 'Export payment - gold',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'Gold', 'Not Undertaking']
    },
    {
      flow       : 'IN',
      code       : '103/04',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Exports:',
      description: 'Export payment - platinum',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'platinum', 'Not Undertaking']
    },
    {
      flow       : 'IN',
      code       : '103/05',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Exports:',
      description: 'Export payment - crude oil',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'crude oil', 'Not Undertaking']
    },
    {
      flow       : 'IN',
      code       : '103/06',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Exports:',
      description: 'Export payment - refined petroleum products',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'refined petroleum products', 'Not Undertaking']
    },
    {
      flow       : 'IN',
      code       : '103/07',
      oldCodes   : '114',
      section    : 'Merchandise',
      subsection : 'Exports:',
      description: 'Export payment - diamonds',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'diamonds', 'Not Undertaking']
    },
    {
      flow       : 'IN',
      code       : '103/08',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Exports:',
      description: 'Export payment - steel',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'steel', 'Not Undertaking']
    },
    {
      flow       : 'IN',
      code       : '103/09',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Exports:',
      description: 'Export payment - coal',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'coal', 'Not Undertaking']
    },
    {
      flow       : 'IN',
      code       : '103/10',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Exports:',
      description: 'Export payment - iron ore',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'iron ore', 'Not Undertaking']
    },
    {
      flow       : 'IN',
      code       : '103/11',
      oldCodes   : '113',
      section    : 'Merchandise',
      subsection : 'Exports:',
      description: 'Export payment - goods exported via the South African Post Office',
      tags       : ['Merchandise', 'Advance Payment', 'Exports', 'Post Office', 'Not Undertaking', 'International Delivery', 'Delivery']
    },
    {
      flow       : 'IN',
      code       : '105',
      oldCodes   : '105',
      section    : 'Merchandise',
      subsection : 'Exports: Other',
      description: 'Consumables acquired in port',
      tags       : ['Merchandise', 'Imports', 'Port']
    },
    {
      flow       : 'IN',
      code       : '106',
      section    : 'Merchandise',
      subsection : 'Exports: Other',
      description: 'Trade finance repayments in respect of exports',
      tags       : ['Merchandise', 'Exports', 'Trade Finance']
    },
    {
      flow       : 'IN',
      code       : '107',
      oldCodes   : '112',
      section    : 'Merchandise',
      subsection : 'Exports: Other',
      description: 'Export proceeds where the Customs value of the shipment is less than R500',
      tags       : ['Merchandise', 'Exports', 'less than R500']
    },
    {
      flow       : 'IN',
      code       : '108',
      oldCodes   : '102,103,112',
      section    : 'Merchandise',
      subsection : 'Exports: Other',
      description: 'Export payments where goods were declared as part of passenger baggage and no UCR is available',
      tags       : ['Merchandise', 'Exports', 'declared', 'customs']
    },
    {
      flow       : 'IN',
      code       : '109/01',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Exports: Other',
      description: 'Proceeds for goods purchased by non residents where no physical export will take place, excluding gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal and iron ore as well as merchanting transactions',
      tags       : ['Merchandise', 'Exports', 'non physical']
    },
    {
      flow       : 'IN',
      code       : '109/02',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Exports: Other',
      description: 'Proceeds for gold purchased by non residents where no physical export will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Exports', 'non physical', 'gold']
    },
    {
      flow       : 'IN',
      code       : '109/03',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Exports: Other',
      description: 'Proceeds for platinum purchased by non residents where no physical export will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Exports', 'non physical', 'platinum']
    },
    {
      flow       : 'IN',
      code       : '109/04',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Exports: Other',
      description: 'Proceeds for crude oil purchased by non residents where no physical export will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Exports', 'non physical', 'crude oil']
    },
    {
      flow       : 'IN',
      code       : '109/05',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Exports: Other',
      description: 'Proceeds for refined petroleum products purchased by non residents where no physical export will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Exports', 'non physical', 'refined petroleum products']
    },
    {
      flow       : 'IN',
      code       : '109/06',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Exports: Other',
      description: 'Proceeds for diamonds purchased by non residents where no physical export will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Exports', 'non physical', 'diamonds']
    },
    {
      flow       : 'IN',
      code       : '109/07',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Exports: Other',
      description: 'Proceeds for steel purchased by non residents where no physical export will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Exports', 'non physical', 'steel']
    },
    {
      flow       : 'IN',
      code       : '109/08',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Exports: Other',
      description: 'Proceeds for coal purchased by non residents where no physical export will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Exports', 'non physical', 'coal']
    },
    {
      flow       : 'IN',
      code       : '109/09',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Exports: Other',
      description: 'Proceeds for iron ore purchased by non residents where no physical export will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Exports', 'non physical', 'iron ore']
    },
    {
      flow       : 'IN',
      code       : '110',
      oldCodes   : '315',
      section    : 'Merchandise',
      subsection : 'Exports: Other',
      description: 'Merchanting transaction',
      tags       : ['Merchandise', 'Exports', 'Merchanting transaction']
    },
    {
      flow       : 'IN',
      code       : '200',
      oldCodes   : '902,950',
      section    : 'Intellectual property and other services',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds applicable to intellectual property and service related items',
      tags       : ['Transactions', 'Adjustments', 'Reversals', 'Refunds']
    },
    {
      flow       : 'IN',
      code       : '201',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Charges for the use of intellectual property',
      description: 'Rights assigned for licences to reproduce and/or distribute',
      tags       : ['Intellectual property', 'Rights', 'licences', 'reproduce', 'distribute']
    },
    {
      flow       : 'IN',
      code       : '202',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Charges for the use of intellectual property',
      description: 'Rights assigned for using patents and inventions (licensing)',
      tags       : ['Intellectual property', 'Rights', 'licences', 'patents', 'inventions']
    },
    {
      flow       : 'IN',
      code       : '203',
      oldCodes   : '312',
      section    : 'Intellectual property and other services',
      subsection : 'Charges for the use of intellectual property',
      description: 'Rights assigned for using patterns and designs (including industrial processes)',
      tags       : ['Intellectual property', 'Rights', 'patterns', 'designs']
    },
    {
      flow       : 'IN',
      code       : '204',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Charges for the use of intellectual property',
      description: 'Rights assigned for using copyrights',
      tags       : ['Intellectual property', 'Rights', 'copyrights']
    },
    {
      flow       : 'IN',
      code       : '205',
      oldCodes   : '605',
      section    : 'Intellectual property and other services',
      subsection : 'Charges for the use of intellectual property',
      description: 'Rights assigned for using franchises and trademarks',
      tags       : ['Intellectual property', 'Rights', 'franchises', 'trademarks']
    },
    {
      flow       : 'IN',
      code       : '210',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Disposal of Intellectual Property',
      description: 'Disposal of patents and inventions',
      tags       : ['Intellectual property', 'Disposal', 'patents', 'inventions', 'Sale']
    },
    {
      flow       : 'IN',
      code       : '211',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Disposal of Intellectual Property',
      description: 'Disposal of patterns and designs (including industrial processes)',
      tags       : ['Intellectual property', 'Disposal', 'patterns', 'designs', 'Sale']
    },
    {
      flow       : 'IN',
      code       : '212',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Disposal of Intellectual Property',
      description: 'Disposal of copyrights',
      tags       : ['Intellectual property', 'Disposal', 'copyrights', 'Sale']
    },
    {
      flow       : 'IN',
      code       : '213',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Disposal of Intellectual Property',
      description: 'Disposal of franchises and trademarks',
      tags       : ['Intellectual property', 'Disposal', 'franchises', 'trademarks', 'Sale']
    },
    {
      flow       : 'IN',
      code       : '220',
      oldCodes   : '319',
      section    : 'Intellectual property and other services',
      subsection : 'Research and Development',
      description: 'Proceeds received for research and development services',
      tags       : ['services', 'research', 'development', 'Proceeds', 'R&D']
    },
    {
      flow       : 'IN',
      code       : '221',
      oldCodes   : '504',
      section    : 'Intellectual property and other services',
      subsection : 'Research and Development',
      description: 'Funding received for research and development',
      tags       : ['services', 'research', 'development', 'R&D', 'Funding']
    },
    {
      flow       : 'IN',
      code       : '225',
      oldCodes   : '323',
      section    : 'Intellectual property and other services',
      subsection : 'Audiovisual and related items',
      description: 'Sales of original manuscripts, sound recordings and films',
      tags       : ['services', 'Sales', 'Audiovisual', 'manuscripts', 'recordings', 'films']
    },
    {
      flow       : 'IN',
      code       : '226',
      oldCodes   : '323',
      section    : 'Intellectual property and other services',
      subsection : 'Audiovisual and related items',
      description: 'Receipt of funds relating to the production of motion pictures, radio and television programs and musical recordings',
      tags       : ['services', 'Funding', 'Audiovisual', 'manuscripts', 'recordings', 'films']
    },
    {
      flow       : 'IN',
      code       : '230',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Computer software and related items',
      description: 'The outright selling of ownership rights of software',
      tags       : ['services', 'Sales', 'Rights', 'software', 'Ownership']
    },
    {
      flow       : 'IN',
      code       : '231',
      oldCodes   : '104,312,319',
      section    : 'Intellectual property and other services',
      subsection : 'Computer software and related items',
      description: 'Computer-related services including maintenance, repair and consultancy',
      tags       : ['services', 'maintenance', 'consultancy', 'repair', 'software']
    },
    {
      flow       : 'IN',
      code       : '232',
      oldCodes   : '101,102,103,112,113,312',
      section    : 'Intellectual property and other services',
      subsection : 'Computer software and related items',
      description: 'Commercial sales of customised software and related licenses for use by customers',
      tags       : ['services', 'customised', 'licenses', 'software', 'sales', 'bespoke software']
    },
    {
      flow       : 'IN',
      code       : '233',
      oldCodes   : '101,102,103,112,113,312',
      section    : 'Intellectual property and other services',
      subsection : 'Computer software and related items',
      description: 'Commercial sales of non-customised software on physical media with periodic licence to use',
      tags       : ['services', 'non-customised', 'periodic license', 'software']
    },
    {
      flow       : 'IN',
      code       : '234',
      oldCodes   : '101,102,103,112,113,312',
      section    : 'Intellectual property and other services',
      subsection : 'Computer software and related items',
      description: 'Commercial sales of non-customised software provided on physical media with right to perpetual (ongoing) use',
      tags       : ['services', 'non-customised', 'physical media', 'licenses', 'software', 'ongoing']
    },
    {
      flow       : 'IN',
      code       : '235',
      oldCodes   : '111,312',
      section    : 'Intellectual property and other services',
      subsection : 'Computer software and related items',
      description: 'Commercial sales of non-customised software provided for downloading or electronically made available with periodic license',
      tags       : ['services', 'non-customised', 'downloaded', 'periodic license', 'software']
    },
    {
      flow       : 'IN',
      code       : '236',
      oldCodes   : '111,312',
      section    : 'Intellectual property and other services',
      subsection : 'Computer software and related items',
      description: 'Commercial sales of non-customised software provided for downloading or electronically made available with single payment',
      tags       : ['services', 'non-customised', 'downloaded', 'licenses', 'software']
    },
    {
      flow       : 'IN',
      code       : '240/01',
      oldCodes   : '320,321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on materials (excluding gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal and iron ore)',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'other']
    },
    {
      flow       : 'IN',
      code       : '240/02',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on gold',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'gold']
    },
    {
      flow       : 'IN',
      code       : '240/03',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on platinum',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'platinum']
    },
    {
      flow       : 'IN',
      code       : '240/04',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on crude oil',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'crude oil']
    },
    {
      flow       : 'IN',
      code       : '240/05',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on refined petroleum products',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'refined petroleum products']
    },
    {
      flow       : 'IN',
      code       : '240/06',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on diamonds',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'diamonds']
    },
    {
      flow       : 'IN',
      code       : '240/07',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on steel',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'steel']
    },
    {
      flow       : 'IN',
      code       : '240/08',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on coal',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'coal']
    },
    {
      flow       : 'IN',
      code       : '240/09',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on iron ore',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'iron ore']
    },
    {
      flow       : 'IN',
      code       : '241',
      oldCodes   : '104',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Repairs and maintenance on machinery and equipment',
      tags       : ['services', 'Technical', 'Repairs', 'maintenance', 'machinery', 'equipment']
    },
    {
      flow       : 'IN',
      code       : '242',
      oldCodes   : '320',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Architectural, engineering and other technical services',
      tags       : ['services', 'Technical', 'Architectural', 'engineering', 'Certification']
    },
    {
      flow       : 'IN',
      code       : '243',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Agricultural, mining, waste treatment and depollution services',
      tags       : ['services', 'Technical', 'Agricultural', 'mining', 'waste treatment', 'depollution']
    },
    {
      flow       : 'IN',
      code       : '250',
      oldCodes   : '253',
      section    : 'Intellectual property and other services',
      subsection : 'Travel services for non-residents',
      description: 'Travel services for non-residents - business travel',
      tags       : ['services', 'Travel', 'non-residents', 'business']
    },
    {
      flow       : 'IN',
      code       : '251',
      oldCodes   : '254',
      section    : 'Intellectual property and other services',
      subsection : 'Travel services for non-residents',
      description: 'Travel services for non-residents - holiday travel',
      tags       : ['services', 'Travel', 'non-residents', 'holiday']
    },
    {
      flow       : 'IN',
      code       : '252',
      oldCodes   : '255',
      section    : 'Intellectual property and other services',
      subsection : 'Travel services for non-residents',
      description: 'Foreign exchange accepted by residents from non-residents',
      tags       : ['services', 'residents', 'foreign exchange', 'forex']
    },
    {
      flow       : 'IN',
      code       : '255',
      oldCodes   : '303',
      section    : 'Intellectual property and other services',
      subsection : 'Travel services for residents',
      description: 'Travel services for residents - business travel',
      tags       : ['services', 'Travel', 'residents', 'business']
    },
    {
      flow       : 'IN',
      code       : '256',
      oldCodes   : '304',
      section    : 'Intellectual property and other services',
      subsection : 'Travel services for residents',
      description: 'Travel services for residents - holiday travel',
      tags       : ['services', 'Travel', 'residents', 'holiday']
    },
    {
      flow       : 'IN',
      code       : '260',
      oldCodes   : '260',
      section    : 'Intellectual property and other services',
      subsection : 'Travel services in respect of third parties',
      description: 'Proceeds for travel services in respect of third parties - business travel',
      tags       : ['services', 'Travel', 'third parties', 'business', 'Proceeds']
    },
    {
      flow       : 'IN',
      code       : '261',
      oldCodes   : '260',
      section    : 'Intellectual property and other services',
      subsection : 'Travel services in respect of third parties',
      description: 'Proceeds for travel services in respect of third parties - holiday travel',
      tags       : ['services', 'Travel', 'third parties', 'holiday', 'Proceeds']
    },
    {
      flow       : 'IN',
      code       : '265',
      oldCodes   : '307',
      section    : 'Intellectual property and other services',
      subsection : 'Telecommunication and information services',
      description: 'Proceeds for telecommunication services',
      tags       : ['services', 'telecommunication', 'information', 'Proceeds']
    },
    {
      flow       : 'IN',
      code       : '266',
      oldCodes   : '313,314',
      section    : 'Intellectual property and other services',
      subsection : 'Telecommunication and information services',
      description: 'Proceeds for information services including data, news related and news agency fees',
      tags       : ['services', 'data', 'information', 'news', 'Proceeds', 'Reporting', 'Media']
    },
    {
      flow       : 'IN',
      code       : '270/01',
      oldCodes   : '302',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for passenger services - road',
      tags       : ['services', 'Transportation', 'passenger', 'road']
    },
    {
      flow       : 'IN',
      code       : '270/02',
      oldCodes   : '302',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for passenger services - rail',
      tags       : ['services', 'Transportation', 'passenger', 'rail', 'train']
    },
    {
      flow       : 'IN',
      code       : '270/03',
      oldCodes   : '302',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for passenger services - sea',
      tags       : ['services', 'Transportation', 'passenger', 'sea', 'ship', 'boat', 'yacht']
    },
    {
      flow       : 'IN',
      code       : '270/04',
      oldCodes   : '302',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for passenger services - air',
      tags       : ['services', 'Transportation', 'passenger', 'air']
    },
    {
      flow       : 'IN',
      code       : '271/01',
      oldCodes   : '301',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for freight services - road',
      tags       : ['services', 'Transportation', 'freight', 'road']
    },
    {
      flow       : 'IN',
      code       : '271/02',
      oldCodes   : '301',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for freight services - rail',
      tags       : ['services', 'Transportation', 'freight', 'rail', 'train']
    },
    {
      flow       : 'IN',
      code       : '271/03',
      oldCodes   : '301',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for freight services - sea',
      tags       : ['services', 'Transportation', 'freight', 'sea', 'ship']
    },
    {
      flow       : 'IN',
      code       : '271/04',
      oldCodes   : '301',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for freight services - air',
      tags       : ['services', 'Transportation', 'freight', 'air']
    },
    {
      flow       : 'IN',
      code       : '272/01',
      oldCodes   : '301,308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for other transport services - road',
      tags       : ['services', 'Transportation', 'other', 'road']
    },
    {
      flow       : 'IN',
      code       : '272/02',
      oldCodes   : '301,308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for other transport services - rail',
      tags       : ['services', 'Transportation', 'other', 'rail', 'train']
    },
    {
      flow       : 'IN',
      code       : '272/03',
      oldCodes   : '301,308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for other transport services - sea',
      tags       : ['services', 'Transportation', 'other', 'sea', 'ship']
    },
    {
      flow       : 'IN',
      code       : '272/04',
      oldCodes   : '301,308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for other transport services - air',
      tags       : ['services', 'Transportation', 'other', 'air', 'plane']
    },
    {
      flow       : 'IN',
      code       : '273/01',
      oldCodes   : '308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for postal and courier services - road',
      tags       : ['services', 'Transportation', 'courier', 'postal', 'road']
    },
    {
      flow       : 'IN',
      code       : '273/02',
      oldCodes   : '308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for postal and courier services - rail',
      tags       : ['services', 'Transportation', 'courier', 'postal', 'rail']
    },
    {
      flow       : 'IN',
      code       : '273/03',
      oldCodes   : '308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for postal and courier services - sea',
      tags       : ['services', 'Transportation', 'courier', 'postal', 'sea']
    },
    {
      flow       : 'IN',
      code       : '273/04',
      oldCodes   : '308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Proceeds for postal and courier services - air',
      tags       : ['services', 'Transportation', 'courier', 'postal', 'air']
    },
    {
      flow       : 'IN',
      code       : '275',
      oldCodes   : '311',
      section    : 'Intellectual property and other services',
      subsection : 'Financial services provided',
      description: 'Commission and fees',
      tags       : ['services', 'Financial', 'Commission']
    },
    {
      flow       : 'IN',
      code       : '276',
      oldCodes   : '311',
      section    : 'Intellectual property and other services',
      subsection : 'Financial services provided',
      description: 'Proceeds for financial services charged for advice provided',
      tags       : ['services', 'Financial', 'advice', 'counsel']
    },
    {
      flow       : 'IN',
      code       : '280',
      oldCodes   : '309',
      section    : 'Intellectual property and other services',
      subsection : 'Construction services',
      description: 'Proceeds for construction services',
      tags       : ['services', 'Financial', 'Proceeds', 'Construction']
    },
    {
      flow       : 'IN',
      code       : '281',
      oldCodes   : '325,901',
      section    : 'Intellectual property and other services',
      subsection : 'Government services',
      description: 'Proceeds for government services',
      tags       : ['services', 'Financial', 'Proceeds', 'Government']
    },
    {
      flow       : 'IN',
      code       : '282',
      oldCodes   : '325',
      section    : 'Intellectual property and other services',
      subsection : 'Government services',
      description: 'Diplomatic transfers',
      tags       : ['services', 'Financial', 'Diplomatic transfers', 'Government']
    },
    {
      flow       : 'IN',
      code       : '285',
      oldCodes   : '305',
      section    : 'Intellectual property and other services',
      subsection : 'Study related services',
      description: 'Tuition fees',
      tags       : ['services', 'Financial', 'Fees', 'Tuition', 'Study', 'Scholarship']
    },
    {
      flow       : 'IN',
      code       : '287',
      oldCodes   : '317',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services rendered',
      description: 'Proceeds for legal services',
      tags       : ['services', 'Financial', 'Business', 'Proceeds', 'legal']
    },
    {
      flow       : 'IN',
      code       : '288',
      oldCodes   : '317',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services rendered',
      description: 'Proceeds for accounting services',
      tags       : ['services', 'Financial', 'Business', 'Proceeds', 'accounting']
    },
    {
      flow       : 'IN',
      code       : '289',
      oldCodes   : '317',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services rendered',
      description: 'Proceeds for management consulting services',
      tags       : ['services', 'Financial', 'Business', 'Proceeds', 'consulting']
    },
    {
      flow       : 'IN',
      code       : '290',
      oldCodes   : '317',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services rendered',
      description: 'Proceeds for public relation services',
      tags       : ['services', 'Financial', 'Business', 'Proceeds', 'public relation']
    },
    {
      flow       : 'IN',
      code       : '291',
      oldCodes   : '318',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services rendered',
      description: 'Proceeds for advertising & market research services',
      tags       : ['services', 'Financial', 'Business', 'Proceeds', 'advertising', 'market research']
    },
    {
      flow       : 'IN',
      code       : '292',
      oldCodes   : '317',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services rendered',
      description: 'Proceeds for managerial services',
      tags       : ['services', 'Financial', 'Business', 'Payment', 'managerial']
    },
    {
      flow       : 'IN',
      code       : '293',
      oldCodes   : '326',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services rendered',
      description: 'Proceeds for medical and dental services',
      tags       : ['services', 'Financial', 'Business', 'Proceeds', 'medical', 'dental']
    },
    {
      flow       : 'IN',
      code       : '294',
      oldCodes   : '319',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services rendered',
      description: 'Proceeds for educational services',
      tags       : ['services', 'Financial', 'Business', 'Proceeds', 'educational', 'tuition']
    },
    {
      flow       : 'IN',
      code       : '295',
      oldCodes   : '316',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services rendered',
      description: 'Operational leasing',
      tags       : ['services', 'Financial', 'Business', 'Proceeds', 'Operational leasing']
    },
    {
      flow       : 'IN',
      code       : '296',
      oldCodes   : '324',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services rendered',
      description: 'Proceeds for cultural and recreational services',
      tags       : ['services', 'Financial', 'Business', 'Proceeds', 'cultural', 'recreational']
    },
    {
      flow       : 'IN',
      code       : '297',
      oldCodes   : '901',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services rendered',
      description: 'Proceeds for other business services not included elsewhere',
      tags       : ['services', 'Financial', 'Business', 'Proceeds', 'Other']
    },
    {
      flow       : 'IN',
      code       : '300',
      oldCodes   : '902,950',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds related to income and yields on financial assets',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Adjustments', 'Reversals', 'Refunds']
    },
    {
      flow       : 'IN',
      code       : '301',
      oldCodes   : '401',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Dividends',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Income', 'Payment', 'Dividends']
    },
    {
      flow       : 'IN',
      code       : '302',
      oldCodes   : '403',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Branch profits',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Income', 'Branch profits']
    },
    {
      flow       : 'IN',
      code       : '303',
      oldCodes   : '404',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Compensation paid by a non-resident to a resident employee temporarily abroad (excluding remittances)',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Compensation', 'resident employee temporarily abroad', 'excluding remittances', 'Remuneration', 'Salary', 'Wages', 'Pay']
    },
    {
      flow       : 'IN',
      code       : '304',
      oldCodes   : '404',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Compensation paid by a non-resident to a non-resident employee in South Africa (excluding remittances)',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Compensation', 'non-resident employee', 'excluding remittances', 'Remuneration', 'Salary', 'Wages', 'Pay']
    },
    {
      flow       : 'IN',
      code       : '305',
      oldCodes   : '404',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Compensation paid by a non-resident to a migrant worker employee (excluding remittances)',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Compensation', 'migrant worker employee', 'excluding remittances', 'Remuneration', 'Salary', 'Wages', 'Pay']
    },
    {
      flow       : 'IN',
      code       : '306',
      oldCodes   : '404',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Compensation paid by a non-resident to a foreign national contract worker employee (excluding remittances)',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Compensation', 'foreign national contract worker employee', 'excluding remittances', 'Remuneration', 'Salary', 'Wages', 'Pay']
    },
    {
      flow       : 'IN',
      code       : '307',
      oldCodes   : '405',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Commission or brokerage',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Commission', 'brokerage', 'Remuneration', 'Salary', 'Wages', 'Pay']
    },
    {
      flow       : 'IN',
      code       : '308',
      oldCodes   : '406',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Rental',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Rental']
    },
    {
      flow       : 'IN',
      code       : '309/01',
      oldCodes   : '407',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Interest received from a resident temporarily abroad in respect of loans',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Interest', 'resident temporarily abroad', 'shareholders', 'loans']
    },
    {
      flow       : 'IN',
      code       : '309/02',
      oldCodes   : '407',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Interest received from a non-resident in respect of individual loans',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Interest', 'received from a non-resident', 'third party', 'loans', 'personal loans']
    },
    {
      flow       : 'IN',
      code       : '309/03',
      oldCodes   : '407',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Interest received from a non-resident in respect of study loans',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Interest', 'received from a non-resident', 'study', 'tuition', 'loans']
    },
    {
      flow       : 'IN',
      code       : '309/04',
      oldCodes   : '407',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Interest received from a non-resident in respect of shareholders loans',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Interest', 'received from a non-resident', 'shareholders', 'loans']
    },
    {
      flow       : 'IN',
      code       : '309/05',
      oldCodes   : '407',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Interest received from a non-resident in respect of third party loans',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Interest', 'received from a non-resident', 'third party', 'loans']
    },
    {
      flow       : 'IN',
      code       : '309/06',
      oldCodes   : '407',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Interest received from a non-resident in respect of trade finance loans',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Interest', 'received from a non-resident', 'trade', 'loans']
    },
    {
      flow       : 'IN',
      code       : '309/07',
      oldCodes   : '407',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Interest received from a non-resident in respect of a bond',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Interest', 'received from a non-resident', 'bond', 'homeloan']
    },
    {
      flow       : 'IN',
      code       : '309/08',
      oldCodes   : '402',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Interest received not in respect of loans',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Interest', 'received from a non-resident', 'not in respect of a loan']
    },
    {
      flow       : 'IN',
      code       : '310/01',
      oldCodes   : '408',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Income in respect of inward listed securities equity individual',
      tags       : ['Transactions', 'Financial', 'income and yields', 'equity', 'Income', 'Inward Listed Securities', 'Individual']
    },
    {
      flow       : 'IN',
      code       : '310/02',
      oldCodes   : '408',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Income in respect of inward listed securities equity corporate',
      tags       : ['Transactions', 'Financial', 'income and yields', 'equity', 'Income', 'Inward Listed Securities', 'Corporate', 'Entity']
    },
    {
      flow       : 'IN',
      code       : '310/03',
      oldCodes   : '408',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Income in respect of inward listed securities equity bank',
      tags       : ['Transactions', 'Financial', 'income and yields', 'equity', 'Income', 'Inward Listed Securities', 'bank']
    },
    {
      flow       : 'IN',
      code       : '310/04',
      oldCodes   : '408',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Income in respect of inward listed securities equity institution',
      tags       : ['Transactions', 'Financial', 'income and yields', 'equity', 'Income', 'Inward Listed Securities', 'Institution']
    },
    {
      flow       : 'IN',
      code       : '311/01',
      oldCodes   : '408',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Income in respect of inward listed securities debt individual',
      tags       : ['Transactions', 'Financial', 'income and yields', 'debt', 'Income', 'Inward Listed Securities', 'Individual']
    },
    {
      flow       : 'IN',
      code       : '311/02',
      oldCodes   : '408',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Income in respect of inward listed securities debt corporate',
      tags       : ['Transactions', 'Financial', 'income and yields', 'debt', 'Income', 'Inward Listed Securities', 'Corporate', 'Entity']
    },
    {
      flow       : 'IN',
      code       : '311/03',
      oldCodes   : '408',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Income in respect of inward listed securities debt bank',
      tags       : ['Transactions', 'Financial', 'income and yields', 'debt', 'Income', 'Inward Listed Securities', 'Bank']
    },
    {
      flow       : 'IN',
      code       : '311/04',
      oldCodes   : '408',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Income in respect of inward listed securities debt institution',
      tags       : ['Transactions', 'Financial', 'income and yields', 'debt', 'Income', 'Inward Listed Securities', 'Institution']
    },
    {
      flow       : 'IN',
      code       : '312/01',
      oldCodes   : '408',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Income in respect of inward listed securities derivatives individual',
      tags       : ['Transactions', 'Financial', 'income and yields', 'derivatives', 'Income', 'Inward Listed Securities', 'Individual']
    },
    {
      flow       : 'IN',
      code       : '312/02',
      oldCodes   : '408',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Income in respect of inward listed securities derivatives corporate',
      tags       : ['Transactions', 'Financial', 'income and yields', 'derivatives', 'Income', 'Inward Listed Securities', 'Corporate', 'Entity']
    },
    {
      flow       : 'IN',
      code       : '312/03',
      oldCodes   : '408',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Income in respect of inward listed securities derivatives bank',
      tags       : ['Transactions', 'Financial', 'income and yields', 'derivatives', 'Income', 'Inward Listed Securities', 'Bank']
    },
    {
      flow       : 'IN',
      code       : '312/04',
      oldCodes   : '408',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Income in respect of inward listed securities derivatives institution',
      tags       : ['Transactions', 'Financial', 'income and yields', 'derivatives', 'Income', 'Inward Listed Securities', 'Institution']
    },
    {
      flow       : 'IN',
      code       : '313',
      oldCodes   : '402',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income receipts',
      description: 'Income earned abroad by a resident on an individual investment',
      tags       : ['Transactions', 'Financial', 'income and yields', 'Earnings', 'Income', 'Off-shore', 'Individual', 'Investment']
    },
    {
      flow       : 'IN',
      code       : '400',
      oldCodes   : '902,950',
      section    : 'Transfers of a current nature',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds related to transfers of a current nature',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Adjustments', 'Reversals', 'Refunds']
    },
    {
      flow       : 'IN',
      code       : '401',
      oldCodes   : '501',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Gifts',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Gifts']
    },
    {
      flow       : 'IN',
      code       : '402',
      oldCodes   : '502',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Annual contributions',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Gifts']
    },
    {
      flow       : 'IN',
      code       : '403',
      oldCodes   : '503',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Contributions in respect of social security schemes',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Gifts']
    },
    {
      flow       : 'IN',
      code       : '404',
      oldCodes   : '504',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Contributions in respect of charitable, religious and cultural (excluding research and development)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Contributions', 'charity', 'religious', 'cultural']
    },
    {
      flow       : 'IN',
      code       : '405',
      oldCodes   : '501',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Other donations / aid to Government (excluding research and development)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Contributions', 'donations', 'aid', 'Government', 'foreign']
    },
    {
      flow       : 'IN',
      code       : '406',
      oldCodes   : '501',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Other donations / aid to private sector (excluding research and development)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Contributions', 'donations', 'aid', 'private', 'foreign']
    },
    {
      flow       : 'IN',
      code       : '407',
      oldCodes   : '505',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Pensions',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Pensions']
    },
    {
      flow       : 'IN',
      code       : '408',
      oldCodes   : '506',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Annuities (pension related)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Pensions', 'Annuities']
    },
    {
      flow       : 'IN',
      code       : '409',
      oldCodes   : '508',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Inheritances',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Inheritances']
    },
    {
      flow       : 'IN',
      code       : '410',
      oldCodes   : '509',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Alimony',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Alimony']
    },
    {
      flow       : 'IN',
      code       : '411/01',
      oldCodes   : '510',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Tax - Income tax',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Tax', 'Income']
    },
    {
      flow       : 'IN',
      code       : '411/02',
      oldCodes   : '510',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Tax - VAT refunds',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Tax', 'VAT', 'Refunds']
    },
    {
      flow       : 'IN',
      code       : '411/03',
      oldCodes   : '510,604',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Tax - Other',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Tax', 'Other']
    },
    {
      flow       : 'IN',
      code       : '412',
      oldCodes   : '511',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Insurance premiums (non life/short term)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Insurance', 'premiums', '(non life/short term)', 'Health', 'Medical', 'Dental', 'Disability']
    },
    {
      flow       : 'IN',
      code       : '413',
      oldCodes   : '512',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Insurance claims (non life/short term)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Insurance', 'premiums', '(non life/short term)', 'Health', 'Medical', 'Dental', 'Disability', 'Claims']
    },
    {
      flow       : 'IN',
      code       : '414',
      oldCodes   : '310',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Insurance premiums (life)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Insurance', 'premiums', 'Life', 'Death']
    },
    {
      flow       : 'IN',
      code       : '415',
      oldCodes   : '310',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Insurance claims (life)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Insurance', 'premiums', 'Life', 'Death', 'Claims']
    },
    {
      flow       : 'IN',
      code       : '416',
      oldCodes   : '404',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Migrant worker remittances (excluding compensation)'
    },
    {
      flow       : 'IN',
      code       : '417',
      oldCodes   : '404',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Foreign national contract worker remittances (excluding compensation)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'remittances', 'foreign national', 'contract', 'worker', 'employee', '(excluding compensation)']
    },
    {
      flow       : 'IN',
      code       : '500',
      oldCodes   : '902,950',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds related to capital transfers and immigrants',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Adjustments', 'Reversals', 'Refunds', 'capital transfers', 'immigrants']
    },
    {
      flow       : 'IN',
      code       : '501',
      oldCodes   : '601',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers relating to government / corporate entities (excluding loans)',
      description: 'Donations to SA Government for fixed assets',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Donations', 'SA Government', 'fixed assets', 'capital transfers', 'immigrants']
    },
    {
      flow       : 'IN',
      code       : '502',
      oldCodes   : '603',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers relating to government / corporate entities (excluding loans)',
      description: 'Donations to corporate entities - fixed assets',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Donations', 'fixed assets', 'capital transfers', 'immigrants', 'corporate', 'entity']
    },
    {
      flow       : 'IN',
      code       : '503',
      oldCodes   : '602',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers relating to government / corporate entities (excluding loans)',
      description: 'Investment into property by a non-resident corporate entity',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Property', 'Investment', 'fixed assets', 'capital transfers', 'immigrants', 'non-resident', 'foreign', 'corporate', 'entity']
    },
    {
      flow       : 'IN',
      code       : '504',
      oldCodes   : '602',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers relating to government / corporate entities (excluding loans)',
      description: 'Disinvestment of property by a resident corporate entity',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Property', 'Disinvestment', 'fixed assets', 'capital transfers', 'immigrants', 'resident', 'corporate', 'entity']
    },
    {
      flow       : 'IN',
      code       : '510/01',
      oldCodes   : '602',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers by non-resident individuals',
      description: 'Investment into property by a non-resident individual',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Property', 'Investment', 'fixed assets', 'capital transfers', 'immigrants', 'non-resident', 'foreign', 'individual']
    },
    {
      flow       : 'IN',
      code       : '510/02',
      oldCodes   : '602',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers by non-resident individuals',
      description: 'Investment by a non-resident individual - other',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Other', 'Investment', 'fixed assets', 'capital transfers', 'immigrants', 'non-resident', 'foreign', 'individual']
    },
    {
      flow       : 'IN',
      code       : '511/01',
      oldCodes   : '606',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Disinvestment of capital by a resident individual - Shares',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Shares', 'Disinvestment', 'fixed assets', 'capital transfers', 'immigrants', 'resident', 'individual']
    },
    {
      flow       : 'IN',
      code       : '511/02',
      oldCodes   : '606',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Disinvestment of capital by a resident individual - Bonds',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Bonds', 'Disinvestment', 'fixed assets', 'capital transfers', 'immigrants', 'resident', 'individual']
    },
    {
      flow       : 'IN',
      code       : '511/03',
      oldCodes   : '606',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Disinvestment of capital by a resident individual - Money market instruments',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Money Market', 'Disinvestment', 'fixed assets', 'capital transfers', 'immigrants', 'resident', 'individual']
    },
    {
      flow       : 'IN',
      code       : '511/04',
      oldCodes   : '606',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Disinvestment of capital by a resident individual - Deposits with a foreign bank',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Deposits', 'Foreign bank', 'Disinvestment', 'fixed assets', 'capital transfers', 'immigrants', 'resident', 'individual']
    },
    {
      flow       : 'IN',
      code       : '511/05',
      oldCodes   : '606',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Disinvestment of capital by a resident individual - Mutual funds / collective investment schemes',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Mutual Funds', 'Collective Investment Schemes', 'Disinvestment', 'fixed assets', 'capital transfers', 'immigrants', 'resident', 'individual']
    },
    {
      flow       : 'IN',
      code       : '511/06',
      oldCodes   : '606',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Disinvestment of capital by a resident individual - Property',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Property', 'Disinvestment', 'fixed assets', 'capital transfers', 'immigrants', 'resident', 'individual']
    },
    {
      flow       : 'IN',
      code       : '511/07',
      oldCodes   : '606',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Disinvestment of capital by a resident individual - Other',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Other', 'Disinvestment', 'fixed assets', 'capital transfers', 'immigrants', 'resident', 'individual']
    },
    {
      flow       : 'IN',
      code       : '516',
      oldCodes   : '606',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Repatriation of capital, on instruction by the Financial Surveillance Department, of a foreign investment by a resident individual in respect of cross-border flows',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Repatriation', 'Financial Surveillance Department', 'Instruction', 'foreign investment', 'fixed assets', 'capital transfers', 'immigrants', 'resident', 'individual']
    },
    {
      flow       : 'IN',
      code       : '517',
      oldCodes   : '607',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Repatriation of capital, on instruction by the Financial Surveillance Department, of a foreign investment by a resident individual originating from an account conducted in foreign currency held at an Authorised Dealer in South Africa',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Repatriation', 'Financial Surveillance Department', 'Instruction', 'foreign investment', 'fixed assets', 'capital transfers', 'immigrants', 'resident', 'individual', 'foreign currency', 'authorised dealer in south africa']
    },
    {
      flow       : 'IN',
      code       : '530/01',
      oldCodes   : '609',
      section    : 'Capital Transfers and immigrants',
      subsection : 'Immigrants',
      description: 'Immigration',
      tags       : ['Transactions', 'Financial', 'Immigration', 'foreign capital allowance', 'Capital Transfers']
    },
    {
      flow       : 'IN',
      code       : '600',
      oldCodes   : '902,950',
      section    : 'Financial investments/disinvestments and Prudential investments',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds related to financial investments/disinvestments and prudential investments',
      tags       : ['Transactions', 'Financial', 'Adjustments', 'Reversals', 'Refunds', 'prudential', 'investments', 'disinvestments']
    },
    {
      flow       : 'IN',
      code       : '601/01',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Investment by a non-resident',
      description: 'Investment in listed shares by a non-resident',
      tags       : ['Transactions', 'Financial', 'Listed', 'Shares', 'Investment', 'non-resident', 'sale proceeds', '(excluding local institutional investors)']
    },
    {
      flow       : 'IN',
      code       : '601/02',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Investment by a non-resident',
      description: 'Investment in non-listed shares by a non-resident',
      tags       : ['Transactions', 'Financial', 'non-listed', 'Shares', 'Investment', 'non-resident', 'sale proceeds', '(excluding local institutional investors)']
    },
    {
      flow       : 'IN',
      code       : '602',
      oldCodes   : '703',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Investment by a non-resident',
      description: 'Investment into money market instruments by a non-resident',
      tags       : ['Transactions', 'Financial', 'money market', 'Investment', 'non-resident', 'sale proceeds', '(excluding local institutional investors)']
    },
    {
      flow       : 'IN',
      code       : '603/01',
      oldCodes   : '702',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Investment by a non-resident',
      description: 'Investment into listed bonds by a non-resident (excluding loans)',
      tags       : ['Transactions', 'Financial', 'Listed', 'Bonds', 'Investment', 'non-resident', 'sale proceeds', '(excluding local institutional investors)']
    },
    {
      flow       : 'IN',
      code       : '603/02',
      oldCodes   : '702',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Investment by a non-resident',
      description: 'Investment into non-listed bonds by a non-resident (excluding loans)',
      tags       : ['Transactions', 'Financial', 'Non-listed', 'Bonds', 'Investment', 'non-resident', 'sale proceeds', '(excluding local institutional investors)']
    },
    {
      flow       : 'IN',
      code       : '605/01',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Disinvestment by a resident corporate entity',
      description: 'Disinvestment of shares by resident - Agriculture, hunting, forestry and fishing',
      tags       : ['Transactions', 'Financial', 'shares', 'Disinvestment', 'resident', 'entity', '(excluding local institutional investors)', 'Agriculture', 'hunting', 'forestry', 'fishing', 'farming']
    },
    {
      flow       : 'IN',
      code       : '605/02',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Disinvestment by a resident corporate entity',
      description: 'Disinvestment of shares by resident - Mining, quarrying and exploration',
      tags       : ['Transactions', 'Financial', 'shares', 'Disinvestment', 'resident', 'entity', '(excluding local institutional investors)', 'Mining', 'quarrying', 'exploration']
    },
    {
      flow       : 'IN',
      code       : '605/03',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Disinvestment by a resident corporate entity',
      description: 'Disinvestment of shares by resident - Manufacturing',
      tags       : ['Transactions', 'Financial', 'shares', 'Disinvestment', 'resident', 'entity', '(excluding local institutional investors)', 'Manufacturing']
    },
    {
      flow       : 'IN',
      code       : '605/04',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Disinvestment by a resident corporate entity',
      description: 'Disinvestment of shares by resident - Electricity, gas and water supply',
      tags       : ['Transactions', 'Financial', 'shares', 'Disinvestment', 'resident', 'entity', '(excluding local institutional investors)', 'electricity', 'gas', 'water', 'utilities', 'energy']
    },
    {
      flow       : 'IN',
      code       : '605/05',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Disinvestment by a resident corporate entity',
      description: 'Disinvestment of shares by resident - Construction',
      tags       : ['Transactions', 'Financial', 'shares', 'Disinvestment', 'resident', 'entity', '(excluding local institutional investors)', 'construction']
    },
    {
      flow       : 'IN',
      code       : '605/06',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Disinvestment by a resident corporate entity',
      description: 'Disinvestment of shares by resident - Wholesale, retail, repairs, hotel and restaurants',
      tags       : ['Transactions', 'Financial', 'shares', 'Disinvestment', 'resident', 'entity', '(excluding local institutional investors)', 'wholesale', 'retail', 'repairs', 'hotels', 'restaurants']
    },
    {
      flow       : 'IN',
      code       : '605/07',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Disinvestment by a resident corporate entity',
      description: 'Disinvestment of shares by resident - Transport and communication',
      tags       : ['Transactions', 'Financial', 'shares', 'Disinvestment', 'resident', 'entity', '(excluding local institutional investors)', 'Transport', 'Communications']
    },
    {
      flow       : 'IN',
      code       : '605/08',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Disinvestment by a resident corporate entity',
      description: 'Disinvestment of shares by resident - Financial services',
      tags       : ['Transactions', 'Financial', 'shares', 'Disinvestment', 'resident', 'entity', '(excluding local institutional investors)', 'Services']
    },
    {
      flow       : 'IN',
      code       : '605/09',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Disinvestment by a resident corporate entity',
      description: 'Disinvestment of shares by resident - Community, social and personal services',
      tags       : ['Transactions', 'Financial', 'shares', 'Disinvestment', 'resident', 'entity', '(excluding local institutional investors)', 'Community', 'Social Services', 'Personal Services']
    },
    {
      flow       : 'IN',
      code       : '610/01',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities equity individual buy back',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'equity', '(excluding local institutional investors)', 'individual', 'buy back', 'investments', 'disinvestments']
    },
    {
      flow       : 'IN',
      code       : '610/02',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities equity corporate buy back',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'equity', '(excluding local institutional investors)', 'corporate', 'entity', 'buy back', 'investments', 'disinvestments']
    },
    {
      flow       : 'IN',
      code       : '610/03',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities equity bank buy back',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'equity', '(excluding local institutional investors)', 'bank', 'buy back', 'investments', 'disinvestments']
    },
    {
      flow       : 'IN',
      code       : '610/04',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities equity institution buy back',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'equity', '(excluding local institutional investors)', 'institution', 'buy back', 'investments', 'disinvestments']
    },
    {
      flow       : 'IN',
      code       : '611/01',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities debt individual redemption',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'debt', '(excluding local institutional investors)', 'individual', 'redemption', 'investments']
    },
    {
      flow       : 'IN',
      code       : '611/02',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities debt corporate redemption',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'debt', '(excluding local institutional investors)', 'corporate', 'entity', 'redemption', 'investments']
    },
    {
      flow       : 'IN',
      code       : '611/03',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities debt bank redemption',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'debt', '(excluding local institutional investors)', 'bank', 'redemption', 'investments']
    },
    {
      flow       : 'IN',
      code       : '611/04',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities debt institution redemption',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'debt', '(excluding local institutional investors)', 'institution', 'redemption', 'investments']
    },
    {
      flow       : 'IN',
      code       : '612/01',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities derivatives individual proceeds',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'proceeds', '(excluding local institutional investors)', 'individual', 'investments']
    },
    {
      flow       : 'IN',
      code       : '612/02',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities derivatives corporate proceeds',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'proceeds', '(excluding local institutional investors)', 'corporate', 'entity', 'investments']
    },
    {
      flow       : 'IN',
      code       : '612/03',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities derivatives bank proceeds',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'proceeds', '(excluding local institutional investors)', 'bank', 'investments']
    },
    {
      flow       : 'IN',
      code       : '612/04',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities derivatives institution proceeds',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'proceeds', '(excluding local institutional investors)', 'institution', 'investments']
    },
    {
      flow       : 'IN',
      code       : '615/01',
      oldCodes   : '704',
      section    : 'Financial investments/disinvestments and Prudential investments',
      subsection : 'Prudential investments (Institutional Investors and Banks)',
      description: 'Disinvestment by resident institutional investor - Asset Manager',
      tags       : ['Transactions', 'Financial', 'investments', '(Institutional Investors and Banks)', 'resident', 'institutional investor', 'Prudential', 'Disinvestment', 'Asset Manager']
    },
    {
      flow       : 'IN',
      code       : '615/02',
      oldCodes   : '704',
      section    : 'Financial investments/disinvestments and Prudential investments',
      subsection : 'Prudential investments (Institutional Investors and Banks)',
      description: 'Disinvestment by resident institutional investor - Collective Investment Scheme',
      tags       : ['Transactions', 'Financial', 'investments', '(Institutional Investors and Banks)', 'resident', 'institutional investor', 'Prudential', 'Disinvestment', 'Collective Investment Schemes']
    },
    {
      flow       : 'IN',
      code       : '615/03',
      oldCodes   : '704',
      section    : 'Financial investments/disinvestments and Prudential investments',
      subsection : 'Prudential investments (Institutional Investors and Banks)',
      description: 'Disinvestment by resident institutional investor - Retirement Fund',
      tags       : ['Transactions', 'Financial', 'investments', '(Institutional Investors and Banks)', 'resident', 'institutional investor', 'Prudential', 'Disinvestment', 'Retirement Funds']
    },
    {
      flow       : 'IN',
      code       : '615/04',
      oldCodes   : '704',
      section    : 'Financial investments/disinvestments and Prudential investments',
      subsection : 'Prudential investments (Institutional Investors and Banks)',
      description: 'Disinvestment by resident institutional investor - Life Linked',
      tags       : ['Transactions', 'Financial', 'investments', '(Institutional Investors and Banks)', 'resident', 'institutional investor', 'Prudential', 'Disinvestment', 'Life Linked']
    },
    {
      flow       : 'IN',
      code       : '615/05',
      oldCodes   : '704',
      section    : 'Financial investments/disinvestments and Prudential investments',
      subsection : 'Prudential investments (Institutional Investors and Banks)',
      description: 'Disinvestment by resident institutional investor - Life Non Linked',
      tags       : ['Transactions', 'Financial', 'investments', '(Institutional Investors and Banks)', 'resident', 'institutional investor', 'Prudential', 'Disinvestment', 'Life non linked']
    },
    {
      flow       : 'IN',
      code       : '616',
      oldCodes   : '705',
      section    : 'Financial investments/disinvestments and Prudential investments',
      subsection : 'Prudential investments (Institutional Investors and Banks)',
      description: 'Bank prudential disinvestment',
      tags       : ['Transactions', 'Financial', 'investments', '(Institutional Investors and Banks)', 'bank', 'Prudential', 'Disinvestment']
    },
    {
      flow       : 'IN',
      code       : '700',
      oldCodes   : '902,950',
      section    : 'Derivatives',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds related to derivatives',
      tags       : ['Derivatives', 'Financial', 'Adjustments', 'Reversals', 'Refunds']
    },
    {
      flow       : 'IN',
      code       : '701/01',
      oldCodes   : '804',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Options - listed',
      tags       : ['Derivatives', 'Financial', 'Options', 'listed', '(excluding inward listed)']
    },
    {
      flow       : 'IN',
      code       : '701/02',
      oldCodes   : '804',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Options - unlisted',
      tags       : ['Derivatives', 'Financial', 'Options', 'unlisted', '(excluding inward listed)']
    },
    {
      flow       : 'IN',
      code       : '702/01',
      oldCodes   : '805',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Futures - listed',
      tags       : ['Derivatives', 'Financial', 'Futures', 'listed', '(excluding inward listed)']
    },
    {
      flow       : 'IN',
      code       : '702/02',
      oldCodes   : '805',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Futures - unlisted',
      tags       : ['Derivatives', 'Financial', 'Futures', 'unlisted', '(excluding inward listed)']
    },
    {
      flow       : 'IN',
      code       : '703/01',
      oldCodes   : '806',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Warrants - listed',
      tags       : ['Derivatives', 'Financial', 'Warrants', 'listed', '(excluding inward listed)']
    },
    {
      flow       : 'IN',
      code       : '703/02',
      oldCodes   : '806',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Warrants - unlisted',
      tags       : ['Derivatives', 'Financial', 'Warrants', 'unlisted', '(excluding inward listed)']
    },
    {
      flow       : 'IN',
      code       : '704/01',
      oldCodes   : '202',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Gold hedging - listed',
      tags       : ['Derivatives', 'Financial', 'Gold', 'Hedging', 'listed', '(excluding inward listed)']
    },
    {
      flow       : 'IN',
      code       : '704/02',
      oldCodes   : '202',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Gold hedging - unlisted',
      tags       : ['Derivatives', 'Financial', 'Gold', 'Hedging', 'unlisted', '(excluding inward listed)']
    },
    {
      flow       : 'IN',
      code       : '705/01',
      oldCodes   : '202,804,805,806,901',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Derivative not specified above - listed',
      tags       : ['Derivatives', 'Financial', 'not specified', 'listed', '(excluding inward listed)']
    },
    {
      flow       : 'IN',
      code       : '705/02',
      oldCodes   : '202,804,805,806,901',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Derivative not specified above - unlisted',
      tags       : ['Derivatives', 'Financial', 'not specified', 'unlisted', '(excluding inward listed)']
    },
    {
      flow       : 'IN',
      code       : '800',
      oldCodes   : '902,950',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds related to loan and miscellaneous payments',
      tags       : ['Loan', 'Miscellaneous', 'Financial', 'Adjustments', 'Reversals', 'Refunds', 'payments']
    },
    {
      flow       : 'IN',
      code       : '801',
      oldCodes   : '999',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans granted to residents',
      description: 'Trade finance loan drawn down in South Africa',
      tags       : ['Loan', 'Repayment', 'Financial', 'trade finance', 'drawn down', 'South Africa']
    },
    {
      flow       : 'IN',
      code       : '802',
      oldCodes   : '999',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans granted to residents',
      description: 'International Bond drawn down',
      tags       : ['Loan', 'Repayment', 'Financial', 'drawn down', 'international Bond']
    },
    {
      flow       : 'IN',
      code       : '803',
      oldCodes   : '999',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans granted to residents',
      description: 'Loan made to a resident by a non-resident shareholder',
      tags       : ['Loan', 'Repayment', 'Financial', 'non-resident', 'shareholder']
    },
    {
      flow       : 'IN',
      code       : '804',
      oldCodes   : '999',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans granted to residents',
      description: 'Loan made to a resident by a non-resident third party',
      tags       : ['Loan', 'Repayment', 'Financial', 'non-resident', 'third party']
    },
    {
      flow       : 'IN',
      code       : '810',
      oldCodes   : '306,910,998',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans repaid by a residents temporarily abroad',
      description: 'Repayment by a resident temporarily abroad of a loan granted by a resident',
      tags       : ['Loan', 'Repayment', 'Financial', 'resident', 'temporarily abroad']
    },
    {
      flow       : 'IN',
      code       : '815',
      oldCodes   : '998',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans repaid by non-residents',
      description: 'Repayment of an individual loan to a resident',
      tags       : ['Loan', 'Repayment', 'Financial', 'resident', 'individual']
    },
    {
      flow       : 'IN',
      code       : '816',
      oldCodes   : '306,998',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans repaid by non-residents',
      description: 'Repayment of a study loan to a resident',
      tags       : ['Loan', 'Repayment', 'Financial', 'resident', 'Study', 'Tuition']
    },
    {
      flow       : 'IN',
      code       : '817',
      oldCodes   : '998',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans repaid by non-residents',
      description: 'Repayment of a shareholders loan to a resident',
      tags       : ['Loan', 'Repayment', 'Financial', 'resident', 'shareholders']
    },
    {
      flow       : 'IN',
      code       : '818',
      oldCodes   : '998',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans repaid by non-residents',
      description: 'Repayment of a third party loan to a resident (excluding shareholders)',
      tags       : ['Loan', 'Repayment', 'Financial', '(excluding shareholders)', 'Third Party']
    },
    {
      flow       : 'IN',
      code       : '819',
      oldCodes   : '998',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans repaid by non-residents',
      description: 'Repayment of a trade finance loan to a resident',
      tags       : ['Loan', 'Repayment', 'Financial', 'resident', 'trade finance']
    },
    {
      flow       : 'IN',
      code       : '830',
      oldCodes   : '901',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Miscellaneous payments',
      description: 'Details of payments not classified',
      tags       : ['Loan', 'Repayment', 'Financial', 'miscellaneous', 'not classified']
    },
    {
      flow       : 'IN',
      code       : '832',
      oldCodes   : '904',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Miscellaneous payments',
      description: 'Rand drafts/cheques drawn on vostro accounts (Only applicable if no description is available)',
      tags       : ['Loan', 'Repayment', 'Financial', 'miscellaneous', 'vostro', 'Rand drafts/cheques']
    },
    {
      flow       : 'IN',
      code       : '833',
      oldCodes   : '901',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Miscellaneous payments',
      description: 'Credit/Debit card company settlement as well as money remitter settlements',
      tags       : ['Loan', 'Repayment', 'Financial', 'money remitter', 'settlement', 'Credit/Debit card']
    },
    {
      flow       : 'OUT',
      code       : 'ZZ1',
      section    : 'Special',
      subsection : 'Report',
      description: 'Report this BOPCUS transaction as a NON REPORTABLE'
    },
    {
      flow       : 'OUT',
      code       : 'ZZ2',
      section    : 'Special',
      subsection : 'Do Not Report',
      description: 'Do not report this BOPCUS transaction'
    },
    {
      flow       : 'OUT',
      code       : '100',
      oldCodes   : '100',
      section    : 'Merchandise',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds applicable to merchandise',
      tags       : ['Merchandise', 'Adjustments', 'Reversals', 'Refunds']
    },
    {
      flow       : 'OUT',
      code       : '101/01',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (not in terms of import undertaking)',
      description: 'Import advance payment',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '101/02',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (not in terms of import undertaking)',
      description: 'Import advance payment - capital goods',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'capital goods', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '101/03',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (not in terms of import undertaking)',
      description: 'Import advance payment - gold',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'Gold', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '101/04',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (not in terms of import undertaking)',
      description: 'Import advance payment - platinum',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'platinum', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '101/05',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (not in terms of import undertaking)',
      description: 'Import advance payment - crude oil',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'crude oil', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '101/06',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (not in terms of import undertaking)',
      description: 'Import advance payment - refined petroleum products',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'refined petroleum products', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '101/07',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (not in terms of import undertaking)',
      description: 'Import advance payment - diamonds',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'diamonds', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '101/08',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (not in terms of import undertaking)',
      description: 'Import advance payment - steel',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'steel', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '101/09',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (not in terms of import undertaking)',
      description: 'Import advance payment - coal',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'coal', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '101/10',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (not in terms of import undertaking)',
      description: 'Import advance payment - iron ore',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'iron ore', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '101/11',
      oldCodes   : '101',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (not in terms of import undertaking)',
      description: 'Import advance payment - goods imported via the South African Post Office',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'Post Office', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '102/01',
      oldCodes   : '108',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (in terms of import undertaking)',
      description: 'Import advance payment',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '102/02',
      oldCodes   : '108',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (in terms of import undertaking)',
      description: 'Import advance payment - capital goods',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'capital goods', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '102/03',
      oldCodes   : '108',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (in terms of import undertaking)',
      description: 'Import advance payment - gold',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'Gold', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '102/04',
      oldCodes   : '108',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (in terms of import undertaking)',
      description: 'Import advance payment - platinum',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'platinum', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '102/05',
      oldCodes   : '108',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (in terms of import undertaking)',
      description: 'Import advance payment - crude oil',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'crude oil', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '102/06',
      oldCodes   : '108',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (in terms of import undertaking)',
      description: 'Import advance payment - refined petroleum products',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'refined petroleum products', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '102/07',
      oldCodes   : '108',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (in terms of import undertaking)',
      description: 'Import advance payment - diamonds',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'diamonds', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '102/08',
      oldCodes   : '108',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (in terms of import undertaking)',
      description: 'Import advance payment - steel',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'steel', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '102/09',
      oldCodes   : '108',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (in terms of import undertaking)',
      description: 'Import advance payment - coal',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'coal', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '102/10',
      oldCodes   : '108',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (in terms of import undertaking)',
      description: 'Import advance payment - iron ore',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'iron ore', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '102/11',
      oldCodes   : '108',
      section    : 'Merchandise',
      subsection : 'Imports: Advance payments (in terms of import undertaking)',
      description: 'Import advance payment - goods imported via the South African Post Office',
      tags       : ['Merchandise', 'Advance Payment', 'Imports', 'Post Office', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '103/01',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments and not in terms of import undertaking)',
      description: 'Import payment (excluding capital goods, gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal, iron ore and goods imported via the South African Post Office)',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '103/02',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments and not in terms of import undertaking)',
      description: 'Import payment - capital goods',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'capital goods', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '103/03',
      oldCodes   : '201',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments and not in terms of import undertaking)',
      description: 'Import payment - gold',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'Gold', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '103/04',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments and not in terms of import undertaking)',
      description: 'Import payment - platinum',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'platinum', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '103/05',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments and not in terms of import undertaking)',
      description: 'Import payment - crude oil',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'crude oil', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '103/06',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments and not in terms of import undertaking)',
      description: 'Import payment - refined petroleum products',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'refined petroleum products', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '103/07',
      oldCodes   : '114',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments and not in terms of import undertaking)',
      description: 'Import payment - diamonds',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'diamonds', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '103/08',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments and not in terms of import undertaking)',
      description: 'Import payment - steel',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'steel', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '103/09',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments and not in terms of import undertaking)',
      description: 'Import payment - coal',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'coal', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '103/10',
      oldCodes   : '102,103',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments and not in terms of import undertaking)',
      description: 'Import payment - iron ore',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'iron ore', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '103/11',
      oldCodes   : '113',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments and not in terms of import undertaking)',
      description: 'Import payment - goods imported via the South African Post Office',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'Post Office', 'not undertaking']
    },
    {
      flow       : 'OUT',
      code       : '104/01',
      oldCodes   : '109,110',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments but in terms of import undertaking)',
      description: 'Import payment (excluding capital goods, gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal, iron ore and goods imported via the South African Post Office)',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '104/02',
      oldCodes   : '109,110',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments but in terms of import undertaking)',
      description: 'Import payment - capital goods',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'capital goods', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '104/03',
      oldCodes   : '203',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments but in terms of import undertaking)',
      description: 'Import payment - gold',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'Gold', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '104/04',
      oldCodes   : '109,110',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments but in terms of import undertaking)',
      description: 'Import payment - platinum',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'platinum', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '104/05',
      oldCodes   : '109,110',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments but in terms of import undertaking)',
      description: 'Import payment- crude oil',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'crude oil', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '104/06',
      oldCodes   : '109,110',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments but in terms of import undertaking)',
      description: 'Import payment- refined petroleum products',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'refined petroleum products', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '104/07',
      oldCodes   : '115',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments but in terms of import undertaking)',
      description: 'Import payment - diamonds',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'diamonds', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '104/08',
      oldCodes   : '109,110',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments but in terms of import undertaking)',
      description: 'Import payment- steel',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'steel', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '104/09',
      oldCodes   : '109,110',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments but in terms of import undertaking)',
      description: 'Import payment- coal',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'coal', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '104/10',
      oldCodes   : '109,110',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments but in terms of import undertaking)',
      description: 'Import payment- iron ore',
      tags       : ['Merchandise', 'not Advance Payment', 'Imports', 'iron ore', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '104/11',
      oldCodes   : '109,110',
      section    : 'Merchandise',
      subsection : 'Imports: (excluding advance payments but in terms of import undertaking)',
      description: 'Import payment - goods imported via the South African Post Office',
      tags       : ['Merchandise', 'Imports', 'not Advance Payment', 'Post Office', 'undertaking']
    },
    {
      flow       : 'OUT',
      code       : '105',
      oldCodes   : '105',
      section    : 'Merchandise',
      subsection : 'Imports: Other',
      description: 'Consumables acquired in port',
      tags       : ['Merchandise', 'Imports', 'Port']
    },
    {
      flow       : 'OUT',
      code       : '106',
      oldCodes   : '107',
      section    : 'Merchandise',
      subsection : 'Imports: Other',
      description: 'Repayment of trade finance for imports',
      tags       : ['Merchandise', 'Imports', 'Trade Finance']
    },
    {
      flow       : 'OUT',
      code       : '107',
      oldCodes   : '112',
      section    : 'Merchandise',
      subsection : 'Imports: Other',
      description: 'Import payments where the Customs value of the shipment is less than R500',
      tags       : ['Merchandise', 'Imports', 'less than R500']
    },
    {
      flow       : 'OUT',
      code       : '108',
      oldCodes   : '102,103,112',
      section    : 'Merchandise',
      subsection : 'Imports: Other',
      description: 'Import payments where goods were declared as part of passenger baggage and no MRN is available',
      tags       : ['Merchandise', 'Imports', 'declared', 'customs']
    },
    {
      flow       : 'OUT',
      code       : '109/01',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Imports: Other',
      description: 'Payments for goods purchased from non-residents in cases where no physical import will take place, excluding gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal and iron ore as well as merchanting transactions',
      tags       : ['Merchandise', 'Imports', 'non physical']
    },
    {
      flow       : 'OUT',
      code       : '109/02',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Imports: Other',
      description: 'Payments for gold purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Imports', 'non physical', 'gold']
    },
    {
      flow       : 'OUT',
      code       : '109/03',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Imports: Other',
      description: 'Payments for platinum purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Imports', 'non physical', 'platinum']
    },
    {
      flow       : 'OUT',
      code       : '109/04',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Imports: Other',
      description: 'Payments for crude oil purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Imports', 'non physical', 'crude oil']
    },
    {
      flow       : 'OUT',
      code       : '109/05',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Imports: Other',
      description: 'Payments for refined petroleum products purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Imports', 'non physical', 'refined petroleum products']
    },
    {
      flow       : 'OUT',
      code       : '109/06',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Imports: Other',
      description: 'Payments for diamonds purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Imports', 'non physical', 'diamonds']
    },
    {
      flow       : 'OUT',
      code       : '109/07',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Imports: Other',
      description: 'Payments for steel purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Imports', 'non physical', 'steel']
    },
    {
      flow       : 'OUT',
      code       : '109/08',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Imports: Other',
      description: 'Payments for coal purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Imports', 'non physical', 'coal']
    },
    {
      flow       : 'OUT',
      code       : '109/09',
      oldCodes   : '116',
      section    : 'Merchandise',
      subsection : 'Imports: Other',
      description: 'Payments for iron ore purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions',
      tags       : ['Merchandise', 'Imports', 'non physical', 'iron ore']
    },
    {
      flow       : 'OUT',
      code       : '110',
      oldCodes   : '315',
      section    : 'Merchandise',
      subsection : 'Imports: Other',
      description: 'Merchanting transaction',
      tags       : ['Merchandise', 'Imports', 'Merchanting transaction']
    },
    {
      flow       : 'OUT',
      code       : '200',
      oldCodes   : '902,950',
      section    : 'Intellectual property and other services',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds applicable to intellectual property and service related items',
      tags       : ['Transactions', 'Adjustments', 'Reversals', 'Refunds']
    },
    {
      flow       : 'OUT',
      code       : '201',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Charges for the use of intellectual property',
      description: 'Rights obtained for licences to reproduce and/or distribute',
      tags       : ['Intellectual property', 'Rights', 'licences', 'reproduce', 'distribute']
    },
    {
      flow       : 'OUT',
      code       : '202',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Charges for the use of intellectual property',
      description: 'Rights obtained for using patents and inventions (licensing)',
      tags       : ['Intellectual property', 'Rights', 'licences', 'patents', 'inventions']
    },
    {
      flow       : 'OUT',
      code       : '203',
      oldCodes   : '312',
      section    : 'Intellectual property and other services',
      subsection : 'Charges for the use of intellectual property',
      description: 'Rights obtained for using patterns and designs (including industrial processes)',
      tags       : ['Intellectual property', 'Rights', 'patterns', 'designs']
    },
    {
      flow       : 'OUT',
      code       : '204',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Charges for the use of intellectual property',
      description: 'Rights obtained for using copyrights',
      tags       : ['Intellectual property', 'Rights', 'copyrights']
    },
    {
      flow       : 'OUT',
      code       : '205',
      oldCodes   : '605',
      section    : 'Intellectual property and other services',
      subsection : 'Charges for the use of intellectual property',
      description: 'Rights obtained for using franchises and trademarks.',
      tags       : ['Intellectual property', 'Rights', 'franchises', 'trademarks']
    },
    {
      flow       : 'OUT',
      code       : '210',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Acquisition of Intellectual Property',
      description: 'Acquisition of patents and inventions',
      tags       : ['Intellectual property', 'Acquisition', 'patents', 'inventions']
    },
    {
      flow       : 'OUT',
      code       : '211',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Acquisition of Intellectual Property',
      description: 'Acquisition of patterns and designs (including industrial processes)',
      tags       : ['Intellectual property', 'Acquisition', 'patterns', 'designs']
    },
    {
      flow       : 'OUT',
      code       : '212',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Acquisition of Intellectual Property',
      description: 'Acquisition of copyrights',
      tags       : ['Intellectual property', 'Acquisition', 'copyrights']
    },
    {
      flow       : 'OUT',
      code       : '213',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Acquisition of Intellectual Property',
      description: 'Acquisition of franchises and trademarks',
      tags       : ['Intellectual property', 'Acquisition', 'franchises', 'trademarks']
    },
    {
      flow       : 'OUT',
      code       : '220',
      oldCodes   : '319',
      section    : 'Intellectual property and other services',
      subsection : 'Research and Development',
      description: 'Payments for research and development services',
      tags       : ['services', 'research', 'development', 'Payments']
    },
    {
      flow       : 'OUT',
      code       : '221',
      oldCodes   : '504',
      section    : 'Intellectual property and other services',
      subsection : 'Research and Development',
      description: 'Funding for research and development',
      tags       : ['services', 'research', 'development', 'Funding']
    },
    {
      flow       : 'OUT',
      code       : '225',
      oldCodes   : '323',
      section    : 'Intellectual property and other services',
      subsection : 'Audiovisual and related items',
      description: 'Acquisition of original manuscripts, sound recordings and films',
      tags       : ['services', 'Acquisition', 'Audiovisual', 'manuscripts', 'recordings', 'films']
    },
    {
      flow       : 'OUT',
      code       : '226',
      oldCodes   : '323',
      section    : 'Intellectual property and other services',
      subsection : 'Audiovisual and related items',
      description: 'Payment relating to the production of motion pictures, radio and television programs and musical recordings',
      tags       : ['services', 'Payments', 'Audiovisual', 'manuscripts', 'recordings', 'films']
    },
    {
      flow       : 'OUT',
      code       : '230',
      oldCodes   : '312,605',
      section    : 'Intellectual property and other services',
      subsection : 'Computer software and related items',
      description: 'The outright purchasing of ownership rights of software',
      tags       : ['services', 'Acquisition', 'Rights', 'software']
    },
    {
      flow       : 'OUT',
      code       : '231',
      oldCodes   : '104,312,319',
      section    : 'Intellectual property and other services',
      subsection : 'Computer software and related items',
      description: 'Computer-related services including maintenance, repair and consultancy',
      tags       : ['services', 'maintenance', 'consultancy', 'repair', 'software']
    },
    {
      flow       : 'OUT',
      code       : '232',
      oldCodes   : '101,102,103,112,113,312',
      section    : 'Intellectual property and other services',
      subsection : 'Computer software and related items',
      description: 'Commercial purchases of customised software and related licenses to use',
      tags       : ['services', 'customised', 'licenses', 'software']
    },
    {
      flow       : 'OUT',
      code       : '233',
      oldCodes   : '101,102,103,112,113,312',
      section    : 'Intellectual property and other services',
      subsection : 'Computer software and related items',
      description: 'Commercial purchases of non-customised software on physical media with periodic licence to use',
      tags       : ['services', 'non-customised', 'periodic license', 'software']
    },
    {
      flow       : 'OUT',
      code       : '234',
      oldCodes   : '101,102,103,112,113,312',
      section    : 'Intellectual property and other services',
      subsection : 'Computer software and related items',
      description: 'Commercial purchases of non-customised software provided on physical media with right to perpetual (ongoing) use',
      tags       : ['services', 'non-customised', 'physical media', 'licenses', 'software']
    },
    {
      flow       : 'OUT',
      code       : '235',
      oldCodes   : '111,312',
      section    : 'Intellectual property and other services',
      subsection : 'Computer software and related items',
      description: 'Commercial purchases of non-customised software downloaded or electronically acquired with periodic license.',
      tags       : ['services', 'non-customised', 'downloaded', 'periodic license', 'software']
    },
    {
      flow       : 'OUT',
      code       : '236',
      oldCodes   : '111,312',
      section    : 'Intellectual property and other services',
      subsection : 'Computer software and related items',
      description: 'Commercial purchases of non-customised software downloaded or electronically acquired with single payment',
      tags       : ['services', 'non-customised', 'downloaded', 'licenses', 'software']
    },
    {
      flow       : 'OUT',
      code       : '240/01',
      oldCodes   : '320,321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on materials (excluding gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal and iron ore)',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'other']
    },
    {
      flow       : 'OUT',
      code       : '240/02',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on gold',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'gold']
    },
    {
      flow       : 'OUT',
      code       : '240/03',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on platinum',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'platinum']
    },
    {
      flow       : 'OUT',
      code       : '240/04',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on crude oil',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'crude oil']
    },
    {
      flow       : 'OUT',
      code       : '240/05',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on refined petroleum products',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'refined petroleum products']
    },
    {
      flow       : 'OUT',
      code       : '240/06',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on diamonds',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'diamonds']
    },
    {
      flow       : 'OUT',
      code       : '240/07',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on steel',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'steel']
    },
    {
      flow       : 'OUT',
      code       : '240/08',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on coal',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'coal']
    },
    {
      flow       : 'OUT',
      code       : '240/09',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Fees for processing - processing done on iron ore',
      tags       : ['services', 'Fees', 'Technical', 'processing', 'iron ore']
    },
    {
      flow       : 'OUT',
      code       : '241',
      oldCodes   : '104',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Repairs and maintenance on machinery and equipment',
      tags       : ['services', 'Technical', 'Repairs', 'maintenance', 'machinery', 'equipment']
    },
    {
      flow       : 'OUT',
      code       : '242',
      oldCodes   : '320',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Architectural, engineering and other technical services',
      tags       : ['services', 'Technical', 'Architectural', 'engineering']
    },
    {
      flow       : 'OUT',
      code       : '243',
      oldCodes   : '321',
      section    : 'Intellectual property and other services',
      subsection : 'Technical related services',
      description: 'Agricultural, mining, waste treatment and depollution services',
      tags       : ['services', 'Technical', 'Agricultural', 'mining', 'waste treatment', 'depollution']
    },
    {
      flow       : 'OUT',
      code       : '250',
      oldCodes   : '253',
      section    : 'Intellectual property and other services',
      subsection : 'Travel services for non-residents',
      description: 'Travel services for non-residents - business travel',
      tags       : ['services', 'Travel', 'non-residents', 'business']
    },
    {
      flow       : 'OUT',
      code       : '251',
      oldCodes   : '254',
      section    : 'Intellectual property and other services',
      subsection : 'Travel services for non-residents',
      description: 'Travel services for non-residents - holiday travel',
      tags       : ['services', 'Travel', 'non-residents', 'holiday']
    },
    {
      flow       : 'OUT',
      code       : '255',
      oldCodes   : '303',
      section    : 'Intellectual property and other services',
      subsection : 'Travel services for residents',
      description: 'Travel services for residents - business travel',
      tags       : ['services', 'Travel', 'residents', 'business']
    },
    {
      flow       : 'OUT',
      code       : '256',
      oldCodes   : '304',
      section    : 'Intellectual property and other services',
      subsection : 'Travel services for residents',
      description: 'Travel services for residents - holiday travel',
      tags       : ['services', 'Travel', 'residents', 'holiday']
    },
    {
      flow       : 'OUT',
      code       : '260',
      oldCodes   : '260',
      section    : 'Intellectual property and other services',
      subsection : 'Travel services in respect of third parties',
      description: 'Payment for travel services in respect of third parties - business travel',
      tags       : ['services', 'Travel', 'third parties', 'business']
    },
    {
      flow       : 'OUT',
      code       : '261',
      oldCodes   : '260',
      section    : 'Intellectual property and other services',
      subsection : 'Travel services in respect of third parties',
      description: 'Payment for travel services in respect of third parties - holiday travel',
      tags       : ['services', 'Travel', 'third parties', 'holiday']
    },
    {
      flow       : 'OUT',
      code       : '265',
      oldCodes   : '307',
      section    : 'Intellectual property and other services',
      subsection : 'Telecommunication and information services',
      description: 'Payment for telecommunication services',
      tags       : ['services', 'telecommunication', 'information']
    },
    {
      flow       : 'OUT',
      code       : '266',
      oldCodes   : '313,314',
      section    : 'Intellectual property and other services',
      subsection : 'Telecommunication and information services',
      description: 'Payment for information services including data, news related and news agency fees',
      tags       : ['services', 'data', 'information', 'news']
    },
    {
      flow       : 'OUT',
      code       : '270/01',
      oldCodes   : '302',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for passenger services - road',
      tags       : ['services', 'Transportation', 'passenger', 'road']
    },
    {
      flow       : 'OUT',
      code       : '270/02',
      oldCodes   : '302',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for passenger services - rail',
      tags       : ['services', 'Transportation', 'passenger', 'rail']
    },
    {
      flow       : 'OUT',
      code       : '270/03',
      oldCodes   : '302',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for passenger services - sea',
      tags       : ['services', 'Transportation', 'passenger', 'sea']
    },
    {
      flow       : 'OUT',
      code       : '270/04',
      oldCodes   : '302',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for passenger services - air',
      tags       : ['services', 'Transportation', 'passenger', 'air']
    },
    {
      flow       : 'OUT',
      code       : '271/01',
      oldCodes   : '301',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for freight services - road',
      tags       : ['services', 'Transportation', 'freight', 'road']
    },
    {
      flow       : 'OUT',
      code       : '271/02',
      oldCodes   : '301',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for freight services - rail',
      tags       : ['services', 'Transportation', 'freight', 'rail']
    },
    {
      flow       : 'OUT',
      code       : '271/03',
      oldCodes   : '301',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for freight services - sea',
      tags       : ['services', 'Transportation', 'freight', 'sea']
    },
    {
      flow       : 'OUT',
      code       : '271/04',
      oldCodes   : '301',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for freight services - air',
      tags       : ['services', 'Transportation', 'freight', 'air']
    },
    {
      flow       : 'OUT',
      code       : '272/01',
      oldCodes   : '301,308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for other transport services - road',
      tags       : ['services', 'Transportation', 'other', 'road']
    },
    {
      flow       : 'OUT',
      code       : '272/02',
      oldCodes   : '301,308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for other transport services - rail',
      tags       : ['services', 'Transportation', 'other', 'rail']
    },
    {
      flow       : 'OUT',
      code       : '272/03',
      oldCodes   : '301,308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for other transport services - sea',
      tags       : ['services', 'Transportation', 'other', 'sea']
    },
    {
      flow       : 'OUT',
      code       : '272/04',
      oldCodes   : '301,308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for other transport services - air',
      tags       : ['services', 'Transportation', 'other', 'air']
    },
    {
      flow       : 'OUT',
      code       : '273/01',
      oldCodes   : '308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for postal and courier services - road',
      tags       : ['services', 'Transportation', 'courier', 'postal', 'road']
    },
    {
      flow       : 'OUT',
      code       : '273/02',
      oldCodes   : '308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for postal and courier services - rail',
      tags       : ['services', 'Transportation', 'courier', 'postal', 'rail']
    },
    {
      flow       : 'OUT',
      code       : '273/03',
      oldCodes   : '308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for postal and courier services - sea',
      tags       : ['services', 'Transportation', 'courier', 'postal', 'sea']
    },
    {
      flow       : 'OUT',
      code       : '273/04',
      oldCodes   : '308',
      section    : 'Intellectual property and other services',
      subsection : 'Transportation services',
      description: 'Payment for postal and courier services - air',
      tags       : ['services', 'Transportation', 'courier', 'postal', 'air']
    },
    {
      flow       : 'OUT',
      code       : '275',
      oldCodes   : '311',
      section    : 'Intellectual property and other services',
      subsection : 'Financial services obtained',
      description: 'Commission and fees',
      tags       : ['services', 'Financial', 'Commission']
    },
    {
      flow       : 'OUT',
      code       : '276',
      oldCodes   : '311',
      section    : 'Intellectual property and other services',
      subsection : 'Financial services obtained',
      description: 'Financial service fees charged for advice provided',
      tags       : ['services', 'Financial', 'advice']
    },
    {
      flow       : 'OUT',
      code       : '280',
      oldCodes   : '309',
      section    : 'Intellectual property and other services',
      subsection : 'Construction services',
      description: 'Payment for construction services',
      tags       : ['services', 'Financial', 'Payment', 'Construction']
    },
    {
      flow       : 'OUT',
      code       : '282',
      oldCodes   : '325',
      section    : 'Intellectual property and other services',
      subsection : 'Government services',
      description: 'Diplomatic transfers',
      tags       : ['services', 'Financial', 'Diplomatic transfers', 'Government']
    },
    {
      flow       : 'OUT',
      code       : '285',
      oldCodes   : '305',
      section    : 'Intellectual property and other services',
      subsection : 'Study related services',
      description: 'Tuition fees',
      tags       : ['services', 'Financial', 'Fees', 'Tuition']
    },
    {
      flow       : 'OUT',
      code       : '287',
      oldCodes   : '317',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services obtained',
      description: 'Payment for legal services',
      tags       : ['services', 'Financial', 'Business', 'Payment', 'legal']
    },
    {
      flow       : 'OUT',
      code       : '288',
      oldCodes   : '317',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services obtained',
      description: 'Payment for accounting services',
      tags       : ['services', 'Financial', 'Business', 'Payment', 'accounting']
    },
    {
      flow       : 'OUT',
      code       : '289',
      oldCodes   : '317',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services obtained',
      description: 'Payment for management consulting services',
      tags       : ['services', 'Financial', 'Business', 'Payment', 'consulting']
    },
    {
      flow       : 'OUT',
      code       : '290',
      oldCodes   : '317',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services obtained',
      description: 'Payment for public relation services',
      tags       : ['services', 'Financial', 'Business', 'Payment', 'public relation']
    },
    {
      flow       : 'OUT',
      code       : '291',
      oldCodes   : '318',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services obtained',
      description: 'Payment for advertising & market research services',
      tags       : ['services', 'Financial', 'Business', 'Payment', 'advertising', 'market research']
    },
    {
      flow       : 'OUT',
      code       : '292',
      oldCodes   : '317',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services obtained',
      description: 'Payment for managerial services',
      tags       : ['services', 'Financial', 'Business', 'Payment', 'managerial']
    },
    {
      flow       : 'OUT',
      code       : '293',
      oldCodes   : '326',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services obtained',
      description: 'Payment for medical and dental services',
      tags       : ['services', 'Financial', 'Business', 'Payment', 'medical', 'dental']
    },
    {
      flow       : 'OUT',
      code       : '294',
      oldCodes   : '319',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services obtained',
      description: 'Payment for educational services',
      tags       : ['services', 'Financial', 'Business', 'Payment', 'educational']
    },
    {
      flow       : 'OUT',
      code       : '295',
      oldCodes   : '316',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services obtained',
      description: 'Operational leasing',
      tags       : ['services', 'Financial', 'Business', 'Payment', 'Operational leasing']
    },
    {
      flow       : 'OUT',
      code       : '296',
      oldCodes   : '324',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services obtained',
      description: 'Payment for cultural and recreational services',
      tags       : ['services', 'Financial', 'Business', 'Payment', 'cultural', 'recreational']
    },
    {
      flow       : 'OUT',
      code       : '297',
      oldCodes   : '901',
      section    : 'Intellectual property and other services',
      subsection : 'Other business services obtained',
      description: 'Payment for other business services not included elsewhere',
      tags       : ['services', 'Financial', 'Business', 'Payment', 'Other']
    },
    {
      flow       : 'OUT',
      code       : '300',
      oldCodes   : '902,950',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds related to income and yields on financial assets',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Adjustments', 'Reversals', 'Refunds']
    },
    {
      flow       : 'OUT',
      code       : '301',
      oldCodes   : '401',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Dividends',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Income', 'Payment', 'Dividends']
    },
    {
      flow       : 'OUT',
      code       : '302',
      oldCodes   : '403',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Branch profits',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Income', 'Branch profits']
    },
    {
      flow       : 'OUT',
      code       : '303',
      oldCodes   : '404',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Compensation paid by a resident to a resident employee temporarily abroad (excluding remittances)',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Compensation', 'resident employee temporarily abroad', 'excluding remittances']
    },
    {
      flow       : 'OUT',
      code       : '304',
      oldCodes   : '404',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Compensation paid by a resident to a non-resident employee (excluding remittances)',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Compensation', 'non-resident employee', 'excluding remittances']
    },
    {
      flow       : 'OUT',
      code       : '305',
      oldCodes   : '404',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Compensation paid by a resident to a migrant worker employee (excluding remittances)',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Compensation', 'migrant worker employee', 'excluding remittances']
    },
    {
      flow       : 'OUT',
      code       : '306',
      oldCodes   : '404',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Compensation paid by a resident to a foreign national contract worker employee (excluding remittances)',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Compensation', 'foreign national contract worker employee', 'excluding remittances']
    },
    {
      flow       : 'OUT',
      code       : '307',
      oldCodes   : '405',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Commission or brokerage',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Commission', 'brokerage']
    },
    {
      flow       : 'OUT',
      code       : '308',
      oldCodes   : '406',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Rental',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Rental']
    },
    {
      flow       : 'OUT',
      code       : '309/04',
      oldCodes   : '407',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Interest paid to a non-resident in respect of shareholders loans',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Interest', 'paid to a non-resident', 'shareholders', 'loans']
    },
    {
      flow       : 'OUT',
      code       : '309/05',
      oldCodes   : '407',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Interest paid to a non-resident in respect of third party loans',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Interest', 'paid to a non-resident', 'third party', 'loans']
    },
    {
      flow       : 'OUT',
      code       : '309/06',
      oldCodes   : '407',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Interest paid to a non-resident in respect of trade finance loans',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Interest', 'paid to a non-resident', 'trade finance', 'loans']
    },
    {
      flow       : 'OUT',
      code       : '309/07',
      oldCodes   : '407',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Interest paid to a non-resident in respect of a bond',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Interest', 'paid to a non-resident', 'bond']
    },
    {
      flow       : 'OUT',
      code       : '309/08',
      oldCodes   : '402',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Interest paid not in respect of loans',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Interest', 'paid to a non-resident', 'Other']
    },
    {
      flow       : 'OUT',
      code       : '312/01',
      oldCodes   : '311',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Fee in respect of inward listed securities derivatives individual',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Fees', 'inward listed', 'derivatives', 'securities', 'individual']
    },
    {
      flow       : 'OUT',
      code       : '312/02',
      oldCodes   : '311',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Fee in respect of inward listed securities derivatives corporate',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Fees', 'inward listed', 'derivatives', 'securities', 'corporate']
    },
    {
      flow       : 'OUT',
      code       : '312/03',
      oldCodes   : '311',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Fee in respect of inward listed securities derivatives bank',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Fees', 'inward listed', 'derivatives', 'securities', 'bank']
    },
    {
      flow       : 'OUT',
      code       : '312/04',
      oldCodes   : '311',
      section    : 'Transactions relating to income and yields on financial assets',
      subsection : 'Income payments',
      description: 'Fee in respect of inward listed securities derivatives institution',
      tags       : ['Transactions', 'Financial', 'income and yields', 'assets', 'Fees', 'inward listed', 'derivatives', 'securities', 'institution']
    },
    {
      flow       : 'OUT',
      code       : '400',
      oldCodes   : '902,950',
      section    : 'Transfers of a current nature',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds related to transfers of a current nature',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Adjustments', 'Reversals', 'Refunds']
    },
    {
      flow       : 'OUT',
      code       : '401',
      oldCodes   : '501',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Gifts',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Gifts']
    },
    {
      flow       : 'OUT',
      code       : '402',
      oldCodes   : '502',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Annual contributions',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Gifts']
    },
    {
      flow       : 'OUT',
      code       : '403',
      oldCodes   : '503',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Contributions in respect of social security schemes',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Gifts']
    },
    {
      flow       : 'OUT',
      code       : '404',
      oldCodes   : '504',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Contributions in respect of foreign charitable, religious and cultural (excluding research and development)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Contributions', 'charity', 'religious', 'cultural']
    },
    {
      flow       : 'OUT',
      code       : '405',
      oldCodes   : '501',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Other donations / aid to a foreign Government (excluding research and development)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Contributions', 'donations', 'aid', 'Government', 'foreign']
    },
    {
      flow       : 'OUT',
      code       : '406',
      oldCodes   : '501',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Other donations / aid to a foreign private sector (excluding research and development)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Contributions', 'donations', 'aid', 'private', 'foreign']
    },
    {
      flow       : 'OUT',
      code       : '407',
      oldCodes   : '505',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Pensions',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Pensions']
    },
    {
      flow       : 'OUT',
      code       : '408',
      oldCodes   : '506',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Annuities (pension related)',
      //description: 'Pensions',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Pensions', 'Annuities']
    },
    {
      flow       : 'OUT',
      code       : '409',
      oldCodes   : '508',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Inheritances',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Inheritances']
    },
    {
      flow       : 'OUT',
      code       : '410',
      oldCodes   : '509',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Alimony',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Alimony']
    },
    {
      flow       : 'OUT',
      code       : '411/01',
      oldCodes   : '510',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Tax - Income tax',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Tax', 'Income']
    },
    {
      flow       : 'OUT',
      code       : '411/02',
      oldCodes   : '510',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Tax - VAT refunds',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Tax', 'VAT']
    },
    {
      flow       : 'OUT',
      code       : '411/03',
      oldCodes   : '510,604',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Tax - Other',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Tax', 'Other']
    },
    {
      flow       : 'OUT',
      code       : '412',
      oldCodes   : '511',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Insurance premiums (non life/short term)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Insurance', 'premiums', '(non life/short term)']
    },
    {
      flow       : 'OUT',
      code       : '413',
      oldCodes   : '512',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Insurance claims (non life/short term)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Insurance', 'claims', '(non life/short term)']
    },
    {
      flow       : 'OUT',
      code       : '414',
      oldCodes   : '310',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Insurance premiums (life)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Insurance', 'premiums', 'life']
    },
    {
      flow       : 'OUT',
      code       : '415',
      oldCodes   : '310',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Insurance claims (life)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Insurance', 'claims', 'life']
    },
    {
      flow       : 'OUT',
      code       : '416',
      oldCodes   : '404',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Migrant worker remittances (excluding compensation)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'remittances', 'migrant worker employee', '(excluding compensation)']
    },
    {
      flow       : 'OUT',
      code       : '417',
      oldCodes   : '404',
      section    : 'Transfers of a current nature',
      subsection : 'Current payments',
      description: 'Foreign national contract worker remittances (excluding compensation)',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'remittances', 'foreign national contract worker employee', '(excluding compensation)']
    },
    {
      flow       : 'OUT',
      code       : '500',
      oldCodes   : '902,950',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds related to capital transfers and emigrants',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Adjustments', 'Reversals', 'Refunds', 'capital transfers', 'emigrants']

    },
    {
      flow       : 'OUT',
      code       : '501',
      oldCodes   : '601',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers relating to government / corporate entities (excluding loans)',
      description: 'Donations by SA Government for fixed assets',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Donations', 'SA Government', 'fixed assets', 'capital transfers', 'emigrants']
    },
    {
      flow       : 'OUT',
      code       : '502',
      oldCodes   : '603',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers relating to government / corporate entities (excluding loans)',
      description: 'Donations by corporate entities for fixed assets',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Donations', 'corporate', 'fixed assets', 'capital transfers', 'emigrants']
    },
    {
      flow       : 'OUT',
      code       : '503',
      oldCodes   : '602',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers relating to government / corporate entities (excluding loans)',
      description: 'Disinvestment of property by a non-resident corporate entity',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Disinvestment', 'property', 'non-resident', 'Capital', 'corporate']
    },
    {
      flow       : 'OUT',
      code       : '504',
      oldCodes   : '602',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers relating to government / corporate entities (excluding loans)',
      description: 'Investment into property by a resident corporate entity',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'property', 'resident', 'Capital', 'corporate']
    },
    {
      flow       : 'OUT',
      code       : '510/01',
      oldCodes   : '602',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by non-resident individuals',
      description: 'Disinvestment of property by a non-resident individual',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Disinvestment', 'property', 'non-resident', 'Capital', 'individuals']

    },
    {
      flow       : 'OUT',
      code       : '510/02',
      oldCodes   : '602',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by non-resident individuals',
      description: 'Disinvestment by a non-resident individual - other',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Disinvestment', 'Other', 'non-resident', 'Capital', 'individuals']
    },
    {
      flow       : 'OUT',
      code       : '511/01',
      oldCodes   : '606',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Investment by a resident individual not related to the investment allowance - Shares',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'Shares', 'resident', 'Capital', 'individuals', 'not related to the investment allowance']
    },
    {
      flow       : 'OUT',
      code       : '511/02',
      oldCodes   : '606',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Investment by a resident individual not related to the investment allowance - Bonds',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'Bonds', 'resident', 'Capital', 'individuals', 'not related to the investment allowance']
    },
    {
      flow       : 'OUT',
      code       : '511/03',
      oldCodes   : '606',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Investment by a resident individual not related to the investment allowance - Money market instruments',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'Money market', 'instruments', 'resident', 'Capital', 'individuals', 'not related to the investment allowance']
    },
    {
      flow       : 'OUT',
      code       : '511/04',
      oldCodes   : '606',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Investment by a resident individual not related to the investment allowance - Deposits with a foreign bank',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'Deposits', 'bank', 'resident', 'Capital', 'individuals', 'not related to the investment allowance']
    },
    {
      flow       : 'OUT',
      code       : '511/05',
      oldCodes   : '606',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Investment by a resident individual not related to the investment allowance - Mutual funds / collective investment schemes',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'Mutual funds', 'collective investment schemes', 'resident', 'Capital', 'individuals', 'not related to the investment allowance']

    },
    {
      flow       : 'OUT',
      code       : '511/06',
      oldCodes   : '606',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Investment by a resident individual not related to the investment allowance - Property',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'Property', 'resident', 'Capital', 'individuals', 'not related to the investment allowance']

    },
    {
      flow       : 'OUT',
      code       : '511/07',
      oldCodes   : '606',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Investment by a resident individual not related to the investment allowance - Other',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'Other', 'resident', 'Capital', 'individuals', 'not related to the investment allowance']

    },
    {
      flow       : 'OUT',
      code       : '512/01',
      oldCodes   : '606',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Foreign investment by a resident individual in respect of the investment allowance - Shares',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'Shares', 'resident', 'Capital', 'individuals', 'in respect of the investment allowance']
    },
    {
      flow       : 'OUT',
      code       : '512/02',
      oldCodes   : '606',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Foreign investment by a resident individual in respect of the investment allowance - Bonds',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'Bonds', 'resident', 'Capital', 'individuals', 'in respect of the investment allowance']
    },
    {
      flow       : 'OUT',
      code       : '512/03',
      oldCodes   : '606',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Foreign investment by a resident individual in respect of the investment allowance - Money market instruments',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'Money market', 'instruments', 'resident', 'Capital', 'individuals', 'in respect of the investment allowance']
    },
    {
      flow       : 'OUT',
      code       : '512/04',
      oldCodes   : '606',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Foreign investment by a resident individual in respect of the investment allowance - Deposits with a foreign bank',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'Deposits', 'bank', 'resident', 'Capital', 'individuals', 'in respect of the investment allowance']

    },
    {
      flow       : 'OUT',
      code       : '512/05',
      oldCodes   : '606',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Foreign investment by a resident individual in respect of the investment allowance - Mutual funds / collective investment schemes',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'Mutual funds', 'collective investment schemes', 'resident', 'Capital', 'individuals', 'in respect of the investment allowance']

    },
    {
      flow       : 'OUT',
      code       : '512/06',
      oldCodes   : '606',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Foreign investment by a resident individual in respect of the investment allowance - Property',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'Property', 'resident', 'Capital', 'individuals', 'in respect of the investment allowance']

    },
    {
      flow       : 'OUT',
      code       : '512/07',
      oldCodes   : '606',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Foreign investment by a resident individual in respect of the investment allowance - Other',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'Other', 'resident', 'Capital', 'individuals', 'in respect of the investment allowance']

    },
    {
      flow       : 'OUT',
      code       : '513',
      oldCodes   : '607',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Capital transfers by South African resident individuals',
      description: 'Investment by a resident individual originating from a local source into an account conducted in foreign currency held at an Authorised Dealer in South Africa',
      tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'FCA', 'resident', 'Capital', 'individuals']

    },
    // {
    //   "flow": "OUT",
    //   "code": "514/01",
    //   "oldCodes": "608",
    //   "section": "Capital Transfers and emigrants",
    //   "subsection": "Capital transfers by South African resident individuals",
    //   "description": "Foreign investment by a resident individual in respect of cross-border flows originating from a foreign currency account held at an Authorised Dealer in South Africa - Shares",
    //   tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'FCA', 'resident', 'Capital', 'individuals', 'foreign', 'shares']
    // }, {
    //   "flow": "OUT",
    //   "code": "514/02",
    //   "oldCodes": "608",
    //   "section": "Capital Transfers and emigrants",
    //   "subsection": "Capital transfers by South African resident individuals",
    //   "description": "Foreign investment by a resident individual in respect of cross-border flows originating from a foreign currency account held at an Authorised Dealer in South Africa - Bonds",
    //   tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'FCA', 'resident', 'Capital', 'individuals', 'foreign', 'bonds']
    // }, {
    //   "flow": "OUT",
    //   "code": "514/03",
    //   "oldCodes": "608",
    //   "section": "Capital Transfers and emigrants",
    //   "subsection": "Capital transfers by South African resident individuals",
    //   "description": "Foreign investment by a resident individual in respect of cross-border flows originating from a foreign currency account held at an Authorised Dealer in South Africa - Money market Instruments",
    //   tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'FCA', 'resident', 'Capital', 'individuals', 'foreign', 'instruments', 'money market']
    // }, {
    //   "flow": "OUT",
    //   "code": "514/04",
    //   "oldCodes": "608",
    //   "section": "Capital Transfers and emigrants",
    //   "subsection": "Capital transfers by South African resident individuals",
    //   "description": "Foreign investment by a resident individual in respect of cross-border flows originating from a foreign currency account held at an Authorised Dealer in South Africa - Deposit into a foreign bank account",
    //   tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'FCA', 'resident', 'Capital', 'individuals', 'foreign', 'deposit']
    // }, {
    //   "flow": "OUT",
    //   "code": "514/05",
    //   "oldCodes": "608",
    //   "section": "Capital Transfers and emigrants",
    //   "subsection": "Capital transfers by South African resident individuals",
    //   "description": "Foreign investment by a resident individual in respect of cross-border flows originating from a foreign currency account held at an Authorised Dealer in South Africa - Mutual funds / collective investment schemes",
    //   tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'FCA', 'resident', 'Capital', 'individuals', 'foreign', 'mutual funds']
    // }, {
    //   "flow": "OUT",
    //   "code": "514/06",
    //   "oldCodes": "608",
    //   "section": "Capital Transfers and emigrants",
    //   "subsection": "Capital transfers by South African resident individuals",
    //   "description": "Foreign investment by a resident individual in respect of cross-border flows originating from a foreign currency account held at an Authorised Dealer in South Africa - Property",
    //   tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'FCA', 'resident', 'Capital', 'individuals', 'foreign', 'property']
    // }, {
    //   "flow": "OUT",
    //   "code": "514/07",
    //   "oldCodes": "608",
    //   "section": "Capital Transfers and emigrants",
    //   "subsection": "Capital transfers by South African resident individuals",
    //   "description": "Foreign investment by a resident individual in respect of cross-border flows originating from a foreign currency account held at an Authorised Dealer in South Africa - Other",
    //   tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 'Investment', 'FCA', 'resident', 'Capital', 'individuals', 'foreign', 'other']
    // }, {
    //   "flow": "OUT",
    //   "code": "515/01",
    //   "oldCodes": "602",
    //   "section": "Capital Transfers and emigrants",
    //   "subsection": "Capital transfers by South African resident individuals",
    //   "description": "Re-transfer of capital repatriated in respect of an individual investment - Shares",
    //   tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 're-transfer', 'repatriation',  'individuals',  'shares']
    // }, {
    //   "flow": "OUT",
    //   "code": "515/02",
    //   "oldCodes": "602",
    //   "section": "Capital Transfers and emigrants",
    //   "subsection": "Capital transfers by South African resident individuals",
    //   "description": "Re-transfer of capital repatriated in respect of an individual investment - Bonds",
    //   tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 're-transfer', 'repatriation',  'individuals',  'bonds']
    // }, {
    //   "flow": "OUT",
    //   "code": "515/03",
    //   "oldCodes": "602",
    //   "section": "Capital Transfers and emigrants",
    //   "subsection": "Capital transfers by South African resident individuals",
    //   "description": "Re-transfer of capital repatriated in respect of an individual investment - Money market Instruments",
    //   tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 're-transfer', 'repatriation',  'individuals',  'instruments']
    // }, {
    //   "flow": "OUT",
    //   "code": "515/04",
    //   "oldCodes": "602",
    //   "section": "Capital Transfers and emigrants",
    //   "subsection": "Capital transfers by South African resident individuals",
    //   "description": "Re-transfer of capital repatriated in respect of an individual investment - Deposit with a foreign bank",
    //   tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 're-transfer', 'repatriation',  'individuals',  'deposit']
    // }, {
    //   "flow": "OUT",
    //   "code": "515/05",
    //   "oldCodes": "602",
    //   "section": "Capital Transfers and emigrants",
    //   "subsection": "Capital transfers by South African resident individuals",
    //   "description": "Re-transfer of capital repatriated in respect of an individual investment - Mutual funds / collective investment schemes",
    //   tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 're-transfer', 'repatriation',  'individuals',  'mutual funds', 'investement']
    // }, {
    //   "flow": "OUT",
    //   "code": "515/06",
    //   "oldCodes": "602",
    //   "section": "Capital Transfers and emigrants",
    //   "subsection": "Capital transfers by South African resident individuals",
    //   "description": "Re-transfer of capital repatriated in respect of an individual investment - Property",
    //   tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 're-transfer', 'repatriation',  'individuals',  'property']
    // }, {
    //   "flow": "OUT",
    //   "code": "515/07",
    //   "oldCodes": "602",
    //   "section": "Capital Transfers and emigrants",
    //   "subsection": "Capital transfers by South African resident individuals",
    //   "description": "Re-transfer of capital repatriated in respect of an individual investment - Other",
    //   tags       : ['Transactions', 'Financial', 'Transfers', 'Current', 're-transfer', 'repatriation',  'individuals',  'other']
    // },
    {
      flow       : 'OUT',
      code       : '530/01',
      oldCodes   : '609',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Emigrants',
      description: 'Emigration foreign capital allowance - fixed property',
      tags       : ['Transactions', 'Financial', 'Emigration', 'foreign capital allowance', 'fixed property']
    },
    {
      flow       : 'OUT',
      code       : '530/02',
      oldCodes   : '609',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Emigrants',
      description: 'Emigration foreign capital allowance - listed investments',
      tags       : ['Transactions', 'Financial', 'Emigration', 'foreign capital allowance', 'listed investments']
    },
    {
      flow       : 'OUT',
      code       : '530/03',
      oldCodes   : '609',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Emigrants',
      description: 'Emigration foreign capital allowance - unlisted investments',
      tags       : ['Transactions', 'Financial', 'Emigration', 'foreign capital allowance', 'unlisted investments']
    },
    {
      flow       : 'OUT',
      code       : '530/04',
      oldCodes   : '609',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Emigrants',
      description: 'Emigration foreign capital allowance - insurance policies',
      tags       : ['Transactions', 'Financial', 'Emigration', 'foreign capital allowance', 'insurance policies']
    },
    {
      flow       : 'OUT',
      code       : '530/05',
      oldCodes   : '609',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Emigrants',
      description: 'Emigration foreign capital allowance - cash',
      tags       : ['Transactions', 'Financial', 'Emigration', 'foreign capital allowance', 'cash']
    },
    {
      flow       : 'OUT',
      code       : '530/06',
      oldCodes   : '609',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Emigrants',
      description: 'Emigration foreign capital allowance - debtors',
      tags       : ['Transactions', 'Financial', 'Emigration', 'foreign capital allowance', 'debtors']
    },
    {
      flow       : 'OUT',
      code       : '530/07',
      oldCodes   : '609',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Emigrants',
      description: 'Emigration foreign capital allowance - capital distribution from trusts',
      tags       : ['Transactions', 'Financial', 'Emigration', 'foreign capital allowance', 'capital distribution from trusts']
    },
    {
      flow       : 'OUT',
      code       : '530/08',
      oldCodes   : '609',
      section    : 'Capital Transfers and emigrants',
      subsection : 'Emigrants',
      description: 'Emigration foreign capital allowance -other assets',
      tags       : ['Transactions', 'Financial', 'Emigration', 'foreign capital allowance', 'other assets']
    },
    {
      flow       : 'OUT',
      code       : '600',
      oldCodes   : '902,950',
      section    : 'Financial investments/disinvestments and Prudential investments',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds related to financial investments/disinvestments and prudential investments',
      tags       : ['Transactions', 'Financial', 'Adjustments', 'Reversals', 'Refunds', 'prudential']
    },
    {
      flow       : 'OUT',
      code       : '601/01',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Disinvestment by a non-resident',
      description: 'Listed shares - sale proceeds paid to a non-resident',
      tags       : ['Transactions', 'Financial', 'Listed', 'Shares', 'Disinvestment', 'non-resident', 'sale proceeds', '(excluding local institutional investors)']
    },
    {
      flow       : 'OUT',
      code       : '601/02',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Disinvestment by a non-resident',
      description: 'Non-listed shares - sale proceeds paid to a non-resident',
      tags       : ['Transactions', 'Financial', 'Non-listed', 'Shares', 'Disinvestment', 'non-resident', 'sale proceeds', '(excluding local institutional investors)']
    },
    {
      flow       : 'OUT',
      code       : '602',
      oldCodes   : '703',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Disinvestment by a non-resident',
      description: 'Disinvestment of money market instruments by a non-resident',
      tags       : ['Transactions', 'Financial', 'money market', 'instruments', 'Disinvestment', 'non-resident', '(excluding local institutional investors)']

    },
    {
      flow       : 'OUT',
      code       : '603/01',
      oldCodes   : '702',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Disinvestment by a non-resident',
      description: 'Disinvestment of listed bonds by a non-resident (excluding loans)',
      tags       : ['Transactions', 'Financial', 'Listed', 'bonds', '(excluding loans)', 'Disinvestment', 'non-resident', '(excluding local institutional investors)']
    },
    {
      flow       : 'OUT',
      code       : '603/02',
      oldCodes   : '702',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Disinvestment by a non-resident',
      description: 'Disinvestment of non-listed bonds by a non-resident (excluding loans)',
      tags       : ['Transactions', 'Financial', 'Non-listed', 'bonds', '(excluding loans)', 'Disinvestment', 'non-resident', '(excluding local institutional investors)']
    },
    {
      flow       : 'OUT',
      code       : '605/01',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Investment by a resident corporate entity',
      description: 'Investment into shares by a resident entity - Agriculture, hunting, forestry and fishing',
      tags       : ['Transactions', 'Financial', 'shares', 'Investment', 'resident', 'entity', '(excluding local institutional investors)', 'Agriculture', 'hunting', 'forestry', 'fishing']

    },
    {
      flow       : 'OUT',
      code       : '605/02',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Investment by a resident corporate entity',
      description: 'Investment into shares by a resident entity - Mining, quarrying and exploration',
      tags       : ['Transactions', 'Financial', 'shares', 'Investment', 'resident', 'entity', '(excluding local institutional investors)', 'Mining', 'quarrying', 'exploration']
    },
    {
      flow       : 'OUT',
      code       : '605/03',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Investment by a resident corporate entity',
      description: 'Investment into shares by a resident entity - Manufacturing',
      tags       : ['Transactions', 'Financial', 'shares', 'Investment', 'resident', 'entity', '(excluding local institutional investors)', 'Manufacturing']
    },
    {
      flow       : 'OUT',
      code       : '605/04',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Investment by a resident corporate entity',
      description: 'Investment into shares by a resident entity - Electricity, gas and water supply',
      tags       : ['Transactions', 'Financial', 'shares', 'Investment', 'resident', 'entity', '(excluding local institutional investors)', 'Electricity', 'gas', 'water']
    },
    {
      flow       : 'OUT',
      code       : '605/05',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Investment by a resident corporate entity',
      description: 'Investment into shares by a resident entity - Construction',
      tags       : ['Transactions', 'Financial', 'shares', 'Investment', 'resident', 'entity', '(excluding local institutional investors)', 'Construction']
    },
    {
      flow       : 'OUT',
      code       : '605/06',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Investment by a resident corporate entity',
      description: 'Investment into shares by a resident entity - Wholesale, retail, repairs, hotel and restaurants',
      tags       : ['Transactions', 'Financial', 'shares', 'Investment', 'resident', 'entity', '(excluding local institutional investors)', ' Wholesale', 'retail', 'repairs', 'hotel', 'restaurants']
    },
    {
      flow       : 'OUT',
      code       : '605/07',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Investment by a resident corporate entity',
      description: 'Investment into shares by a resident entity - Transport and communication',
      tags       : ['Transactions', 'Financial', 'shares', 'Investment', 'resident', 'entity', '(excluding local institutional investors)', ' Transport', 'communication']

    },
    {
      flow       : 'OUT',
      code       : '605/08',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Investment by a resident corporate entity',
      description: 'Investment into shares by a resident entity - Financial services',
      tags       : ['Transactions', 'Financial', 'shares', 'Investment', 'resident', 'entity', '(excluding local institutional investors)', 'services']
    },
    {
      flow       : 'OUT',
      code       : '605/09',
      oldCodes   : '701',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Investment by a resident corporate entity',
      description: 'Investment into shares by a resident entity - Community, social and personal services',
      tags       : ['Transactions', 'Financial', 'shares', 'Investment', 'resident', 'entity', '(excluding local institutional investors)', 'Community', 'social', 'personal']
    },
    {
      flow       : 'OUT',
      code       : '610/01',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities equity individual',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'equity', '(excluding local institutional investors)', 'individual']
    },
    {
      flow       : 'OUT',
      code       : '610/02',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities equity corporate',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'equity', '(excluding local institutional investors)', 'corporate']
    },
    {
      flow       : 'OUT',
      code       : '610/03',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities equity bank',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'equity', '(excluding local institutional investors)', 'bank']
    },
    {
      flow       : 'OUT',
      code       : '610/04',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities equity institution',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'equity', '(excluding local institutional investors)', 'institution']
    },
    {
      flow       : 'OUT',
      code       : '611/01',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities debt individual',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'debt', '(excluding local institutional investors)', 'individual']
    },
    {
      flow       : 'OUT',
      code       : '611/02',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities debt corporate',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'debt', '(excluding local institutional investors)', 'corporate']
    },
    {
      flow       : 'OUT',
      code       : '611/03',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities debt bank',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'debt', '(excluding local institutional investors)', 'bank']
    },
    {
      flow       : 'OUT',
      code       : '611/04',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities debt institution',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'debt', '(excluding local institutional investors)', 'institution']
    },
    {
      flow       : 'OUT',
      code       : '612/01',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities derivatives individual',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'derivatives', '(excluding local institutional investors)', 'individual']
    },
    {
      flow       : 'OUT',
      code       : '612/02',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities derivatives corporate',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'derivatives', '(excluding local institutional investors)', 'corporate']
    },
    {
      flow       : 'OUT',
      code       : '612/03',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities derivatives bank',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'derivatives', '(excluding local institutional investors)', 'bank']
    },
    {
      flow       : 'OUT',
      code       : '612/04',
      oldCodes   : '706',
      section    : 'Financial investments/disinvestments (excluding local institutional investors)',
      subsection : 'Inward listed investments',
      description: 'Inward listed securities derivatives institution',
      tags       : ['Transactions', 'Financial', 'Inward listed securities', 'derivatives', '(excluding local institutional investors)', 'institution']
    },
    {
      flow       : 'OUT',
      code       : '615/01',
      oldCodes   : '704',
      section    : 'Financial investments/disinvestments and Prudential investments',
      subsection : 'Prudential investments (Institutional Investors and Banks)',
      description: 'Investment by resident institutional investor - Asset Manager',
      tags       : ['Transactions', 'Financial', 'Investment', 'resident', 'Asset Manager', 'institution']
    },
    {
      flow       : 'OUT',
      code       : '615/02',
      oldCodes   : '704',
      section    : 'Financial investments/disinvestments and Prudential investments',
      subsection : 'Prudential investments (Institutional Investors and Banks)',
      description: 'Investment by resident institutional investor - Collective Investment Scheme',
      tags       : ['Transactions', 'Financial', 'Investment', 'resident', 'Collective Investment Scheme', 'institution']
    },
    {
      flow       : 'OUT',
      code       : '615/03',
      oldCodes   : '704',
      section    : 'Financial investments/disinvestments and Prudential investments',
      subsection : 'Prudential investments (Institutional Investors and Banks)',
      description: 'Investment by resident institutional investor - Retirement Fund',
      tags       : ['Transactions', 'Financial', 'Investment', 'resident', 'Retirement Fund', 'institution']
    },
    {
      flow       : 'OUT',
      code       : '615/04',
      oldCodes   : '704',
      section    : 'Financial investments/disinvestments and Prudential investments',
      subsection : 'Prudential investments (Institutional Investors and Banks)',
      description: 'Investment by resident institutional investor - Life Linked',
      tags       : ['Transactions', 'Financial', 'Investment', 'resident', 'Life Linked', 'institution']
    },
    {
      flow       : 'OUT',
      code       : '615/05',
      oldCodes   : '704',
      section    : 'Financial investments/disinvestments and Prudential investments',
      subsection : 'Prudential investments (Institutional Investors and Banks)',
      description: 'Investment by resident institutional investor - Life Non Linked',
      tags       : ['Transactions', 'Financial', 'Investment', 'resident', 'Life Non Linked', 'institution']
    },
    {
      flow       : 'OUT',
      code       : '616',
      oldCodes   : '705',
      section    : 'Financial investments/disinvestments and Prudential investments',
      subsection : 'Prudential investments (Institutional Investors and Banks)',
      description: 'Bank prudential investment',
      tags       : ['Transactions', 'Financial', 'Investment', 'prudential', 'Bank']
    },
    {
      flow       : 'OUT',
      code       : '700',
      oldCodes   : '902,950',
      section    : 'Derivatives',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds related to derivatives',
      tags       : ['Derivatives', 'Financial', 'Adjustments', 'Reversals', 'Refunds']
    },
    {
      flow       : 'OUT',
      code       : '701/01',
      oldCodes   : '804',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Options - listed',
      tags       : ['Derivatives', 'Financial', 'Options', 'listed']
    },
    {
      flow       : 'OUT',
      code       : '701/02',
      oldCodes   : '804',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Options - unlisted',
      tags       : ['Derivatives', 'Financial', 'Options', 'unlisted']
    },
    {
      flow       : 'OUT',
      code       : '702/01',
      oldCodes   : '805',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Futures - listed',
      tags       : ['Derivatives', 'Financial', 'Futures', 'listed']
    },
    {
      flow       : 'OUT',
      code       : '702/02',
      oldCodes   : '805',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Futures - unlisted',
      tags       : ['Derivatives', 'Financial', 'Futures', 'unlisted']
    },
    {
      flow       : 'OUT',
      code       : '703/01',
      oldCodes   : '806',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Warrants - listed',
      tags       : ['Derivatives', 'Financial', 'Warrants', 'listed']
    },
    {
      flow       : 'OUT',
      code       : '703/02',
      oldCodes   : '806',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Warrants - unlisted',
      tags       : ['Derivatives', 'Financial', 'Warrants', 'unlisted']
    },
    {
      flow       : 'OUT',
      code       : '704/01',
      oldCodes   : '202',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Gold hedging - listed',
      tags       : ['Derivatives', 'Financial', 'Gold hedging', 'listed']
    },
    {
      flow       : 'OUT',
      code       : '704/02',
      oldCodes   : '202',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Gold hedging - unlisted',
      tags       : ['Derivatives', 'Financial', 'Gold hedging', 'unlisted']
    },
    {
      flow       : 'OUT',
      code       : '705/01',
      oldCodes   : '202,804,805,806,901',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Derivative not specified above - listed',
      tags       : ['Derivatives', 'Financial', 'not specified', 'listed']
    },
    {
      flow       : 'OUT',
      code       : '705/02',
      oldCodes   : '202,804,805,806,901',
      section    : 'Derivatives',
      subsection : 'Derivatives (excluding inward listed)',
      description: 'Derivative not specified above - unlisted',
      tags       : ['Derivatives', 'Financial', 'not specified', 'unlisted']
    },
    {
      flow       : 'OUT',
      code       : '800',
      oldCodes   : '902,950',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Transaction adjustments',
      description: 'Adjustments / Reversals / Refunds related to loan and miscellaneous payments',
      tags       : ['Loan', 'Miscellaneous', 'Financial', 'Adjustments', 'Reversals', 'Refunds']
    },
    {
      flow       : 'OUT',
      code       : '801',
      oldCodes   : '999',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loan repayments by residents',
      description: 'Repayment of trade finance drawn down in South Africa',
      tags       : ['Loan', 'Repayment', 'Financial', 'trade finance', 'drawn down', 'South Africa']
    },
    {
      flow       : 'OUT',
      code       : '802',
      oldCodes   : '999',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loan repayments by residents',
      description: 'Repayment of an international Bond drawn down',
      tags       : ['Loan', 'Repayment', 'Financial', 'drawn down', 'international Bond']
    },
    {
      flow       : 'OUT',
      code       : '803',
      oldCodes   : '999',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loan repayments by residents',
      description: 'Repayment by a resident of a loan received from a non-resident shareholder',
      tags       : ['Loan', 'Repayment', 'Financial', 'non-resident', 'shareholder']
    },
    {
      flow       : 'OUT',
      code       : '804',
      oldCodes   : '999',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loan repayments by residents',
      description: 'Repayment by a resident of a loan received from a non-resident third party',
      tags       : ['Loan', 'Repayment', 'Financial', 'non-resident', 'third party']
    },
    {
      flow       : 'OUT',
      code       : '810',
      oldCodes   : '306,901,998',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans granted to residents temporarily abroad',
      description: 'Loan made by a resident to a resident temporarily abroad',
      tags       : ['Loan', 'granted', 'Financial', 'resident', 'temporarily abroad']
    },
    {
      flow       : 'OUT',
      code       : '815',
      oldCodes   : '998',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans granted to non-residents',
      description: 'Individual loan to a non-resident',
      tags       : ['Loan', 'granted', 'Financial', 'non-resident']
    },
    {
      flow       : 'OUT',
      code       : '816',
      oldCodes   : '306,998',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans granted to non-residents',
      description: 'Study loan to a non-resident',
      tags       : ['Loan', 'granted', 'Financial', 'non-resident', 'Study']
    },
    {
      flow       : 'OUT',
      code       : '817',
      oldCodes   : '998',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans granted to non-residents',
      description: 'Shareholders loan to a non-resident',
      tags       : ['Loan', 'granted', 'Financial', 'non-resident', 'Shareholders']
    },
    {
      flow       : 'OUT',
      code       : '818',
      oldCodes   : '998',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans granted to non-residents',
      description: 'Third party loan to a non-resident (excluding shareholders)',
      tags       : ['Loan', 'granted', 'Financial', 'non-resident', 'excluding Shareholders', 'third party']
    },
    {
      flow       : 'OUT',
      code       : '819',
      oldCodes   : '998',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Loans granted to non-residents',
      description: 'Trade finance to a non-resident',
      tags       : ['Loan', 'granted', 'Financial', 'non-resident', 'Trade finance']
    },
    {
      flow       : 'OUT',
      code       : '830',
      oldCodes   : '901',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Miscellaneous payments',
      description: 'Details of payments not classified',
      tags       : ['Miscellaneous', 'payments', 'not classified']
    },
    {
      flow       : 'OUT',
      code       : '831',
      oldCodes   : '903',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Miscellaneous payments',
      description: 'Rand collections for the credit of vostro accounts',
      tags       : ['Miscellaneous', 'payments', 'vostro']
    },
    {
      flow       : 'OUT',
      code       : '833',
      oldCodes   : '901',
      section    : 'Loan and Miscellaneous payments',
      subsection : 'Miscellaneous payments',
      description: 'Credit/Debit card company settlement as well as money remitter settlements',
      tags       : ['Miscellaneous', 'payments', 'settlement']
    }
  ]
})
