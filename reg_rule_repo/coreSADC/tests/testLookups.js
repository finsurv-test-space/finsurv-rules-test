define({
  ReportingQualifier           : ["BOPCUS", "NON REPORTABLE", "BOPCARD RESIDENT", "BOPCARD NON RESIDENT", "NON RESIDENT RAND", "INTERBANK", "BOPDIR"],
  ihqCompanies : [
    {
      ihqCode : "IHQ123",
      registrationNumber : "2013/1234567/07",
      companyName : "company1"
    },
    {
      ihqCode : "IHQ124",
      registrationNumber : "2013/1234568/07",
      companyName : "company2"
    }
  ],
  luClientCCNs : [
    {
      ccn : "12345678",
      accountName : "TEST 1",
      accountNumber : "2"
    },
    {
      ccn : "98765432",
      accountName : "TEST 2",
      accountNumber : "1"
    }
  ]
})