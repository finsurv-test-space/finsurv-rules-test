define(function () {
  return function (testBase) {
    with (testBase) {
      var mappings = {
        LocalCurrencySymbol: "R",
        LocalCurrencyName: "Rand",
        LocalCurrency: "ZAR",
        LocalValue: "RandValue",
        Regulator: "SARB",
        DealerPrefix: "AD",
        RegulatorPrefix: "SARB",
        Locale:'ZA'
      };

      setMappings(mappings, true);

      var test_cases = [

        assertSuccess("ReportingQualifier.minLen", { ReportingQualifier: 'BOPCUS' }),
        assertSuccess("ReportingQualifier.maxLen", { ReportingQualifier: 'BOPCUS' }),
        assertFailure("ReportingQualifier.minLen", { ReportingQualifier: 'BOPCU' }),
        assertWarning("ReportingQualifier.maxLen", { ReportingQualifier: '12345678901234567890123456' }),

        //ReplacementTransaction
        assertSuccess("repyn1", { ReportingQualifier: 'BOPCUS' }),
        assertSuccess("repyn1", {
          ReportingQualifier: 'BOPCUS',
          ReplacementTransaction: "Y",
          ReplacementOriginalReference: "123"
        }),
        assertFailure("repyn1", { ReportingQualifier: 'BOPCUS', ReplacementOriginalReference: "123" }),

        assertSuccess("repyn2", { ReportingQualifier: 'BOPCUS' }),
        assertSuccess("repyn2", {
          ReportingQualifier: 'BOPCUS',
          ReplacementTransaction: "Y",
          ReplacementOriginalReference: "123"
        }),
        assertFailure("repyn2", {
          ReportingQualifier: 'BOPCUS',
          ReplacementTransaction: "E",
          ReplacementOriginalReference: "123"
        }),

        //ReplacementOriginalReference
        // If ReplacementTransaction is Y, must be completed
        assertSuccess("repor1", { ReportingQualifier: 'BOPCUS' }),
        assertSuccess("repor1", {
          ReportingQualifier: 'BOPCUS',
          ReplacementTransaction: "Y",
          ReplacementOriginalReference: "123"
        }),
        assertFailure("repor1", {
          ReportingQualifier: 'BOPCUS',
          ReplacementTransaction: "Y",
          ReplacementOriginalReference: ""
        }),
        assertFailure("repor1", { ReportingQualifier: 'BOPCUS', ReplacementTransaction: "Y" }),
        // Must not be completed if ReplacementTransactionIndicator is N
        assertSuccess("repor2", { ReportingQualifier: 'BOPCUS' }),
        assertSuccess("repor2", { ReportingQualifier: 'BOPCUS', ReplacementTransaction: "N" }),
        assertSuccess("repor2", {
          ReportingQualifier: 'BOPCUS',
          ReplacementTransaction: "N",
          ReplacementOriginalReference: ""
        }),
        assertFailure("repor2", {
          ReportingQualifier: 'BOPCUS',
          ReplacementTransaction: "N",
          ReplacementOriginalReference: "123"
        }),
        // Additional spaces identified in data content
        assertSuccess("repor3", {
          ReportingQualifier: 'BOPCUS',
          ReplacementOriginalReference: ""
        }),
        assertSuccess("repor3", {
          ReportingQualifier: 'BOPCUS',
          ReplacementOriginalReference: "1234"
        }),
        assertSuccess("repor3", {
          ReportingQualifier: 'BOPCUS',
          ReplacementOriginalReference: "12 34"
        }),
        assertFailure("repor3", {
          ReportingQualifier: 'BOPCUS',
          ReplacementOriginalReference: "12  34"
        }),
        assertFailure("repor3", {
          ReportingQualifier: 'BOPCUS',
          ReplacementOriginalReference: " 1234"
        }),
        assertFailure("repor3", {
          ReportingQualifier: 'BOPCUS',
          ReplacementOriginalReference: " 1234 "
        }),

        // ReportingQualifier
        assertSuccess("repq1", { ReportingQualifier: 'BOPCUS' }),
        assertFailure("repq1", {}),
        assertSuccess("repq2", { ReportingQualifier: 'BOPCUS' }),
        assertFailure("repq2", { ReportingQualifier: 'DNE!' }),

        // Flow
        assertFailure("flow1", {}),
        assertSuccess("flow1", { Flow: 'IN' }),
        //Must contain the value IN or OUT
        assertSuccess("flow2", { ReportingQualifier: 'BOPCUS', Flow: 'IN' }),
        assertSuccess("flow2", { ReportingQualifier: 'BOPCUS', Flow: 'OUT' }),
        assertFailure("flow2", { ReportingQualifier: 'BOPCUS', Flow: 'OVER' }),

        assertFailure("flow3", { ReportingQualifier: 'BOPCARD NON RESIDENT', Flow: 'OUT' }),
        assertSuccess("flow3", { ReportingQualifier: 'BOPCARD NON RESIDENT', Flow: 'IN' }),
        //The flow direction of the report data must match the flow on the account entry. Please ask IT support to investigate
        assertSuccessCustom("flow4", { ReportingQualifier: 'BOPCUS', Flow: 'IN' }, {
          DealerType: "AD",
          AccountFlow: "IN"
        }),
        assertSuccessCustom("flow4", { ReportingQualifier: 'BOPCUS', Flow: 'IN' }, { DealerType: "AD" }),
        assertSuccessCustom("flow4", { ReportingQualifier: 'BOPCUS', Flow: 'OUT' }, { DealerType: "AD" }),
        assertFailureCustom("flow4", { ReportingQualifier: 'BOPCUS', Flow: 'IN' }, {
          DealerType: "AD",
          AccountFlow: "OUT"
        }),
        //The flow direction of the report data must match the flow on the account entry unless this is a VOSTRO related transaction. Please ask IT support to investigate
        assertSuccessCustom("flow5", { ReportingQualifier: 'NON REPORTABLE', Flow: 'IN' }, {
          DealerType: "AD",
          AccountFlow: "IN"
        }),
        assertSuccessCustom("flow5", { ReportingQualifier: 'NON REPORTABLE', Flow: 'IN' }, { DealerType: "AD" }),
        assertSuccessCustom("flow5", {
          ReportingQualifier: 'NON REPORTABLE', Flow: 'IN',
          Resident: { Exception: { ExceptionName: 'VOSTRO NON REPORTABLE' } }
        }, { DealerType: "AD", AccountFlow: "OUT" }),
        assertSuccessCustom("flow5", {
          ReportingQualifier: 'NON REPORTABLE', Flow: 'IN',
          NonResident: { Exception: { ExceptionName: 'VOSTRO NON REPORTABLE' } }
        }, { DealerType: "AD", AccountFlow: "OUT" }),
        assertFailureCustom("flow5", {
          ReportingQualifier: 'NON REPORTABLE', Flow: 'IN',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } }
        }, { DealerType: "AD", AccountFlow: "OUT" }),
        assertFailureCustom("flow5", { ReportingQualifier: 'NON REPORTABLE', Flow: 'IN' }, {
          DealerType: "AD",
          AccountFlow: "OUT"
        }),

        // ValueDate
        assertFailure("vd1", {}),
        assertSuccess("vd1", { ValueDate: "2013-01-01" }),
        assertFailure("vd2", { ValueDate: "2013-01-31" }),
        assertSuccess("vd2", { ValueDate: "2013-01-22" }),
        assertWarning("vd3", { ValueDate: "2012-01-01" }),
        assertSuccess("vd3", { ValueDate: "2013-01-17" }),
        assertSuccess("vd4", { ValueDate: "2013-08-18" }),
        assertFailure("vd4", { ValueDate: "2013-08-17" }),
        assertSuccess("vd5", { ValueDate: "2013-08-17" }),
        assertFailure("vd5", { ValueDate: "2013-13-32" }),


        // FlowCurrency
        assertFailure("fcurr1", {}),
        assertSuccess("fcurr1", { FlowCurrency: 'ZAR' }),

        // TrnReference
        assertFailure("tref1", {}),
        assertSuccess("tref1", { TrnReference: 'trnref1' }),

        assertSuccess("tref2", { TrnReference: 'trnref' }),
        assertFailure("tref2", { TrnReference: 'trn  ref' }),
        assertFailure("tref2", { TrnReference: ' trnref' }),
        assertFailure("tref2", { TrnReference: 'trnref ' }),

        // OriginatingBank
        assertFailure("obank1", { ReportingQualifier: 'BOPCUS', Flow: 'OUT' }),
        assertSuccess("obank1", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', OriginatingBank: 'XYZ' }),
        assertFailure("obank2", { ReportingQualifier: 'BOPCARD RESIDENT', OriginatingBank: 'XYZ' }),
        assertSuccess("obank2", { ReportingQualifier: 'BOPCARD RESIDENT', OriginatingBank: '' }),
        assertFailure("obank3", { ReportingQualifier: 'NON REPORTABLE', Flow: 'IN' }),
        assertSuccess("obank3", { ReportingQualifier: 'NON REPORTABLE', Flow: 'IN', OriginatingBank: 'XYZ' }),
        assertSuccess("obank3", {
          ReportingQualifier: 'NON REPORTABLE', Flow: 'IN',
          NonResident: { Individual: { AccountIdentifier: 'CASH' } }
        }),
        assertSuccess("obank3", {
          ReportingQualifier: 'NON REPORTABLE', Flow: 'IN',
          NonResident: { Entity: { AccountIdentifier: 'CASH' } }
        }),
        assertNoRule("obank3", { ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingBank: 'XYZ' }),

        assertSuccess("obank4", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '250' }, { CategoryCode: '101' }]
        }),
        assertFailure("obank4", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingBank: 'XYZ',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '250' }, { CategoryCode: '101' }]
        }),
        assertSuccess("obank4", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingBank: 'XYZ',
          Resident: { Exception: { ExceptionName: 'BULK INTEREST' } },
          MonetaryAmount: [{ CategoryCode: '250' }, { CategoryCode: '101' }]
        }),
        assertNoRule("obank4", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingBank: 'XYZ',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '101' }]
        }),

        assertSuccess("obank5", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '252' }, { CategoryCode: '101' }]
        }),
        assertFailure("obank5", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingBank: 'XYZ',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '252' }, { CategoryCode: '101' }]
        }),
        assertSuccess("obank5", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingBank: 'XYZ',
          NonResident: { Exception: { ExceptionName: 'BULK INTEREST' } },
          MonetaryAmount: [{ CategoryCode: '252' }, { CategoryCode: '101' }]
        }),
        assertNoRule("obank5", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingBank: 'XYZ',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '101' }]
        }),

        assertFailure("obank6", { ReportingQualifier: 'BOPCUS', OriginatingCountry: 'ZA' }),
        assertSuccess("obank6", { ReportingQualifier: 'BOPCUS', OriginatingCountry: 'ZA', OriginatingBank: 'XYZ' }),

        //If Flow is IN, must be completed except if the Non-Resident AccountIdentifier is CASH
        assertSuccess("obank7", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingBank: 'XYZ',
          MonetaryAmount: [{ CategoryCode: '101' }]
        }),
        assertSuccess("obank7", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          NonResident: { Individual: { AccountIdentifier: 'CASH' } },
          MonetaryAmount: [{ CategoryCode: '101' }]
        }),
        assertSuccess("obank7", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          NonResident: { Entity: { AccountIdentifier: 'CASH' } },
          MonetaryAmount: [{ CategoryCode: '101' }]
        }),
        assertFailure("obank7", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '101' }]
        }),
        assertNoRule("obank7", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '251' }]
        }),
        //If Flow is IN, must be completed except if the Non-Resident AccountIdentifier is CASH or Resident ExceptionName is MUTUAL PARTY
        assertSuccess("obank8", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingBank: 'XYZ',
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),
        assertSuccess("obank8", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          NonResident: { Individual: { AccountIdentifier: 'CASH' } },
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),
        assertSuccess("obank8", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),
        assertFailure("obank8", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          NonResident: { Individual: { AccountIdentifier: 'NOT CASH' } },
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),
        assertFailure("obank8", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Exception: { ExceptionName: 'NOT MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),
        //If Flow is IN, must be completed except if the Non-Resident AccountIdentifier is CASH or NonResident ExceptionName is MUTUAL PARTY
        assertSuccess("obank9", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingBank: 'XYZ',
          MonetaryAmount: [{ CategoryCode: '256' }]
        }),
        assertSuccess("obank9", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          NonResident: { Individual: { AccountIdentifier: 'CASH' } },
          MonetaryAmount: [{ CategoryCode: '256' }]
        }),
        assertSuccess("obank9", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '256' }]
        }),
        assertFailure("obank9", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          NonResident: { Individual: { AccountIdentifier: 'NOT CASH' } },
          MonetaryAmount: [{ CategoryCode: '256' }]
        }),
        assertFailure("obank9", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          NonResident: { Exception: { ExceptionName: 'NOT MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '256' }]
        }),


        // failure("ocntry7", 229, "If category 252, 255 or 256 is used and the Flow is IN and the NonResident ExceptionName is MUTUAL PARTY, the OriginatingCountry may not be completed",
        //   notEmpty.and(hasNonResException("MUTUAL PARTY"))).onInflow().onSection("AB").onCategory(["252", "255", "256"])

        // OriginatingCountry
        assertSuccess("ocntry1", { ReportingQualifier: 'BOPCUS', OriginatingCountry: 'ZA', OriginatingBank: 'XYZ' }),
        assertFailure("ocntry1", { ReportingQualifier: 'BOPCUS', OriginatingBank: 'XYZ' }),
        assertFailure("ocntry2", { ReportingQualifier: 'BOPCARD RESIDENT', OriginatingCountry: 'ZA' }),
        assertSuccess("ocntry2", { ReportingQualifier: 'BOPCARD RESIDENT' }),
        assertFailure("ocntry3", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', OriginatingCountry: 'US' }),
        assertSuccess("ocntry3", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', OriginatingCountry: 'ZA' }),
        assertSuccess("ocntry4", { ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingCountry: 'US' }),
        assertFailure("ocntry4", { ReportingQualifier: 'BOPCUS', Flow: 'IN', OriginatingCountry: 'ZA' }),
        assertSuccess("ocntry5", { ReportingQualifier: 'INTERBANK', OriginatingCountry: 'US' }),
        assertFailure("ocntry5", { ReportingQualifier: 'INTERBANK', OriginatingCountry: 'ZZ' }),
        assertSuccess("ocntry5", { ReportingQualifier: 'BOPCUS', OriginatingCountry: 'US' }),
        assertFailure("ocntry5", { ReportingQualifier: 'BOPCUS', OriginatingCountry: 'ZZ' }),
        assertFailure("ocntry6", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '251' }],
          OriginatingCountry: 'US'
        }),
        assertSuccess("ocntry6", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '251' }],
          OriginatingCountry: ''
        }),
        assertFailure("ocntry7", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '252' }],
          OriginatingCountry: 'US'
        }),
        assertSuccess("ocntry7", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '252' }],
          OriginatingCountry: ''
        }),

        // CorrespondentBank
        assertSuccess("cbank1", { ReportingQualifier: 'BOPCUS', CorrespondentCountry: 'ZA', CorrespondentBank: 'XYZ' }),
        assertFailure("cbank1", { ReportingQualifier: 'BOPCUS', CorrespondentCountry: 'ZA' }),
        assertSuccess("cbank2", { ReportingQualifier: 'BOPCARD RESIDENT' }),
        assertFailure("cbank2", { ReportingQualifier: 'BOPCARD RESIDENT', CorrespondentBank: 'ZA' }),

        // CorrespondentCountry
        assertSuccess("ccntry1", {
          ReportingQualifier: 'BOPCUS',
          CorrespondentCountry: 'ZA',
          CorrespondentBank: 'XYZ'
        }),
        assertFailure("ccntry1", { ReportingQualifier: 'BOPCUS', CorrespondentBank: 'XYZ' }),
        assertFailure("ccntry2", { ReportingQualifier: 'BOPCARD RESIDENT', CorrespondentCountry: 'ZA' }),
        assertSuccess("ccntry2", { ReportingQualifier: 'BOPCARD RESIDENT' }),
        assertFailure("ccntry3", { ReportingQualifier: 'BOPCUS', CorrespondentCountry: 'ZZ' }),
        assertSuccess("ccntry3", { ReportingQualifier: 'BOPCUS', CorrespondentCountry: 'ZA' }),
        assertNoRule("ccntry3", { ReportingQualifier: 'BOPCUS', CorrespondentBank: '', CorrespondentCountry: '' }),

        // ReceivingBank
        assertFailure("rbank1", { ReportingQualifier: 'BOPCUS', Flow: 'IN' }),
        assertSuccess("rbank1", { ReportingQualifier: 'BOPCUS', Flow: 'IN', ReceivingBank: 'XYZ' }),
        assertFailure("rbank2", { ReportingQualifier: 'BOPCARD RESIDENT', ReceivingBank: 'XYZ' }),
        assertSuccess("rbank2", { ReportingQualifier: 'BOPCARD RESIDENT', ReceivingBank: '' }),
        assertFailure("rbank3", { ReportingQualifier: 'BOPCUS', ReceivingCountry: 'ZA' }),
        assertSuccess("rbank3", { ReportingQualifier: 'BOPCUS', ReceivingCountry: 'ZA', ReceivingBank: 'XYZ' }),

        assertFailure("rbank4", { ReportingQualifier: 'INTERBANK', Flow: 'OUT' }),
        assertSuccess("rbank4", { ReportingQualifier: 'INTERBANK', Flow: 'OUT', ReceivingBank: 'XYZ' }),
        assertSuccess("rbank4", {
          ReportingQualifier: 'INTERBANK', Flow: 'OUT',
          NonResident: { Individual: { AccountIdentifier: 'CASH' } }
        }),
        assertSuccess("rbank4", {
          ReportingQualifier: 'INTERBANK', Flow: 'OUT',
          NonResident: { Entity: { AccountIdentifier: 'CASH' } }
        }),

        assertSuccess("rbank5", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '250' }, { CategoryCode: '101' }]
        }),
        assertSuccess("rbank5", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', ReceivingBank: 'XYZ',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          NonResident: { Individual: { AccountIdentifier: 'NON RESIDENT OTHER' } },
          MonetaryAmount: [{ CategoryCode: '250' }, { CategoryCode: '101' }]
        }),
        assertFailure("rbank5", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', ReceivingBank: 'XYZ',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          NonResident: { Individual: { AccountIdentifier: 'CASH' } },
          MonetaryAmount: [{ CategoryCode: '250' }, { CategoryCode: '101' }]
        }),
        // Linked rules: rbank5 and rbank7
        assertSuccess("rbank7", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', ReceivingBank: 'XYZ',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          NonResident: { Individual: { AccountIdentifier: 'CASH' } },
          MonetaryAmount: [{ CategoryCode: '250' }, { CategoryCode: '101' }]
        }),
        assertSuccess("rbank5", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', ReceivingBank: 'XYZ',
          Resident: { Exception: { ExceptionName: 'BULK INTEREST' } },
          MonetaryAmount: [{ CategoryCode: '250' }, { CategoryCode: '101' }]
        }),
        assertNoRule("rbank5", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', ReceivingBank: 'XYZ',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '101' }]
        }),

        assertSuccess("rbank6", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '255' }, { CategoryCode: '101' }]
        }),
        assertFailure("rbank6", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', ReceivingBank: 'XYZ',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '256' }, { CategoryCode: '101' }]
        }),
        assertSuccess("rbank6", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', ReceivingBank: 'XYZ',
          NonResident: { Exception: { ExceptionName: 'BULK INTEREST' } },
          MonetaryAmount: [{ CategoryCode: '255' }, { CategoryCode: '101' }]
        }),
        assertNoRule("rbank6", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', ReceivingBank: 'XYZ',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '252' }]
        }),

        // If Flow is OUT, must be completed except if the Resident AccountIdentifier is CASH, VISA NET or MASTER SEND (not on 250, 251, 255, 256)
        assertFailure("rbank7", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', MonetaryAmount: [{ CategoryCode: '401' }] }),
        assertSuccess("rbank7", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          ReceivingBank: 'XYZ',
          MonetaryAmount: [{ CategoryCode: '401' }]
        }),
        assertSuccess("rbank7", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          NonResident: { Individual: { AccountIdentifier: 'CASH' } },
          MonetaryAmount: [{ CategoryCode: '401' }]
        }),
        assertSuccess("rbank7", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          NonResident: { Entity: { AccountIdentifier: 'CASH' } },
          MonetaryAmount: [{ CategoryCode: '401' }]
        }),

        // If Flow is OUT, must be completed except if the Resident AccountIdentifier is CASH, VISA NET or MASTER SEND (exclude res exception "MUTUAL PARTY" on 250, 251)
        assertFailure("rbank8", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', MonetaryAmount: [{ CategoryCode: '250' }] }),
        assertSuccess("rbank8", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          ReceivingBank: 'XYZ',
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),
        assertSuccess("rbank8", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),
        assertSuccess("rbank8", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          NonResident: { Individual: { AccountIdentifier: 'CASH' } },
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),
        assertSuccess("rbank8", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          NonResident: { Entity: { AccountIdentifier: 'CASH' } },
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),

        // If Flow is OUT, must be completed except if the Resident AccountIdentifier is CASH, VISA NET or MASTER SEND (exclude non-res exception "MUTUAL PARTY" on 255, 256)
        assertFailure("rbank9", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', MonetaryAmount: [{ CategoryCode: '255' }] }),
        assertSuccess("rbank9", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          ReceivingBank: 'XYZ',
          MonetaryAmount: [{ CategoryCode: '255' }]
        }),
        assertSuccess("rbank9", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '255' }]
        }),
        assertSuccess("rbank9", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          NonResident: { Individual: { AccountIdentifier: 'CASH' } },
          MonetaryAmount: [{ CategoryCode: '255' }]
        }),
        assertSuccess("rbank9", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          NonResident: { Entity: { AccountIdentifier: 'CASH' } },
          MonetaryAmount: [{ CategoryCode: '255' }]
        }),


        // ReceivingCountry
        assertSuccess("rcntry1", { ReportingQualifier: 'BOPCUS', ReceivingCountry: 'ZA', ReceivingBank: 'XYZ' }),
        assertFailure("rcntry1", { ReportingQualifier: 'BOPCUS', ReceivingBank: 'XYZ' }),
        assertFailure("rcntry2", { ReportingQualifier: 'BOPCARD RESIDENT', ReceivingCountry: 'ZA' }),
        assertSuccess("rcntry2", { ReportingQualifier: 'BOPCARD RESIDENT' }),
        assertSuccess("rcntry3", { ReportingQualifier: 'INTERBANK', ReceivingCountry: 'US' }),
        assertFailure("rcntry3", { ReportingQualifier: 'INTERBANK', ReceivingCountry: 'ZZ' }),
        assertSuccess("rcntry3", { ReportingQualifier: 'BOPCUS', ReceivingCountry: 'US' }),
        assertFailure("rcntry3", { ReportingQualifier: 'BOPCUS', ReceivingCountry: 'ZZ' }),
        assertFailure("rcntry4", { ReportingQualifier: 'BOPCUS', Flow: 'IN', ReceivingCountry: 'US' }),
        assertSuccess("rcntry4", { ReportingQualifier: 'BOPCUS', Flow: 'IN', ReceivingCountry: 'ZA' }),
        assertSuccess("rcntry5", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', ReceivingCountry: 'US' }),
        assertFailure("rcntry5", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', ReceivingCountry: 'ZA' }),
        assertFailure("rcntry6", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '200' }],
          ReceivingCountry: 'US'
        }),
        assertSuccess("rcntry6", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          Resident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '200' }],
          ReceivingCountry: ''
        }),
        assertFailure("rcntry7", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '255' }],
          ReceivingCountry: 'US'
        }),
        assertSuccess("rcntry7", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          NonResident: { Exception: { ExceptionName: 'MUTUAL PARTY' } },
          MonetaryAmount: [{ CategoryCode: '255' }],
          ReceivingCountry: ''
        }),

        // NonResident
        assertSuccess("nr1", { ReportingQualifier: 'BOPCUS', NonResident: { Exception: {} } }),
        assertFailure("nr1", { ReportingQualifier: 'BOPCUS' }),
        assertSuccess("nr2", { ReportingQualifier: 'NON RESIDENT RAND', NonResident: { Individual: {} } }),
        assertFailure("nr2", { ReportingQualifier: 'NON RESIDENT RAND', NonResident: { Exception: {} } }),
        assertSuccess("nr2", { ReportingQualifier: 'BOPCARD RESIDENT', NonResident: { Individual: {} } }),
        assertSuccess("nr2", { ReportingQualifier: 'BOPCARD RESIDENT', NonResident: { Entity: {} } }),
        assertFailure("nr2", { ReportingQualifier: 'BOPCARD RESIDENT', NonResident: { Exception: {} } }),
        assertSuccess("nr3", {
          ReportingQualifier: 'BOPDIR', NonResident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '304' }]
        }),
        assertFailure("nr3", {
          ReportingQualifier: 'BOPDIR', NonResident: { Exception: {} },
          MonetaryAmount: [{ CategoryCode: '304' }]
        }),
        assertNoRule("nr3", {
          ReportingQualifier: 'BOPDIR', NonResident: { Exception: {} },
          MonetaryAmount: [{ CategoryCode: '101' }]
        }),
        //Non Resident Entity element must be completed (unless VISA NET or MASTER SEND)
        assertSuccess("nr4", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          NonResident: { Entity: {} }
        }),
        assertSuccess("nr4", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          NonResident: { Individual: { AccountIdentifier: "VISA NET" } }
        }),
        assertFailure("nr4", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          NonResident: { Exception: {} }
        }),
        assertFailure("nr4", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          NonResident: { Individual: {} }
        }),
        assertSuccess("nr5", { ReportingQualifier: 'BOPCARD NON RESIDENT' }),
        assertFailure("nr5", { ReportingQualifier: 'BOPCARD NON RESIDENT', NonResident: { Individual: {} } }),
        assertSuccess("nr6", {
          ReportingQualifier: 'BOPCUS', NonResident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),
        assertFailure("nr6", {
          ReportingQualifier: 'BOPCUS', NonResident: { Exception: {} },
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),
        assertFailure("nr6", {
          ReportingQualifier: 'BOPCUS', NonResident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '251' }]
        }),

        // NonResident.Individual.Surname
        assertSuccess("nrsn1", { ReportingQualifier: 'BOPCUS', NonResident: { Individual: { Surname: 'Doe' } } }),
        assertFailure("nrsn1", { ReportingQualifier: 'BOPCUS', NonResident: { Individual: {} } }),
        assertSuccess("nrsn1", { ReportingQualifier: 'BOPCARD RESIDENT', NonResident: { Individual: { Surname: 'Doe' } } }),
        assertFailure("nrsn1", { ReportingQualifier: 'BOPCARD RESIDENT', NonResident: { Individual: {} } }),
        assertSuccess("nrsn2", { ReportingQualifier: 'BOPCUS', NonResident: { Individual: { Surname: 'Doe' } } }),
        assertSuccess("nrsn2", { ReportingQualifier: 'BOPCUS', NonResident: { Individual: { Surname: 'STRATE' } } }),
        assertFailure("nrsn2", { ReportingQualifier: 'BOPCUS', NonResident: { Individual: { Surname: 'MUTUAL PARTY' } } }),
        // If the Flow is OUT and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then the Surname must have a value
        assertSuccess("nrsn3", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          NonResident: { Individual: { Surname: 'Doe' } }
        }),
        assertSuccess("nrsn3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', NonResident: { Individual: { Surname: 'Doe' } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertFailure("nrsn3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', NonResident: { Individual: { Surname: '' } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        // If the Flow is IN and the category is 400 and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then the Surname must have a value
        assertSuccess("nrsn4", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          NonResident: { Individual: { Surname: 'Doe' } },
          MonetaryAmount: [{ CategoryCode: '400' }]
        }),
        assertSuccess("nrsn4", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', NonResident: { Individual: { Surname: 'Doe' } },
          MonetaryAmount: [{ CategoryCode: '400', AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertFailure("nrsn4", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', NonResident: { Individual: { Surname: '' } },
          MonetaryAmount: [{ CategoryCode: '400', AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertNoRule("nrsn4", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', NonResident: { Individual: { Surname: '' } },
          MonetaryAmount: [{ CategoryCode: '100', AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // NonResident.Individual.Name
        assertSuccess("nrnm1", { ReportingQualifier: 'BOPCUS', NonResident: { Individual: { Name: 'John' } } }),
        assertFailure("nrnm1", { ReportingQualifier: 'BOPCUS', NonResident: { Individual: {} } }),
        assertSuccess("nrnm2", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', NonResident: { Individual: { Name: 'John' } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertFailure("nrnm2", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', NonResident: { Individual: {} },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // NonResident.Individual.Name: If the Flow is IN and the category is 400 and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then the Name must have a value
        assertSuccess("nrnm3", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', NonResident: { Individual: { Name: 'John' } },
          MonetaryAmount: [{ CategoryCode: '400', AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertFailure("nrnm3", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', NonResident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: '400', AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertNoRule("nrnm3", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', NonResident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: '100', AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        //The words specified under Non-resident or Resident ExceptionName, excluding STRATE, must not be used.
        assertSuccess("nrnm4", {
          ReportingQualifier: 'BOPCUS', NonResident: {
            Individual: { name: 'STRATE' }
          }
        }),

        // NonResident.Individual.Gender
        assertSuccess("nrgn1", { ReportingQualifier: 'BOPCUS', NonResident: { Individual: { Gender: 'M' } } }),
        assertFailure("nrgn1", { ReportingQualifier: 'BOPCUS', NonResident: { Individual: { Gender: 'X' } } }),

        // NonResident.Individual.PassportNumber
        assertSuccess("nrpn1", {
          ReportingQualifier: 'BOPCUS', NonResident: { Individual: { PassportNumber: '12345' } },
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),
        assertFailure("nrpn1", {
          ReportingQualifier: 'BOPCUS', NonResident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),
        // Additional spaces identified in data content
        assertSuccess("nrpn2", {
          ReportingQualifier: 'BOPCUS', NonResident: { Individual: { PassportNumber: '12345' } }
        }),
        assertFailure("nrpn2", {
          ReportingQualifier: 'BOPCUS', NonResident: { Individual: { PassportNumber: '12345 ' } }
        }),

        // NonResident.Individual.PassportCountry
        assertSuccess("nrpc1", {
          ReportingQualifier: 'BOPCUS', NonResident: { Individual: { PassportCountry: 'ZA' } },
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),
        assertFailure("nrpc1", {
          ReportingQualifier: 'BOPCUS', NonResident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: '250' }]
        }),
        assertSuccess("nrpc2", { ReportingQualifier: 'BOPCUS', NonResident: { Individual: { PassportCountry: 'ZA' } } }),
        assertFailure("nrpc2", { ReportingQualifier: 'BOPCUS', NonResident: { Individual: { PassportCountry: 'ZZ' } } }),

        //NonResident.Entity.EntityName
        assertSuccess("nrlen1", { ReportingQualifier: "BOPCUS", NonResident: { Entity: { EntityName: 'Synthesis' } } }),
        assertNoRule("nrlen1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Entity: { EntityName: 'Synthesis' } }
        }),
        assertFailure("nrlen1", { ReportingQualifier: "BOPCUS", NonResident: { Entity: { EntityName: "" } } }),
        assertFailure("nrlen1", { ReportingQualifier: "BOPCUS", NonResident: { Entity: {} } }),
        //NonResident.Entity.EntityName: Must not be completed (other than for VISA NET or MASTER SEND)
        assertSuccess("nrlen2", { ReportingQualifier: "BOPCARD RESIDENT", NonResident: { Entity: { EntityName: "" } } }),
        assertNoRule("nrlen2", { ReportingQualifier: "BOPCUS", NonResident: { Entity: { EntityName: "" } } }),
        assertSuccess("nrlen2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { EntityName: "Synthesis", AccountIdentifier: "VISA NET" } }
        }),
        assertFailure("nrlen2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { EntityName: "Synthesis" } }
        }),
        assertSuccess("nrlen3", { ReportingQualifier: "BOPCUS", NonResident: {} }),
        assertSuccess("nrlen3", { ReportingQualifier: "BOPCUS", NonResident: { Entity: { EntityName: "STRATE" } } }),
        assertFailure("nrlen3", { ReportingQualifier: "BOPCUS", NonResident: { Entity: { EntityName: "MUTUAL PARTY" } } }),
        assertSuccess("nrlen4", {
          ReportingQualifier: "BOPCUS", NonResident: { Entity: { EntityName: "" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertFailure("nrlen4", {
          ReportingQualifier: "BOPCUS", NonResident: { Entity: { EntityName: "STUFF AND NONSENSE" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        //NonResident.Entity.EntityName: This field or CardMerchantName must be completed (Only for VISA NET or MASTER SEND)
        assertSuccess("nrlen5", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { EntityName: "Synthesis", CardMerchantName: "", AccountIdentifier: "VISA NET" } }
        }),
        assertSuccess("nrlen5", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { EntityName: "", CardMerchantName: "Merchant", AccountIdentifier: "VISA NET" } }
        }),
        assertFailure("nrlen5", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { EntityName: "", CardMerchantName: "", AccountIdentifier: "VISA NET" } }
        }),
        assertFailure("nrlen5", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { EntityName: "", CardMerchantName: "", AccountIdentifier: "VISA NET" } }
        }),

        //NonResident.Entity.CardMerchantName
        //Must be completed (other than for VISA NET or MASTER SEND)
        assertSuccess("nrcmn1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { CardMerchantName: "My Card" } }
        }),
        assertSuccess("nrcmn1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { CardMerchantName: "My Card", AccountIdentifier: "VISA NET" } }
        }),
        assertSuccess("nrcmn1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { CardMerchantName: "", AccountIdentifier: "VISA NET" } }
        }),
        assertFailure("nrcmn1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { CardMerchantName: "" } }
        }),
        assertSuccess("nrcmn2", { ReportingQualifier: "BOPCUS", NonResident: { Entity: { CardMerchantName: "" } } }),
        assertFailure("nrcmn2", { ReportingQualifier: "BOPCUS", NonResident: { Entity: { CardMerchantName: "Bla" } } }),
        assertNoRule("nrcmn2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { CardMerchantName: "" } }
        }),
        assertSuccess("nrcmn3", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { CardMerchantName: "Bla" } }
        }),
        assertFailure("nrcmn3", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { CardMerchantName: "Bla " } }
        }),
        //This field or EntityName must be completed
        assertSuccess("nrcmn4", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { EntityName: "Synthesis", CardMerchantName: "", AccountIdentifier: "VISA NET" } }
        }),
        assertSuccess("nrcmn4", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { EntityName: "", CardMerchantName: "Merchant", AccountIdentifier: "VISA NET" } }
        }),
        assertFailure("nrcmn4", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { EntityName: "", CardMerchantName: "", AccountIdentifier: "VISA NET" } }
        }),
        assertSuccess("nrcmn4", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { EntityName: "", CardMerchantName: "", AccountIdentifier: "" } }
        }),

        //NonResident.Entity.CardMerchantCode
        //Must be completed (other than for VISA NET or MASTER SEND)
        assertSuccess("nrcmc1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { CardMerchantCode: "slkdjf" } }
        }),
        assertFailure("nrcmc1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { CardMerchantCode: "", AccountIdentifier: "MASTER SEND" } }
        }),
        assertFailure("nrcmc1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { CardMerchantCode: "" } }
        }),
        assertSuccess("nrcmc2", { ReportingQualifier: "BOPCUS", NonResident: { Entity: { CardMerchantCode: "" } } }),
        assertFailure("nrcmc2", { ReportingQualifier: "BOPCUS", NonResident: { Entity: { CardMerchantCode: "dgs" } } }),
        assertNoRule("nrcmc2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { CardMerchantCode: "" } }
        }),

        //NonResident.Exception
        assertSuccess("nrex1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        assertFailure("nrex1", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        assertSuccess("nrex2", { ReportingQualifier: "NON RESIDENT RAND", NonResident: { Individual: {} } }),
        assertNoRule("nrex2", { ReportingQualifier: "BOPCUS", NonResident: { Individual: {} } }),
        assertFailure("nrex2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),

        //NonResident.Exception.ExceptionName
        assertSuccess("nrexn1", {
          ReportingQualifier: "INTERBANK",
          NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        assertFailure("nrexn1", { ReportingQualifier: "INTERBANK", NonResident: { Exception: { ExceptionName: "JFJF" } } }),
        assertFailure("nrexn1", { ReportingQualifier: "INTERBANK", NonResident: { Exception: {} } }),
        assertNoRule("nrexn2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "252" }]
        }),
        assertFailure("nrexn2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertSuccess("nrexn3", {
          ReportingQualifier: "BOPCUS", Flow: "IN", NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "252" }]
        }),
        assertFailure("nrexn3", {
          ReportingQualifier: "BOPCUS", Flow: "IN", NonResident: { Exception: { ExceptionName: "IHQ" } },
          MonetaryAmount: [{ CategoryCode: "252" }]
        }),
        assertNoRule("nrexn4", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "BULK INTEREST" } },
          MonetaryAmount: [{ CategoryCode: "309", CategorySubCode: "08" }]
        }),
        assertNoRule("nrexn4", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "BULK INTEREST" } },
          MonetaryAmount: [{ CategoryCode: "300" }]
        }),
        assertSuccess("nrexn4", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "252" }]
        }),
        assertFailure("nrexn4", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "BULK INTEREST" } },
          MonetaryAmount: [{ CategoryCode: "252" }]
        }),
        assertNoRule("nrexn5", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "BULK VAT REFUNDS" } },
          MonetaryAmount: [{ CategoryCode: "411", CategorySubCode: "02" }]
        }),
        assertNoRule("nrexn5", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "BULK VAT REFUNDS" } },
          MonetaryAmount: [{ CategoryCode: "400" }]
        }),
        assertSuccess("nrexn5", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "252" }]
        }),
        assertFailure("nrexn5", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "BULK VAT REFUNDS" } },
          MonetaryAmount: [{ CategoryCode: "252" }]
        }),
        //ExceptionName of BULK BANK CHARGES may only be used for category 200 or 275
        assertNoRule("nrexn6", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "BULK BANK CHARGES" } },
          MonetaryAmount: [{ CategoryCode: "275" }]
        }),
        assertNoRule("nrexn6", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "BULK BANK CHARGES" } },
          MonetaryAmount: [{ CategoryCode: "200" }]
        }),
        assertSuccess("nrexn6", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "252" }]
        }),
        assertFailure("nrexn6", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "BULK BANK CHARGES" } },
          MonetaryAmount: [{ CategoryCode: "252" }]
        }),
        //ExceptionName of BULK PENSIONS may only be used for category 400 or 407
        assertNoRule("nrexn7", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "BULK PENSIONS" } },
          MonetaryAmount: [{ CategoryCode: "407" }]
        }),
        assertNoRule("nrexn7", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "BULK PENSIONS" } },
          MonetaryAmount: [{ CategoryCode: "400" }]
        }),
        assertSuccess("nrexn7", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "252" }]
        }),
        assertFailure("nrexn7", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "BULK PENSIONS" } },
          MonetaryAmount: [{ CategoryCode: "252" }]
        }),
        //ExceptionName of STRATE may only be used for category 601/01 or 603/01
        assertNoRule("nrexn9", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "601", CategorySubCode: "01" }]
        }),
        assertNoRule("nrexn9", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "603", CategorySubCode: "01" }]
        }),
        assertSuccess("nrexn9", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "252" }]
        }),
        assertFailure("nrexn9", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "252" }]
        }),
        //May not be used. MUTUAL PARTY is only applicable for BOPCUS transactions.
        assertNoRule("nrexn10", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        assertSuccess("nrexn10", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertFailure("nrexn10", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        //May not be used. BULK INTEREST is only applicable for BOPCUS transactions.
        assertNoRule("nrexn11", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "BULK INTEREST" } }
        }),
        assertSuccess("nrexn11", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertFailure("nrexn11", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "BULK INTEREST" } }
        }),
        //May not be used. BULK VAT REFUNDS is only applicable for BOPCUS transactions.
        assertNoRule("nrexn12", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "BULK VAT REFUNDS" } }
        }),
        assertSuccess("nrexn12", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertFailure("nrexn12", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "BULK VAT REFUNDS" } }
        }),
        //May not be used. BULK BANK CHARGES is only applicable for BOPCUS transactions.
        assertNoRule("nrexn13", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "BULK BANK CHARGES" } }
        }),
        assertSuccess("nrexn13", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertFailure("nrexn13", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "BULK BANK CHARGES" } }
        }),
        //May not be used. BULK PENSIONS is only applicable for BOPCUS transactions.
        assertNoRule("nrexn14", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "BULK PENSIONS" } }
        }),
        assertSuccess("nrexn14", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertFailure("nrexn14", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "BULK PENSIONS" } }
        }),
        //May not be used. STRATE is only applicable for BOPCUS transactions.
        assertNoRule("nrexn15", { ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "STRATE" } } }),
        assertSuccess("nrexn15", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertFailure("nrexn15", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "STRATE" } }
        }),
        //May not be used. FCA RESIDENT NON REPORTABLE is only applicable for NON REPORTABLE transactions.
        assertNoRule("nrexn16", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "FCA RESIDENT NON REPORTABLE" } }
        }),
        assertSuccess("nrexn16", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        assertFailure("nrexn16", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "FCA RESIDENT NON REPORTABLE" } }
        }),
        //May not be used. CFC RESIDENT NON REPORTABLE is only applicable for NON REPORTABLE transactions.
        assertNoRule("nrexn17", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "CFC RESIDENT NON REPORTABLE" } }
        }),
        assertSuccess("nrexn17", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        assertFailure("nrexn17", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "CFC RESIDENT NON REPORTABLE" } }
        }),
        //May not be used. VOSTRO NON REPORTABLE is only applicable for NON REPORTABLE transactions.
        assertNoRule("nrexn18", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertSuccess("nrexn18", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        assertFailure("nrexn18", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        //May not be used. VOSTRO INTERBANK is only applicable for INTERBANK and NON REPORTABLE transactions
        assertNoRule("nrexn19", {
          ReportingQualifier: "INTERBANK",
          NonResident: { Exception: { ExceptionName: "VOSTRO INTERBANK" } }
        }),
        assertNoRule("nrexn19", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "VOSTRO INTERBANK" } }
        }),
        assertSuccess("nrexn19", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertFailure("nrexn19", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "VOSTRO INTERBANK" } }
        }),
        //May not be used. NOSTRO INTERBANK is only applicable for INTERBANK and NON REPORTABLE transactions
        assertNoRule("nrexn20", {
          ReportingQualifier: "INTERBANK",
          NonResident: { Exception: { ExceptionName: "NOSTRO INTERBANK" } }
        }),
        assertNoRule("nrexn20", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "NOSTRO INTERBANK" } }
        }),
        assertSuccess("nrexn20", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertFailure("nrexn20", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "NOSTRO INTERBANK" } }
        }),
        //May not be used. NOSTRO NON REPORTABLE is only applicable for NON REPORTABLE transactions.
        assertNoRule("nrexn21", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "NOSTRO NON REPORTABLE" } }
        }),
        assertSuccess("nrexn21", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        assertFailure("nrexn21", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "NOSTRO NON REPORTABLE" } }
        }),
        //May not be used. RTGS NON REPORTABLE is only applicable for NON REPORTABLE transactions.
        assertNoRule("nrexn22", {
          ReportingQualifier: "NON REPORTABLE",
          NonResident: { Exception: { ExceptionName: "RTGS NON REPORTABLE" } }
        }),
        assertSuccess("nrexn22", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        assertFailure("nrexn22", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "RTGS NON REPORTABLE" } }
        }),
        assertFailure("nrexn23", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "256", AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess("nrexn23", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "" } },
          MonetaryAmount: [{ CategoryCode: "256", AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertNoRule("nrexn23", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "401", AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertFailure("nrexn24", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "IHQ" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'IHQxxx' } }]
        }),
        assertSuccess("nrexn24", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "IHQ" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'IHQ123' } }]
        }),
        assertNoRule("nrexn24", {
          ReportingQualifier: "BOPDIR",
          NonResident: { Exception: { ExceptionName: "IHQ" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'IHQ123' } }]
        }),
        assertFailure("nrexn25", {
          ReportingQualifier: "BOPDIR",
          NonResident: { Exception: { ExceptionName: "IHQ" } }
        }),
        assertNoRule("nrexn25", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Exception: { ExceptionName: "IHQ" } }
        }),
        // If the MoneyTransferAgentIndicator is TRAVEL CARD or TRAVELLERS CHEQUE, the category can only be 252, 255, 256 or 530/05
        assertNoRule("nrexn26", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "TRAVEL CARD", CategoryCode: "252" }]
        }),
        assertSuccess("nrexn26", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "AD", CategoryCode: "401" }]
        }),
        assertSuccess("nrexn26", {
          ReportingQualifier: "BOPCUS", NonResident: { Individual: {} },
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "TRAVEL CARD", CategoryCode: "401" }]
        }),
        assertFailure("nrexn26", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "TRAVEL CARD", CategoryCode: "401" }]
        }),
        assertFailure("nrexn26", {
          ReportingQualifier: "BOPCUS", NonResident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "TRAVELLERS CHEQUE", CategoryCode: "401" }]
        }),

        //NonResident.Individual.AccountIdentifier
        assertSuccess("nriaid1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { AccountIdentifier: "CASH" } }
        }),
        assertSuccess("nriaid1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } }
        }),
        assertNoRule("nriaid1:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { AccountIdentifier: "CASH" } }
        }),
        assertNoRule("nriaid1:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { AccountIdentifier: "CASH" } }
        }),
        assertFailure("nriaid1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { AccountIdentifier: "CSH" } }
        }),
        assertSuccess("nriaid2:1", {
          ReportingQualifier: "NON RESIDENT RAND",
          NonResident: { Individual: { AccountIdentifier: "NON RESIDENT RAND" } }
        }),
        assertSuccess("nriaid2:1", {
          ReportingQualifier: "NON RESIDENT RAND",
          NonResident: { Individual: { AccountIdentifier: "VISA NET" } }
        }),
        assertSuccess("nriaid2:1", {
          ReportingQualifier: "NON RESIDENT RAND",
          NonResident: { Individual: { AccountIdentifier: "MASTER SEND" } }
        }),
        assertFailure("nriaid2:1", {
          ReportingQualifier: "NON RESIDENT RAND",
          NonResident: { Individual: { AccountIdentifier: "CASH" } }
        }),
        //The below rule is scheduled for future implementation so should not yet be active.
        assertSuccess("nriaid3:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { AccountIdentifier: "VISA NET" } }
        }),
        assertSuccess("nriaid3:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { AccountIdentifier: "MASTER SEND" } }
        }),
        assertFailure("nriaid3:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { AccountIdentifier: "CASH" } }
        }),
        assertSuccess("nriaid3:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { AccountIdentifier: "" } }
        }),

        assertSuccess("nriaid4:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT" } }
        }),
        assertNoRule("nriaid4:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT" } }
        }),
        assertFailure("nriaid4:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: {
            Individual: { AccountIdentifier: "FCA RESIDENT" },
            Exception: { ExceptionName: "lskdf" }
          }
        }),
        assertSuccess("nriaid4:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { AccountIdentifier: "NON RESIDENT OTHER" } }
        }),
        assertNoRule("nriaid4:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { AccountIdentifier: "NON RESIDENT OTHER" } }
        }),
        assertFailure("nriaid4:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: {
            Individual: { AccountIdentifier: "NON RESIDENT OTHER" },
            Exception: { ExceptionName: "lskdf" }
          }
        }),
        assertSuccess("nriaid4:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { AccountIdentifier: "NON RESIDENT RAND" } }
        }),
        assertNoRule("nriaid4:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { AccountIdentifier: "NON RESIDENT RAND" } }
        }),
        assertFailure("nriaid4:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: {
            Individual: { AccountIdentifier: "NON RESIDENT RAND" },
            Exception: { ExceptionName: "lskdf" }
          }
        }),
        assertSuccess("nriaid4:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { AccountIdentifier: "NON RESIDENT FCA" } }
        }),
        assertNoRule("nriaid4:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { AccountIdentifier: "NON RESIDENT FCA" } }
        }),
        assertFailure("nriaid4:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: {
            Individual: { AccountIdentifier: "NON RESIDENT FCA" },
            Exception: { ExceptionName: "lskdf" }
          }
        }),
        assertSuccess("nriaid4:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } }
        }),
        assertNoRule("nriaid4:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } }
        }),
        assertFailure("nriaid4:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: {
            Individual: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" },
            Exception: { ExceptionName: "lskdf" }
          }
        }),

        assertSuccess("nriaid5:1", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          NonResident: { Individual: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "255" }]
        }),
        assertSuccess("nriaid5:1", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          NonResident: { Individual: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "256" }]
        }),
        assertSuccess("nriaid5:1", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          NonResident: { Individual: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "810" }]
        }),
        assertSuccess("nriaid5:1", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          NonResident: { Individual: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "416" }]
        }),


        assertSuccess("nriaid6:1", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          NonResident: { Individual: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "255" }]
        }),
        assertNoRule("nriaid6:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Flow: "OUT",
          NonResident: { Individual: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "255" }]
        }),
        assertSuccess("nriaid6:1", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          NonResident: { Individual: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "256" }]
        }),
        assertNoRule("nriaid6:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Flow: "OUT",
          NonResident: { Individual: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "256" }]
        }),
        assertSuccess("nriaid6:1", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          NonResident: { Individual: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "810" }]
        }),
        assertNoRule("nriaid6:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Flow: "OUT",
          NonResident: { Individual: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "810" }]
        }),

        //If the Flow is OUT must contain a value of NON RESIDENT OTHER, NON RESIDENT RAND, NON RESIDENT FCA, CASH, FCA RESIDENT, RES FOREIGN BANK ACCOUNT or VOSTRO
        /* Rule ignored because it seems to be a duplicate of nriaid1
         assertSuccess("nriaid7:1", {
         ReportingQualifier: "BOPCUS",
         Flow              : "OUT",
         NonResident       : {Individual: {AccountIdentifier: "CASH"}}
         }),
         assertSuccess("nriaid7:1", {
         ReportingQualifier: "BOPCUS",
         Flow              : "OUT",
         NonResident       : {Individual: {AccountIdentifier: "VOSTRO"}}
         }),
         assertFailure("nriaid7:1", {
         ReportingQualifier: "BOPCUS",
         Flow              : "OUT",
         NonResident       : {Individual: {AccountIdentifier: "CARD DIRECT"}}
         }),
         assertNoRule("nriaid7:1", {
         ReportingQualifier: "BOPCARD RESIDENT",
         Flow              : "OUT",
         NonResident       : {Individual: {AccountIdentifier: "CARD DIRECT"}}
         }),*/

        //If AccountIdentifier is FCA RESIDENT and the Flow is OUT the category 513 must be completed
        assertNoRule("nriaid9:1", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT" } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        assertFailure("nriaid9:1", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertNoRule("nriaid9:1", {
          ReportingQualifier: "BOPCUS", Flow: "IN", NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertSuccess("nriaid9:1", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident: { Individual: { AccountIdentifier: "CASH" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),

        //If AccountIdentifier is FCA RESIDENT and the Flow is IN the category 517 must be completed
        assertNoRule("nriaid10:1", {
          ReportingQualifier: "BOPCUS", Flow: "IN", NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT" } },
          MonetaryAmount: [{ CategoryCode: "517" }]
        }),
        assertFailure("nriaid10:1", {
          ReportingQualifier: "BOPCUS", Flow: "IN", NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertNoRule("nriaid10:1", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertSuccess("nriaid10:1", {
          ReportingQualifier: "BOPCUS", Flow: "IN", NonResident: { Individual: { AccountIdentifier: "CASH" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),

        //If the AccountIdentifier is NON RESIDENT OTHER or CASH and the category is 250 or 251, the Resident Individual element must be completed
        //assertSuccess("nriaid11:1", {ReportingQualifier: "BOPCUS", NonResident: {Individual: {AccountIdentifier: "CASH"}}, Resident: {Individual: {}},
        //    MonetaryAmount: [{CategoryCode: "251"}]}),
        //assertSuccess("nriaid11:1", {ReportingQualifier: "BOPCUS", NonResident: {Individual: {AccountIdentifier: "NON RESIDENT OTHER"}}, Resident: {Individual: {}},
        //    MonetaryAmount: [{CategoryCode: "251"}]}),
        //assertSuccess("nriaid11:1", {ReportingQualifier: "BOPCUS", NonResident: {Individual: {AccountIdentifier: "OTHER"}}, Resident: {Entity: {}},
        //    MonetaryAmount: [{CategoryCode: "251"}]}),
        //assertNoRule("nriaid11:1", {ReportingQualifier: "BOPCUS", NonResident: {Individual: {AccountIdentifier: "CASH"}}, Resident: {Entity: {}},
        //    MonetaryAmount: [{CategoryCode: "101"}]}),
        //assertFailure("nriaid11:1", {ReportingQualifier: "BOPCUS", NonResident: {Individual: {AccountIdentifier: "CASH"}}, Resident: {Entity: {}},
        //    MonetaryAmount: [{CategoryCode: "251"}]}),

        //NonResident.Entity.AccountIdentifier
        assertSuccess("nriaid1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { AccountIdentifier: "CASH" } }
        }),
        assertSuccess("nriaid1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } }
        }),
        assertNoRule("nriaid1:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { AccountIdentifier: "CASH" } }
        }),
        assertFailure("nriaid1:2", { ReportingQualifier: "BOPCUS", NonResident: { Entity: { AccountIdentifier: "CSH" } } }),
        assertSuccess("nriaid2:2", {
          ReportingQualifier: "NON RESIDENT RAND",
          NonResident: { Entity: { AccountIdentifier: "NON RESIDENT RAND" } }
        }),
        assertFailure("nriaid2:2", {
          ReportingQualifier: "NON RESIDENT RAND",
          NonResident: { Entity: { AccountIdentifier: "CASH" } }
        }),
        //The below rule is scheduled for future implementation so should not yet be active.
        assertSuccess("nriaid3:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { AccountIdentifier: "VISA NET" } }
        }),
        assertSuccess("nriaid3:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { AccountIdentifier: "MASTER SEND" } }
        }),
        assertFailure("nriaid3:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { AccountIdentifier: "CASH" } }
        }),

        assertSuccess("nriaid4:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { AccountIdentifier: "FCA RESIDENT" } }
        }),
        assertNoRule("nriaid4:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { AccountIdentifier: "FCA RESIDENT" } }
        }),
        assertFailure("nriaid4:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: {
            Entity: { AccountIdentifier: "FCA RESIDENT" },
            Exception: { ExceptionName: "lskdf" }
          }
        }),
        assertSuccess("nriaid4:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { AccountIdentifier: "NON RESIDENT OTHER" } }
        }),
        assertNoRule("nriaid4:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { AccountIdentifier: "NON RESIDENT OTHER" } }
        }),
        assertFailure("nriaid4:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: {
            Entity: { AccountIdentifier: "NON RESIDENT OTHER" },
            Exception: { ExceptionName: "lskdf" }
          }
        }),
        assertSuccess("nriaid4:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { AccountIdentifier: "NON RESIDENT RAND" } }
        }),
        assertNoRule("nriaid4:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { AccountIdentifier: "NON RESIDENT RAND" } }
        }),
        assertFailure("nriaid4:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: {
            Entity: { AccountIdentifier: "NON RESIDENT RAND" },
            Exception: { ExceptionName: "lskdf" }
          }
        }),
        assertSuccess("nriaid4:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { AccountIdentifier: "NON RESIDENT FCA" } }
        }),
        assertNoRule("nriaid4:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { AccountIdentifier: "NON RESIDENT FCA" } }
        }),
        assertFailure("nriaid4:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: {
            Entity: { AccountIdentifier: "NON RESIDENT FCA" },
            Exception: { ExceptionName: "lskdf" }
          }
        }),
        assertSuccess("nriaid4:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } }
        }),
        assertNoRule("nriaid4:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } }
        }),
        assertFailure("nriaid4:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: {
            Entity: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" },
            Exception: { ExceptionName: "lskdf" }
          }
        }),

        assertFailure("nriaid5:2", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          NonResident: { Entity: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "255" }]
        }),
        assertFailure("nriaid5:2", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          NonResident: { Entity: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "256" }]
        }),
        assertFailure("nriaid5:2", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          NonResident: { Entity: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "810" }]
        }),
        assertFailure("nriaid5:2", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          NonResident: { Entity: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "416" }]
        }),

        assertFailure("nriaid6:2", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          NonResident: { Entity: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "255" }]
        }),
        assertNoRule("nriaid6:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Flow: "OUT",
          NonResident: { Entity: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "255" }]
        }),
        assertFailure("nriaid6:2", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          NonResident: { Entity: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "256" }]
        }),
        assertNoRule("nriaid6:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Flow: "OUT",
          NonResident: { Entity: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "256" }]
        }),
        assertFailure("nriaid6:2", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          NonResident: { Entity: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "810" }]
        }),
        assertNoRule("nriaid6:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Flow: "OUT",
          NonResident: { Entity: { AccountIdentifier: "RES FOREIGN BANK ACCOUNT" } },
          MonetaryAmount: [{ CategoryCode: "810" }]
        }),

        //If AccountIdentifier is FCA RESIDENT and the Flow is OUT the category 513 must be completed
        assertNoRule("nriaid9:2", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident: { Entity: { AccountIdentifier: "FCA RESIDENT" } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        assertFailure("nriaid9:2", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident: { Entity: { AccountIdentifier: "FCA RESIDENT" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertNoRule("nriaid9:2", {
          ReportingQualifier: "BOPCUS", Flow: "IN", NonResident: { Entity: { AccountIdentifier: "FCA RESIDENT" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertSuccess("nriaid9:2", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident: { Entity: { AccountIdentifier: "CASH" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),

        //If AccountIdentifier is FCA RESIDENT and the Flow is IN the category 517 must be completed
        assertNoRule("nriaid10:2", {
          ReportingQualifier: "BOPCUS", Flow: "IN", NonResident: { Entity: { AccountIdentifier: "FCA RESIDENT" } },
          MonetaryAmount: [{ CategoryCode: "517" }]
        }),
        assertFailure("nriaid10:2", {
          ReportingQualifier: "BOPCUS", Flow: "IN", NonResident: { Entity: { AccountIdentifier: "FCA RESIDENT" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertNoRule("nriaid10:2", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident: { Entity: { AccountIdentifier: "FCA RESIDENT" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertSuccess("nriaid10:2", {
          ReportingQualifier: "BOPCUS", Flow: "IN", NonResident: { Entity: { AccountIdentifier: "CASH" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),

        // warning("nriaid12", 272, "The value CARD DIRECT is being deprecated (VISA NET and MASTER SEND to be implemented by 2016-05-15)", hasValue("CARD DIRECT")).onSection("E")
        assertNoRule("nriaid7", {}),

        assertWarning("nriaid12:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { AccountIdentifier: "CARD DIRECT" } },
          MonetaryAmount: [{ CategoryCode: "272" }]
        }),

        assertSuccess("nriaid13:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: { Individual: { AccountIdentifier: "" } }
        }),
        assertSuccess("nriaid13:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: { Entity: { AccountIdentifier: "" } }
        }),

        assertFailure("nriaid13:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: { Individual: { AccountIdentifier: "ABC" } }
        }),
        assertFailure("nriaid13:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: { Entity: { AccountIdentifier: "ABC" } }
        }),

        assertSuccess("nriaid14:1", {
          Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: "513" }],
          ReportingQualifier: "BOPCUS", NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT" } }
        }),
        assertFailure("nriaid14:2", {
          Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: "513" }],
          ReportingQualifier: "BOPCUS", NonResident: { Entity: { AccountIdentifier: "FCA RESIDENT" } }
        }),
        assertSuccess("nriaid14:1", {
          Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: "513" }],
          ReportingQualifier: "BOPCUS", NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT" } }
        }),
        assertFailure("nriaid14:2", {
          Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: "513" }],
          ReportingQualifier: "BOPCUS", NonResident: { Entity: { AccountIdentifier: "FCA RESIDENT" } }
        }),



        //If the AccountIdentifier is NON RESIDENT OTHER or CASH and the category is 250 or 251, the Resident Individual element must be completed
        //assertSuccess("nriaid11:2", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {AccountIdentifier: "CASH"}}, Resident: {Individual: {}},
        //    MonetaryAmount: [{CategoryCode: "251"}]}),
        //assertSuccess("nriaid11:2", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {AccountIdentifier: "NON RESIDENT OTHER"}}, Resident: {Individual: {}},
        //    MonetaryAmount: [{CategoryCode: "251"}]}),
        //assertSuccess("nriaid11:2", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {AccountIdentifier: "OTHER"}}, Resident: {Entity: {}},
        //    MonetaryAmount: [{CategoryCode: "251"}]}),
        //assertNoRule("nriaid11:2", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {AccountIdentifier: "CASH"}}, Resident: {Entity: {}},
        //    MonetaryAmount: [{CategoryCode: "101"}]}),
        //assertFailure("nriaid11:2", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {AccountIdentifier: "CASH"}}, Resident: {Entity: {}},
        //    MonetaryAmount: [{CategoryCode: "251"}]}),

        //NonResident.Individual.AccountNumber
        // assertSuccess("nrian1", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Individual:{AccountIdentifier: "FCA RESIDENT", AccountNumber: "1235498"}}}),
        // assertSuccess("nrian1", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Individual:{AccountIdentifier: "CASH", AccountNumber: ""}}}),
        // assertFailure("nrian1", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Individual:{AccountIdentifier: "FCA RESIDENT", AccountNumber: ""}}}),
        //May not contain invalid characters like ' or &

        assertSuccess("nrian1:1", { ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "1235498" } } }),
        assertSuccess("nrian1:1", { ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident: { Individual: { AccountIdentifier: "CASH", AccountNumber: "" } } }),
        assertFailure("nrian1:1", { ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "" } } }),

        assertSuccess("nrian1:2", { ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident: { Entity: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "1235498" } } }),
        assertSuccess("nrian1:2", { ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident: { Entity: { AccountIdentifier: "CASH", AccountNumber: "" } } }),
        assertFailure("nrian1:2", { ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident: { Entity: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "" } } }),

        assertSuccess("nrian2:1", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "1235498" } },
          Resident: { Individual: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "12543498" } }
        }),
        assertFailure("nrian2:1", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "1235498" } },
          Resident: { Individual: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "1235498" } }
        }),
        assertSuccess("nrian2:2", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          NonResident: { Entity: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "1235498" } },
          Resident: { Entity: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "12543498" } }
        }),
        assertFailure("nrian2:2", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          NonResident: { Entity: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "1235498" } },
          Resident: { Entity: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "1235498" } }
        }),
        // assertSuccess("nrian2:1", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Individual:{AccountIdentifier: "CASH", AccountNumber: ""}}}),
        // assertFailure("nrian2:1", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Individual:{AccountIdentifier: "FCA RESIDENT", AccountNumber: ""}}}),
        //
        // assertSuccess("nrian2:2", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Entity:{AccountIdentifier: "FCA RESIDENT", AccountNumber: "1235498"}}}),
        // assertSuccess("nrian2:2", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Entity:{AccountIdentifier: "CASH", AccountNumber: ""}}}),
        // assertFailure("nrian2:2", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Entity:{AccountIdentifier: "FCA RESIDENT", AccountNumber: ""}}}),

        //
        // failure("nrian2", 280, "May not be equal to AccountNumber under Resident element",
        //   notEmpty.and(matchesResidentField("AccountNumber"))).onSection("ABCDG"),


        assertSuccess("nrian3:1", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "123 5498" } }
        }),
        assertFailure("nrian3:1", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "123'5498" } }
        }),
        assertFailure("nrian3:1", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          NonResident: { Individual: { AccountIdentifier: "FCA RESIDENT", AccountNumber: "123 & 5498" } }
        }),
        assertSuccess("nrian4:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: { Individual: { AccountNumber: "" } }
        }),
        assertSuccess("nrian4:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: { Entity: { AccountNumber: "" } }
        }),

        assertFailure("nrian4:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: { Individual: { AccountNumber: "ABC" } }
        }),
        assertFailure("nrian4:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: { Entity: { AccountNumber: "ABC" } }
        }),


        //NonResident.Individual.AddressLine1-3
        assertSuccess("nrial11:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { Address: { AddressLine1: "" } } }
        }),
        assertFailure("nrial11:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { Address: { AddressLine1: "jsdhfs" } } }
        }),
        assertNoRule("nrial11:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Individual: { Address: { AddressLine1: "" } } }
        }),

        assertSuccess("nrial21:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { Address: { AddressLine2: "" } } }
        }),
        assertFailure("nrial21:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { Address: { AddressLine2: "sjhvlksaf" } } }
        }),
        assertNoRule("nrial21:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Individual: { Address: { AddressLine2: "" } } }
        }),

        assertSuccess("nrial31:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { Address: { Suburb: "" } } }
        }),
        assertFailure("nrial31:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { Address: { Suburb: "jdhsgf" } } }
        }),
        assertNoRule("nrial31:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Individual: { Address: { Suburb: "" } } }
        }),

        //NonResident.Entity.AddressLine1-3
        assertSuccess("nrial11:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { AddressLine1: "" } } }
        }),
        assertFailure("nrial11:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { AddressLine1: "jsdhfs" } } }
        }),
        assertSuccess("nrial21:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { AddressLine2: "" } } }
        }),
        assertFailure("nrial21:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { AddressLine2: "sjhvlksaf" } } }
        }),
        assertSuccess("nrial31:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { Suburb: "" } } }
        }),
        assertFailure("nrial31:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { Suburb: "jdhsgf" } } }
        }),

        //NonResident.Individual.City
        assertSuccess("nric1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { AddressLine1: "lskdfj", City: "jsdf" } } }
        }),
        assertNoRule("nric1:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Individual: { Address: { AddressLine1: "lskdfj", City: "jsdf" } } }
        }),
        assertSuccess("nric1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { AddressLine2: "lskdfj", City: "jsdf" } } }
        }),
        assertSuccess("nric1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { Suburb: "lskdfj", City: "jsdf" } } }
        }),
        assertSuccess("nric1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { AddressLine1: "lskdfj", City: "" } } }
        }),
        assertSuccess("nric1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { AddressLine2: "lskdfj", City: "" } } }
        }),
        assertSuccess("nric1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { Suburb: "lskdfj", City: "" } } }
        }),
        assertSuccess("nric2:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { Address: { City: "" } } }
        }),
        assertFailure("nric2:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { Address: { City: "fg" } } }
        }),
        assertNoRule("nric2:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Entity: { Address: { City: "" } } }
        }),

        //NonResident.Entity.City
        assertSuccess("nric1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { AddressLine1: "lskdfj", City: "jsdf" } } }
        }),
        assertNoRule("nric1:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Entity: { Address: { AddressLine1: "lskdfj", City: "jsdf" } } }
        }),
        assertSuccess("nric1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { AddressLine2: "lskdfj", City: "jsdf" } } }
        }),
        assertSuccess("nric1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { Suburb: "lskdfj", City: "jsdf" } } }
        }),
        assertSuccess("nric1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { AddressLine1: "lskdfj", City: "" } } }
        }),
        assertSuccess("nric1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { AddressLine2: "lskdfj", City: "" } } }
        }),
        assertSuccess("nric1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { Suburb: "lskdfj", City: "" } } }
        }),
        assertSuccess("nric2:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { City: "" } } }
        }),
        assertFailure("nric2:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { City: "fg" } } }
        }),

        //NonResident.Individual.State
        assertSuccess("nris1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { AddressLine1: "lskdfj", State: "jsdf" } } }
        }),
        assertSuccess("nris1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { AddressLine2: "lskdfj", State: "jsdf" } } }
        }),
        assertSuccess("nris1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { Suburb: "lskdfj", State: "jsdf" } } }
        }),
        assertSuccess("nris1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { AddressLine1: "lskdfj", State: "" } } }
        }),
        assertSuccess("nris1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { AddressLine2: "lskdfj", State: "" } } }
        }),
        assertSuccess("nris1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { Suburb: "lskdfj", State: "" } } }
        }),
        assertNoRule("nris1:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Individual: { Address: { AddressLine3: "lskdfj", State: "" } } }
        }),
        assertSuccess("nris2:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { Address: { State: "" } } }
        }),
        assertFailure("nris2:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { Address: { State: "fg" } } }
        }),
        assertNoRule("nris2:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Entity: { Address: { State: "" } } }
        }),

        //NonResident.Entity.State
        assertSuccess("nris1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { AddressLine1: "lskdfj", State: "jsdf" } } }
        }),
        assertSuccess("nris1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { AddressLine2: "lskdfj", State: "jsdf" } } }
        }),
        assertSuccess("nris1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { Suburb: "lskdfj", State: "jsdf" } } }
        }),
        assertSuccess("nris1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { AddressLine1: "lskdfj", State: "" } } }
        }),
        assertSuccess("nris1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { AddressLine2: "lskdfj", State: "" } } }
        }),
        assertSuccess("nris1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { Suburb: "lskdfj", State: "" } } }
        }),
        assertNoRule("nris1:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Entity: { Address: { AddressLine3: "lskdfj", State: "" } } }
        }),
        assertSuccess("nris2:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { State: "" } } }
        }),
        assertFailure("nris2:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { State: "fg" } } }
        }),

        //NonResident.Individual.PostalCode
        assertSuccess("nriz1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: {
            Individual: {
              Address: {
                AddressLine1: "lskdfj",
                PostalCode: "13579"
              }
            }
          }
        }),
        assertSuccess("nriz1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: {
            Individual: {
              Address: {
                AddressLine2: "lskdfj",
                PostalCode: "13579"
              }
            }
          }
        }),
        assertSuccess("nriz1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { Suburb: "lskdfj", PostalCode: "13579" } } }
        }),
        assertNoRule("nriz1:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Individual: { Address: { Suburb: "lskdfj", PostalCode: "13579" } } }
        }),
        assertSuccess("nriz1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { AddressLine1: "lskdfj", PostalCode: "" } } }
        }),
        assertSuccess("nriz1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { AddressLine2: "lskdfj", PostalCode: "" } } }
        }),
        assertSuccess("nriz1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { Suburb: "lskdfj", PostalCode: "" } } }
        }),
        assertSuccess("nriz2:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { Address: { PostalCode: "" } } }
        }),
        assertFailure("nriz2:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { Address: { PostalCode: "fg" } } }
        }),
        assertSuccess("nriz2:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { PostalCode: "" } } }
        }),
        assertFailure("nriz2:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { PostalCode: "fg" } } }
        }),

        //NonResident.Entity.PostalCode
        assertSuccess("nriz1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { AddressLine1: "lskdfj", PostalCode: "13579" } } }
        }),
        assertSuccess("nriz1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { AddressLine2: "lskdfj", PostalCode: "13579" } } }
        }),
        assertSuccess("nriz1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { Suburb: "lskdfj", PostalCode: "13579" } } }
        }),
        assertNoRule("nriz1:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Entity: { Address: { Suburb: "lskdfj", PostalCode: "13579" } } }
        }),
        assertSuccess("nriz1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { AddressLine1: "lskdfj", PostalCode: "" } } }
        }),
        assertSuccess("nriz1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { AddressLine2: "lskdfj", PostalCode: "" } } }
        }),
        assertSuccess("nriz1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { Suburb: "lskdfj", PostalCode: "" } } }
        }),
        assertSuccess("nriz2:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { PostalCode: "" } } }
        }),
        assertFailure("nriz2:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { PostalCode: "fg" } } }
        }),

        //NonResident.Individual.Country
        assertSuccess("nrictry1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { Country: "sl" } } }
        }),
        assertNoRule("nrictry1:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Individual: { Address: { Country: "dk" } } }
        }),
        assertFailure("nrictry1:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { Country: "" } } }
        }),
        assertSuccess("nrictry2:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { Country: "CF" } } }
        }),
        assertNoRule("nrictry2:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Individual: { Address: { Country: "CF" } } }
        }),
        assertFailure("nrictry2:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { Country: "CT" } } }
        }),
        assertSuccess("nrictry3:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { Country: "AD" } } }
        }),
        assertFailure("nrictry3:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { Country: "ZA" } } }
        }),
        assertNoRule("nrictry3:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { Address: { Country: "AD" } } }
        }),
        //SWIFT country code must not be ZA except if the ForeignIDCountry under IndividualCustomer is NA, LS or SZ
        assertSuccess("nrictry4:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Individual: { Address: { Country: "AD" } } }
        }),
        assertSuccess("nrictry4:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { Name: "John", ForeignIDCountry: "LS" } },
          NonResident: { Individual: { Address: { Country: "ZA" } } }
        }),
        assertFailure("nrictry4:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { Name: "John", ForeignIDCountry: "US" } },
          NonResident: { Individual: { Address: { Country: "ZA" } } }
        }),
        assertNoRule("nrictry4:1", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Individual: { Address: { Country: "AD" } } }
        }),
        //EU is not a valid country and may not be used
        assertSuccess("nrictry5:1", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          NonResident: { Individual: { Address: { Country: "AD" } } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertFailure("nrictry5:1", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          NonResident: { Individual: { Address: { Country: "EU" } } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertNoRule("nrictry5:1", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          NonResident: { Individual: { Address: { Country: "EU" } } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        //EU is not a valid country and may not be used
        assertSuccess("nrictry6:1", {
          ReportingQualifier: "BOPCUS", Flow: "IN",
          NonResident: { Individual: { Address: { Country: "AD" } } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertFailure("nrictry6:1", {
          ReportingQualifier: "BOPCUS", Flow: "IN",
          NonResident: { Individual: { Address: { Country: "EU" } } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertNoRule("nrictry6:1", {
          ReportingQualifier: "BOPCUS", Flow: "IN",
          NonResident: { Individual: { Address: { Country: "EU" } } },
          MonetaryAmount: [{ CategoryCode: "517" }]
        }),
        //For Outflow category 513 the country must be linked to the currency (EU must be used for EUR payments)
        assertSuccess("nrictry7:1", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", FlowCurrency: 'USD',
          NonResident: { Individual: { Address: { Country: "US" } } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        assertSuccess("nrictry7:1", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", FlowCurrency: 'EUR',
          NonResident: { Individual: { Address: { Country: "EU" } } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        assertFailure("nrictry7:1", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", FlowCurrency: 'EUR',
          NonResident: { Individual: { Address: { Country: "GB" } } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        //For Inflow category 517 the country must be linked to the currency (EU must be used for EUR payments)
        assertSuccess("nrictry8:1", {
          ReportingQualifier: "BOPCUS", Flow: "IN", FlowCurrency: 'USD',
          NonResident: { Individual: { Address: { Country: "US" } } },
          MonetaryAmount: [{ CategoryCode: "517" }]
        }),
        assertSuccess("nrictry8:1", {
          ReportingQualifier: "BOPCUS", Flow: "IN", FlowCurrency: 'EUR',
          NonResident: { Individual: { Address: { Country: "EU" } } },
          MonetaryAmount: [{ CategoryCode: "517" }]
        }),
        assertFailure("nrictry8:1", {
          ReportingQualifier: "BOPCUS", Flow: "IN", FlowCurrency: 'EUR',
          NonResident: { Individual: { Address: { Country: "GB" } } },
          MonetaryAmount: [{ CategoryCode: "517" }]
        }),
        assertSuccess("nrictry9:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: { Individual: { Address: { Country: "" } } }
        }),
        assertSuccess("nrictry9:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: { Entity: { Address: { Country: "" } } }
        }),

        assertFailure("nrictry9:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: { Individual: { Address: { Country: "GB" } } }
        }),
        assertFailure("nrictry9:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: { Entity: { Address: { Country: "GB" } } }
        }),


        //NonResident.Entity.Country
        assertSuccess("nrictry1:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { Country: "sl" } } }
        }),
        assertNoRule("nrictry1:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Entity: { Address: { Country: "dk" } } }
        }),
        assertFailure("nrictry1:2", { ReportingQualifier: "BOPCUS", NonResident: { Entity: { Address: { Country: "" } } } }),
        assertSuccess("nrictry2:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { Country: "CF" } } }
        }),
        assertNoRule("nrictry2:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          NonResident: { Entity: { Address: { Country: "CF" } } }
        }),
        assertFailure("nrictry2:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { Country: "CT" } } }
        }),
        assertSuccess("nrictry3:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { Country: "AD" } } }
        }),
        assertFailure("nrictry3:2", {
          ReportingQualifier: "BOPCUS",
          NonResident: { Entity: { Address: { Country: "ZA" } } }
        }),
        assertNoRule("nrictry3:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { Country: "AD" } } }
        }),
        //SWIFT country code must not be ZA except if the ForeignIDCountry under IndividualCustomer is NA, LS or SZ
        assertSuccess("nrictry4:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          NonResident: { Entity: { Address: { Country: "AD" } } }
        }),
        assertSuccess("nrictry4:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { Name: "John", ForeignIDCountry: "LS" } },
          NonResident: { Entity: { Address: { Country: "ZA" } } }
        }),
        assertFailure("nrictry4:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { Name: "John", ForeignIDCountry: "US" } },
          NonResident: { Entity: { Address: { Country: "ZA" } } }
        }),
        assertNoRule("nrictry4:2", { ReportingQualifier: "BOPCUS", NonResident: { Entity: { Address: { Country: "AD" } } } }),
        //EU is not a valid country and may not be used
        assertSuccess("nrictry5:2", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          NonResident: { Entity: { Address: { Country: "AD" } } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertFailure("nrictry5:2", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          NonResident: { Entity: { Address: { Country: "EU" } } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertNoRule("nrictry5:2", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          NonResident: { Entity: { Address: { Country: "EU" } } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        //EU is not a valid country and may not be used
        assertSuccess("nrictry6:2", {
          ReportingQualifier: "BOPCUS", Flow: "IN",
          NonResident: { Entity: { Address: { Country: "AD" } } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertFailure("nrictry6:2", {
          ReportingQualifier: "BOPCUS", Flow: "IN",
          NonResident: { Entity: { Address: { Country: "EU" } } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertNoRule("nrictry6:2", {
          ReportingQualifier: "BOPCUS", Flow: "IN",
          NonResident: { Entity: { Address: { Country: "EU" } } },
          MonetaryAmount: [{ CategoryCode: "517" }]
        }),
        //For Outflow category 513 the country must be linked to the currency (EU must be used for EUR payments)
        assertSuccess("nrictry7:2", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", FlowCurrency: 'USD',
          NonResident: { Entity: { Address: { Country: "US" } } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        assertSuccess("nrictry7:2", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", FlowCurrency: 'EUR',
          NonResident: { Entity: { Address: { Country: "EU" } } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        assertFailure("nrictry7:2", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", FlowCurrency: 'EUR',
          NonResident: { Entity: { Address: { Country: "GB" } } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        //For Inflow category 517 the country must be linked to the currency (EU must be used for EUR payments)
        assertSuccess("nrictry8:2", {
          ReportingQualifier: "BOPCUS", Flow: "IN", FlowCurrency: 'USD',
          NonResident: { Entity: { Address: { Country: "US" } } },
          MonetaryAmount: [{ CategoryCode: "517" }]
        }),
        assertSuccess("nrictry8:2", {
          ReportingQualifier: "BOPCUS", Flow: "IN", FlowCurrency: 'EUR',
          NonResident: { Entity: { Address: { Country: "EU" } } },
          MonetaryAmount: [{ CategoryCode: "517" }]
        }),
        assertFailure("nrictry8:2", {
          ReportingQualifier: "BOPCUS", Flow: "IN", FlowCurrency: 'EUR',
          NonResident: { Entity: { Address: { Country: "GB" } } },
          MonetaryAmount: [{ CategoryCode: "517" }]
        }),

        //Resident
        //Resident General
        assertSuccess("rg1", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Name: "lskdfj" } } }),
        assertSuccess("rg1", { ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "lskdfj" } } }),
        assertSuccess("rg1", { ReportingQualifier: "BOPCUS", Resident: { Exception: { ExceptionName: "lskdfj" } } }),
        assertFailure("rg1", { ReportingQualifier: "BOPCUS", Resident: {} }),
        assertSuccess("rg2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { EntityName: "bla" } },
          MonetaryAmount: [{ CategoryCode: "255" }]
        }),
        assertFailure("rg2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { Name: "John" } },
          MonetaryAmount: [{ CategoryCode: "255" }]
        }),
        assertSuccess("riidn4", { ReportingQualifier: "BOPCUS", Resident: { Individual: { IDNumber: "1234567890123" } } }),
        assertSuccess("riidn4", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { TempResPermitNumber: "9874654" } }
        }),
        assertSuccess("riidn4", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { ForeignIDNumber: "987645121" } }
        }),
        assertFailure("riidn4", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Name: "aslkdfj" } } }),
        assertNoRule("riidn4", { ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "bla" } } }),

        //Resident.Individual.IDNumber: If the Subject is SDA, IDNumber must be completed
        assertSuccess("riidn5", {
          ReportingQualifier: "BOPCUS", Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'SDA' } }]
        }),
        assertSuccess("riidn5", { ReportingQualifier: "BOPCUS", Resident: { Individual: { IDNumber: "1234567890123" } } }),
        assertSuccess("riidn5", { ReportingQualifier: "BOPCUS", Resident: { Individual: {} } }),
        assertFailure("riidn5", {
          ReportingQualifier: "BOPCUS", Resident: { Individual: {} },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'SDA' } }]
        }),

        assertSuccess("ritrpn3", { ReportingQualifier: "BOPCUS", Resident: { Individual: { IDNumber: "1234567890123" } } }),
        assertSuccess("ritrpn3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { TempResPermitNumber: "9874654" } }
        }),
        assertSuccess("ritrpn3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { ForeignIDNumber: "987645121" } }
        }),
        assertFailure("ritrpn3", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Name: "aslkdfj" } } }),
        assertNoRule("ritrpn3", { ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "bla" } } }),
        assertSuccess("ritrpn4", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { TempResPermitNumber: "9874654" } }
        }),
        assertFailure("ritrpn4", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { TempResPermitNumber: "9874654" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'SDA' } }]
        }),

        assertSuccess("rifidn3", { ReportingQualifier: "BOPCUS", Resident: { Individual: { IDNumber: "1234567890123" } } }),
        assertSuccess("rifidn3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { TempResPermitNumber: "9874654" } }
        }),
        assertSuccess("rifidn3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { ForeignIDNumber: "987645121" } }
        }),
        assertFailure("rifidn3", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Name: "aslkdfj" } } }),
        assertNoRule("rifidn3", { ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "bla" } } }),

        //If the Non Resident AccountIdentifier is NON RESIDENT OTHER or CASH, the Resident IndividualCustomer is completed and the category is 250 or 251, the ForeignIDNumber must be completed
        assertSuccess("rifidn4", {
          ReportingQualifier: "BOPCUS", NonResident: { Individual: { AccountIdentifier: "CASH" } },
          Resident: { Individual: { ForeignIDNumber: "987645121" } },
          MonetaryAmount: [{ CategoryCode: "250" }]
        }),
        assertSuccess("rifidn4", {
          ReportingQualifier: "BOPCUS", NonResident: { Individual: { AccountIdentifier: "OTHER" } },
          Resident: { Individual: { ForeignIDNumber: "" } },
          MonetaryAmount: [{ CategoryCode: "250" }]
        }),
        assertFailure("rifidn4", {
          ReportingQualifier: "BOPCUS", NonResident: { Individual: { AccountIdentifier: "CASH" } },
          Resident: { Individual: { ForeignIDNumber: "" } },
          MonetaryAmount: [{ CategoryCode: "251" }]
        }),
        assertFailure("rifidn4", {
          ReportingQualifier: "BOPCUS", NonResident: { Individual: { AccountIdentifier: "NON RESIDENT OTHER" } },
          Resident: { Individual: { ForeignIDNumber: "" } },
          MonetaryAmount: [{ CategoryCode: "250" }]
        }),
        assertSuccess("rifidn5", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { ForeignIDNumber: "987645121" } }
        }),
        assertFailure("rifidn5", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { ForeignIDNumber: "987645121" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'SDA' } }]
        }),

        assertSuccess("rg5", { ReportingQualifier: "BOPCARD NON RESIDENT" }),
        assertNoRule("rg5", { ReportingQualifier: "BOPCARD RESIDENT" }),
        assertFailure("rg5", { ReportingQualifier: "BOPCARD NON RESIDENT", Resident: { Individual: { Name: "slkdjf" } } }),

        //Resident.Individual.Surname
        assertSuccess("risn1", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Surname: "alksdjf" } } }),
        assertNoRule("risn1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { Surname: "alksdjf" } }
        }),
        assertFailure("risn1", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Name: "alksdjf" } } }),
        assertSuccess("risn2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Surname: "alksdjf" } } }),
        assertSuccess("risn2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Surname: "STRATE" } } }),
        assertFailure("risn2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Surname: "MUTUAL PARTY" } } }),
        assertFailure("risn2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Surname: "MUTuAL PaRTy" } } }),
        assertSuccess("risn3", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Surname: "" } } }),
        assertNoRule("risn3", {
          ReportingQualifier: "NON RESIDENT RAND", Resident: { Individual: { Surname: "" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess("risn3", {
          ReportingQualifier: "BOPCUS", Resident: { Individual: { Surname: "alksdjf" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertFailure("risn3", {
          ReportingQualifier: "BOPCUS", Resident: { Individual: { Surname: "" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('risn4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { Surname: "Surname" } },
          MonetaryAmount: [{ CategoryCode: '400', AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertFailure('risn4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { Surname: "" } },
          MonetaryAmount: [{ CategoryCode: '400', AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertNoRule('risn4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { Surname: "" } },
          MonetaryAmount: [{ CategoryCode: '100', AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        //Resident.Individual.Name
        assertSuccess("rin1", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Name: "alksdjf" } } }),
        assertNoRule("rin1", { ReportingQualifier: "BOPCARD NON RESIDENT", Resident: { Individual: { Name: "alksdjf" } } }),
        assertFailure("rin1", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Surname: "alksdjf" } } }),
        assertSuccess("rin2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Name: "" } } }),
        assertNoRule("rin2", {
          ReportingQualifier: "NON RESIDENT RAND", Resident: { Individual: { Name: "" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess("rin2", {
          ReportingQualifier: "BOPCUS", Resident: { Individual: { Name: "alksdjf" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertFailure("rin2", {
          ReportingQualifier: "BOPCUS", Resident: { Individual: { Name: "" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess("rin3", {
          ReportingQualifier: "BOPCUS", Flow: "IN", Resident: { Individual: { Name: "testSurname" } },
          MonetaryAmount: [{ CategoryCode: '400', AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertFailure("rin3", {
          ReportingQualifier: "BOPCUS", Flow: "IN", Resident: { Individual: { Name: "" } },
          MonetaryAmount: [{ CategoryCode: '400', AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertNoRule("rin3", {
          ReportingQualifier: "BOPCUS", Flow: "IN", Resident: { Individual: { Name: "testSurname" } },
          MonetaryAmount: [{ CategoryCode: '100', AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        // The words specified under NonResident or Resident ExceptionName, excluding STRATE, must not be used.
        assertSuccess("rin4", {
          ReportingQualifier: "BOPCUS", Resident: {
            Individual: { Name: "STRATE" }
          }
        }),

        //Resident.Individual.Gender
        assertSuccess("rig1", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Gender: "M" } } }),
        assertNoRule("rig1", { ReportingQualifier: "BOPCARD NON RESIDENT", Resident: { Individual: { Gender: "M" } } }),
        assertFailure("rig1", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Name: "M" } } }),
        assertSuccess("rig2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Gender: "F" } } }),
        assertNoRule("rig2", { ReportingQualifier: "BOPCARD NON RESIDENT", Resident: { Individual: { Gender: "F" } } }),
        assertFailure("rig2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Gender: "Male" } } }),

        //Resident.Individual.DateOfBirth
        assertSuccess("ridob1", { ReportingQualifier: "BOPCUS", Resident: { Individual: { DateOfBirth: "1999/1/1" } } }),
        assertNoRule("ridob1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { Gender: "M" } }
        }),
        assertFailure("ridob1", { ReportingQualifier: "BOPCUS", Resident: { Individual: { Name: "M" } } }),
        assertSuccess("ridob2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { DateOfBirth: "1999-01-01" } } }),
        assertSuccess("ridob2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { DateOfBirth: "1968-11-03" } } }),
        assertSuccess("ridob2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { DateOfBirth: "2010-02-30" } } }),
        assertFailure("ridob2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { DateOfBirth: "1968-13-03" } } }),
        assertFailure("ridob2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { DateOfBirth: "1968-11-40" } } }),
        assertFailure("ridob2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { DateOfBirth: "1968-11-32" } } }),
        assertFailure("ridob2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { DateOfBirth: "1807-11-03" } } }),
        assertFailure("ridob2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { DateOfBirth: "11968-11-03" } } }),
        assertFailure("ridob2", { ReportingQualifier: "BOPCUS", Resident: { Individual: { DateOfBirth: "1968-11-033" } } }),
        assertSuccess("ridob3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { IDNumber: "6811035039084", DateOfBirth: "1968-11-03" } }
        }),
        assertSuccess("ridob3", { ReportingQualifier: "BOPCUS", Resident: { Individual: { DateOfBirth: "1968-11-03" } } }),
        assertWarning("ridob3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { IDNumber: "6811035039084", DateOfBirth: "1969-12-03" } }
        }),

        //Resident.Individual.IDNumber
        assertSuccess("riidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "511" }]
        }),
        assertNoRule("riidn1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "511" }]
        }),
        assertFailure("riidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { Name: "M" } },
          MonetaryAmount: [{ CategoryCode: "511" }]
        }),
        assertSuccess("riidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "512" }]
        }),
        assertNoRule("riidn1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "512" }]
        }),
        assertFailure("riidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { Name: "M" } },
          MonetaryAmount: [{ CategoryCode: "512" }]
        }),
        assertSuccess("riidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        assertNoRule("riidn1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        assertFailure("riidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { Name: "M" } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        // assertSuccess("riidn1", {
        //   ReportingQualifier: "BOPCUS",
        //   Resident: {Individual: {IDNumber: "1234567890123"}},
        //   MonetaryAmount: [{CategoryCode: "514"}]
        // }),
        // assertNoRule("riidn1", {
        //   ReportingQualifier: "BOPCARD NON RESIDENT",
        //   Resident: {Individual: {IDNumber: "1234567890123"}},
        //   MonetaryAmount: [{CategoryCode: "514"}]
        // }),
        // assertFailure("riidn1", {
        //   ReportingQualifier: "BOPCUS",
        //   Resident: {Individual: {Name: "M"}},
        //   MonetaryAmount: [{CategoryCode: "514"}]
        // }),
        // assertSuccess("riidn1", {
        //   ReportingQualifier: "BOPCUS",
        //   Resident: {Individual: {IDNumber: "1234567890123"}},
        //   MonetaryAmount: [{CategoryCode: "515"}]
        // }),
        // assertNoRule("riidn1", {
        //   ReportingQualifier: "BOPCARD NON RESIDENT",
        //   Resident: {Individual: {IDNumber: "1234567890123"}},
        //   MonetaryAmount: [{CategoryCode: "515"}]
        // }),
        // assertFailure("riidn1", {
        //   ReportingQualifier: "BOPCUS",
        //   Resident: {Individual: {Name: "M"}},
        //   MonetaryAmount: [{CategoryCode: "515"}]
        // }),

        //If the Flow is OUT and category 401 is used, must be completed
        assertSuccess("riidn2", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "401" }]
        }),
        assertNoRule("riidn2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Flow: "OUT",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "401" }]
        }),
        assertNoRule("riidn2", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "401" }]
        }),
        assertNoRule("riidn2", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "515" }]
        }),
        assertFailure("riidn2", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Individual: { Name: "M" } },
          MonetaryAmount: [{ CategoryCode: "401" }]
        }),

        //This is an Invalid ID number (Note, if the ID number does not comply to the algorithm, the Subject must be INVALIDIDNUMBER to accept an invalid ID number)
        assertSuccess("riidn3", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Individual: { IDNumber: "6606190363083" } }
        }),
        assertFailure("riidn3", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Individual: { IDNumber: "6606190363084" } }
        }),
        assertSuccess("riidn3", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: { Individual: { IDNumber: "6606190363084" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'INVALIDIDNUMBER' } }]
        }),
        assertFailure("riidn3", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Individual: { IDNumber: "196606190363083" } }
        }),
        assertSuccess("riidn3", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Flow: "OUT",
          Resident: { Individual: { IDNumber: "6606190363083" } }
        }),
        assertFailure("riidn3", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Flow: "OUT",
          Resident: { Individual: { IDNumber: "6606190363084" } }
        }),
        assertSuccess("riidn3", {
          ReportingQualifier: "BOPCARD RESIDENT", Flow: "OUT", Resident: { Individual: { IDNumber: "6606190363084" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'INVALIDIDNUMBER' } }]
        }),
        assertFailure("riidn3", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Flow: "OUT",
          Resident: { Individual: { IDNumber: "196606190363083" } }
        }),

        //Resident.Individual.TempResPermitNumber
        assertSuccess("ritrpn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: "511", CategorySubCode: "02" }]
        }),
        assertNoRule("ritrpn1", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: "511", CategorySubCode: "02" }]
        }),
        assertNoRule("ritrpn1", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: "101", CategorySubCode: "01" }]
        }),
        assertFailure("ritrpn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { TempResPermitNumber: "9874652" } },
          MonetaryAmount: [{ CategoryCode: "511", CategorySubCode: "02" }]
        }),
        assertSuccess("ritrpn2", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: "401" }]
        }),
        assertNoRule("ritrpn2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Flow: "OUT",
          Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: "401" }]
        }),
        assertFailure("ritrpn2", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Individual: { TempResPermitNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "401" }]
        }),

        //Resident.Individual.ForeignIDNumber
        assertSuccess("rifidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { Name: "name" } },
          MonetaryAmount: [{ CategoryCode: "511" }]
        }),
        assertNoRule("rifidn1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { Name: "name" } },
          MonetaryAmount: [{ CategoryCode: "511" }]
        }),
        assertFailure("rifidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { ForeignIDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "511" }]
        }),
        assertSuccess("rifidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { Name: "name" } },
          MonetaryAmount: [{ CategoryCode: "512" }]
        }),
        assertNoRule("rifidn1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { Name: "name" } },
          MonetaryAmount: [{ CategoryCode: "512" }]
        }),
        assertFailure("rifidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { ForeignIDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "512" }]
        }),
        assertSuccess("rifidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { Name: "name" } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        assertNoRule("rifidn1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { Name: "name" } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        assertFailure("rifidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { ForeignIDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        // assertSuccess("rifidn1", {
        //   ReportingQualifier: "BOPCUS",
        //   Resident: {Individual: {Name: "name"}},
        //   MonetaryAmount: [{CategoryCode: "514"}]
        // }),
        // assertNoRule("rifidn1", {
        //   ReportingQualifier: "BOPCARD NON RESIDENT",
        //   Resident: {Individual: {Name: "name"}},
        //   MonetaryAmount: [{CategoryCode: "514"}]
        // }),
        // assertFailure("rifidn1", {
        //   ReportingQualifier: "BOPCUS",
        //   Resident: {Individual: {ForeignIDNumber: "1234567890123"}},
        //   MonetaryAmount: [{CategoryCode: "514"}]
        // }),
        // assertSuccess("rifidn1", {
        //   ReportingQualifier: "BOPCUS",
        //   Resident: {Individual: {Name: "name"}},
        //   MonetaryAmount: [{CategoryCode: "515"}]
        // }),
        // assertNoRule("rifidn1", {
        //   ReportingQualifier: "BOPCARD NON RESIDENT",
        //   Resident: {Individual: {Name: "name"}},
        //   MonetaryAmount: [{CategoryCode: "515"}]
        // }),
        // assertFailure("rifidn1", {
        //   ReportingQualifier: "BOPCUS",
        //   Resident: {Individual: {ForeignIDNumber: "1234567890123"}},
        //   MonetaryAmount: [{CategoryCode: "515"}]
        // }),

        // failure("rifidn2", 452, "If the Flow is OUT and category 401 is used, may not be completed",
        // notEmpty).onSection("AB").onCategory("401"),
        assertSuccess("rifidn2", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: "401" }]
        }),
        assertNoRule("rifidn2", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          Resident: { Individual: { ForeignIDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "401" }]
        }),
        assertNoRule("rifidn2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Flow: "OUT",
          Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: "401" }]
        }),
        assertFailure("rifidn2", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Individual: { ForeignIDNumber: "1234567890123" } },
          MonetaryAmount: [{ CategoryCode: "401" }]
        }),

        //Resident.Individual.ForeignIDCountry
        assertSuccess("rifidc1", {
          ReportingQualifier: "BOPCUS",
          Resident: {
            Individual: {
              ForeignIDNumber: "1234567890123",
              ForeignIDCountry: "ZA"
            }
          }
        }),
        assertNoRule("rifidc1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: {
            Individual: {
              ForeignIDNumber: "1234567890123",
              ForeignIDCountry: "ZA"
            }
          }
        }),
        assertFailure("rifidc1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { ForeignIDNumber: "1234567890123" } }
        }),
        assertSuccess("rifidc2", {
          ReportingQualifier: "BOPCUS",
          Resident: {
            Individual: {
              ForeignIDNumber: "1234567890123",
              ForeignIDCountry: "ZA"
            }
          }
        }),
        assertNoRule("rifidc2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: {
            Individual: {
              ForeignIDNumber: "1234567890123",
              ForeignIDCountry: "ZA"
            }
          }
        }),
        assertFailure("rifidc2", {
          ReportingQualifier: "BOPCUS",
          Resident: {
            Individual: {
              ForeignIDNumber: "1234567890123",
              ForeignIDCountry: "ZZ"
            }
          }
        }),
        //The ForeignID Country must not be ZA
        assertSuccess("rifidc3", {
          ReportingQualifier: "BOPCUS",
          Resident: {
            Individual: {
              ForeignIDNumber: "1234567890123",
              ForeignIDCountry: "US"
            }
          }
        }),
        assertFailure("rifidc3", {
          ReportingQualifier: "BOPCUS",
          Resident: {
            Individual: {
              ForeignIDNumber: "1234567890123",
              ForeignIDCountry: "ZA"
            }
          }
        }),

        //Resident.Individual.PassportNumber
        assertSuccess("ripn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: "255" }]
        }),
        assertNoRule("ripn1", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: "255" }]
        }),
        assertFailure("ripn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { PassportNumber: "984R1" } },
          MonetaryAmount: [{ CategoryCode: "255" }]
        }),
        assertSuccess("ripn2", { ReportingQualifier: "BOPDIR", Resident: { Individual: {} } }),
        assertNoRule("ripn2", { ReportingQualifier: "NON REPORTABLE", Resident: { Individual: {} } }),
        assertFailure("ripn2", { ReportingQualifier: "BOPDIR", Resident: { Individual: { PassportNumber: "984R1" } } }),


        assertFailure("ripn3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { PassportNumber: "" } },
          MonetaryAmount: [{ CategoryCode: "256" }]
        }),
        assertNoRule("ripn3", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: "256" }]
        }),
        assertSuccess("ripn3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { PassportNumber: "984R1" } },
          MonetaryAmount: [{ CategoryCode: "256" }]
        }),


        //Resident.Individual.PassportCountry
        assertSuccess("ripc1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { PassportNumber: "1234567890123", PassportCountry: "ZA" } }
        }),
        assertNoRule("ripc1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { PassportNumber: "1234567890123", PassportCountry: "ZA" } }
        }),
        assertFailure("ripc1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { PassportNumber: "1234567890123" } }
        }),
        assertSuccess("ripc2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { PassportNumber: "1234567890123", PassportCountry: "ZA" } }
        }),
        assertNoRule("ripc2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { PassportNumber: "1234567890123", PassportCountry: "ZA" } }
        }),
        assertFailure("ripc2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { PassportNumber: "1234567890123", PassportCountry: "ZZ" } }
        }),

        //Resident.Entity.EntityName
        assertSuccess("relen1", { ReportingQualifier: "NON REPORTABLE", Resident: { Entity: { EntityName: "slkdjf" } } }),
        assertNoRule("relen1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { EntityName: "slkdjf" } }
        }),
        assertFailure("relen1", {
          ReportingQualifier: "NON RESIDENT RAND",
          Resident: { Entity: { TradingName: "slkdjf" } }
        }),
        assertSuccess("relen2", { ReportingQualifier: "BOPCARD NON RESIDENT", Resident: { Entity: {} } }),
        assertNoRule("relen2", { ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "slkdjf" } } }),
        assertFailure("relen2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { EntityName: "slkdjf" } }
        }),
        assertSuccess("relen3", { ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "" } } }),
        assertNoRule("relen3", {
          ReportingQualifier: "NON RESIDENT RAND", Resident: { Entity: { EntityName: "" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertFailure("relen3", {
          ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "alksdjf" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess("relen3", {
          ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "" } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),


        assertSuccess("relen4", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { EntityName: "company2", RegistrationNumber: "2013/1234567/07" } }
        }),
        assertFailure("relen4", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { EntityName: "company3", RegistrationNumber: "2013/1234567/07" } }
        }),
        assertSuccess("relen4", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { EntityName: "notinlist", RegistrationNumber: "not IHQ reg no" } }
        }),

        // If NonResident Exception is IHQ, value must not be same as SARB-registered IHQ name
        assertSuccess("relen5", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { EntityName: "notinlist" } },
          NonResident: { Exception: { ExceptionName: "IHQ" } }
        }),
        assertFailure("relen5", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { EntityName: "company2" } },
          NonResident: { Exception: { ExceptionName: "IHQ" } }
        }),
        assertSuccess("relen5", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { EntityName: "company2" } },
          NonResident: { Exception: { ExceptionName: "OTHER" } }
        }),

        //Resident.Entity.TradingName
        assertSuccess("retn1", { ReportingQualifier: "BOPCUS", Resident: { Entity: { TradingName: "slkdjf" } } }),
        assertNoRule("retn1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { TradingName: "slkdjf" } }
        }),
        assertFailure("retn1", { ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "slkdjf" } } }),

        //Resident.Entity.RegistrationNumber
        assertSuccess("rern1", { ReportingQualifier: "BOPCUS", Resident: { Entity: { RegistrationNumber: "slkdjf" } } }),
        assertNoRule("rern1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { RegistrationNumber: "slkdjf" } }
        }),
        assertFailure("rern1", { ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "slkdjf" } } }),
        //If Subject is AIRPORT the RegistrationNumber must be GOVERNMENT
        assertSuccess("rern2", {
          ReportingQualifier: "BOPCUS", Resident: { Entity: { RegistrationNumber: "GOVERNMENT" } },
          MonetaryAmount: [{ CategoryCode: "830", AdHocRequirement: { Subject: 'AIRPORT' } }]
        }),
        assertFailure("rern2", {
          ReportingQualifier: "BOPCUS", Resident: { Entity: { RegistrationNumber: "slkdjf" } },
          MonetaryAmount: [{ CategoryCode: "830", AdHocRequirement: { Subject: 'AIRPORT' } }]
        }),
        assertSuccess("rern2", {
          ReportingQualifier: "BOPCUS", Resident: { Entity: { RegistrationNumber: "slkdjf" } },
          MonetaryAmount: [{ CategoryCode: "830", AdHocRequirement: { Subject: 'SETOFF' } }]
        }),
        assertNoRule("rern2", {
          ReportingQualifier: "BOPCUS", Resident: { Entity: { RegistrationNumber: "slkdjf" } },
          MonetaryAmount: [{ CategoryCode: "256", AdHocRequirement: { Subject: 'AIRPORT' } }]
        }),
        assertFailure("rern3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { RegistrationNumber: "2013/1234568/07" } },
          NonResident: { Exception: { ExceptionName: "IHQ" } }
        }),
        assertSuccess("rern3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { RegistrationNumber: "" } },
          NonResident: { Exception: { ExceptionName: "IHQ" } }
        }),
        assertSuccess("rern3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { RegistrationNumber: "notinlist" } },
          NonResident: { Exception: { ExceptionName: "IHQ" } }
        }),

        assertSuccess("rern4", {
          ReportingQualifier: "BOPCUS", Resident: { Entity: { RegistrationNumber: "123" } }
        }),
        assertFailure("rern4", {
          ReportingQualifier: "BOPCUS", Resident: { Entity: { RegistrationNumber: " 123 " } }
        }),

        //Resident.Entity.InstitutionalSector
        assertSuccess("reis1", { ReportingQualifier: "BOPCUS", Resident: { Entity: { InstitutionalSector: "slkdjf" } } }),
        assertNoRule("reis1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { InstitutionalSector: "kd" } }
        }),
        assertFailure("reis1", { ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "slkdjf" } } }),
        assertSuccess("reis2", { ReportingQualifier: "BOPCARD NON RESIDENT", Resident: { Entity: {} } }),
        assertNoRule("reis2", { ReportingQualifier: "BOPCUS", Resident: { Entity: {} } }),
        assertFailure("reis2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { InstitutionalSector: "slkdjf" } }
        }),

        //Resident.Entity.IndustrialClassification
        assertSuccess("reic1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { IndustrialClassification: "slkdjf" } }
        }),
        assertNoRule("reic1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { IndustrialClassification: "kd" } }
        }),
        assertFailure("reic1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { EntityName: "slkdjf" } }
        }),
        assertSuccess("reic2", { ReportingQualifier: "BOPCARD NON RESIDENT", Resident: { Entity: {} } }),
        assertNoRule("reic2", { ReportingQualifier: "BOPCUS", Resident: { Entity: {} } }),
        assertFailure("reic2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { IndustrialClassification: "slkdjf" } }
        }),

        //Resident.Exception
        assertSuccess("re1", { ReportingQualifier: "BOPCARD RESIDENT", Resident: { Individual: {} } }),
        assertNoRule("re1", { ReportingQualifier: "BOPCUS", Resident: { Individual: {} } }),
        assertFailure("re1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Exception: { ExceptionName: "skjdh" } }
        }),

        //Resident.Exception.ExceptionName
        assertSuccess("ren1", { ReportingQualifier: "BOPCUS", Resident: { Exception: { ExceptionName: "RAND CHEQUE" } } }),
        assertSuccess("ren1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "NON RESIDENT RAND" } }
        }),
        assertSuccess("ren1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK DIVIDENDS" } }
        }),
        assertSuccess("ren1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "NOSTRO NON REPORTABLE" } }
        }),
        assertSuccess("ren1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "VOSTRO INTERBANK" } }
        }),
        assertSuccess("ren1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "RTGS NON REPORTABLE" } }
        }),
        assertSuccess("ren1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK BANK CHARGES" } }
        }),
        assertNoRule("ren1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Exception: { ExceptionName: "STRATE" } }
        }),
        assertFailure("ren1", { ReportingQualifier: "BOPCUS", Resident: { Exception: { ExceptionName: "skjdh" } } }),
        assertFailure("ren1", { ReportingQualifier: "BOPCUS", Resident: { Exception: {} } }),

        assertSuccess("ren2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "250" }]
        }),
        assertSuccess("ren2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "251" }]
        }),
        assertNoRule("ren2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "250" }]
        }),
        assertNoRule("ren2", {
          ReportingQualifier: "NON RESIDENT RAND",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "251" }]
        }),
        assertFailure("ren2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK INTEREST" } },
          MonetaryAmount: [{ CategoryCode: "250" }]
        }),
        assertFailure("ren2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "251" }]
        }),

        assertSuccess("ren3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "311" }]
        }),
        assertSuccess("ren3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "UNCLAIMED DRAFTS" } },
          MonetaryAmount: [{ CategoryCode: "519" }]
        }),
        assertNoRule("ren3", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "250" }]
        }),
        assertNoRule("ren3", {
          ReportingQualifier: "NON RESIDENT RAND",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "251" }]
        }),
        assertFailure("ren3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "118" }]
        }),
        assertFailure("ren3", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),

        assertSuccess("ren4", { ReportingQualifier: "NON RESIDENT RAND", Resident: { Exception: { ExceptionName: "" } } }),
        assertSuccess("ren4", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertSuccess("ren4", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "VOSTRO INTERBANK" } }
        }),
        assertSuccess("ren4", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "FCA NON RESIDENT NON REPORTABLE" } }
        }),
        assertSuccess("ren4", { ReportingQualifier: "INTERBANK", Resident: { Exception: {} } }),
        assertSuccess("ren4", { ReportingQualifier: "BOPCARD RESIDENT", Resident: { Exception: { ExceptionName: "" } } }),
        assertSuccess("ren4", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Exception: { ExceptionName: "" } }
        }),
        assertSuccess("ren4", { ReportingQualifier: "BOPDIR", Resident: { Exception: { ExceptionName: "" } } }),
        assertNoRule("ren4", { ReportingQualifier: "BOPCUS", Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } } }),
        assertFailure("ren4", {
          ReportingQualifier: "NON RESIDENT RAND",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        assertFailure("ren4", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        assertFailure("ren4", {
          ReportingQualifier: "INTERBANK",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        assertFailure("ren4", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        assertFailure("ren4", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } }
        }),
        assertFailure("ren4", { ReportingQualifier: "BOPDIR", Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } } }),
        //May not be used. BULK PENSIONS is only valid for category 407 or 400
        assertSuccess("ren5", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "228" }]
        }),
        assertSuccess("ren5", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "" } },
          MonetaryAmount: [{ CategoryCode: "514" }]
        }),
        assertNoRule("ren5", {
          ReportingQualifier: "INTERBANK",
          Resident: { Exception: { ExceptionName: "BULK PENSIONS" } },
          MonetaryAmount: [{ CategoryCode: "407" }]
        }),
        assertNoRule("ren5", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK PENSIONS" } },
          MonetaryAmount: [{ CategoryCode: "407" }]
        }),
        assertNoRule("ren5", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK PENSIONS" } },
          MonetaryAmount: [{ CategoryCode: "400" }]
        }),
        assertFailure("ren5", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK PENSIONS" } },
          MonetaryAmount: [{ CategoryCode: "228" }]
        }),
        assertFailure("ren5", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK PENSIONS" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),

        assertSuccess("ren6", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "228" }]
        }),
        assertNoRule("ren6", {
          ReportingQualifier: "DIRECT",
          Resident: { Exception: { ExceptionName: "UNCLAIMED DRAFTS" } },
          MonetaryAmount: [{ CategoryCode: "200" }]
        }),
        assertFailure("ren6", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "UNCLAIMED DRAFTS" } },
          MonetaryAmount: [{ CategoryCode: "228" }]
        }),
        assertFailure("ren6", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "UNCLAIMED DRAFTS" } },
          MonetaryAmount: [{ CategoryCode: "101" }]
        }),
        assertFailure("ren6", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "UNCLAIMED DRAFTS" } },
          MonetaryAmount: [{ CategoryCode: "987" }]
        }),
        assertFailure("ren6", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "UNCLAIMED DRAFTS" } },
          MonetaryAmount: [{ CategoryCode: "555" }]
        }),

        assertSuccess("ren7", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK INTEREST" } }
        }),
        assertNoRule("ren7", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "BULK INTEREST" } }
        }),
        assertFailure("ren7", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "FCA NON RESIDENT NON REPORTABLE" } }
        }),

        assertSuccess("ren8", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK INTEREST" } }
        }),
        assertNoRule("ren8", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "BULK INTEREST" } }
        }),
        assertFailure("ren8", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),

        assertSuccess("ren9", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK INTEREST" } }
        }),
        assertNoRule("ren9", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "BULK INTEREST" } }
        }),
        assertFailure("ren9", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "VOSTRO INTERBANK" } }
        }),
        //May not be used. BULK INTEREST is only valid for category 309/08 or 300
        assertSuccess("ren10", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "202" }]
        }),
        assertNoRule("ren10", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "200" }]
        }),
        assertNoRule("ren10", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "275" }]
        }),
        assertNoRule("ren10", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "201" }]
        }),
        assertFailure("ren10", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK INTEREST" } },
          MonetaryAmount: [{ CategoryCode: "559" }]
        }),
        assertFailure("ren10", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK INTEREST" } },
          MonetaryAmount: [{ CategoryCode: "159" }]
        }),

        assertSuccess("ren11", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "202" }]
        }),
        assertNoRule("ren11", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "300" }]
        }),
        assertNoRule("ren11", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "301" }]
        }),
        assertNoRule("ren11", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "201" }]
        }),
        assertFailure("ren11", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK DIVIDENDS" } },
          MonetaryAmount: [{ CategoryCode: "559" }]
        }),
        assertFailure("ren11", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK DIVIDENDS" } },
          MonetaryAmount: [{ CategoryCode: "159" }]
        }),

        assertSuccess("ren12", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "202" }]
        }),
        assertNoRule("ren12", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "200" }]
        }),
        assertNoRule("ren12", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "275" }]
        }),
        assertNoRule("ren12", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "201" }]
        }),
        assertFailure("ren12", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK BANK CHARGES" } },
          MonetaryAmount: [{ CategoryCode: "559" }]
        }),
        assertFailure("ren12", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK BANK CHARGES" } },
          MonetaryAmount: [{ CategoryCode: "159" }]
        }),

        assertSuccess("ren13", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "202" }]
        }),
        assertNoRule("ren13", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "600" }]
        }),
        assertNoRule("ren13", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "601", CategorySubCode: "01" }]
        }),
        assertNoRule("ren13", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "603", CategorySubCode: "01" }]
        }),
        assertNoRule("ren13", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [{ CategoryCode: "201" }]
        }),
        assertFailure("ren13", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "559" }]
        }),
        assertFailure("ren13", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "STRATE" } },
          MonetaryAmount: [{ CategoryCode: "159" }]
        }),

        assertSuccess("ren14", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertNoRule("ren14", { ReportingQualifier: "BOPCUS", Resident: { Exception: { ExceptionName: "RAND CHEQUE" } } }),
        assertFailure("ren14", {
          ReportingQualifier: "NON RESIDENT RAND",
          Resident: { Exception: { ExceptionName: "RAND CHEQUE" } }
        }),
        assertFailure("ren14", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "RAND CHEQUE" } }
        }),

        assertSuccess("ren15", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertNoRule("ren15", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK PENSIONS" } }
        }),
        assertFailure("ren15", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "BULK PENSIONS" } }
        }),
        //May not be completed except if the Non Resident AccountIdentifier is NON RESIDENT RAND
        assertSuccess("ren16", {
          ReportingQualifier: "BOPCUS", NonResident: { Individual: { AccountIdentifier: "NON RESIDENT RAND" } },
          Resident: { Exception: { ExceptionName: "NON RESIDENT RAND" } }
        }),
        assertFailure("ren16", {
          ReportingQualifier: "BOPCUS", NonResident: { Individual: { AccountIdentifier: "CASH" } },
          Resident: { Exception: { ExceptionName: "NON RESIDENT RAND" } }
        }),
        assertFailure("ren16", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "NON RESIDENT RAND" } }
        }),
        //May not be used. NON RESIDENT RAND is only applicable for BOPCUS and NON REPORTABLE transactions.
        assertSuccess("ren17", {
          ReportingQualifier: "INTERBANK",
          Resident: { Exception: { ExceptionName: "NOSTRO INTERBANK" } }
        }),
        assertNoRule("ren17", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "NON RESIDENT RAND" } }
        }),
        assertFailure("ren17", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "NON RESIDENT RAND" } }
        }),
        assertFailure("ren17", {
          ReportingQualifier: "INTERBANK",
          Resident: { Exception: { ExceptionName: "NON RESIDENT RAND" } }
        }),
        //May not be used. UNCLAIMED DRAFTS is only applicable for BOPCUS transactions.
        assertSuccess("ren18", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertNoRule("ren18", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "UNCLAIMED DRAFTS" } }
        }),
        assertFailure("ren18", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "UNCLAIMED DRAFTS" } }
        }),
        //May not be used. BULK INTEREST is only applicable for BOPCUS transactions.
        assertSuccess("ren19", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertNoRule("ren19", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK INTEREST" } }
        }),
        assertFailure("ren19", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "BULK INTEREST" } }
        }),
        //May not be used. BULK DIVIDENDS is only applicable for BOPCUS transactions.
        assertSuccess("ren20", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertNoRule("ren20", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK DIVIDENDS" } }
        }),
        assertFailure("ren20", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "BULK DIVIDENDS" } }
        }),
        //May not be used. BULK BANK CHARGES is only applicable for BOPCUS transactions.
        assertSuccess("ren21", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertNoRule("ren21", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "BULK BANK CHARGES" } }
        }),
        assertFailure("ren21", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "BULK BANK CHARGES" } }
        }),
        //May not be used. NOSTRO INTERBANK is only applicable for INTERBANK and NON REPORTABLE transactions
        assertSuccess("ren22", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertNoRule("ren22", {
          ReportingQualifier: "INTERBANK",
          Resident: { Exception: { ExceptionName: "NOSTRO INTERBANK" } }
        }),
        assertNoRule("ren22", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "NOSTRO INTERBANK" } }
        }),
        assertFailure("ren22", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "NOSTRO INTERBANK" } }
        }),
        //May not be used. NOSTRO NON REPORTABLE is only applicable for INTERBANK and NON REPORTABLE transactions
        assertSuccess("ren23", { ReportingQualifier: "BOPCUS", Resident: { Exception: { ExceptionName: "RAND CHEQUE" } } }),
        assertNoRule("ren23", {
          ReportingQualifier: "INTERBANK",
          Resident: { Exception: { ExceptionName: "NOSTRO NON REPORTABLE" } }
        }),
        assertNoRule("ren23", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "NOSTRO NON REPORTABLE" } }
        }),
        assertFailure("ren23", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "NOSTRO NON REPORTABLE" } }
        }),
        //May not be used. RTGS NON REPORTABLE is only applicable for INTERBANK and NON REPORTABLE transactions
        assertSuccess("ren24", { ReportingQualifier: "BOPCUS", Resident: { Exception: { ExceptionName: "RAND CHEQUE" } } }),
        assertNoRule("ren24", {
          ReportingQualifier: "INTERBANK",
          Resident: { Exception: { ExceptionName: "RTGS NON REPORTABLE" } }
        }),
        assertNoRule("ren24", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "RTGS NON REPORTABLE" } }
        }),
        assertFailure("ren24", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { ExceptionName: "RTGS NON REPORTABLE" } }
        }),
        //May not be used. STRATE is only applicable for BOPCUS transactions.
        assertSuccess("ren25", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } }
        }),
        assertNoRule("ren25", { ReportingQualifier: "BOPCUS", Resident: { Exception: { ExceptionName: "STRATE" } } }),
        assertFailure("ren25", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "STRATE" } }
        }),
        //For categories 511/01 to 511/08 or 512/01 to 512/07 or 513 or 514/01 to 514/07 or 515/01 to 515/07 the Exception must not be completed
        assertSuccess("ren26", {
          ReportingQualifier: "BOPCUS", Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        assertFailure("ren26", {
          ReportingQualifier: "BOPCUS", Resident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE" } },
          MonetaryAmount: [{ CategoryCode: "513" }]
        }),
        //If the Subject under the MonetaryDetails is REMITTANCE DISPENSATION and category 251 then MUTUAL PARTY may not be used
        assertFailure("ren27", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [
            {
              AdHocRequirement: {
                Subject: "REMITTANCE DISPENSATION"
              },
              CategoryCode: "251"
            }]
        }),
        assertNoRule("ren27", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [
            {
              AdHocRequirement: {
                Subject: "REMITTANCE DISPENSATION"
              },
              CategoryCode: "251"
            }]
        }),
        assertSuccess("ren27", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Exception: { ExceptionName: "MUTUAL PARTY" } },
          MonetaryAmount: [
            {
              AdHocRequirement: {
                Subject: "sdfsdf"
              },
              CategoryCode: "251"
            }]
        }),

        //check for case sensitive characters
        assertSuccess("ren28", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Exception: { ExceptionName: "UPPER CASE" } }
        }),
        assertSuccess("ren28", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          Resident: { Exception: { ExceptionName: "UPPER CASE" } }
        }),
        assertFailure("ren28", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Exception: { ExceptionName: "Mixed Case" } }
        }),
        assertFailure("ren28", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          Resident: { Exception: { ExceptionName: "Mixed Case" } }
        }),
        assertFailure("ren28", {
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: { Exception: { ExceptionName: "lower case" } }
        }),
        assertFailure("ren28", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          Resident: { Exception: { ExceptionName: "lower case" } }
        }),
        assertFailure("ren28", {
          ReportingQualifier: "BOPCUS",
          Flow: "IN",
          Resident: { Exception: { ExceptionName: "ALPHANUMERIC1" } }
        }),

        //Resident.Exception.Country
        assertSuccess("rec1", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { Country: "sfsd" } }
        }),
        assertSuccess("rec1", { ReportingQualifier: "INTERBANK", Resident: { Exception: { Country: "sd" } } }),
        assertNoRule("rec1", { ReportingQualifier: "BOPCUS", Resident: { Exception: { Country: "sf" } } }),
        assertFailure("rec1", { ReportingQualifier: "NON REPORTABLE", Resident: { Exception: { Country: "" } } }),
        assertFailure("rec1", { ReportingQualifier: "INTERBANK", Resident: { Exception: { Country: "" } } }),
        assertSuccess("rec2", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE", Country: "BG" } }
        }),
        assertSuccess("rec2", {
          ReportingQualifier: "INTERBANK",
          Resident: { Exception: { ExceptionName: "VOSTRO INTERBANK", Country: "BG" } }
        }),
        assertNoRule("rec2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Exception: { Country: "sfsd" } }
        }),
        assertFailure("rec2", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Exception: { ExceptionName: "VOSTRO NON REPORTABLE", Country: "ZA" } }
        }),
        assertFailure("rec2", {
          ReportingQualifier: "INTERBANK",
          Resident: { Exception: { ExceptionName: "VOSTRO INTERBANK", Country: "ZA" } }
        }),

        assertSuccess("rec3", { ReportingQualifier: "NON REPORTABLE", Resident: { Exception: { Country: "ZA" } } }),
        assertSuccess("rec3", { ReportingQualifier: "INTERBANK", Resident: { Exception: { Country: "AD" } } }),
        assertNoRule("rec3", { ReportingQualifier: "BOPCUS", Resident: { Exception: { Country: "AD" } } }),
        assertFailure("rec3", { ReportingQualifier: "NON REPORTABLE", Resident: { Exception: { Country: "ZZ" } } }),
        assertFailure("rec3", { ReportingQualifier: "INTERBANK", Resident: { Exception: { Country: "AY" } } }),

        assertSuccess("rec4", { ReportingQualifier: "BOPCUS", Resident: { Exception: { Country: "" } } }),
        assertNoRule("rec4", { ReportingQualifier: "NON REPORTABLE", Resident: { Exception: { Country: "" } } }),
        assertFailure("rec4", { ReportingQualifier: "BOPCUS", Resident: { Exception: { Country: "ZA" } } }),

        //Resident.Individual
        assertSuccess("g1:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { ContactDetails: { Email: "slkdfj" } } }
        }),
        assertSuccess("g1:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { ContactDetails: { Fax: "0123456789" } } }
        }),
        assertSuccess("g1:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { ContactDetails: { Telephone: "0123456789" } } }
        }),
        assertSuccess("g1:1", {
          ReportingQualifier: "BOPCUS",
          Resident: {
            Individual: {
              ContactDetails: {
                Email: "slkdfj@lf.cl.d",
                Fax: "2983742398",
                Telephone: "283472438"
              }
            }
          }
        }),
        assertNoRule("g1:1", { ReportingQualifier: "BOPCARD NON RESIDENT", Resident: { Individual: {} } }),
        assertFailure("g1:1", { ReportingQualifier: "BOPCUS", Resident: { Individual: {} } }),

        //Resident.Entity
        assertSuccess("g1:2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { ContactDetails: { Email: "slkdfj@lf.cl.d" } } }
        }),
        assertSuccess("g1:2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { ContactDetails: { Fax: "0123456789" } } }
        }),
        assertSuccess("g1:2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { ContactDetails: { Telephone: "0123456789" } } }
        }),
        assertSuccess("g1:2", {
          ReportingQualifier: "BOPCUS",
          Resident: {
            Entity: {
              ContactDetails: {
                Email: "slkdfj@lf.cl.d",
                Fax: "2983742398",
                Telephone: "283472438"
              }
            }
          }
        }),
        assertNoRule("g1:2", { ReportingQualifier: "BOPCARD NON RESIDENT", Resident: { Entity: {} } }),
        assertFailure("g1:2", { ReportingQualifier: "BOPCUS", Resident: { Entity: {} } }),

        //Resident.Individual.AccountName
        assertSuccess("an1:1", { ReportingQualifier: "BOPCUS", Resident: { Individual: { AccountName: "slkfj" } } }),
        assertNoRule("an1:1", { ReportingQualifier: "NON REPORTABLE", Resident: { Individual: { AccountName: "slkfj" } } }),
        assertNoRule("an1:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountName: "slkfj", AccountIdentifier: "CASH" } }
        }),
        assertFailure("an1:1", { ReportingQualifier: "BOPCUS", Resident: { Individual: { AccountName: "" } } }),

        //Resident.Entity.AccountName
        assertSuccess("an1:2", { ReportingQualifier: "BOPCUS", Resident: { Entity: { AccountName: "slkfj" } } }),
        assertNoRule("an1:2", { ReportingQualifier: "NON REPORTABLE", Resident: { Entity: { AccountName: "slkfj" } } }),
        assertNoRule("an1:2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { AccountName: "slkfj", AccountIdentifier: "CASH" } }
        }),
        assertFailure("an1:2", { ReportingQualifier: "BOPCUS", Resident: { Entity: { AccountName: "" } } }),

        //Resident.Individual.AccountIdentifier
        assertSuccess("accid1:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountIdentifier: "RESIDENT OTHER" } }
        }),
        assertSuccess("accid1:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountIdentifier: "CFC RESIDENT" } }
        }),
        assertSuccess("accid1:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountIdentifier: "FCA RESIDENT" } }
        }),
        assertSuccess("accid1:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountIdentifier: "CASH" } }
        }),
        assertNoRule("accid1:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { AccountIdentifier: "CASH" } }
        }),
        assertFailure("accid1:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountIdentifier: "VOSTRO" } }
        }),
        assertFailure("accid1:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountIdentifier: "NON RESIDENT RAND" } }
        }),

        assertSuccess("accid2:1", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Individual: { AccountIdentifier: "RESIDENT OTHER" } }
        }),
        assertSuccess("accid2:1", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Individual: { AccountIdentifier: "CFC RESIDENT" } }
        }),
        assertSuccess("accid2:1", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Individual: { AccountIdentifier: "FCA RESIDENT" } }
        }),
        assertSuccess("accid2:1", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Individual: { AccountIdentifier: "CASH" } }
        }),
        assertSuccess("accid2:1", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Individual: { AccountIdentifier: "VOSTRO" } }
        }),
        assertNoRule("accid2:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { AccountIdentifier: "CASH" } }
        }),
        assertFailure("accid2:1", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Individual: { AccountIdentifier: "DEBIT CARD" } }
        }),

        assertSuccess("accid3:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { AccountIdentifier: "" } }
        }),
        assertNoRule("accid3:1", { ReportingQualifier: "BOPCUS", Resident: { Individual: { AccountIdentifier: "" } } }),
        assertFailure("accid3:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { AccountIdentifier: "VOSTRO" } }
        }),

        assertSuccess("accid4:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { AccountIdentifier: "DEBIT CARD" } }
        }),
        assertSuccess("accid4:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { AccountIdentifier: "CREDIT CARD" } }
        }),
        assertNoRule("accid4:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { AccountIdentifier: "DEBIT CARD" } }
        }),
        assertFailure("accid4:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { AccountIdentifier: "CASH" } }
        }),
        assertFailure("accid4:1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { AccountIdentifier: "" } }
        }),

        assertSuccess("accid5:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountIdentifier: "DEBIT CARD" } }
        }),
        assertNoRule("accid5:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { AccountIdentifier: "DEBIT CARD" } }
        }),
        assertFailure("accid5:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountIdentifier: "DEBCARD" } }
        }),
        assertFailure("accid5:1", { ReportingQualifier: "BOPCUS", Resident: { Individual: { AccountIdentifier: "" } } }),

        assertSuccess("accid6:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountIdentifier: "CFC RESIDENT" } },
          FlowCurrency: "CAD"
        }),
        assertNoRule("accid6:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { AccountIdentifier: "DEBIT CARD" } }
        }),
        assertFailure("accid6:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountIdentifier: "CFC RESIDENT" } },
          FlowCurrency: "ZAR"
        }),

        assertSuccess("accid7:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountIdentifier: "VOSTRO" } },
          FlowCurrency: "ZAR"
        }),
        assertNoRule("accid7:1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { AccountIdentifier: "DEBIT CARD" } }
        }),
        assertFailure("accid7:1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountIdentifier: "VOSTRO" } },
          FlowCurrency: "USD"
        }),

        // The Account Identifier cannot be 'FCA RESIDENT' for ZAR transactions
        //          assertSuccess("accid8:1", {
        //            ReportingQualifier: "BOPCUS",
        //            Resident: {Individual: {AccountIdentifier: "FCA RESIDENT"}},
        //            FlowCurrency: "USD"
        //          }),
        //          assertFailure("accid8:1", {
        //            ReportingQualifier: "BOPCUS",
        //            Resident: {Individual: {AccountIdentifier: "FCA RESIDENT"}},
        //            FlowCurrency: "ZAR"
        //          }),

        //Resident.Entity.AccountIdentifier
        assertSuccess("accid1:2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { AccountIdentifier: "RESIDENT OTHER" } }
        }),
        assertSuccess("accid1:2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { AccountIdentifier: "CFC RESIDENT" } }
        }),
        assertSuccess("accid1:2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { AccountIdentifier: "FCA RESIDENT" } }
        }),
        assertSuccess("accid1:2", { ReportingQualifier: "BOPCUS", Resident: { Entity: { AccountIdentifier: "CASH" } } }),
        assertNoRule("accid1:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { AccountIdentifier: "CASH" } }
        }),
        assertFailure("accid1:2", { ReportingQualifier: "BOPCUS", Resident: { Entity: { AccountIdentifier: "VOSTRO" } } }),
        assertFailure("accid1:2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { AccountIdentifier: "NON RESIDENT RAND" } }
        }),

        assertSuccess("accid2:2", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Entity: { AccountIdentifier: "RESIDENT OTHER" } }
        }),
        assertSuccess("accid2:2", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Entity: { AccountIdentifier: "CFC RESIDENT" } }
        }),
        assertSuccess("accid2:2", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Entity: { AccountIdentifier: "FCA RESIDENT" } }
        }),
        assertSuccess("accid2:2", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Entity: { AccountIdentifier: "CASH" } }
        }),
        assertSuccess("accid2:2", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Entity: { AccountIdentifier: "VOSTRO" } }
        }),
        assertNoRule("accid2:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { AccountIdentifier: "CASH" } }
        }),
        assertFailure("accid2:2", {
          ReportingQualifier: "NON REPORTABLE",
          Resident: { Entity: { AccountIdentifier: "DEBIT CARD" } }
        }),

        assertSuccess("accid3:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { AccountIdentifier: "" } }
        }),
        assertNoRule("accid3:2", { ReportingQualifier: "BOPCUS", Resident: { Entity: { AccountIdentifier: "" } } }),
        assertFailure("accid3:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { AccountIdentifier: "VOSTRO" } }
        }),

        assertSuccess("accid4:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Entity: { AccountIdentifier: "DEBIT CARD" } }
        }),
        assertSuccess("accid4:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Entity: { AccountIdentifier: "CREDIT CARD" } }
        }),
        assertNoRule("accid4:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { AccountIdentifier: "DEBIT CARD" } }
        }),
        assertFailure("accid4:2", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Entity: { AccountIdentifier: "CASH" } }
        }),

        assertSuccess("accid5:2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { AccountIdentifier: "DEBIT CARD" } }
        }),
        assertNoRule("accid5:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { AccountIdentifier: "DEBIT CARD" } }
        }),
        assertFailure("accid5:2", { ReportingQualifier: "BOPCUS", Resident: { Entity: { AccountIdentifier: "DEBCARD" } } }),

        assertSuccess("accid6:2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { AccountIdentifier: "CFC RESIDENT" } },
          FlowCurrency: "CAD"
        }),
        assertNoRule("accid6:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { AccountIdentifier: "DEBIT CARD" } }
        }),
        assertFailure("accid6:2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { AccountIdentifier: "CFC RESIDENT" } },
          FlowCurrency: "ZAR"
        }),

        assertSuccess("accid7:2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { AccountIdentifier: "VOSTRO" } },
          FlowCurrency: "ZAR"
        }),
        assertNoRule("accid7:2", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { AccountIdentifier: "DEBIT CARD" } }
        }),
        assertFailure("accid7:2", {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { AccountIdentifier: "VOSTRO" } },
          FlowCurrency: "USD"
        }),
        assertSuccess("eaccid1", {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Entity: { RegistrationNumber: "2013/1234567/07", AccountIdentifier: "FCA RESIDENT" }
          }
        }),
        assertSuccess("eaccid1", {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Entity: { RegistrationNumber: "2099/1231234/99", AccountIdentifier: "Not FCA RESIDENT" }
          }
        }),
        assertFailure("eaccid1", {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Entity: { RegistrationNumber: "2013/1234567/07", AccountIdentifier: "Not FCA RESIDENT" }
          }
        }),
        assertSuccess("eaccid1", {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Entity: { RegistrationNumber: "2013/1234567/07", AccountIdentifier: "Not FCA RESIDENT" }
          },
          NonResident: { Exception: { ExceptionName: "IHQ" } }
        }),
        assertSuccess("eaccid1", {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Entity: { RegistrationNumber: "2013/1234567/07", AccountIdentifier: "FCA RESIDENT" }
          }
        }),
        assertFailure("eaccid1", {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Entity: { RegistrationNumber: "2013/1234567/07", AccountIdentifier: "Not FCA RESIDENT" }
          }
        }),


        //Resident.Individual.AccountNumber

        assertFailure('accno1:1', {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { AccountNumber: "" } }
        }),
        assertSuccess('accno1:1', {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { AccountNumber: "1234" } }
        }),
        assertFailure('accno2:1', {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { AccountNumber: "1234" } }
        }),
        assertSuccess('accno2:1', {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Individual: { AccountNumber: "" } }
        }),
        assertFailure('accno3:1', {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountNumber: "1234" } },
          NonResident: { Entity: { AccountNumber: "1234" } }
        }),
        assertFailure('accno3:1', {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountNumber: "1234" } },
          NonResident: { Individual: { AccountNumber: "1234" } }
        }),
        assertSuccess('accno3:1', {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountNumber: "1234" } },
          NonResident: { Entity: { AccountNumber: "4321" } }
        }),
        assertSuccess('accno3:1', {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { AccountNumber: "1234" } },
          NonResident: { Individual: { AccountNumber: "4321" } }
        }),
        assertFailure('accno4:1', {
          ReportingQualifier: "BOPCUS",
          Flow: 'OUT',
          Resident: { Individual: { AccountNumber: "", AccountIdentifier: "CREDIT CARD" } }
        }),
        assertSuccess('accno4:1', {
          ReportingQualifier: "BOPCUS",
          Flow: 'OUT',
          Resident: {
            Individual: {
              AccountNumber: "234234",
              AccountIdentifier: "CREDIT CARD"
            }
          }
        }),
        assertNoRule('accno4:1', {
          ReportingQualifier: "BOPCUS",
          Flow: 'IN',
          Resident: { Individual: { AccountNumber: "1234", AccountIdentifier: "CASH" } }
        }),


        //Resident.Entity.AccountNumber
        assertFailure('accno1:2', { ReportingQualifier: "BOPCARD RESIDENT", Resident: { Entity: { AccountNumber: "" } } }),
        assertSuccess('accno1:2', {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Entity: { AccountNumber: "1234" } }
        }),
        assertFailure('accno2:2', {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { AccountNumber: "1234" } }
        }),
        assertSuccess('accno2:2', {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { AccountNumber: "" } }
        }),
        assertFailure('accno3:2', {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { AccountNumber: "1234" } },
          NonResident: { Entity: { AccountNumber: "1234" } }
        }),
        assertFailure('accno3:2', {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { AccountNumber: "1234" } },
          NonResident: { Individual: { AccountNumber: "1234" } }
        }),
        assertSuccess('accno3:2', {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { AccountNumber: "1234" } },
          NonResident: { Entity: { AccountNumber: "4321" } }
        }),
        assertSuccess('accno3:2', {
          ReportingQualifier: "BOPCUS",
          Resident: { Entity: { AccountNumber: "1234" } },
          NonResident: { Individual: { AccountNumber: "4321" } }
        }),
        assertFailure('accno4:2', {
          ReportingQualifier: "BOPCUS",
          Flow: 'OUT',
          Resident: { Entity: { AccountNumber: "", AccountIdentifier: "CREDIT CARD" } }
        }),
        assertSuccess('accno4:2', {
          ReportingQualifier: "BOPCUS",
          Flow: 'OUT',
          Resident: {
            Entity: {
              AccountNumber: "234234",
              AccountIdentifier: "CREDIT CARD"
            }
          }
        }),
        assertNoRule('accno4:2', {
          ReportingQualifier: "BOPCUS",
          Flow: 'IN',
          Resident: { Entity: { AccountNumber: "", AccountIdentifier: "CASH" } }
        }),

        //Resident.Individual.CustomsClientNumber

        assertSuccess('ccn1:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { CustomsClientNumber: '1234' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertNoRule('ccn1:1', {
          ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'IN', Resident: { Individual: { CustomsClientNumber: '1234' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertFailure('ccn1:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { CustomsClientNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertNoRule('ccn1:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { CustomsClientNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '11' }]
        }),

        assertSuccess('ccn2:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '1234' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertFailure('ccn2:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertNoRule('ccn2:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '11' }]
        }),

        assertSuccess('ccn3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { CustomsClientNumber: '00281124' } }, // ccn
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { CustomsClientNumber: '0254089063' } }, //tax number
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { CustomsClientNumber: '6811035039084' } }, // id number
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn3:1', {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          Resident: { Individual: { CustomsClientNumber: '70707070'/*'00281125'*/ } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertFailure('ccn3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { CustomsClientNumber: 't89554354' } }, // letter
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertFailure('ccn3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { CustomsClientNumber: '5686' } }, // too short
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertFailure('ccn3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { CustomsClientNumber: '569854567898587686' } }, // too long
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),

        assertSuccess('ccn4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '20265566' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '0254089063' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '6811035039084' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertFailure('ccn4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: 't89554354' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertFailure('ccn4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '5686' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertFailure('ccn4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '569854567898587686' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),

        assertSuccess('ccn5:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { CustomsClientNumber: '70707070' } },
          MonetaryAmount: [{ RandValue: '49999.00' }]
        }),
        assertWarning('ccn5:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { CustomsClientNumber: '70707070' } },
          MonetaryAmount: [{ RandValue: '50001.00' }]
        }),

        assertFailure('ccn6:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { CustomsClientNumber: '1234' } }
        }),

        assertNoRule('ccn7:1', {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { CustomsClientNumber: '1234' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' }]
        }),
        assertWarning('ccn7:1', {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { CustomsClientNumber: '1234' } },
          MonetaryAmount: [{ CategoryCode: '203' }]
        }),
        assertSuccess('ccn7:1', {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { CustomsClientNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '203' }]
        }),

        //Resident.Entity.CustomsClientNumber

        assertSuccess('ccn1:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { CustomsClientNumber: '1234' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertNoRule('ccn1:2', {
          ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'IN', Resident: { Entity: { CustomsClientNumber: '1234' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertFailure('ccn1:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { CustomsClientNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertNoRule('ccn1:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { CustomsClientNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '11' }]
        }),

        assertSuccess('ccn2:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { CustomsClientNumber: '1234' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertFailure('ccn2:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { CustomsClientNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertNoRule('ccn2:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { CustomsClientNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '11' }]
        }),

        assertSuccess('ccn3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { CustomsClientNumber: '00281124' } }, // ccn
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { CustomsClientNumber: '0254089063' } }, //tax number
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { CustomsClientNumber: '6811035039084' } }, // id number
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn3:2', {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          Resident: { Entity: { CustomsClientNumber: '70707070'/*'00281125'*/ } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertFailure('ccn3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { CustomsClientNumber: 't89554354' } }, // letter
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertFailure('ccn3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { CustomsClientNumber: '5686' } }, // too short
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertFailure('ccn3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { CustomsClientNumber: '569854567898587686' } }, // too long
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),


        assertSuccess('ccn4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { CustomsClientNumber: '20265566' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { CustomsClientNumber: '0254089063' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { CustomsClientNumber: '6811035039084' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertFailure('ccn4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { CustomsClientNumber: 't89554354' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertFailure('ccn4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { CustomsClientNumber: '5686' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertFailure('ccn4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { CustomsClientNumber: '569854567898587686' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),


        assertSuccess('ccn5:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { CustomsClientNumber: '70707070' } },
          MonetaryAmount: [{ RandValue: '49999.00' }]
        }),
        assertWarning('ccn5:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { CustomsClientNumber: '70707070' } },
          MonetaryAmount: [{ RandValue: '50001.00' }]
        }),

        assertFailure('ccn6:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { CustomsClientNumber: '1234' } }
        }),

        assertNoRule('ccn7:2', {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { CustomsClientNumber: '1234' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' }]
        }),
        assertWarning('ccn7:2', {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { CustomsClientNumber: '1234' } },
          MonetaryAmount: [{ CategoryCode: '203' }]
        }),
        assertSuccess('ccn7:2', {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { CustomsClientNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '203' }]
        }),


        //Unless the category is 101/01 to 101/10 or 102/01 to 102/10 or 103/01 to 103/10 or 104/01 to 104/10 or 105 or 106,
        // this need not be provided and if invalid will cause the SARB to reject transaction
        assertWarning('ccn9:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '01126225' } },
          MonetaryAmount: [{ CategoryCode: '401' },
          { CategoryCode: '407' }]
        }),
        assertSuccess('ccn9:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '01126225' } },
          MonetaryAmount: [{ CategoryCode: '401' },
          { CategoryCode: '101' }]
        }),
        assertSuccess('ccn9:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '20910076' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),

        assertSuccess('ccn10:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { CustomsClientNumber: '01126225' } }, // valid ccn
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn10:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { CustomsClientNumber: '02540/89063' } }, // valid tax
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn10:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { CustomsClientNumber: '6811035039084' } }, // valid id
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertWarning('ccn10:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: { CustomsClientNumber: '20910076' } }, // invalid ccn
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),

        assertSuccess('ccn11:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '01126225' } }, // valid ccn
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn11:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '0254089063' } }, // valid tax
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn11:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '6811035039084' } }, // valid id
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertWarning('ccn11:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '20910076' } }, // invalid ccn
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),

        assertFailure('ccn12:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { CustomsClientNumber: '70707070' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),

        assertSuccess('ccn10:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { CustomsClientNumber: '01126225' } }, // valid ccn
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn10:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { CustomsClientNumber: '0254089063' } }, // valid tax
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn10:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { CustomsClientNumber: '6811035039084' } }, // valid id
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertWarning('ccn10:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { CustomsClientNumber: '20910076' } }, // invalid ccn
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),

        assertSuccess('ccn11:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { CustomsClientNumber: '01126225' } }, // valid ccn
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn11:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { CustomsClientNumber: '0254089063' } }, // valid tax
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('ccn11:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { CustomsClientNumber: '6811035039084' } }, // valid id
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertWarning('ccn11:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { CustomsClientNumber: '20910076' } }, // invalid ccn
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),

        //Resident.Individual.TaxNumber
        // Must be completed if Flow is OUT and category is 512/01 to 512/07 or 513
        assertSuccess('tni1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { TaxNumber: '1234' } },
          MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '06' },
          { CategoryCode: '512', CategorySubCode: '07' }]
        }),
        assertFailure('tni1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { TaxNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '06' },
          { CategoryCode: '512', CategorySubCode: '07' }]
        }),
        assertNoRule('tni1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { TaxNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '11' }]
        }),

        assertSuccess('tni2', { ReportingQualifier: 'BOPCARD NON RESIDENT', Resident: { Individual: { TaxNumber: '' } } }),
        assertFailure('tni2', {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          Resident: { Individual: { TaxNumber: '123' } }
        }),


        //Resident.Entity.TaxNumber
        // Must be completed if category is 101/01 to 101/10 or 102/01 to 102/10 or 103/01 to 103/10 or 104/01 to 104/10 or 105 or 106
        assertSuccess('tne1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { TaxNumber: '1234' } },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '06' },
          { CategoryCode: '101', CategorySubCode: '07' }]
        }),
        assertFailure('tne1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { TaxNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '06' },
          { CategoryCode: '101', CategorySubCode: '07' }]
        }),
        assertNoRule('tne1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { TaxNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '11' }]
        }),

        assertSuccess('tne2', { ReportingQualifier: 'BOPCARD NON RESIDENT', Resident: { Entity: { TaxNumber: '' } } }),
        assertFailure('tne2', { ReportingQualifier: 'BOPCARD NON RESIDENT', Resident: { Entity: { TaxNumber: '123' } } }),

        assertSuccess('tn1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { TaxNumber: '0254/089/06/3' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '01' }]
        }),
        assertWarning('tn1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { TaxNumber: '0254/089/06/6' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '01' }]
        }),
        assertWarning('tn1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { TaxNumber: '0254/089/06/6' } },
          MonetaryAmount: [{ CategoryCode: '401' }]
        }),
        assertWarning('tn1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { TaxNumber: '5878899529' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '01' }]
        }),

        assertSuccess('tn1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { TaxNumber: '0254/089/06/3' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '01' }]
        }),
        assertWarning('tn1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { TaxNumber: '0254/089/06/6' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '01' }]
        }),
        assertWarning('tn1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { TaxNumber: '0254/089/06/6' } },
          MonetaryAmount: [{ CategoryCode: '401' }]
        }),
        assertWarning('tn1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { TaxNumber: '5878899529' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '01' }]
        }),
        //Resident.Individual.VATNumber

        assertSuccess('vni2', { ReportingQualifier: 'BOPCARD NON RESIDENT', Resident: { Individual: { VATNumber: '' } } }),
        assertFailure('vni2', {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          Resident: { Individual: { VATNumber: '123' } }
        }),


        //Resident.Entity.VATNumber
        // Must be completed or have "NO VAT NUMBER" if category is 101/01 to 101/10 or 102/01 to 102/10 or 103/01 to 103/10 or 104/01 to 104/10 or 105 or 106

        assertSuccess('vne1', {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { VATNumber: '1234' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertSuccess('vne1', {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { VATNumber: 'NO VAT NUMBER' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '101', CategorySubCode: '10' }]
        }),
        assertFailure('vne1', {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { VATNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '10' }]
        }),
        assertNoRule('vne1', {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { VATNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '11' }]
        }),

        assertSuccess('vne2', { ReportingQualifier: 'BOPCARD NON RESIDENT', Resident: { Entity: { VATNumber: '' } } }),
        assertFailure('vne2', { ReportingQualifier: 'BOPCARD NON RESIDENT', Resident: { Entity: { VATNumber: '123' } } }),

        // VAT number warning.
        assertSuccess('vn1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { VATNumber: '4090103146' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '01' }]
        }),
        assertWarning('vn1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { VATNumber: '0254089066' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '01' }]
        }),
        assertWarning('vn1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { VATNumber: '0254089066' } },
          MonetaryAmount: [{ CategoryCode: '401' }]
        }),
        assertWarning('vn1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { VATNumber: '5878899529' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '01' }]
        }),


        assertSuccess('vn1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { VATNumber: '4090103146' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '01' }]
        }),
        assertWarning('vn1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { VATNumber: '0254089066' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '01' }]
        }),
        assertWarning('vn1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { VATNumber: '0254089066' } },
          MonetaryAmount: [{ CategoryCode: '401' }]
        }),
        assertWarning('vn1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { VATNumber: '5878899529' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '01' }]
        }),

        // Resident.Individual.TAXClearanceCertificateIndicator

        assertSuccess('tcci1:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { TaxClearanceCertificateIndicator: 'N' } },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '06' },
          { CategoryCode: '512', CategorySubCode: '07' }]
        }),
        assertFailure('tcci1:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { TaxClearanceCertificateIndicator: 'o' } },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '06' },
          { CategoryCode: '512', CategorySubCode: '07' }]
        }),
        assertFailure('tcci1:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { TaxClearanceCertificateIndicator: '' } },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '06' },
          { CategoryCode: '512', CategorySubCode: '07' }]
        }),
        assertNoRule('tcci1:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { TaxClearanceCertificateIndicator: '' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '11' }]
        }),

        // Resident.Entity.TAXClearanceCertificateIndicator


        assertSuccess('tcci1:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { TaxClearanceCertificateIndicator: 'N' } },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '06' },
          { CategoryCode: '512', CategorySubCode: '07' }]
        }),
        assertFailure('tcci1:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { TaxClearanceCertificateIndicator: 'O' } },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '06' },
          { CategoryCode: '512', CategorySubCode: '07' }]
        }),
        assertFailure('tcci1:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { TaxClearanceCertificateIndicator: '' } },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '06' },
          { CategoryCode: '512', CategorySubCode: '07' }]
        }),
        assertNoRule('tcci1:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { TaxClearanceCertificateIndicator: '' } },
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' },
          { CategoryCode: '103', CategorySubCode: '11' }]
        }),
        assertSuccess('tcci2:1', {
          ReportingQualifier: 'BOPCARD NON RESIDENT'
        }),
        assertFailure('tcci2:1', {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          Resident: {
            Individual: {
              TaxClearanceCertificateIndicator: 'Y',
              TaxClearanceCertificateReference: '1234'
            }
          }
        }),



        // Resident.Individual.TAXClearanceCertificateReference

        assertSuccess('tccr1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Individual: {
              TaxClearanceCertificateIndicator: 'Y',
              TaxClearanceCertificateReference: '1234'
            }
          }
        }),
        assertFailure('tccr1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Individual: {
              TaxClearanceCertificateIndicator: 'Y',
              TaxClearanceCertificateReference: ''
            }
          }
        }),
        assertNoRule('tccr1:1', {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          Resident: {
            Individual: {
              TaxClearanceCertificateIndicator: 'Y',
              TaxClearanceCertificateReference: ''
            }
          }
        }),
        assertFailure('tccr2:1', {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          Resident: {
            Individual: {
              TaxClearanceCertificateIndicator: 'Y',
              TaxClearanceCertificateReference: '1234'
            }
          }
        }),


        // Resident.Entity.TAXClearanceCertificateReference

        assertSuccess('tccr1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Entity: {
              TaxClearanceCertificateIndicator: 'Y',
              TaxClearanceCertificateReference: '1234'
            }
          }
        }),
        assertFailure('tccr1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Entity: {
              TaxClearanceCertificateIndicator: 'Y',
              TaxClearanceCertificateReference: ''
            }
          }
        }),
        assertNoRule('tccr1:2', {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          Resident: {
            Entity: {
              TaxClearanceCertificateIndicator: 'Y',
              TaxClearanceCertificateReference: ''
            }
          }
        }),
        assertFailure('tccr2:2', {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          Resident: {
            Entity: {
              TaxClearanceCertificateIndicator: 'Y',
              TaxClearanceCertificateReference: '1234'
            }
          }
        }),

        // Resident.Individual.StreetAddress.AddressLine1
        assertSuccess('a1_1:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { StreetAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertFailure('a1_1:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { StreetAddress: { AddressLine1: '' } } }
        }),
        assertSuccess('a1_2:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { StreetAddress: { AddressLine1: '' } } }
        }),
        assertFailure('a1_2:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { StreetAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertSuccess('a1_3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { StreetAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertFailure('a1_3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { StreetAddress: { AddressLine1: '' } } }
        }),
        assertSuccess('a1_3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { StreetAddress: { AddressLine1: '12 Foo Street' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('a1_3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { StreetAddress: { AddressLine1: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('a1_4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { StreetAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertFailure('a1_4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { StreetAddress: { AddressLine1: '' } } }
        }),
        assertSuccess('a1_4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { StreetAddress: { AddressLine1: '12 Foo Street' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('a1_4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { StreetAddress: { AddressLine1: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // Resident.Entity.StreetAddress.AddressLine1
        assertSuccess('a1_1:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { StreetAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertFailure('a1_1:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { StreetAddress: { AddressLine1: '' } } }
        }),
        assertSuccess('a1_2:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { StreetAddress: { AddressLine1: '' } } }
        }),
        assertFailure('a1_2:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { StreetAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertSuccess('a1_3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { StreetAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertFailure('a1_3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { StreetAddress: { AddressLine1: '' } } }
        }),
        assertSuccess('a1_3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { StreetAddress: { AddressLine1: '12 Foo Street' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('a1_3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { StreetAddress: { AddressLine1: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('a1_4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { StreetAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertFailure('a1_4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { StreetAddress: { AddressLine1: '' } } }
        }),
        assertSuccess('a1_4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { StreetAddress: { AddressLine1: '12 Foo Street' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('a1_4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { StreetAddress: { AddressLine1: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // Resident.Individual.PostalAddress.AddressLine1
        assertSuccess('a1_1:3', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { PostalAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertFailure('a1_1:3', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { PostalAddress: { AddressLine1: '' } } }
        }),
        assertSuccess('a1_2:3', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { PostalAddress: { AddressLine1: '' } } }
        }),
        assertFailure('a1_2:3', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { PostalAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertSuccess('a1_3:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertFailure('a1_3:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { AddressLine1: '' } } }
        }),
        assertSuccess('a1_3:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { AddressLine1: '12 Foo Street' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('a1_3:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { AddressLine1: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('a1_4:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertFailure('a1_4:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { AddressLine1: '' } } }
        }),
        assertSuccess('a1_4:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { AddressLine1: '12 Foo Street' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('a1_4:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { AddressLine1: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // Resident.Entity.PostalAddress.AddressLine1
        assertSuccess('a1_1:4', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertFailure('a1_1:4', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { AddressLine1: '' } } }
        }),
        assertSuccess('a1_2:4', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { PostalAddress: { AddressLine1: '' } } }
        }),
        assertFailure('a1_2:4', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { PostalAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertSuccess('a1_3:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertFailure('a1_3:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { AddressLine1: '' } } }
        }),
        assertSuccess('a1_3:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { AddressLine1: '12 Foo Street' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('a1_3:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { AddressLine1: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('a1_4:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { AddressLine1: '12 Foo Street' } } }
        }),
        assertFailure('a1_4:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { AddressLine1: '' } } }
        }),
        assertSuccess('a1_4:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { AddressLine1: '12 Foo Street' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('a1_4:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { AddressLine1: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // Resident.Individual.StreetAddress.AddressLine2
        assertSuccess('a2_1:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { StreetAddress: { AddressLine2: '' } } }
        }),
        assertFailure('a2_1:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { StreetAddress: { AddressLine2: '12 Foo Street' } } }
        }),

        // Resident.Entity.StreetAddress.AddressLine2
        assertSuccess('a2_1:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { StreetAddress: { AddressLine2: '' } } }
        }),
        assertFailure('a2_1:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { StreetAddress: { AddressLine2: '12 Foo Street' } } }
        }),

        // Resident.Individual.PostalAddress.AddressLine2
        assertSuccess('a2_1:3', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { PostalAddress: { AddressLine2: '' } } }
        }),
        assertFailure('a2_1:3', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { PostalAddress: { AddressLine2: '12 Foo Street' } } }
        }),

        // Resident.Entity.PostalAddress.AddressLine2
        assertSuccess('a2_1:4', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { PostalAddress: { AddressLine2: '' } } }
        }),
        assertFailure('a2_1:4', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { PostalAddress: { AddressLine2: '12 Foo Street' } } }
        }),

        // Resident.Individual.StreetAddress.Suburb
        assertSuccess('s1:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { StreetAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertFailure('s1:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { StreetAddress: { Suburb: '' } } }
        }),
        assertSuccess('s2:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { StreetAddress: { Suburb: '' } } }
        }),
        assertFailure('s2:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { StreetAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertSuccess('s3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { StreetAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertFailure('s3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { StreetAddress: { Suburb: '' } } }
        }),
        assertSuccess('s3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { StreetAddress: { Suburb: 'MoeggoeVille' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('s3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { StreetAddress: { Suburb: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('s4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { StreetAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertFailure('s4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { StreetAddress: { Suburb: '' } } }
        }),
        assertSuccess('s4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { StreetAddress: { Suburb: 'MoeggoeVille' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('s4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { StreetAddress: { Suburb: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // Resident.Entity.StreetAddress.Suburb
        assertSuccess('s1:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { StreetAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertFailure('s1:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { StreetAddress: { Suburb: '' } } }
        }),
        assertSuccess('s2:2', { ReportingQualifier: 'INTERBANK', Resident: { Entity: { StreetAddress: { Suburb: '' } } } }),
        assertFailure('s2:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { StreetAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertSuccess('s3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { StreetAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertFailure('s3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { StreetAddress: { Suburb: '' } } }
        }),
        assertSuccess('s3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { StreetAddress: { Suburb: 'MoeggoeVille' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('s3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { StreetAddress: { Suburb: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('s4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { StreetAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertFailure('s4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { StreetAddress: { Suburb: '' } } }
        }),
        assertSuccess('s4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { StreetAddress: { Suburb: 'MoeggoeVille' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('s4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { StreetAddress: { Suburb: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // Resident.Individual.PostalAddress.Suburb
        assertSuccess('s1:3', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { PostalAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertFailure('s1:3', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { PostalAddress: { Suburb: '' } } }
        }),
        assertSuccess('s2:3', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { PostalAddress: { Suburb: '' } } }
        }),
        assertFailure('s2:3', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { PostalAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertSuccess('s3:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertFailure('s3:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { Suburb: '' } } }
        }),
        assertSuccess('s3:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { Suburb: 'MoeggoeVille' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('s3:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { Suburb: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('s4:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertFailure('s4:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { Suburb: '' } } }
        }),
        assertSuccess('s4:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { Suburb: 'MoeggoeVille' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('s4:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { Suburb: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // Resident.Entity.PostalAddress.Suburb
        assertSuccess('s1:4', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertFailure('s1:4', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { Suburb: '' } } }
        }),
        assertSuccess('s2:4', { ReportingQualifier: 'INTERBANK', Resident: { Entity: { PostalAddress: { Suburb: '' } } } }),
        assertFailure('s2:4', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { PostalAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertSuccess('s3:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertFailure('s3:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { Suburb: '' } } }
        }),
        assertSuccess('s3:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { Suburb: 'MoeggoeVille' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('s3:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { Suburb: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('s4:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { Suburb: 'MoeggoeVille' } } }
        }),
        assertFailure('s4:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { Suburb: '' } } }
        }),
        assertSuccess('s4:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { Suburb: 'MoeggoeVille' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('s4:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { Suburb: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // Resident.Individual.StreetAddress.City
        assertSuccess('c1:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { StreetAddress: { City: 'Boopburg' } } }
        }),
        assertFailure('c1:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { StreetAddress: { City: '' } } }
        }),
        assertSuccess('c2:1', { ReportingQualifier: 'INTERBANK', Resident: { Individual: { StreetAddress: { City: '' } } } }),
        assertFailure('c2:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { StreetAddress: { City: 'Boopburg' } } }
        }),
        assertSuccess('c3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { StreetAddress: { City: 'Boopburg' } } }
        }),
        assertFailure('c3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { StreetAddress: { City: '' } } }
        }),
        assertSuccess('c3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { StreetAddress: { City: 'Boopburg' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('c3:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { StreetAddress: { City: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('c4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { StreetAddress: { City: 'Boopburg' } } }
        }),
        assertFailure('c4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { StreetAddress: { City: '' } } }
        }),
        assertSuccess('c4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { StreetAddress: { City: 'Boopburg' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('c4:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { StreetAddress: { City: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // Resident.Entity.StreetAddress.City
        assertSuccess('c1:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { StreetAddress: { City: 'Boopburg' } } }
        }),
        assertFailure('c1:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { StreetAddress: { City: '' } } }
        }),
        assertSuccess('c2:2', { ReportingQualifier: 'INTERBANK', Resident: { Entity: { StreetAddress: { City: '' } } } }),
        assertFailure('c2:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { StreetAddress: { City: 'Boopburg' } } }
        }),
        assertSuccess('c3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { StreetAddress: { City: 'Boopburg' } } }
        }),
        assertFailure('c3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { StreetAddress: { City: '' } } }
        }),
        assertSuccess('c3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { StreetAddress: { City: 'Boopburg' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('c3:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { StreetAddress: { City: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('c4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { StreetAddress: { City: 'Boopburg' } } }
        }),
        assertFailure('c4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { StreetAddress: { City: '' } } }
        }),
        assertSuccess('c4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { StreetAddress: { City: 'Boopburg' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('c4:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { StreetAddress: { City: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // Resident.Individual.PostalAddress.City
        assertSuccess('c1:3', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { PostalAddress: { City: 'Boopburg' } } }
        }),
        assertFailure('c1:3', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { PostalAddress: { City: '' } } }
        }),
        assertSuccess('c2:3', { ReportingQualifier: 'INTERBANK', Resident: { Individual: { PostalAddress: { City: '' } } } }),
        assertFailure('c2:3', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { PostalAddress: { City: 'Boopburg' } } }
        }),
        assertSuccess('c3:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { City: 'Boopburg' } } }
        }),
        assertFailure('c3:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { City: '' } } }
        }),
        assertSuccess('c3:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { City: 'Boopburg' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('c3:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { City: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('c4:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { City: 'Boopburg' } } }
        }),
        assertFailure('c4:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { City: '' } } }
        }),
        assertSuccess('c4:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { City: 'Boopburg' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('c4:3', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { City: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // Resident.Entity.PostalAddress.City
        assertSuccess('c1:4', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { City: 'Boopburg' } } }
        }),
        assertFailure('c1:4', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { City: '' } } }
        }),
        assertSuccess('c2:4', { ReportingQualifier: 'INTERBANK', Resident: { Entity: { PostalAddress: { City: '' } } } }),
        assertFailure('c2:4', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { PostalAddress: { City: 'Boopburg' } } }
        }),
        assertSuccess('c3:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { City: 'Boopburg' } } }
        }),
        assertFailure('c3:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { City: '' } } }
        }),
        assertSuccess('c3:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { City: 'Boopburg' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('c3:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { City: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('c4:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { City: 'Boopburg' } } }
        }),
        assertFailure('c4:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { City: '' } } }
        }),
        assertSuccess('c4:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { City: 'Boopburg' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('c4:4', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { City: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),


        // Resident.Individual.StreetAddress.PostalCode
        assertSuccess('Resident.Individual.StreetAddress.PostalCode.minLen', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { StreetAddress: { PostalCode: '1234' } } }
        }),
        assertWarning('Resident.Individual.StreetAddress.PostalCode.maxLen', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { StreetAddress: { PostalCode: '12345678901' } } }
        }),

        assertSuccess('spc1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { StreetAddress: { PostalCode: '' } } }
        }),
        assertSuccess('spc1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { StreetAddress: { PostalCode: '123' } } }
        }),
        assertFailure('spc1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { StreetAddress: { PostalCode: '12342' } } }
        }),
        assertSuccess('spc2:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { StreetAddress: { PostalCode: '' } } }
        }),
        assertNoRule('spc2:1', {
          ReportingQualifier: 'NON REPORTABLE',
          Resident: { Individual: { StreetAddress: { PostalCode: '123' } } }
        }),
        assertFailure('spc2:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { StreetAddress: { PostalCode: '123' } } }
        }),
        //If the Street Province is NAMIBIA or LESOTHO or SWAZILAND then the PostalCode must be 9999
        assertSuccess('spc3:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              StreetAddress: {
                Province: 'NAMIBIA',
                PostalCode: '9999'
              }
            }
          }
        }),
        assertSuccess('spc3:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              StreetAddress: {
                Province: 'SWAZILAND',
                PostalCode: '9999'
              }
            }
          }
        }),
        assertFailure('spc3:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              StreetAddress: {
                Province: 'NAMIBIA',
                PostalCode: '1234'
              }
            }
          }
        }),
        assertFailure('spc3:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              StreetAddress: {
                Province: 'SWAZILAND',
                PostalCode: '1234'
              }
            }
          }
        }),
        assertSuccess('spc3:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              StreetAddress: {
                Province: 'GAUTENG',
                PostalCode: '9999'
              }
            }
          }
        }),
        //If the Street Province is NAMIBIA or LESOTHO or SWAZILAND then the PostalCode must be 9999
        assertSuccess('spc4:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Individual: {
              StreetAddress: {
                Province: 'NAMIBIA',
                PostalCode: '9999'
              }
            }
          }
        }),
        assertSuccess('spc4:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Individual: {
              StreetAddress: {
                Province: 'SWAZILAND',
                PostalCode: '9999'
              }
            }
          }
        }),
        assertSuccess('spc4:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Individual: {
              StreetAddress: {
                Province: 'NAMIBIA',
                PostalCode: '1234'
              }
            }
          }
        }),
        assertSuccess('spc4:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Individual: {
              StreetAddress: {
                Province: 'SWAZILAND',
                PostalCode: '1234'
              }
            }
          }
        }),
        assertFailure('spc4:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: {
            Individual: {
              StreetAddress: {
                Province: 'GAUTENG',
                PostalCode: '9999'
              }
            }
          }
        }),


        // Resident.Entity.StreetAddress.PostalCode
        assertSuccess('spc1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { StreetAddress: { PostalCode: '' } } }
        }),
        assertSuccess('spc1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { StreetAddress: { PostalCode: '123' } } }
        }),
        assertSuccess('spc2:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { StreetAddress: { PostalCode: '' } } }
        }),
        assertNoRule('spc2:2', {
          ReportingQualifier: 'NON REPORTABLE',
          Resident: { Entity: { StreetAddress: { PostalCode: '123' } } }
        }),
        assertFailure('spc2:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { StreetAddress: { PostalCode: '123' } } }
        }),
        //If the Street Province is NAMIBIA or LESOTHO or SWAZILAND then the PostalCode must be 9999
        assertSuccess('spc3:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { StreetAddress: { Province: 'NAMIBIA', PostalCode: '9999' } } }
        }),
        assertSuccess('spc3:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Entity: {
              StreetAddress: {
                Province: 'SWAZILAND',
                PostalCode: '9999'
              }
            }
          }
        }),
        assertFailure('spc3:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { StreetAddress: { Province: 'NAMIBIA', PostalCode: '1234' } } }
        }),
        assertFailure('spc3:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Entity: {
              StreetAddress: {
                Province: 'SWAZILAND',
                PostalCode: '1234'
              }
            }
          }
        }),
        assertSuccess('spc3:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { StreetAddress: { Province: 'GAUTENG', PostalCode: '9999' } } }
        }),
        //If the Street Province is NAMIBIA or LESOTHO or SWAZILAND then the PostalCode must be 9999
        assertSuccess('spc4:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { StreetAddress: { Province: 'NAMIBIA', PostalCode: '9999' } } }
        }),
        assertSuccess('spc4:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Entity: {
              StreetAddress: {
                Province: 'SWAZILAND',
                PostalCode: '9999'
              }
            }
          }
        }),
        assertSuccess('spc4:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { StreetAddress: { Province: 'NAMIBIA', PostalCode: '1234' } } }
        }),
        assertSuccess('spc4:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Entity: {
              StreetAddress: {
                Province: 'SWAZILAND',
                PostalCode: '1234'
              }
            }
          }
        }),
        assertFailure('spc4:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { StreetAddress: { Province: 'GAUTENG', PostalCode: '9999' } } }
        }),

        // Resident.Individual.PostalAddress.PostalCode
        assertSuccess('pc1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { PostalAddress: { PostalCode: '' } } }
        }),
        assertSuccess('pc1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { PostalAddress: { PostalCode: '123' } } }
        }),
        assertSuccess('pc2:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { PostalAddress: { PostalCode: '1234' } } }
        }),
        assertFailure('pc2:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { PostalAddress: { PostalCode: '' } } }
        }),
        assertSuccess('pc3:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { PostalAddress: { PostalCode: '' } } }
        }),
        assertNoRule('pc3:1', {
          ReportingQualifier: 'NON REPORTABLE',
          Resident: { Individual: { PostalAddress: { PostalCode: '123' } } }
        }),
        assertFailure('pc3:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { PostalAddress: { PostalCode: '123' } } }
        }),
        //If the Street Province is NAMIBIA or LESOTHO or SWAZILAND then the PostalCode must be 9999
        assertSuccess('pc4:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              PostalAddress: {
                Province: 'NAMIBIA',
                PostalCode: '9999'
              }
            }
          }
        }),
        assertSuccess('pc4:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              PostalAddress: {
                Province: 'SWAZILAND',
                PostalCode: '9999'
              }
            }
          }
        }),
        assertFailure('pc4:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              PostalAddress: {
                Province: 'NAMIBIA',
                PostalCode: '1234'
              }
            }
          }
        }),
        assertFailure('pc4:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              PostalAddress: {
                Province: 'SWAZILAND',
                PostalCode: '1234'
              }
            }
          }
        }),
        assertSuccess('pc4:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              PostalAddress: {
                Province: 'GAUTENG',
                PostalCode: '9999'
              }
            }
          }
        }),
        //If the Street Province is NAMIBIA or LESOTHO or SWAZILAND then the PostalCode must be 9999
        assertSuccess('pc5:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              PostalAddress: {
                Province: 'NAMIBIA',
                PostalCode: '9999'
              }
            }
          }
        }),
        assertSuccess('pc5:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              PostalAddress: {
                Province: 'SWAZILAND',
                PostalCode: '9999'
              }
            }
          }
        }),
        assertSuccess('pc5:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              PostalAddress: {
                Province: 'NAMIBIA',
                PostalCode: '1234'
              }
            }
          }
        }),
        assertSuccess('pc5:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              PostalAddress: {
                Province: 'SWAZILAND',
                PostalCode: '1234'
              }
            }
          }
        }),
        assertFailure('pc5:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: {
            Individual: {
              PostalAddress: {
                Province: 'GAUTENG',
                PostalCode: '9999'
              }
            }
          }
        }),
        // field: "Resident.Individual.PostalAddress.PostalCode"
        // If the Flow is IN and the category is 400 and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION, this field must be completed
        assertSuccess('pc6:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { PostalCode: '1234' } } }
        }),
        assertFailure('pc6:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { PostalCode: '' } } }
        }),
        assertSuccess('pc6:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { PostalCode: '1234' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('pc6:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { PostalAddress: { PostalCode: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('pc7:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { PostalCode: '1234' } } }
        }),
        assertFailure('pc7:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { PostalCode: '' } } }
        }),
        assertSuccess('pc7:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { PostalCode: '1234' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('pc7:1', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { PostalAddress: { PostalCode: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // Resident.Entity.PostalAddress.PostalCode"
        assertSuccess('pc1:2', { ReportingQualifier: 'BOPCUS', Resident: { Entity: { PostalAddress: { PostalCode: '' } } } }),
        assertSuccess('pc1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { PostalAddress: { PostalCode: '123' } } }
        }),
        assertSuccess('pc2:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { PostalCode: '1234' } } }
        }),
        assertFailure('pc2:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { PostalCode: '' } } }
        }),
        assertSuccess('pc3:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { PostalAddress: { PostalCode: '' } } }
        }),
        assertNoRule('pc3:2', {
          ReportingQualifier: 'NON REPORTABLE',
          Resident: { Entity: { PostalAddress: { PostalCode: '123' } } }
        }),
        assertFailure('pc3:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { PostalAddress: { PostalCode: '123' } } }
        }),
        //If the Street Province is NAMIBIA or LESOTHO or SWAZILAND then the PostalCode must be 9999
        assertSuccess('pc4:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { Province: 'NAMIBIA', PostalCode: '9999' } } }
        }),
        assertSuccess('pc4:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { Province: 'SWAZILAND', PostalCode: '9999' } } }
        }),
        assertFailure('pc4:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { Province: 'NAMIBIA', PostalCode: '1234' } } }
        }),
        assertFailure('pc4:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { Province: 'SWAZILAND', PostalCode: '1234' } } }
        }),
        assertSuccess('pc4:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { Province: 'GAUTENG', PostalCode: '9999' } } }
        }),
        //If the Street Province is NAMIBIA or LESOTHO or SWAZILAND then the PostalCode must be 9999
        assertSuccess('pc5:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { Province: 'NAMIBIA', PostalCode: '9999' } } }
        }),
        assertSuccess('pc5:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { Province: 'SWAZILAND', PostalCode: '9999' } } }
        }),
        assertSuccess('pc5:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { Province: 'NAMIBIA', PostalCode: '1234' } } }
        }),
        assertSuccess('pc5:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { Province: 'SWAZILAND', PostalCode: '1234' } } }
        }),
        assertFailure('pc5:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { PostalAddress: { Province: 'GAUTENG', PostalCode: '9999' } } }
        }),
        assertSuccess('pc6:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { PostalCode: '1234' } } }
        }),
        assertFailure('pc6:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { PostalCode: '' } } }
        }),
        assertSuccess('pc6:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { PostalCode: '1234' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('pc6:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { PostalAddress: { PostalCode: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('pc7:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { PostalCode: '1234' } } }
        }),
        assertFailure('pc7:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { PostalCode: '' } } }
        }),
        assertSuccess('pc7:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { PostalCode: '1234' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess('pc7:2', {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Entity: { PostalAddress: { PostalCode: '' } } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // Resident.Individual.ContactDetails.ContactSurname
        assertFailure('cn1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { ContactSurname: '' } } }
        }),
        assertSuccess('cn1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { ContactSurname: 'Foo' } } }
        }),
        assertSuccess('cn2:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { ContactDetails: { ContactSurname: '' } } }
        }),
        assertFailure('cn2:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { ContactDetails: { ContactSurname: 'Foo' } } }
        }),

        // Resident.Entity.ContactDetails.ContactSurname
        assertFailure('cn1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { ContactSurname: '' } } }
        }),
        assertSuccess('cn1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { ContactSurname: 'Foo' } } }
        }),
        assertSuccess('cn2:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { ContactDetails: { ContactSurname: '' } } }
        }),
        assertFailure('cn2:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { ContactDetails: { ContactSurname: 'Foo' } } }
        }),

        // Resident.Individual.ContactDetails.ContactName
        assertFailure('cn1:3', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { ContactName: '' } } }
        }),
        assertSuccess('cn1:3', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { ContactName: 'Foo' } } }
        }),
        assertFailure('cn1:3', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { ContactDetails: { ContactName: '' } } }
        }),
        assertSuccess('cn1:3', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { ContactDetails: { ContactName: 'Foo' } } }
        }),
        assertSuccess('cn2:3', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { ContactDetails: { ContactName: '' } } }
        }),
        assertFailure('cn2:3', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { ContactDetails: { ContactName: 'Foo' } } }
        }),

        // Resident.Entity.ContactDetails.ContactName"
        assertFailure('cn1:4', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { ContactName: '' } } }
        }),
        assertSuccess('cn1:4', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { ContactName: 'Foo' } } }
        }),
        assertSuccess('cn2:4', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { ContactDetails: { ContactName: '' } } }
        }),
        assertFailure('cn2:4', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { ContactDetails: { ContactName: 'Foo' } } }
        }),


        // Resident.Individual.ContactDetails.Email
        assertSuccess('cnte1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Email: 'Foo' } } }
        }),
        assertSuccess('cnte1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Email: '', Fax: '123' } } }
        }),
        assertFailure('cnte1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Email: '' } } }
        }),
        assertSuccess('cnte2:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { ContactDetails: { Email: '' } } }
        }),
        assertFailure('cnte2:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { ContactDetails: { Email: 'Foo' } } }
        }),

        // Resident.Entity.ContactDetails.Email
        assertSuccess('cnte1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { Email: 'Foo' } } }
        }),
        assertSuccess('cnte1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { Email: '', Fax: '123' } } }
        }),
        assertFailure('cnte1:2', { ReportingQualifier: 'BOPCUS', Resident: { Entity: { ContactDetails: { Email: '' } } } }),
        assertSuccess('cnte2:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { ContactDetails: { Email: '' } } }
        }),
        assertFailure('cnte2:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { ContactDetails: { Email: 'Foo' } } }
        }),

        // Resident.Individual.ContactDetails.Fax
        assertSuccess('cntft1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Fax: 'Foo' } } }
        }),
        assertSuccess('cntft1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Email: '', Fax: '123' } } }
        }),
        assertFailure('cntft1:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Fax: '' } } }
        }),
        assertSuccess('cntft2:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { ContactDetails: { Fax: '' } } }
        }),
        assertFailure('cntft2:1', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { ContactDetails: { Fax: 'Foo' } } }
        }),
        assertSuccess('cntft3:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Fax: '1231231234' } } }
        }),
        assertSuccess('cntft3:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Fax: '123123123412345' } } }
        }),
        assertFailure('cntft3:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Fax: '123' } } }
        }),
        assertSuccess('cntft3:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Fax: '123456789012' } } }
        }),
        assertFailure('cntft3:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Fax: '1234567890123456' } } }
        }),

        // Resident.Entity.ContactDetails.Fax
        assertSuccess('cntft1:2', { ReportingQualifier: 'BOPCUS', Resident: { Entity: { ContactDetails: { Fax: 'Foo' } } } }),
        assertSuccess('cntft1:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { Email: '', Fax: '123' } } }
        }),
        assertFailure('cntft1:2', { ReportingQualifier: 'BOPCUS', Resident: { Entity: { ContactDetails: { Fax: '' } } } }),
        assertSuccess('cntft2:2', { ReportingQualifier: 'INTERBANK', Resident: { Entity: { ContactDetails: { Fax: '' } } } }),
        assertFailure('cntft2:2', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { ContactDetails: { Fax: 'Foo' } } }
        }),
        assertSuccess('cntft3:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { Fax: '1231231234' } } }
        }),
        assertSuccess('cntft3:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { Fax: '123123123412345' } } }
        }),
        assertFailure('cntft3:2', { ReportingQualifier: 'BOPCUS', Resident: { Entity: { ContactDetails: { Fax: '123' } } } }),
        assertSuccess('cntft3:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { Fax: '123456789012' } } }
        }),
        assertFailure('cntft3:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { Fax: '1234567890123456' } } }
        }),

        // Resident.Individual.ContactDetails.Telephone
        assertSuccess('cntft1:3', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Telephone: 'Foo' } } }
        }),
        assertSuccess('cntft1:3', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Email: '', Telephone: '123' } } }
        }),
        assertFailure('cntft1:3', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Telephone: '' } } }
        }),
        assertSuccess('cntft2:3', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { ContactDetails: { Telephone: '' } } }
        }),
        assertFailure('cntft2:3', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Individual: { ContactDetails: { Telephone: 'Foo' } } }
        }),
        assertSuccess('cntft3:3', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Telephone: '1231231234' } } }
        }),
        assertSuccess('cntft3:3', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Telephone: '123123123412345' } } }
        }),
        assertFailure('cntft3:3', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { ContactDetails: { Telephone: '123' } } }
        }),

        // Resident.Entity.ContactDetails.Telephone
        assertSuccess('cntft1:4', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { Telephone: 'Foo' } } }
        }),
        assertSuccess('cntft1:4', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { Email: '', Telephone: '123' } } }
        }),
        assertFailure('cntft1:4', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { Telephone: '' } } }
        }),
        assertSuccess('cntft2:4', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { ContactDetails: { Telephone: '' } } }
        }),
        assertFailure('cntft2:4', {
          ReportingQualifier: 'INTERBANK',
          Resident: { Entity: { ContactDetails: { Telephone: 'Foo' } } }
        }),
        assertSuccess('cntft3:4', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { Telephone: '1231231234' } } }
        }),
        assertSuccess('cntft3:4', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { Telephone: '123123123412345' } } }
        }),
        assertFailure('cntft3:4', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { ContactDetails: { Telephone: '123' } } }
        }),

        // Resident.Individual.CardNumber
        assertSuccess('crd1:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { CardNumber: '123' } }
        }),
        assertFailure('crd1:1', { ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Individual: { CardNumber: '' } } }),
        assertSuccess('crd2:1', { ReportingQualifier: 'BOPCUS', Resident: { Individual: { CardNumber: '' } } }),
        assertFailure('crd2:1', { ReportingQualifier: 'BOPCUS', Resident: { Individual: { CardNumber: '123' } } }),

        // Resident.Entity.CardNumber
        assertSuccess('crd1:2', { ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { CardNumber: '123' } } }),
        assertFailure('crd1:2', { ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { CardNumber: '' } } }),
        assertSuccess('crd2:2', { ReportingQualifier: 'BOPCUS', Resident: { Entity: { CardNumber: '' } } }),
        assertFailure('crd2:2', { ReportingQualifier: 'BOPCUS', Resident: { Entity: { CardNumber: '123' } } }),

        // Resident.Individual.SupplementaryCardIndicator
        assertSuccess('crdi1:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { SupplementaryCardIndicator: 'Y' } }
        }),
        assertSuccess('crdi1:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { SupplementaryCardIndicator: 'N' } }
        }),
        assertSuccess('crdi1:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { SupplementaryCardIndicator: '' } }
        }),
        assertFailure('crdi1:1', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Individual: { SupplementaryCardIndicator: 'X' } }
        }),
        assertSuccess('crdi2:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { SupplementaryCardIndicator: '' } }
        }),
        assertFailure('crdi2:1', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: { SupplementaryCardIndicator: 'Foo' } }
        }),

        // Resident.Entity.SupplementaryCardIndicator
        assertSuccess('crdi1:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { SupplementaryCardIndicator: 'Y' } }
        }),
        assertSuccess('crdi1:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { SupplementaryCardIndicator: 'N' } }
        }),
        assertSuccess('crdi1:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { SupplementaryCardIndicator: '' } }
        }),
        assertFailure('crdi1:2', {
          ReportingQualifier: 'BOPCARD RESIDENT',
          Resident: { Entity: { SupplementaryCardIndicator: 'X' } }
        }),
        assertSuccess('crdi2:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { SupplementaryCardIndicator: '' } }
        }),
        assertFailure('crdi2:2', {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { SupplementaryCardIndicator: 'Foo' } }
        }),

        // At least one MonetaryAmount entry must be provided
        assertSuccess('tma1', {
          ReportingQualifier: 'BOPCARD',
          MonetaryAmount: [{}]
        }),
        assertFailure('tma1', {
          ReportingQualifier: 'BOPCARD',
          MonetaryAmount: []
        }),
        assertFailure('tma1', {
          ReportingQualifier: 'BOPCARD'
        }),
        // Must contain a sequential SequenceNumber entries that must start with the value 1 (except if the ReplacementTransaction indicator is Y)
        assertSuccess('tma2', {
          ReportingQualifier: 'BOPCARD',
          MonetaryAmount: [{}]
        }),
        assertSuccess('tma2', {
          ReportingQualifier: 'BOPCARD',
          MonetaryAmount: [{ SequenceNumber: "1" }, { SequenceNumber: "2" }]
        }),
        assertSuccess('tma2', {
          ReportingQualifier: 'BOPCARD', ReplacementTransaction: "Y",
          MonetaryAmount: [{ SequenceNumber: "2" }]
        }),
        assertFailure('tma2', {
          ReportingQualifier: 'BOPCARD',
          MonetaryAmount: [{ SequenceNumber: "2" }]
        }),
        assertFailure('tma2', {
          ReportingQualifier: 'BOPCARD',
          MonetaryAmount: [{}, { SequenceNumber: "2" }]
        }),
        //----------------------------------------------------------
        // Money
        //__________________________________________________________
        // Money: SequenceNumber
        assertSuccess("mseq1", { MonetaryAmount: [{ SequenceNumber: 1 }] }),
        assertFailure("mseq1", { MonetaryAmount: [{}] }),

        // Money: MoneyTransferAgentIndicator
        assertSuccess("mta1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ MoneyTransferAgentIndicator: "AD" }] }),
        assertSuccess("mta1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "AYOBA" }]
        }),
        assertFailure("mta1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ MoneyTransferAgentIndicator: "ZZ" }] }),
        assertSuccess("mta1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "CARD" }]
        }),
        assertSuccess("mta2", {
          ReportingQualifier: 'BOPDIR',
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "BOPDIR" }]
        }),
        assertFailure("mta2", { ReportingQualifier: 'BOPDIR', MonetaryAmount: [{ MoneyTransferAgentIndicator: "AD" }] }),
        //If the BoPcategory is 833, the MoneyTransferAgentIndicator may not be AD or ADLA
        assertSuccess("mta3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "BOPDIR", CategoryCode: '833' }]
        }),
        assertNoRule("mta3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "ADLA", CategoryCode: '251' },
          { MoneyTransferAgentIndicator: "AD", CategoryCode: '250' }]
        }),
        assert2Failures("mta3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "ADLA", CategoryCode: '833' },
          { MoneyTransferAgentIndicator: "AD", CategoryCode: '833' }]
        }),
        //CARD was incorrectly excluded from MTA lookups for 833 - BUT, its required for BULK settlements for Res and Non Res CARD (ie: Settlement payments by an AD to VISA)
        assertSuccess("mta3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "CARD", CategoryCode: '833' }]
        }),
        assertFailure("mta3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "ADLA", CategoryCode: '250' },
          { MoneyTransferAgentIndicator: "AD", CategoryCode: '833' }]
        }),
        //Must contain the value CARD
        assertSuccess("mta4", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "CARD" }]
        }),
        assertFailure("mta4", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "AD" }]
        }),
        assertSuccess("mta5", { MonetaryAmount: [{ MoneyTransferAgentIndicator: "AD" }] }),
        assertFailure("mta5", { MonetaryAmount: [{}] }),
        //This dealer is an AD and therefore may not specify ADLA
        assertSuccess("mta6", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ MoneyTransferAgentIndicator: "AD" }] }),
        assertSuccess("mta6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "AYOBA" }]
        }),
        assertFailure("mta6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ MoneyTransferAgentIndicator: "ADLA" }]
        }),

        assertNoRule("mta7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{CategoryCode: '833', MoneyTransferAgentIndicator: "CARD"}]
        }),
        assertFailure("mta7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{CategoryCode: '401', MoneyTransferAgentIndicator: "CARD"}]
        }),
        assertSuccess("mta7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{CategoryCode: '401', MoneyTransferAgentIndicator: ""}]
        }),

        // Money: RandValue
        //At least one of RandValue or ForeignValue must be present
        assertSuccess("mrv1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ RandValue: '10.00' }] }),
        assertSuccess("mrv1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ ForeignValue: '10.00' }] }),
        assertFailure("mrv1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}] }),
        //May not contain a negative value
        assertSuccess("mrv2", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ RandValue: '10.00' }] }),
        assertFailure("mrv2", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ RandValue: '-10.00' }] }),
        //May not equal ForeignValue except if ForeignCurrencyCode is LSL, NAD, SZL, DKK, NOK, SEK, CNY, BWP, UAH, ZAR or HKD
        assertSuccess("mrv3", {
          ReportingQualifier: 'BOPCUS',
          FlowCurrency: 'ZAR',
          MonetaryAmount: [{ RandValue: '10.00', ForeignValue: '10.00' }]
        }),
        assertSuccess("mrv3", {
          ReportingQualifier: 'BOPCUS',
          FlowCurrency: 'RTG',
          MonetaryAmount: [{ RandValue: '10.00', ForeignValue: '10.00' }]
        }),
        assertSuccess("mrv3", {
          ReportingQualifier: 'BOPCUS',
          FlowCurrency: 'USD',
          MonetaryAmount: [{ RandValue: '10.00', ForeignValue: '1.00' }]
        }),
        assertFailure("mrv3", {
          ReportingQualifier: 'BOPCUS',
          FlowCurrency: 'USD',
          MonetaryAmount: [{ RandValue: '10.00', ForeignValue: '10.00' }]
        }),
        //May not be completed
        assertSuccess("mrv5", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mrv5", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{ RandValue: '10.00' }] }),
        //BoPCategory and SubBoPCategory 101/11 or 102/11 or 103/11 or 104/11 is used and the value exceeds ZAR 100,000.00
        assertSuccess("mrv6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11', RandValue: '10.00' }]
        }),
        assertNoRule("mrv6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '12', RandValue: '200123.00' }]
        }),
        assertWarning("mrv6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11', RandValue: '200123.00' }]
        }),
        assertSuccess("mrv6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11', RandValue: '67.64' }]
        }),
        //If BoPCategory 107 is used, the value may not exceed ZAR 1,000.00
        assertSuccess("mrv7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '107', RandValue: '10.00' }]
        }),
        assertFailure("mrv7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '107', RandValue: '1000.01' }]
        }),
        //May not exceed 20 digits
        assertSuccess("mrv8", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ RandValue: '10.00' }] }),
        assertFailure("mrv8", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ RandValue: '1000123243424677783.01' }]
        }),
        //If the flow is OUT and the Subject is REMITTANCE DISPENSATION then the Rand value is mandatory
        assertSuccess("mrv9", {
          ReportingQualifier: 'BOPCUS', Flow: "OUT",
          MonetaryAmount: [{ RandValue: '10.00', AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertFailure("mrv9", {
          ReportingQualifier: 'BOPCUS', Flow: "OUT",
          MonetaryAmount: [{ RandValue: '', AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        /*
                  //If the flow is OUT and the Subject is REMITTANCE DISPENSATION then a only maximum Rand value of R3,000 is allowed
                  assertSuccess("mrv10", {
                    ReportingQualifier: 'BOPCUS', Flow: "OUT",
                    MonetaryAmount: [{RandValue: '10.00', AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
                  }),
                  assertSuccess("mrv10", {
                    ReportingQualifier: 'BOPCUS', Flow: "OUT",
                    MonetaryAmount: [{
                      RandValue: '2900.00',
                      AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}
                    }, {RandValue: '100.00'}]
                  }),
                  assertFailure("mrv10", {
                    ReportingQualifier: 'BOPCUS', Flow: "OUT",
                    MonetaryAmount: [{RandValue: '2900.00'}, {
                      RandValue: '101.00',
                      AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}
                    }]
                  }),
                */
        // 353, "Both the RandValue and ForeignValue for RTG transactions must be supplied, since the RTG rate is not available."
        assertSuccess("mrv11", { ReportingQualifier: 'BOPCUS', FlowCurrency: 'RTG', MonetaryAmount: [{ RandValue: '10.00', ForeignValue: '11.00' }] }),

        assertSuccess("mrv11", { ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD', MonetaryAmount: [{ RandValue: '10.00' }] }),
        assertSuccess("mrv11", { ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD', MonetaryAmount: [{ ForeignValue: '10.00' }] }),

        assertNoRule("mrv11", { ReportingQualifier: 'BOPCUS', FlowCurrency: 'RTG' }),
        assertFailure("mrv11", { ReportingQualifier: 'BOPCUS', FlowCurrency: 'RTG', MonetaryAmount: [{ RandValue: '10.00' }] }),
        assertFailure("mrv11", { ReportingQualifier: 'BOPCUS', FlowCurrency: 'RTG', MonetaryAmount: [{ ForeignValue: '10.00' }] }),

        assertSuccess("mrv12", { ReportingQualifier: 'BOPCUS', FlowCurrency: 'ZAR', MonetaryAmount: [{ RandValue: '10.00', ForeignValue: '10.00' }] }),
        assertFailure("mrv12", { ReportingQualifier: 'BOPCUS', FlowCurrency: 'ZAR', MonetaryAmount: [{ RandValue: '8000.00', ForeignValue: '5000.00' }] }),

        // Money: ForeignValue
        //At least one of RandValue or ForeignValue must be present
        // TODO: Test doesn't pass
        // NOTE: Changed LocalValue -> DomesticValue
        assertSuccess("mfv1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ RandValue: '10.00' }] }),
        assertSuccess("mfv1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ ForeignValue: '10.00' }] }),
        assertFailure("mfv1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}] }),
        //May not contain a negative value
        assertSuccess("mfv2", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ ForeignValue: '10.00' }] }),
        assertFailure("mfv2", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ ForeignValue: '-10.00' }] }),
        //If FlowCurrency is not ZAR, ForeignValue must be completed
        assertSuccess("mfv3", {
          ReportingQualifier: 'BOPCUS',
          FlowCurrency: 'USD',
          MonetaryAmount: [{ ForeignValue: '10.00' }]
        }),
        assertSuccess("mfv3", {
          ReportingQualifier: 'BOPCUS',
          FlowCurrency: 'ZAR',
          MonetaryAmount: [{ RandValue: '10.00' }]
        }),
        assertFailure("mfv3", {
          ReportingQualifier: 'BOPCUS',
          FlowCurrency: 'USD',
          MonetaryAmount: [{ RandValue: '10.00' }]
        }),
        //May not be completed
        assertSuccess("mfv4", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mfv4", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ForeignValue: '10.00' }]
        }),
        //May not exceed 20 digits
        assertSuccess("mfv5", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ ForeignValue: '10.00' }] }),
        assertFailure("mfv5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ForeignValue: '1000123243424677783.01' }]
        }),

        assertSuccess("mfv6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ForeignValue: '10.0' }],
          Resident: { Individual: { AccountIdentifier: "CFC RESIDENT" } }
        }),
        assertFailure("mfv6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ForeignValue: '' }],
          Resident: { Individual: { AccountIdentifier: "CFC RESIDENT" } }
        }),
        assertSuccess("mfv6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ForeignValue: '10.0' }],
          Resident: { Entity: { AccountIdentifier: "CFC RESIDENT" } }
        }),
        assertFailure("mfv6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ForeignValue: '' }],
          Resident: { Entity: { AccountIdentifier: "CFC RESIDENT" } }
        }),




        // Money: CategoryCode
        //Category Length Checks
        assertSuccess("CategoryCode.len", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ CategoryCode: '' }]
        }),
        assertSuccess("CategoryCode.len", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("CategoryCode.len", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ CategoryCode: '2' }]
        }),
        assertFailure("CategoryCode.len", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ CategoryCode: '20' }]
        }),
        //Must be completed
        assertSuccess("mcc1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '101' }] }),
        assertFailure("mcc1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}] }),
        //May not be completed
        assertSuccess("mcc2", { ReportingQualifier: 'INTERBANK', MonetaryAmount: [{}] }),
        assertFailure("mcc2", { ReportingQualifier: 'INTERBANK', MonetaryAmount: [{ CategoryCode: '101' }] }),
        //Invalid BoPCategory
        assertSuccess("mcc3", { ReportingQualifier: 'BOPCUS', Flow: 'IN', MonetaryAmount: [{ CategoryCode: '101' }] }),
        assertFailure("mcc3", { ReportingQualifier: 'BOPCUS', Flow: 'IN', MonetaryAmount: [{ CategoryCode: '099' }] }),
        //For NON REPORTABLE transactions this must be set to ZZ1
        assertSuccess("mcc4", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ CategoryCode: 'ZZ1' }] }),
        assertFailure("mcc4", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ CategoryCode: '101' }] }),
        
        assertFailure("mcc4", { 
          ReportingQualifier: 'NON REPORTABLE', 
          MonetaryAmount: [
            { 
              CategoryCode: 'ZZ1'
            }, 
            {
              CategoryCode: '101'
            }
          ] 
        }),
        assertFailure("mcc4", { 
          ReportingQualifier: 'NON REPORTABLE', 
          MonetaryAmount: [
            { 
              CategoryCode: '101'
            }, 
            {
              CategoryCode: 'ZZ1'
            }
          ] 
        }),
        
        assertFailure("mcc11", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: 'ZZ1' }] }),
        assertSuccess("mcc11", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '101' }] }),

        assertFailure("mcc11", { 
          ReportingQualifier: 'BOPCUS', 
          MonetaryAmount: [
            { 
              CategoryCode: '101'
            }, 
            {
              CategoryCode: 'ZZ1'
            }
          ] 
        }),

        assertFailure("mcc11", { 
          ReportingQualifier: 'BOPCUS', 
          MonetaryAmount: [
            { 
              CategoryCode: 'ZZ1'
            }, 
            {
              CategoryCode: '101'
            }
          ] 
        }),

        //If the Flow is IN and category 303, 304, 305, 306, 416 or 417 is used, the Entity element under Resident is completed, the ThirdParty Individual attributes must be completed.
        assertSuccess("mcc5", {
          ReportingQualifier: "BOPCUS", Flow: "IN", Resident: { Individual: { Name: "John" } },
          MonetaryAmount: [{ CategoryCode: "305" }]
        }),
        assertSuccess("mcc5", {
          ReportingQualifier: "BOPCUS", Flow: "IN", Resident: { Entity: { EntityName: "bla" } },
          MonetaryAmount: [{ CategoryCode: "305", ThirdParty: { Individual: { Surname: 'XYZ' } } }]
        }),
        assertFailure("mcc5", {
          ReportingQualifier: "BOPCUS", Flow: "IN", Resident: { Entity: { EntityName: "bla" } },
          MonetaryAmount: [{ CategoryCode: "305" }]
        }),
        assertNoRule("mcc5", {
          ReportingQualifier: "BOPCUS", Flow: "IN", Resident: { Entity: { EntityName: "bla" } },
          MonetaryAmount: [{ CategoryCode: "256" }]
        }),
        //If CategoryCode 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, the third party individual details and subject must be completed
        assertSuccess("mcc6", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: { Entity: { EntityName: "bla" } },
          MonetaryAmount: [{
            CategoryCode: "513",
            AdHocRequirement: { Subject: 'SETOFF' },
            ThirdParty: {
              Individual: { Surname: 'XYZ' },
              StreetAddress: { AddressLine1: 'Street Home' },
              PostalAddress: { AddressLine1: 'Postal Home' }
            }
          }]
        }),
        assertFailure("mcc6", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: { Entity: { EntityName: "bla" } },
          MonetaryAmount: [{
            CategoryCode: "513",
            AdHocRequirement: { Subject: 'SETOFF' },
            ThirdParty: {
              StreetAddress: { AddressLine1: 'Street Home' },
              PostalAddress: { AddressLine1: 'Postal Home' }
            }
          }]
        }),
        assertFailure("mcc6", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: { Entity: { EntityName: "bla" } },
          MonetaryAmount: [{
            CategoryCode: "513",
            AdHocRequirement: { Subject: 'SETOFF' },
            ThirdParty: { Individual: { Surname: 'XYZ' }, PostalAddress: { AddressLine1: 'Postal Home' } }
          }]
        }),
        assertFailure("mcc6", {
          ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: { Entity: { EntityName: "bla" } },
          MonetaryAmount: [{
            CategoryCode: "513",
            AdHocRequirement: { Subject: 'SETOFF' },
            ThirdParty: { Individual: { Surname: 'XYZ' }, StreetAddress: { AddressLine1: 'Street Home' } }
          }]
        }),
        //If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, the third party individual details and subject must be completed
        assertSuccess("mcc7", {
          ReportingQualifier: "BOPCUS", Flow: "IN", Resident: { Entity: { EntityName: "bla" } },
          MonetaryAmount: [{
            CategoryCode: "516",
            AdHocRequirement: { Subject: 'SETOFF' },
            ThirdParty: {
              Individual: { Surname: 'XYZ' },
              StreetAddress: { AddressLine1: 'Street Home' },
              PostalAddress: { AddressLine1: 'Postal Home' }
            }
          }]
        }),
        assertFailure("mcc7", {
          ReportingQualifier: "BOPCUS", Flow: "IN", Resident: { Entity: { EntityName: "bla" } },
          MonetaryAmount: [{
            CategoryCode: "516",
            AdHocRequirement: { Subject: 'SETOFF' },
            ThirdParty: {
              StreetAddress: { AddressLine1: 'Street Home' },
              PostalAddress: { AddressLine1: 'Postal Home' }
            }
          }]
        }),
        assertFailure("mcc7", {
          ReportingQualifier: "BOPCUS", Flow: "IN", Resident: { Entity: { EntityName: "bla" } },
          MonetaryAmount: [{
            CategoryCode: "516",
            AdHocRequirement: { Subject: 'SETOFF' },
            ThirdParty: { Individual: { Surname: 'XYZ' }, PostalAddress: { AddressLine1: 'Postal Home' } }
          }]
        }),
        assertFailure("mcc7", {
          ReportingQualifier: "BOPCUS", Flow: "IN", Resident: { Entity: { EntityName: "bla" } },
          MonetaryAmount: [{
            CategoryCode: "516",
            AdHocRequirement: { Subject: 'SETOFF' },
            ThirdParty: { Individual: { Surname: 'XYZ' }, StreetAddress: { AddressLine1: 'Street Home' } }
          }]
        }),

        //If the Flow is OUT then the categories 102/01 to 102/10 or 104/01 to 104/10 can only be used for import undertaking customers
        assertSuccess("mcc8", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: "102", CategorySubCode: '01', ThirdParty: { CustomsClientNumber: "12345678" } }]
        }),
        assertSuccess("mcc8", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          Resident: { Individual: { CustomsClientNumber: "98765432" } },
          MonetaryAmount: [{ CategoryCode: "102", CategorySubCode: '01' }]
        }),
        assertSuccess("mcc8", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          Resident: { Entity: { CustomsClientNumber: "98765432" } },
          MonetaryAmount: [{ CategoryCode: "102", CategorySubCode: '01' }]
        }),
        assertSuccess("mcc8", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          Resident: { Individual: { CustomsClientNumber: "not LU" } },
          MonetaryAmount: [{ CategoryCode: "102", CategorySubCode: '01', ThirdParty: { CustomsClientNumber: "12345678" } }]
        }),
        assertFailure("mcc8", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: "102", CategorySubCode: '01' }]
        }),
        assertFailure("mcc8", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: "102", CategorySubCode: '01', ThirdParty: { CustomsClientNumber: "not LU" } }]
        }),
        assertFailure("mcc8", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          Resident: { Individual: { CustomsClientNumber: "not LU" } },
          MonetaryAmount: [{ CategoryCode: "102", CategorySubCode: '01' }]
        }),
        assertNoRule("mcc8", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: "102", CategorySubCode: '11' }]
        }),

        //If the Flow is OUT then the categories 101/01 to 101/10 or 103/01 to 103/10 may not be used for import undertaking customers
        assertFailure("mcc9", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: "101", CategorySubCode: '01', ThirdParty: { CustomsClientNumber: "12345678" } }]
        }),
        assertFailure("mcc9", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          Resident: { Individual: { CustomsClientNumber: "98765432" } },
          MonetaryAmount: [{ CategoryCode: "101", CategorySubCode: '01' }]
        }),
        assertFailure("mcc9", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          Resident: { Entity: { CustomsClientNumber: "98765432" } },
          MonetaryAmount: [{ CategoryCode: "101", CategorySubCode: '01' }]
        }),
        assertFailure("mcc9", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          Resident: { Individual: { CustomsClientNumber: "not LU" } },
          MonetaryAmount: [{ CategoryCode: "101", CategorySubCode: '01', ThirdParty: { CustomsClientNumber: "12345678" } }]
        }),
        assertSuccess("mcc9", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          Resident: { Individual: { CustomsClientNumber: "not LU" } },
          MonetaryAmount: [{ CategoryCode: "101", CategorySubCode: '01', ThirdParty: { CustomsClientNumber: "not LU" } }]
        }),
        assertSuccess("mcc9", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          Resident: { Individual: { CustomsClientNumber: "12345678" } },
          MonetaryAmount: [{ CategoryCode: "101", CategorySubCode: '01', ThirdParty: { CustomsClientNumber: "not LU" } }]
        }),
        assertSuccess("mcc9", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: "101", CategorySubCode: '01' }]
        }),
        assertSuccess("mcc9", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: "101", CategorySubCode: '01', ThirdParty: { CustomsClientNumber: "not LU" } }]
        }),
        assertSuccess("mcc9", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          Resident: { Individual: { CustomsClientNumber: "not LU" } },
          MonetaryAmount: [{ CategoryCode: "101", CategorySubCode: '01' }]
        }),
        assertNoRule("mcc9", {
          ReportingQualifier: "BOPCUS", Flow: "OUT",
          MonetaryAmount: [{ CategoryCode: "101", CategorySubCode: '11' }]
        }),

        // Money: CategorySubCode
        assertSuccess("msc1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '11' }]
        }),
        assertSuccess("msc1", { ReportingQualifier: 'BOPCUS', Flow: 'IN', MonetaryAmount: [{ CategoryCode: '100' }] }),
        assertFailure("msc1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '99' }]
        }),
        assertFailure("msc1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '100', CategorySubCode: '01' }]
        }),
        //May not be completed
        assertSuccess("msc2", { ReportingQualifier: 'INTERBANK', MonetaryAmount: [{}] }),
        assertFailure("msc2", { ReportingQualifier: 'INTERBANK', MonetaryAmount: [{ CategorySubCode: '11' }] }),

        // Money: SWIFTDetails

        assertSuccess("mswd1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          MonetaryAmount: [{ SWIFTDetails: '' }]
        }),
        assertSuccess("mswd1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          MonetaryAmount: [{ SWIFTDetails: '' }]
        }),
        assertFailure("mswd1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          MonetaryAmount: [{ SWIFTDetails: '12' }]
        }),
        assertFailure("mswd1", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          MonetaryAmount: [{ SWIFTDetails: '12' }]
        }),

        assertSuccess("SWIFTDetails.minLen", { MonetaryAmount: [{ SWIFTDetails: '12' }] }),
        assertFailure("SWIFTDetails.minLen", { MonetaryAmount: [{ SWIFTDetails: '1' }] }),
        assertSuccess("SWIFTDetails.maxLen", { MonetaryAmount: [{ SWIFTDetails: '0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789' }] }),
        assertWarning("SWIFTDetails.maxLen", { MonetaryAmount: [{ SWIFTDetails: '01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567891' }] }),

        // Money: StrateRefNumber
        //Must be completed if the BoPCategory and SubBoPCategory is 601/01 or 603/01
        assertSuccess("msrn1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '603', CategorySubCode: '01', StrateRefNumber: 'XYZ' }]
        }),
        assertNoRule("msrn1", { ReportingQualifier: 'BOPCUS', Flow: 'IN', MonetaryAmount: [{ CategoryCode: '100' }] }),
        assertFailure("msrn1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '603', CategorySubCode: '01' }]
        }),
        //Must be completed if the Resident ExceptionName is STRATE
        assertSuccess("msrn2", {
          ReportingQualifier: 'BOPCUS', Resident: { Exception: { ExceptionName: 'STRATE' } },
          MonetaryAmount: [{ StrateRefNumber: 'XYZ' }]
        }),
        assertFailure("msrn2", {
          ReportingQualifier: 'BOPCUS', Resident: { Exception: { ExceptionName: 'STRATE' } },
          MonetaryAmount: [{}]
        }),
        //May not be completed
        assertSuccess("msrn3", { ReportingQualifier: 'INTERBANK', MonetaryAmount: [{}] }),
        assertFailure("msrn3", { ReportingQualifier: 'INTERBANK', MonetaryAmount: [{ StrateRefNumber: 'XYZ' }] }),
        //Must only contain ON MARKET or OFF MARKET
        assertSuccess("msrn4", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ StrateRefNumber: 'ON MARKET' }] }),
        assertSuccess("msrn4", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ StrateRefNumber: 'OFF MARKET' }] }),
        assertFailure("msrn4", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ StrateRefNumber: 'XYZ' }] }),
        assertSuccess("msrn4", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ StrateRefNumber: '' }] }),

        // Money: Travel
        assertSuccess("mtvl1:1", { MonetaryAmount: [{}] }),
        assertDeprecated("mtvl1:1", { MonetaryAmount: [{ Travel: {} }] }),
        assertSuccess("mtvl1:2", { MonetaryAmount: [{}] }),
        assertDeprecated("mtvl1:2", { MonetaryAmount: [{ Travel: { Surname: 'Soap' } }] }),

        // Money: LoanRefNumber
        assertSuccess("LoanRefNumber.minLen", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '802', LoanRefNumber: '12' }]
        }),
        assertFailure("LoanRefNumber.minLen", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '802', LoanRefNumber: '1' }]
        }),
        assertWarning("LoanRefNumber.maxLen", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            CategoryCode: '802',
            LoanRefNumber: '123456789012345678901'
          }]
        }),
        //If CategoryCode 801, or 802, or 803 or 804 is used, must be completed
        assertSuccess("mlrn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '802', LoanRefNumber: 'XYZ' }]
        }),
        assertFailure("mlrn1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '802' }] }),
        //If CategoryCode is 801, or 802, or 803 or 804 and LocationCountry is LS and the LoanRefNumber must be 99012301230123
        assertSuccess("mlrn2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            CategoryCode: '802',
            LocationCountry: 'LS',
            LoanRefNumber: '99012301230123'
          }]
        }),
        assertFailure("mlrn2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            CategoryCode: '802',
            LocationCountry: 'LS',
            LoanRefNumber: '99999999999999'
          }]
        }),

        assertSuccess("mlrn3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            CategoryCode: '802',
            LocationCountry: 'SZ',
            LoanRefNumber: '99456745674567'
          }]
        }),
        assertFailure("mlrn3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            CategoryCode: '802',
            LocationCountry: 'SZ',
            LoanRefNumber: '99999999999999'
          }]
        }),

        assertSuccess("mlrn4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            CategoryCode: '802',
            LocationCountry: 'NA',
            LoanRefNumber: '99789078907890'
          }]
        }),
        assertFailure("mlrn4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            CategoryCode: '802',
            LocationCountry: 'NA',
            LoanRefNumber: '99999999999999'
          }]
        }),
        //If CategoryCode and CategorySubCode is 106 or 309/04 or 309/05 or 309/06 or 309/07 is used and the Flow is OUT, and the LocationCountry is LS and the LoanRefNumber must be 99012301230123
        assertSuccess("mlrn5", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '04',
            LocationCountry: 'LS',
            LoanRefNumber: '99012301230123'
          }]
        }),
        assertFailure("mlrn5", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '04',
            LocationCountry: 'LS',
            LoanRefNumber: '99999999999999'
          }]
        }),
        assertSuccess("mlrn6", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '04',
            LocationCountry: 'SZ',
            LoanRefNumber: '99456745674567'
          }]
        }),
        assertFailure("mlrn6", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '04',
            LocationCountry: 'SZ',
            LoanRefNumber: '99999999999999'
          }]
        }),
        assertSuccess("mlrn7", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '04',
            LocationCountry: 'NA',
            LoanRefNumber: '99789078907890'
          }]
        }),
        assertFailure("mlrn7", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '04',
            LocationCountry: 'NA',
            LoanRefNumber: '99999999999999'
          }]
        }),


        //If the Flow is OUT, and CategoryCode and CategorySubCode is 106 or 309/04 or 309/05 or 309/06 or 309/07, it must be completed
        assertSuccess("mlrn8", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '309', CategorySubCode: '04', LoanRefNumber: 'XYZ' }]
        }),
        assertFailure("mlrn8", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '309', CategorySubCode: '04' }]
        }),
        //If the Flow is OUT and CategoryCode is 810 or 815 or 816 or 817 or 818 or 819 is used, it may not be completed
        assertSuccess("mlrn9", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', MonetaryAmount: [{ CategoryCode: '815' }] }),
        assertFailure("mlrn9", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanRefNumber: 'XYZ' }]
        }),
        //May not be completed
        assertSuccess("mlrn10", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("mlrn10", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ LoanRefNumber: 'XYZ' }] }),
        //For any other category other than 801, 802, 803, 804, 106, 309/04, 309/05, 309/06 or 309/07 this must not be completed
        assertSuccess("mlrn11", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '101', LoanRefNumber: '' }]
        }),
        assertSuccess("mlrn11", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '309/03', LoanRefNumber: '' }]
        }),
        assertFailure("mlrn11", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '101', LoanRefNumber: 'XYZ' }]
        }),
        assertNoRule("mlrn11", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '801', LoanRefNumber: 'XYZ' }]
        }),
        assertNoRule("mlrn11", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '309/04', LoanRefNumber: 'XYZ' }]
        }),

        assertSuccess("mlrn12", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            CategoryCode: '802',
            LocationCountry: 'LS',
            LoanRefNumber: '99012301230123'
          }]
        }),
        assertFailure("mlrn12", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            CategoryCode: '802',
            LocationCountry: 'LS',
            LoanRefNumber: '9A999999999999'
          }]
        }),




        // Money: LoanTenor
        //If the Flow is Out and BoPCategory 810 or 815 or 816 or 817 or 818 or 819 is used, must reflect the date of maturity in the format CCYY-MM-DD or ON DEMAND if no date is applicable
        assertSuccess("mlt1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanTenor: 'ON DEMAND' }]
        }),
        assertFailure("mlt1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanTenor: '' }]
        }),
        assertSuccess("mlt1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanTenor: '2013-10-01' }]
        }),
        assertFailure("mlt1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanTenor: '20131001' }]
        }),
        //If the LoanTenor in the format CCYY-MM-DD it must be a future date
        assertSuccess("mlt2", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ LoanTenor: '2013-01-31' }] }),
        assertFailure("mlt2", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ LoanTenor: '2013-01-01' }] }),
        //May not be completed
        assertSuccess("mlt3", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("mlt3", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ LoanTenor: 'ON DEMAND' }] }),

        // Money: LoanInterestRate
        //If the Flow is Out and BoPCategory 810 or 815 or 816 or 817 or 818 or 819 is used, must contain a value of reflecting interest rate percentage of the loan in the proper format
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: '0.0' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: '0.00' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: '5.12' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: 'BASE PLUS 5.12' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: 'BASE MINUS 5.12' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: 'LIBOR' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: '12 LIBOR' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: '9 LIBOR PLUS 5.12' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: '6 LIBOR MINUS 5.12' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: 'JIBAR' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: '12 JIBAR' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: '12 JIBAR PLUS 5.12' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: '12 JIBAR MINUS 5.12' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: 'FOOBAR' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: '6 FOOBAR' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: '12 FOOBAR PLUS 2.1' }]
        }),
        assertSuccess("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: '12 FOOBAR MINUS 2.1' }]
        }),
        assertFailure("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: '2DAR 5.12' }]
        }),
        assertFailure("mlir1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '815', LoanInterestRate: 'BASE 5.12' }]
        }),
        //May not be completed
        assertSuccess("mlir2", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("mlir2", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ LoanInterestRate: '5.12' }] }),
        //If the Flow is Out and BoPCategory 309/04 to 309/07 is used, must be completed reflecting the percentage interest paid
        assertSuccess("mlir3", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '05',
            LoanInterestRate: '5.12'
          }]
        }),
        assertSuccess("mlir3", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '05',
            LoanInterestRate: '105.12'
          }]
        }),
        assertSuccess("mlir3", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '05',
            LoanInterestRate: '10.00'
          }]
        }),
        assertFailure("mlir3", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '05',
            LoanInterestRate: '2000.10'
          }]
        }),
        assertFailure("mlir3", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '05',
            LoanInterestRate: 'BASE PLUS 5.12'
          }]
        }),
        assertFailure("mlir3", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '309', CategorySubCode: '05', LoanInterestRate: '' }]
        }),
        assertFailure("mlir3", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '05',
            LoanInterestRate: '2.1'
          }]
        }),
        assertFailure("mlir3", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '309', CategorySubCode: '05', LoanInterestRate: '2' }]
        }),
        assertFailure("mlir3", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '309', CategorySubCode: '05' }]
        }),
        //If the Flow is In and BoPCategory 309/01 to 309/07 is used, must be completed reflecting the percentage interest paid
        assertSuccess("mlir4", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '02',
            LoanInterestRate: '5.12'
          }]
        }),
        assertSuccess("mlir4", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '02',
            LoanInterestRate: '5.00'
          }]
        }),
        assertFailure("mlir4", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '02',
            LoanInterestRate: '1023467'
          }]
        }),
        assertFailure("mlir4", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '02',
            LoanInterestRate: 'BASE PLUS 5.12'
          }]
        }),
        assertFailure("mlir4", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '309', CategorySubCode: '02', LoanInterestRate: '' }]
        }),
        assertFailure("mlir4", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '309', CategorySubCode: '02' }]
        }),
        assertFailure("mlir4", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '02',
            LoanInterestRate: '.12'
          }]
        }),

        assertSuccess("mlir5", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '05',
            LoanInterestRate: '50.01'
          }]
        }),
        assertWarning("mlir5", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '05',
            LoanInterestRate: '200.10'
          }]
        }),
        assertSuccess("mlir6", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '02',
            LoanInterestRate: '50.01'
          }]
        }),
        assertWarning("mlir6", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '02',
            LoanInterestRate: '500.00'
          }]
        }),
        //May not be completed unless a loan related transaction is being captured
        assertNoRule("mlir7", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '810',
            LoanInterestRate: '50.01'
          }]
        }),
        assertSuccess("mlir7", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '01' }]
        }),
        assertFailure("mlir7", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '101',
            CategorySubCode: '01',
            LoanInterestRate: '500.00'
          }]
        }),
        assertNoRule("mlir7", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '02',
            LoanInterestRate: '500.00'
          }]
        }),
        //May not be completed unless a loan related transaction is being captured
        assertNoRule("mlir8", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '816', LoanInterestRate: '50.01' }]
        }),
        assertSuccess("mlir8", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '01' }]
        }),
        assertFailure("mlir8", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '101',
            CategorySubCode: '01',
            LoanInterestRate: '500.00'
          }]
        }),

        // Money: SARBAuth.RulingsSection
        //Must be completed if the Flow is OUT and no data is supplied under either ADInternalAuthNumber or CBAuthAppNumber. (Categories 100, 200, 300, 400, 500, 600, 700 and 800 are excluded)
        assertSuccess("mars1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', SARBAuth: { RulingsSection: 'XYZ' } }]
        }),
        // TODO: Test doesn't pass
        assertSuccess("mars1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', SARBAuth: { SARBAuthAppNumber: 'XYZ' } }]
        }),
        assertSuccess("mars1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', SARBAuth: { ADInternalAuthNumber: 'XYZ' } }]
        }),
        // TODO: Test does pass incorrectly
        assertFailure("mars1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', SARBAuth: { ADInternalAuthNumber: '' } }]
        }),
        assertNoRule("mars1", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', MonetaryAmount: [{ CategoryCode: '100' }] }),
        assertFailure("mars1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', SARBAuth: {} }]
        }),
        assertFailure("mars1", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', MonetaryAmount: [{ CategoryCode: '101' }] }),
        //May not be completed
        assertSuccess("mars2", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("mars2", {
          ReportingQualifier: 'NON REPORTABLE',
          MonetaryAmount: [{ SARBAuth: { RulingsSection: 'XYZ' } }]
        }),

        //If the flow is OUT and the Subject is REMITTANCE DISPENSATION then the RulingsSection must be 'Circular 06/2019'
        assertSuccess("mars3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ SARBAuth: { RulingsSection: 'XYZ' } }]
        }),
        assertFailure("mars3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ SARBAuth: { RulingsSection: 'XYZ' }, AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess("mars3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ SARBAuth: { RulingsSection: 'Circular 06/2019' }, AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertSuccess("mars3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ SARBAuth: { RulingsSection: 'CIRCULAR 06/2019' }, AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        assertFailure("mars4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{
            RandValue: "7000.00",
            SARBAuth: { RulingsSection: 'Circular 06/2019' },
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' }
          }]
        }),
        assertFailure("mars4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{
            RandValue: "3000.00",
            SARBAuth: { RulingsSection: 'Circular 06/2019' }
          }]
        }),
        assertFailure("mars4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{
            RandValue: "7000.00",
            SARBAuth: { RulingsSection: 'Circular 06/2019' },
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' }
          }]
        }),
        assertSuccess("mars4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{
            RandValue: "3000.00",
            SARBAuth: { RulingsSection: 'Circular 06/2019' },
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' }
          }]
        }),
        assertSuccess("mars4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{
            RandValue: "3000.00",
            SARBAuth: { RulingsSection: 'CIRCULAR 06/2019' },
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' }
          }]
        }),

        assertSuccess("mars5", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: "400",
            RandValue: "3000.00",
            SARBAuth: { RulingsSection: 'Circular 06/2019' },
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' }
          }]
        }),
        assertSuccess("mars5", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: "400",
            RandValue: "3000.00",
            SARBAuth: { RulingsSection: 'CIRCULAR 06/2019' },
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' }
          }]
        }),
        assertFailure("mars5", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: "400",
            RandValue: "7000.00",
            SARBAuth: { RulingsSection: 'Circular 06/2019' },
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' }
          }]
        }),
        assertFailure("mars5", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: "400",
            RandValue: "3000.00",
            SARBAuth: { RulingsSection: 'CIRCULAR 06/2019' }
          }]
        }),
        assertFailure("mars5", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: "400",
            RandValue: "7000.00",
            SARBAuth: { RulingsSection: 'Circular 06/2019' }
          }]
        }),

        assertSuccess("mars6", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ RandValue: "3000.00", SARBAuth: { RulingsSection: '' } }],
          Resident: {Exception: {ExceptionName: "STRATE"}}
        }),
        assertSuccess("mars6", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ RandValue: "3000.00", SARBAuth: { RulingsSection: '' } }],
          Resident: {Individual: { StreetAddress: { AddressLine1: 'ADDR' } } }
        }),
        assertSuccess("mars6", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ RandValue: "3000.00", SARBAuth: { RulingsSection: '' } }],
          Resident: {Individual: { PostalAddress: { AddressLine1: 'ADDR' } } }
        }),
        assertSuccess("mars6", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ RandValue: "7000.00", SARBAuth: { RulingsSection: '' } }],
          Resident: {Exception: {ExceptionName: ""}}
        }),
        assertSuccess("mars6", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ RandValue: "3000.00", SARBAuth: { RulingsSection: 'Circular 06/2019' } }],
          Resident: {Exception: {ExceptionName: ""}}
        }),
        assertSuccess("mars6", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ RandValue: "3000.00", SARBAuth: { RulingsSection: 'CIRCULAR 06/2019' } }],
          Resident: {Exception: {ExceptionName: ""}}
        }),
        assertFailure("mars6", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ RandValue: "3000.00", SARBAuth: { RulingsSection: '' } }],
          Resident: {Exception: {ExceptionName: ""}}
        }),
        assertFailure("mars6", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ RandValue: "3000.00", SARBAuth: { RulingsSection: '' } }],
        }),

        assertSuccess("mars7", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "400", RandValue: "3000.00", SARBAuth: { RulingsSection: '' } }],
          Resident: {Exception: {ExceptionName: "STRATE"}}
        }),
        assertSuccess("mars7", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "400", RandValue: "3000.00", SARBAuth: { RulingsSection: '' } }],
          Resident: {Individual: { StreetAddress: { AddressLine1: 'ADDR' } } }
        }),
        assertSuccess("mars7", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "400", RandValue: "3000.00", SARBAuth: { RulingsSection: '' } }],
          Resident: {Individual: { PostalAddress: { AddressLine1: 'ADDR' } } }
        }),
        assertSuccess("mars7", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "400", RandValue: "7000.00", SARBAuth: { RulingsSection: '' } }],
          Resident: {Exception: {ExceptionName: ""}}
        }),
        assertSuccess("mars7", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "400", RandValue: "3000.00", SARBAuth: { RulingsSection: '22/2015' } }],
          Resident: {Exception: {ExceptionName: ""}}
        }),

        assertFailure("mars7", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: "400", RandValue: "3000.00", SARBAuth: { RulingsSection: '' } }],
          Resident: {Exception: {ExceptionName: ""}}
        }),
        assertFailure("mars7", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: "400",
            RandValue: "3000.00",
            SARBAuth: { RulingsSection: '' } }],
        }),

        // Money: SARBAuth.ADInternalAuthNumber
        //Must be completed if the Flow is OUT and no data is supplied under either RulingsSection or SARBAuthAppNumber (does not apply to adjustments)
        assertSuccess("maian1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', SARBAuth: { ADInternalAuthNumber: 'XYZ' } }]
        }),
        // TODO: Test doesn't pass
        assertSuccess("maian1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', SARBAuth: { SARBAuthAppNumber: 'XYZ' } }]
        }),
        assertNoRule("maian1", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', MonetaryAmount: [{ CategoryCode: '100' }] }),
        assertFailure("maian1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', SARBAuth: {} }]
        }),
        assertFailure("maian1", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', MonetaryAmount: [{ CategoryCode: '101' }] }),
        //May not be completed
        assertSuccess("maian2", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("maian2", {
          ReportingQualifier: 'NON REPORTABLE',
          MonetaryAmount: [{ SARBAuth: { ADInternalAuthNumber: 'XYZ' } }]
        }),

        // Money: SARBAuth.ADInternalAuthNumberDate
        //If ADInternalAuthNumber has a value, it must be completed
        assertSuccess("maiad1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            SARBAuth: {
              ADInternalAuthNumber: 'XYZ',
              ADInternalAuthNumberDate: "2013-01-01"
            }
          }]
        }),
        // TODO: Test doesn't pass
        assertFailure("maiad1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ SARBAuth: { ADInternalAuthNumber: 'XYZ' } }]
        }),
        //May not be completed
        assertSuccess("maiad2", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("maiad2", {
          ReportingQualifier: 'NON REPORTABLE',
          MonetaryAmount: [{ SARBAuth: { ADInternalAuthNumberDate: "2013-01-01" } }]
        }),
        //Is not in the required date format is CCYY-MM-DD
        assertSuccess("maiad3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ SARBAuth: { ADInternalAuthNumberDate: "2013-02-01" } }]
        }),
        assertSuccess("maiad3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ SARBAuth: { ADInternalAuthNumberDate: "" } }]
        }),
        assertFailure("maiad3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ SARBAuth: { ADInternalAuthNumberDate: "20130201" } }]
        }),
        assertFailure("maiad3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ SARBAuth: { ADInternalAuthNumberDate: "ON DEMAND" } }]
        }),
        assertFailure("maiad3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ SARBAuth: { ADInternalAuthNumberDate: "2013/02/01" } }]
        }),

        // Money: SARBAuth.SARBAuthAppNumber
        //Must be completed if the Flow is OUT and no data is supplied under either RulingsSection or ADInternalAuthNumber (does not apply to adjustments)
        assertSuccess("masan1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', SARBAuth: { SARBAuthAppNumber: 'XYZ' } }]
        }),
        assertSuccess("masan1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', SARBAuth: { RulingsSection: 'XYZ' } }]
        }),
        assertNoRule("masan1", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', MonetaryAmount: [{ CategoryCode: '100' }] }),
        assertFailure("masan1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', SARBAuth: {} }]
        }),
        assertFailure("masan1", { ReportingQualifier: 'BOPCUS', Flow: 'OUT', MonetaryAmount: [{ CategoryCode: '101' }] }),
        //If the Flow is IN and the Subject is SETOFF, it must be completed
        assertSuccess("masan2", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'SETOFF' },
            SARBAuth: { SARBAuthAppNumber: 'XYZ' }
          }]
        }),
        assertFailure("masan2", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'SETOFF' },
            SARBAuth: { SARBAuthAppNumber: '' }
          }]
        }),
        assertFailure("masan2", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'SETOFF' }, SARBAuth: {} }]
        }),
        //May not be completed
        assertSuccess("masan3", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("masan3", {
          ReportingQualifier: 'NON REPORTABLE',
          MonetaryAmount: [{ SARBAuth: { SARBAuthAppNumber: 'XYZ' } }]
        }),

        assertSuccess("masan4", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { RegistrationNumber: "2013/1234567/07" } },
          MonetaryAmount: [{
            SARBAuth: { SARBAuthAppNumber: 'XYZ' }
          }]
        }),
        assertSuccess("masan4", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { RegistrationNumber: "2013/1234568/07" } },
          MonetaryAmount: [{
            SARBAuth: { SARBAuthAppNumber: 'XYZ' }
          }]
        }),
        assertFailure("masan4", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { RegistrationNumber: "2013/1234568/07" } },
          MonetaryAmount: [{
            SARBAuth: { SARBAuthAppNumber: '' }
          }]
        }),
        assertSuccess("masan4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'IHQ123' },
            SARBAuth: { SARBAuthAppNumber: 'XYZ' }
          }]
        }),
        assertFailure("masan4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'IHQ123' },
            SARBAuth: { SARBAuthAppNumber: '' }
          }]
        }),
        assertNoRule("masan4", { ReportingQualifier: "NON REPORTABLE" }),

        // Money: SARBAuth.SARBAuthRefNumber
        //If SARBAuthAppNumber has a value, it must be completed
        assertSuccess("masar1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ SARBAuth: { SARBAuthAppNumber: 'XYZ', SARBAuthRefNumber: 'ABC' } }]
        }),
        assertFailure("masar1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ SARBAuth: { SARBAuthAppNumber: 'XYZ', SARBAuthRefNumber: '' } }]
        }),
        assertFailure("masar1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ SARBAuth: { SARBAuthAppNumber: 'XYZ' } }]
        }),
        assertSuccess("masar1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}] }),
        //Must be in the format CCYY-nnnnnnnn where CC is the Century, YY is the year and nnnnnnnn is the e-docs number or any other SARB authorisation number allocated by the SARB in the reply to the application
        //assertSuccess("masar2", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{SARBAuth: {SARBAuthAppNumber: 'XYZ', SARBAuthRefNumber: '2013-2345'}}]}),
        //assertSuccess("masar2", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{SARBAuth: {SARBAuthAppNumber: 'XYZ', SARBAuthRefNumber: '1999-8762567'}}]}),
        //assertFailure("masar2", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{SARBAuth: {SARBAuthAppNumber: 'XYZ', SARBAuthRefNumber: '2345-2345'}}]}),
        //May not be completed
        assertSuccess("masar3", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("masar3", {
          ReportingQualifier: 'NON REPORTABLE',
          MonetaryAmount: [{ SARBAuth: { SARBAuthRefNumber: 'ABC' } }]
        }),

        // Money: CannotCategorize
        //If CategoryCode 830 is used, must be completed
        assertSuccess("mexc1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '830', CannotCategorize: 'XYZ' }]
        }),
        assertFailure("mexc1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '830', CannotCategorize: '' }]
        }),
        assertFailure("mexc1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '830' }] }),
        //May not be completed
        assertSuccess("mexc2", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("mexc2", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ CannotCategorize: 'ABC' }] }),

        // Money: AdHocRequirement.Subject
        //If the Subject contains a value, it must be INVALIDIDNUMBER, AIRPORT, IHQnnn (where nnn is numeric) or SETOFF or ZAMBIAN GRAIN or YES or NO
        assertSuccess("madhs1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'INVALIDIDNUMBER' } }]
        }),
        assertSuccess("madhs1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'ZAMBIAN GRAIN' } }]
        }),
        assertSuccess("madhs1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'IHQ123' } }]
        }),
        assertSuccess("madhs1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'SDA' } }]
        }),
        assertFailure("madhs1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'FAIL' } }]
        }),
        assertSuccess("madhs1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ AdHocRequirement: { Subject: '' } }] }),
        assertSuccess("madhs1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}] }),
        //May not be completed
        assertSuccess("madhs2", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("madhs2", {
          ReportingQualifier: 'NON REPORTABLE',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'INVALIDIDNUMBER' } }]
        }),
        //If the Subject contains the value AIRPORT the Flow must be IN
        assertSuccess("madhs3", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'AIRPORT' } }]
        }),
        assertFailure("madhs3", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'AIRPORT' } }]
        }),
        //If the Subject is IHQnnn the AccountIdentifier must be FCA RESIDENT
        //assertSuccess("madhs4", {
        //  ReportingQualifier: 'BOPCUS', Resident: {Individual: {AccountIdentifier: 'FCA RESIDENT'}},
        //  MonetaryAmount: [{AdHocRequirement: {Subject: "IHQ123"}}]
        //}),
        //assertFailure("madhs4", {
        //  ReportingQualifier: 'BOPCUS', Resident: {Individual: {AccountIdentifier: 'NON RESIDENT OTHER'}},
        //  MonetaryAmount: [{AdHocRequirement: {Subject: "IHQ123"}}]
        //}),
        //If the value is SETOFF, the CategoryCode and SubCategory must be 100 or 101/01 to 101/11 or 102/01 to 102/11 103/01 to 103/11 or 104/01 to 104/11 or 105 or 106 or 107 or 108
        assertNoRule("madhs5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            CategoryCode: '101',
            CategorySubCode: '05',
            AdHocRequirement: { Subject: 'SETOFF' }
          }]
        }),
        assertNoRule("madhs5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '105', AdHocRequirement: { Subject: 'SETOFF' } }]
        }),
        assertFailure("madhs5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '401', AdHocRequirement: { Subject: 'SETOFF' } }]
        }),
        //If the value is SETOFF, the Flow must be IN
        assertSuccess("madhs6", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'SETOFF' } }]
        }),
        assertFailure("madhs6", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'SETOFF' } }]
        }),
        //If the Value is ZAMBIAN GRAIN, the CategoryCode must be 101/01 or 109/01 or 110
        assertNoRule("madhs7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            CategoryCode: '109',
            CategorySubCode: '01',
            AdHocRequirement: { Subject: 'ZAMBIAN GRAIN' }
          }]
        }),
        assertNoRule("madhs7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '110', AdHocRequirement: { Subject: 'ZAMBIAN GRAIN' } }]
        }),
        assertFailure("madhs7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            CategoryCode: '101',
            CategorySubCode: '02',
            AdHocRequirement: { Subject: 'ZAMBIAN GRAIN' }
          }]
        }),
        //If the Flow is OUT and the CategoryCode is 511/01 to 511/07 or 512/01 to 512/07 or 513 and the Resident Entity element is used, the Subject must be YES or NO
        assertSuccess("madhs8", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '03', AdHocRequirement: { Subject: 'NO' } }]
        }),
        assertSuccess("madhs8", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '03', AdHocRequirement: { Subject: 'SETOFF' } }]
        }),
        assertFailure("madhs8", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '03', AdHocRequirement: { Subject: 'SETOFF' } }]
        }),
        //If Subject is AIRPORT, the CategoryCode must be 830, the Resident Entity RegistrationNumber must be GOVERNMENT and EntityName must be CORPORATION FOR PUBLIC DEPOSITS
        assertNoRule("madhs9", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { EntityName: 'CORPORATION FOR PUBLIC DEPOSITS', RegistrationNumber: 'GOVERNMENT' } },
          MonetaryAmount: [{ CategoryCode: '830', AdHocRequirement: { Subject: 'AIRPORT' } }]
        }),
        assertSuccess("madhs10", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { EntityName: 'CORPORATION FOR PUBLIC DEPOSITS', RegistrationNumber: 'GOVERNMENT' } },
          MonetaryAmount: [{ CategoryCode: '830', AdHocRequirement: { Subject: 'AIRPORT' } }]
        }),
        assertSuccess("madhs11", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { EntityName: 'CORPORATION FOR PUBLIC DEPOSITS', RegistrationNumber: 'GOVERNMENT' } },
          MonetaryAmount: [{ CategoryCode: '830', AdHocRequirement: { Subject: 'AIRPORT' } }]
        }),
        assertFailure("madhs9", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { EntityName: 'CORPORATION FOR PUBLIC DEPOSITS', RegistrationNumber: 'GOVERNMENT' } },
          MonetaryAmount: [{ CategoryCode: '512', AdHocRequirement: { Subject: 'AIRPORT' } }]
        }),
        assertFailure("madhs10", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { EntityName: 'CORPORATION FOR PUBLIC DEPOSITS', RegistrationNumber: '123' } },
          MonetaryAmount: [{ CategoryCode: '830', AdHocRequirement: { Subject: 'AIRPORT' } }]
        }),
        assertFailure("madhs11", {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { EntityName: 'FAIL', RegistrationNumber: 'GOVERNMENT' } },
          MonetaryAmount: [{ CategoryCode: '830', AdHocRequirement: { Subject: 'AIRPORT' } }]
        }),
        assertFailure("madhs14", {
          ReportingQualifier: 'BOPCUS',
          NonResident: { Exception: { ExceptionName: 'IHQ' } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'IHQ0000' } }]
        }),
        assertNoRule("madhs14", {
          ReportingQualifier: 'BOPDIR',
          NonResident: { Exception: { ExceptionName: 'IHQ' } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'IHQ1234' } }]
        }),
        assertSuccess("madhs14", {
          ReportingQualifier: 'BOPCUS',
          NonResident: { Exception: { ExceptionName: 'IHQ' } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'IHQ000' } }]
        }),

        assertSuccess("madhs16", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { RegistrationNumber: "2013/1234567/07" } },
          MonetaryAmount: [{
            SARBAuth: { SARBAuthAppNumber: 'XYZ' },
            AdHocRequirement: { Subject: 'IHQ123' }
          }]
        }),
        assertFailure("madhs16", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { RegistrationNumber: "2013/1234568/07" } },
          MonetaryAmount: [{
            SARBAuth: { SARBAuthAppNumber: 'XYZ' },
            AdHocRequirement: { Subject: 'IHQ125' }
          }]
        }),
        assertSuccess("madhs17", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            SARBAuth: { SARBAuthAppNumber: 'XYZ' },
            AdHocRequirement: { Subject: 'IHQ123' }
          }]
        }),
        assertFailure("madhs17", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            SARBAuth: { SARBAuthAppNumber: 'XYZ' },
            AdHocRequirement: { Subject: 'IHQ999' }
          }]
        }),
        // If Resident Registration Number is not an IHQ, the value can only be be IHQnnn if Non Resident Exception is IHQ
        assertSuccess("madhs19", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { RegistrationNumber: "2013/1234567/07" } },
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'IHQ123' }
          }]
        }),
        assertSuccess("madhs19", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { RegistrationNumber: "Not IHQ" } },
          NonResident: { Exception: { ExceptionName: 'IHQ' } },
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'IHQ123' }
          }]
        }),
        assertFailure("madhs19", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: { RegistrationNumber: "Not IHQ" } },
          NonResident: { Exception: { ExceptionName: 'Not IHQ' } },
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'IHQ123' }
          }]
        }),

        //AdHocRequirement. Subject: If the Flow is OUT and the Subject is REMITTANCE DISPENSATION, the {{LocalCurrencyName}} value may not exceed 5,000
        assertSuccess("madhs12", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' },
            RandValue: '5000.00'
          }]
        }),
        assertFailure("madhs12", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' },
            RandValue: '7000.00'
          }]
        }),
        assertFailure("madhs12", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' },
            RandValue: '4000.00'
          },
          {
            RandValue: '2000.00'
          }]
        }),
        assertSuccess("madhs12", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            RandValue: '4000.00'
          },
          {
            RandValue: '2000.00'
          }]
        }),
        /*          assertSuccess("madhs15", {
                    ReportingQualifier: 'BOPCUS',
                    Flow: 'OUT',
                    MonetaryAmount: [{
                      AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'},
                      SARBAuth: {RulingsSection: 'Circular 06/2019'}
                    }]
                  }),
                  assertFailure("madhs15", {
                    ReportingQualifier: 'BOPCUS',
                    Flow: 'OUT',
                    MonetaryAmount: [{
                      AdHocRequirement: {Subject: 'REMITTANCE DISPENSATIONSSS'},
                      SARBAuth: {RulingsSection: 'Circular 06/2019'}
                    }]
                  }),
        */
        //AdHocRequirement. Subject: If the Flow is IN and the Subject is REMITTANCE DISPENSATION on category 400, the {{LocalCurrencyName}} value may not exceed 5,000
        assertSuccess("madhs20", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '400',
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' },
            RandValue: '5000.00'
          }]
        }),
        assertFailure("madhs20", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '400',
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' },
            RandValue: '7000.00'
          }]
        }),
        assertFailure("madhs20", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '400',
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' },
            RandValue: '4000.00'
          },
          {
            RandValue: '2000.00'
          }]
        }),
        assertNoRule("madhs20", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '401',
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' },
            RandValue: '7000.00'
          }]
        }),

        assertNoRule("madhs21", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '400',
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' }
          }]
        }),
        assertFailure("madhs21", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '401',
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' }
          }]
        }),
        assertFailure("madhs22", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'abc' },
            SARBAuth: {RulingsSection: "Circular 06/2019"}
          }]
        }),
        assertFailure("madhs22", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'abc' },
            SARBAuth: {RulingsSection: "CIRCULAR 06/2019"}
          }]
        }),
        assertSuccess("madhs22", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' },
            SARBAuth: {RulingsSection: "CIRCULAR 06/2019"}
          }]
        }),


        // Money: AdHocRequirement.Description
        //If Subject is used, must be completed
        assertSuccess("madhd1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'AIRPORT', Description: 'XYZ' } }]
        }),
        assertFailure("madhd1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'AIRPORT', Description: '' } }]
        }),
        //If Subject is NO, the Description must be NONE
        assertSuccess("madhd2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'NO', Description: 'NONE' } }]
        }),
        assertFailure("madhd2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'NO', Description: 'FAIL' } }]
        }),
        //May not be completed
        assertSuccess("madhd3", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("madhd3", {
          ReportingQualifier: 'NON REPORTABLE',
          MonetaryAmount: [{ AdHocRequirement: { Description: 'NONE' } }]
        }),

        // Money: LocationCountry
        //Must be completed
        assertSuccess("mlc1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ LocationCountry: 'EU' }] }),
        assertFailure("mlc1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}] }),
        //Invalid SWIFT country code
        assertSuccess("mlc2", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ LocationCountry: 'ZA' }] }),
        assertFailure("mlc2", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ LocationCountry: 'ZZ' }] }),
        assertSuccess("mlc2", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ LocationCountry: 'ZA' }]
        }),
        assertFailure("mlc2", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ LocationCountry: 'ZZ' }]
        }),
        //SWIFT country code may not be ZA
        assertSuccess("mlc3", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ LocationCountry: 'US' }] }),
        assertFailure("mlc3", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ LocationCountry: 'ZA' }] }),
        assertSuccess("mlc3", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ LocationCountry: 'US' }]
        }),
        assertFailure("mlc3", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ LocationCountry: 'ZA' }]
        }),
        //The LocationCountry EU may only be used if the CategoryCode is 513
        assertNoRule("mlc4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '513', LocationCountry: 'EU' }]
        }),
        assertFailure("mlc4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '03', LocationCountry: 'EU' }]
        }),
        //Must not not be completed
        assertSuccess("mlc5", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("mlc5", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ LocationCountry: 'US' }] }),
        assertNoRule("mlc5", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{ LocationCountry: 'US' }] }),
        //Must be completed if ForeignCardHoldersPurchasesRandValue and/or ForeignCardHoldersCashWithdrawalsRandValue is greater 0.00
        assertSuccess("mlc6", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{ LocationCountry: '' }] }),
        //assertFailure("mlc6", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LocationCountry: 'US'}]}),
        assertSuccess("mlc6", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ LocationCountry: 'US', ForeignCardHoldersPurchasesRandValue: 1.15 }]
        }),
        assertSuccess("mlc6", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ LocationCountry: '', ForeignCardHoldersPurchasesRandValue: 0.00 }]
        }),
        //assertFailure("mlc6", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LocationCountry: 'US', ForeignCardHoldersPurchasesRandValue: 0.00}]}),
        assertFailure("mlc6", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ LocationCountry: '', ForeignCardHoldersPurchasesRandValue: 1.15 }]
        }),
        assertFailure("mlc6", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{
            LocationCountry: '',
            ForeignCardHoldersCashWithdrawalsRandValue: 2.85
          }]
        }),
        assertFailure("mlc6", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{
            LocationCountry: '',
            ForeignCardHoldersPurchasesRandValue: 1.15,
            ForeignCardHoldersCashWithdrawalsRandValue: 2.85
          }]
        }),
        //Must not be completed if ForeignCardHoldersPurchasesRandValue and ForeignCardHoldersCashWithdrawalsRandValue is equal to 0.00
        assertSuccess("mlc7", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{ LocationCountry: '' }] }),
        assertSuccess("mlc7", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ LocationCountry: '', ForeignCardHoldersPurchasesRandValue: 0.00 }]
        }),
        assertSuccess("mlc7", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{
            LocationCountry: '',
            ForeignCardHoldersCashWithdrawalsRandValue: 0.00
          }]
        }),
        assertFailure("mlc7", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ LocationCountry: 'US', ForeignCardHoldersPurchasesRandValue: 0.00 }]
        }),
        assertFailure("mlc7", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ LocationCountry: 'US' }]
        }),
        assertFailure("mlc8", {
          ReportingQualifier: 'BOPCUS',
          NonResident: { Exception: { ExceptionName: "IHQ" } },
          MonetaryAmount: [{ LocationCountry: 'US' }]
        }),
        assertNoRule("mlc8", {
          ReportingQualifier: 'BOPDIR',
          NonResident: { Exception: { ExceptionName: "IHQ" } },
          MonetaryAmount: [{ LocationCountry: 'US' }]
        }),
        assertSuccess("mlc8", {
          ReportingQualifier: 'BOPCUS',
          NonResident: { Exception: { ExceptionName: "IHQ" } },
          MonetaryAmount: [{ LocationCountry: 'ZA' }]
        }),
        assertNoRule("mlc8", {
          ReportingQualifier: 'BOPCUS',
          NonResident: { Exception: { ExceptionName: "IHQ" } },
          MonetaryAmount: [{}]
        }),

        // Money: ReversalTrnRefNumber
        //If CategoryCode 100 or 200 or 300 or 400 or 500 or 600 or 700 or 800 is used, it must be completed
        assertSuccess("mrtrn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '400', ReversalTrnRefNumber: 'XYZ' }]
        }),
        assertFailure("mrtrn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '400', ReversalTrnRefNumber: '' }]
        }),
        //If ReversalTrnRefNumber has a value, the CardChargeBack must be Y
        assertSuccess("mrtrn2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ CardChargeBack: 'Y', ReversalTrnRefNumber: 'XYZ' }]
        }),
        assertSuccess("mrtrn2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ CardChargeBack: 'Y', ReversalTrnRefNumber: '' }]
        }),
        assertSuccess("mrtrn2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ CardChargeBack: 'N', ReversalTrnRefNumber: '' }]
        }),
        assertSuccess("mrtrn2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ReversalTrnRefNumber: '' }]
        }),
        assertFailure("mrtrn2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ CardChargeBack: 'N', ReversalTrnRefNumber: 'XYZ' }]
        }),
        assertFailure("mrtrn2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ReversalTrnRefNumber: 'XYZ' }]
        }),
        //May not be completed
        assertNoRule("mrtrn3", { ReportingQualifier: 'INTERBANK', MonetaryAmount: [{}] }),
        assertSuccess("mrtrn3", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mrtrn3", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ReversalTrnRefNumber: 'XYZ' }]
        }),
        //Additional spaces identified in data content
        assertSuccess("mrtrn4", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ ReversalTrnRefNumber: 'XYZ' }] }),
        assertFailure("mrtrn4", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ ReversalTrnRefNumber: ' XYZ' }] }),
        //Unless CategoryCode 100 or 200 or 300 or 400 or 500 or 600 or 700 or 800 is used, this must not be completed
        assertSuccess("mrtrn5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '401', ReversalTrnRefNumber: '' }]
        }),
        assertFailure("mrtrn5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '401', ReversalTrnRefNumber: 'XYZ' }]
        }),
        assertSuccess("mrtrn6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '400', ReversalTrnRefNumber: 'XYZ' }]
        }),

        assertFailure("mrtrn6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '401', ReversalTrnRefNumber: 'XYZ' }]
        }),

        // Money: ReversalTrnSeqNumber
        //If CategoryCode 100 or 200 or 300 or 400 or 500 or 600 or 700 or 800 is used, it must be completed
        assertSuccess("mrtrs1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '400', ReversalTrnSeqNumber: 1 }]
        }),
        assertFailure("mrtrs1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ CategoryCode: '400' }] }),
        //If the ReversalTrnRefNumber has a value, it must be completed
        assertSuccess("mrtrs2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertSuccess("mrtrs2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ReversalTrnRefNumber: 'XYZ', ReversalTrnSeqNumber: 1 }]
        }),
        assertFailure("mrtrs2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ReversalTrnRefNumber: 'XYZ' }]
        }),
        assertFailure("mrtrs2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ReversalTrnSeqNumber: 1 }]
        }),
        //May not be completed
        assertNoRule("mrtrs3", { ReportingQualifier: 'INTERBANK', MonetaryAmount: [{}] }),
        assertSuccess("mrtrs3", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mrtrs3", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ReversalTrnSeqNumber: 1 }]
        }),

        assertSuccess("mrtrs4", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ CategoryCode: '400', ReversalTrnSeqNumber: 1 }]
        }),

        assertFailure("mrtrs4", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ CategoryCode: '401', ReversalTrnSeqNumber: 1}]
        }),

        // Money: BOPDIRTrnReference
        //May not be completed
        assertSuccess("mdirtr1", { ReportingQualifier: 'NON RESIDENT RAND', MonetaryAmount: [{}] }),
        assertFailure("mdirtr1", { ReportingQualifier: 'NON RESIDENT RAND', MonetaryAmount: [{ BOPDIRTrnReference: 'XYZ' }] }),


        // Money: "BOPDIR{{DealerPrefix}}Code"
        assertSuccess("mdircd2", { ReportingQualifier: 'NON RESIDENT RAND', MonetaryAmount: [{}] }),
        assertFailure("mdircd2", { ReportingQualifier: 'NON RESIDENT RAND', MonetaryAmount: [{ BOPDIRADCode: 'XYZ' }] }),
        assertNoRule("mdircd2", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ BOPDIRADCode: 'XYZ' }] }),


        // Money: BOPDIRADCode
        //If the Reporting Entity Code is not 304 or 305 it may not be completed
        assertSuccess("mdircd1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}] }),
        assertFailure("mdircd1", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ BOPDIRADCode: '123' }] }),

        // Money: ThirdParty.Individual.Surname
        //If CategoryCode 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, it must be completed
        assertSuccess("mtpisn1", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '04', ThirdParty: { Individual: { Surname: 'XYZ' } } }]
        }),
        assertFailure("mtpisn1", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '04', ThirdParty: { Individual: { Surname: '' } } }]
        }),
        assertFailure("mtpisn1", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '04', ThirdParty: { Entity: {} } }]
        }),
        assertFailure("mtpisn1", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '04' }]
        }),
        //If Individual ThirdParty Name contains a value, this must be completed
        assertSuccess("mtpisn2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Name: 'ABC', Surname: 'XYZ' } } }]
        }),
        assertFailure("mtpisn2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Name: 'ABC', Surname: '' } } }]
        }),
        //If the category is 256 and the PassportNumber under Resident Individual contains no value, this must be completed
        assertSuccess("mtpisn3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { PassportNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Surname: 'XYZ' } } }]
        }),
        assertSuccess("mtpisn3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { PassportNumber: '123' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Surname: 'XYZ' } } }]
        }),
        assertFailure("mtpisn3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { PassportNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Surname: '' } } }]
        }),
        //If the category is 255 or 256 and the Resident Entity element is completed, this must be completed
        assertSuccess("mtpisn4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Surname: 'XYZ' } } }]
        }),
        assertFailure("mtpisn4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Surname: '' } } }]
        }),
        assertFailure("mtpisn4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '256' }]
        }),
        //May not be equal to Resident EntityName
        assertSuccess("mtpisn5", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Surname: 'JONES' } } }]
        }),
        assertFailure("mtpisn5", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Surname: 'ACME' } } }]
        }),
        //If the SupplementaryCardIndicator is Y, it must be completed
        assertSuccess("mtpisn6", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'Y' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'JONES' } } }]
        }),
        assertSuccess("mtpisn6", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'N' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: '' } } }]
        }),
        assertFailure("mtpisn6", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'Y' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: '' } } }]
        }),
        //May not be completed
        assertSuccess("mtpisn7", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("mtpisn7", {
          ReportingQualifier: 'NON REPORTABLE',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'JONES' } } }]
        }),
        //If the Flow is IN and category 303 or 304 or 305 or 306 or 416 or 417 is used and Resident Entity is completed, then must be completed
        assertSuccess("mtpisn8", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '304', ThirdParty: { Individual: { Surname: 'JONES' } } }]
        }),
        assertSuccess("mtpisn8", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: '304', ThirdParty: { Individual: { Surname: '' } } }]
        }),
        assertFailure("mtpisn8", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '304', ThirdParty: { Individual: { Surname: '' } } }]
        }),
        //If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed
        assertSuccess("mtpisn9", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '04', ThirdParty: { Individual: { Surname: 'XYZ' } } }]
        }),
        assertFailure("mtpisn9", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '04', ThirdParty: { Individual: { Surname: '' } } }]
        }),
        assertFailure("mtpisn9", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '04', ThirdParty: { Entity: {} } }]
        }),
        assertFailure("mtpisn9", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '04' }]
        }),

        // If Flow is OUT and category is 511/01 to 511/07 and Subject is SDA and Resident Entity is used, Third Party Individual details must be provided
        assertSuccess("mtpi1", {
          ReportingQualifier: 'BOPCUS', Flow: "OUT",
          Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '511', CategorySubCode: '04',
            ThirdParty: { Individual: { Surname: '' } },
            AdHocRequirement: { Subject: 'SDA' }
          }]
        }),
        assertSuccess("mtpi1", {
          ReportingQualifier: 'BOPCUS', Flow: "OUT",
          Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '511', CategorySubCode: '04',
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' }
          }]
        }),
        assertSuccess("mtpi1", {
          ReportingQualifier: 'BOPCUS', Flow: "OUT",
          Resident: { Individual: {} },
          MonetaryAmount: [{
            CategoryCode: '511', CategorySubCode: '04',
            AdHocRequirement: { Subject: 'SDA' }
          }]
        }),
        assertFailure("mtpi1", {
          ReportingQualifier: 'BOPCUS', Flow: "OUT",
          Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '511', CategorySubCode: '04',
            AdHocRequirement: { Subject: 'SDA' }
          }]
        }),

        // failure("mtpisn10", 418, "If the Subject is REMITTANCE DISPENSATION the Individual ThirdParty Surname must not be completed",
        //   notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")

        assertSuccess("mtpisn10", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            ThirdParty: { Individual: { Surname: '' } },
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' }
          }]
        }),
        assertFailure("mtpisn10", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            ThirdParty: { Individual: { Surname: 'XYZ' } },
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' }
          }]
        }),

        assertSuccess("mtpisn11", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'SDA' }, CategoryCode: '511', CategorySubCode: '04', ThirdParty: { Individual: { Surname: 'TestSurname' } } }]
        }),
        assertFailure("mtpisn11", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'SDA' }, CategoryCode: '511', CategorySubCode: '04', ThirdParty: { Individual: { Surname: '' } } }]
        }),
        assertNoRule("mtpisn11", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ AdHocRequirement: { Subject: 'SDA' }, CategoryCode: '101', CategorySubCode: '04', ThirdParty: { Individual: { Surname: 'TestSurname' } } }]
        }),


        // Money: ThirdParty.Individual.Name
        //If Individual ThirdParty Surname contains a value, this must be completed
        assertSuccess("mtpinm1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Name: 'ABC', Surname: 'XYZ' } } }]
        }),
        assertFailure("mtpinm1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Name: '', Surname: 'XYZ' } } }]
        }),
        //If the category is 256 and the PassportNumber under Resident Individual contains no value, this must be completed
        assertSuccess("mtpinm2", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { PassportNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Name: 'XYZ' } } }]
        }),
        assertSuccess("mtpinm2", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { PassportNumber: '123' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Name: 'XYZ' } } }]
        }),
        assertFailure("mtpinm2", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: { PassportNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Name: '' } } }]
        }),
        //If the category is 255 or 256 and the Resident Entity element is completed, this must be completed
        assertSuccess("mtpinm3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Name: 'XYZ' } } }]
        }),
        assertFailure("mtpinm3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Name: '' } } }]
        }),
        assertFailure("mtpinm3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '256' }]
        }),
        //May not be equal to Resident EntityName
        assertSuccess("mtpinm4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Name: 'JOHN' } } }]
        }),
        assertFailure("mtpinm4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: { EntityName: 'ACME' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Name: 'ACME' } } }]
        }),
        //May not be completed
        assertSuccess("mtpinm5", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpinm5", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ThirdParty: { Individual: { Name: 'JOHN' } } }]
        }),
        //If the SupplementaryCardIndicator is Y, it must be completed
        assertSuccess("mtpinm6", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'Y' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { Name: 'JOHN' } } }]
        }),
        assertSuccess("mtpinm6", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'N' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { Name: '' } } }]
        }),
        assertFailure("mtpinm6", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'Y' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { Name: '' } } }]
        }),

        assertSuccess("mtpinm7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Name: '' } }, AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),
        assertFailure("mtpinm7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Name: 'XYZ' } }, AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' } }]
        }),

        // Money: ThirdParty.Individual.Gender
        //If Individual ThirdParty Surname contains a value, this must be completed
        assertSuccess("mtpig1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Gender: 'M', Surname: 'XYZ' } } }]
        }),
        assertFailure("mtpig1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Gender: '', Surname: 'XYZ' } } }]
        }),
        //Invalid gender
        assertSuccess("mtpig2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Gender: 'M' } } }]
        }),
        assertSuccess("mtpig2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Gender: 'F' } } }]
        }),
        assertFailure("mtpig2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Gender: 'Z' } } }]
        }),
        //If the category is 256 and the PassportNumber under Resident Individual contains no value, this must be completed
        assertSuccess("mtpig3", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { PassportNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Gender: 'M' } } }]
        }),
        assertSuccess("mtpig3", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { PassportNumber: '123' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Gender: 'M' } } }]
        }),
        assertFailure("mtpig3", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { PassportNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { Gender: '' } } }]
        }),
        //If the category is 255, this must be completed
        assertSuccess("mtpig4", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '255', ThirdParty: { Individual: { Gender: 'M' } } }]
        }),
        assertFailure("mtpig4", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '255', ThirdParty: { Individual: { Gender: '' } } }]
        }),
        assertSuccess("mtpig4", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: '255', ThirdParty: { Individual: { Gender: '' } } }]
        }),
        //May not be completed
        assertSuccess("mtpig5", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpig5", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ThirdParty: { Individual: { Gender: 'M' } } }]
        }),
        //If the SupplementaryCardIndicator is Y, it must be completed
        assertSuccess("mtpig6", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'Y' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { Gender: 'M' } } }]
        }),
        assertSuccess("mtpig6", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'N' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { Gender: '' } } }]
        }),
        assertFailure("mtpig6", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'Y' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { Gender: '' } } }]
        }),

        assertSuccess("mtpig7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Gender: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpig7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Gender: 'M' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),

        // Money: ThirdParty.Individual.IDNumber
        //If Individual ThirdParty Surname contains a value, either this or Individual ThirdParty TempResPermitNumber must be completed
        assertSuccess("mtpiid1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { IDNumber: '6811035039084', Surname: 'XYZ' } } }]
        }),
        assertSuccess("mtpiid1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { TempResPermitNumber: '123', Surname: 'XYZ' } } }]
        }),
        assertSuccess("mtpiid1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: {} } }]
        }),
        assertFailure("mtpiid1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' } } }]
        }),
        //If category 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed
        assertSuccess("mtpiid2", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '512',
            CategorySubCode: '04',
            ThirdParty: { Individual: { IDNumber: '6811035039084' } }
          }]
        }),
        assertFailure("mtpiid2", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '04', ThirdParty: { Individual: { IDNumber: '' } } }]
        }),
        //Invalid Individual ThirdParty IDNumber
        assertSuccess("mtpiid3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { IDNumber: '6811035039084' } } }]
        }),
        assertFailure("mtpiid3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { IDNumber: '6811035039085' } } }]
        }),
        assertFailure("mtpiid3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { IDNumber: '6808155032083' } } }]
        }),
        assertFailure("mtpiid3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { IDNumber: '0000000000000' } } }]
        }),
        assertSuccess("mtpiid3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            ThirdParty: { Individual: { IDNumber: '6811035039085' } },
            AdHocRequirement: { Subject: 'INVALIDIDNUMBER' }
          }]
        }),

        //May not be completed
        assertSuccess("mtpiid4", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpiid4", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ThirdParty: { Individual: { IDNumber: '6811035039084' } } }]
        }),
        //If the SupplementaryCardIndicator is Y, either this or Individual ThirdParty TempResPermitNumber must be completed
        assertSuccess("mtpiid5", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'Y' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { IDNumber: '6811035039084' } } }]
        }),
        assertSuccess("mtpiid5", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'N' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { TempResPermitNumber: '123' } } }]
        }),
        assertFailure("mtpiid5", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'Y' } },
          MonetaryAmount: [{ ThirdParty: { Individual: {} } }]
        }),
        //Must not be equal to IDNumber under Resident IndividualCustomer
        assertSuccess("mtpiid6", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { IDNumber: '1234567890' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { IDNumber: '6811035039084' } } }]
        }),
        assertSuccess("mtpiid6", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { IDNumber: '' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { IDNumber: '6811035039084' } } }]
        }),
        assertSuccess("mtpiid6", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { IDNumber: '1234567890' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { IDNumber: '' } } }]
        }),
        assertFailure("mtpiid6", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { IDNumber: '6811035039084' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { IDNumber: '6811035039084' } } }]
        }),
        // If the Subject is REMITTANCE DISPENSATION the Individual ThirdParty IDNumber must not be completed
        assertSuccess("mtpiid7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' },
            ThirdParty: { Individual: { IDNumber: '' } }
          }]
        }),
        assertSuccess("mtpiid7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { IDNumber: '6811035039084' } } }]
        }),
        assertFailure("mtpiid7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' },
            ThirdParty: { Individual: { IDNumber: '6811035039084' } }
          }]
        }),
        // If the Subject is SDA and the EntityCustomer element is completed in respect of any category the ThirdParty IDNumber must be completed
        assertSuccess("mtpiid8", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '256',
            AdHocRequirement: { Subject: 'SDA' },
            ThirdParty: { Individual: { IDNumber: '6811035039084' } }
          }]
        }),
        assertSuccess("mtpiid8", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: {} },
          MonetaryAmount: [{
            CategoryCode: '256',
            AdHocRequirement: { Subject: 'SDA' },
            ThirdParty: { Individual: { IDNumber: '' } }
          }]
        }),
        assertFailure("mtpiid8", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '513',
            AdHocRequirement: { Subject: 'SDA' },
            ThirdParty: { Individual: { IDNumber: '' } }
          }]
        }),
        assertSuccess("mtpiid8", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '256',
            AdHocRequirement: { Subject: 'OTHER' },
            ThirdParty: { Individual: { IDNumber: '' } }
          }]
        }),
        assertFailure("mtpiid8", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '256',
            AdHocRequirement: { Subject: 'SDA' },
            ThirdParty: { Individual: { IDNumber: '' } }
          }]
        }),

        assertSuccess("mtpiid9", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '511',
            CategorySubCode: '01',
            ThirdParty: { Individual: { IDNumber: "12344321" } }
          }]
        }),
        assertFailure("mtpiid9", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '516',
            ThirdParty: { Individual: { IDNumber: "" } }
          }]
        }),



        // Money: ThirdParty.Individual.DateOfBirth
        //If Individual ThirdParty Surname contains a value, this must be completed
        assertSuccess("mtpibd1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { DateOfBirth: "1968-11-03", Surname: 'XYZ' } } }]
        }),
        assertFailure("mtpibd1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' } } }]
        }),
        //If the category is 256 and the PassportNumber under Resident Individual contains no value, this must be completed
        assertSuccess("mtpibd2", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { PassportNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { DateOfBirth: "1968-11-03" } } }]
        }),
        assertSuccess("mtpibd2", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { PassportNumber: '123' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { DateOfBirth: "1968-11-03" } } }]
        }),
        assertFailure("mtpibd2", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { PassportNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: {} } }]
        }),
        //If the category is 255, this must be completed
        assertSuccess("mtpibd3", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '255', ThirdParty: { Individual: { DateOfBirth: "1968-11-03" } } }]
        }),
        assertFailure("mtpibd3", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '255', ThirdParty: { Individual: {} } }]
        }),
        assertSuccess("mtpibd3", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: '255', ThirdParty: { Individual: {} } }]
        }),
        //May not be completed
        assertSuccess("mtpibd4", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpibd4", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ThirdParty: { Individual: { DateOfBirth: "1968-11-03" } } }]
        }),
        //If the SupplementaryCardIndicator is Y, it must be completed
        assertSuccess("mtpibd5", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'Y' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { DateOfBirth: "1968-11-03" } } }]
        }),
        assertSuccess("mtpibd5", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'N' } },
          MonetaryAmount: [{ ThirdParty: { Individual: {} } }]
        }),
        assertFailure("mtpibd5", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'Y' } },
          MonetaryAmount: [{ ThirdParty: { Individual: {} } }]
        }),
        assertFailure("mtpibd5", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Individual: { SupplementaryCardIndicator: 'Y' } },
          MonetaryAmount: [{ ThirdParty: { Individual: {} } }]
        }),
        assertSuccess("mtpibd6", {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ ThirdParty: { Individual: { DateOfBirth: "1999-01-01" } } }]
        }),
        assertSuccess("mtpibd6", {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ ThirdParty: { Individual: { DateOfBirth: "1968-11-03" } } }]
        }),
        assertSuccess("mtpibd6", {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ ThirdParty: { Individual: { DateOfBirth: "2010-02-30" } } }]
        }),
        assertFailure("mtpibd6", {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ ThirdParty: { Individual: { DateOfBirth: "1968-13-03" } } }]
        }),
        assertFailure("mtpibd6", {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ ThirdParty: { Individual: { DateOfBirth: "1968-11-40" } } }]
        }),
        assertFailure("mtpibd6", {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ ThirdParty: { Individual: { DateOfBirth: "1968-11-32" } } }]
        }),
        assertFailure("mtpibd6", {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ ThirdParty: { Individual: { DateOfBirth: "1807-11-03" } } }]
        }),
        assertFailure("mtpibd6", {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ ThirdParty: { Individual: { DateOfBirth: "11968-11-03" } } }]
        }),
        assertFailure("mtpibd6", {
          ReportingQualifier: "BOPCUS",
          MonetaryAmount: [{ ThirdParty: { Individual: { DateOfBirth: "1968-11-033" } } }]
        }),

        assertSuccess("mtpibd7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { DateOfBirth: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpibd7", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { DateOfBirth: '2010-02-30' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.Individual.IDNumber
        //If Individual ThirdParty Surname contains a value, either this or Individual ThirdParty IDNumber must be completed
        assertSuccess("mtpitp1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { IDNumber: '6811035039084', Surname: 'XYZ' } } }]
        }),
        assertSuccess("mtpitp1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { TempResPermitNumber: '123', Surname: 'XYZ' } } }]
        }),
        assertSuccess("mtpitp1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: {} } }]
        }),
        assertFailure("mtpitp1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' } } }]
        }),
        //If category is 511/01 to 511/08 or 512/01 to 512/07 or 513 or 514/01 to 514/07 or 515/01 to 515/07 is used, this may not be completed
        assertFailure("mtpitp2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            CategoryCode: '512',
            CategorySubCode: '04',
            ThirdParty: { Individual: { TempResPermitNumber: '123' } }
          }]
        }),
        assertSuccess("mtpitp2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            CategoryCode: '512',
            CategorySubCode: '04',
            ThirdParty: { Individual: { TempResPermitNumber: '' } }
          }]
        }),
        //If the SupplementaryCardIndicator is Y, either this or Individual ThirdParty IDNumber must be completed
        assertSuccess("mtpitp3", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'Y' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { IDNumber: '6811035039084' } } }]
        }),
        assertSuccess("mtpitp3", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'N' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { TempResPermitNumber: '123' } } }]
        }),
        assertFailure("mtpitp3", {
          ReportingQualifier: 'BOPCARD RESIDENT', Resident: { Entity: { SupplementaryCardIndicator: 'Y' } },
          MonetaryAmount: [{ ThirdParty: { Individual: {} } }]
        }),
        //May not be completed
        assertSuccess("mtpitp4", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpitp4", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ThirdParty: { Individual: { TempResPermitNumber: '123' } } }]
        }),
        //Must not be equal to TempResPermitNumber under Resident IndividualCustomer
        assertSuccess("mtpitp5", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { TempResPermitNumber: '999' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { TempResPermitNumber: '123' } } }]
        }),
        assertSuccess("mtpitp5", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { TempResPermitNumber: '' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { TempResPermitNumber: '123' } } }]
        }),
        assertSuccess("mtpitp5", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { TempResPermitNumber: '999' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { TempResPermitNumber: '' } } }]
        }),
        assertFailure("mtpitp5", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { TempResPermitNumber: '123' } },
          MonetaryAmount: [{ ThirdParty: { Individual: { TempResPermitNumber: '123' } } }]
        }),

        assertSuccess("mtpitp6", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { TempResPermitNumber: '999' } },
          MonetaryAmount: [{
            ThirdParty: { Individual: { TempResPermitNumber: '' } },
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' }
          }]
        }),
        assertFailure("mtpitp6", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { TempResPermitNumber: '999' } },
          MonetaryAmount: [{
            ThirdParty: { Individual: { TempResPermitNumber: '123' } },
            AdHocRequirement: { Subject: 'REMITTANCE DISPENSATION' }
          }]
        }),
        //
        // failure("mtpitp7", 282, "If Individual ThirdParty Surname contains a value and the BoPCategory is 250 and the Flow is OUT, either IndividualThirdPartyTempResPermit Number or IndividualThirdPartyPassportNumber must be completed",
        //   isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname")).and(notMoneyField("ThirdParty.Individual.TempResPermitNumber").or(notMoneyField("ThirdParty.Individual.PassportNumber")))).onOutflow().onCategory("250").onSection("AB")


        assertSuccess("mtpitp7", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '250',
            ThirdParty: { Individual: { TempResPermitNumber: '123', Surname: "ABC" } }
          }]
        }),
        assertSuccess("mtpitp7", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '250',
            ThirdParty: { Individual: { PassportNumber: '123', Surname: "ABC" } }
          }]
        }),
        assertFailure("mtpitp7", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '250',
            ThirdParty: { Individual: { PassportNumber: '', Surname: "ABC" } }
          }]
        }),


        // assertSuccess("mtpitp5", {
        //   ReportingQualifier: 'BOPCUS', Resident: {Individual: {TempResPermitNumber: ''}},
        //   MonetaryAmount: [{ThirdParty: {Individual: {TempResPermitNumber: '123'}}}]
        // }),


        // Money: ThirdParty.Individual.PassportNumber
        //If the category is 256 and the PassportNumber under Resident Individual contains no value, this must be completed
        assertSuccess("mtpipn1", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { PassportNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { PassportNumber: '123' } } }]
        }),
        assertSuccess("mtpipn1", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { PassportNumber: '123' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: { PassportNumber: '123' } } }]
        }),
        assertFailure("mtpipn1", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { PassportNumber: '' } },
          MonetaryAmount: [{ CategoryCode: '256', ThirdParty: { Individual: {} } }]
        }),
        //If the category is 255, this must be completed
        assertSuccess("mtpipn2", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '255', ThirdParty: { Individual: { PassportNumber: '123' } } }]
        }),
        assertFailure("mtpipn2", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '255', ThirdParty: { Individual: {} } }]
        }),
        assertSuccess("mtpipn2", {
          ReportingQualifier: 'BOPCUS',
          Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: '255', ThirdParty: { Individual: {} } }]
        }),
        //May not be completed
        assertSuccess("mtpipn3", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpipn3", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ThirdParty: { Individual: { PassportNumber: '123' } } }]
        }),

        assertSuccess("mtpipn4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '255', ThirdParty: { Individual: { PassportNumber: '123' } } }],
          Resident: { Individual: { PassportNumber: '321' } }
        }),
        assertFailure("mtpipn4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '255', ThirdParty: { Individual: { PassportNumber: '123' } } }],
          Resident: { Individual: { PassportNumber: '123' } }
        }),
        assertSuccess("mtpipn5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { PassportNumber: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpipn5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { PassportNumber: '123' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertSuccess("mtpipn6", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '250',
            ThirdParty: { Individual: { PassportNumber: '123', Surname: "ABC" } }
          }]
        }),
        assertSuccess("mtpipn6", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '250',
            ThirdParty: { Individual: { TempResPermitNumber: '123', Surname: "ABC" } }
          }]
        }),
        assertFailure("mtpipn6", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '250',
            ThirdParty: { Individual: { TempResPermitNumber: '', Surname: "ABC" } }
          }]
        }),


        // Money: ThirdParty.Individual.PassportCountry
        //If the Individual ThirdParty PassportNumber contains a value, this must be completed
        assertSuccess("mtpipc1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { PassportNumber: '123', PassportCountry: 'ZA' } } }]
        }),
        assertFailure("mtpipc1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { PassportNumber: '123' } } }]
        }),
        //May not be completed
        assertSuccess("mtpipc2", { ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpipc2", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ThirdParty: { Individual: { PassportCountry: 'ZA' } } }]
        }),
        //Invalid SWIFT country code
        assertSuccess("mtpipc3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { PassportCountry: 'ZA' } } }]
        }),
        assertFailure("mtpipc3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { PassportCountry: 'ZZ' } } }]
        }),
        assertSuccess("mtpipc4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { PassportCountry: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpipc4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { PassportCountry: '123' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.Entity.Name
        //In the case of Business travel, category 255, this attribute must be used
        //assertSuccess("mtpenm1", {ReportingQualifier: 'BOPCUS',
        //    MonetaryAmount: [{CategoryCode: '255', ThirdParty: {Entity: {Name: 'XYZ'}}}]}),
        //assertFailure("mtpenm1", {ReportingQualifier: 'BOPCUS',
        //    MonetaryAmount: [{CategoryCode: '255', ThirdParty: {Entity: {}}}]}),
        //May not be completed
        assertSuccess("mtpenm2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpenm2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { Entity: { Name: 'XYZ' } } }]
        }),
        assertSuccess("mtpenm3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Entity: { Name: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpenm3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Entity: { Name: '123' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.Entity.RegistrationNumber
        //May not be completed
        assertSuccess("mtpern1", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpern1", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { Entity: { RegistrationNumber: '123' } } }]
        }),
        //Must not be equal to RegistrationNumber under Resident EntityCustomer
        assertSuccess("mtpern2", {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { RegistrationNumber: '999' } },
          MonetaryAmount: [{ ThirdParty: { Entity: { RegistrationNumber: '123' } } }]
        }),
        assertSuccess("mtpern2", {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { RegistrationNumber: '' } },
          MonetaryAmount: [{ ThirdParty: { Entity: { RegistrationNumber: '123' } } }]
        }),
        assertSuccess("mtpern2", {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { RegistrationNumber: '999' } },
          MonetaryAmount: [{ ThirdParty: { Entity: { RegistrationNumber: '' } } }]
        }),
        assertFailure("mtpern2", {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { RegistrationNumber: '123' } },
          MonetaryAmount: [{ ThirdParty: { Entity: { RegistrationNumber: '123' } } }]
        }),
        assertSuccess("mtpern3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Entity: { RegistrationNumber: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpern3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Entity: { RegistrationNumber: '123' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.CustomsClientNumber
        //CustomsClientNumber must be numeric and contain between 8 and 13 digits
        assertSuccess("mtpccn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: '12345678' } }] //ccn
        }),
        assertSuccess("mtpccn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: '0254089063' } }] //tax
        }),
        assertSuccess("mtpccn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: '6811035039084' } }] //id
        }),
        assertSuccess("mtpccn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: '' } }]
        }),
        assertFailure("mtpccn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: 'A2345678' } }]
        }),
        //The value 70707070 may not be used
        assertSuccess("mtpccn2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: '12345678' } }]
        }),
        assertFailure("mtpccn2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: '70707070' } }]
        }),
        //May not be completed
        assertSuccess("mtpccn3", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpccn3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: '123' } }]
        }),

        assertSuccess("mtpccn4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: '' }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpccn4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: '123' }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),

        assertSuccess("mtpccn5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: '12345678' } }] // valid ccn
        }),
        assertSuccess("mtpccn5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: '0254/089063' } }] // valid tax
        }),
        assertSuccess("mtpccn5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: '6811035039084' } }] // valid id number
        }),
        assertSuccess("mtpccn5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: '' } }]
        }),
        assertSuccess("mtpccn5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: 'A2345678' } }] // invalid CCN, but expect rule mtpccn1 to catch this case
        }),
        assertWarning("mtpccn5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { CustomsClientNumber: '6811035039085' } }] // Invalid ID Number
        }),

        // Money: ThirdParty.TaxNumber
        //If category 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed
        assertSuccess("mtptx1", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '04', ThirdParty: { TaxNumber: '123' } }]
        }),
        assertSuccess("mtptx1", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '04', ThirdParty: { TaxNumber: '' } }]
        }),
        assertFailure("mtptx1", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '512', CategorySubCode: '04', ThirdParty: { TaxNumber: '' } }]
        }),
        //May not be completed
        assertSuccess("mtptx2", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("mtptx2", {
          ReportingQualifier: 'NON REPORTABLE',
          MonetaryAmount: [{ ThirdParty: { TaxNumber: '123' } }]
        }),
        //Must not be equal to TaxNumber under Resident
        assertSuccess("mtptx3", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { TaxNumber: '999' } },
          MonetaryAmount: [{ ThirdParty: { TaxNumber: '123' } }]
        }),
        assertSuccess("mtptx3", {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { TaxNumber: '999' } },
          MonetaryAmount: [{ ThirdParty: { TaxNumber: '123' } }]
        }),
        assertFailure("mtptx3", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { TaxNumber: '123' } },
          MonetaryAmount: [{ ThirdParty: { TaxNumber: '123' } }]
        }),
        assertFailure("mtptx3", {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { TaxNumber: '123' } },
          MonetaryAmount: [{ ThirdParty: { TaxNumber: '123' } }]
        }),
        //If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed
        assertSuccess("mtptx4", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '04', ThirdParty: { TaxNumber: '123' } }]
        }),
        assertSuccess("mtptx4", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: {} },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '04', ThirdParty: { TaxNumber: '' } }]
        }),
        assertFailure("mtptx4", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: {} },
          MonetaryAmount: [{ CategoryCode: '511', CategorySubCode: '04', ThirdParty: { TaxNumber: '' } }]
        }),
        assertSuccess("mtptx5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { TaxNumber: '' }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtptx5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { TaxNumber: '123' }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),

        // Money: ThirdParty.VATNumber
        //May not be completed
        assertSuccess("mtpvn1", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("mtpvn1", {
          ReportingQualifier: 'NON REPORTABLE',
          MonetaryAmount: [{ ThirdParty: { VATNumber: '123' } }]
        }),
        //Must not be equal to VATNumber under Resident
        assertSuccess("mtpvn2", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { VATNumber: '999' } },
          MonetaryAmount: [{ ThirdParty: { VATNumber: '123' } }]
        }),
        assertSuccess("mtpvn2", {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { VATNumber: '999' } },
          MonetaryAmount: [{ ThirdParty: { VATNumber: '123' } }]
        }),
        assertFailure("mtpvn2", {
          ReportingQualifier: 'BOPCUS', Resident: { Individual: { VATNumber: '123' } },
          MonetaryAmount: [{ ThirdParty: { VATNumber: '123' } }]
        }),
        assertFailure("mtpvn2", {
          ReportingQualifier: 'BOPCUS', Resident: { Entity: { VATNumber: '123' } },
          MonetaryAmount: [{ ThirdParty: { VATNumber: '123' } }]
        }),
        assertSuccess("mtpvn3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { VATNumber: '' }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpvn3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { VATNumber: '123' }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),

        // Money: ThirdParty.StreetAddress.AddressLine1
        //If category 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed
        assertSuccess("mtpsal11", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '512',
            CategorySubCode: '04',
            ThirdParty: { StreetAddress: { AddressLine1: 'ADDR' } }
          }]
        }),
        assertSuccess("mtpsal11", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: {} },
          MonetaryAmount: [{
            CategoryCode: '512',
            CategorySubCode: '04',
            ThirdParty: { StreetAddress: { AddressLine1: '' } }
          }]
        }),
        assertFailure("mtpsal11", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '512',
            CategorySubCode: '04',
            ThirdParty: { StreetAddress: { AddressLine1: '' } }
          }]
        }),
        //May not be completed
        assertSuccess("mtpsal12", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("mtpsal12", {
          ReportingQualifier: 'NON REPORTABLE',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: 'ADDR' } } }]
        }),
        //If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed
        assertSuccess("mtpsal13", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '511',
            CategorySubCode: '04',
            ThirdParty: { StreetAddress: { AddressLine1: 'ADDR' } }
          }]
        }),
        assertSuccess("mtpsal13", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: {} },
          MonetaryAmount: [{
            CategoryCode: '511',
            CategorySubCode: '04',
            ThirdParty: { StreetAddress: { AddressLine1: '' } }
          }]
        }),
        assertFailure("mtpsal13", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '511',
            CategorySubCode: '04',
            ThirdParty: { StreetAddress: { AddressLine1: '' } }
          }]
        }),
        assertSuccess("mtpsal14", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpsal14", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: 'ADDR' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.StreetAddress.AddressLine2
        //May not be completed
        assertSuccess("mtpsal21", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpsal21", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine2: 'ADDR' } } }]
        }),
        assertSuccess("mtpsal22", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine2: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpsal22", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine2: 'ADDR' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.StreetAddress.Suburb
        //If ThirdParty Street AddressLine1 has a value, this must be completed
        assertSuccess("mtpsas1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: 'ADDR', Suburb: 'SUB' } } }]
        }),
        assertSuccess("mtpsas1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: '', Suburb: '' } } }]
        }),
        assertFailure("mtpsas1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: 'ADDR', Suburb: '' } } }]
        }),
        //May not be completed
        assertSuccess("mtpsas2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpsas2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { Suburb: 'SUB' } } }]
        }),
        assertSuccess("mtpsas3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { Suburb: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpsas3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { Suburb: 'ADDR' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.StreetAddress.City
        //If ThirdParty Street AddressLine1 has a value, this must be completed
        assertSuccess("mtpsac1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: 'ADDR', City: 'CITY' } } }]
        }),
        assertSuccess("mtpsac1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: '', City: '' } } }]
        }),
        assertFailure("mtpsac1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: 'ADDR', City: '' } } }]
        }),
        //May not be completed
        assertSuccess("mtpsac2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpsac2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { City: 'CITY' } } }]
        }),
        assertSuccess("mtpsac3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { City: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpsac3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { City: 'ADDR' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.StreetAddress.Province
        //If ThirdParty Street AddressLine1 has a value, this must be completed
        assertSuccess("mtpsap1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: 'ADDR', Province: 'GAUTENG' } } }]
        }),
        assertSuccess("mtpsap1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: '', Province: '' } } }]
        }),
        assertFailure("mtpsap1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: 'ADDR', Province: '' } } }]
        }),
        //May not be completed
        assertSuccess("mtpsap2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpsap2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { Province: 'GAUTENG' } } }]
        }),
        //Must be either GAUTENG, LIMPOPO, NORTH WEST, WESTERN CAPE, EASTERN CAPE, NORTHERN CAPE, FREE STATE, MPUMALANGA or KWAZULU NATAL
        assertSuccess("mtpsap3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { Province: 'GAUTENG' } } }]
        }),
        assertFailure("mtpsap3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { Province: 'XXX' } } }]
        }),
        assertSuccess("mtpsap4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { Province: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpsap4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { Province: 'GAUTENG' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.StreetAddress.PostalCode
        //If ThirdParty Street AddressLine1 has a value, this must be completed
        assertSuccess("mtpsaz1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: 'ADDR', PostalCode: '2192' } } }]
        }),
        assertSuccess("mtpsaz1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: '', PostalCode: '' } } }]
        }),
        assertFailure("mtpsaz1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { AddressLine1: 'ADDR', PostalCode: '' } } }]
        }),
        //May not be completed
        assertSuccess("mtpsaz2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpsaz2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { PostalCode: '2192' } } }]
        }),
        //Invalid Postal Code
        assertSuccess("mtpsaz3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { PostalCode: '2192' } } }]
        }),
        assertSuccess("mtpsaz3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { PostalCode: '2' } } }]
        }),
        assertFailure("mtpsaz3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { PostalCode: 'A192' } } }]
        }),
        assertSuccess("mtpsaz3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { PostalCode: '000' } } }]
        }),
        assertSuccess("mtpsaz4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { PostalCode: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpsaz4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { StreetAddress: { PostalCode: '000' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.PostalAddress.AddressLine1
        //If category 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed
        assertSuccess("mtppal11", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '512',
            CategorySubCode: '04',
            ThirdParty: { PostalAddress: { AddressLine1: 'ADDR' } }
          }]
        }),
        assertSuccess("mtppal11", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Individual: {} },
          MonetaryAmount: [{
            CategoryCode: '512',
            CategorySubCode: '04',
            ThirdParty: { PostalAddress: { AddressLine1: '' } }
          }]
        }),
        assertFailure("mtppal11", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '512',
            CategorySubCode: '04',
            ThirdParty: { PostalAddress: { AddressLine1: '' } }
          }]
        }),
        //May not be completed
        assertSuccess("mtppal12", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("mtppal12", {
          ReportingQualifier: 'NON REPORTABLE',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: 'ADDR' } } }]
        }),
        //If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed
        assertSuccess("mtppal13", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '511',
            CategorySubCode: '04',
            ThirdParty: { PostalAddress: { AddressLine1: 'ADDR' } }
          }]
        }),
        assertSuccess("mtppal13", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Individual: {} },
          MonetaryAmount: [{
            CategoryCode: '511',
            CategorySubCode: '04',
            ThirdParty: { PostalAddress: { AddressLine1: '' } }
          }]
        }),
        assertFailure("mtppal13", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: { Entity: {} },
          MonetaryAmount: [{
            CategoryCode: '511',
            CategorySubCode: '04',
            ThirdParty: { PostalAddress: { AddressLine1: '' } }
          }]
        }),
        assertSuccess("mtppal14", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtppal14", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: 'ADDR' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.PostalAddress.AddressLine2
        //May not be completed
        assertSuccess("mtppal21", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtppal21", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine2: 'ADDR' } } }]
        }),
        assertSuccess("mtppal22", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine2: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtppal22", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine2: 'ADDR' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.PostalAddress.Suburb
        //If ThirdParty Street AddressLine1 has a value, this must be completed
        assertSuccess("mtppas1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: 'ADDR', Suburb: 'SUB' } } }]
        }),
        assertSuccess("mtppas1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: '', Suburb: '' } } }]
        }),
        assertFailure("mtppas1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: 'ADDR', Suburb: '' } } }]
        }),
        //May not be completed
        assertSuccess("mtppas2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtppas2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { Suburb: 'SUB' } } }]
        }),
        assertSuccess("mtppas3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { Suburb: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtppas3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { Suburb: 'SUB' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.PostalAddress.City
        //If ThirdParty Street AddressLine1 has a value, this must be completed
        assertSuccess("mtppac1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: 'ADDR', City: 'CITY' } } }]
        }),
        assertSuccess("mtppac1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: '', City: '' } } }]
        }),
        assertFailure("mtppac1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: 'ADDR', City: '' } } }]
        }),
        //May not be completed
        assertSuccess("mtppac2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtppac2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { City: 'CITY' } } }]
        }),
        assertSuccess("mtppac3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { City: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtppac3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { City: 'CITY' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.PostalAddress.Province
        //If ThirdParty Street AddressLine1 has a value, this must be completed
        assertSuccess("mtppap1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: 'ADDR', Province: 'GAUTENG' } } }]
        }),
        assertSuccess("mtppap1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: '', Province: '' } } }]
        }),
        assertFailure("mtppap1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: 'ADDR', Province: '' } } }]
        }),
        //May not be completed
        assertSuccess("mtppap2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtppap2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { Province: 'GAUTENG' } } }]
        }),
        //Must be either GAUTENG, LIMPOPO, NORTH WEST, WESTERN CAPE, EASTERN CAPE, NORTHERN CAPE, FREE STATE, MPUMALANGA or KWAZULU NATAL
        assertSuccess("mtppap3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { Province: 'GAUTENG' } } }]
        }),
        assertFailure("mtppap3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { Province: 'XXX' } } }]
        }),
        assertSuccess("mtppap4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { Province: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtppap4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { Province: 'GAUTENG' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.PostalAddress.PostalCode
        //If ThirdParty Street AddressLine1 has a value, this must be completed
        assertSuccess("mtppaz1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: 'ADDR', PostalCode: '2192' } } }]
        }),
        assertSuccess("mtppaz1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: '', PostalCode: '' } } }]
        }),
        assertFailure("mtppaz1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { AddressLine1: 'ADDR', PostalCode: '' } } }]
        }),
        //May not be completed
        assertSuccess("mtppaz2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtppaz2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { PostalCode: '2192' } } }]
        }),
        //Invalid Postal Code
        assertSuccess("mtppaz3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { PostalCode: '2192' } } }]
        }),
        assertSuccess("mtppaz3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { PostalCode: '2' } } }]
        }),
        assertFailure("mtppaz3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { PostalCode: 'A192' } } }]
        }),
        assertSuccess("mtppaz3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { PostalCode: '000' } } }]
        }),
        assertSuccess("mtppaz4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { PostalCode: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtppaz4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { PostalAddress: { PostalCode: '000' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.ContactDetails.ContactSurname
        //If Individual ThirdParty Surname or Entity ThirdParty Name has a value, this must be completed
        assertSuccess("mtpcds1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Entity: { Name: 'XYZ' }, ContactDetails: { ContactSurname: 'ABC' } } }]
        }),
        assertFailure("mtpcds1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Entity: { Name: 'XYZ' }, ContactDetails: { ContactSurname: '' } } }]
        }),
        assertFailure("mtpcds1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' }, ContactDetails: { ContactSurname: '' } } }]
        }),
        assertFailure("mtpcds1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' } } }]
        }),
        //May not be completed
        assertSuccess("mtpcds2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpcds2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { ContactSurname: 'ABC' } } }]
        }),
        assertSuccess("mtpcds3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { ContactSurname: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpcds3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { ContactSurname: 'XYZ' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.ContactDetails.ContactName
        //If Individual ThirdParty Surname or Entity ThirdParty Name has a value, this must be completed
        assertSuccess("mtpcdn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Entity: { Name: 'XYZ' }, ContactDetails: { ContactName: 'ABC' } } }]
        }),
        assertFailure("mtpcdn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Entity: { Name: 'XYZ' }, ContactDetails: { ContactName: '' } } }]
        }),
        assertFailure("mtpcdn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' }, ContactDetails: { ContactName: '' } } }]
        }),
        assertFailure("mtpcdn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' } } }]
        }),
        //May not be completed
        assertSuccess("mtpcdn2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpcdn2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { ContactName: 'ABC' } } }]
        }),
        assertSuccess("mtpcdn3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { ContactName: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpcdn3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { ContactName: 'XYZ' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.ContactDetails.Email
        //If Individual ThirdParty Surname or Entity ThirdParty Name has a value, either this or ThirdParty Fax or ThirdParty Telephone be must be completed
        assertSuccess("mtpcde1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Entity: { Name: 'XYZ' }, ContactDetails: { Email: 'a@b.com' } } }]
        }),
        assertSuccess("mtpcde1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' }, ContactDetails: { Email: 'a@b.com' } } }]
        }),
        assertSuccess("mtpcde1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' }, ContactDetails: { Fax: '0115559999' } } }]
        }),
        assertSuccess("mtpcde1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' }, ContactDetails: { Telephone: '0115559999' } } }]
        }),
        assertFailure("mtpcde1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Entity: { Name: 'XYZ' }, ContactDetails: {} } }]
        }),
        assertFailure("mtpcde1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' }, ContactDetails: {} } }]
        }),
        //May not be completed
        assertSuccess("mtpcde2", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("mtpcde2", {
          ReportingQualifier: 'NON REPORTABLE',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { Email: 'a@b.com' } } }]
        }),
        assertSuccess("mtpcde3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { Email: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpcde3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { Email: 'a@b.com' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.ContactDetails.Fax
        //If Individual ThirdParty Surname or Entity ThirdParty Name has a value, either this or ThirdParty Email or ThirdParty Telephone be must be completed
        assertSuccess("mtpcdf1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Entity: { Name: 'XYZ' }, ContactDetails: { Fax: '0115559999' } } }]
        }),
        assertSuccess("mtpcdf1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' }, ContactDetails: { Fax: '0115559999' } } }]
        }),
        assertSuccess("mtpcdf1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' }, ContactDetails: { Email: '0115559999' } } }]
        }),
        assertSuccess("mtpcdf1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' }, ContactDetails: { Telephone: '0115559999' } } }]
        }),
        assertFailure("mtpcdf1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Entity: { Name: 'XYZ' }, ContactDetails: {} } }]
        }),
        assertFailure("mtpcdf1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' }, ContactDetails: {} } }]
        }),
        //May not be completed
        assertSuccess("mtpcdf2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpcdf2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { Fax: '0115559999' } } }]
        }),
        assertSuccess("mtpcdf3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { Fax: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpcdf3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { Fax: '0115559999' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),


        // Money: ThirdParty.ContactDetails.Telephone
        //If Individual ThirdParty Surname or Entity ThirdParty Name has a value, either this or ThirdParty Email or ThirdParty Telephone be must be completed
        assertSuccess("mtpcdt1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Entity: { Name: 'XYZ' }, ContactDetails: { Telephone: '0115559999' } } }]
        }),
        assertSuccess("mtpcdt1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' }, ContactDetails: { Telephone: '0115559999' } } }]
        }),
        assertSuccess("mtpcdt1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' }, ContactDetails: { Email: '0115559999' } } }]
        }),
        assertSuccess("mtpcdt1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' }, ContactDetails: { Fax: '0115559999' } } }]
        }),
        assertFailure("mtpcdt1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Entity: { Name: 'XYZ' }, ContactDetails: {} } }]
        }),
        assertFailure("mtpcdt1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { Individual: { Surname: 'XYZ' }, ContactDetails: {} } }]
        }),
        //May not be completed
        assertSuccess("mtpcdt2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}] }),
        assertFailure("mtpcdt2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { Telephone: '0115559999' } } }]
        }),
        assertSuccess("mtpcdt3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { Telephone: '' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),
        assertFailure("mtpcdt3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ThirdParty: { ContactDetails: { Telephone: '0115559999' } }, AdHocRequirement: { Subject: "REMITTANCE DISPENSATION" } }]
        }),

        // Money: CardChargeBack
        //May not be completed
        assertSuccess("mcrdcb1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{}]
        }),
        assertFailure("mcrdcb1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CardChargeBack: 'Y' }]
        }),
        //Must be blank or contain value Y
        assertSuccess("mcrdcb2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{ CardChargeBack: 'Y' }] }),
        assertSuccess("mcrdcb2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{ CardChargeBack: 'N' }] }),
        assertSuccess("mcrdcb2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{ CardChargeBack: '' }] }),
        assertFailure("mcrdcb2", { ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{ CardChargeBack: 'X' }] }),
        //If CardChargeBack is Y, the Flow must be IN
        assertNoRule("mcrdcb3", {
          ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'IN',
          MonetaryAmount: [{ CardChargeBack: 'Y' }]
        }),
        assertFailure("mcrdcb3", {
          ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'OUT',
          MonetaryAmount: [{ CardChargeBack: 'Y' }]
        }),
        assertSuccess("mcrdcb3", {
          ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'OUT',
          NonResident: { Entity: { AccountIdentifier: "VISA NET" } },
          MonetaryAmount: [{ CardChargeBack: 'Y' }]
        }),

        // Money: CardIndicator


        //May not be completed
        assertSuccess("mcrdci1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{}]
        }),
        assertFailure("mcrdci1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CardIndicator: 'AMEX' }]
        }),
        //Must contain AMEX or DINERS or ELECTRON or MAESTRO or MASTER or VISA or BOCEXPRESS
        assertSuccess("mcrdci2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ CardIndicator: 'ELECTRON' }]
        }),
        assertFailure("mcrdci2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ CardIndicator: 'FAIL' }]
        }),

        // Money: ElectronicCommerceIndicator
        //Must be completed except if the Non Resident AccountIdentifier is VISA NET or MASTER SEND
        assertNoRule("mcrdec1", {
          ReportingQualifier: 'BOPCUS',
          NonResident: { Entity: { AccountIdentifier: "VISA NET" } },
          MonetaryAmount: [{ ElectronicCommerceIndicator: "Foo" }]
        }),
        assertSuccess("mcrdec1", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          NonResident: { Entity: { AccountIdentifier: "VISA NET" } },
          MonetaryAmount: [{ ElectronicCommerceIndicator: "Foo" }]
        }),
        assertSuccess("mcrdec1", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          NonResident: { Entity: { AccountIdentifier: "VISA NET" } },
          MonetaryAmount: [{ ElectronicCommerceIndicator: "" }]
        }),
        assertFailure("mcrdec1", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "" }]
        }),
        //Must not be completed
        assertSuccess("mcrdec2", { ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ ElectronicCommerceIndicator: "" }] }),
        assertFailure("mcrdec2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "Foo" }]
        }),
        assertNoRule("mcrdec2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "Foo" }]
        }),
        //Invalid ElectronicCommerceIndicator
        //VISA (Tests 1-13)
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "Foo", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "XX", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "00", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "01", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "02", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "03", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "04", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "05", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "06", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "07", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "08", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "09", CardIndicator: 'VISA' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "10", CardIndicator: 'VISA' }]
        }),

        //ELECTRON  (Tests 14-26)
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "Foo", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "XX", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "00", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "01", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "02", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "03", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "04", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "05", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "06", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "07", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "08", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "09", CardIndicator: 'ELECTRON' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "10", CardIndicator: 'ELECTRON' }]
        }),

        // MASTER CARD
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "0", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "1", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "2", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "3", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "X", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "4", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "5", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "6", CardIndicator: 'MASTER' }]
        }),

        //MAESTRO
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "0", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "1", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "2", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "3", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "X", CardIndicator: 'MAESTRO' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "4", CardIndicator: 'MAESTRO' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "5", CardIndicator: 'MAESTRO' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "6", CardIndicator: 'MAESTRO' }]
        }),

        //AMEX
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "05", CardIndicator: 'AMEX' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "06", CardIndicator: 'AMEX' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "07", CardIndicator: 'AMEX' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "04", CardIndicator: 'AMEX' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "03", CardIndicator: 'AMEX' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "02", CardIndicator: 'AMEX' }]
        }),

        //DINERS
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "05", CardIndicator: 'DINERS' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "06", CardIndicator: 'DINERS' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "07", CardIndicator: 'DINERS' }]
        }),
        assertSuccess("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "08", CardIndicator: 'DINERS' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "01", CardIndicator: 'DINERS' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "02", CardIndicator: 'DINERS' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "03", CardIndicator: 'DINERS' }]
        }),
        assertFailure("mcrdec3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ ElectronicCommerceIndicator: "04", CardIndicator: 'DINERS' }]
        }),



        // Money: POSEntryMode
        assertFailure("mcrdem1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ POSEntryMode: "Foo" }]
        }),
        assertNoRule("mcrdem1", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "Foo" }]
        }),
        // Must be completed except if the Non Resident AccountIdentifier is VISA NET or MASTER SEND
        assertSuccess("mcrdem2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "Foo" }]
        }),
        assertSuccess("mcrdem2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          NonResident: { Entity: { AccountIdentifier: "VISA NET" } },
          MonetaryAmount: [{ POSEntryMode: "Foo" }]
        }),
        assertFailure("mcrdem2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "" }]
        }),
        assertSuccess("mcrdem2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          NonResident: { Entity: { AccountIdentifier: "VISA NET" } },
          MonetaryAmount: [{ POSEntryMode: "" }]
        }),

        //VISA
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "Foo", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "XX", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "00", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "01", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "02", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "03", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "04", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "05", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "06", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "07", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "84", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "90", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "91", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "95", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "96", CardIndicator: 'VISA' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "10", CardIndicator: 'VISA' }]
        }),

        //MASTER CARD
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "00", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "20", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "01", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "21", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "02", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "22", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "06", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "26", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "0A", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "2A", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "0B", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "2B", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "1C", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "3C", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "1F", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "3F", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "1M", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "3M", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "1N", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "3N", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "9O", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "4O", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "9R", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "4R", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "9S", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "4S", CardIndicator: 'MASTER' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "9T", CardIndicator: 'MASTER' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "4T", CardIndicator: 'MASTER' }]
        }),


        //AMEX
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "Foo", CardIndicator: 'AMEX' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "00", CardIndicator: 'AMEX' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "11", CardIndicator: 'AMEX' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "22", CardIndicator: 'AMEX' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "33", CardIndicator: 'AMEX' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "44", CardIndicator: 'AMEX' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "55", CardIndicator: 'AMEX' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "66", CardIndicator: 'AMEX' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "77", CardIndicator: 'AMEX' }]
        }),

        //DINERS
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "Foo", CardIndicator: 'DINERS' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "00", CardIndicator: 'DINERS' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "11", CardIndicator: 'DINERS' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "22", CardIndicator: 'DINERS' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "33", CardIndicator: 'DINERS' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "44", CardIndicator: 'DINERS' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "95", CardIndicator: 'DINERS' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "S6", CardIndicator: 'DINERS' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "49", CardIndicator: 'DINERS' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "97", CardIndicator: 'DINERS' }]
        }),

        //MAESTRO
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "00", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "01", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "02", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "03", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "04", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "05", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "06", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "07", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "08", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "79", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "80", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "81", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "82", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "90", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "91", CardIndicator: 'MAESTRO' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "92", CardIndicator: 'MAESTRO' }]
        }),
        assertFailure("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "93", CardIndicator: 'MAESTRO' }]
        }),

        //ELECTRON
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "XX", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "01", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "02", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "03", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "04", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "05", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "06", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "07", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "10", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "81", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "84", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "86", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "90", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "91", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "95", CardIndicator: 'ELECTRON' }]
        }),
        assertSuccess("mcrdem3", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ POSEntryMode: "96", CardIndicator: 'ELECTRON' }]
        }),



        assertWarning("mcrdem4", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          NonResident: { Entity: { CardMerchantCode: "7995" } },
          MonetaryAmount: [{ POSEntryMode: "00", CardIndicator: "VISA" }]
        }),
        assertWarning("mcrdem4", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          NonResident: { Entity: { CardMerchantCode: "7800" } },
          MonetaryAmount: [{ POSEntryMode: "00", CardIndicator: "AMEX" }]
        }),

        //CardFraudulentTransactionIndicator
        assertSuccess("mcrdft1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CardFraudulentTransactionIndicator: '' }]
        }),
        assertFailure("mcrdft1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CardFraudulentTransactionIndicator: 'ABC' }]
        }),
        assertSuccess("mcrdft2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ CardFraudulentTransactionIndicator: 'Y' }]
        }),
        assertSuccess("mcrdft2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ CardFraudulentTransactionIndicator: 'N' }]
        }),
        assertFailure("mcrdft2", {
          ReportingQualifier: 'BOPCARD RESIDENT',
          MonetaryAmount: [{ CardFraudulentTransactionIndicator: 'ABC' }]
        }),

        //ForeignCardHoldersPurchasesRandValue
        assertSuccess("mcrdfp1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ForeignCardHoldersPurchasesRandValue: '' }]
        }),
        assertFailure("mcrdfp1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ForeignCardHoldersPurchasesRandValue: '10.0' }]
        }),
        assertSuccess("mcrdfp2", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ForeignCardHoldersPurchasesRandValue: '10.0' }]
        }),
        assertFailure("mcrdfp2", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ForeignCardHoldersPurchasesRandValue: '' }]
        }),
        assertSuccess("mcrdfp3", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ForeignCardHoldersPurchasesRandValue: '10.0' }]
        }),
        assertSuccess("mcrdfp3", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ForeignCardHoldersPurchasesRandValue: '0.00' }]
        }),
        assertFailure("mcrdfp3", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ForeignCardHoldersPurchasesRandValue: '-10.0' }]
        }),

        //ForeignCardHoldersCashWithdrawalsRandValue
        assertSuccess("mcrdfw1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ForeignCardHoldersCashWithdrawalsRandValue: '' }]
        }),
        assertFailure("mcrdfw1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ForeignCardHoldersCashWithdrawalsRandValue: '10.0' }]
        }),
        assertSuccess("mcrdfw2", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ForeignCardHoldersCashWithdrawalsRandValue: '10.0' }]
        }),
        assertFailure("mcrdfw2", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ForeignCardHoldersCashWithdrawalsRandValue: '' }]
        }),
        assertSuccess("mcrdfw3", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ForeignCardHoldersCashWithdrawalsRandValue: '10.0' }]
        }),
        assertFailure("mcrdfw3", {
          ReportingQualifier: 'BOPCARD NON RESIDENT',
          MonetaryAmount: [{ ForeignCardHoldersCashWithdrawalsRandValue: '-10.0' }]
        }),







        // NonResident.Entity.CardMerchantCode



        // Money: ImportExport
        //If the Flow is IN and category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, the ImportExport element must be completed
        assertSuccess("mtie1", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '103', CategorySubCode: '04', ImportExport: [{}] }]
        }),
        assertFailure("mtie1", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '103', CategorySubCode: '04' }]
        }),
        assertFailure("mtie1", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '106' }]
        }),
        assertSuccess("mtie1", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '106', ThirdParty: { CustomsClientNumber: "12345678" } }]
        }),
        assertSuccess("mtie1", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { CustomsClientNumber: "12345678" } },
          MonetaryAmount: [{ CategoryCode: '106' }]
        }),
        assertSuccess("mtie1", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '106', ThirdParty: { CustomsClientNumber: "12345678" } }]
        }),
        assertSuccess("mtie1", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { CustomsClientNumber: "12345678" } },
          MonetaryAmount: [{ CategoryCode: '106', ImportExport: [{}] }]
        }),
        assertSuccess("mtie1", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: { Individual: { CustomsClientNumber: "not LU" } },
          MonetaryAmount: [{ CategoryCode: '106', ThirdParty: { CustomsClientNumber: "12345678" }, ImportExport: [{}] }]
        }),
        assertSuccess("mtie1", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '106', ImportExport: [{}] }]
        }),

        //May not be completed
        assertSuccess("mtie2", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}] }),
        assertFailure("mtie2", { ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{ ImportExport: [{}] }] }),
        //Total PaymentValue of all ImportExport entries may not exceed a 1% variance with the RandValue or ForeignValue
        assertSuccess("mtie3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            RandValue: '100.00',
            ImportExport: [{ PaymentCurrencyCode: 'ZAR', PaymentValue: '50.00' }, {
              PaymentCurrencyCode: 'ZAR',
              PaymentValue: '49.50'
            }]
          }]
        }),
        assertSuccess("mtie3", {
          ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD',
          MonetaryAmount: [{
            ForeignValue: '100.00',
            ImportExport: [{ PaymentCurrencyCode: 'USD', PaymentValue: '50.00' }, {
              PaymentCurrencyCode: 'USD',
              PaymentValue: '49.50'
            }]
          }]
        }),
        assertSuccess("mtie3", {
          ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD',
          MonetaryAmount: [{ ForeignValue: '100.00' }]
        }),
        assertFailure("mtie3", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{
            RandValue: '100.00',
            ImportExport: [{ PaymentCurrencyCode: 'ZAR', PaymentValue: '50.00' }, {
              PaymentCurrencyCode: 'ZAR',
              PaymentValue: '52.00'
            }]
          }]
        }),
        assertFailure("mtie3", {
          ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD',
          MonetaryAmount: [{
            ForeignValue: '100.00',
            ImportExport: [{ PaymentCurrencyCode: 'ZAR', PaymentValue: '50.00' }, {
              PaymentCurrencyCode: 'ZAR',
              PaymentValue: '49.50'
            }]
          }]
        }),
        //If outflow and the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, the ImportExport element must be completed unless a) the Subject is SDA or REMITTANCE DISPENSATION or b) category 106 and import undertaking client
        assertSuccess("mtie4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '103', CategorySubCode: '04', ImportExport: [{}] }]
        }),
        assertFailure("mtie4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '103', CategorySubCode: '04' }]
        }),
        assertSuccess("mtie4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '103', CategorySubCode: '04', AdHocRequirement: { Subject: 'SDA' } }]
        }),
        assertSuccess("mtie4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '106', ImportExport: [{}] }]
        }),
        assertFailure("mtie4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '106' }]
        }),
        assertSuccess("mtie4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '106', AdHocRequirement: { Subject: 'SDA' } }]
        }),
        assertSuccess("mtie4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { CustomsClientNumber: "not LU" } },
          MonetaryAmount: [{ CategoryCode: '106', ThirdParty: { CustomsClientNumber: "12345678" }, ImportExport: [{}] }]
        }),
        assertSuccess("mtie4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { CustomsClientNumber: "not LU" } },
          MonetaryAmount: [{ CategoryCode: '106', ThirdParty: { CustomsClientNumber: "not LU" }, ImportExport: [{}] }]
        }),
        assertSuccess("mtie4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { CustomsClientNumber: "not LU" } },
          MonetaryAmount: [{ CategoryCode: '106', ThirdParty: { CustomsClientNumber: "12345678" } }]
        }),
        assertSuccess("mtie4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { CustomsClientNumber: "12345678" } },
          MonetaryAmount: [{ CategoryCode: '106' }]
        }),
        assertSuccess("mtie4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { CustomsClientNumber: "12345678" } },
          MonetaryAmount: [{ CategoryCode: '106', ImportExport: [{}] }]
        }),
        assertFailure("mtie4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { CustomsClientNumber: "12345678" } },
          MonetaryAmount: [{ CategoryCode: '106', ThirdParty: { CustomsClientNumber: "not LU" } }]
        }),
        assertFailure("mtie4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: { Individual: { CustomsClientNumber: "not LU" } },
          MonetaryAmount: [{ CategoryCode: '106', ThirdParty: { CustomsClientNumber: "not LU" } }]
        }),
        //For any category other than 101/01 to 101/11 or 103/01 to 103/11 or 105 or 106, the ImportExport element must not be completed
        assertSuccess("mtie5", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '401' }]
        }),
        assertNoRule("mtie5", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '103', CategorySubCode: '11' }]
        }),
        assertFailure("mtie5", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '401', ImportExport: [{}] }]
        }),
        // Must contain a sequential SubSequence entries that must start with the value 1
        assertSuccess("mtie6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{}]
        }),
        assertSuccess("mtie6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ SubSequence: '1' }, { SubSequence: '2' }] }]
        }),
        assertSuccess("mtie6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{}, {}] }]
        }),
        assertSuccess("mtie6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ SubSequence: '1' }, { SubSequence: '2' }] },
          { ImportExport: [{ SubSequence: '1' }] },
          { ImportExport: [{}] }]
        }),
        assertFailure("mtie6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ SubSequence: '2' }, { SubSequence: '3' }] }]
        }),
        assertFailure("mtie6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ SubSequence: '1' }, { SubSequence: '1' }] }]
        }),
        assertFailure("mtie6", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{}, { SubSequence: '2' }] }]
        }),


        // ImportExport: ImportControlNumber
        //Must be completed if the Flow is OUT and the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106
        assertSuccess("ieicn1", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '103', CategorySubCode: '04', ImportExport: [{ ImportControlNumber: 'XYZ' }] }]
        }),
        assertFailure("ieicn1", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '103', CategorySubCode: '04', ImportExport: [{ ImportControlNumber: '' }] }]
        }),
        assertSuccess("ieicn1", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '103',
            CategorySubCode: '04',
            AdHocRequirement: { Subject: 'SDA' },
            ImportExport: [{ ImportControlNumber: '' }]
          }]
        }),
        //For any category other than 101/01 to 101/11 or 103/01 to 103/11 or 105 or 106, this may not be completed
        assertSuccess("ieicn2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '511', ImportExport: [{ ImportControlNumber: '' }] }]
        }),
        assertFailure("ieicn2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '511', ImportExport: [{ ImportControlNumber: 'XYZ' }] }]
        }),
        //If the Flow is OUT and the category is 101/01 to 101/10, the first 3 characters must be INV followed by the invoice number. The minimum total number of characters must be 4
        assertSuccess("ieicn3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '101',
            CategorySubCode: '04',
            ImportExport: [{ ImportControlNumber: 'INV1' }]
          }]
        }),
        assertSuccess("ieicn3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '101',
            CategorySubCode: '04',
            ImportExport: [{ ImportControlNumber: 'INV1246KJDKJF' }]
          }]
        }),
        assertFailure("ieicn3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '04', ImportExport: [{ ImportControlNumber: 'INV' }] }]
        }),
        assertFailure("ieicn3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '101', CategorySubCode: '04', ImportExport: [{ ImportControlNumber: 'PS' }] }]
        }),
        assertSuccess("ieicn3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '101',
            CategorySubCode: '11',
            ImportExport: [{ ImportControlNumber: 'NOT INV' }]
          }, {
            CategoryCode: '101',
            CategorySubCode: '10',
            ImportExport: [{ ImportControlNumber: 'INV1246KJDKJF' }]
          }]
        }),
        assertNoRule("ieicn3", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '101',
            CategorySubCode: '11',
            ImportExport: [{ ImportControlNumber: 'NOT INV' }]
          }]
        }),



        //If the Flow is OUT and the category is 103/01 to 103/10 or 105 or 106, the format is as follows: AAACCYYMMDD0000000 where AAA is a valid customs office code in alpha format, CC is the century of import, YY is the year of import, MM is the month of import, DD is the day of import, and 0000000 is the 7 digit unique bill of entry number allocated by SARS as part of the MRN
        assertSuccess("ieicn4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '105', ImportExport: [{ ImportControlNumber: 'PEZ201302281234567' }] }]
        }),
        assertSuccess("ieicn4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '105', ImportExport: [{ ImportControlNumber: 'WOR201302281234567' }] }]
        }),
        assertFailure("ieicn4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '105', ImportExport: [{ ImportControlNumber: 'XXX201302281234567' }] }]
        }),
        assertFailure("ieicn4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '105', ImportExport: [{ ImportControlNumber: 'XXX201302301234567' }] }]
        }),
        assertFailure("ieicn4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '105', ImportExport: [{ ImportControlNumber: 'XXX188802281234567' }] }]
        }),
        assertFailure("ieicn4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ CategoryCode: '105', ImportExport: [{ ImportControlNumber: 'XXX18880228ABCDEFG' }] }]
        }),
        //May also not contain a comma (,)
        assertSuccess("ieicn5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ ImportControlNumber: 'WOR201302281234567' }] }]
        }),
        assertFailure("ieicn5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ ImportControlNumber: 'WOR2  01302281234567' }] }]
        }),
        assertFailure("ieicn5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ ImportControlNumber: ' WOR2 01302281 234567' }] }]
        }),
        assertFailure("ieicn5", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ ImportControlNumber: 'XXX2,01302281234567' }] }]
        }),
        assertSuccess("ieicn6", {
          ReportingQualifier: 'NON RESIDENT RAND',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '101',
            CategorySubCode: '101',
            ImportExport: [{ ImportControlNumber: 'WOR201302281234567' }]
          }]
        }),
        assertFailure("ieicn6", {
          ReportingQualifier: 'NON RESIDENT RAND',
          Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '101',
            CategorySubCode: '101',
            ImportExport: [{ ImportControlNumber: '' }]
          }]
        }),

        // assertFailure("ieicn6", {
        //   ReportingQualifier: 'NON RESIDENT RAND',
        //   MonetaryAmount: [{ImportExport: [{ImportControlNumber: 'WOR2  01302281234567'}]}]
        // }),

        // ImportExport: TransportDocumentNumber
        //Must be completed if the Flow is OUT and the category is 103/01 to 103/10, unless the Subject is SDA
        assertSuccess("ietdn1", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '103',
            CategorySubCode: '04',
            ImportExport: [{ TransportDocumentNumber: 'XYZ' }]
          }]
        }),
        assertFailure("ietdn1", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '103',
            CategorySubCode: '04',
            ImportExport: [{ TransportDocumentNumber: '' }]
          }]
        }),
        assertSuccess("ietdn1", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '103', CategorySubCode: '04',
            AdHocRequirement: { Subject: 'SDA' },
            ImportExport: [{ TransportDocumentNumber: '' }]
          }]
        }),
        //For any category other than 103/01 to 103/11 or 105 or 106, the this may not be completed
        assertSuccess("ietdn2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '511', ImportExport: [{ TransportDocumentNumber: '' }] }]
        }),
        assertFailure("ietdn2", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '511', ImportExport: [{ TransportDocumentNumber: 'XYZ' }] }]
        }),
        //Must be completed if the Flow is OUT and the category is 103/01 to 103/10
        assertSuccess("ietdn3", {
          ReportingQualifier: 'NON RESIDENT RAND', Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '103',
            CategorySubCode: '01',
            ImportExport: [{ TransportDocumentNumber: 'XYZ' }]
          }]
        }),
        assertFailure("ietdn3", {
          ReportingQualifier: 'NON RESIDENT RAND', Flow: 'OUT',
          MonetaryAmount: [{
            CategoryCode: '103',
            CategorySubCode: '01',
            ImportExport: [{ TransportDocumentNumber: '' }]
          }]
        }),
        //TransportDocumentNumber: May NOT be completed if the CustomsClientNumber is registered as an Imports Undertaking entity and the category is 105 or 106
        assertSuccess("ietdn4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '106', ImportExport: [{ TransportDocumentNumber: 'XYZ' }] }]
        }),
        assertSuccessCustom("ietdn4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '106', ImportExport: [{ TransportDocumentNumber: 'XYZ' }] }]
        }, { LUClient: 'N' }),
        assertFailureCustom("ietdn4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '106', ImportExport: [{ TransportDocumentNumber: 'XYZ' }] }]
        }, { LUClient: 'Y' }),
        assertSuccessCustom("ietdn4", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ CategoryCode: '106', ImportExport: [{ TransportDocumentNumber: '' }] }]
        }, { LUClient: 'Y' }),

        // ImportExport: UCR
        //If UCR contains a value and the Flow is IN, the minimum characters is 12 but not exceeding 35 characters and is in the following format: nZA12345678a...24a where n = last digit of the year, ZA = Fixed, 12345678 = Valid Customs Client Number, a...24a = consignment number
        assertSuccess("ieucr1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ ImportExport: [{ UCR: '3ZA12345678ABCDEFGHIJa1' }] }]
        }),
        assertFailure("ieucr1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ ImportExport: [{ UCR: '3ZA12345678' }] }]
        }),
        assertFailure("ieucr1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ ImportExport: [{ UCR: '3ZA12345678ABCDEFGHIJ123456789012345' }] }]
        }),
        assertSuccess("ieucr1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ ImportExport: [{ UCR: '3PQ12345678ABCDEFGHIJ' }] }] // Turned off format checking
        }),
        assertSuccess("ieucr1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ ImportExport: [{ UCR: 'PQ12345678ABCDEFGHIJ' }] }] // Turned off format checking
        }),
        assertSuccess("ieucr1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ ImportExport: [{ UCR: '3ZA12345678 ABCDEFGHIJ' }] }] // Turned off format checking
        }),
        assertFailure("ieucr1", {
          ReportingQualifier: 'BOPCUS',
          Flow: 'IN',
          MonetaryAmount: [{ ImportExport: [{ UCR: '3ZA12345678abcdefghijklmnopqrstuvwxy' }] }]
        }),
        //Must be completed if the Flow is IN and the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106
        assertSuccess("ieucr2", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{
            CategoryCode: '101',
            CategorySubCode: '04',
            ImportExport: [{ UCR: '3ZA12345678ABCDEFGHIJ' }]
          }]
        }),
        assertSuccess("ieucr2", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '105', ImportExport: [{ UCR: '3ZA12345678ABCDEFGHIJ' }] }]
        }),
        assertFailure("ieucr2", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '105', ImportExport: [{ UCR: '' }] }]
        }),
        assertSuccess("ieucr3", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '101', ImportExport: [{ UCR: '' }] }]
        }),
        assertFailure("ieucr3", {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{ CategoryCode: '101', ImportExport: [{ UCR: '3ZA12345678ABCDEFGHIJ' }] }]
        }),
        assertSuccess("ieucr4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ ImportExport: [{ UCR: '' }] }]
        }),
        assertFailure("ieucr4", {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [{ ImportExport: [{ UCR: '3ZA12345678ABCDEFGHIJ' }] }]
        }),
        // Third Party Details must be completed because CCN in UCR is different from resident CCN
        /*          assertSuccess("ieucr7", {
                    ReportingQualifier: 'BOPCUS', Flow: 'IN',
                    Resident: {Entity: {CustomsClientNumber: "01930824"}},
                    MonetaryAmount: [{ImportExport: [{UCR: '6ZA01930824ALMAE'}]}]
                  }),
                  assertSuccess("ieucr7", {
                    ReportingQualifier: 'BOPCUS', Flow: 'IN',
                    Resident: {Entity: {CustomsClientNumber: "01930824"}},
                    MonetaryAmount: [{ImportExport: [{UCR: '6ZA01930824ALMAE'},{UCR: '6ZA01930824ALMAE'}]}]
                  }),
                  assertSuccess("ieucr7", {
                    ReportingQualifier: 'BOPCUS', Flow: 'IN',
                    Resident: {Individual: {CustomsClientNumber: "01930824"}},
                    MonetaryAmount: [{ImportExport: [{UCR: '6ZA01930824ALMAE'}]}]
                  }),
                  assertSuccess("ieucr7", {
                    ReportingQualifier: 'BOPCUS', Flow: 'IN',
                    Resident: {Individual: {CustomsClientNumber: "01930824"}},
                    MonetaryAmount: [{ImportExport: [{UCR: '6ZA01930824ALMAE'},{UCR: '6ZA01930824ALMAE'}]}]
                  }),
                  assertSuccess("ieucr7", {
                    ReportingQualifier: 'BOPCUS', Flow: 'IN',
                    Resident: {Entity: {CustomsClientNumber: "01930824"}},
                    MonetaryAmount: [{ThirdParty: {CustomsClientNumber: "NotValidCCN"}, ImportExport: [{UCR: '6ZA01930824ALMAE'}]}]
                  }),
                  assertSuccess("ieucr7", {
                    ReportingQualifier: 'BOPCUS', Flow: 'IN',
                    Resident: {Entity: {CustomsClientNumber: "01930824"}},
                    MonetaryAmount: [{ThirdParty: {CustomsClientNumber: "55930825"}, ImportExport: [{UCR: '6ZA01930824ALMAE'},{UCR: '6ZA55930825ALMAE'}]}]
                  }),
                  assertWarning("ieucr7", {
                    ReportingQualifier: 'BOPCUS', Flow: 'IN',
                    Resident: {Entity: {CustomsClientNumber: "NotValidCCN"}},
                    MonetaryAmount: [{ImportExport: [{UCR: '6ZA01930824ALMAE'}]}]
                  }),
                  assertWarning("ieucr7", {
                    ReportingQualifier: 'BOPCUS', Flow: 'IN',
                    Resident: {Entity: {CustomsClientNumber: "NotValidCCN"}},
                    MonetaryAmount: [{ThirdParty: {CustomsClientNumber: "NotValidCCN"}, ImportExport: [{UCR: '6ZA01930824ALMAE'}]}]
                  }),*/

        // ImportExport: PaymentCurrencyCode
        //Must be completed
        assertSuccess("iepcc1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ PaymentCurrencyCode: 'ZAR' }] }]
        }),
        assertFailure("iepcc1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ PaymentCurrencyCode: '' }] }]
        }),

        // ImportExport: PaymentValue
        assertSuccess("iepv1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ PaymentValue: 'ZAR' }] }]
        }),
        assertFailure("iepv1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ PaymentValue: '' }] }]
        }),

        // ImportExport: MRNNotOnIVS
        assertSuccess("iemrn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ MRNNotOnIVS: 'Y' }] }]
        }),
        assertSuccess("iemrn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ MRNNotOnIVS: 'N' }] }]
        }),
        assertFailure("iemrn1", {
          ReportingQualifier: 'BOPCUS',
          MonetaryAmount: [{ ImportExport: [{ MRNNotOnIVS: 'M' }] }]
        }),


        //PaymentCurrencyCode of all ImportExport entries is not ZAR or does not match FlowCurrency
        assertSuccess("iepcc2", {
          ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD',
          MonetaryAmount: [{ ImportExport: [{ PaymentCurrencyCode: 'USD' }] }]
        }),
        assertSuccess("iepcc2", {
          ReportingQualifier: 'BOPCUS', FlowCurrency: 'ZAR',
          MonetaryAmount: [{ ImportExport: [{ PaymentCurrencyCode: 'ZAR' }] }]
        }),
        assertSuccess("iepcc2", {
          ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD',
          MonetaryAmount: [{ RandValue: '50.00', ImportExport: [{ PaymentCurrencyCode: 'ZAR' }] }]
        }),
        assertFailure("iepcc2", {
          ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD',
          MonetaryAmount: [{ ImportExport: [{ PaymentCurrencyCode: 'GBP' }] }]
        }),

        // Resident.Entity.EntityName
        assertFailure("relen6", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Entity: { EntityName: 'MUTUAL PARTY' } }
        }),
        assertSuccess("relen6", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Entity: { EntityName: 'SOMETHING ELSE' } }
        }),
        assertNoRule("relen6", {
          ReportingQualifier: "BOPCARD NON RESIDENT",
          Resident: { Entity: { EntityName: 'MUTUAL PARTY' } }
        }),
        assertSuccess("relen6", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Entity: { EntityName: 'STRATE' } }
        }),

        // Transaction Total{{LocalValue}}
        assertSuccess("tlv1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          TotalRandValue: 20000,
          MonetaryAmount: [
            { RandValue: 20000 }
          ]
        }),
        assertSuccess("tlv1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          TotalRandValue: 20000,
          MonetaryAmount: [
            { RandValue: 10000 },
            { RandValue: 10000 }
          ]
        }),
        assertWarning("tlv1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          TotalRandValue: 20000,
          MonetaryAmount: [
            { RandValue: 60000 }
          ]
        }),
        assertWarning("tlv1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          TotalRandValue: 20000,
          MonetaryAmount: [
            { RandValue: 60000 },
            { RandValue: 60000 }
          ]
        })
      ]

    }
    return testBase;
  }
})
