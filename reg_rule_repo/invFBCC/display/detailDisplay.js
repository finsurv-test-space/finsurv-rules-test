define(function () {
  return function (predef) {
    var display;

    with (predef) {
      display = {
        detailTrans: {
          ruleset: "invCCM Transaction Display Rules",
          scope: "transaction",
          fields: [
            {
              field: "NonResident.Type",
              display: [
                disable(
                  isTransactionFieldProvided("NonResident.Individual.Surname").or(
                    isTransactionFieldProvided("NonResident.Individual.Name")
                  ).or(
                    isTransactionFieldProvided("NonResident.Entity.EntityName")
                  ))
              ]
            },
            {
              field: "NonResident.Entity.EntityName",
              display: [
                show().onSection("ABCDG"),
                appendValue("%s", "Resident.Entity.EntityName", notMatchesTransactionField("Resident.Entity.EntityName").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y"))),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Entity.EntityName"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Entity.EntityName")))),
                disable(isTransactionFieldProvided("NonResident.Entity.EntityName"))
              ]
            },
            {
              field: "NonResident.Individual.Surname",
              display: [
                show().onSection("ABCDG"),
                appendValue("%s", "Resident.Individual.Surname", notMatchesTransactionField("Resident.Individual.Surname").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y"))),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Individual.Surname"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Individual.Surname")))),
                disable(isTransactionFieldProvided("NonResident.Individual.Surname"))
              ]
            },
            {
              field: "NonResident.Individual.Name",
              display: [
                show().onSection("ABCDG"),
                appendValue("%s", "Resident.Individual.Name", notMatchesTransactionField("Resident.Individual.Name").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y"))),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Individual.Name"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Individual.Name")))),
                disable(isTransactionFieldProvided("NonResident.Individual.Name"))
              ]
            },
            {
              field: "NonResident.Individual.MiddleNames",
              display: [
                show().onSection("ABCDG"),
                appendValue("%s", "Resident.Individual.MiddleNames", notMatchesTransactionField("Resident.Individual.MiddleNames").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y"))),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Individual.MiddleNames"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Individual.MiddleNames")))),
                disable(isTransactionFieldProvided("NonResident.Individual.MiddleNames"))
              ]
            },
            {
              field: "NonResident.Individual.Gender",
              display: [
                show().onSection("ABCDG"),
                appendValue("%s", "Resident.Individual.Gender", notMatchesTransactionField("Resident.Individual.Gender").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y"))),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Individual.Gender"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Individual.Gender")))),
                disable(isTransactionFieldProvided("NonResident.Individual.Gender"))
              ]
            },
            {
              field: "NonResident.Individual.AccountNumber",
              display: [
                show(),
                disable(isTransactionFieldProvided("NonResident.Individual.AccountNumber"))
              ]
            },
            {
              field: "NonResident.Individual.PassportNumber",
              display: [
                hide()
              ]
            },
            {
              field: "NonResident.Individual.PassportCountry",
              display: [
                hide()
              ]
            },
            {
              field: "NonResident.Individual.IsMutualParty",
              display: [
                hide(),
                show(hasTransactionField("Resident.Individual")).onInflow(),
                disable(isTransactionFieldProvided("NonResident.Individual.IsMutualParty")).onOutflow()
              ]
            },
            {
              field: "NonResident.Entity.IsMutualParty",
              display: [
                hide(),
                show(hasTransactionField("Resident.Entity")).onInflow(),
                disable(isTransactionFieldProvided("NonResident.Entity.IsMutualParty")).onOutflow()
              ]
            },
            {
              field: "NonResident.Entity.AccountNumber",
              display: [
                show(),
                disable(isTransactionFieldProvided("NonResident.Entity.AccountNumber"))
              ]
            },
            {
              field: "NonResident.Individual.Address.AddressLine1",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.AddressLine1", notMatchesTransactionField("Resident.Individual.StreetAddress.AddressLine1").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))),
                disable(isTransactionFieldProvided("NonResident.Individual.Address.AddressLine1"))
              ]
            },
            {
              field: "NonResident.Individual.Address.AddressLine2",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.AddressLine2", notMatchesTransactionField("Resident.Individual.StreetAddress.AddressLine2").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Individual.Address.Suburb",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.Suburb", notMatchesTransactionField("Resident.Individual.StreetAddress.Suburb").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))),
                disable(isTransactionFieldProvided("NonResident.Individual.Address.Suburb"))
              ]
            },
            {
              field: "NonResident.Individual.Address.City",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.City", notMatchesTransactionField("Resident.Individual.StreetAddress.City").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))),
                disable(isTransactionFieldProvided("NonResident.Individual.Address.City"))
              ]
            },
            {
              field: "NonResident.Individual.Address.State",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.Province", notMatchesTransactionField("Resident.Individual.StreetAddress.Province").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))),
                disable(isTransactionFieldProvided("NonResident.Individual.Address.State"))
              ]
            },
            {
              field: "NonResident.Individual.Address.PostalCode",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.PostalCode", notMatchesTransactionField("Resident.Individual.StreetAddress.PostalCode").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))),
                disable(isTransactionFieldProvided("NonResident.Individual.Address.PostalCode"))
              ]
            },
            {
              field: "NonResident.Individual.Address.Country",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.Country", notMatchesTransactionField("Resident.Individual.StreetAddress.Country").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                setValue("%s", "LocationCountry", isEmpty.and(notMatchesTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(hasResidentFieldValue("StreetAddress.Country", "ZA"))))),
                setValue("%s", "LocationCountry", isEmpty.and(hasTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N")))),
                disable(isTransactionFieldProvided("NonResident.Individual.Address.Country").or(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                enable(not(isTransactionFieldProvided("NonResident.Individual.Address.Country")).and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))))
              ]
            },
            {
              field: "NonResident.Entity.Address.AddressLine1",
              display: [
                setValue("%s", "Resident.Entity.StreetAddress.AddressLine1", notMatchesTransactionField("Resident.Entity.StreetAddress.AddressLine1").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))),
                disable(isTransactionFieldProvided("NonResident.Entity.Address.AddressLine1"))
              ]
            },
            {
              field: "NonResident.Entity.Address.AddressLine2",
              display: [
                setValue("%s", "Resident.Entity.StreetAddress.AddressLine2", notMatchesTransactionField("Resident.Entity.StreetAddress.AddressLine2").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Entity.Address.Suburb",
              display: [
                setValue("%s", "Resident.Entity.StreetAddress.Suburb", notMatchesTransactionField("Resident.Entity.StreetAddress.Suburb").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))),
                disable(isTransactionFieldProvided("NonResident.Entity.Address.Suburb"))
              ]
            },
            {
              field: "NonResident.Entity.Address.City",
              display: [
                setValue("%s", "Resident.Entity.StreetAddress.City", notMatchesTransactionField("Resident.Entity.StreetAddress.City").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))),
                disable(isTransactionFieldProvided("NonResident.Entity.Address.City"))
              ]
            },
            {
              field: "NonResident.Entity.Address.State",
              display: [
                setValue("%s", "Resident.Entity.StreetAddress.Province", notMatchesTransactionField("Resident.Entity.StreetAddress.Province").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))),
                disable(isTransactionFieldProvided("NonResident.Entity.Address.State"))
              ]
            },
            {
              field: "NonResident.Entity.Address.PostalCode",
              display: [
                setValue("%s", "Resident.Entity.StreetAddress.PostalCode", notMatchesTransactionField("Resident.Entity.StreetAddress.PostalCode").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))),
                disable(isTransactionFieldProvided("NonResident.Entity.Address.PostalCode"))
              ]
            },
            {
              field: "NonResident.Entity.Address.Country",
              display: [
                setValue("%s", "Resident.Entity.StreetAddress.Country", notMatchesTransactionField("Resident.Entity.StreetAddress.Country").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                setValue("%s", "LocationCountry", isEmpty.and(notMatchesTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(hasResidentFieldValue("StreetAddress.Country", "ZA"))))),
                setValue("%s", "LocationCountry", isEmpty.and(hasTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N")))),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))),
                disable(isTransactionFieldProvided("NonResident.Entity.Address.Country"))
              ]
            }
          ]
        },
        detailMoney: {
          ruleset: "invCCM Money Display Rules",
          scope: "money",
          fields: [
            {
              field: "ThirdParty",
              display: [
                hide(),
                show().onSection("AB").onCategory(["255", "256"])
              ]
            }
          ]
        },
        detailImportExport: {
          ruleset: "invCCM Import/Export Display Rules",
          scope: "importexport",
          fields: []

        }
      };
    }

    return display;
  }
});