define(function () {
  return function (predef) {
    var docs;
    with (predef) {
      docs = {
        ruleset: "Transaction Documents",
        scope: "transaction",
        validations: [

          {
            field: ["TrnReference"],
            rules: [
              ignore('dt-inv-rot')
            ]
          }
        ]
      };
    }
    return docs;
  }
});