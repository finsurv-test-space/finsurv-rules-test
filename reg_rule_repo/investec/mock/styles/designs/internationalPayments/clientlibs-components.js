
var queryVar;
var newProfileKo;

function Profile(item) {
    var self = this;
    self.profileId = item.ProfileId;                //": "84236847411",
    self.profileName = item.ProfileName;            //": "Johann Wilhelm Grosskopf",
    self.de_fault = item.Default;                    //": true,
    self.isFicaCompliant = item.IsFicaCompliant;    //": true,
    self.vasEnabled = item.VasEnabled;              //": true,
    self.vasKillSwitch = item.VasKillSwitch;        //": false,
    self.accounts = item.Accounts;                  //": 4
}

function Account(item) {
    var self = this;
    self.accountId = ko.observable(item.AccountId);                //": "84236847411-507796846",
    self.accountName = ko.observable(item.AccountName);            //": "Johann Wilhelm Grosskopf",
    self.accountNumber = ko.observable(item.AccountNumber);        //": 10010792651,
    self.accNameNumber = ko.observable(item.accNameNumber);        //": Mr Johann Wilhelm Grosskopf - 10010792651,
    self.isFCA = ko.observable(item.isFCA);                      //": false
}

function myProfileKo() {
    var self = this;

    //set to 3 as default to show the Platinum image as default
    self.currentAcc = ko.observable(3);
    //used to set the account
    self.accountType = ko.computed(function () {
        if (self.currentAcc() == 1) {
            return 'accTypeDiv_Business';
        }
        else if (self.currentAcc() == 2) {
            return 'accTypeDiv_Enigma';
        }
        else if (self.currentAcc() == 3) {
            return 'accTypeDiv_Platinum';
        }
        else if (self.currentAcc() == 4) {
            return 'accTypeDiv_Voyage';
        }
    });

    self.userProfileList = ko.observableArray();
    self.showUserProfileList = ko.observable(false);
    self.selectedProfile = ko.observable();
    self.selectedProfileName = ko.observable();

    //all accounts for GCN
    self.accountList = ko.observableArray();
    self.accountListFull = ko.observableArray();
    self.accountListTop5 = ko.observableArray();
    self.accountListOther = ko.observableArray();
    self.accountListAggregated = ko.observableArray();
    self.accountsLoading = ko.observable(true);

    //using a knockout extender here to track changes in the value
    self.selectedAccount = ko.observable();
    self.selectedAccountChange = ko.observable(false);
    self.apDisplayedAccount = ko.observable();
    self.apAccountNumber = ko.observable();

    self.apAccIsCFCFCA = ko.observable(false);
    self.apAccountName = ko.observable();
    self.apAvailable = ko.observable();
    self.apDisplayAvailable = ko.observable();
    self.apDisplayBalance = ko.observable();
    self.apProductName = ko.observable();
    self.profileType = ko.observable();

    //used to check if profile list is loaded
    self.profileListFinishedLoading = ko.observable(false);

    self.loadAccount = function (item) {
        /* newProfileKo.profileListFinishedLoading(false);
         newProfileKo.profileListFinishedLoading(true); */
        //set account change to true to trigger any functionality that needs to happen when it changes - then reset it at end of func
        if (item.accNameNumber == "Show more") {
            showAccListOther();
        } else if (item.accNameNumber == "Show other") {
            showAccListAggregated();
        } else {
            self.setSelectedAccount(item)

            newProfileKo.selectedAccountChange(true);
            newProfileKo.selectedAccountChange(false);

            //var x = '{\"accNum\":\"'+newProfileKo.apAccountNumber()+'\"}';
            //setSelectedAcc(x, newProfileKo.setSelAccLoadHandler, newProfileKo.handleSetSelAccLoadFail);
        }
    }


    self.setSelectedAccount = function (item) {
        self.selectedAccount(item);
        self.apAccountNumber(item.accountNumber());
        self.apAccountName(item.accountName());
        self.apAccIsCFCFCA(item.isFCA());
        //TODO: There is no available amount in the source account service
        self.apAvailable("0.00");//item.AvailableBalance);
        self.apDisplayAvailable("0.00");//formatMonetaryAmounts(item.AvailableBalance,"R"));
        //TODO: There is no BalanceAssets amount and BalanceLiabilities in the source account service  to calculate Balance
        /*var BalanceAssets = Number(item.BalanceAssets) - Number(item.BalanceLiabilities);
         if(BalanceAssets == null) {
         BalanceAssets = 0;
         }*/
        self.apDisplayBalance("0.00");
        //formatMonetaryAmounts(BalanceAssets, "R"));
        //TODO: There is no AccountType
        self.apProductName("AccountType not available");//item.AccountType);
    };

    //ACCOUNT LIST LOAD HANDLERS
    self.accountListLoadFail = ko.observable(false);
    self.handleAccountListLoad = function (data, profileId) {
        try {

            var thisData = eval(data);
            self.profileListFinishedLoading(false);
            if (thisData != null) {

                if (thisData.faultCode) {
                    if (thisData.faultCode == 300) {
                        document.location.href = "/content/investec/sso/en/desktop/logout.html";
                    }
                    else {

                    }
                }
                else {
                    self.accountListLoadFail(false);

                    var pbAccounts = thisData;

                    self.accountListFull(pbAccounts);

                    var tempArrPbAcc = [];

                    for (var x = 0; x < pbAccounts.length; x++) {
                        var accNameNum = pbAccounts[x].AccountName;

                        if (pbAccounts[x].AccountNumber) {
                            accNameNum = accNameNum + " - " + pbAccounts[x].AccountNumber;
                            pbAccounts[x].isFCA = false;
                        }
                        else {
                            pbAccounts[x].isFCA = true;
                        }

                        pbAccounts[x].accNameNumber = accNameNum;

                        tempArrPbAcc.push(new Account(pbAccounts[x]));
                    }

                    self.accountList(tempArrPbAcc);

                    var tempArrTop5 = [];
                    var tempArrTopOther = [];
                    if (self.accountList().length > 5) {
                        for (var i = 0; i < self.accountList().length; i++) {
                            if (i < 5) {
                                tempArrTop5.push(new Account(self.accountList()[i]));
                            }
                            else {
                                tempArrTopOther.push(new Account(self.accountList()[i]));
                            }
                        }

                        tempArrTop5.push(new Account({ "accNameNumber": "Show more" }));

                        self.accountListTop5(tempArrTop5);
                        self.accountListOther(tempArrTopOther);

                        if (self.accountListOther().length < 10) {
                            $(".accountListOtherContain").css("width", "302px");
                        }
                    }
                    else {
                        self.accountListTop5(self.accountList());
                    }

                    var item = tempArrPbAcc[0];

                    //set account change to true to trigger any functionality that needs to happen when it changes - then reset it at end of func
                    //self.selectedAccountChange(true);

                    self.setSelectedAccount(item);

                    //getSelectedAcc(self.getSelAccLoadHandler, self.getSelAccLoadFail);
                    //Only call after initial account load is done
                    self.profileListFinishedLoading(true);
                }
            }
            else {
                self.accountListLoadFail(true);
            }
        }
        catch (x) {
            self.accountListLoadFail(true);
        }
    }

    self.handleBalanceUpdate = function (data, item) {
        try {
            var thisData = eval(data);
            if (thisData != null) {
                if (thisData.faultCode) {
                    if (thisData.faultCode == 300) {
                        document.location.href = "/content/investec/sso/en/desktop/logout.html"
                    }
                } else {

                    var pbAccounts = thisData.PrivateBankAccounts;
                    //get active account from account list returned
                    var result = pbAccounts.filter(function (objAcc) {
                        return objAcc.AccountNumber == self.apAccountNumber();
                    });

                    item = result[0];
                    self.apAvailable(item.AvailableBalance);
                    self.apDisplayAvailable(formatMonetaryAmounts(item.AvailableBalance, "R"));
                    var BalanceAssets = Number(item.BalanceAssets) - Number(item.BalanceLiabilities);
                    if (BalanceAssets == null) {
                        BalanceAssets = 0;
                    }
                    self.apDisplayBalance(formatMonetaryAmounts(BalanceAssets, "R"));

                }
            } else {
                self.accountListLoadFail(true);
            }
        } catch (x) {
            self.accountListLoadFail(true);
        }
    }

    self.updateBalances = function () {
        getAccountsList(self.userProfileList()[0].profileId, self.handleBalanceUpdate, self.handleAccountListLoadFail);
    }

    self.handleAccountListLoadFail = function (item) {
        self.accountListLoadFail(true);
    }

    self.getProfilesLoadFail = ko.observable(false);
    self.getProfilesLoadHandler = function (data) {
        try {
            var thisData = eval(data);
            if (self.userProfileList().length != 0) {
                for (var x = 0; x < self.userProfileList().length; x++) {
                    if (self.userProfileList()[x].de_fault == true) {
                        self.selectedProfileName(self.userProfileList()[x].profileName);
                        self.selectedProfile(self.userProfileList()[x]);
                        showAccountsLoading();
                    }
                }
            }
            else {
                self.selectedProfileName("Error loading profile service");
                self.apAccountNumber("Error loading account profile");
                self.apProductName("Please try again later");
                self.selectedProfile({});
            }
            if (self.userProfileList().length > 1) {
                self.showUserProfileList(true);
            }
            else {
                self.showUserProfileList(false);
            }

            if (thisData != null) {

                if (thisData.faultCode) {
                    if (thisData.faultCode == 300) {
                        document.location.href = "/content/investec/sso/en/desktop/logout.html"
                    }
                }
                else {
                    getAccountsList(self.selectedProfile().profileId, self.handleAccountListLoad, self.handleAccountListLoadFail);

                }
            } else {
                self.getProfilesLoadFail(true);
                getAccountsList(self.selectedProfile().profileId, self.handleAccountListLoad, self.handleAccountListLoadFail);
            }
        } catch (x) {
            self.getProfilesLoadFail(true);
        }
    }

    self.handleSelProfLoadFail = function (item) {
        self.getProfilesLoadFail(true);
    }

    self.getSelAccLoadFail = ko.observable(false);
    self.getSelAccLoadHandler = function (data, item) {
        try {

            var thisData = eval(data);
            self.accountsLoading(false);
            if (thisData != null) {

                if (thisData.faultCode) {
                    if (thisData.faultCode == 300) {
                        document.location.href = "/content/investec/sso/en/desktop/logout.html"
                    }
                }
                else {
                    for (var i = 0; i < newProfileKo.accountList().length; i++) {
                        if (data.accNum == newProfileKo.accountList()[i].AccountNumber) {
                            newProfileKo.selectedAccount(newProfileKo.accountList()[i]);

                            newProfileKo.apAccountNumber(newProfileKo.selectedAccount().AccountNumber);
                            newProfileKo.apAccountName(newProfileKo.selectedAccount().AccountName);
                            newProfileKo.apAvailable(newProfileKo.selectedAccount().AvailableBalance);
                            newProfileKo.apDisplayAvailable(formatMonetaryAmounts(newProfileKo.selectedAccount().AvailableBalance, "R"));
                            var BalanceAssets = Number(newProfileKo.selectedAccount().BalanceAssets) - Number(newProfileKo.selectedAccount().BalanceLiabilities);
                            if (BalanceAssets == null) {
                                BalanceAssets = 0;
                            }
                            newProfileKo.apDisplayBalance(formatMonetaryAmounts(BalanceAssets, "R"));
                            newProfileKo.apProductName(newProfileKo.selectedAccount().AccountType);


                        }
                    }

                    self.selectedAccountChange(true);
                    self.profileListFinishedLoading(true);
                    self.selectedAccountChange(false);
                    //check if selected account needs auth
                    var msg = "{\"id\":\"" + newProfileKo.selectedProfile().profileId + "\", \"subid\":\"" + newProfileKo.selectedAccount().AccountNumberForRequests + "\"}";
                    getAuthorisationList(msg, self.getAuthListLoadHandler, self.handleGetAuthListLoadFail);
                }
            } else {
                self.getSelAccLoadFail(true);
                //check if selected account needs auth
                var msg = "{\"id\":\"" + newProfileKo.selectedProfile().profileId + "\", \"subid\":\"" + newProfileKo.selectedAccount().AccountNumberForRequests + "\"}";
                getAuthorisationList(msg, self.getAuthListLoadHandler, self.handleGetAuthListLoadFail);
                self.selectedAccountChange(true);
                self.profileListFinishedLoading(true);
                self.selectedAccountChange(false);
            }
        }
        catch (x) {
            self.getSelAccLoadFail(true);
        }
    }
    /*    self.openAccountList = ko.computed(function(){
            if (!self.accountsLoading()){
                setTimeout(function(){
                    $("#dropDownButtonAccountSelector").click();
                },500)
            }
        })*/
    self.handleSelAccLoadFail = function (item) {
        self.getSelAccLoadFail(true);
    }

    self.setSelAccLoadFail = ko.observable(false);
    self.setSelAccLoadHandler = function (data, item) {
        try {
            var thisData = eval(data);

            if (thisData != null) {

                if (thisData.faultCode) {
                    if (thisData.faultCode == 300) {
                        document.location.href = "/content/investec/sso/en/desktop/logout.html"
                    }
                }
                else {

                }
            } else {
                self.setSelAccLoadFail(true);
            }
        } catch (x) {
            self.setSelAccLoadFail(true);
        }
    }
    self.handleSetSelAccLoadFail = function (item) {
        self.setSelAccLoadFail(true);
    }

    self.firstLoad = ko.observable(true);

    self.needsAuth = ko.observable(false);
    self.authList = ko.observable();
    self.authListLoadComplete = ko.observable(false);
    self.getAuthListLoadFail = ko.observable(false);
    self.getAuthListLoadHandler = function (data, item) {
        try {
            var thisData = eval(data);

            if (thisData != null) {

                if (thisData.faultCode) {
                    if (thisData.faultCode == 300) {
                        document.location.href = "/content/investec/sso/en/desktop/logout.html"
                    }
                }
                else {


                    if (parseInt(data.NumberOfAuthorisationRequiredCount) > 0) {
                        self.needsAuth(true);

                        //remove the currently selected profile from the list of possible users to select to authorise
                        //if they need authorisation there is always an A list, but we need to check for the B list
                        for (var i = 0; i < data.AuthorisersListA.length; i++) {
                            if (data.AuthorisersListA[i].Name == self.selectedProfile().profileName) {
                                data.AuthorisersListA.splice(i, 1);
                            }
                        }
                        if (data.AuthorisersListB != null) {
                            for (var i = 0; i < data.AuthorisersListB.length; i++) {
                                if (data.AuthorisersListB[i].Name == self.selectedProfile().profileName) {
                                    data.AuthorisersListB.splice(i, 1);
                                }
                            }
                        }
                    }
                    else {
                        self.needsAuth(false);
                    }
                    self.authList(data);
                }
                self.authListLoadComplete(true);
            } else {
                self.getAuthListLoadFail(true);
                self.authListLoadComplete(true);
            }
        } catch (x) {
            self.getAuthListLoadFail(true);
            self.authListLoadComplete(true);
        }
    }
    self.handleGetAuthListLoadFail = function (item) {
        self.getAuthListLoadFail(true);
        self.authListLoadComplete(true);
    }


}

$(document).ready(function () {
    newProfileKo = new myProfileKo();
    if (newProfileKo.firstLoad() == true) {

        ko.applyBindings(newProfileKo, document.getElementById("profileSelectorContainer"));

        //getProfiles(newProfileKo.getProfilesLoadHandler, newProfileKo.handleSelProfLoadFail);
        newProfileKo.firstLoad(false);

        $('.closeLink').on('click', function (event) {
            event.stopPropagation();
            $.fancybox.close();
        });
    }
});

self.showAccListOther = function (item) {
    $("#accListOther").fancybox({
        'modal': true
    }).click();
}

self.showAccListAggregated = function (item) {
    $("#accListAggregated").fancybox({
        'modal': true
    }).click();
}

function showAccountsLoading() {
    newProfileKo.apAccountName("");
    newProfileKo.apAccountNumber("");
    newProfileKo.apProductName("");

    newProfileKo.accountsLoading(true);
}


function setSelectedProfile() {

    var foundDefault = false;
    for (var i = 0; i < newProfileKo.userProfileList().length; i++) {
        if (queryVar != null && queryVar != "" && newProfileKo.userProfileList()[i].profileId == queryVar) {
            newProfileKo.selectedProfileName(newProfileKo.userProfileList()[i].profileName);
            newProfileKo.selectedProfile(newProfileKo.userProfileList()[i]);
            getAccountsList(newProfileKo.userProfileList()[i].profileId, newProfileKo.handleAccountListLoad, newProfileKo.handleAccountListLoadFail);
            showAccountsLoading();
        }
        else {
            if (newProfileKo.userProfileList()[i].de_fault == true) {
                queryVar = newProfileKo.userProfileList()[i].profileId;
                newProfileKo.selectedProfileName(newProfileKo.userProfileList()[i].profileName);
                newProfileKo.selectedProfile(newProfileKo.userProfileList()[i]);
                getAccountsList(newProfileKo.userProfileList()[i].profileId, newProfileKo.handleAccountListLoad, newProfileKo.handleAccountListLoadFail);
                showAccountsLoading();
                foundDefault = true;
            }

            if (foundDefault == false) {
                queryVar = newProfileKo.userProfileList()[0].profileId;
                newProfileKo.selectedProfileName(newProfileKo.userProfileList()[0].ProfileName);
                newProfileKo.selectedProfile(newProfileKo.userProfileList()[0]);
                getAccountsList(newProfileKo.userProfileList()[0].profileId, newProfileKo.handleAccountListLoad, newProfileKo.handleAccountListLoadFail);
                showAccountsLoading();
            }
        }

        if (newProfileKo.userProfileList().length > 1) {
            newProfileKo.showUserProfileList(true);
        }
        else {
            newProfileKo.showUserProfileList(false);
        }


    }
}


self.popUpLoadAccount = function (item) {
    $.fancybox.close();
    loadAccount(item);
}


function getAccountsList(profileId, loadHandler, failHandler) {
    $.ajax({
        url: "/bin/restproxy/za/pb/internationalpayments/sourceaccounts/" + profileId,
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'csrfToken': $('meta[name=csrf-token]').attr('content')
        },
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            loadHandler(data, profileId);
        },
        error: function (i) {
            console.error("error: " + i);
            failHandler(profileId)
        }
    });
}

function getProfiles(loadHandler, failHandler) {
    $.ajax({
        url: "/bin/restproxy/za/pb/profiles",
        type: "GET",
        contentType: "application/json",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {

            var tempProfilesArray = [];

            $.each(data, function (i, item) {
                tempProfilesArray.push(new Profile(item));
            });

            newProfileKo.userProfileList(tempProfilesArray);

            loadHandler(data);
        },
        error: function (i) {
            failHandler(true)
        }
    })
}



function getSelectedAcc(loadHandler, failHandler) {
    $.ajax({
        url: "/content/investec/payments/en/service.pbPaymentProxy.json",
        type: "POST",
        data: '{"method":"getSelectedAcc"}',
        contentType: "application/json",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            loadHandler(data, item);
        },
        error: function () {
            failHandler(item)
        }
    })
}

function setSelectedAcc(item, loadHandler, failHandler) {
    $.ajax({
        url: "/content/investec/payments/en/service.pbPaymentProxy.json",
        type: "POST",
        data: '{"method":"setSelectedAcc", "JSONData":' + item + '}',
        contentType: "application/json",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            loadHandler(data, item);
        },
        error: function () {
            failHandler(item)
        }
    })
}

function getAuthorisationList(item, loadHandler, failHandler) {
    $.ajax({
        url: "/content/investec/payments/en/service.pbAccountListProxy.json",
        type: "POST",
        data: '{"method":"getAuthorisationList", "JSONData":' + item + '}',
        contentType: "application/json",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {
            loadHandler(data, item);
        },
        error: function () {
            failHandler(item)
        }
    })
}

function reloadAccount() {
    //RELOAD ACCOUNT LIST WITH UPDATED AVAILABLE BALANCES
    getAccountsList(queryVar, newProfileKo.handleAccountListLoad, newProfileKo.handleAccountListLoadFail);
    showAccountsLoading();
}

//gets any URL variables
function getQueryVariable(variable) {
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i = 0; i < vars.length; i++) {
        var pair = vars[i].split("=");
        if (pair[0] == variable) { return pair[1]; }
    }
    return (false);
}


