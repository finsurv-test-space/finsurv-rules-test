//format the display balance and available balance - add thousand separator, 'denom(R in most cases)' designation and make sure there are two decimal places on the end if it's a whole number 
function formatMonetaryAmounts(val, denom) {
    var amount = parseFloat(val).toFixed(2)
    amount = numberWithCommas(amount);
    amount = denom + amount;
    return amount;
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

//use this custom data-binding to ensure that only numbers are entered into a field 
ko.bindingHandlers.numeric = {
    init: function (element, valueAccessor) {

        $(element).on("keydown", function (event) {
            // Allow: backspace, delete, tab, escape, and enter
            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
                /*      // Allow: . ,
                 (event.keyCode == 188 || event.keyCode == 190 || event.keyCode == 110) || */
                // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                    event.preventDefault();
                }
            }
        });
    }
};

//same as above, but now it also allows decimal point and is fixed to two decimal places - this was a pain
ko.bindingHandlers.monetary = {
    init: function (element, valueAccessor) {

        $(element).on("keydown", function (event) {

            //only allow decimal to be entered once
            if (event.keyCode == 190 || event.keyCode == 110) {
                var str = event.target.value;
                var x = str.replace(/[^.]/g, "").length;

                if (x > 0) {
                    event.preventDefault();
                }
                else {
                    return;
                }
            }

            // Allow: backspace, delete, tab, escape, and enter
            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
                // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            else {
                // Ensure that it is a number and stop the keypress
                if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                    event.preventDefault();
                }
            }
            //cannot use toFixed(2) because that doesnt take into account the key that has JUST been pressed
            //this will stop them from entering any more than two numbers after the decimal place, but it will also disallow users from editing the values of the number BEFORE the decimal place, so commenting this out until solution is found
            /*if(event.srcElement.value.indexOf(".") != -1) {
                var x = event.srcElement.value.split(".");
                
                if(x[1].length ==2) {
                     event.preventDefault();
                }
                else {
                    return;
                }
            }*/


        });
    }
};

//monetary that allows decimal point and is fixed to two decimal places - it allows commas and period aswell
ko.bindingHandlers.monetaryPeriodComma = {
    /*init: function (element, valueAccessor) {
        
      $(element).on("keydown", function (event) {
              
              //only allow decimal to be entered once
              if(event.keyCode == 190 || event.keyCode == 110) {
                   var str = event.srcElement.value;
                   var x = str.replace(/[^.]/g, "").length;
   
                if(x > 0) {
                    event.preventDefault();
                }
                else { 
                    return; 
                }
              }
              
                // Allow: backspace, delete, tab, escape, and enter
                if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
                // Allow: Ctrl+A
                (event.keyCode == 65 && event.ctrlKey === true) ||
                	 // Allow: . ,
                (event.keyCode == 188 || event.keyCode == 190 || event.keyCode == 110 || event.keyCode == 109 || event.keyCode == 189) ||
                    // Allow: home, end, left, right
                (event.keyCode >= 35 && event.keyCode <= 39)) {
                    // let it happen, don't do anything
                        return;
                }
                else {
                    // Ensure that it is a number and stop the keypress
                    if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                        event.preventDefault();
                    }
                }
              
        });
    }*/
};
//use this custom data-binding to ensure no html tags are entered into an input field
ko.bindingHandlers.sanitise = {
    init: function (element, valueAccessor) {
        $(element).on("keydown", function (event) {
            //<, >, /
            if (event.keyCode == 188 && event.shiftKey == true || event.keyCode == 190 && event.shiftKey == true || event.keyCode == 191) {
                event.preventDefault();
            }
        });
    }
};

ko.bindingHandlers.currency = {
    symbol: ko.observable('R'),
    update: function (element, valueAccessor, allBindingsAccessor) {
        return ko.bindingHandlers.text.update(element, function () {
            var value = +(ko.utils.unwrapObservable(valueAccessor()) || 0),
                symbol = ko.utils.unwrapObservable(allBindingsAccessor().symbol === undefined
                            ? allBindingsAccessor().symbol
                            : ko.bindingHandlers.currency.symbol);
            return symbol + value.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, "$1,");
        });
    }
};

//sorts an array by numeric order
function sortByKey(array, key) {
    return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}

//fadeVisible animation for knockout show and hide
ko.bindingHandlers.fadeVisible = {
    init: function (element, valueAccessor) {
        // Initially set the element to be instantly visible/hidden depending on the value
        var value = valueAccessor();
        $(element).toggle(ko.utils.unwrapObservable(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable;
    },
    update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        // Whenever the value subsequently changes, slowly fade the element in or out
        var value = valueAccessor();
        ko.utils.unwrapObservable(value) ? $(element).fadeIn() : $(element).hide();
    }
};

function validateEmailAddress(item) {
    if (item != "" && item != null) {
        var reg = /^(([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+)?$/;
        var OK = reg.exec(item);
        if (!OK) {
            return false;
        }
        else {
            return true;
        }
    }
}

function validatePhoneNumber(item) {
    if (item != "" && item != null) {
        var reg = /^[0-9]{10}$/;
        var OK = reg.exec(item);
        if (!OK) {
            return false;
        }
        else {
            return true;
        }
    }
}

//format the display balance and available balance - add thousand separator, 'denom(R in most cases)' designation and make sure there are two decimal places on the end if it's a whole number 
function formatMonetaryAmounts(val, denom) {
    var amount = parseFloat(val).toFixed(2)
    amount = numberWithCommas(amount);
    amount = denom + amount;
    return amount;
}

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function getTodaysDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var today = dd + '/' + mm + '/' + yyyy;
    return today;
}
//
function validateDate(date) {
    // regular expression to match required date format - dd/mm/yyyy
    re = /^(0?[1-9]|[12][0-9]|3[01])[\/\-](0?[1-9]|1[012])[\/\-]\d{4}$/;
    match = false;

    if (date != '' && !date.match(re)) {
        match = false;
    }
    else {
        // Extract the string into month, date and year
        var pdate = date.split('/');

        var dd = parseInt(pdate[0]);
        var mm = parseInt(pdate[1]);
        var yy = parseInt(pdate[2]);
        // Create list of days of a month [assume there is no leap year by default]
        var ListofDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
        if (mm == 1 || mm > 2) {
            if (dd > ListofDays[mm - 1]) {
                match = false;
            }
            else {
                match = true;
            }
        }
        if (mm == 2) {
            var lyear = false;
            if ((!(yy % 4) && yy % 100) || !(yy % 400)) {
                lyear = true;
            }
            if ((lyear == false) && (dd >= 29)) {
                match = false;
            }
            else {
                match = true;
            }
            if ((lyear == true) && (dd > 29)) {
                match = false;
            }
            else {
                match = true;
            }
        }

    }

    return match;
}


//this function is used to check if a date occurs within the desired period - IE: past, present, or future
function validateDateOccurence(date, period) {
    var isValid = false;
    //this function assumes your date is formatted DD/MM/YYYY and has already been validated
    var splitDate = date.split("/");
    var transDate = new Date(splitDate[2], splitDate[1] - 1, splitDate[0]);
    var todaysDate = new Date();

    switch (period) {
        case "past":
            if (transDate < todaysDate) {
                isValid = true;
            }
            break;
        case "present":
            if (transDate == todaysDate) {
                isValid = true;
            }
            break;
        case "future":
            if (transDate > todaysDate) {
                isValid = true;
            }
            break;
    }

    return isValid;
}

function formatDate(newDate) {
    if (newDate != "") {
        var tempDateString = (newDate).split(" ");
        thisDate = new Date(tempDateString[0]);
        var formattedDay = String(thisDate.getDate());
        if (formattedDay.length == 1) {
            formattedDay = "0" + formattedDay;
        }
        var formattedMonth = String(thisDate.getMonth() + 1);
        if (formattedMonth.length == 1) {
            formattedMonth = "0" + formattedMonth;
        }

        return formattedDay + "/" + formattedMonth + "/" + thisDate.getUTCFullYear();
    }
    else {
        return "";
    }
}

function formatLongDate(newDate) {
    var monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
	                  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];

    if (newDate != "") {
        var tempDateString = (newDate).split(" ");
        thisDate = new Date(tempDateString[0]);
        var formattedDay = String(thisDate.getDate());
        if (formattedDay.length == 1) {
            formattedDay = "0" + formattedDay;
        }
        var formattedMonth = thisDate.getMonth();
        formattedMonth = monthNames[formattedMonth];

        return formattedDay + " " + formattedMonth + " " + thisDate.getUTCFullYear();
    }
    else {
        return "";
    }
}

function isValidInternationalPhonenumber(value) {
    return (/^\d{10,}$/).test(value.replace(/[\s()+\-\.]|ext/gi, ''));
}

function validateInternationalPhoneNumber(item) {
    if (item != "" && item != null) {
        var reg = /^[0\+][0-9]{9,18}$/;
        var OK = reg.exec(item);
        if (!OK) {
            return false;
        }
        else {
            return true;
        }
    }
}
var fancyBoxState = "close";
var submitName = "";
var submitVal = "";
var parentContext = "";
var DEFAULT_SESSION_TIMEOUT = "5";
var IsSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
var IsFirefox = typeof InstallTrigger !== 'undefined';
var IsOpera = !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
var IsChrome = !!window.chrome && !IsOpera;
var IsIE = /*@cc_on!@*/false || !!document.documentMode;

var CURRENCY_CODE = "ZAR", COUNTRY_CODE = "ZAF";
var ShowUxpGuideArray = [{ "Code": "DHB", "Name": "dashboard" },
                         { "Code": "MPP", "Name": "personal-portfolio" },
                         { "Code": "INS", "Name": "income-statement" },
                         { "Code": "STP", "Name": "account-overview" },
                         { "Code": "MFL", "Name": "overview" },
                         { "Code": "NONE", "Name": "NONE" }];
var GUIDECODE = "NONE";

window.onbeforeunload = null;

function OPENWINDOW(url, width, height) {
    window.open(url, "width=" + width + ",height=" + height + ",0,status=0,scrollbars=1");
}

$(document).ready(function () {
    //Enable on enter submit for otp forms
    $("body").on("keydown", "[name='sentOtp']", function (e) {
        var code = e.keyCode || e.which;
        if (code == 13) {
            submitSMSCode();
        }
    });

    if (!Modernizr.input.autofocus) {
        $(function () { $('[autofocus]').focus() });
    }

    var touched = false;
    var confirmExit = function () {
        if (touched === false) {
            return "";
        }
    };

    var printLightboxOnlyOn = function () {
        if ($('.secureMessageComposer').length) {
            $('#wrapper').addClass('doPrint');
            return;
        }
        $('#wrapper').addClass('doNotPrint');
    };

    var printLightboxOnlyOff = function () {
        $('#wrapper').removeClass('doNotPrint').removeClass('doPrint');
    };

    $('body').on('click', function () {
        touched = true;
        window.setTimeout(function () {
            touched = false;
        }, 500);
    });

    $('form:not([novalidate=novalidate])').on('submit', function () {
        var areAllDateFieldsValid = true;
        $('select', this).attr('required', 'required');
        $('.dateField', this).each(function () {
            areAllDateFieldsValid = areAllDateFieldsValid && isDateFieldValid(this);
        });
        return areAllDateFieldsValid && validateSortCode(this);
    }).h5Validate({
        focusout: false
    });

    $('.dateField').change(function () {
        isDateFieldValid(this);
    });

    $('select').on('focus', function () {
        $(this).removeAttr('required');
        return false;
    }).change(function () {
        $(this).attr('required', 'required');
    });

    $('.dateField').removeAttr('required').addClass('novalidate');

    function isDateFieldValid(dateField) {
        var isDateValid = true;
        var pattern = "(0[1-9]|1[0-9]|2[0-9]|3[01])/(0[1-9]|1[012])/[0-9]{4}";
        if ($(dateField).val().match(pattern)) {
            var dateEntered = parseDate($(dateField).val());
            var minDateStr = $(dateField).attr('data-min');
            var maxDateStr = $(dateField).attr('data-max');
            var errorFieldId = $(dateField).attr('data-h5-errorid');
            isDateValid = isDateValid && (minDateStr == undefined || dateEntered >= parseDate(minDateStr));
            isDateValid = isDateValid && (maxDateStr == undefined || dateEntered <= parseDate(maxDateStr));
        }
        $('#' + errorFieldId).toggle(!isDateValid);
        return isDateValid;
    }

    function validateSortCode(form) {
        var isSortCodeValid = true;
        if ($('.sortCode', form).val() != undefined) {
            var count = 3;

            $('.sortCode', form).each(function (i) {
                if ($("input[name=sortCodePart" + (i + 1) + "]").val().match($(this).attr('pattern'))) {
                    count--;
                }
            });
            isSortCodeValid = (count <= 0);
            $('#sortCodeError', form).toggle(!isSortCodeValid);
        }
        return isSortCodeValid;
    }

    $("input[name=sortCodePart3]").on('focusout', function (event) {
        return validateSortCode($(this).closest('form'));
    });

    $.ajaxSetup({
        cache: false,
        statusCode: {
            401: function () {
                window.onbeforeunload = null;
                window.location.reload();
            },
            500: function ajaxError() {
                window.onbeforeunload = null;
                window.location.reload();
            }
        }
    });

    $("form.lightbox input[type=submit]").on("click", function (event) {
        event.preventDefault();
        submitName = $(this).attr('name') ? $(this).attr('name') : 'noSubmitName';
        submitVal = $(this).val();
        var form = $(this).closest("form");
        parentContext = $(this).closest('.section');
        form.submit();
    });

    $(document).on("submit", "form.lightbox", function (event) {
        event.preventDefault();
        var form = $(this);
        if (fancyBoxState === 'close') {
            $.fancybox({
                modal: true,
                content: '<div class="loading">...</div>'
            });
        }
        else {
            $('.fancybox-inner').html('<div class="loading">...</div>');
        }
        $.ajax({
            type: 'POST',
            cache: false,
            url: $(form).attr('action') + '?rn=' + new Date().getTime(),
            data: $(form).serialize() + '&' + submitName + '=' + submitVal,
            success: function (data) {
                if (data.indexOf('redirect') == 0) {
                    $.fancybox.close(true);
                    window.onbeforeunload = null;
                    window.location = data.substring(9);
                    return;
                }
                window.onbeforeunload = confirmExit;
                printLightboxOnlyOn();
                var errorState = data.indexOf('IsNotValidated') > -1;
                if (errorState === true && fancyBoxState === 'close') {
                    $.fancybox.close(true);
                    window.onbeforeunload = null;
                    $(parentContext).html(data);
                }
                else {
                    if (fancyBoxState === 'close') {
                        $.fancybox({
                            modal: true,
                            onStart: function () {
                                fancyBoxState = 'open';
                            },
                            afterClose: function () {
                                fancyBoxState = 'close';
                            },
                            afterShow: function () {
                                try {
                                    if (handleFancyBoxLoad) {
                                        handleFancyBoxLoad();
                                    }
                                } catch (x) {

                                }
                            },
                            content: data
                        });
                    } else {
                        $('.fancybox-inner').html(data);
                    }
                }
            },
            error: function () {
                inv.modal.alert({
                    header: 'Error',
                    text: 'An error occurred, please try again.',
                    buttons: [{
                        'callback': function () { $('.modal').modal('hide'); }
                    }]
                });
            }
        });

    });


    $(document).on("submit", "form.lightbox_1", function (event) {
        event.preventDefault();
        var form = $(this);
        if (fancyBoxState === 'close') {
            $.fancybox({
                modal: true,
                content: '<div id="fancybox-loading2" class="loading_box"> <div class="margin-top-20"><table class="loadingTextContainer"><tr><td class="lockContainer"><div class="center-block"><div class="loading_lock"></div></td><td class="spinnerContainer"><div class="spinner"><div class="loading_clock_hidden"></div></div></td> </tr> <tr class="loadingTextContainer"> <td  style="border-bottom: 0px white" class="loadingTextContainer" colspan="2" ><div class="loading_text text-center">We are securely logging you<br/>in to Investec Online </div></td> </tr></table> </div></div>'
            });
        }
        else {
            $('.fancybox-inner').html('<div class="lockContainer center-block"><div class="loading_lock loading_text"><div>Loading.......pls wait</div></div></div>');
        }
        $.ajax({
            type: 'POST',
            cache: false,
            url: $(form).attr('action') + '?rn=' + new Date().getTime(),
            data: $(form).serialize() + '&' + submitName + '=' + submitVal,
            success: function (data) {
                if (data.indexOf('redirect') == 0) {
                    $.fancybox.close(true);
                    window.onbeforeunload = null;
                    window.location = data.substring(9);
                    return;
                }
                window.onbeforeunload = confirmExit;
                printLightboxOnlyOn();
                var errorState = data.indexOf('IsNotValidated') > -1;
                if (errorState === true && fancyBoxState === 'close') {
                    $.fancybox.close(true);
                    window.onbeforeunload = null;
                    $(parentContext).html(data);
                }
                else {
                    if (fancyBoxState === 'close') {
                        $.fancybox({
                            modal: true,
                            onStart: function () {
                                fancyBoxState = 'open';
                            },
                            afterClose: function () {
                                fancyBoxState = 'close';
                            },
                            afterShow: function () {
                                try {
                                    if (handleFancyBoxLoad) {
                                        handleFancyBoxLoad();
                                    }
                                } catch (x) {

                                }
                            },
                            content: data
                        });
                    } else {
                        $('.fancybox-inner').html(data);
                    }
                }
            },
            error: function () {
                inv.modal.alert({
                    header: 'Error',
                    text: 'An error occurred, please try again.',
                    buttons: [{
                        'callback': function () { $('.modal').modal('hide'); }
                    }]
                });
            }
        });
    });

    $(".closeLightbox").on("click", function (event) {
        event.preventDefault();
        $.fancybox.close(true);
        printLightboxOnlyOff();
        window.onbeforeunload = null;
    });

    // contextualHelp for Ipad
    $(".tooltip a").on("touchstart", function (e) {
        $(this).trigger("hover");
    });

    $(".tooltip a").on("touchend", function (e) {
        $(this).trigger("blur");
    });

    // Search help
    var lastLink;
    $(document).click(function (e) {
        if (!$(e.target).is('.helpMenu *')) {
            $(".helpMenu").removeClass('open');
        }
    });
    $(document).on('click', '.breadcrumbs a', function (event) {
        var link = $(this).attr("href").replace(/html$/i, 'ajax.html');
        $(".helpMenu .helpMenuBlock").load(link, initialiseHelp);
        event.preventDefault();
    });

    $(document).on('click', '.simpleNavigation a', function (event) {
        var link = $(this).attr("href").replace(/html$/i, 'ajax.html');
        $(".helpMenu .helpMenuBlock").load(link, initialiseHelp);
        event.preventDefault();
    });

    $(document).on('click', '.search-results a', function (event) {
        var link = $(this).attr("href").replace(/html$/i, 'ajax.html');
        $(".helpMenu .helpMenuBlock").load(link, initialiseHelp);
        event.preventDefault();
    });

    $(document).on('submit', 'form.search', function (event) {
        event.preventDefault();
        if ($(".helpMenu").hasClass('open') || $("#search").val() != null) {
            link = lastLink;
        } else {
            link = $(this).parents("form").attr("data-page-help").replace(/html$/i, 'ajax.html');
            lastLink = link;
        }
        $(".helpMenu").addClass('open');
        submitSearchForm($(this).attr("action").replace(/html$/i, 'ajax.html'));
    });

    // submit on enter
    $(document).on('keypress', ".helpMenu input", function (event) {
        if (event.keyCode === 13) {
            event.preventDefault();
            $(".helpMenu .helpMenuBlock").load(link, initialiseHelp);
            submitSearchForm($(this).parents("form.search").attr("action").replace(/html$/i, 'ajax.html'));
        }
    });


    $(".helpMenu .helpSearch").on('click', function (event) {
        var link = null;
        if ($(".helpMenu").hasClass('open') || $("#search").val()) {
            link = lastLink;
        } else {
            link = $(this).parents("form").attr("data-page-help").replace(/html$/i, 'ajax.html');
            lastLink = link;
        }
        $(".helpMenu .helpMenuBlock").load(link, initialiseHelp);
        $(".helpMenu").addClass('open');
        event.preventDefault();
    });

    $(".closeHelp").on("click", function (event) {
        $(".helpMenu").removeClass('open');
        event.preventDefault();
    });

    $(".helpButton .helpSearch").each(function (index) {
        var helpLink = $(this).attr("href");
        if (helpLink !== undefined) {
            helpLink = helpLink.replace(/html$/i, 'ajax.html');
            $(this).attr("href", helpLink);
        }
    });

    $(".helpButton .helpMenuBlock a").on('click', 'a', function (event) {
        var link = $(this).attr("href").replace(/html$/i, 'ajax.html');
        $(".helpMenu .helpMenuBlock").load(link, initialiseHelp);
        event.preventDefault();
    });

    $("div.calenderIcon").on('click', function () {
        $(this).siblings(".dateField").focus();
    });

    $("li.calenderIcon").on('click', function () {
        $(this).children(".dateField").focus();
    });

    function addDaysToDate(startDate, days) {
        var date = startDate;
        if (typeof startDate === 'string') {
            date = parseDate(startDate);
        }
        date.setDate(date.getDate() + days);
        return date;
    }

    // Default date picker
    $(".dateField").datepicker({
        dateFormat: "dd/mm/yy",
        beforeShow: function (element, datepicker) {
            var options = {};
            options.minDate = $(element).data('min');
            options.maxDate = $(element).data('max');
            return options;
        }
    });

    //Hover menu
    $("#pageContent ul li.hoverMenu").hover(
        function () {
            $(this).addClass("hover");
        },
        function () {
            $(this).removeClass("hover");
        }
    );

    $("#pageContent ul li input").hover(
        function () {
            $(this).addClass("hover");
        },
        function () {
            $(this).removeClass("hover");
        }
    );

    // Context menu positioning, IE7 only
    if (parseInt($('#pageContent .inline-form-nav').first().css('margin-left'), 10) === -80) {
        var $link, $width;
        $('#pageContent .inline-form-nav').each(function (index, el) {
            if (parseInt($(el).css('margin-left'), 10) === -80) {
                $link = $(el).prev('a.menuLink');
                $width = parseInt($link.width(), 10);
                $link.width($width);
                $link.html('<span>' + $link.html() + '</span>');
                $link.find('span').css('width', $width);
                $(el).css('margin-left', -$width - 10);
            }
        });
    }


    $("textarea#message").keypress(function (e) {
        var lengthF = $(this).val();
        if (lengthF.length > 499) {
            e.preventDefault();
        }
    });
});

function submitSearchForm(linkURL) {
    var link = linkURL;
    var query = $(".helpMenu input#search").val();
    query = encodeURIComponent(query);
    link += "?q=" + query;
    $(".helpMenu .helpMenuBlock").load(link, initialiseHelp);
    // prevent default action
    return false;
}

function initialiseHelp() {
    // on load, attach events
    var helpListItems = $(".helpMenu .componentContent > ul > li");
    helpListItems.removeClass("active");
    helpListItems.mouseover(function (event) {
        helpListItems.removeClass("active");
        $(this).addClass("active");
    });

    // focus on load
    $(".helpMenu input[type=text]:first").select();
}

function parseDate(dateString) {
    var dateParts = dateString.split("/");
    return new Date(parseInt(dateParts[2], 10), parseInt(dateParts[1], 10) - 1, parseInt(dateParts[0], 10));
}

$(window).bind("pageshow", function (event) {
    if (event.originalEvent.persisted) {
        window.location.reload()
    }
});

$(document).on('keypress input paste', 'textarea[maxlength]', function () {

    var text = $(this).val();
    var strippedTextLength = $(this).val().replace(/(\r\n|\n|\r)/gm, "").length;
    var chars = text.length;
    var diff = text.length - strippedTextLength;
    var limit = parseInt($(this).attr('maxlength'));

    if ((chars + diff) > limit) {
        var new_text = text.substr(0, (limit - diff));
        $(this).val(new_text);
    }
});



/**
 * This is here for the Dashboard Account Graph. This must be here due to race-condition on page-load
**/
var globalAssets = ko.observable(0);
var globalLiabilities = ko.observable(0);
var globalAvailableCash = ko.observable(0);
var globalAvailableCurrencies;
var globalSelectedCurrency;
var globalSelectedCurrencySymbol;
var globalAvailableCurrencies = new Array();
var globalIsPbZaAccount;
var globalIsUkAccount
var globalIsAuAccount
var globalIsWiAccount
var globalIsPPAccount
var globalIsCIAccount
var firstRowOpened = ko.observable(false);



//Improves JS rounding BUG, rounding off values e.g "R11.57500" to become "R11.58" ONLY UP TO 2 DECIMAL PLACES! - WANDILE

Number.prototype.formatMoney = function (places, symbol, thousand, decimal) {
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "R";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var number = isNaN(parseFloat(this)) ? "0.00" : parseFloat(this),
        negative = number < 0 ? "-" : "",
        finVal = Math.round((number + 0.00000001) * 100) / 100,
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 4 ? j % 4 : 0;
    finValString = finVal.toString().split(".");
    finValString[0] = numberWithCommas(finValString[0]);
    finValString[1] = typeof finValString[1] == "undefined" ? "00" : finValString[1];
    if (places == 2) {
        return negative + symbol + finValString[0].replace(/-/g, '') + "." + finValString[1];
    }
    else
        return negative + symbol + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};

Number.prototype.formatMoneyWithoutCurrency = function (places, thousand, decimal) {
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    thousand = thousand || ",";
    decimal = decimal || ".";
    var number = this,
        negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};


//Can be removed & replaced by formatValue...
Number.prototype.formatPercentage = function (places, decimal) {
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    decimal = decimal || ".";
    var number = this,
        negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return negative + (j ? i.substr(0, j) : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1") + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};


Number.prototype.formatValue = function (places, decimal) {
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    decimal = decimal || ".";
    var number = this,
        negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return negative + (j ? i.substr(0, j) : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1") + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
};

var ISOCurrencyMapper = new Array();
ISOCurrencyMapper["USD"] = "\u0024";
ISOCurrencyMapper["GBP"] = "\u00A3";
ISOCurrencyMapper["ZAR"] = "R";
ISOCurrencyMapper["EUR"] = "\u20AC";
ISOCurrencyMapper["AUD"] = "\u0024";
ISOCurrencyMapper["CHF"] = "CHF";
ISOCurrencyMapper["JPY"] = "\u00A5";
ISOCurrencyMapper["CAD"] = "\u0024";
ISOCurrencyMapper["AED"] = "د.إ.‏";
ISOCurrencyMapper["CZK"] = "Kč‏";
ISOCurrencyMapper["DKK"] = "kr.";
ISOCurrencyMapper["HKD"] = "HK\u0024";
ISOCurrencyMapper["HUF"] = "Ft";
ISOCurrencyMapper["ILS"] = "\u20AA";
ISOCurrencyMapper["INR"] = "\u20B9";
ISOCurrencyMapper["NOK"] = "kr";
ISOCurrencyMapper["NZD"] = "\u0024";
ISOCurrencyMapper["PLN"] = "zł";
ISOCurrencyMapper["SEK"] = "kr";
ISOCurrencyMapper["SGD"] = "\u0024";
ISOCurrencyMapper["THB"] = "\u0E3F";
ISOCurrencyMapper["TRY"] = "\u20BA";
ISOCurrencyMapper["MUR"] = "Rs";

function RemoveCurrencyFormat(str) {
    str = str.replace(/[^0-9\.]+/g, "");
    return str;
}

/*
 * Get rid off hardcoded currency symbols  
 * function getCurrencySymbol(ISO){
	return ISOCurrencyMapper[ISO];	
}*/

/**
 * Get the currency symbols from API 
 * */
function getCurrencySymbol(CurencyCode) {
    for (i = 0; i < globalAvailableCurrencies.length; i++) {
        if (globalAvailableCurrencies[i].Name == CurencyCode) {
            return globalAvailableCurrencies[i].Symbol;
        }
    }
    return "NA";
}
function trackWithGA(category, action, label) {
    if (!(typeof ga == "undefined")) {
        ga('send', 'event', category, action, label);
    }
}

function setRefCurrency(value) {
    storeValue("RefCur", value);
}

function getRefCurrency() {
    return getStoredValue("RefCur");
}

function expireAccListCookie() {
    ExpireCookieInBrowser("pfmacclist");
}

function storeValue(key, value) {
    if ("https:" == document.location.protocol) {
        document.cookie = key + "=" + value + ";path=/;secure=true";
    } else {
        document.cookie = key + "=" + value + ";path=/";
    }
}

function setCookieValue(key, value, exdays) {
    var expirydate = new Date();
    expirydate.setTime(expirydate.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + expirydate.toGMTString(s);
    if ("https:" == document.location.protocol) {
        document.cookie = key + "=" + value + ";" + expires + ";path=/;secure=true";
    } else {
        document.cookie = key + "=" + value + ";" + expires + ";path=/";
    }
}

function getStoredValue(key) {
    var name = key + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = $.trim(ca[i]);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

function loadFancyLoader() {
    $.fancybox({
        modal: true,
        content: '<div class="loading">...</div>'
    });
}

function closeFancyBoxWindow() {
    $.fancybox.close(true);
}


/**
 * Session Check Start
**/
function KeepAliveSession() {
    $.ajax({
        url: "/content/investec/sso/en/service.keepalive.json",
        type: "POST",
        data: '{"method":"isLoggedIn"}',
        contentType: "application/json",
        dataType: "json",
        success: function (data, textStatus, jqXHR) {

            if (data) {
                if (data.faultCode == 300) {
                    LOG_OUT();
                } else {
                    /* Success Session */
                }
            } else {
                LOG_OUT();
            }
        }
    });
}

function LOG_OUT() {
    //window.onbeforeunload = null;
    //document.location.href = LOGOUTURI;
}

function calculateTimeAndPutItinCookie(sec, cookieName) {

    var popupNow = new Date();
    var popupTime = popupNow.getTime();
    popupTime += sec * 1000;
    popupNow.setTime(popupTime);

    if ("https:" == document.location.protocol) {
        document.cookie = cookieName + "=true;expires=" + popupNow.toUTCString() + ";path=/;secure=true";
    } else {
        document.cookie = cookieName + "=true;expires=" + popupNow.toUTCString() + ";path=/";
    }
}

function ExpireCookieInBrowser(cookieName) {
    if ("https:" == document.location.protocol) {
        document.cookie = cookieName + "=true;expires=Thu, 01-Jan-1970 00:00:01 GMT;path=/;secure=true";
    } else {
        document.cookie = cookieName + "=true;expires=Thu, 01-Jan-1970 00:00:01 GMT;path=/";
    }

}

function checkSessionCounterStarted() {

    if ((Number(getStoredValue("SHOWPOPUPBOX")) == 600) || (Number(getStoredValue("SHOWPOPUPBOX")) == 36000)) {
        storeValue("SHOWPOPUPBOX", Number(getStoredValue("SHOWPOPUPBOX")) - 10);
        checkSessionExpire();
    } else {
        ShowPopUpDialog();
    }
}

function ShowPopUpDialog() {
    if (getStoredValue("SessionExpire") == "") {
        LOG_OUT();
    }
    setTimeout(ShowPopUpDialog, 1000);
}

function checkSessionExpire() {

    var GET_STORED_VALUE = Number(getStoredValue("SHOWPOPUPBOX"));
    if (getStoredValue("SESSION_COUNT_DOWN") == "") {
        storeValue("SESSION_COUNT_DOWN", "true");
    }
    if (getStoredValue("SessionExpire") == "") {
        LOG_OUT();
    } else {
        if (getStoredValue("SessionExpire") != "") {
            if (GET_STORED_VALUE > 1000) {
                storeValue("SHOWPOPUPBOX", GET_STORED_VALUE - 500);
            } else {
                storeValue("SHOWPOPUPBOX", GET_STORED_VALUE - 10);
            }
            GET_STORED_VALUE = Number(getStoredValue("SHOWPOPUPBOX"));
            //console.log("will expires in --> " + GET_STORED_VALUE + "secs");
            if (GET_STORED_VALUE == 60) {
                SessionCounter();
            }
            if (getStoredValue("TenHoursSession") != "") {
                if (GET_STORED_VALUE > 500) {
                    KeepAliveSession();
                    calculateTimeAndPutItinCookie(540, "TenHoursSession");
                    setTimeout(checkSessionExpire, 500000);
                } else {
                    ExpireCookieInBrowser("TenHoursSession");
                    setTimeout(checkSessionExpire, 10000);
                }
            } else {
                setTimeout(checkSessionExpire, 10000);
            }
        } else {
            LOG_OUT();
        }
    }
}

$(document).ready(function () {
    if ((CURRENTPAGE.indexOf("login.html")) == -1 && (CURRENTPAGE.indexOf("logout.html") == -1) &&
			(CURRENTPAGE.indexOf("/isl/") == -1)) {
        if (getStoredValue("SESSION_COUNT_DOWN") == "") {
            setTimeout(checkSessionExpire, 10000);
        }
        setTimeout(checkSessionCounterStarted, 20000);

    } else if ((CURRENTPAGE.indexOf("login.html") != -1) || (CURRENTPAGE.indexOf("logout.html") != -1)) {

        ExpireCookieInBrowser("SessionExpire"); ExpireCookieInBrowser("pfmacclist");
        ExpireCookieInBrowser("RefCur"); ExpireCookieInBrowser("TenHoursSession");
        ExpireCookieInBrowser("SHOWPOPUPBOX"); ExpireCookieInBrowser("SESSION_TIME");
        ExpireCookieInBrowser("SESSION_COUNT_DOWN");
    }

    $(document).ajaxStart(function () {
        if (location.pathname != "/sso/login.html" && getStoredValue("TenHoursSession") != "true" && getStoredValue("SESSION_TIME") == "10" && location.pathname != "/settings/device-registration.html") {
            clearInterval(timerInterval);
            KeepAliveSession();
            storeValue("SHOWPOPUPBOX", Number(getStoredValue("SESSION_TIME")) * 60);
            calculateTimeAndPutItinCookie(Number(getStoredValue("SESSION_TIME")) * 60, "SessionExpire");
        }
    });

});
/**
 * Session Check End
**/


function p_dsdsds9871() { return $("#userid").val(); }

var Investec = Investec || { models: {} };
Investec.models = Investec.models || {};

Investec.models.TargetedPromo = function () {
    var self = this;
    self.banners = ko.observableArray([]);

    self.loadBanners = function (endpoint) {
        if (endpoint) {
            $.getJSON(endpoint, function (data) {
                self.banners(data);
                if (self.banners().length > 0) {
                    self.initSlick();
                }
            });
        }
    };

    self.initSlick = function () {
        if ($(document).slick) {
            var $carousel = $('.carousel.autoplay');
            if ($carousel.hasClass('slick-initialized')) {
                $carousel.unslick();
            }
            $carousel.slick({
                dots: false,
                arrows: false,
                slidesToShow: 1,
                slidesToScroll: 1,
                swipeToSlide: true,
                autoplay: true,
                autoplaySpeed: 6000,
                infinite: true,
                cssEase: 'ease'
            });
        }
    }
};

$(document).ready(
    function () {
        var targetedPromo = $('div[data-controller="TargetedPromoController"]').first();
        if (targetedPromo.length) {
            var targetedPromoModel = new Investec.models.TargetedPromo();
            targetedPromoModel.loadBanners(targetedPromo.attr('data-url'));
            ko.applyBindings(targetedPromoModel, targetedPromo.get(0));
        }
    }
);

