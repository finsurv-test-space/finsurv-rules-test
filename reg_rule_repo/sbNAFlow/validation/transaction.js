define(function () {
  return function (predef) {
    var extTrans;
    with (predef) {

      extTrans = {
        ruleset: "External Transaction Rules",
        scope: "transaction",
        validations: [
          {
            field: "",
            rules: [
              // ignore("nm_riidn3")
            ]
          }
          
        ]
      };

    }
    return extTrans;
  }
});

