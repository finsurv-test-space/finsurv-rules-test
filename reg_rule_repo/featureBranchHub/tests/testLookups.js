define({
  ReportingQualifier           : ["BOPCUS", "NON REPORTABLE", "BOPCARD RESIDENT", "BOPCARD NON RESIDENT", "NON RESIDENT RAND", "INTERBANK", "BOPDIR"],
  branches : [
    {code: '123123', name : 'TestBranch123', hubCode: "123", hubName: "TestHub123", validFrom: "2013-01-04"},
    {code: '124124', name : 'TestBranch124', validFrom: "2013-01-04", validTo : "2017-01-04"},
    {code: '124124', name : 'Maraisburg', hubCode: "", hubName: "", validFrom: "2016-01-04", validTo : "2017-01-04"}
  ],
  mtaAccounts : [
    {
      accountNumber : "12341234",
      MTA : "RANDBUREAU",
      rulingSection : "Test Ruling Section",
      ADLALevel : "3",
      isADLA : false
    },
    {
      accountNumber : "54321",
      MTA : "MUKURU",
      rulingSection : "A.4 (B)(ii)",
      ADLALevel : "2",
      isADLA : true
    }
  ],
  ihqCompanies : [
    {
      ihqCode : "IHQ123",
      registrationNumber : "2013/1234567/07",
      companyName : "company1"
    },
    {
      ihqCode : "IHQ124",
      registrationNumber : "2013/1234568/07",
      companyName : "company2"
    }
  ],
  holdcoCompanies : [
    {
      accountNumber : "1234567",
      registrationNumber : "2013/1234567/07",
      companyName : "holdco1"
    },
    {
      accountNumber : "1234568",
      registrationNumber : "2013/1234568/07",
      companyName : "holdco2"
    }
  ]
})
