define(function() {
  return function(testBase) {
      with (testBase) {

        var test_cases = [
             //BranchCode
          // Must be completed
          assertSuccess("bh_brc1", {ReportingQualifier: 'BOPCUS', BranchCode: 'BRANCHY'}),
          assertFailure("bh_brc1", {ReportingQualifier: 'BOPCUS', BranchCode: ''}),
          assertFailure("bh_brc1", {ReportingQualifier: 'BOPCUS'}),
          // Must not be completed
          assertSuccess("bh_brc2", {ReportingQualifier: 'BOPCARD RESIDENT'}),
          assertFailure("bh_brc2", {ReportingQualifier: 'BOPCARD RESIDENT', BranchCode: 'BRANCHY'}),
          assertSuccess("bh_brc2", {ReportingQualifier: 'BOPCARD RESIDENT', BranchCode: ''}),
          //Invalid branch code as per the BranchCode table
          assertSuccess("bh_brc3", {ReportingQualifier: 'BOPCUS'}),
          assertSuccess("bh_brc3", {ReportingQualifier: 'BOPCUS', BranchCode: ''}),
          assertSuccess("bh_brc3", {ReportingQualifier: 'BOPCUS', BranchCode: '123123'}),
          assertFailure("bh_brc3", {ReportingQualifier: 'BOPCUS', BranchCode: 'BRANCHY'}),

          //BranchName
          // Must be completed
          assertSuccess("bh_brn1", {ReportingQualifier: 'BOPCUS', BranchName: 'BRANCHY'}),
          assertFailure("bh_brn1", {ReportingQualifier: 'BOPCUS', BranchName: ''}),
          assertFailure("bh_brn1", {ReportingQualifier: 'BOPCUS'}),
          // Must not be completed
          assertSuccess("bh_brn2", {ReportingQualifier: 'BOPCARD RESIDENT'}),
          assertFailure("bh_brn2", {ReportingQualifier: 'BOPCARD RESIDENT', BranchName: 'BRANCHY'}),
          assertSuccess("bh_brn2", {ReportingQualifier: 'BOPCARD RESIDENT', BranchName: ''}),
          //BranchName does not correspond to the provided BranchCode
          assertSuccess("bh_brn3", {ReportingQualifier: 'BOPCUS'}),
          assertSuccess("bh_brn3", {ReportingQualifier: 'BOPCUS', BranchName: ''}),
          assertSuccess("bh_brn3", {ReportingQualifier: 'BOPCUS', BranchCode: '123123', BranchName: 'TestBranch123'}),
          assertFailure("bh_brn3", {ReportingQualifier: 'BOPCUS', BranchCode: '123123', BranchName: 'BRANCHY'}),

          //HubCode
          // Must be completed
          assertSuccess("bh_hubc1", {ReportingQualifier: 'BOPCARD RESIDENT'}),
          assertFailure("bh_hubc1", {ReportingQualifier: 'BOPCARD RESIDENT', HubCode: '1234'}),
          assertNoRule("bh_hubc1", {ReportingQualifier: 'BOPCUS', HubCode: '1234'}),
          assertSuccess("bh_hubc1", {ReportingQualifier: 'BOPCARD RESIDENT', HubCode: ''}),
          assertSuccess("bh_hubc2", { HubCode: '1234'}),
          assertFailure("bh_hubc2", {ReportingQualifier: 'BOPCARD RESIDENT', HubCode: ' 1234 '}),
          assertFailure("bh_hubc2", {ReportingQualifier: 'BOPCARD RESIDENT', HubCode: '12  34'}),
          //Invalid HUB code as per the HubCode table
          assertSuccess("bh_hubc3", {ReportingQualifier: 'BOPCUS'}),
          assertSuccess("bh_hubc3", {ReportingQualifier: 'BOPCUS', HubCode: ''}),
          assertSuccess("bh_hubc3", {ReportingQualifier: 'BOPCUS', HubCode: '123'}),
          assertFailure("bh_hubc3", {ReportingQualifier: 'BOPCUS', HubCode: 'HUBBY'}),

          //HubName
          // Must be completed
          assertSuccess("bh_hubn1", {ReportingQualifier: 'BOPCARD RESIDENT'}),
          assertFailure("bh_hubn1", {ReportingQualifier: 'BOPCARD RESIDENT', HubName: 'HUBBY'}),
          assertSuccess("bh_hubn1", {ReportingQualifier: 'BOPCARD RESIDENT', HubName: ''}),
          assertSuccess("bh_hubn2", {ReportingQualifier: 'BOPCUS', HubCode: '1234' , HubName: 'HUBBY'}),
          assertFailure("bh_hubn2", {ReportingQualifier: 'BOPCUS', HubCode: '1234', HubName: ''}),
          assertFailure("bh_hubn2", {ReportingQualifier: 'BOPCUS', HubCode: '1234'}),
          //BranchName does not correspond to the provided HubCode
          assertSuccess("bh_hubn3", {ReportingQualifier: 'BOPCUS'}),
          assertSuccess("bh_hubn3", {ReportingQualifier: 'BOPCUS', HubName: ''}),
          assertSuccess("bh_hubn3", {ReportingQualifier: 'BOPCUS', HubCode: '123', HubName: 'TestHub123'}),
          assertFailure("bh_hubn3", {ReportingQualifier: 'BOPCUS', HubCode: '123', HubName: 'HUBBY'}),


        ]

      }
    return testBase;
  }
})
