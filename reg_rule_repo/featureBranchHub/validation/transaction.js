define(function () {
  return function (predef) {
    var feature;
    with (predef) {

      feature = {
        ruleset: "Branch and Hub Rules",
        scope: "transaction",
        validations: [
          {
            field: "BranchCode",
            minLen: 2,
            maxLen: 10,
            rules: [
              failure("bh_brc1", 222, "Must be completed", isEmpty).onSection("ABCDG"),
              failure("bh_brc2", 223, "Must not be completed", notEmpty).onSection("EF"),
              failure("bh_brc3", 224, "Invalid branch code as per the BranchCode table",
                notEmpty.and(not(isValidBranchCode))).onSection("ABCDG")
            ]
          },
          {
            field: "BranchName",
            minLen: 2,
            maxLen: 50,
            rules: [
              failure("bh_brn1", 225, "Must be completed", isEmpty).onSection("ABCDG"),
              failure("bh_brn2", 226, "Must not be completed", notEmpty).onSection("EF"),
              failure("bh_brn3", 283, "BranchName does not correspond to the provided BranchCode",
                notEmpty.and(not(isValidBranchName))).onSection("ABCDG")
            ]
          },
          {
            field: "HubCode",
            minLen: 2,
            maxLen: 10,
            rules: [
              failure("bh_hubc1", 223, "Must not be completed", notEmpty).onSection("EF"),
              failure("bh_hubc2", 219, "Additional spaces identified in data content",
                notEmpty.and(hasAdditionalSpaces)),
              failure("bh_hubc3", 224, "Invalid hub code as per the HubCode table",
                notEmpty.and(not(isValidHubCode))).onSection("ABCDG")
            ]
          },
          {
            field: "HubName",
            minLen: 2,
            maxLen: 50,
            rules: [
              failure("bh_hubn1", 226, "Must not be completed", notEmpty).onSection("EF"),
              failure("bh_hubn2", 225, "Must be completed if HubCode contains a value",
                isEmpty.and(hasTransactionField("HubCode"))).onSection("ABCDG"),
              failure("bh_hubn3", 283, "HubName does not correspond to the provided HubCode",
                notEmpty.and(not(isValidHubName))).onSection("ABCDG")
            ]
          }
        ]
      };
   }
    return feature;
  }
});
