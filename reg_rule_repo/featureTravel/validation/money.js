define(function () {
  return function (predef) {
    var money;

    with (predef) {
      money = {
        ruleset: "Money Rules for Flow Form",
        scope: "money",
        validations: [
          {
            field: "TravelMode.TravellerStatus",
            rules: [
              failure("flw_tpits1", "I08", "Traveller Status must be completed",
                isEmpty).onSection("AB").onCategory(["255", "256"]),
              failure("flw_tpits2", "I09", "If category 256 is used, and the Applicant is the traveller then the Applicant Passport Number must be completed",
                hasValue("Account Holder").and(notTransactionField("Resident.Individual.PassportNumber"))).onSection("AB").onCategory("256"),
              failure("flw_tpits3", "I10", "If category 255 or 256 is used, Applicant information cannot be prepopulated because the Applicant Type is an Entity",
                hasValue("Account Holder").and(hasTransactionField("Resident.Entity"))).onSection("AB").onCategory(["255", "256"]),
              failure("flw_tpits4", "I11", "If category 303, 304, 305, 306, 416 or 417 is used, Applicant information cannot be prepopulated because the Applicant Type is an Entity",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("AB").onCategory(["303", "304", "305", "306", "416", "417"]),
              failure("flw_tpits5", "I08", "Not required for category chosen",
                notEmpty).onSection("AB").notOnCategory(["255", "256"])
            ]
          },
          {
            field: "ThirdParty.Individual.Surname",
            rules: [
              failure("flw_mtpisn3", "I??", "Not required if Applicant is selected",
                notEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onSection("AB").onCategory(["255", "256"])

            ]
          },
          {
            field: "ThirdParty.Individual.Name",
            rules: [
              failure("flw_mtpinm0", 419, "This must be completed",
                isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", [
                  'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
                ]))).onSection("AB").onCategory(["255", "256"])
            ]
          },
          {
            field: "ThirdParty.Individual.IDNumber",
            rules: [
              failure("mtpiid1", 424, "ID number is required",
                isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", "South African Resident"))).onSection("AB").onCategory(["255", "256"])

            ]
          },
          {
            field: "ThirdParty.Individual.DateOfBirth",
            rules: [
              failure("mtpibd1", 428, "Date of birth is required",
                isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", [
                  'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
                ]))).onSection("AB").onCategory(["255", "256"]),
              failure("mtpibd3", 428, "Date of birth is required",
                isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", [
                  'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
                ])))
                .onSection("AB").onCategory("255")

            ]
          },
          {
            field: "ThirdParty.Individual.TempResPermitNumber",
            rules: [
              failure("mtpitp1", 424, "Temporary resident permit number is required",
                isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", "Foreign Temporary Resident"))).onSection("AB").onCategory(["255", "256"])

            ]
          },
          {
            field: "ThirdParty.Individual.TempResExpiryDate",
            rules: [
              failure("flw_mtpite1", 424, "This must be completed",
                isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", "Foreign Temporary Resident"))).onSection("AB").onCategory(["255", "256"]),
              warning("flw_mtpite3", "I29", "Traveller Temporary Resident Permit has expired",
                notEmpty.and(isDaysInPast(0))).onSection("ABG"),
            ]
          },
          {
            field: "ThirdParty.Individual.PassportNumber",
            rules: [
              failure("mtpipn1", 431, "Passport number is required",
                isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", [
                  'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
                ]))).onSection("AB").onCategory(["255", "256"])

            ]
          },
          {
            field: "ThirdParty.Individual.PassportCountry",
            rules: [
              failure("mtpipc1", 433, "If category 255 or 256 is used this must be completed",
                isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", [
                  'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
                ]))).onSection("AB").onCategory(["255", "256"]),
            ]
          },
          {
            field: "ThirdParty.Individual.PassportExpiryDate",
            rules: [
              failure("flw_mtpipe1", 433, "This must be completed",
                isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", [
                  'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
                ]))).onSection("AB").onCategory(["255", "256"])

            ]
          },
          {
            field: "TravelMode.Mode",
            rules: [
              failure("flw_tvlm1", "I??", "If category 255 or 256 is used this must be completed",
                isEmpty).onOutflow().onSection("A").onCategory(["255", "256"]),
              failure("flw_tvlm2", "I??", "Unless category 255 or 256 is used this cannot be specified",
                notEmpty).onOutflow().onSection("A").notOnCategory(["255", "256"])
            ]
          },
          {
            field: "TravelMode.BorderPost",
            rules: [
              failure("flw_tvlbp1", "I??", "Required field for this category when traveling by ROAD",
                isEmpty.and(hasMoneyFieldValue("TravelMode.Mode", "ROAD"))).onOutflow().onSection("A").onCategory(["255", "256"]),
              failure("flw_tvlbp2", "I??", "Not required for category chosen",
                notEmpty).onOutflow().onSection("A").notOnCategory(["255", "256"]),
              failure("flw_tvlbp3", "I??", "Not required when traveling by AIR, SEA or RAIL",
                notEmpty.and(hasMoneyFieldValue("TravelMode.Mode", ["AIR", "SEA", "RAIL"]))).onOutflow().onSection("A").onCategory(["255", "256"])
            ]
          },
          {
            field: "TravelMode.TicketNumber",
            rules: [
              failure("flw_tvltn1", "I??", "Required field for this category when traveling by AIR, SEA or RAIL",
                isEmpty.and(hasMoneyFieldValue("TravelMode.Mode", ["AIR", "SEA", "RAIL"]))).onOutflow().onSection("A").onCategory(["255", "256"]),
              failure("flw_tvltn2", "I??", "Unless category 255 or 256 is used this cannot be specified",
                notEmpty).onOutflow().onSection("A").notOnCategory(["255", "256"])
            ]
          },
          {
            field: "TravelMode.DepartureDate",
            rules: [
              failure("flw_tvldd1", "I??", "If category 255 or 256 is used this must be completed", isEmpty).onOutflow().onSection("A").onCategory(["255", "256"]),
              failure("flw_tvldd2", "I??", "Must be a valid date in the future in the format: YYYY-MM-DD",
                notEmpty.and(notDatePattern.or(isDaysInPast(1)))).onSection("ABG"),
              failure("flw_tvldd3", "I??", "Unless category 255 or 256 is used this cannot be specified",
                notEmpty).onOutflow().onSection("A").notOnCategory(["255", "256"])
            ]
          },
          {
            field: "TravelMode.DestinationCountry",
            rules: [
              failure("flw_tvdc1", "I??", "If category 255 or 256 is used this must be completed", isEmpty).onOutflow().onSection("A").onCategory(["255", "256"]),
              failure("flw_tvdc2", "I??", "Unless category 255 or 256 is used this cannot be specified",
                notEmpty).onOutflow().onSection("A").notOnCategory(["255", "256"])
            ]
          }
        ]
      }
    };

    return money;
  }
});
