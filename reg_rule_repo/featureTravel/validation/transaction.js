define(function () {
  //THIS IS the Transaciotn rules
  return function (predef) {
    var transaction;

    with (predef) {
      transaction = {
        ruleset: "Transaction Rules for Flow form",
        scope: "transaction",
        validations: [

          {
            field: "AccountHolderStatus",
            rules: [
              failure("flw_ahs1", "I04", "This must be completed",
                isEmpty).onSection("A"),
              failure("flw_ahs2", "I15", "Non Residents must use categories 250 or 251 for travel and not 255 or 256",
                hasValueIn(["Non Resident"])).onSection("A").onCategory(["255", "256"]),
              failure("flw_ahs3", "I16", "Travel categories 250, 251 are reserved for Non Residents only",
                hasValueIn(["South African Resident"])).onSection("A").onCategory(["250", "251"])
            ]
          },
          {
            field: "CounterpartyStatus",
            rules: [
              failure("flw_cps1", "I05", "This must be completed",
                isEmpty).onSection("A")
            ]
          },
          {
            field: "Resident.Individual.PassportNumber",
            rules: [
              failure("flw_ripn1", 204, "If the category is 250 or 251, the Passport Number must be completed",
                isEmpty).onSection("A").onCategory(["250", "251"]),
              failure("flw_ripn2", 294, "For a Non Resident Individual the Passport Number must be completed",
                isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "Non Resident")))
                .onSection("AB"),
              failure("flw_ripn3", "I10", "If category 256 is used, for a South African Resident Individual the Passport Number must be completed",
                isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "South African Resident")))
                .onSection("AB").onCategory("256"),
              failure("flw_ripn4", "I52", "This field is too long and must be shortened to 15 characters",
                notEmpty.and(isTooLong(15)))
                .onSection("A")
            ]
          },
          {
            field: "Resident.Individual.PassportCountry",
            rules: [
              failure("flw_ripc1", 204, "If the category is 250 or 251, the Passport Country must be completed",
                isEmpty).onSection("A").onCategory(["250", "251"]),
              failure("flw_ripc2", 294, "For a Non Resident Individual the Passport Country must be completed",
                isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "Non Resident")))
                .onSection("AB"),
              failure("flw_ripc3", "I10", "If category 256 is used, for a South African Resident Individual the Passport Country must be completed",
                isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "South African Resident")))
                .onSection("AB").onCategory("256")
            ]
          },
          {
            field: "Resident.Individual.PassportExpiryDate",
            rules: [
              failure("flw_ripe1", "I11", "If category 255 is used, Traveller Passport Expiry Date must be completed",
                notEmpty).onSection("AB").onCategory("255"),
              failure("flw_ripe2", "I12", "If the category is 250 or 251, the Passport Expiry must be completed",
                isEmpty).onSection("A").onCategory(["250", "251"]),
              failure("flw_ripe3", "I13", "For a Non Resident the Passport Expiry must be completed",
                isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "Non Resident"))).onSection("AB"),
              failure("flw_ripe4", "I14", "If category 256 is used, for a South African Resident the Passport Expiry must be completed",
                isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "South African Resident"))).onSection("AB").onCategory("256"),
              failure("flw_ripe5", "I20", "Must be in a valid date format: YYYY-MM-DD",
                notEmpty.and(notDatePattern)).onSection("ABG"),
              failure("flw_ripe6", "I21", "We are unable to proceed with your request as it appears your passport has expired. \nPlease contact your Private Banker or the 24/7 global Client Support Centre to update your passport details.",
                notEmpty.and(hasDatePattern).and(isDaysInPast(0))).onSection("ABG")
            ]
          },
          {
            field: "Resident.Entity.EntityName",
            rules: [
              failure("relen1", 304, "Required field for entities",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("ABCE"),
              failure("flw_relen1", "I54", "Entity Name is limited to 70 characters", notEmpty.and(isTooLong(70))).onSection("A")
            ]
          }
        ]
      }
    };

    return transaction;
  }

});

