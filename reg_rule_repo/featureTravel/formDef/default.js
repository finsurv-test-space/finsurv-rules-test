/**
 * Created by petruspretorius on 24/03/2015.
 */
define(function () {
  
    var _export = {
      detail: {
        Monetary                 : {
          fields: [
            "Travel", "Travel.Surname",
            "Travel.Name", "Travel.IDNumber", "Travel.DateOfBirth", "Travel.TempResPermitNumber",
            "TravelMode.Mode", "TravelMode.BorderPost", "TravelMode.TicketNumber",
            "TravelMode.DepartureDate", "TravelMode.DestinationCountry"
          ]
        },
        MonetaryThirdPartyContact: {
          fields : [ 
             "TravelMode.TravellerStatus"
            ]
        }
  
      }
    }
    return _export;
  });
  
  