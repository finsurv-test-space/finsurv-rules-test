define(function () {
  return function (predef) {
    var extTrans;
    with (predef) {

      extTrans = {
        ruleset: "External Transaction Rules",
        scope: "transaction",
        validations: [
          {
            field: "NonResident.Individual.Gender",
            rules: [
              failure("ls_nrgn2", "SB_NRIG", "Must be completed",
                isEmpty.and(hasTransactionField("NonResident.Individual"))).onSection("C")
            ]
          }
        ]
      };

    }
    return extTrans;
  }
});

