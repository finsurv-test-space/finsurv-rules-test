define(function () {
  return function (predef) {
    var extTrans;
    with (predef) {

      extTrans = {
        ruleset: "External Money Rules",
        scope: "money",
        validations: []
      };

    }
    return extTrans;
  }
});
