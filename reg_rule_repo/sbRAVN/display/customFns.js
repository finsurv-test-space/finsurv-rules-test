define(function (require) {
  return function (app, config) {

    var superInit = function () { };

    var tradeCats = ['101', '103', '105', '106'];
    var notTrade = ['101/11'];

    app.run(["$locale", function ($locale) {
      $locale.NUMBER_FORMATS.GROUP_SEP = " ";
      $locale.NUMBER_FORMATS.DECIMAL_SEP = ".";
    }]);

    if (config.initializationFn) superInit = config.initializationFn;

    var _getData = app.getData;

    app.getData = function () {
      var data = JSON.parse(JSON.stringify(_getData.apply(this, arguments)));
      if (!data.customData) data.customData = {};
      if (data.customData.snapShot) delete data.customData.snapShot;
      data.customData.AuthIssuers = [];
      data.transaction.MonetaryAmount.forEach(function (m) {
        data.customData.AuthIssuers.push(m.RegulatorAuth ? m.RegulatorAuth.AuthIssuer : undefined)
      })
      return data;
    }

    var _config = {
      initializationFn: function (model, scope) {
        superInit(model, scope);

        scope.updateAllowance = function (model, selectedOption) {
          if (selectedOption.value == 'SDA') {
            delete model.data.TaxClearanceCertificateIndicator;
            if (!model.data.AdHocRequirement) model.data.AdHocRequirement = {};
            model.data.AdHocRequirement.Subject = 'SDA';
            model.data.AdHocRequirement.Description = 'Single Discretionary Alowance';
          } else if (selectedOption.value == 'FIA') {
            delete model.data.AdHocRequirement;
            model.data.TaxClearanceCertificateIndicator = 'Y'
          } else { // option 'N'
            delete model.data.TaxClearanceCertificateIndicator;
            delete model.data.AdHocRequirement;
          }
          scope.updateCC(scope);
          app.validate();
        }

        function clearIE(_scope) {
          for (i = _scope.current.money.ImportExport.length - 1; i >= 0; i--) {
            var ie = _scope.current.money.ImportExport[i];
            if (ie)
              scope.removeImportExport(_scope.current.money, ie);
          }
        }

        function addSingleIE(_scope) {
          scope.addImportExport(_scope.current.money, true);
          var inZAR = _getData().customData.inZAR;
          _scope.current.money.ImportExport[0].PaymentValue.val = inZAR ? _scope.current.money.DomesticValue.val : _scope.current.money.ForeignValue.val;
        }

        scope.updateCC = function (_scope) {
          var subject = _scope.current.money.AdHocRequirement.Subject.val;
          var cat = _scope.current.money.CategoryCode.val;
          if (cat){
            if (_scope.current.money.CategorySubCode.val) {
              cat += "/" + _scope.current.money.CategorySubCode.val;
            }
            if (tradeCats.indexOf(_scope.current.money.CategoryCode.val) != -1
              && notTrade.indexOf(cat) == -1) {
              if (['SDA', 'REMITTANCE DISPENSATION'].indexOf(subject) == -1){
                if(!(cat == "106" && scope.customData.LUClient == "Y")) {
                  if (_scope.current.money.ImportExport.length != 0) {
                    clearIE(_scope);
                  }
                  addSingleIE(_scope);
                } else {
                  clearIE(_scope);
                }
              } else { 
                clearIE(_scope);
              } 
            } else {
              clearIE(_scope);
            }
          }
          setTimeout(function () {
            if (!scope.$root.$$phase) {
              scope.$root.$digest();
            }
          }, 500);
        }

        
        scope.updateCCN = function (_scope) {
          // "_scope" is curently not being passed to this method, needs to be investigated,
          // current page scope is working fine in current scenarious
          if (scope.current && scope.current.money) {
            var cat = scope.current.money.CategoryCode.val;

            if (scope.current.money.CategorySubCode.val) {
              cat += "/" + scope.current.money.CategorySubCode.val;
            }
            if (cat == "106" && scope.customData.LUClient == "Y" && scope.current.money.ImportExport.length != 0) {
              clearIE(scope);
            } 
            
            // Adds back IE if CCN is not LU Client, this is commented out because it could add back IE when not applicable
            // else if (cat == "106" && scope.customData.LUClient == "N" && scope.current.money.ImportExport.length == 0) {
            //   addSingleIE(scope);
            // }

            setTimeout(function () {
              if (!scope.$root.$$phase) {
                scope.$root.$digest();
              }
            }, 500);
          } 
          // else {
          //   if (!scope.bopdata && scope.model && scope.model.data) scope.bopdata = scope.model.data; /* Probably nonsense. */
          //   if (scope.bopdata && scope.bopdata.MonetaryAmount && scope.bopdata.MonetaryAmount.length > 0) {
          //     var isLU = (scope && scope.customData && scope.customData.LUClient && scope.customData.LUClient == "Y");
          //     if (isLU) {
          //       for (mnyIdx in scope.bopdata.MonetaryAmount) {
          //         var mny = scope.bopdata.MonetaryAmount[mnyIdx];
          //         var cat = mny.CategoryCode.val || mny.CategoryCode;
          //         if (cat == "106" /*&& isSectionInError('Money', 'importExport', mny)*/) {/* should only clout the import exports if they are in error. otherwise leave them as they are. */
          //           for(ie in mny.ImportExport){
          //             try {
          //               scope.removeImportExport(mny, ie);
          //             } catch (error) {
          //               /* ignore */
          //             }
          //             if (mny.ImportExport) try {
          //               delete mny.ImportExport;
          //             } catch (error){
          //               /* ignore */
          //             }
          //           }
          //         }
          //       }
          //     }
          //   }
          // }
        }

        scope.clearAuth = function (money) {
          // The click is pre-model-set so will be opposite of what you expect.
          if (money.haveAuth.val) {
            money.haveAuth.data.RegulatorAuth = {};
          }
          app.validate();
        }

        scope.hasAdditionalInfo = function () {

          var container = document.getElementById('allFields');
          if (container) {
            var ifields = Array.prototype.slice.apply(container.getElementsByTagName("ifield"));
            var lfields = Array.prototype.slice.apply(container.getElementsByTagName("lfield"));
            var sfields = Array.prototype.slice.apply(container.getElementsByTagName("sfield"));
            var datefields = Array.prototype.slice.apply(container.getElementsByTagName("datefield"));
            var fields = ifields.concat(lfields).concat(sfields).concat(datefields);
            var visibleChildren = _.filter(fields, function (el) {
              return (_.find(el.classList, function (e) { return e == 'ng-hide' }) == null) &&
                (_.find(el.firstChild.classList, function (e) { return e == 'ng-hide' }) == null);
            })
            return !!visibleChildren.length;
          }

          // if(!hadAdditionalInfo){
          //   events = app.getRaisedEvents().filter(function(ev){
          //     return (ev.event == "transaction" && ev.type == "ERROR");
          //   });
          //   return hadAdditionalInfo = hadAdditionalInfo || (!!events.length);
          // }
          // return true;
        }

      }
    }

    Object.assign(config, _config);

    return config;

  }
})