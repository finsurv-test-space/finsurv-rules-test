define({
  engine: {major: "1", minor: "0"},
  dependsOn: "flow",
  features: [
    //"featureBranchHub", "featureHOLDCO", "featureMTAAccounts", "featureSARBManB4", 
    "featureLimit", 
    "featureEntity511"//, 
    //"featureSchema"
  ],
  mappings: {
    LocalCurrencySymbol: "R",
    LocalCurrencyName: "Local Currency",
    LocalCurrency: "ZAR",
    Locale: "ZA",
    LocalValue: "DomesticValue",
    Regulator: "Regulator",
    DealerPrefix: "RE",
    RegulatorPrefix: "CB",
    StateName: "Province",
    _minLenErrorType: "ERROR", // SUCCESS, ERROR, WARNING
    _maxLenErrorType: "ERROR",
    _lenErrorType: "ERROR"
  }

})