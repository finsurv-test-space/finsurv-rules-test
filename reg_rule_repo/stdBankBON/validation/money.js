define(function () {
  return function (predef) {
    var extTrans;
    with (predef) {

      extTrans = {
        ruleset: "External Money Rules",
        scope: "money",
        validations: [
          {
            field: "",
            rules: [
              ignore("mtaa_cc1"),
              ignore("mtaa_cc2")
            ]
          }
        ]
      };

    }
    return extTrans;
  }
});
