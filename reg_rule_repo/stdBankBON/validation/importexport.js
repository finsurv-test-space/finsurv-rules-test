define(function () {
  return function (predef) {
    var extImportExport;
    with (predef) {

      extImportExport = {
        ruleset: "External ImportExport Rules",
        scope: "importexport",
        validations: []
      };

    }
    return extImportExport;
  }
});
