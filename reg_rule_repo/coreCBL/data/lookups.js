define({
  ReportingQualifier           : ["BOPCUS", "NON REPORTABLE", "BOPCARD RESIDENT", "BOPCARD NON RESIDENT"],
  provinces                    : ["BEREA", "BUTHA-BUTHE", "LERIBE", "MAFETENG", "MASERU", "MOHALE'S HOEK", "MOKHOTLONG", "QACHA'S NEK", "QUTHING", "THABA-TSEKA"],
  nonResidentExceptions        : ["MUTUAL PARTY", "BULK INTEREST", "BULK VAT REFUNDS", "BULK BANK CHARGES", "BULK PENSIONS"],
  residentExceptions           : ["MUTUAL PARTY","BULK PENSIONS", "BULK INTEREST", "UNCLAIMED DRAFTS", "BULK DIVIDENDS", "BULK BANK CHARGES", "NON RESIDENT DOMESTIC"],
  moneyTransferAgents          : ["AD", "ADLA", "WESTERN UNION", "MONEYGRAM", "MUKURU", "CARD", "SHOPRITE", "PAYPAL"],
  accountIdentifiersNonResident: ["NON RESIDENT OTHER", "NON RESIDENT DOMESTIC CURRENCY", "NON RESIDENT FCA", "CASH", "FCA RESIDENT", "RES FOREIGN BANK ACCOUNT", "VOSTRO"],
  accountIdentifiersResident   : ["RESIDENT OTHER", "FCA RESIDENT","CFC RESIDENT", "CASH", "EFT", "CARD PAYMENT", "DEBIT CARD", "CREDIT CARD"],
  institutionalSectors         : [
    {code: "01", description: "FINANCIAL CORPORATE"},
    {code: "02", description: "NON FINANCIAL CORPORATE"},
    {code: "03", description: "GENERAL GOVERNMENT"},
    {code: "04", description: "HOUSEHOLD"}
  ], 
    industrialClassifications    : [
      {code: "01", description: "AGRICULTURE, HUNTING, FORESTRY AND FISHING"},
      {code: "02", description: "MINING AND QUARRYING"},
      {code: "03", description: "MANUFACTURING"},
      {code: "04", description: "ELECTRICITY, GAS AND WATER SUPPLY"},
      {code: "05", description: "CONSTRUCTION"},
      {code: "06", description: "WHOLESALE AND RETAIL TRADE, REPAIR OF MOTOR VEHICLES, MOTOR CYCLES AND PERSONAL AND HOUSEHOLD GOODS, HOTELS AND RESTAURANTS"},
      {code: "07", description: "TRANSPORT, STORAGE AND COMMUNICATION"},
      {code: "08", description: "FINANCIAL INTERMEDIATION, INSURANCE, REAL ESTATE AND BUSINESS SERVICES"},
      {code: "09", description: "COMMUNITY, SOCIAL AND PERSONAL SERVICES"},
      {code: "10", description: "PRIVATE HOUSEHOLDS, EXTERRITORIAL ORGANISATIONS, REPRESENTATIVES OF FOREIGN GOVERNMENTS AND OTHER ACTIVITIES NOT ADEQUATELY DEFINED"},
    ],
    customsOffices : [
      {code: "217", name:	"Maseru stateware house"},
      {code: "218", name:	"Maseru bridge"},
      {code: "219", name:	"Maseru station"},
      {code: "220", name:	"Maputsoe Bridge"},
      {code: "221", name:	"Caldon Sproot"},
      {code: "222", name:	"Van rooyen"},
      {code: "223", name:	"Qachas Nek Bridge"},
      {code: "224", name:	"Makhaleng Bridge"},
      {code: "225", name:	"Tele Bridge"},
      {code: "226", name:	"Sani Top"},
      {code: "227", name:	"Peka Bridge"},
      {code: "228", name:	"Ramatseliso"},
      {code: "229", name:	"Sephapo gate"},
      {code: "230", name:	"Moshoeshoe 1 Airport"},
      {code: "231", name:	"Parcel Post"}
    ],
    adhocSubjects : ["INVALIDIDNUMBER"], 
    categories: [
      {
       flow: "IN", 
       code: "100", 
       section: "Merchandise(exports)", 
       description: "Adjustments/reversals/refunds applicable to merchandise"
      },
      {
       flow: "IN", 
       code: "101/01", 
       section: "Merchandise(exports)", 
       description: "Export advance payment (excluding capital goods, gold, platinum, crude oil, refined petroleum products, gemstones, steel, coal, iron ore, copper and goods exported via the Post Office)"
      },
      {
       flow: "IN", 
       code: "101/02", 
       section: "Merchandise(exports)", 
       description: "Export advance payment - capital goods"
      },
      {
       flow: "IN", 
       code: "101/03", 
       section: "Merchandise(exports)", 
       description: "Export advance payment - gold"
      },
      {
       flow: "IN", 
       code: "101/04", 
       section: "Merchandise(exports)", 
       description: "Export advance payment - platinum"
      },
      {
       flow: "IN", 
       code: "101/05", 
       section: "Merchandise(exports)", 
       description: "Export advance payment - crude oil"
      },
      {
       flow: "IN", 
       code: "101/06", 
       section: "Merchandise(exports)", 
       description: "Export advance payment - refined petroleum products"
      },
      {
       flow: "IN", 
       code: "101/07", 
       section: "Merchandise(exports)", 
       description: "Export advance payment - gemstones (all precious stones, including diamonds, sapphires, rubies, tanzanite, emeralds, alexandrines, moonstones, pearls, etc.)"
      },
      {
       flow: "IN", 
       code: "101/08", 
       section: "Merchandise(exports)", 
       description: "Export advance payment - steel"
      },
      {
       flow: "IN",
       code: "101/09", 
       section: "Merchandise(exports)", 
       description: "Export advance payment - coal"
      },
      {
       flow: "IN", 
       code: "101/10", 
       section: "Merchandise(exports)", 
       description: "Export advance payment - iron ore"
      },
      {
       flow: "IN", 
       code: "101/11", 
       section: "Merchandise(exports)", 
       description: "Export advance payment - goods exported via the country\u2019s Post Office"
      },
      {
       flow: "IN", 
       code: "103/01", 
       section: "Merchandise(exports)", 
       description: "Export payments (excluding capital goods, gold, platinum, crude oil, refined petroleum products, gemstones, steel, coal, copper, iron ore and goods exported via the country\u2019s Post Office)"
      },
      {
       flow: "IN", 
       code: "103/02", 
       section: "Merchandise(exports)", 
       description: "Export payment - capital goods"
      },
      {
       flow: "IN", 
       code: "103/03", 
       section: "Merchandise(exports)", 
       description: "Export payment - gold"
      },
      {
       flow: "IN", 
       code: "103/04", 
       section: "Merchandise(exports)", 
       description: "Export payment - platinum"
      },
      {
       flow: "IN", 
       code: "103/05", 
       section: "Merchandise(exports)", 
       description: "Export payment - crude oil"
      },
      {
       flow: "IN", 
       code: "103/06", 
       section: "Merchandise(exports)", 
       description: "Export payment - refined petroleum products"
      },
      {
       flow: "IN", 
       code: "103/07", 
       section: "Merchandise(exports)", 
       description: "Export payment - diamonds"
      },
      {
       flow: "IN", 
       code: "103/08", 
       section: "Merchandise(exports)", 
       description: "Export payment - steel"
      },
      {
       flow: "IN", 
       code: "103/09", 
       section: "Merchandise(exports)", 
       description: "Export payment - coal"
      },
      {
       flow: "IN", 
       code: "103/10", 
       section: "Merchandise(exports)", 
       description: "Export payment - iron ore"
      },
      {
       flow: "IN", 
       code: "103/11", 
       section: "Merchandise(exports)", 
       description: "Export payment - goods exported via the country\u2019s Post Office"
      },
      {
       flow: "IN", 
       code: "105", 
       section: "Merchandise(exports)", 
       description: "Consumables acquired in port"
      },
      {
        flow: "IN", 
        code: "106", 
        section: "Merchandise(exports)", 
        description: "Trade finance repayments in respect of exports"
       },
       {
        flow: "IN", 
        code: "107", 
        section: "Merchandise(exports)", 
        description: "Export proceeds where the Customs value of the shipment is less than M500"
       },
      {
       flow: "IN", 
       code: "108", 
       section: "Merchandise(exports)", 
       description: "Export payments where goods were declared as part of passenger baggage and no UCR is available"
      },
      {
       flow: "IN", 
       code: "109/01", 
       section: "Merchandise(exports)", 
       description: "Proceeds for goods purchased by non-residents where no physical export takes place, excluding gold, platinum, oil, petroleum, gemstones, copper, steel, coal, iron ore and merchanting transactions"
      },
      {
       flow: "IN", 
       code: "109/02", 
       section: "Merchandise(exports)", 
       description: "Proceeds for gold purchased by non-residents where no physical export will take place, excluding merchanting transactions"
      },
      {
       flow: "IN", 
       code: "109/03", 
       section: "Merchandise(exports)", 
       description: "Proceeds for platinum purchased by non-residents where no physical export will take place, excluding merchanting transactions"
      },
      {
       flow: "IN", 
       code: "109/04", 
       section: "Merchandise(exports)", 
       description: "Proceeds for crude oil purchased by non-residents where no physical export will take place, excluding merchanting transactions"
      },
      {
       flow: "IN", 
       code: "109/05", 
       section: "Merchandise(exports)", 
       description: "Proceeds for refined petroleum products purchased by non-residents where no physical export will take place, excluding merchanting transactions"
      },
      {
       flow: "IN", 
       code: "109/06", 
       section: "Merchandise(exports)", 
       description: "Proceeds for diamonds purchased by non-residents where no physical export will take place, excluding merchanting transactions"
      },
      {
       flow: "IN", 
       code: "109/07", 
       section: "Merchandise(exports)", 
       description: "Proceeds for steel purchased by non-residents where no physical export will take place, excluding merchanting transactions"
      },
      {
       flow: "IN", 
       code: "109/08", 
       section: "Merchandise(exports)", 
       description: "Proceeds for coal purchased by non-residents where no physical export will take place, excluding merchanting transactions"
      },
      {
       flow: "IN", 
       code: "109/09", 
       section: "Merchandise(exports)", 
       description: "Proceeds for iron ore purchased by non-residents where no physical export will take place, excluding merchanting transactions"
      },
      {
        flow: "IN", 
        code: "110", 
        section: "Merchandise(exports)", 
        description: "Merchanting transactions"
       },
      {
       flow: "IN", 
       code: "200", 
       section: "Intellectual property and other services", 
       description: "Adjustments/reversals/refunds applicable to intellectual property and service related items"
      },
      {
       flow: "IN", 
       code: "201", 
       section: "Intellectual property and other services", 
       description: "Rights assigned for licences to reproduce and/or distribute"
      },
      {
       flow: "IN", 
       code: "202", 
       section: "Intellectual property and other services", 
       description: "Rights assigned for using patents and inventions (licensing)"
      },
      {
       flow: "IN", 
       code: "203", 
       section: "Intellectual property and other services", 
       description: "Rights assigned for using patterns and designs (including industrial processes)"
      },
      {
       flow: "IN", 
       code: "204", 
       section: "Intellectual property and other services", 
       description: "Rights assigned for using copyrights"
      },
      {
       flow: "IN", 
       code: "205", 
       section: "Intellectual property and other services", 
       description: "Rights assigned for using franchises and trademarks"
      },
      {
       flow: "IN", 
       code: "210", 
       section: "Intellectual property and other services", 
       description: "Disposal of patents and inventions"
      },
      {
       flow: "IN", 
       code: "211", 
       section: "Intellectual property and other services", 
       description: "Disposal of patterns and designs (including industrial processes)"
      },
      {
       flow: "IN", 
       code: "212", 
       section: "Intellectual property and other services", 
       description: "Disposal of copyrights"
      },
      {
       flow: "IN", 
       code: "213", 
       section: "Intellectual property and other services", 
       description: "Disposal of franchises and trademarks"
      },
      {
       flow: "IN", 
       code: "220", 
       section: "Intellectual property and other services", 
       description: "Proceeds received for research and development services"
      },
      {
       flow: "IN", 
       code: "221", 
       section: "Intellectual property and other services", 
       description: "Funding received for research and development"
      },
      {
       flow: "IN", 
       code: "225", 
       section: "Intellectual property and other services", 
       description: "Sales of original manuscripts, sound recordings and films"
      },
      {
       flow: "IN", 
       code: "226", 
       section: "Intellectual property and other services", 
       description: "Receipt of funds relating to the production of motion pictures, radio and television programs and musical recordings"
      },
      {
       flow: "IN", 
       code: "230", 
       section: "Intellectual property and other services", 
       description: "The outright selling of ownership rights of software"
      },
      {
       flow: "IN", 
       code: "231", 
       section: "Intellectual property and other services", 
       description: "Computer-related services including maintenance, repair and consultancy"
      },
      {
       flow: "IN", 
       code: "232", 
       section: "Intellectual property and other services", 
       description: "Commercial sales of customised software and related licenses for use by customers"
      },
      {
       flow: "IN", 
       code: "233", 
       section: "Intellectual property and other services", 
       description: "Commercial sales of non-customised software on physical media with periodic licence to use"
      },
      {
       flow: "IN", 
       code: "234", 
       section: "Intellectual property and other services", 
       description: "Commercial sales of non-customised software provided on physical media with right to perpetual (ongoing) use"
      },
      {
       flow: "IN", 
       code: "235", 
       section: "Intellectual property and other services", 
       description: "Commercial sales of non-customised software provided for downloading or electronically made available with periodic license"
      },
      {
       flow: "IN", 
       code: "236", 
       section: "Intellectual property and other services", 
       description: "Commercial sales of non-customised software provided for downloading or electronically made available with single payment"
      },
      {
       flow: "IN", 
       code: "240/01", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on materials (excluding gold, platinum, crude oil, refined petroleum products, diamonds, steel, coal and iron ore)"
      },
      {
       flow: "IN", 
       code: "240/02", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on gold"
      },
      {
       flow: "IN", 
       code: "240/03", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on platinum"
      },
      {
       flow: "IN", 
       code: "240/04", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on crude oil"
      },
      {
       flow: "IN", 
       code: "240/05", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on refined petroleum products"
      },
      {
       flow: "IN", 
       code: "240/06", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on diamonds"
      },
      {
       flow: "IN", 
       code: "240/07", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on steel"
      },
      {
       flow: "IN", 
       code: "240/08", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on coal"
      },
      {
       flow: "IN", 
       code: "240/09", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on iron ore"
      },
      {
       flow: "IN", 
       code: "241", 
       section: "Intellectual property and other services", 
       description: "Repairs and maintenance on machinery and equipment"
      },
      {
       flow: "IN", 
       code: "242", 
       section: "Intellectual property and other services", 
       description: "Architectural, engineering and other technical services"
      },
      {
       flow: "IN", 
       code: "243", 
       section: "Intellectual property and other services", 
       description: "Agricultural, mining, waste treatment and depollution services"
      },
      {
       flow: "IN", 
       code: "250", 
       section: "Intellectual property and other services", 
       description: "Travel services for non-residents - business travel"
      },
      {
       flow: "IN", 
       code: "251", 
       section: "Intellectual property and other services", 
       description: "Travel services for non-residents - holiday travel"
      },
      {
       flow: "IN", 
       code: "252", 
       section: "Intellectual property and other services", 
       description: "Foreign exchange accepted by residents from non-residents"
      },
      {
       flow: "IN", 
       code: "255", 
       section: "Intellectual property and other services", 
       description: "Travel services for residents - business travel"
      },
      {
       flow: "IN", 
       code: "256", 
       section: "Intellectual property and other services", 
       description: "Travel services for residents - holiday travel"
      },
      {
       flow: "IN", 
       code: "260", 
       section: "Intellectual property and other services", 
       description: "Proceeds for travel services in respect of third parties - business travel"
      },
      {
       flow: "IN", 
       code: "261", 
       section: "Intellectual property and other services", 
       description: "Proceeds for travel services in respect of third parties - holiday travel"
      },
      {
       flow: "IN", 
       code: "265", 
       section: "Intellectual property and other services", 
       description: "Proceeds for telecommunication services"
      },
      {
       flow: "IN", 
       code: "266", 
       section: "Intellectual property and other services", 
       description: "Proceeds for information services including data, news related and news agency fees"
      },
      {
       flow: "IN", 
       code: "270/01", 
       section: "Intellectual property and other services", 
       description: "Proceeds for passenger services - road"
      },
      {
       flow: "IN", 
       code: "270/02", 
       section: "Intellectual property and other services", 
       description: "Proceeds for passenger services - rail"
      },
      {
       flow: "IN", 
       code: "270/03", 
       section: "Intellectual property and other services", 
       description: "Proceeds for passenger services - sea"
      },
      {
       flow: "IN", 
       code: "270/04", 
       section: "Intellectual property and other services", 
       description: "Proceeds for passenger services - air"
      },
      {
       flow: "IN", 
       code: "271/01", 
       section: "Intellectual property and other services", 
       description: "Proceeds for freight services - road"
      },
      {
       flow: "IN", 
       code: "271/02", 
       section: "Intellectual property and other services", 
       description: "Proceeds for freight services - rail"
      },
      {
       flow: "IN", 
       code: "271/03", 
       section: "Intellectual property and other services", 
       description: "Proceeds for freight services - sea"
      },
      {
       flow: "IN", 
       code: "271/04", 
       section: "Intellectual property and other services", 
       description: "Proceeds for freight services - air"
      },
      {
       flow: "IN", 
       code: "272/01", 
       section: "Intellectual property and other services", 
       description: "Proceeds for other transport services - road"
      },
      {
       flow: "IN", 
       code: "272/02", 
       section: "Intellectual property and other services", 
       description: "Proceeds for other transport services - rail"
      },
      {
       flow: "IN", 
       code: "272/03", 
       section: "Intellectual property and other services", 
       description: "Proceeds for other transport services - sea"
      },
      {
       flow: "IN", 
       code: "272/04", 
       section: "Intellectual property and other services", 
       description: "Proceeds for other transport services - air"
      },
      {
       flow: "IN", 
       code: "273/01", 
       section: "Intellectual property and other services", 
       description: "Proceeds for postal and courier services - road"
      },
      {
       flow: "IN", 
       code: "273/02", 
       section: "Intellectual property and other services", 
       description: "Proceeds for postal and courier services - rail"
      },
      {
       flow: "IN", 
       code: "273/03", 
       section: "Intellectual property and other services", 
       description: "Proceeds for postal and courier services - sea"
      },
      {
       flow: "IN", 
       code: "273/04", 
       section: "Intellectual property and other services", 
       description: "Proceeds for postal and courier services - air"
      },
      {
       flow: "IN", 
       code: "275", 
       section: "Intellectual property and other services", 
       description: "Commission and fees"
      },
      {
       flow: "IN", 
       code: "276", 
       section: "Intellectual property and other services", 
       description: "Proceeds for financial services charged for advice provided"
      },
      {
       flow: "IN", 
       code: "280", 
       section: "Intellectual property and other services", 
       description: "Proceeds for construction services"
      },
      {
       flow: "IN", 
       code: "281", 
       section: "Intellectual property and other services", 
       description: "Proceeds for government services"
      },
      {
       flow: "IN", 
       code: "282", 
       section: "Intellectual property and other services", 
       description: "Diplomatic transfers"
      },
      {
       flow: "IN", 
       code: "285", 
       section: "Intellectual property and other services", 
       description: "Tuition fees"
      },
      {
       flow: "IN", 
       code: "287", 
       section: "Intellectual property and other services", 
       description: "Proceeds for legal services"
      },
      {
       flow: "IN", 
       code: "288", 
       section: "Intellectual property and other services", 
       description: "Proceeds for accounting services"
      },
      {
       flow: "IN", 
       code: "289", 
       section: "Intellectual property and other services", 
       description: "Proceeds for management consulting services"
      },
      {
       flow: "IN", 
       code: "290", 
       section: "Intellectual property and other services", 
       description: "Proceeds for public relation services"
      },
      {
       flow: "IN", 
       code: "291", 
       section: "Intellectual property and other services", 
       description: "Proceeds for advertising and market research services"
      },
      {
       flow: "IN", 
       code: "292", 
       section: "Intellectual property and other services", 
       description: "Proceeds for managerial services"
      },
      {
       flow: "IN", 
       code: "293", 
       section: "Intellectual property and other services", 
       description: "Proceeds for medical and dental services"
      },
      {
       flow: "IN", 
       code: "294", 
       section: "Intellectual property and other services", 
       description: "Proceeds for educational services"
      },
      {
       flow: "IN", 
       code: "295", 
       section: "Intellectual property and other services", 
       description: "Operational leasing"
      },
      {
       flow: "IN", 
       code: "296", 
       section: "Intellectual property and other services", 
       description: "Proceeds for cultural and recreational services"
      },
      {
       flow: "IN", 
       code: "297", 
       section: "Intellectual property and other services", 
       description: "Proceeds for other business services not included elsewhere"
      },
      {
       flow: "IN", 
       code: "300", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Adjustments / Reversals / Refunds related to income and yields on financial assets"
      },
      {
       flow: "IN", 
       code: "301", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Dividends"
      },
      {
       flow: "IN", 
       code: "302", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Branch profits"
      },
      {
       flow: "IN", 
       code: "303", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Compensation paid by a non-resident to a resident employee temporarily abroad (excluding remittances)"
      },
      {
       flow: "IN", 
       code: "304", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Compensation paid by a non-resident to a non-resident employee in Lesotho (excluding remittances)"
      },
      {
       flow: "IN", 
       code: "305", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Compensation paid by a non-resident to a migrant worker employee (excluding remittances)"
      },
      {
       flow: "IN", 
       code: "306", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Compensation paid by a non-resident to a foreign national contract worker employee (excluding remittances)"
      },
      {
       flow: "IN", 
       code: "307", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Commission or brokerage"
      },
      {
       flow: "IN", 
       code: "308", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Rental"
      },
      {
       flow: "IN", 
       code: "309/01", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Interest received from a resident temporarily abroad in respect of loans"
      },
      {
       flow: "IN", 
       code: "309/02", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Interest received from a non-resident in respect of individual loans"
      },
      {
       flow: "IN", 
       code: "309/03", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Interest received from a non-resident in respect of study loans"
      },
      {
       flow: "IN", 
       code: "309/04", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Interest received from a non-resident in respect of shareholders loans"
      },
      {
       flow: "IN", 
       code: "309/05", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Interest received from a non-resident in respect of third party loans"
      },
      {
       flow: "IN", 
       code: "309/06", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Interest received from a non-resident in respect of trade finance loans"
      },
      {
       flow: "IN", 
       code: "309/07", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Interest received from a non-resident in respect of a bond"
      },
      {
       flow: "IN", 
       code: "309/08", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Interest received not in respect of loans"
      },
      {
       flow: "IN", 
       code: "310/01", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Income in respect of inward listed securities equity individual"
      },
      {
       flow: "IN", 
       code: "310/02", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Income in respect of inward listed securities equity corporate"
      },
      {
       flow: "IN", 
       code: "310/03", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Income in respect of inward listed securities equity bank"
      },
      {
       flow: "IN", 
       code: "310/04", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Income in respect of inward listed securities equity institution"
      },
      {
       flow: "IN", 
       code: "311/01", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Income in respect of inward listed securities debt individual"
      },
      {
       flow: "IN", 
       code: "311/02", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Income in respect of inward listed securities debt corporate"
      },
      {
       flow: "IN", 
       code: "311/03", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Income in respect of inward listed securities debt bank"
      },
      {
       flow: "IN", 
       code: "311/04", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Income in respect of inward listed securities debt institution"
      },
      {
       flow: "IN", 
       code: "312/01", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Income in respect of inward listed securities derivatives individual"
      },
      {
       flow: "IN", 
       code: "312/02", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Income in respect of inward listed securities derivatives corporate"
      },
      {
       flow: "IN", 
       code: "312/03", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Income in respect of inward listed securities derivatives bank"
      },
      {
       flow: "IN", 
       code: "312/04", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Income in respect of inward listed securities derivatives institution"
      },
      {
       flow: "IN", 
       code: "313", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Income earned abroad by a resident on an individual investment"
      },
      {
       flow: "IN", 
       code: "400", 
       section: "Transfers of a current nature", 
       description: "Adjustments / Reversals / Refunds related to transfers of a current nature"
      },
      {
       flow: "IN", 
       code: "401", 
       section: "Transfers of a current nature", 
       description: "Gifts"
      },
      {
       flow: "IN", 
       code: "402", 
       section: "Transfers of a current nature", 
       description: "Annual contributions"
      },
      {
       flow: "IN", 
       code: "403", 
       section: "Transfers of a current nature", 
       description: "Contributions in respect of social security schemes"
      },
      {
       flow: "IN", 
       code: "404", 
       section: "Transfers of a current nature", 
       description: "Contributions in respect of charitable, religious and cultural (excluding research and development)"
      },
      {
       flow: "IN", 
       code: "405", 
       section: "Transfers of a current nature", 
       description: "Other donations / aid to Government (excluding research and development)"
      },
      {
       flow: "IN", 
       code: "406", 
       section: "Transfers of a current nature", 
       description: "Other donations / aid to private sector (excluding research and development)"
      },
      {
       flow: "IN", 
       code: "407", 
       section: "Transfers of a current nature", 
       description: "Pensions"
      },
      {
       flow: "IN", 
       code: "408", 
       section: "Transfers of a current nature", 
       description: "Annuities (pension related)"
      },
      {
       flow: "IN", 
       code: "409", 
       section: "Transfers of a current nature", 
       description: "Inheritances"
      },
      {
       flow: "IN", 
       code: "410", 
       section: "Transfers of a current nature", 
       description: "Alimony"
      },
      {
       flow: "IN", 
       code: "411/01", 
       section: "Transfers of a current nature", 
       description: "Tax - Income tax"
      },
      {
       flow: "IN", 
       code: "411/02", 
       section: "Transfers of a current nature", 
       description: "Tax - VAT refunds"
      },
      {
       flow: "IN", 
       code: "411/03", 
       section: "Transfers of a current nature", 
       description: "Tax - Other"
      },
      {
       flow: "IN", 
       code: "412", 
       section: "Transfers of a current nature", 
       description: "Insurance premiums (non-life/short term)"
      },
      {
       flow: "IN", 
       code: "413", 
       section: "Transfers of a current nature", 
       description: "Insurance claims (non-life/short term)"
      },
      {
       flow: "IN", 
       code: "414", 
       section: "Transfers of a current nature", 
       description: "Insurance premiums (life)"
      },
      {
       flow: "IN", 
       code: "415", 
       section: "Transfers of a current nature", 
       description: "Insurance claims (life)"
      },
      {
       flow: "IN", 
       code: "416", 
       section: "Transfers of a current nature", 
       description: "Migrant worker remittances (excluding compensation)"
      },
      {
       flow: "IN", 
       code: "417", 
       section: "Transfers of a current nature", 
       description: "Foreign national contract worker remittances (excluding compensation)"
      },
      {
       flow: "IN", 
       code: "500", 
       section: "Transfers of a capital nature", 
       description: "Adjustments/reversals/refunds related to capital transfers and immigrants"
      },
      {
       flow: "IN", 
       code: "501", 
       section: "Transfers of a capital nature", 
       description: "Donations to Lesotho Government for fixed assets"
      },
      {
       flow: "IN", 
       code: "502", 
       section: "Transfers of a capital nature", 
       description: "Donations to corporate entities - fixed assets"
      },
      {
       flow: "IN", 
       code: "503", 
       section: "Transfers of a capital nature", 
       description: "Investment into property by a non-resident corporate entity"
      },
      {
       flow: "IN", 
       code: "504", 
       section: "Transfers of a capital nature", 
       description: "Disinvestment of property by a resident corporate entity"
      },
      {
       flow: "IN", 
       code: "510/01", 
       section: "Transfers of a capital nature", 
       description: "Investment into property by a non-resident individual"
      },
      {
       flow: "IN", 
       code: "510/02", 
       section: "Transfers of a capital nature", 
       description: "Investment by a non-resident individual - other"
      },
      {
       flow: "IN", 
       code: "511/01", 
       section: "Transfers of a capital nature", 
       description: "Disinvestment of capital by a resident individual - Shares"
      },
      {
       flow: "IN", 
       code: "511/02", 
       section: "Transfers of a capital nature", 
       description: "Disinvestment of capital by a resident individual - Bonds"
      },
      {
       flow: "IN", 
       code: "511/03", 
       section: "Transfers of a capital nature", 
       description: "Disinvestment of capital by a resident individual - Money market instruments"
      },
      {
       flow: "IN", 
       code: "511/04", 
       section: "Transfers of a capital nature", 
       description: "Disinvestment of capital by a resident individual - Deposits with a foreign bank"
      },
      {
       flow: "IN", 
       code: "511/05", 
       section: "Transfers of a capital nature", 
       description: "Disinvestment of capital by a resident individual - Mutual funds / collective investment schemes"
      },
      {
       flow: "IN", 
       code: "511/06", 
       section: "Transfers of a capital nature", 
       description: "Disinvestment of capital by a resident individual - Property"
      },
      {
       flow: "IN", 
       code: "511/07", 
       section: "Transfers of a capital nature", 
       description: "Disinvestment of capital by a resident individual - Other"
      },
      {
       flow: "IN", 
       code: "530/01", 
       section: "Transfers of a capital nature", 
       description: "Immigration"
      },
      {
       flow: "IN", 
       code: "600", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Adjustments / Reversals / Refunds related to financial investments/disinvestments and prudential investments"
      },
      {
       flow: "IN", 
       code: "601/01", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment in listed shares by a non-resident"
      },
      {
       flow: "IN", 
       code: "601/02", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment in non-listed shares by a non-resident"
      },
      {
       flow: "IN", 
       code: "602", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment into money market instruments by a non-resident"
      },
      {
       flow: "IN", 
       code: "603/01", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment into listed bonds by a non-resident (excluding loans)"
      },
      {
       flow: "IN", 
       code: "603/02", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment into non-listed bonds by a non-resident (excluding loans"
      },
      {
       flow: "IN", 
       code: "605/01", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment of shares by resident -agriculture, hunting, forestry and fishing"
      },
      {
       flow: "IN", 
       code: "605/02", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment of shares by resident - mining, quarrying and exploration"
      },
      {
       flow: "IN", 
       code: "605/03", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment of shares by resident - manufacturing"
      },
      {
       flow: "IN", 
       code: "605/04", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment of shares by resident - electricity, gas and water supply"
      },
      {
       flow: "IN", 
       code: "605/05", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment of shares by resident - construction"
      },
      {
       flow: "IN", 
       code: "605/06", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment of shares by resident - wholesale, retail, repairs, hotel and restaurants"
      },
      {
       flow: "IN", 
       code: "605/07", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment of shares by resident - transport and communication"
      },
      {
       flow: "IN", 
       code: "605/08", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment of shares by resident - financial services"
      },
      {
       flow: "IN", 
       code: "605/09", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment of shares by resident - community, social and personal services"
      },
      {
       flow: "IN", 
       code: "610/01", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities equity individual buy back"
      },
      {
       flow: "IN", 
       code: "610/02", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities equity corporate buy back"
      },
      {
       flow: "IN", 
       code: "610/03", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities equity bank buy back"
      },
      {
       flow: "IN", 
       code: "610/04", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities equity institution buy back"
      },
      {
       flow: "IN", 
       code: "611/01", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities debt individual redemption"
      },
      {
       flow: "IN", 
       code: "611/02", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities debt corporate redemption"
      },
      {
       flow: "IN", 
       code: "611/03", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities debt bank redemption"
      },
      {
       flow: "IN", 
       code: "611/04", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities debt institution redemption"
      },
      {
       flow: "IN", 
       code: "612/01", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities derivatives individual proceeds"
      },
      {
       flow: "IN", 
       code: "612/02", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities derivatives corporate proceeds"
      },
      {
       flow: "IN", 
       code: "612/03", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities derivatives bank proceeds"
      },
      {
       flow: "IN", 
       code: "612/04", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities derivatives institution proceeds"
      },
      {
       flow: "IN", 
       code: "615/01", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment by resident institutional investor - asset Manager"
      },
      {
       flow: "IN", 
       code: "615/02", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment by resident institutional investor - collective Investment Scheme"
      },
      {
       flow: "IN", 
       code: "615/03", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment by resident institutional investor - retirement Fund"
      },
      {
       flow: "IN", 
       code: "615/04", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment by resident institutional investor - life Linked"
      },
      {
       flow: "IN", 
       code: "615/05", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment by resident institutional investor - life Non Linked"
      },
      {
       flow: "IN", 
       code: "616", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Bank prudential disinvestment"
      },
      {
       flow: "IN", 
       code: "700", 
       section: "Derivatives", 
       description: "Adjustments/reversals/refunds related to derivatives"
      },
      {
       flow: "IN", 
       code: "701/01", 
       section: "Derivatives", 
       description: "Options - listed"
      },
      {
       flow: "IN", 
       code: "701/02", 
       section: "Derivatives", 
       description: "Options - unlisted"
      },
      {
       flow: "IN", 
       code: "702/01", 
       section: "Derivatives", 
       description: "Futures - listed"
      },
      {
       flow: "IN", 
       code: "702/02", 
       section: "Derivatives", 
       description: "Futures - unlisted"
      },
      {
       flow: "IN", 
       code: "703/01", 
       section: "Derivatives", 
       description: "Warrants - listed"
      },
      {
       flow: "IN", 
       code: "703/02", 
       section: "Derivatives", 
       description: "Warrants - unlisted"
      },
      {
       flow: "IN", 
       code: "704/01", 
       section: "Derivatives", 
       description: "Gold hedging - listed"
      },
      {
       flow: "IN", 
       code: "704/02", 
       section: "Derivatives", 
       description: "Gold hedging - unlisted"
      },
      {
       flow: "IN", 
       code: "705/01", 
       section: "Derivatives", 
       description: "Derivative not specified above - listed"
      },
      {
       flow: "IN", 
       code: "705/02", 
       section: "Derivatives", 
       description: "Derivative not specified above - unlisted"
      },
      {
       flow: "IN", 
       code: "800", 
       section: "Loan and Miscellaneous payments", 
       description: "Adjustments/reversals/refunds related to loan and miscellaneous payments"
      },
      {
       flow: "IN", 
       code: "801", 
       section: "Loan and Miscellaneous payments", 
       description: "Trade finance loan drawn down in Lesotho"
      },
      {
       flow: "IN", 
       code: "802", 
       section: "Loan and Miscellaneous payments", 
       description: "International Bond drawn down"
      },
      {
       flow: "IN", 
       code: "803", 
       section: "Loan and Miscellaneous payments", 
       description: "Loan made to a resident by a non-resident shareholder"
      },
      {
       flow: "IN", 
       code: "804", 
       section: "Loan and Miscellaneous payments", 
       description: "Loan made to a resident by a non-resident third party"
      },
      {
       flow: "IN", 
       code: "810", 
       section: "Loan and Miscellaneous payments", 
       description: "Repayment by a resident temporarily abroad of a loan granted by a resident"
      },
      {
       flow: "IN", 
       code: "815", 
       section: "Loan and Miscellaneous payments", 
       description: "Repayment of an individual loan to a resident"
      },
      {
       flow: "IN", 
       code: "816", 
       section: "Loan and Miscellaneous payments", 
       description: "Repayment of a study loan to a resident"
      },
      {
       flow: "IN", 
       code: "817", 
       section: "Loan and Miscellaneous payments", 
       description: "Repayment of a shareholders loan to a resident"
      },
      {
       flow: "IN", 
       code: "818", 
       section: "Loan and Miscellaneous payments", 
       description: "Repayment of a third party loan to a resident (excluding shareholders)"
      },
      {
       flow: "IN", 
       code: "819", 
       section: "Loan and Miscellaneous payments", 
       description: "Repayment of a trade finance loan to a resident"
      },
      {
       flow: "IN", 
       code: "830", 
       section: "Loan and Miscellaneous payments", 
       description: "Details of payments not classified"
      },
      {
       flow: "IN", 
       code: "832", 
       section: "Loan and Miscellaneous payments", 
       description: "Maloti drafts/cheques drawn on Vostro accounts (Only applicable if no description is available)"
      },
      {
       flow: "IN", 
       code: "833", 
       section: "Loan and Miscellaneous payments", 
       description: "Credit/Debit card company settlement as well as money remitter settlements"
      },
      {
        flow: "IN", 
        code: "ZZ1", 
        section: "Non-Reportable Transactions", 
        description: "For Internal Bank use ONLY"
      },
      {
       flow: "OUT",
       code: "100",
       section: "Merchandise (imports)", 
       description: "Adjustments/reversals/refunds applicable to merchandise"
      },
      {
       flow: "OUT", 
       code: "101/01", 
       section: "Merchandise (imports)", 
       description: "Import advance payment (excluding capital goods, gold, platinum, crude oil, refined petroleum products, gemstones, steel, coal, iron ore, copper and goods imported via the country Post Office)"
      },
      {
       flow: "OUT", 
       code: "101/02", 
       section: "Merchandise (imports)", 
       description: "Import advance payment - capital goods"
      },
      {
       flow: "OUT", 
       code: "101/03", 
       section: "Merchandise (imports)", 
       description: "Import advance payment - gold"
      },
      {
       flow: "OUT", 
       code: "101/04", 
       section: "Merchandise (imports)", 
       description: "Import advance payment - platinum"
      },
      {
       flow: "OUT", 
       code: "101/05", 
       section: "Merchandise (imports)", 
       description: "Import advance payment - crude oil"
      },
      {
       flow: "OUT", 
       code: "101/06", 
       section: "Merchandise (imports)", 
       description: "Import advance payment - refined petroleum products"
      },
      {
       flow: "OUT", 
       code: "101/07", 
       section: "Merchandise (imports)", 
       description: "Import advance payment - gemstones (all precious stones, including diamonds, sapphires, rubies, tanzanite, emeralds, alexandrines, moonstones, pearls, etc.)"
      },
      {
       flow: "OUT", 
       code: "101/08", 
       section: "Merchandise (imports)", 
       description: "Import advance payment - steel"
      },
      {
       flow: "OUT", 
       code: "101/09", 
       section: "Merchandise (imports)", 
       description: "Import advance payment - coal"
      },
      {
       flow: "OUT", 
       code: "101/10", 
       section: "Merchandise (imports)", 
       description: "Import advance payment - iron ore"
      },
      {
       flow: "OUT", 
       code: "101/11", 
       section: "Merchandise (imports)", 
       description: "Import advance payment - goods imported via the country\u2019s Post Office"
      },
      {
       flow: "OUT", 
       code: "103/01", 
       section: "Merchandise (imports)", 
       description: "Import payment (excluding capital goods, gold, platinum, crude oil, refined petroleum products, gemstones , steel, coal, iron ore, copper and goods imported via the country Post Office)"
      },
      {
       flow: "OUT", 
       code: "103/02", 
       section: "Merchandise (imports)", 
       description: "Import payment - capital goods"
      },
      {
       flow: "OUT", 
       code: "103/03", 
       section: "Merchandise (imports)", 
       description: "Import payment - gold"
      },
      {
       flow: "OUT", 
       code: "103/04", 
       section: "Merchandise (imports)", 
       description: "Import payment - platinum"
      },
      {
       flow: "OUT", 
       code: "103/05", 
       section: "Merchandise (imports)", 
       description: "Import payment - crude oil"
      },
      {
       flow: "OUT", 
       code: "103/06", 
       section: "Merchandise (imports)", 
       description: "Import payment - refined petroleum products"
      },
      {
       flow: "OUT", 
       code: "103/07", 
       section: "Merchandise (imports)", 
       description: "Import payment - diamonds"
      },
      {
       flow: "OUT", 
       code: "103/08", 
       section: "Merchandise (imports)", 
       description: "Import payment - steel"
      },
      {
       flow: "OUT", 
       code: "103/09", 
       section: "Merchandise (imports)", 
       description: "Import payment - coal"
      },
      {
       flow: "OUT", 
       code: "103/10", 
       section: "Merchandise (imports)", 
       description: "Import payment - iron ore"
      },
      {
       flow: "OUT", 
       code: "103/11", 
       section: "Merchandise (imports)", 
       description: "Import payment - goods imported via the country\u2019s Post Office"
      },
      {
       flow: "OUT", 
       code: "105", 
       section: "Merchandise (imports)", 
       description: "Consumables acquired in port"
      },
      {
       flow: "OUT", 
       code: "106", 
       section: "Merchandise (imports)", 
       description: "Repayment of trade finance for imports"
      },
      {
        flow: "OUT", 
        code: "107", 
        section: "Merchandise (imports)", 
        description: "Import payments where the Customs value of the shipment is less than M500"
       },
      {
       flow: "OUT", 
       code: "108", 
       section: "Merchandise (imports)", 
       description: "Import payments where goods were declared as part of passenger baggage and no MRN is available"
      },
      {
       flow: "OUT", 
       code: "109/01", 
       section: "Merchandise (imports)", 
       description: "Proceeds for goods purchased by non-residents where no physical export takes place, excluding gold, platinum, oil, petroleum, diamonds, steel, coal, iron ore and merchanting transactions"
      },
      {
       flow: "OUT", 
       code: "109/02", 
       section: "Merchandise (imports)", 
       description: "Payments for gold purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
      },
      {
       flow: "OUT", 
       code: "109/03", 
       section: "Merchandise (imports)", 
       description: "Payments for platinum purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
      },
      {
       flow: "OUT", 
       code: "109/04", 
       section: "Merchandise (imports)", 
       description: "Payments for crude oil purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
      },
      {
       flow: "OUT", 
       code: "109/05", 
       section: "Merchandise (imports)", 
       description: "Payments for refined petroleum products purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
      },
      {
       flow: "OUT", 
       code: "109/06", 
       section: "Merchandise (imports)", 
       description: "Payments for diamonds purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
      },
      {
       flow: "OUT", 
       code: "109/07", 
       section: "Merchandise (imports)", 
       description: "Payments for steel purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
      },
      {
       flow: "OUT", 
       code: "109/08", 
       section: "Merchandise (imports)", 
       description: "Payments for coal purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
      },
      {
       flow: "OUT", 
       code: "109/09", 
       section: "Merchandise (imports)", 
       description: "Payments for iron ore purchased from non-residents in cases where no physical import will take place, excluding merchanting transactions"
      },
      {
       flow: "OUT", 
       code: "110", 
       section: "Merchandise (imports)", 
       description: "Merchanting transaction"
      },
      {
       flow: "OUT", 
       code: "200", 
       section: "Intellectual property and other services", 
       description: "Adjustments/reversals/refunds applicable to intellectual property and service related items"
      },
      {
       flow: "OUT", 
       code: "201", 
       section: "Intellectual property and other services", 
       description: "Rights obtained for licences to reproduce and/or distribute"
      },
      {
       flow: "OUT", 
       code: "202", 
       section: "Intellectual property and other services", 
       description: "Rights obtained for using patents and inventions (licensing)"
      },
      {
       flow: "OUT", 
       code: "203", 
       section: "Intellectual property and other services", 
       description: "Rights obtained for using patterns and designs (including industrial processes)"
      },
      {
       flow: "OUT", 
       code: "204", 
       section: "Intellectual property and other services", 
       description: "Rights obtained for using copyrights"
      },
      {
       flow: "OUT", 
       code: "205", 
       section: "Intellectual property and other services", 
       description: "Rights obtained for using franchises and trademarks"
      },
      {
       flow: "OUT", 
       code: "210", 
       section: "Intellectual property and other services", 
       description: "Acquisition of patents and inventions"
      },
      {
       flow: "OUT", 
       code: "211", 
       section: "Intellectual property and other services", 
       description: "Acquisition of patterns and designs (including industrial processes)"
      },
      {
       flow: "OUT", 
       code: "212", 
       section: "Intellectual property and other services", 
       description: "Acquisition of copyrights"
      },
      {
       flow: "OUT", 
       code: "213", 
       section: "Intellectual property and other services", 
       description: "Acquisition of franchises and trademarks"
      },
      {
       flow: "OUT", 
       code: "220", 
       section: "Intellectual property and other services", 
       description: "Payments for research and development services"
      },
      {
       flow: "OUT", 
       code: "221", 
       section: "Intellectual property and other services", 
       description: "Funding for research and development"
      },
      {
       flow: "OUT", 
       code: "225", 
       section: "Intellectual property and other services", 
       description: "Acquisition of original manuscripts, sound recordings and films"
      },
      {
       flow: "OUT", 
       code: "226", 
       section: "Intellectual property and other services", 
       description: "Payment relating to the production of motion pictures, radio and television programs and musical recordings"
      },
      {
       flow: "OUT", 
       code: "230", 
       section: "Intellectual property and other services", 
       description: "The outright purchasing of ownership rights of software"
      },
      {
       flow: "OUT", 
       code: "231", 
       section: "Intellectual property and other services", 
       description: "Computer-related services including maintenance, repair and consultancy"
      },
      {
       flow: "OUT", 
       code: "232", 
       section: "Intellectual property and other services", 
       description: "Commercial purchases of customised software and related licenses to use"
      },
      {
       flow: "OUT", 
       code: "233", 
       section: "Intellectual property and other services", 
       description: "Commercial purchases of non-customised software on physical media with periodic licence to use"
      },
      {
       flow: "OUT", 
       code: "234", 
       section: "Intellectual property and other services", 
       description: "Commercial purchases of non-customised software provided on physical media with right to perpetual (ongoing) use"
      },
      {
       flow: "OUT", 
       code: "235", 
       section: "Intellectual property and other services", 
       description: "Commercial purchases of non-customised software downloaded or electronically acquired with periodic license"
      },
      {
       flow: "OUT", 
       code: "236", 
       section: "Intellectual property and other services", 
       description: "Commercial purchases of non-customised software downloaded or electronically acquired with single payment"
      },
      {
       flow: "OUT", 
       code: "240/01", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on materials (excluding gold, platinum, crude oil, refined petroleum products, gemstones , steel, copper, coal and iron ore)"
      },
      {
       flow: "OUT", 
       code: "240/02", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on gold"
      },
      {
       flow: "OUT", 
       code: "240/03", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on platinum"
      },
      {
       flow: "OUT", 
       code: "240/04", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on crude oil"
      },
      {
       flow: "OUT", 
       code: "240/05", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on refined petroleum products"
      },
      {
       flow: "OUT", 
       code: "240/06", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on diamonds"
      },
      {
       flow: "OUT", 
       code: "240/07", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on steel"
      },
      {
       flow: "OUT", 
       code: "240/08", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on coal"
      },
      {
       flow: "OUT", 
       code: "240/09", 
       section: "Intellectual property and other services", 
       description: "Fees for processing - processing done on iron ore"
      },
      {
       flow: "OUT", 
       code: "241", 
       section: "Intellectual property and other services", 
       description: "Repairs and maintenance on machinery and equipment"
      },
      {
       flow: "OUT", 
       code: "242", 
       section: "Intellectual property and other services", 
       description: "Architectural, engineering and other technical services"
      },
      {
       flow: "OUT", 
       code: "243", 
       section: "Intellectual property and other services", 
       description: "Agricultural, mining, waste treatment and depollution services"
      },
      {
       flow: "OUT", 
       code: "250", 
       section: "Intellectual property and other services", 
       description: "Travel services for non-residents - business travel"
      },
      {
       flow: "OUT", 
       code: "251", 
       section: "Intellectual property and other services", 
       description: "Travel services for non-residents - holiday travel"
      },
      {
       flow: "OUT", 
       code: "255", 
       section: "Intellectual property and other services", 
       description: "Travel services for residents - business travel"
      },
      {
       flow: "OUT", 
       code: "256", 
       section: "Intellectual property and other services", 
       description: "Travel services for residents - holiday travel"
      },
      {
       flow: "OUT", 
       code: "260", 
       section: "Intellectual property and other services", 
       description: "Payment for travel services in respect of third parties - business travel"
      },
      {
       flow: "OUT", 
       code: "261", 
       section: "Intellectual property and other services", 
       description: "Payment for travel services in respect of third parties - holiday travel"
      },
      {
       flow: "OUT", 
       code: "265", 
       section: "Intellectual property and other services", 
       description: "Payment for telecommunication services"
      },
      {
       flow: "OUT", 
       code: "266", 
       section: "Intellectual property and other services", 
       description: "Payment for information services including data, news related and news agency fees"
      },
      {
       flow: "OUT", 
       code: "270/01", 
       section: "Intellectual property and other services", 
       description: "Payment for passenger services - road"
      },
      {
       flow: "OUT", 
       code: "270/02", 
       section: "Intellectual property and other services", 
       description: "Payment for passenger services - rail"
      },
      {
       flow: "OUT", 
       code: "270/03", 
       section: "Intellectual property and other services", 
       description: "Payment for passenger services - sea"
      },
      {
       flow: "OUT", 
       code: "270/04", 
       section: "Intellectual property and other services", 
       description: "Payment for passenger services - air"
      },
      {
       flow: "OUT", 
       code: "271/01", 
       section: "Intellectual property and other services", 
       description: "Payment for freight services - road"
      },
      {
       flow: "OUT", 
       code: "271/02", 
       section: "Intellectual property and other services", 
       description: "Payment for freight services - rail"
      },
      {
       flow: "OUT", 
       code: "271/03", 
       section: "Intellectual property and other services", 
       description: "Payment for freight services - sea"
      },
      {
       flow: "OUT", 
       code: "271/04", 
       section: "Intellectual property and other services", 
       description: "Payment for freight services - air"
      },
      {
       flow: "OUT", 
       code: "272/01", 
       section: "Intellectual property and other services", 
       description: "Payment for other transport services - road"
      },
      {
       flow: "OUT", 
       code: "272/02", 
       section: "Intellectual property and other services", 
       description: "Payment for other transport services - rail"
      },
      {
       flow: "OUT", 
       code: "272/03", 
       section: "Intellectual property and other services", 
       description: "Payment for other transport services - sea"
      },
      {
       flow: "OUT", 
       code: "272/04", 
       section: "Intellectual property and other services", 
       description: "Payment for other transport services - air"
      },
      {
       flow: "OUT", 
       code: "273/01", 
       section: "Intellectual property and other services", 
       description: "Payment for postal and courier services - road"
      },
      {
       flow: "OUT", 
       code: "273/02", 
       section: "Intellectual property and other services", 
       description: "Payment for postal and courier services - rail"
      },
      {
       flow: "OUT", 
       code: "273/03", 
       section: "Intellectual property and other services", 
       description: "Payment for postal and courier services - sea"
      },
      {
       flow: "OUT", 
       code: "273/04", 
       section: "Intellectual property and other services", 
       description: "Payment for postal and courier services - air"
      },
      {
       flow: "OUT", 
       code: "275", 
       section: "Intellectual property and other services", 
       description: "Commission and fees"
      },
      {
       flow: "OUT", 
       code: "276", 
       section: "Intellectual property and other services", 
       description: "Financial service fees charged for advice provided"
      },
      {
       flow: "OUT", 
       code: "280", 
       section: "Intellectual property and other services", 
       description: "Payment for construction services"
      },
      {
       flow: "OUT", 
       code: "281", 
       section: "Intellectual property and other services", 
       description: "Payment for government services"
      },
      {
       flow: "OUT", 
       code: "282", 
       section: "Intellectual property and other services", 
       description: "Diplomatic transfers"
      },
      {
       flow: "OUT", 
       code: "285", 
       section: "Intellectual property and other services", 
       description: "Tuition fees"
      },
      {
       flow: "OUT", 
       code: "287", 
       section: "Intellectual property and other services", 
       description: "Payment for legal services"
      },
      {
       flow: "OUT", 
       code: "288", 
       section: "Intellectual property and other services", 
       description: "Payment for accounting services"
      },
      {
       flow: "OUT", 
       code: "289", 
       section: "Intellectual property and other services", 
       description: "Payment for management consulting services"
      },
      {
       flow: "OUT", 
       code: "290", 
       section: "Intellectual property and other services", 
       description: "Payment for public relation services"
      },
      {
       flow: "OUT", 
       code: "291", 
       section: "Intellectual property and other services", 
       description: "Payment for advertising and market research services"
      },
      {
       flow: "OUT", 
       code: "292", 
       section: "Intellectual property and other services", 
       description: "Payment for managerial services"
      },
      {
       flow: "OUT", 
       code: "293", 
       section: "Intellectual property and other services", 
       description: "Payment for medical and dental services"
      },
      {
       flow: "OUT", 
       code: "294", 
       section: "Intellectual property and other services", 
       description: "Payment for educational services"
      },
      {
       flow: "OUT", 
       code: "295", 
       section: "Intellectual property and other services", 
       description: "Operational leasing"
      },
      {
       flow: "OUT", 
       code: "296", 
       section: "Intellectual property and other services", 
       description: "Payment for cultural and recreational services"
      },
      {
       flow: "OUT", 
       code: "297", 
       section: "Intellectual property and other services", 
       description: "Payment for other business services not included elsewhere"
      },
      {
       flow: "OUT", 
       code: "300", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Adjustments/reversals/refunds related to income and yields on financial assets"
      },
      {
       flow: "OUT", 
       code: "301", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Dividends"
      },
      {
       flow: "OUT", 
       code: "302", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Branch profits"
      },
      {
       flow: "OUT", 
       code: "303", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Compensation paid by a resident to a resident employee temporarily abroad (excluding remittances)"
      },
      {
       flow: "OUT", 
       code: "304", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Compensation paid by a resident to a non-resident employee (excluding remittances)"
      },
      {
       flow: "OUT", 
       code: "305", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Compensation paid by a resident to a migrant worker employee (excluding remittances)"
      },
      {
       flow: "OUT", 
       code: "306", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Compensation paid by a resident to a foreign national contract worker employee (excluding remittances)"
      },
      {
       flow: "OUT", 
       code: "307", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Commission or brokerage"
      },
      {
       flow: "OUT", 
       code: "308", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Rental"
      },
      {
       flow: "OUT", 
       code: "309/04", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Interest paid to a non-resident in respect of shareholders loans"
      },
      {
       flow: "OUT", 
       code: "309/05", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Interest paid to a non-resident in respect of third party loans"
      },
      {
       flow: "OUT", 
       code: "309/06", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Interest paid to a non-resident in respect of trade finance loans"
      },
      {
       flow: "OUT", 
       code: "309/07", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Interest paid to a non-resident in respect of a bond"
      },
      {
       flow: "OUT", 
       code: "309/08", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Interest paid not in respect of loans"
      },
      {
       flow: "OUT", 
       code: "312/01", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Fee in respect of inward listed securities derivatives individual"
      },
      {
       flow: "OUT", 
       code: "312/02", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Fee in respect of inward listed securities derivatives corporate"
      },
      {
       flow: "OUT", 
       code: "312/03", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Fee in respect of inward listed securities derivatives bank"
      },
      {
       flow: "OUT", 
       code: "312/04", 
       section: "Transactions relating to income and yields on financial assets", 
       description: "Fee in respect of inward listed securities derivatives institution"
      },
      {
       flow: "OUT", 
       code: "400", 
       section: "Transfers of a current nature", 
       description: "Adjustments / Reversals / Refunds related to transfers of a current nature"
      },
      {
       flow: "OUT", 
       code: "401", 
       section: "Transfers of a current nature", 
       description: "Gifts"
      },
      {
       flow: "OUT", 
       code: "402", 
       section: "Transfers of a current nature", 
       description: "Annual contributions"
      },
      {
       flow: "OUT", 
       code: "403", 
       section: "Transfers of a current nature", 
       description: "Contributions in respect of social security schemes"
      },
      {
       flow: "OUT", 
       code: "404", 
       section: "Transfers of a current nature", 
       description: "Contributions in respect of foreign charitable,religious and cultural (excluding research and development)"
      },
      {
       flow: "OUT", 
       code: "405", 
       section: "Transfers of a current nature", 
       description: "Other donations / aid to a foreign Government (excluding research and development)"
      },
      {
       flow: "OUT", 
       code: "406", 
       section: "Transfers of a current nature", 
       description: "Other donations / aid to a foreign private sector (excluding research and development)"
      },
      {
       flow: "OUT", 
       code: "407", 
       section: "Transfers of a current nature", 
       description: "Pensions"
      },
      {
       flow: "OUT", 
       code: "408", 
       section: "Transfers of a current nature", 
       description: "Annuities (pension related)"
      },
      {
       flow: "OUT", 
       code: "409", 
       section: "Transfers of a current nature", 
       description: "Inheritances"
      },
      {
       flow: "OUT", 
       code: "410", 
       section: "Transfers of a current nature", 
       description: "Alimony"
      },
      {
       flow: "OUT", 
       code: "411/01", 
       section: "Transfers of a current nature", 
       description: "Tax - Income tax"
      },
      {
       flow: "OUT", 
       code: "411/02", 
       section: "Transfers of a current nature", 
       description: "Tax - VAT refunds"
      },
      {
       flow: "OUT", 
       code: "411/03", 
       section: "Transfers of a current nature", 
       description: "Tax - Other"
      },
      {
       flow: "OUT", 
       code: "412", 
       section: "Transfers of a current nature", 
       description: "Insurance premiums (non-life/short term)"
      },
      {
       flow: "OUT", 
       code: "413", 
       section: "Transfers of a current nature", 
       description: "Insurance claims (non-life/short term)"
      },
      {
       flow: "OUT", 
       code: "414", 
       section: "Transfers of a current nature", 
       description: "Insurance premiums (life)"
      },
      {
       flow: "OUT", 
       code: "415", 
       section: "Transfers of a current nature", 
       description: "Insurance claims (life)"
      },
      {
       flow: "OUT", 
       code: "416", 
       section: "Transfers of a current nature", 
       description: "Migrant worker remittances (excluding compensation)"
      },
      {
       flow: "OUT", 
       code: "417", 
       section: "Transfers of a current nature", 
       description: "Foreign national contract worker remittances (excluding compensation)"
      },
      {
       flow: "OUT", 
       code: "500", 
       section: "Transfers of a capital nature", 
       description: "Adjustments/reversals/refunds related to capital transfers and emigrants"
      },
      {
       flow: "OUT", 
       code: "501", 
       section: "Transfers of a capital nature", 
       description: "Donations by Lesotho Government for fixed assets"
      },
      {
       flow: "OUT", 
       code: "502", 
       section: "Transfers of a capital nature", 
       description: "Donations by corporate entities for fixed assets"
      },
      {
       flow: "OUT", 
       code: "503", 
       section: "Transfers of a capital nature", 
       description: "Disinvestment of property by a non-resident corporate entity"
      },
      {
       flow: "OUT", 
       code: "504", 
       section: "Transfers of a capital nature", 
       description: "Investment into property by a resident corporate entity"
      },
      {
       flow: "OUT", 
       code: "510/01", 
       section: "Transfers of a capital nature", 
       description: "Disinvestment of property by a non-resident individual"
      },
      {
       flow: "OUT", 
       code: "510/02", 
       section: "Transfers of a capital nature", 
       description: "Disinvestment by a non-resident individual - other"
      },
      {
       flow: "OUT", 
       code: "511/01", 
       section: "Transfers of a capital nature", 
       description: "Investment by a resident individual not related to the investment allowance - Shares"
      },
      {
       flow: "OUT", 
       code: "511/02", 
       section: "Transfers of a capital nature", 
       description: "Investment by a resident individual not related to the investment allowance - Bonds"
      },
      {
       flow: "OUT", 
       code: "511/03", 
       section: "Transfers of a capital nature", 
       description: "Investment by a resident individual not related to the investment allowance - Money market instruments"
      },
      {
       flow: "OUT", 
       code: "511/04", 
       section: "Transfers of a capital nature", 
       description: "Investment by a resident individual not related to the investment allowance - Deposits with a foreign bank"
      },
      {
       flow: "OUT", 
       code: "511/05", 
       section: "Transfers of a capital nature", 
       description: "Investment by a resident individual not related to the investment allowance - Mutual funds / collective investment schemes"
      },
      {
       flow: "OUT", 
       code: "511/06", 
       section: "Transfers of a capital nature", 
       description: "Investment by a resident individual not related to the investment allowance - Property"
      },
      {
       flow: "OUT", 
       code: "511/07", 
       section: "Transfers of a capital nature", 
       description: "Investment by a resident individual not related to the investment allowance - Other"
      },
      {
       flow: "OUT", 
       code: "512/01", 
       section: "Transfers of a capital nature", 
       description: "Foreign investment by a resident individual in respect of the investment allowance - Shares"
      },
      {
       flow: "OUT", 
       code: "512/02", 
       section: "Transfers of a capital nature", 
       description: "Foreign investment by a resident individual in respect of the investment allowance - Bonds"
      },
      {
       flow: "OUT", 
       code: "512/03", 
       section: "Transfers of a capital nature", 
       description: "Foreign investment by a resident individual in respect of the investment allowance - Money market instruments"
      },
      {
       flow: "OUT", 
       code: "512/04", 
       section: "Transfers of a capital nature", 
       description: "Foreign investment by a resident individual in respect of the investment allowance - Deposits with a foreign bank"
      },
      {
       flow: "OUT", 
       code: "512/05", 
       section: "Transfers of a capital nature", 
       description: "Foreign investment by a resident individual in respect of the investment allowance - Mutual funds/collective investment schemes"
      },
      {
       flow: "OUT", 
       code: "512/06", 
       section: "Transfers of a capital nature", 
       description: "Foreign investment by a resident individual in respect of the investment allowance - Property"
      },
      {
       flow: "OUT", 
       code: "512/07", 
       section: "Transfers of a capital nature", 
       description: "Foreign investment by a resident individual in respect of the investment allowance - Other"
      },
      {
       flow: "OUT", 
       code: "513", 
       section: "Transfers of a capital nature", 
       description: "Investment by a resident individual originating from a local source into an account conducted in foreign currency held at an Authorised Dealer in the Country"
      },
      {
       flow: "OUT", 
       code: "530/01", 
       section: "Transfers of a capital nature", 
       description: "Emigration foreign capital allowance - fixed property"
      },
      {
       flow: "OUT", 
       code: "530/02", 
       section: "Transfers of a capital nature", 
       description: "Emigration foreign capital allowance - listed investments"
      },
      {
       flow: "OUT", 
       code: "530/03", 
       section: "Transfers of a capital nature", 
       description: "Emigration foreign capital allowance - unlisted investments"
      },
      {
       flow: "OUT", 
       code: "530/04", 
       section: "Transfers of a capital nature", 
       description: "Emigration foreign capital allowance - insurance policies"
      },
      {
       flow: "OUT", 
       code: "530/05", 
       section: "Transfers of a capital nature", 
       description: "Emigration foreign capital allowance - cash"
      },
      {
       flow: "OUT", 
       code: "530/06", 
       section: "Transfers of a capital nature", 
       description: "Emigration foreign capital allowance - debtors"
      },
      {
       flow: "OUT", 
       code: "530/07", 
       section: "Transfers of a capital nature", 
       description: "Emigration foreign capital allowance - capital distribution from trusts"
      },
      {
       flow: "OUT", 
       code: "530/08", 
       section: "Transfers of a capital nature", 
       description: "Emigration foreign capital allowance -other assets"
      },
      {
       flow: "OUT", 
       code: "600", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Adjustments/reversals/refunds related to financial investments/disinvestments and prudential investments"
      },
      {
       flow: "OUT", 
       code: "601/01", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Listed shares - sale proceeds paid to a non-resident"
      },
      {
       flow: "OUT", 
       code: "601/02", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Non-listed shares - sale proceeds paid to a non-resident"
      },
      {
       flow: "OUT", 
       code: "602", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment of money market instruments by a non-resident"
      },
      {
       flow: "OUT", 
       code: "603/01", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment of listed bonds by a non-resident (excluding loans)"
      },
      {
       flow: "OUT", 
       code: "603/02", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Disinvestment of non-listed bonds by a non-resident (excluding loans)"
      },
      {
       flow: "OUT", 
       code: "605/01", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment into shares by a resident entity - Agriculture, hunting, forestry and fishing"
      },
      {
       flow: "OUT", 
       code: "605/02", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment into shares by a resident entity - Mining, quarrying and exploration"
      },
      {
       flow: "OUT", 
       code: "605/03", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment into shares by a resident entity - Manufacturing"
      },
      {
       flow: "OUT", 
       code: "605/04", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment into shares by a resident entity - Electricity, gas and water supply"
      },
      {
       flow: "OUT", 
       code: "605/05", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment into shares by a resident entity - Construction"
      },
      {
       flow: "OUT", 
       code: "605/06", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment into shares by a resident entity - Wholesale, retail, repairs, hotel and restaurants"
      },
      {
       flow: "OUT", 
       code: "605/07", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment into shares by a resident entity - Transport and communication"
      },
      {
       flow: "OUT", 
       code: "605/08", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment into shares by a resident entity - Financial services"
      },
      {
       flow: "OUT", 
       code: "605/09", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment into shares by a resident entity - Community, social and personal services"
      },
      {
       flow: "OUT", 
       code: "610/01", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities equity individual"
      },
      {
       flow: "OUT", 
       code: "610/02", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities equity corporate"
      },
      {
       flow: "OUT", 
       code: "610/03", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities equity bank"
      },
      {
       flow: "OUT", 
       code: "610/04", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities equity institution"
      },
      {
       flow: "OUT", 
       code: "611/01", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities debt individual"
      },
      {
       flow: "OUT", 
       code: "611/02", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities debt corporate"
      },
      {
       flow: "OUT", 
       code: "611/03", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities debt bank"
      },
      {
       flow: "OUT", 
       code: "611/04", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities debt institution"
      },
      {
       flow: "OUT", 
       code: "612/01", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities derivatives individual"
      },
      {
       flow: "OUT", 
       code: "612/02", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities derivatives corporate"
      },
      {
       flow: "OUT", 
       code: "612/03", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities derivatives bank"
      },
      {
       flow: "OUT", 
       code: "612/04", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Inward listed securities derivatives institution"
      },
      {
       flow: "OUT", 
       code: "615/01", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment by resident institutional investor - Asset Manager"
      },
      {
       flow: "OUT", 
       code: "615/02", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment by resident institutional investor - Collective Investment Scheme"
      },
      {
       flow: "OUT", 
       code: "615/03", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment by resident institutional investor - Retirement Fund"
      },
      {
       flow: "OUT", 
       code: "615/04", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment by resident institutional investor - Life Linked"
      },
      {
       flow: "OUT", 
       code: "615/05", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Investment by resident institutional investor - Life Non Linked"
      },
      {
       flow: "OUT", 
       code: "616", 
       section: "Financial investments/disinvestments and prudential investments", 
       description: "Bank prudential investment"
      },
      {
       flow: "OUT", 
       code: "700", 
       section: "Derivatives", 
       description: "Adjustments/reversals/refunds related to derivatives"
      },
      {
       flow: "OUT", 
       code: "701/01", 
       section: "Derivatives", 
       description: "Options - listed"
      },
      {
       flow: "OUT", 
       code: "701/02", 
       section: "Derivatives", 
       description: "Options - unlisted"
      },
      {
       flow: "OUT", 
       code: "702/01", 
       section: "Derivatives", 
       description: "Futures - listed"
      },
      {
       flow: "OUT", 
       code: "702/02", 
       section: "Derivatives", 
       description: "Futures - unlisted"
      },
      {
       flow: "OUT", 
       code: "703/01", 
       section: "Derivatives", 
       description: "Warrants - listed"
      },
      {
       flow: "OUT", 
       code: "703/02", 
       section: "Derivatives", 
       description: "Warrants - unlisted"
      },
      {
       flow: "OUT", 
       code: "704/01", 
       section: "Derivatives", 
       description: "Gold hedging - listed"
      },
      {
       flow: "OUT", 
       code: "704/02", 
       section: "Derivatives", 
       description: "Gold hedging - unlisted"
      },
      {
       flow: "OUT", 
       code: "705/01", 
       section: "Derivatives", 
       description: "Derivative not specified above - listed"
      },
      {
       flow: "OUT", 
       code: "705/02", 
       section: "Derivatives", 
       description: "Derivative not specified above - unlisted"
      },
      {
       flow: "OUT", 
       code: "800", 
       section: "Loan and Miscellaneous payments", 
       description: "Adjustments / Reversals / Refunds related to loan and miscellaneous payments"
      },
      {
       flow: "OUT", 
       code: "801", 
       section: "Loan and Miscellaneous payments", 
       description: "Repayment of trade finance drawn down in Lesotho"
      },
      {
       flow: "OUT", 
       code: "802", 
       section: "Loan and Miscellaneous payments", 
       description: "Repayment of an international Bond drawn down"
      },
      {
       flow: "OUT", 
       code: "803", 
       section: "Loan and Miscellaneous payments", 
       description: "Repayment by a resident of a loan received from a non-resident shareholder"
      },
      {
       flow: "OUT", 
       code: "804", 
       section: "Loan and Miscellaneous payments", 
       description: "Repayment by a resident of a loan received from a non-resident third party"
      },
      {
       flow: "OUT", 
       code: "810", 
       section: "Loan and Miscellaneous payments", 
       description: "Loan made by a resident to a resident temporarily abroad"
      },
      {
       flow: "OUT", 
       code: "815", 
       section: "Loan and Miscellaneous payments", 
       description: "Individual loan to a non-resident"
      },
      {
       flow: "OUT", 
       code: "816", 
       section: "Loan and Miscellaneous payments", 
       description: "Study loan to a non-resident"
      },
      {
       flow: "OUT", 
       code: "817", 
       section: "Loan and Miscellaneous payments", 
       description: "Shareholders loan to a non-resident"
      },
      {
       flow: "OUT", 
       code: "818", 
       section: "Loan and Miscellaneous payments", 
       description: "Third party loan to a non-resident (excluding shareholders)"
      },
      {
       flow: "OUT", 
       code: "819", 
       section: "Loan and Miscellaneous payments", 
       description: "Trade finance to a non-resident"
      },
      {
       flow: "OUT", 
       code: "830", 
       section: "Loan and Miscellaneous payments", 
       description: "Details of payments not classified"
      },
      {
       flow: "OUT", 
       code: "831", 
       section: "Loan and Miscellaneous payments", 
       description: "Maloti collections for the credit of Vostro accounts"
      },
      {
       flow: "OUT", 
       code: "833", 
       section: "Loan and Miscellaneous payments", 
       description: "Credit/debit card company settlement as well as money remitter settlements"
      },
      {
        flow: "OUT", 
        code: "ZZ1", 
        section: "Non-Reportable Transactions", 
        description: "For Internal Bank use ONLY"
      }
     ], 
})