define(function () {
  return function (evaluationEx) {
    var assumptions;
    with (evaluationEx) {
      //TODO: maybe implement an array resStatus?

      assumptions = [
        {
          knownSide: drcr.DR,
          resStatus: resStatus.RESIDENT,
          accType: [accType.LOCAL_ACC, accType.CASH_LOCAL, accType.CASH_CURR],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.RESIDENT,
          accType: [accType.CFC],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.CFC, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.RESIDENT,
          accType: [accType.NOSTRO],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.RESIDENT,
          accType: [accType.FCA],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.HOLDCO, accType.FCA, entity.and(localBank.and(transferCURR))),
            use(resStatus.RESIDENT, accType.FCA, individual.and(localBank.and(transferCURR))),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.RESIDENT,
          accType: [accType.VOSTRO],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.RESIDENT,
          accType: [accType.VOSTRO],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.RESIDENT,
          accType: [accType.CASH_CURR, accType.CASH_LOCAL],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.IHQ,
          accType: [accType.LOCAL_ACC, accType.CASH_LOCAL, accType.CASH_CURR],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.IHQ,
          accType: [accType.CFC],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.CFC, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.IHQ,
          accType: [accType.FCA],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.HOLDCO, accType.FCA, entity.and(localBank.and(transferCURR))),
            use(resStatus.RESIDENT, accType.FCA, individual.and(localBank.and(transferCURR))),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.NONRES,
          accType: [accType.LOCAL_ACC],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.NONRES,
          accType: [accType.CFC, accType.FCA],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.NONRES,
          accType: [accType.NOSTRO],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.NONRES,
          accType: [accType.VOSTRO],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.NONRES,
          accType: [accType.CASH_LOCAL, accType.CASH_CURR],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.HOLDCO,
          accType: [accType.LOCAL_ACC],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.HOLDCO,
          accType: [accType.FCA],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.HOLDCO,
          accType: [accType.VOSTRO],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBank.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBank.and(transferCURR)),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank.and(transferLOCAL.or(transferCURR)))
          ]
        },
        {
          knownSide: drcr.CR,
          resStatus: resStatus.RESIDENT,
          accType: [accType.LOCAL_ACC, accType.FCA],
          rules: [
            use(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            use(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            use(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            use(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            use(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          knownSide: drcr.CR,
          resStatus: resStatus.RESIDENT,
          accType: [accType.CASH_LOCAL, accType.CASH_CURR],
          rules: [
            use(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            use(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            use(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            use(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          knownSide: drcr.CR,
          resStatus: resStatus.RESIDENT,
          accType: [accType.VOSTRO],
          rules: [
            use(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            use(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            use(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            use(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          knownSide: drcr.CR,
          resStatus: resStatus.NONRES,
          accType: [accType.LOCAL_ACC, accType.FCA],
          rules: [
            use(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            use(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            use(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            use(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          knownSide: drcr.CR,
          resStatus: resStatus.NONRES,
          accType: [accType.CASH_LOCAL, accType.CASH_CURR],
          rules: [
            use(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            use(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            use(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            use(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          knownSide: drcr.CR,
          resStatus: resStatus.HOLDCO,
          accType: [accType.LOCAL_ACC, accType.FCA],
          rules: [
            use(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            use(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            use(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            use(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          knownSide: drcr.CR,
          resStatus: resStatus.RESIDENT,
          accType: [accType.CFC, accType.NOSTRO],
          rules: [
            use(resStatus.NONRES, accType.FCA, localBankOther.and(transferCURR).and(field72("TRANSFER FROM ABROAD").or(field72("RETURN OF FUNDS")))),
            use(resStatus.RESIDENT, accType.CFC, localBankOther.and(transferCURR).and(not(field72("TRANSFER FROM ABROAD").or(field72("RETURN OF FUNDS"))))),
            use(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC").or(field72("TRANSFER FROM ABROAD").or(field72("RETURN OF FUNDS"))))),
            use(resStatus.RESIDENT, accType.CFC, localBankAD.and(transferCURR).and(not(field72("NTNRC").or(field72("TRANSFER FROM ABROAD").or(field72("RETURN OF FUNDS")))))),
            use(resStatus.NONRES, accType.LOCAL_ACC, localBankOther.and(transferLOCAL).and(field72("TRANSFER FROM ABROAD").or(field72("RETURN OF FUNDS")))),
            use(resStatus.RESIDENT, accType.CFC, localBankOther.and(transferLOCAL).and(not(field72("TRANSFER FROM ABROAD").or(field72("RETURN OF FUNDS"))))),
            use(resStatus.RESIDENT, accType.CFC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            use(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          knownSide: drcr.CR,
          resStatus: resStatus.NONRES,
          accType: [accType.CFC, accType.NOSTRO],
          rules: [
            use(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            use(resStatus.NONRES, accType.FCA, localBankAD.and(transferCURR).and(field72("NTNRC"))),
            use(resStatus.RESIDENT, accType.CFC, localBankAD.and(transferCURR).and(not(field72("NTNRC")))),
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            use(resStatus.NONRES, accType.VOSTRO, localBankAD.and(transferLOCAL).and(field72("NTNRB"))),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        },
        {
          knownSide: drcr.CR,
          resStatus: resStatus.NONRES,
          accType: [accType.VOSTRO],
          rules: [
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBankOther.and(transferLOCAL)),
            use(resStatus.RESIDENT, accType.FCA, localBankOther.and(transferCURR)),
            use(resStatus.RESIDENT, accType.LOCAL_ACC, localBankAD.and(transferLOCAL).and(not(field72("NTNRB")))),
            use(resStatus.RESIDENT, accType.FCA, localBankAD.and(transferCURR).and(not(field72("NTNRB")))),
            use(resStatus.NONRES, accType.VOSTRO, localBankAD.and(field72("NTNRB"))),
            use(resStatus.NONRES, accType.NOSTRO, offshoreBank)
          ]
        }
      ]
    }
    return assumptions;
  }
})