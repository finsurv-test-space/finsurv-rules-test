define(function () {
  return function (evaluationEx) {
    return {
      localCountryRegex: "[Ll][Ss]",
      localCurrencyRegex: "[Ll][Ss][Ll]",
      whoAmIRegex: "SBICLS.*",
      onshoreRegex: "....LS.*",
      onshoreADRegex: [
        "FIRNLSJJ.*", // FIRSTRAND BANK
        "NEDLLSJJ.*", // NEDBANKK
        "LESHLSJJ.*", // LESOTHO POST BANK
        "SBICLSJJ.*"],// STANDARD BANK
      CMARegex: [
        "....ZA.*",  // South Africa
        "....SZ.*",  // Swaziland
        "....NA.*"], // Namibia
      ZZ1Reportability : false
    };
  }
})
