define(function () {
  return function (predef) {
    var stdImportExport;
    with (predef) {
      stdImportExport = {
        ruleset: "CBL Import/Export Rules",
        scope: "importexport",
        validations: [
          {
            field: "",
            rules: [
              // field: "ImportControlNumber",
              ignore('ieicn1'),
              message('ieicn2', '407', 'For any category other than 10101 to 10111 or 10301 to 10311 or 10500 or 10600, the ImportControlNumber must not be completed.'),
              ignore('ieicn3'),
              ignore('ieicn4'),
              ignore('ieicn5'),
              ignore('ieicn6'),
              // field: "TransportDocumentNumber",
              ignore('ietdn1'),
              message('ietdn2', '413', 'For any category other than 10301 to 10311 or 10500 or 10600, the TransportDocumentNumber must not be completed.'),
              ignore('ietdn3'),
              ignore('ietdn4'),
              // field: "UCR",
              ignore('ieucr1'),
              ignore('ieucr2'),
              ignore('ieucr3'),
              message('ieucr4', '416', null),
              // field: "PaymentCurrencyCode",
              message('iepcc1', '435', null),
              message('iepcc2', '318', null),
              message('iepcc2', '436', 'PaymentCurrencyCode of all SubSequence is not DomesticCurrencyCode or does not match ForeignCurrencyCode under MonetaryDetails element. (NOTE: CurrencyCode must be consistent in respect of all the SubSequences i.e. may not be a USD in SubSequence 1 and a DomesticCurrencyCode in SubSequrence 2.)'),
              // field: "PaymentValue",
              message('iepv1', '417', null),
              // field: "MRNNotOnIVS",
              message('iemrn1', '203', 'The value must only be Y or N'),
            ]
          },
          //================================================================================================================================================
          // CoreCBL specific import/export
          //================================================================================================================================================
          {
            field: "ImportControlNumber",
            rules: [
              failure("ls_ieicn1", 406, "Must be completed if the Flow is OUT and the BoPCategory is 10101 to 10111 or 10301 to 10311 or 10500 or 10600.",
                isEmpty).onOutflow().onCategory(['101', '103', '105', '106']).notOnCategory(['101/12', '101/13', '101/14', '101/15', '101/16', '101/17', '101/18', '101/19', '101/20', '101/21', '101/22', '101/23', '103/12', '103/13', '103/14', '103/15', '103/16', '103/17', '103/18', '103/19', '103/20', '103/21', '103/22', '103/23']).onSection("A"),
              failure("ls_ieicn3", 408, "If the Flow is OUT and the BoPCategory is 10101 to 10111, the first 3 characters must be INV.",
                notPattern(/^INV.+$/)).onOutflow().onSection("ABG").onCategory("101"),
              failure("ls_ieicn4", 409, "If the Flow is OUT and the BoPCategory is 10301 to 10311 or 10500 or 10600, the first 3 characters must be a valid customs office code.",
                notValidICNCustomsOffice).onOutflow().onSection("A").onCategory(["103", "105", "106"])
            ]
          },
          {
            field: "TransportDocumentNumber",
            rules: [
              failure("ls_ietdn1", 412, "Must be completed if the Flow is OUT and the BoPCategory is 10301 to 10311 or 10500 or 10600.",
                isEmpty).onOutflow().onCategory(['103','105','106']).notOnCategory(['103/12', '103/13', '103/14', '103/15', '103/16', '103/17', '103/18', '103/19', '103/20', '103/21', '103/22', '103/23']).onSection("A"),
            ]
          },
          {
            field: "UCR",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("ls_ieucr1", 414, "If UCR contains a value and the Flow is IN, the minimum characters is 12 but not exceeding 35 characters and is in the following format: nLS12345678a...35a where n = last digit of the year LS = Fixed character 12345678 = Valid Customs Client Number and a = unique alpha numeric consignment number.",
                notEmpty.and(isTooShort(12).or(isTooLong(35).or(notPattern(/^\dLS\d{8,13}[a-zA-Z0-9]{1,24}$/))))).onInflow().onSection("A"),
              failure("ls_ieucr2", 415, "Must be completed if the Flow is IN and the BoPCategory is 10301 to 10311 or 10500 or 10600",
                isEmpty).onSection("A").onInflow().onCategory(['103', '105', '106']),
                // For any BoPCategory other than 10101 to 10111 or 10301 to 10311 or 10500 or 10600, the UCR must not be completed. 
              failure("ls_ieucr3", 416, "For any BoPCategory other than 10301 to 10311 or 10500 or 10600, the UCR must not be completed.",
                notEmpty).onSection("A").onInflow().notOnCategory(["103", "105", "106"]),
            ]
          },
        ]
      };
    }

    return stdImportExport;
  }
});
