define(function () {
  return function (predef) {
    var feature;
    with (predef) {

      feature = {
        ruleset: "Specific CBL Transaction rules",
        scope: "transaction",
        validations: [
          {
            field: "",
            rules: [
              //================================================================================================================================================
              // TRANSACTION
              //================================================================================================================================================
              // field: "ReplacementTransaction",
              message('repyn1', '440', null),
              message('repyn2', '208', null),
              // field: "ReplacementOriginalReference",
              message('repor1', '209', null),
              message('repor2', '210', null),
              ignore('repor3'),
              // field: "ReportingQualifier",
              message('repq1', '438', null),
              message('repq2', '206', null),
              // field: "Flow",
              message('flow1', '439', null),
              message('flow2', '207', null), //NOTE: Does not include section F
              ignore('flow3'),
              // field: "ValueDate",
              message('vd1', '441', null),
              message('vd2', '214', null), //NOTE: Does not exceed today date (Missing +10 days)
              ignore('vd3'),
              ignore('vd4'),
              message('vd5', '213', 'Date format incorrect. Format is CCYY-MM-DD'),
              // field: "FlowCurrency",
              message('fcurr2', '318', null),
              // field: "TotalForeignValue",
              message('tfv1','231', null),
              message('tfv2','231', null),
              message('tfv3','445', 'ForeignCardHoldersPurchasesDomesticValue + ForeignCardHoldersCashWithdrawalsDomesticValue must equal TotalValue'),
              message('tfv4','232', 'Must be completed and must be greater than 0.00'),
              // featureChecksum: field : "TotalValue",
              message('totv1', '231', 'DomesticValue + ForeignValue must equal TotalValue (Only values under the MonetaryDetail element. This is the total transaction value calculated with the ForeignVale and Domestic value added together.)'),
              // field: "TrnReference",
              message('tref1', '442', null),
              ignore('tref2'),
              // field: "OriginatingBank"
              ignore('obank1'),
              message('obank2','227', "Must not be completed"),
              ignore('obank3'),
              ignore('obank4'),
              ignore('obank5'),
              ignore('obank6'),
              ignore('obank7'),
              ignore('obank8'),
              ignore('obank9'),
              // field: "OriginatingCountry",
              ignore('ocntry1'),
              message('ocntry2','227', 'Must not be completed'),
              ignore('ocntry3'),
              ignore('ocntry4'),
              message('ocntry5','229', null),
              ignore('ocntry6'),
              ignore('ocntry7'),
              // field: "CorrespondentBank",
              ignore('cbank1'),
              message('cbank2','228', "Must not be completed"),
              // field: "CorrespondentCountry",
              ignore('ccntry1'),
              message('ccntry2','228', null),
              message('ccntry3','229', null),
              // field: "ReceivingBank",
              ignore('rbank1'),
              message('rbank2','230', null),
              ignore('rbank3'),
              ignore('rbank4'),
              ignore('rbank5'),
              ignore('rbank6'),
              ignore('rbank7'),
              ignore('rbank8'),
              ignore('rbank9'),
              // field: "ReceivingCountry",
              ignore('rcntry1'),
              message('rcntry2','230', null),
              message('rcntry3','229', null),
              ignore('rcntry4'),
              ignore('rcntry5'),
              ignore('rcntry6'),
              ignore('rcntry7'),
              // field: "NonResident",
              message('nr1', '233', null),
              ignore('nr2'),
              ignore('nr3'),
              message('nr5','235', null),
              message('nr6','238', 'If BoPCategory 25000 or 25100 is used, Non Resident Entity, or NonResident Exception element must not be completed'),
              // field: "NonResident.Individual.Surname",
              message('nrsn1', '239', null),
              message('nrsn2', '240', 'The words specified under Non- resident or Resident ExceptionName, must not be used.'),
              ignore('nrsn3'),
              ignore('nrsn4'),
              // field: "NonResident.Individual.Name",
              message('nrnm1', '241', null),
              ignore('nrnm2'),
              ignore('nrnm3'),
              message('nrnm4', '240', null),
              // field: "NonResident.Individual.Gender",
              message('nrgn1', '271', null),
              // field: "NonResident.Individual.PassportNumber",
              message('nrpn1','243', 'If BoPCategory 25000 or 25100 is completed, must be completed'),
              ignore('nrpn2'),
              // field: "NonResident.Individual.PassportCountry",
              message('nrpc1','244', 'If BoPCategory 25000 or 25100 is completed, must be completed'),
              message('nrpc2', '229', null),
              // field: "NonResident.Entity.EntityName",
              message('nrlen1', '245', null),
              message('nrlen2', '451', 'CardMerchantName must be completed'),
              message('nrlen3', '240', 'The words specified under Non- resident or Resident ExceptionName must not be used.'),
              ignore('nrlen4'),
              message('nrlen5', '451', 'CardMerchantName must be completed'),
              // field: "NonResident.Entity.CardMerchantName",
              message('nrcmn1', '451', 'CardMerchantName must be completed'),
              message('nrcmn2', '246', null),
              ignore('nrcmn3'),
              message('nrcmn4','451', 'CardMerchantName must be completed'),
              // field: "NonResident.Entity.CardMerchantCode",
              message('nrcmc1','247', null),
              message('nrcmc2','248', null),
              message('nrcmc3','249', null),
              // field: "NonResident.Exception",
              message('nrex1', '251', null),
              message('nrex2', '252', null),
              // field: "NonResident.Exception.ExceptionName",
              message('nrexn1', '250', null),
              message('nrexn2','253', 'If the ExceptionName is MUTUAL PARTY, the BoPCategory must only be 20000, 25200, 25500, 25600 or 53005'),
              message('nrexn3','253', 'If BoPCategory is 25200 and the Flow is IN, must only contain the value MUTUAL PARTY under NonResident ExceptionName'),
              message('nrexn4','253', 'BoPCategory or SubBoPCategory must only be 30000 or 30908'),
              message('nrexn5','253', 'BoPCategory must only be 40000 or 41102'),
              message('nrexn6','253', 'BoPCategory must only be 20000 or 27500'),
              message('nrexn7','253', 'BoPCategory must only be 40000 or 40700'),
              ignore('nrexn9'),
              message('nrexn10','252', null),
              message('nrexn11','252', null),
              message('nrexn12','252', null),
              ignore('nrexn13'),
              message('nrexn14','252', null),
              ignore('nrexn15'),
              ignore('nrexn16'),
              ignore('nrexn17'),
              ignore('nrexn18'),
              ignore('nrexn19'),
              ignore('nrexn20'),
              ignore('nrexn21'),
              ignore('nrexn22'),
              ignore('nrexn23'),
              ignore('nrexn24'),
              ignore('nrexn25'),
              message('nrexn26', '253', "If the MoneyTransferAgentIndicator is TRAVEL CARD or TRAVELLERS CHEQUE, the category can only be 25200, 25500, 25600 or 53005"),
              ignore('nrexn27'),
              ignore('nrexn28'),
              // field: "NonResident.Exception.AccountIdentifier",
              ignore('nrexai'),
              // field: "NonResident.Exception.AccountNumber",
              ignore('nrexan'),
              // field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
              ignore('nriaid1'),
              ignore('nriaid2'),
              ignore('nriaid3'),
              ignore('nriaid4'),
              message('nriaid5','256', 'If the AccountIdentifier is RES FOREIGN BANK ACCOUNT and the Flow is IN and the BoPCategory is 25500 or 25600 or 81000 or 41600, the non resident Individual element must be completed.'),
              message('nriaid6','256', 'If the AccountIdentifier is RES FOREIGN BANK ACCOUNT and the Flow is OUT and the BoPCategory is 25500 or 25600 or 81000, the non resident Individual element must be completed.'),
              ignore('nriaid9'),
              ignore('nriaid10'),
              ignore('nriaid12'),
              ignore('nriaid13'),
              ignore('nriaid14'),
              // field: ["NonResident.Individual.AccountNumber", "NonResident.Entity.AccountNumber"],
              ignore('nrian1'),
              message('nrian2', '259', null),
              message('nrian3','437', null),
              message('nrian4','237', null),              
              // field: ["NonResident.Individual.Address.AddressLine1", "NonResident.Entity.Address.AddressLine1"],
              ignore('nrial11'),
              // field: ["NonResident.Individual.Address.AddressLine2", "NonResident.Entity.Address.AddressLine2"],
              ignore('nrial21'),
              // field: ["NonResident.Individual.Address.Suburb", "NonResident.Entity.Address.Suburb"],
              ignore('nrial31'),
              // field: ["NonResident.Individual.Address.City", "NonResident.Entity.Address.City"],
              ignore('nric2'),
              // field: ["NonResident.Individual.Address.State", "NonResident.Entity.Address.State"],
              ignore('nris2'),
              // field: ["NonResident.Individual.Address.PostalCode", "NonResident.Entity.Address.PostalCode"],
              ignore('nriz1'),
              ignore('nriz2'),
              ignore('nriz3'),

              // field: ["NonResident.Individual.Address.Country", "NonResident.Entity.Address.Country"],
              message('nrictry1', '266', null),
              message('nrictry2', '229', null),
              ignore('nrictry3'),
              ignore('nrictry4'),
              ignore('nrictry5'),
              ignore('nrictry6'),
              ignore('nrictry7'),
              ignore('nrictry8'),
              message('nrictry9', '215', null),
              // field: "Resident",
              message('rg1', '257', null),
              message('rg2','268', "If BoPCategory 25500 is used and the Flow is OUT, Resident EntityCustomer element must be completed"),
              message('rg5', '444', null),
              // field: "Resident.Individual.Surname",
              message('risn1', '239', null),
              message('risn2', '240', 'The words specified under Non- resident or Resident ExceptionName must not be used.'),
              ignore('risn3'),
              ignore('risn4'),
              // field: "Resident.Individual.Name",
              message('rin1', '241', null),
              ignore('rin2'),
              ignore('rin3'),
              ignore('rin4'),
              // field: "Resident.Individual.Gender",
              message('rig1', '242', null),
              message('rig2', '271', null),
              ignore('rig3'),
              // field: "Resident.Individual.DateOfBirth",
              message('ridob1', '272', null),
              message('ridob2', '213', null),
              ignore('ridob3'),
              // field: "Resident.Individual.IDNumber",
              ignore('riidn1'),
              ignore('riidn2'),
              ignore('riidn3'),
              ignore('riidn4'),
              ignore('riidn5'),
              // field: "Resident.Individual.TempResPermitNumber",
              ignore('ritrpn1'),
              ignore('ritrpn2'),
              ignore('ritrpn3'),
              ignore('ritrpn4'),
              // field: "Resident.Individual.ForeignIDNumber",
              ignore('rifidn1'),
              ignore('rifidn2'),
              ignore('rifidn3'),
              ignore('rifidn4'),
              ignore('rifidn5'),
              // field: "Resident.Individual.ForeignIDCountry",
              message('rifidc1', '274', null).onSection('AE'),
              message('rifidc2', '229', null),
              message('rifidc3', '341', 'SWIFT country code must not be LS'),
              // field: "Resident.Individual.PassportNumber",
              message('ripn1','275', 'If BoPCategory 25500 is used and the Flow is OUT, must not be completed (IndividualThirdPartyPassportNumber must be completed)'),
              ignore('ripn2'),
              message('ripn3','276', 'If BoPCategory 25600 is used and the PassportNumber is not completed, the IndividualThirdPartyPassportNumber must be completed.'),
              // field: "Resident.Individual.PassportCountry",
              message('ripc1', '244', null),
              message('ripc2', '229', null),
              message('ripc3','276', 'If BoPCategory 25600 is used and the PassportCountry is not completed, the IndividualThirdPartyPassportCountry must be completed.'),
              // field: ["Resident.Individual.BeneficiaryID1", "Resident.Individual.BeneficiaryID2", "Resident.Individual.BeneficiaryID3", "Resident.Individual.BeneficiaryID4"],
              ignore('ribenid'),
              // field: "Resident.Entity.EntityName",
              ignore('relen1'),
              message('relen2', '277', null),
              ignore('relen3'),
              ignore('relen4'),
              ignore('relen5'),
              ignore('relen6'),
              ignore('relen7'),
              // field: "Resident.Entity.TradingName",
              ignore('retn1'),
              // field: "Resident.Entity.RegistrationNumber",
              message('rern1', '280', null),
              ignore('rern2'),
              ignore('rern3'),
              ignore('rern4'),
              // field: "Resident.Entity.InstitutionalSector",
              message('reis1', '281', null),
              message('reis2', '424', null),
              message('reis3', '282', null),
              // field: "Resident.Entity.IndustrialClassification",
              message('reic1', '283', null),
              message('reic2', '425', null),
              message('reic3', '284', null),
              // field: "Resident.Exception",
              message('re1', '286', null),
              // field: "Resident.Exception.ExceptionName",
              message('ren1', '250', 'Must only contain one of the specified values'),
              message('ren2','287', 'If the ExceptionName is MUTUAL PARTY, the BoPCategory must only be 20000, 25000 or 25100'),
              message('ren3','287', 'For any BoPCategory other than 20000, 25000, 25100 the value MUTUAL PARTY must not be completed.'),
              message('ren4','286', null),
              message('ren5','287', 'BoPCategory must only be 40000 or 40700'),
              message('ren6', '287', 'BoPCategory must only be 10000, 20000, 30000, 40000, 50000, 60000, 70000 or 80000'),
              ignore('ren7'),
              ignore('ren8'),
              ignore('ren9'),
              message('ren10','287', 'BoPCategory must only be 30908 or 30000'),
              message('ren11','287', 'BoPCategory must only be 30100 or 30000'),
              message('ren12','287', 'BoPCategory must only be 27500 or 20000'),
              ignore('ren13'),
              message('ren15','286', null),
              ignore('ren16'),
              ignore('ren17'),
              ignore('ren18'),
              message('ren19','286', null),
              message('ren20','286', null),
              message('ren21','286', null),
              ignore('ren22'),
              ignore('ren23'),
              ignore('ren24'),
              ignore('ren25'),
              ignore('ren26'),
              ignore('ren27'),
              ignore('ren28'),
              // field: "Resident.Exception.Country",
              message('rec1', '318', 'Must be completed'),
              ignore('rec2'),
              ignore('rec3'),
              message('rec4', '285', 'Must not be completed'),
              // field: "Resident.Exception.AccountIdentifier",
              ignore('rexai'),
              // field: "Resident.Exception.AccountNumber",
              ignore('rexan'),
              // field: ["Resident.Individual", "Resident.Entity"],
              message('g1', '301', null),
              // field: ["Resident.Individual.AccountName", "Resident.Entity.AccountName"],
              message('an1', '289', null),
              // field: ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
              ignore('accid1'),
              ignore('accid2'),
              message('accid3','426', null),
              message('accid4','255', null),
              ignore('accid5'),
              message('accid6','314', null),              
              ignore('accid7'),
              // field: "Resident.Entity.AccountIdentifier",
              ignore('eaccid1'),
              // field: ["Resident.Individual.AccountNumber", "Resident.Entity.AccountNumber"],
              message('accno1', '258', null),
              message('accno2', '427', null),
              message('accno3', '259', null),
              ignore('accno4'),
              // field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],
              ignore('ccn1'),
              ignore('ccn2'),
              ignore('ccn3'),
              ignore('ccn4'),
              ignore('ccn5'),
              ignore('ccn6'),
              ignore('ccn7'),
              ignore('ccn8'),
              ignore('ccn9'),
              ignore('ccn10'),
              ignore('ccn11'),
              ignore('ccn12'),
              // field: "Resident.Individual.TaxNumber",
              ignore('tni1'),
              message('tni2', '428', null),
              // field: "Resident.Entity.TaxNumber",
              ignore('tne1'),
              message('tne2', '428', null),
              // field: ["Resident.Individual.TaxNumber", "Resident.Entity.TaxNumber"],
              ignore('tn1'),
              // field: "Resident.Individual.VATNumber",
              message('vni2', '429', null),
              // field: "Resident.Entity.VATNumber",
              ignore('vne1'),
              message('vne2','429', null),
              // field: ["Resident.Individual.VATNumber", "Resident.Entity.VATNumber"],
              ignore('vn1'),
              // field: ["Resident.Individual.TaxClearanceCertificateIndicator", "Resident.Entity.TaxClearanceCertificateIndicator"],
              ignore('tcci1'),
              ignore('tcci2'),
              // field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
              ignore('tccr1'),
              ignore('tccr2'),
              // field: ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1", "Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1"],
              ignore('a1_1'),
              message('a1_2','431', null),              
              ignore('a1_3'),
              ignore('a1_4'),
              // field: ["Resident.Individual.StreetAddress.AddressLine2", "Resident.Entity.StreetAddress.AddressLine2", "Resident.Individual.PostalAddress.AddressLine2", "Resident.Entity.PostalAddress.AddressLine2"],
              message('a2_1', '432', null),
              // field: ["Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb", "Resident.Individual.PostalAddress.Suburb", "Resident.Entity.PostalAddress.Suburb"],
              ignore('s1'),
              ignore('s2'),
              ignore('s3'),
              ignore('s4'),
              // field: ["Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City", "Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City"],
              ignore('c1'),
              ignore('c2'),
              ignore('c3'),
              ignore('c4'),
              // field: ["Resident.Individual.StreetAddress.PostalCode", "Resident.Entity.StreetAddress.PostalCode"],
              ignore('spc1'),
              message('spc2', '432', null),
              ignore('spc3'),
              ignore('spc4'),
              // field: ["Resident.Individual.PostalAddress.PostalCode", "Resident.Entity.PostalAddress.PostalCode"],
              ignore('pc1'),
              ignore('pc2'),
              message('pc3', '432', null),
              ignore('pc4'),
              ignore('pc5'),
              ignore('pc6'),
              ignore('pc7'),
              // field: ["Resident.Individual.ContactDetails.ContactSurname", "Resident.Entity.ContactDetails.ContactSurname", "Resident.Individual.ContactDetails.ContactName", "Resident.Entity.ContactDetails.ContactName"],
              message('cn1','300', 'Must be completed if Resident EntityName is used').onSection("AE"),
              message('cn2', '433', null),
              // field: ["Resident.Individual.ContactDetails.Email", "Resident.Entity.ContactDetails.Email"],
              message('cnte1', '301', null),
              message('cnte2', '433', null),
              // field: ["Resident.Individual.ContactDetails.Fax", "Resident.Entity.ContactDetails.Fax", "Resident.Individual.ContactDetails.Telephone", "Resident.Entity.ContactDetails.Telephone"],
              message('cntft1', '301', null),
              message('cntft2', '433', null),
              // message('cntft3', '302', null),
              ignore('cntft3'),
              // field: ["Resident.Individual.CardNumber", "Resident.Entity.CardNumber"],
              message('crd1', '303', null),
              message('crd2', '305', null),
              // field: ["Resident.Individual.SupplementaryCardIndicator", "Resident.Entity.SupplementaryCardIndicator"],
              message('crdi1', '306', 'Must be blank or contain a value "Y" or "N"'),
              message('crdi2', '307', null),
              // field: "MonetaryAmount",
              message('tma1', '308', null),
              message('tma2', '309', 'Must contain a sequential number that must start with the value 1'),
            ]
          },
          //================================================================================================================================================
          // CoreCBL Specific Transaction
          //================================================================================================================================================
          {
              field: ["Resident.Individual.ContactDetails.Fax",
                "Resident.Entity.ContactDetails.Fax",
                "Resident.Individual.ContactDetails.Telephone",
                "Resident.Entity.ContactDetails.Telephone"],
              minLen: 2,
              maxLen: 15,
              rules: [
                failure('ls_cntft3', 302, 'Must be in a 2 to 15 digit format',
                notEmpty.and(notPattern(/^\d{2,15}$/))).onSection("AE")
              ]
          },
          {
            field: "ValueDate",
            rules: [
              failure("ls_vd4", 216, "ValueDate must be equal to or after the 2020-05-10",
                notEmpty.and(isBeforeGoLive)),
            ]
          },
          {
            field: "Resident.Entity.TradingName",
            minLen: 2,
            maxLen: 70,
            rules: [
              failure("ls_retn1", 278, "If Resident EntityCustomer is used, trading name must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("AEC")
            ]
          },
          {
            field: ["NonResident.Individual.Address.Country", "NonResident.Entity.Address.Country"],
            len: 2,
            rules: [
            failure("ls_nrictry3", 267, "SWIFT country code may not be {{Locale}}",
              notEmpty.and(hasValue(map("Locale")))).onSection("ACE"),
            ]
          },
          {
            field: ["NonResident.Individual.Address.AddressLine1", "NonResident.Entity.Address.AddressLine1"],
            minLen: 2,
            maxLen: 50,
            rules: [
              failure("ls_nrial11", 260, "Must not be completed", notEmpty).onSection("EF")
            ]
          },
          {
            field: ["NonResident.Individual.Address.AddressLine2", "NonResident.Entity.Address.AddressLine2"],
            minLen: 2,
            maxLen: 50,
            rules: [
              failure("ls_nrial21", 260, "Must not be completed", notEmpty).onSection("EF")
            ]
          },
          {
            field: ["NonResident.Individual.Address.Suburb", "NonResident.Entity.Address.Suburb"],
            minLen: 2,
            maxLen: 50,
            rules: [
              failure("ls_nrial31", 260, "Must not be completed", notEmpty).onSection("EF")
            ]
          },
          {
            field: ["NonResident.Individual.Address.City", "NonResident.Entity.Address.City"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("ls_nric2", 262, "Must not be completed", notEmpty).onSection("EF")
            ]
          },
          {
            field: ["NonResident.Individual.Address.State", "NonResident.Entity.Address.State"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure("ls_nris2", 264, "Must not be completed",
                notEmpty).onSection("EF")
            ]
          },
          {
            field: ["NonResident.Individual.Address.PostalCode", "NonResident.Entity.Address.PostalCode"],
            minLen: 2,
            maxLen: 10,
            rules: [
              failure("ls_nriz2", 265, "Must not be completed",
                notEmpty).onSection("EF"),
            ]
          },
          {
            field: "NonResident",
            rules: [
              failure('ls_nr1', 234, "Non Resdent Entity Element must be completed",
              isMissingField(["Entity"])).onSection("E")
            ]
          },
          {
            field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
            rules: [
              failure("ls_nriaid1", 255, "If the Flow is OUT must contain a value of NON RESIDENT OTHER or NON RESIDENT DOMESTIC CURRENCY or NON RESIDENT FCA or CASH or FCA RESIDENT or RES FOREIGN BANK ACCOUNT or VOSTRO",
                notValueIn(["NON RESIDENT OTHER", "NON RESIDENT DOMESTIC CURRENCY", "NON RESIDENT FCA", "CASH", "FCA RESIDENT", "RES FOREIGN BANK ACCOUNT", "VOSTRO"])).onOutflow().onSection("AC")
            ]
          },
          {
            field: "Resident.Individual.IDNumber",
            rules: [
              // failure("ls_riidn3", "SCN", "This value must be a number",
              //   notEmpty.and(notPattern(/^\d*$/)))
              // Passport Number: ^[A-Z]{2}\d{6}$
              // Identification Number: ^\d{12}$
              failure("ls_riidn3", "263", "Invalid ID number format. Must be one of the following: (1) Passport Number - AAnnnnnn OR (2) National Identification Number - nnnnnnnnnnnn. Where n is a number from 0 to 9.",
                notEmpty.and(notPattern(/^[A-Z]{2}\d{6}$/)).and(notPattern(/^\d{12}$/))),
              
              failure("ls_riidn4", 270, "If IndividualCustomer is selected, at least one of IDNumber or TempResPermitNumber or ForeignIDNumber or PassportNumber must be completed",
                isEmpty.and(
                  notTransactionField("Resident.Individual.TempResPermitNumber")
                  .and(notTransactionField("Resident.Individual.ForeignIDNumber"))
                  .and(notTransactionField("Resident.Individual.PassportNumber"))
              )).onSection("AE"),
            ]
          },
          {
            field: "Resident.Individual.TempResPermitNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("ls_ritrpn3", 270, "If IndividualCustomer is selected, at least one of IDNumber or TempResPermitNumber or ForeignIDNumber or PassportNumber must be completed",
                isEmpty.and(
                  notTransactionField("Resident.Individual.IDNumber")
                  .and(notTransactionField("Resident.Individual.ForeignIDNumber"))
                  .and(notTransactionField("Resident.Individual.PassportNumber"))
                )).onSection("AE")
            ]
          },
          {
            field: "Resident.Individual.ForeignIDNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("ls_rifidn3", 270, "If IndividualCustomer is selected, at least one of IDNumber or TempResPermitNumber or ForeignIDNumber or PassportNumber must be completed",
                isEmpty.and(
                  notTransactionField("Resident.Individual.IDNumber")
                  .and(notTransactionField("Resident.Individual.TempResPermitNumber"))
                  .and(notTransactionField("Resident.Individual.PassportNumber"))
                )).onSection("AE"),
            ]
          },
          {
            field: "Resident.Individual.PassportNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("ls_ripn4", 270, "If IndividualCustomer is selected, at least one of IDNumber or TempResPermitNumber or ForeignIDNumber or PassportNumber must be completed",
                isEmpty.and(
                  notTransactionField("Resident.Individual.IDNumber")
                  .and(notTransactionField("Resident.Individual.TempResPermitNumber"))
                  .and(notTransactionField("Resident.Individual.ForeignIDNumber"))
                )).onSection("AE"),
            ]
          },
          {
            field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],
            minLen: 2,
            maxLen: 15,
            rules: [
              failure('ls_ccn_1_and_2', 290, 'Must be completed if BoPCategory is 10101 to 10111 or 10301 to 10311 or 10500 or 10600',
                isEmpty).onSection("A").onCategory(['101', '103', '105', '106']),
              failure("ls_ccn3", 292, "If the FLOW is IN and the BoPCategory is 10101 to 10111 or 10301 to 10311 or 10500 or 10600, the CustomsClientNumber must contain a valid Customs client number.",
                notEmpty.and(notPattern(/^\d{2,15}$/))).onInflow().onCategory(['101','103','105','106']).onSection("A"),
              failure("ls_ccn4", 292, "If the FLOW is OUT and BoPCategory is 10101 to 10111 or 10301 to 10311 or 10500 or 10600, the CustomsClientNumber must contain a valid Customs Client Number",
                notEmpty.and(notPattern(/^\d{2,15}$/))).onOutflow().onCategory(['101','103','105','106']).onSection("A"),
              failure('ls_ccn5', 291, 'Must not be completed',
                notEmpty).onSection("EF"),
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province", 
            "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('ls_p1', 299, 'Must be a valid region',
                notEmpty.and(notValidProvince)).onSection("AE"),
              failure('ls_p2', 299, 'Region or district must be completed',
                isEmpty).onSection("AE")
            ]
          },
          {
            field: "Resident.Entity.EntityName",
            rules: [
              failure("ls_relen1", 278, "If Resident EntityCustomer is used, must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("AEC"),
              failure("ls_relen6", 240, "The words specified under Non-resident or Resident ExceptionName, must not be used.",
                notEmpty.and(isExceptionName)).onSection("ACE"),
            ]
          },
          {
            field: ["Resident.Individual.TaxClearanceCertificateIndicator", "Resident.Entity.TaxClearanceCertificateIndicator"],
            len: 1,
            rules: [
              //TODO: If value is less than M100 000
              failure('ls_tcci2', 430, 'Must not be completed',
                notEmpty).onSection("ACEF")
            ]
          },
          {
            field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
            minLen: 2,
            maxLen: 30,
            rules: [
              //TODO: If value is less than M100 000
              failure('ls_tccr2', 431, 'Must not be completed', notEmpty).onSection("ACEF")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1"],
            minLen: 2,
            maxLen: 70,
            rules: [
              failure('ls_a1_1', 295, 'Physical address must be completed', isEmpty).onSection("AE"),
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('ls_s1', 296, 'Physical StreetSuburb must be completed', isEmpty).onSection("AE"),
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('ls_c1', 297, 'Physical city or town must be completed', isEmpty).onSection("AE"),
            ]
          },
          {
            field: ["Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1"],
            minLen: 2,
            maxLen: 70,
            rules: [
              failure('ls_a1_3', 295, 'Must be completed', isEmpty).onSection("AE"),
            ]
          },
          {
            field: ["Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('ls_c2', 297, 'Must be completed', isEmpty).onSection("AE"),
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1",
              "Resident.Individual.StreetAddress.AddressLine2", "Resident.Entity.StreetAddress.AddressLine2",
              "Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb",
              "Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City",
              "Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province",
              "Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1",
              "Resident.Individual.PostalAddress.AddressLine2", "Resident.Entity.PostalAddress.AddressLine2",
              "Resident.Individual.PostalAddress.Suburb", "Resident.Entity.PostalAddress.Suburb",
              "Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City",
              "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
            rules: [
              failure('ls_s3', 432, 'Must not be completed', notEmpty).onSection("F")
            ]
          },
          {
            field: ["Resident.Individual.AccountName", "Resident.Entity.AccountName"],
            minLen: 2,
            maxLen: 70,
            rules: [
              // Do not remove this or an1 - needed for rule completion
              failure("ls_an2", 289, "Must be completed except if the AccountIdentifier is CASH or EFT or CARD PAYMENT",
                notEmpty.and(hasResidentFieldValue("AccountIdentifier", ["CASH", "EFT", "CARD PAYMENT"]))).onSection("AE")
            ]
          },
          {
            field: ["Resident.Individual.AccountNumber", "Resident.Entity.AccountNumber"],
            minLen: 2,
            maxLen: 40,
            rules: [
              failure('ls_accno4', 259, "Must be completed if the Flow is OUT and the AccountIdentifier under AdditionalCustomerData is not CASH or EFT or CARD PAYMENT",
                isEmpty.and(notResidentFieldValue("AccountIdentifier", ["CASH", "EFT", "CARD PAYMENT"]))).onOutflow().onSection("AC")
            ]
          },
          {
            field: "ReplacementOriginalReference",
            rules: [
              failure("ls_repor3", 217, "Additional spaces identified in data content",
                notEmpty.and(hasSpaces)).onSection("AEF")
            ]
          },
          {
            field: "NonResident.Individual.PassportNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure("ls_nrpn2", 217, "Additional spaces identified in data content",
                notEmpty.and(hasSpaces)).onSection("A")
            ]
          },
          {
            field: "Resident.Entity.RegistrationNumber",
            minLen: 2,
            maxLen: 30,
            rules: [
              failure("ls_rern4", 217, "Additional spaces identified in data content",
                notEmpty.and(hasSpaces)).onSection("AE")
            ]
          },
          {
            field: "TrnReference",
            minLen: 1,
            maxLen: 30,
            rules: [
              failure("ls_tref2", 217, "Additional spaces identified in data content",
                notEmpty.and(hasSpaces))
            ]
          },
          {
            field: ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
            rules: [
              failure("ls_accid1", 255, "Must contain a value of RESIDENT OTHER or CFC RESIDENT or FCA RESIDENT or CASH or EFT or CARD PAYMENT or DEBIT CARD or CREDIT CARD",
              notValueIn(["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "EFT", "CARD PAYMENT","DEBIT CARD","CREDIT CARD"])).onSection("A"),
              failure("ls_accid5", 255, "Invalid AccountIdentifier",
              notValueIn(["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "EFT", "CARD PAYMENT","DEBIT CARD","CREDIT CARD"])).onSection("AE"),
            ]
          },
          {
            field: ["NonResident.Individual.AccountNumber", "NonResident.Entity.AccountNumber"],
            minLen: 2,
            maxLen: 40,
            rules: [
              failure("ls_nrian1", 258, "If the Flow is OUT must be completed if the AccountIdentifier is not CASH or NON RESIDENT RAND",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["CASH", "NON RESIDENT DOMESTIC CURRENCY"]))).onOutflow().onSection("ABCDG"),
            ]
          },
        ]
      };
    }
    return feature;
  }
});
