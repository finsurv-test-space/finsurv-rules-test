define({
  engine: { major: "1", minor: "0" },
  dependsOn: "coreSADC",
  features: ["featureBranchHub", "featureHOLDCO"],
  mappings: {
    LocalCurrencySymbol: "L",
    LocalCurrencyName: "Loti",
    LocalCurrency: "LSL",
    Locale: "LS",
    LocalValue: "DomesticValue",
    Regulator: "CB",
    DealerPrefix: "RE",
    RegulatorPrefix: "CB",
    StateName: "District",
    _minLenErrorType: "ERROR", // SUCCESS, ERROR, WARNING
    _maxLenErrorType: "ERROR",
    _lenErrorType: "ERROR"
  }
});