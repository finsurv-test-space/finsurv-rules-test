define(function() {
  return function(testBase) {
      with (testBase) {

        var test_cases = [

//================================================================================================================================================
//  # LENGTH CHECKS (1/6)
//================================================================================================================================================

      // field: "ReportingQualifier",
          assertSuccess("ReportingQualifier.minLen", {ReportingQualifier: 'BOPCUS'}),
          assertSuccess("ReportingQualifier.maxLen", {ReportingQualifier: 'BOPCUS'}),
          assertFailure("ReportingQualifier.minLen", {ReportingQualifier: 'BOPCU'}),
          assertFailure("ReportingQualifier.maxLen", {ReportingQualifier: '12345678901234567890123456'}),
           
      // field: "ImportControlNumber",
           assertSuccess("ImportControlNumber.maxLen", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: 'PEZ2010302281234567'}]}]
          }),
          assertSuccess("ImportControlNumber.maxLen", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: 'WOR201302281234567'}]}]
          }),
          assertFailure("ImportControlNumber.maxLen", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: '111111111111111111111111111111111111'}]}] //Too long
          }),
          assertNoRule("ImportControlNumber.maxLen", {
            ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: 'XXX301302301234567'}]}]
          }),

//================================================================================================================================================
//  # TRANSACTION (2/6)
//================================================================================================================================================
      // field: "ValueDate",
        // ValueDate must be equal to or after the 2020-05-10"
        // NOTE: Using testBase GoLive date for test
        assertSuccess("ls_vd4", {ValueDate: "2020-07-19"}),
        assertFailure("ls_vd4", {ValueDate: "2013-07-17"}),


      // field: "NonResident",
          // Non Resdent Entity Element must be completed
          assertSuccess("ls_nr1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: "Synthesis"}}),
          assertSuccess("ls_nr1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: {}}}),
          assertFailure("ls_nr1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {}}),

      // field: "Resident.Entity.EntityName",
          // If Resident EntityCustomer is used, must be completed
          assertSuccess("ls_relen1", { ReportingQualifier: "BOPCARD RESIDENT", Resident: {Entity: {EntityName: "slkdjf"}}}),
          assertSuccess("ls_relen1", { ReportingQualifier: "NON REPORTABLE", Resident: {Entity: {EntityName: "Entity Name"}}}),
          assertFailure("ls_relen1", { ReportingQualifier: "NON REPORTABLE", Resident: {Entity: {EntityName: ""}}}),
          assertNoRule("ls_relen1", { ReportingQualifier: "BOPCARD NON RESIDENT", Resident: {Entity: {EntityName: "slkdjf"}}}),
          assertFailure("ls_relen1", { ReportingQualifier: "BOPCARD RESIDENT", Resident: {Entity: {TradingName: "slkdjf"}}}),

          // The words specified under Non-resident or Resident ExceptionName, must not be used
          // NOTE: Needs to make provision for STRATE
          assertFailure("ls_relen6", {
            ReportingQualifier: "BOPCARD RESIDENT", 
            Resident: {Entity: {EntityName: 'MUTUAL PARTY'}}
          }),
          assertSuccess("ls_relen6", {
            ReportingQualifier: "BOPCARD RESIDENT", 
            Resident: {Entity: {EntityName: 'SOMETHING ELSE'}}
          }),
          assertNoRule("ls_relen6", {
            ReportingQualifier: "BOPCARD NON RESIDENT", 
            Resident: {Entity: {EntityName: 'MUTUAL PARTY'}}
          }),

      // field: "ReplacementOriginalReference",
          // Additional spaces identified in data content
          assertSuccess("ls_repor3", {
            ReportingQualifier: 'BOPCUS',
            ReplacementOriginalReference: ""
          }),
          assertFailure("ls_repor3", {
            ReportingQualifier: 'BOPCUS',
            ReplacementOriginalReference: "Space here"
          }),
          assertFailure("ls_repor3", {
            ReportingQualifier: 'BOPCUS',
            ReplacementOriginalReference: "BothCharacter!and Space"
          }),
          assertSuccess("ls_repor3", {
            ReportingQualifier: 'BOPCUS',
            ReplacementOriginalReference: "NoSpacehere"
          }),

      // field: "NonResident.Individual.PassportNumber",
          // Additional spaces identified in data content
          assertSuccess("ls_nrpn2", {
            ReportingQualifier: 'BOPCUS', NonResident: {Individual: {PassportNumber: ''}}
          }),
          assertFailure("ls_nrpn2", {
            ReportingQualifier: 'BOPCUS', NonResident: {Individual: {PassportNumber: 'Space here'}}
          }),
          assertFailure("ls_nrpn2", {
            ReportingQualifier: 'BOPCUS', NonResident: {Individual: {PassportNumber: 'BothCharacter!and Space'}}
          }),
          assertSuccess("ls_nrpn2", {
            ReportingQualifier: 'BOPCUS', NonResident: {Individual: {PassportNumber: 'NoSpacehere'}}
          }),

      // field: "Resident.Entity.RegistrationNumber",
          // Additional spaces identified in data content
          assertSuccess("ls_rern4", {
            ReportingQualifier: "BOPCUS",
            Resident: {Entity: {EntityName: "company3", RegistrationNumber: ""}}
          }),
          assertFailure("ls_rern4", {
            ReportingQualifier: "BOPCUS",
            Resident: {Entity: {EntityName: "company3", RegistrationNumber: "Space here"}}
          }),
          assertFailure("ls_rern4", {
            ReportingQualifier: "BOPCUS",
            Resident: {Entity: {EntityName: "company3", RegistrationNumber: "BothCharacter!and Space"}}
          }),
          assertSuccess("ls_rern4", {
            ReportingQualifier: "BOPCUS",
            Resident: {Entity: {EntityName: "company3", RegistrationNumber: "NoSpacehere"}}
          }),

      // field: "Resident.Entity.TradingName",
          // If Resident EntityCustomer is used, it may be completed
          assertSuccess("ls_retn1", {ReportingQualifier: "BOPCUS", Resident: {Entity: {TradingName: "tradingName"}}}),
          assertNoRule("ls_retn1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            Resident: {Entity: {TradingName: "tradingName"}}
          }),
          assertFailure("ls_retn1", {ReportingQualifier: "BOPCARD RESIDENT", Resident: {Entity: {EntityName: "tradingName"}}}),

      // field: ["NonResident.Individual.Address.Country", "NonResident.Entity.Address.Country"],
          // SWIFT country code may not be {{Locale}}
          assertSuccess("ls_nrictry3:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {Country: "AD"}}}
          }),
          assertFailure("ls_nrictry3:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {Country: "LS"}}}
          }),
          assertNoRule("ls_nrictry3:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Individual: {Address: {Country: "AD"}}}
          }),
          assertSuccess("ls_nrictry3:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {Country: "AD"}}}
          }),
          assertFailure("ls_nrictry3:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {Country: "LS"}}}
          }),
          assertNoRule("ls_nrictry3:2", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Entity: {Address: {Country: "AD"}}}
          }),
          
      // field: ["NonResident.Individual.Address.AddressLine1", "NonResident.Entity.Address.AddressLine1"],
          // Must not be completed
          assertSuccess("ls_nrial11:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Individual: {Address: {AddressLine1: ""}}}
          }),
          assertFailure("ls_nrial11:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Individual: {Address: {AddressLine1: "testAddressLine"}}}
          }),
          assertNoRule("ls_nrial11:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {AddressLine1: "testAddressLine"}}}
          }),
          assertSuccess("ls_nrial11:2", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {Address: {AddressLine1: ""}}}
          }),
          assertFailure("ls_nrial11:2", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Entity: {Address: {AddressLine1: "testAddressLine"}}}
          }),
          assertNoRule("ls_nrial11:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {AddressLine1: "testAddressLine"}}}
          }),

      // field: ["NonResident.Individual.Address.AddressLine2", "NonResident.Entity.Address.AddressLine2"],
          // Must not be completed  
          assertSuccess("ls_nrial21:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Individual: {Address: {AddressLine2: ""}}}
          }),
          assertFailure("ls_nrial21:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Individual: {Address: {AddressLine2: "testAddressLine2"}}}
          }),
          assertNoRule("ls_nrial21:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {AddressLine2: "testAddressLine2"}}}
          }),
          assertSuccess("ls_nrial21:2", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {Address: {AddressLine2: ""}}}
          }),
          assertFailure("ls_nrial11:2", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Entity: {Address: {AddressLine1: "testAddressLine2"}}}
          }),
          assertNoRule("ls_nrial21:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {AddressLine2: "testAddressLine2"}}}
          }),

      // field: ["NonResident.Individual.Address.Suburb", "NonResident.Entity.Address.Suburb"],
          // Must not be completed
          assertSuccess("ls_nrial31:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Individual: {Address: {Suburb: ""}}}
          }),
          assertFailure("ls_nrial31:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Individual: {Address: {Suburb: "testSuburb"}}}
          }),
          assertNoRule("ls_nrial31:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {Suburb: "testSuburb"}}}
          }),
          assertSuccess("ls_nrial31:2", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {Address: {Suburb: ""}}}
          }),
          assertFailure("ls_nrial31:2", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Entity: {Address: {Suburb: "testSuburb"}}}
          }),
          assertNoRule("ls_nrial31:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {Suburb: "testSuburb"}}}
          }),

      // field: ["NonResident.Individual.Address.City", "NonResident.Entity.Address.City"],
          // Must not be completed 
          assertSuccess("ls_nric2:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Individual: {Address: {City: ""}}}
          }),
          assertFailure("ls_nric2:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Individual: {Address: {City: "testCity"}}}
          }),
          assertNoRule("ls_nric2:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {City: "testCity"}}}
          }),
          assertSuccess("ls_nric2:2", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {Address: {City: ""}}}
          }),
          assertFailure("ls_nric2:2", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Entity: {Address: {City: "testCity"}}}
          }),
          assertNoRule("ls_nric2:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {City: "testCity"}}}
          }),

      // field: ["NonResident.Individual.Address.State", "NonResident.Entity.Address.State"],
          // Must not be completed
          assertSuccess("ls_nris2:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Individual: {Address: {State: ""}}}
          }),
          assertFailure("ls_nris2:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Individual: {Address: {State: "testState"}}}
          }),
          assertNoRule("ls_nris2:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {State: "testState"}}}
          }),
          assertSuccess("ls_nris2:2", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {Address: {State: ""}}}
          }),
          assertFailure("ls_nris2:2", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Entity: {Address: {State: "testState"}}}
          }),
          assertNoRule("ls_nris2:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {State: "testState"}}}
          }),

      // field: ["NonResident.Individual.Address.PostalCode", "NonResident.Entity.Address.PostalCode"],
          // Must not be completed
          assertSuccess("ls_nriz2:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Individual: {Address: {PostalCode: ""}}}
          }),
          assertFailure("ls_nriz2:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Individual: {Address: {PostalCode: "testPostalCode"}}}
          }),
          assertNoRule("ls_nriz2:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {Address: {PostalCode: "testPostalCode"}}}
          }),
          assertSuccess("ls_nriz2:2", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {Address: {PostalCode: ""}}}
          }),
          assertFailure("ls_nriz2:2", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            NonResident: {Entity: {Address: {PostalCode: "testPostalCode"}}}
          }),
          assertNoRule("ls_nriz2:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {Address: {PostalCode: "testPostalCode"}}}
          }),

      // field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
          //NonResident.Individual.AccountIdentifier - If the Flow is OUT must contain a value of NON RESIDENT OTHER or NON RESIDENT LOTI or NON RESIDENT FCDA or CASH or FCDA RESIDENT or RES FOREIGN BANK ACCOUNT or VOSTRO or VISA NET or MASTER SEND
          assertSuccess("ls_nriaid1:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {AccountIdentifier: "CASH"}}
          }),
          assertSuccess("ls_nriaid1:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {AccountIdentifier: "NON RESIDENT DOMESTIC CURRENCY"}}
          }),
          assertFailure("ls_nriaid1:1", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Individual: {AccountIdentifier: "INVALID ACCOUNT IDENTIFIER"}}
          }),
          assertNoRule("ls_nriaid1:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Individual: {AccountIdentifier: "CASH"}}
          }),

          //NonResident.Entity.AccountIdentifier - If the Flow is OUT must contain a value of NON RESIDENT OTHER or NON RESIDENT NAD or NON RESIDENT FCA or FCA RESIDENT or VOSTRO or CASH or FCA RESIDENT or RES FOREIGN BANK ACCOUNT
          assertSuccess("ls_nriaid1:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {AccountIdentifier: "CASH"}}
          }),
          assertSuccess("ls_nriaid1:2", {
            ReportingQualifier: "BOPCUS",
            NonResident: {Entity: {AccountIdentifier: "RES FOREIGN BANK ACCOUNT"}}
          }),
          assertFailure("ls_nriaid1:2", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {AccountIdentifier: "SOMETHING ELSE"}}}),
          assertNoRule("ls_nriaid1:2", {
            ReportingQualifier: "BOPCARD RESIDENT",
            NonResident: {Entity: {AccountIdentifier: "CASH"}}
          }),

      // field: ["NonResident.Individual.AccountNumber", "NonResident.Entity.AccountNumber"],
          // If the Flow is OUT must be completed if the AccountIdentifier is not CASH
          assertSuccess("ls_nrian1:1", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Individual:{AccountIdentifier: "FCA RESIDENT", AccountNumber: "1235498"}}}),
          assertSuccess("ls_nrian1:1", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Individual:{AccountIdentifier: "CASH", AccountNumber: ""}}}),
          assertFailure("ls_nrian1:1", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Individual:{AccountIdentifier: "FCA RESIDENT", AccountNumber: ""}}}),
          assertSuccess("ls_nrian1:2", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Entity:{AccountIdentifier: "FCA RESIDENT", AccountNumber: "1235498"}}}),
          assertSuccess("ls_nrian1:2", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Entity:{AccountIdentifier: "CASH", AccountNumber: ""}}}),
          assertFailure("ls_nrian1:2", {ReportingQualifier: "BOPCUS", Flow: "OUT", NonResident:{Entity:{AccountIdentifier: "FCA RESIDENT", AccountNumber: ""}}}),

      // field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],
        // Must be completed if BoPCategory is 10101 to 10111 or 10301 to 10311 or 10500 or 10600'
        assertSuccess('ls_ccn_1_and_2:1', {
          ReportingQualifier: 'BOPCUS', 
          Flow: 'OUT', 
          Resident: {
            Individual: {
              CustomsClientNumber: '1234'
            }
          },
          MonetaryAmount: [
            {CategoryCode: '101', CategorySubCode: '11'},
          ]
        }),

        assertSuccess('ls_ccn_1_and_2:1', {
          ReportingQualifier: 'BOPCUS', 
          Flow: 'IN', 
          Resident: {
            Individual: {
              CustomsClientNumber: '1234'
            }
          },
          MonetaryAmount: [
            {CategoryCode: '101', CategorySubCode: '11'},
          ]
        }),

        assertFailure('ls_ccn_1_and_2:1', {
          ReportingQualifier: 'BOPCUS', 
          Flow: 'IN', 
          Resident: {
            Individual: {
              CustomsClientNumber: ''
            }
          },
          MonetaryAmount: [
            {CategoryCode: '101', CategorySubCode: '11'},
          ]
        }),

        assertNoRule('ls_ccn_1_and_2:1', {
          ReportingQualifier: 'BOPCARD RESIDENT', 
          Flow: 'IN', 
          Resident: {
            Individual: {
              CustomsClientNumber: ''
            }
          },
          MonetaryAmount: [
            {CategoryCode: '101', CategorySubCode: '11'},
          ]
        }),

        assertSuccess('ls_ccn_1_and_2:2', {
          ReportingQualifier: 'BOPCUS', 
          Flow: 'OUT', 
          Resident: {
            Entity: {
              CustomsClientNumber: '1234'
            }
          },
          MonetaryAmount: [
            {CategoryCode: '101', CategorySubCode: '11'},
          ]
        }),

        assertSuccess('ls_ccn_1_and_2:2', {
          ReportingQualifier: 'BOPCUS', 
          Flow: 'IN', 
          Resident: {
            Entity: {
              CustomsClientNumber: '1234'
            }
          },
          MonetaryAmount: [
            {CategoryCode: '101', CategorySubCode: '11'},
          ]
        }),

        assertFailure('ls_ccn_1_and_2:2', {
          ReportingQualifier: 'BOPCUS', 
          Flow: 'IN', 
          Resident: {
            Entity: {
              CustomsClientNumber: ''
            }
          },
          MonetaryAmount: [
            {CategoryCode: '101', CategorySubCode: '11'},
          ]
        }),

        assertNoRule('ls_ccn_1_and_2:2', {
          ReportingQualifier: 'BOPCARD RESIDENT', 
          Flow: 'IN', 
          Resident: {
            Entity: {
              CustomsClientNumber: ''
            }
          },
          MonetaryAmount: [
            {CategoryCode: '101', CategorySubCode: '11'},
          ]
        }),

          // If the FLOW is IN and the BoPCategory is 10101 to 10110 or 10301 to 10310 or 10500 or 10600, the CustomsClientNumber must contain a valid Customs client number.
          assertFailure('ls_ccn3:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {CustomsClientNumber: '1'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '11'}]
          }),
          assertSuccess('ls_ccn3:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {CustomsClientNumber: '123'}}, 
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '11'}]
          }),
          assertNoRule('ls_ccn3:1', {
            ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'IN', Resident: {Individual: {CustomsClientNumber: '123'}}, 
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '11'}]
          }),
          assertNoRule('ls_ccn3:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {CustomsClientNumber: '123'}},
            MonetaryAmount: [{CategoryCode: '107', CategorySubCode: '11'},
              {CategoryCode: '107', CategorySubCode: '11'}]
          }),

          assertFailure('ls_ccn3:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {CustomsClientNumber: '1'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '11'}]
          }),
          assertSuccess('ls_ccn3:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {CustomsClientNumber: '123'}}, 
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertNoRule('ls_ccn3:2', {
            ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'IN', Resident: {Entity: {CustomsClientNumber: '123'}}, 
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '11'}]
          }),
          assertNoRule('ls_ccn3:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {CustomsClientNumber: '123'}},
            MonetaryAmount: [{CategoryCode: '107', CategorySubCode: '11'},
              {CategoryCode: '107', CategorySubCode: '11'}]
          }),

          // If the FLOW is OUT and BoPCategory is 10101 to 10110 or 10301 to 10310 or 10500 or 10600, the CustomsClientNumber must contain a valid Customs Client Number
          assertFailure('ls_ccn4:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: '1'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertFailure('ls_ccn4:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: 'string'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertSuccess('ls_ccn4:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: '123'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertNoRule('ls_ccn4:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {CustomsClientNumber: '123'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),

          assertFailure('ls_ccn4:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: '1'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertFailure('ls_ccn4:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: 'string'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertSuccess('ls_ccn4:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: '123'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),
          assertNoRule('ls_ccn4:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {CustomsClientNumber: '123'}},
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11'},
              {CategoryCode: '101', CategorySubCode: '10'}]
          }),

          // Must not be completed
          assertFailure('ls_ccn5:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {CustomsClientNumber: 'abc'}},
          }),
          assertSuccess('ls_ccn5:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {CustomsClientNumber: ''}},
          }),
          assertNoRule('ls_ccn5:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {CustomsClientNumber: ''}},
          }),
          assertFailure('ls_ccn5:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {CustomsClientNumber: 'abc'}},
          }),
          assertSuccess('ls_ccn5:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {CustomsClientNumber: ''}},
          }),
          assertNoRule('ls_ccn5:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {CustomsClientNumber: ''}},
          }),

      // Resident.Individual.TAXClearanceCertificateIndicator, Resident.Entity.TaxClearanceCertificateIndicator
          // Must not be completed
          assertSuccess('ls_tcci2:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT'
          }),
          assertFailure('ls_tcci2:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {
              Individual: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: '1234'
              }
            }
          }),
          assertSuccess('ls_tcci2:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT'
          }),
          assertFailure('ls_tcci2:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {
              Entity: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: '1234'
              }
            }
          }),

      // field: ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1"],
          // Resident.Individual.StreetAddress.AddressLine1 - Physical address must be completed
          assertSuccess('ls_a1_1:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {AddressLine1: '12 Foo Street'}}}
          }),
          assertFailure('ls_a1_1:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {AddressLine1: ''}}}
          }),
          assertNoRule('ls_a1_1:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {AddressLine1: ''}}}
          }),
          // Resident.Individual.StreetAddress.AddressLine1 - Physical address must be completed
          assertSuccess('ls_a1_1:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {AddressLine1: '12 Foo Street'}}}
          }),
          assertFailure('ls_a1_1:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {AddressLine1: ''}}}
          }),
          assertNoRule('ls_a1_1:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {AddressLine1: ''}}}
          }),

      // field: ["Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb"],
          // Resident.Individual.StreetAddress.Suburb - Physical StreetSuburb must be completed
          assertSuccess('ls_s1:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {Suburb: 'MoeggoeVille'}}}
          }),
          assertFailure('ls_s1:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {Suburb: ''}}}
          }),
          assertNoRule('ls_s1:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {Suburb: ''}}}
          }),
          // Resident.Entity.StreetAddress.Suburb - Physical StreetSuburb must be completed
          assertSuccess('ls_s1:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {Suburb: 'MoeggoeVille'}}}
          }),
          assertFailure('ls_s1:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {Suburb: ''}}}
          }),
          assertNoRule('ls_s1:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {Suburb: ''}}}
          }),

      // field: ["Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City"],
          // Resident.Individual.StreetAddress.City - Physical city or town must be completed
          assertSuccess('ls_c1:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {City: 'TestCity'}}}
          }),
          assertFailure('ls_c1:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {City: ''}}}
          }),
          assertNoRule('ls_c1:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {City: ''}}}
          }),
          // Resident.Entity.StreetAddress.City - Physical city or town must be completed
          assertSuccess('ls_c1:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {City: 'TestCity'}}}
          }),
          assertFailure('ls_c1:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {City: ''}}}
          }),
          assertNoRule('ls_c1:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {City: ''}}}
          }),

      // field: [ "Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City"],
          // Resident.Individual.PostalAddress.City - City or town must be completed
          assertSuccess('ls_c2:1', {ReportingQualifier: 'BOPCUS', Resident: {Individual: {PostalAddress: {City: 'TestCity'}}}}),
          assertFailure('ls_c2:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {PostalAddress: {City: ''}}}
          }),
          assertNoRule('ls_c2:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {PostalAddress: {City: ''}}}
          }),
          // Resident.Entity.PostalAddress.City - City or town must be completed
          assertSuccess('ls_c2:2', {ReportingQualifier: 'BOPCUS', Resident: {Entity: {PostalAddress: {City: 'TestCity'}}}}),
          assertFailure('ls_c2:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {PostalAddress: {City: ''}}}
          }),
          assertNoRule('ls_c2:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {PostalAddress: {City: ''}}}
          }),

      // field: [ "Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1",
      //   "Resident.Individual.StreetAddress.AddressLine2", "Resident.Entity.StreetAddress.AddressLine2",
      //   "Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb",
      //   "Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City",
      //   "Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province",
      //   "Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1",
      //   "Resident.Individual.PostalAddress.AddressLine2", "Resident.Entity.PostalAddress.AddressLine2",
      //   "Resident.Individual.PostalAddress.Suburb", "Resident.Entity.PostalAddress.Suburb",
      //   "Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City",
      //   "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
          // "Resident.Individual.StreetAddress.AddressLine1" - Must not be completed
          assertSuccess('ls_s3:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {AddressLine1: ''}}}
          }),
          assertFailure('ls_s3:1', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {AddressLine1: 'TestAddressLine1'}}}
          }),
          assertNoRule('ls_s3:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {StreetAddress: {AddressLine1: 'TestAddressLine1'}}}
          }),
          // "Resident.Entity.StreetAddress.AddressLine1" - Must not be completed
          assertSuccess('ls_s3:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {AddressLine1: ''}}}
          }),
          assertFailure('ls_s3:2', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {AddressLine1: 'TestAddressLine1'}}}
          }),
          assertNoRule('ls_s3:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {StreetAddress: {AddressLine1: 'TestAddressLine1'}}}
          }),
          // "Resident.Individual.StreetAddress.AddressLine2" - Must not be completed
          assertSuccess('ls_s3:3', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {AddressLine2: ''}}}
          }),
          assertFailure('ls_s3:3', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {AddressLine2: 'TestAddressLine2'}}}
          }),
          assertNoRule('ls_s3:3', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {StreetAddress: {AddressLine2: 'TestAddressLine2'}}}
          }),
          // "Resident.Entity.StreetAddress.AddressLine2" - Must not be completed
          assertSuccess('ls_s3:4', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {AddressLine2: ''}}}
          }),
          assertFailure('ls_s3:4', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {AddressLine2: 'TestAddressLine2'}}}
          }),
          assertNoRule('ls_s3:4', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {StreetAddress: {AddressLine2: 'TestAddressLine2'}}}
          }),
          // "Resident.Individual.StreetAddress.Suburb" - Must not be completed
          assertSuccess('ls_s3:5', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {Suburb: ''}}}
          }),
          assertFailure('ls_s3:5', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Individual: {StreetAddress: {Suburb: 'TestSuburb'}}}
          }),
          assertNoRule('ls_s3:5', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {StreetAddress: {Suburb: 'TestSuburb'}}}
          }),
          // "Resident.Entity.StreetAddress.Suburb" - Must not be completed
          assertSuccess('ls_s3:6', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {Suburb: ''}}}
          }),
          assertFailure('ls_s3:6', {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            Resident: {Entity: {StreetAddress: {Suburb: 'TestSuburb'}}}
          }),
          assertNoRule('ls_s3:6', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {StreetAddress: {Suburb: 'TestSuburb'}}}
          }),

      // field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
          // Must not be completed
          assertFailure('ls_tccr2:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: '1234'
              }
            }
          }),
          assertSuccess('ls_tccr2:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Individual: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: ''
              }
            }
          }),
          assertNoRule('ls_tccr2:1', {
            ReportingQualifier: 'BOPDIR',
            Resident: {
              Individual: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: ''
              }
            }
          }),
          assertFailure('ls_tccr2:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Entity: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: '1234'
              }
            }
          }),
          assertSuccess('ls_tccr2:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {
              Entity: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: ''
              }
            }
          }),
          assertNoRule('ls_tccr2:2', {
            ReportingQualifier: 'BOPDIR',
            Resident: {
              Entity: {
                TaxClearanceCertificateIndicator: 'Y',
                TaxClearanceCertificateReference: ''
              }
            }
          }),

      // field: [ "Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1"],
          // Must be completed
          assertSuccess('ls_a1_3:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {PostalAddress: {AddressLine1: '12 Foo Street'}}}
          }),
          assertFailure('ls_a1_3:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {PostalAddress: {AddressLine1: ''}}}
          }),
          assertFailure('ls_a1_3:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {PostalAddress: {AddressLine1: ''}}}
          }),
          assertSuccess('ls_a1_3:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {PostalAddress: {AddressLine1: '12 Foo Street'}}}
          }),
          assertFailure('ls_a1_3:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {PostalAddress: {AddressLine1: ''}}}
          }),
          assertFailure('ls_a1_3:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {PostalAddress: {AddressLine1: ''}}}
          }),

      // field: ["Resident.Individual.AccountNumber", "Resident.Entity.AccountNumber"],
          // Must be completed if the Flow is OUT and the AccountIdentifier under AdditionalCustomerData is not CASH or EFT or CARD PAYMENT
          assertFailure('ls_accno4:1', {
            ReportingQualifier: "BOPCUS",
            Flow: 'OUT',
            Resident: {Individual: {AccountNumber: "", AccountIdentifier: "CREDIT CARD"}}
          }),
          assertSuccess('ls_accno4:1', {
            ReportingQualifier: "BOPCUS",
            Flow: 'OUT',
            Resident: {
              Individual: {
                AccountNumber: "234234",
                AccountIdentifier: "CREDIT CARD"
              }
            }
          }),
          assertNoRule('ls_accno4:1', {
            ReportingQualifier: "BOPCUS",
            Flow: 'IN',
            Resident: {Individual: {AccountNumber: "1234", AccountIdentifier: "CASH"}}
          }),
          assertFailure('ls_accno4:2', {
            ReportingQualifier: "BOPCUS",
            Flow: 'OUT',
            Resident: {Entity: {AccountNumber: "", AccountIdentifier: "CREDIT CARD"}}
          }),
          assertSuccess('ls_accno4:2', {
            ReportingQualifier: "BOPCUS",
            Flow: 'OUT',
            Resident: {
              Entity: {
                AccountNumber: "234234",
                AccountIdentifier: "CREDIT CARD"
              }
            }
          }),
          assertNoRule('ls_accno4:2', {
            ReportingQualifier: "BOPCUS",
            Flow: 'IN',
            Resident: {Entity: {AccountNumber: "", AccountIdentifier: "CASH"}}
          }),

      // field: ["Resident.Individual.AccountName", "Resident.Entity.AccountName"],
          // Must be completed except if the AccountIdentifier is CASH or EFT or CARD PAYMENT
          assertSuccess("ls_an2:1", {ReportingQualifier: "BOPCUS", Resident: {Individual: {AccountName: ""}}}),
          assertFailure("ls_an2:1", {ReportingQualifier: "BOPCUS", Resident: {Individual: {AccountIdentifier: "CASH", AccountName: "slkfj"}}}),
          assertNoRule("ls_an2:1", {ReportingQualifier: "BOPCUS", Resident: {Individual: {AccountIdentifier: "RESIDENT OTHER", AccountName: "slkfj"}}}),
          assertSuccess("ls_an2:2", {ReportingQualifier: "BOPCUS", Resident: {Entity: {AccountName: ""}}}),
          assertFailure("ls_an2:2", {ReportingQualifier: "BOPCUS", Resident: {Entity: {AccountIdentifier: "CASH", AccountName: "slkfj"}}}),
          assertNoRule("ls_an2:2", {ReportingQualifier: "BOPCUS", Resident: {Entity: {AccountIdentifier: "RESIDENT OTHER", AccountName: "slkfj"}}}),

      // field: "TrnReference",
          // Additional spaces identified in data content
          assertSuccess("ls_tref2", {TrnReference: 'trnref'}),
          assertFailure("ls_tref2", {TrnReference: 'trn  ref'}),
          assertFailure("ls_tref2", {TrnReference: ' trnref'}),
          assertFailure("ls_tref2", {TrnReference: 'trnref '}),

      // field: "Resident.Individual.IDNumber",
          assertSuccess("ls_riidn3", {Resident: {Individual: {IDNumber: "AA123456"}}}),
          assertSuccess("ls_riidn3", {Resident: {Individual: {IDNumber: "123456789012"}}}),
          assertFailure("ls_riidn3", {Resident: {Individual: {IDNumber: "1234567"}}}),
          assertFailure("ls_riidn3", {Resident: {Individual: {IDNumber: "AA123"}}}),

          // If IndividualCustomer is selected, at least one of IDNumber or TempResPermitNumber or ForeignIDNumber or PassportNumber must be completed
          assertSuccess("ls_riidn4", {
            ReportingQualifier: "BOPCUS", 
            Resident: {Individual: {IDNumber: "1234567890123"}}
          }),
          assertSuccess("ls_riidn4", {
            ReportingQualifier: "BOPCUS",
            Resident: {Individual: {TempResPermitNumber: "9874654"}}
          }),
          assertSuccess("ls_riidn4", {
            ReportingQualifier: "BOPCUS",
            Resident: {Individual: {ForeignIDNumber: "987645121"}}
          }),
          assertSuccess("ls_riidn4", {
            ReportingQualifier: "BOPCUS",
            Resident: {Individual: {PassportNumber: "1029384756"}}
          }),
          assertFailure("ls_riidn4", {
            ReportingQualifier: "BOPCUS", 
            Resident: {Individual: {Name: "Has_No_Identifying_Number"}}
          }),
          assertNoRule("ls_riidn4", {
            ReportingQualifier: "BOPCUS", 
            Resident: {Entity: {EntityName: "Has_No_Identifying_Number"}}
          }),

      // field: "Resident.Individual.TempResPermitNumber",
          // If IndividualCustomer is selected, at least one of IDNumber or TempResPermitNumber or ForeignIDNumber or PassportNumber must be completed
          assertSuccess("ls_ritrpn3", {
            ReportingQualifier: "BOPCUS", 
            Resident: {Individual: {IDNumber: "1234567890123"}}
          }),
          assertSuccess("ls_ritrpn3", {
            ReportingQualifier: "BOPCUS",
            Resident: {Individual: {TempResPermitNumber: "9874654"}}
          }),
          assertSuccess("ls_ritrpn3", {
            ReportingQualifier: "BOPCUS",
            Resident: {Individual: {ForeignIDNumber: "987645121"}}
          }),
          assertSuccess("ls_ritrpn3", {
            ReportingQualifier: "BOPCUS",
            Resident: {Individual: {PassportNumber: "1029384756"}}
          }),
          assertFailure("ls_ritrpn3", {
            ReportingQualifier: "BOPCUS", 
            Resident: {Individual: {Name: "Has_No_Identifying_Number"}}
          }),
          assertNoRule("ls_ritrpn3", {
            ReportingQualifier: "BOPCUS", 
            Resident: {Entity: {EntityName: "Has_No_Identifying_Number"}}
          }),

      // field: "Resident.Individual.ForeignIDNumber",
          // If IndividualCustomer is selected, at least one of IDNumber or TempResPermitNumber or ForeignIDNumber or PassportNumber must be completed
          assertSuccess("ls_rifidn3", {
            ReportingQualifier: "BOPCUS", 
            Resident: {Individual: {IDNumber: "1234567890123"}}
          }),
          assertSuccess("ls_rifidn3", {
            ReportingQualifier: "BOPCUS",
            Resident: {Individual: {TempResPermitNumber: "9874654"}}
          }),
          assertSuccess("ls_rifidn3", {
            ReportingQualifier: "BOPCUS",
            Resident: {Individual: {ForeignIDNumber: "987645121"}}
          }),
          assertSuccess("ls_rifidn3", {
            ReportingQualifier: "BOPCUS",
            Resident: {Individual: {PassportNumber: "1029384756"}}
          }),
          assertFailure("ls_rifidn3", {
            ReportingQualifier: "BOPCUS", 
            Resident: {Individual: {Name: "Has_No_Identifying_Number"}}
          }),
          assertNoRule("ls_rifidn3", {
            ReportingQualifier: "BOPCUS", 
            Resident: {Entity: {EntityName: "Has_No_Identifying_Number"}}
          }),


      // field: "Resident.Individual.PassportNumber",
          // If IndividualCustomer is selected, at least one of IDNumber or TempResPermitNumber or ForeignIDNumber or PassportNumber must be completed
          assertSuccess("ls_ripn4", {
            ReportingQualifier: "BOPCUS", 
            Resident: {Individual: {IDNumber: "1234567890123"}}
          }),
          assertSuccess("ls_ripn4", {
            ReportingQualifier: "BOPCUS",
            Resident: {Individual: {TempResPermitNumber: "9874654"}}
          }),
          assertSuccess("ls_ripn4", {
            ReportingQualifier: "BOPCUS",
            Resident: {Individual: {ForeignIDNumber: "987645121"}}
          }),
          assertSuccess("ls_ripn4", {
            ReportingQualifier: "BOPCUS",
            Resident: {Individual: {PassportNumber: "1029384756"}}
          }),
          assertFailure("ls_ripn4", {
            ReportingQualifier: "BOPCUS", 
            Resident: {Individual: {Name: "Has_No_Identifying_Number"}}
          }),
          assertNoRule("ls_ripn4", {
            ReportingQualifier: "BOPCUS", 
            Resident: {Entity: {EntityName: "Has_No_Identifying_Number"}}
          }),

      //Resident.Individual.AccountIdentifier
          assertSuccess("ls_accid1:1", {
            ReportingQualifier: "BOPCUS",
            Resident: {Individual: {AccountIdentifier: "RESIDENT OTHER"}}
          }),
          assertFailure("ls_accid1:1", {
            ReportingQualifier: "BOPCUS",
            Resident: {Individual: {AccountIdentifier: "CFDC RESIDENT"}}
          }),
          assertNoRule("ls_accid1:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            Resident: {Individual: {AccountIdentifier: "CFDC RESIDENT"}}
          }),
          assertSuccess("ls_accid5:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            Resident: {Individual: {AccountIdentifier: "RESIDENT OTHER"}}
          }),
          assertFailure("ls_accid5:1", {
            ReportingQualifier: "BOPCARD RESIDENT",
            Resident: {Individual: {AccountIdentifier: "CFDC RESIDENT"}}
          }),
          assertNoRule("ls_accid5:1", {
            ReportingQualifier: "BOPCARD NON RESIDENT",
            Resident: {Individual: {AccountIdentifier: "CFDC RESIDENT"}}
          }),


//================================================================================================================================================
//  # MONEY (3/6)
//================================================================================================================================================

      // field: "SequenceNumber",
          // Must contain a sequential SequenceNumber entries 
          assertSuccess('ls_mseq1', {
            ReportingQualifier: 'BOPCARD',
            MonetaryAmount: [{SequenceNumber: "1"}, {SequenceNumber: "2"}]
          }),
          assertFailure('ls_mseq1', {
            ReportingQualifier: 'BOPCARD',
            MonetaryAmount: [{SequenceNumber: "2"}]
          }),
     
      // // field: "{{LocalValue}}",
      //     // If DomesticCurrencyCode is LSL and the ForeignCurrencyCode is the same, the DomesticValue must be equal to ForeignValue.
      //     assertSuccess('ls_mrv11', {
      //       ReportingQualifier: 'BOPCARD RESIDENT',
      //       FlowCurrency: 'LSL',
      //       MonetaryAmount: [{
      //         DomesticValue:8.11,
      //         ForeignValue: 8.11}]
      //     }),
      //     assertFailure('ls_mrv11', {
      //       ReportingQualifier: 'BOPCARD RESIDENT',
      //       FlowCurrency: 'LSL',
      //       MonetaryAmount: [{
      //         DomesticValue:8.11,
      //         ForeignValue: 9.11}]
      //     }),
      //     assertNoRule('ls_mrv11', {
      //       ReportingQualifier: 'BOPCARD RESIDENT',
      //       FlowCurrency: 'USD',
      //       MonetaryAmount: [{
      //         DomesticValue:8.11,
      //         ForeignValue: 8.11}]
      //     }),

          // Must not equal ForeignValue
          assertSuccess('ls_mrv12', {
            ReportingQualifier: 'BOPCUS',
            FlowCurrency: 'ZAR',
            MonetaryAmount: [{
              DomesticValue:8.11,
              ForeignValue: 8.11}]
          }),
          assertSuccess('ls_mrv12', {
            ReportingQualifier: 'BOPCUS',
            FlowCurrency: 'ZAR',
            MonetaryAmount: [{
              DomesticValue:800.00,
              ForeignValue: 800}]
          }),
          assertFailure('ls_mrv12', {
            ReportingQualifier: 'BOPCUS',
            FlowCurrency: 'USD',
            MonetaryAmount: [{
              DomesticValue:8.11,
              ForeignValue: 8.11}]
          }),
          assertFailure('ls_mrv12', {
            ReportingQualifier: 'BOPCUS',
            FlowCurrency: 'USD',
            MonetaryAmount: [{
              DomesticValue:852.00,
              ForeignValue: 852}]
          }),
          assertFailure('ls_mrv12', {
            ReportingQualifier: 'BOPCUS',
            FlowCurrency: 'USD',
            MonetaryAmount: [{
              DomesticValue:8.11,
              ForeignValue: 8.11}]
          }),
         
      // field: "DomesticCurrencyCode",
          // Invalid SWIFT currency code.
          assertSuccess("ls_ncc2", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{DomesticCurrencyCode: 'LSL'}]}),
          assertSuccess("ls_ncc2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{DomesticCurrencyCode: 'LSL'}]}),
          assertFailure("ls_ncc2", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{DomesticCurrencyCode: 'ABC'}]}),
          assertFailure("ls_ncc2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{DomesticCurrencyCode: 'ABC'}]}),
          
          // SWIFT currecy code must only be LSL
          assertSuccess("ls_ncc3", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{DomesticCurrencyCode: 'LSL'}]}),
          assertFailure("ls_ncc3", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{DomesticCurrencyCode: 'ZAR'}]}),
          
          // Must not be completed
          // assertSuccess("ls_ncc4", {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   LocalCurrency: 'LSL',
          //   ForeignCurrencyCode:'ZAR',
          //   MonetaryAmount: [{
          //     LocalValue:0.00,
          //     ForeignValue: 0.00}]
          // }),
          // assertFailure("ls_ncc4", {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   LocalCurrency: 'LSL',
          //   ForeignCurrencyCode:'ZAR',
          //   MonetaryAmount: [{
          //     DomesticCurrencyCode: 'COMPLETED',
          //     LocalValue:0.20,
          //     ForeignValue: 0.00}]
          // }),

      // field: "LoanRefNumber",
          // If the BoPCategory 80100, or 80200, or 80300, or 80400, or 81000 or 81500 or 81600 or 81700 or 81800 or 81900 is used, must be completed.
          assertSuccess("ls_mlrn1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '801', LoanRefNumber: 'XYZ'}]
          }),
          assertFailure("ls_mlrn1", {
            ReportingQualifier: 'BOPCUS', 
            MonetaryAmount: [{CategoryCode: '801'}]}),

          // If the BoPCategory 10600 or 30901 or 30902 or 30903 or 30904 or 30905 or 30906 or 30907, it must be completed.
          assertSuccess("ls_mlrn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '106',
              LoanRefNumber: 'test'
            }]
          }),
          assertFailure("ls_mlrn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '106',
              LoanRefNumber: ''
            }]
          }),
          assertNoRule("ls_mlrn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '08',
              LoanRefNumber: ''
            }]
          }),
          assertNoRule("ls_mlrn2", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{
              CategoryCode: '106',
              LoanRefNumber: 'loanref'
            }]
          }),

          // For any other BoPCategory other than 801, or 802, or 803 or 804 or 810 or 815 or 816 or 817 or 818 or 819 or 106 or 309, it must not be completed
          assertSuccess("ls_mlrn3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '80100', LoanRefNumber: ''}]
          }),
          assertNoRule("ls_mlrn3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '80500', LoanRefNumber: ''}]
          }),
          assertFailure("ls_mlrn3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '80100', LoanRefNumber: 'XYZ'}]
          }),

          // Invalid loan reference number
          assertSuccess("ls_mlrn5", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              LoanRefNumber: '123'
            }]
          }),
          assertFailure("ls_mlrn5", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              LoanRefNumber: '123a'
            }]
          }),     

      // field: "LoanInterestRate",
         //If the BoPCategory 30901 to 30907 is used, must be completed reflecting the percentage interest paid.
          assertSuccess("ls_mlir1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '309',CategorySubCode: '01', LoanInterestRate: '0.0'}]
          }),
          assertFailure("ls_mlir1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '309',CategorySubCode: '01'}]
          }),
          assertNoRule("ls_mlir1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '309',CategorySubCode: '08', LoanInterestRate: '0.0'}]
          }),

          //If the BoPCategory 30901 to 30907 must be completed in the format 0.00
          assertSuccess("ls_mlir2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '01',
              LoanInterestRate: '5.12'
            }]
          }),
          assertFailure("ls_mlir2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '01',
              LoanInterestRate: ''
            }]
          }),

          // Except for BoPCategory 30901, 30902, 30903, 30904, 30905, 30906, 30907 it must not be completed
          assertSuccess("ls_mlir3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '01',
              LoanInterestRate: '20.00'
            }]
          }),
          assertFailure("ls_mlir3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '01',
              LoanInterestRate: '20'
            }]
          }),
          assertNoRule("ls_mlir3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '08',
              LoanInterestRate: '20'
            }]
          }),

          // Must not be completed
          assertSuccess("ls_mlir4", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{}]}),
          assertFailure("ls_mlir4", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{LoanInterestRate: '5.12'}]}),
          assertNoRule("ls_mlir4", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{
            CategoryCode: '309',
            CategorySubCode: '01',
            LoanInterestRate: '5.12'}]
          }),


      // field: "ThirdParty.CustomsClientNumber",
          // Invalid CustomsClientNumber
          assertSuccess("ls_mtpccn1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '12345'}}]
          }),
          assertSuccess("ls_mtpccn1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '111111111111111'}}] //15 Digits
          }),
          assertFailure("ls_mtpccn1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '1111111111111111'}}] //16 Digits
          }),
          assertFailure("ls_mtpccn1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: 'a12345'}}]
          }),
          assertNoRule("ls_mtpccn1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: ''}}]
          }),

          // If it contains a value, the CustomsClientNumber must contain a valid Customs Client number
          assertSuccess("ls_mtpccn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '12345'}}]
          }),
          assertSuccess("ls_mtpccn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '111111111111111'}}] //15 Digits
          }),
          assertFailure("ls_mtpccn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: '1111111111111111'}}] //16 Digits
          }),
          assertFailure("ls_mtpccn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: 'a12345'}}]
          }),
          assertNoRule("ls_mtpccn2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{ThirdParty: {CustomsClientNumber: ''}}]
          }),

      // field: "ImportExport",
          // ImportExportData Element must not be completed
          assertSuccess("ls_mtie7", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{}]}),
          assertFailure("ls_mtie7", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{ImportExport: [{}]}]}),
          assertNoRule("ls_mtie7", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ImportExport: [{}]}]}),

      // field: "ReversalTrnRefNumber",
          // Additional spaces identified in data content
          assertSuccess("ls_mrtrn4", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ReversalTrnRefNumber: 'XYZ'}]}),
          assertFailure("ls_mrtrn4", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ReversalTrnRefNumber: ' XYZ'}]}),
          assertFailure("ls_mrtrn4", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{ReversalTrnRefNumber: 'X YZ'}]}),
          assertNoRule("ls_mrtrn4", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{ReversalTrnRefNumber: ' XYZ'}]}),

//================================================================================================================================================
//  # IMPORT/EXPORT (4/6)
//================================================================================================================================================
      // field: "ImportControlNumber",
          //Must be completed if the Flow is OUT and the BoPCategory is 10101 to 10111 or 10301 to 10310 or 10500 or 10600.
          assertSuccess("ls_ieicn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11', ImportExport: [{ImportControlNumber: 'XYZ'}]}]
          }),
          assertFailure("ls_ieicn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11', ImportExport: [{ImportControlNumber: ''}]}]
          }),

          // If the Flow is OUT and the BoPCategory is 10301 to 10311 or 10500 or 10600, the first 3 characters must be a valid customs office code. 
          assertSuccess("ls_ieicn4", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: '23100000'}]}]
          }),
          assertFailure("ls_ieicn4", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: '231'}]}]
          }),
          assertFailure("ls_ieicn4", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: '23200000'}]}]
          }),
          assertFailure("ls_ieicn4", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: '23'}]}]
          }),
          assertNoRule("ls_ieicn4", {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{ImportControlNumber: '23100000'}]}]
          }),


          // field: "TransportDocumentNumber",
          // Must be completed if the Flow is OUT and the BoPCategory is 10301 to 10310.
          assertSuccess("ls_ietdn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{
              CategoryCode: '103',
              CategorySubCode: '04',
              ImportExport: [{TransportDocumentNumber: 'TransportDocumentNumber'}]
            }]
          }),

          assertSuccess("ls_ietdn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{
              CategoryCode: '105',
              ImportExport: [{TransportDocumentNumber: 'TransportDocumentNumber'}]
            }]
          }),

          assertNoRule("ls_ietdn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{
              CategoryCode: '401',
              ImportExport: [{TransportDocumentNumber: 'TransportDocumentNumber'}]
            }]
          }),

          assertFailure("ls_ietdn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{
              CategoryCode: '103',
              CategorySubCode: '11',
              ImportExport: [{TransportDocumentNumber: ''}]
            }]
          }),

          // field: "UCR",
          // If UCR contains a value and the Flow is IN, the minimum characters is 2 but not exceeding 35 characters and is in the following format: nNA12345678a...35a where n = last digit of the year NA = Fixed character 12345678 = Valid Customs Client Numbera = unique alpha numeric consiglsent number
          assertSuccess("ls_ieucr1", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'IN',
            MonetaryAmount: [{ImportExport: [{UCR: '2LS001234560500010596'}]}]
          }),
          assertWarning("ls_ieucr1", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'IN',
            MonetaryAmount: [{ImportExport: [{UCR: '2ZA001234560500010596'}]}]
          }),
          assertNoRule("ls_ieucr1", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{ImportExport: [{UCR: '2ZA001234560500010596'}]}]
          }),

          //Must be completed if the Flow is IN and the BoPCategory is 10101 to 10111 or 10301 to 10311 or 10500 or 10600
          assertSuccess("ls_ieucr2", {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            MonetaryAmount: [{
              CategoryCode: '103',
              CategorySubCode: '04',
              ImportExport: [{UCR: '3ZA12345678ABCDEFGHIJ'}]
            }]
          }),
          assertSuccess("ls_ieucr2", {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{UCR: '3ZA12345678ABCDEFGHIJ'}]}]
          }),
          assertFailure("ls_ieucr2", {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            MonetaryAmount: [{CategoryCode: '105', ImportExport: [{UCR: ''}]}]
          }),
          assertFailure("ls_ieucr2", {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            MonetaryAmount: [{
              CategoryCode: '103',
              CategorySubCode: '11',
              ImportExport: [{UCR: ''}]
            }]
          }),

          // For any BoPCategory other than 10101 to 10111 or 10301 to 10311 or 10500 or 10600, the UCR must not be completed. 
          assertNoRule("ls_ieucr3", {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            MonetaryAmount: [{
              CategoryCode: '103',
              CategorySubCode: '04',
              ImportExport: [{UCR: '3ZA12345678ABCDEFGHIJ'}]}]
          }),
          assertNoRule("ls_ieucr3", {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            MonetaryAmount: [{
              CategoryCode: '103',
              CategorySubCode: '11',
              ImportExport: [{UCR: '3ZA12345678ABCDEFGHIJ'}]}]
          }),
          assertSuccess("ls_ieucr3", {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            MonetaryAmount: [{
              CategoryCode: '401', 
              ImportExport: [{UCR: ''}]}]
          }),
          assertFailure("ls_ieucr3", {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            MonetaryAmount: [{
              CategoryCode: '401',
              ImportExport: [{UCR: '3ZA12345678ABCDEFGHIJ'}]}]
          }),

//================================================================================================================================================
//  # NOT FROM THIS CHANNEL (5/6)
//================================================================================================================================================
      // TotalForeignValue
          assertSuccess("tfv1", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'LSL', TotalForeignValue: '101.01',
            MonetaryAmount: [{DomesticValue: '50.00'}, {DomesticValue: '51.01'}]
          }),
          assertFailure("tfv1", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'LSL', TotalForeignValue: '101.01',
            MonetaryAmount: [{DomesticValue: '50.00'}, {DomesticValue: '49.99'}]
          }),
          assertSuccess("tfv1", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'LSL',
            MonetaryAmount: [{DomesticValue: '50.00'}, {DomesticValue: '49.99'}]
          }),

          assertSuccess("tfv2", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD', TotalForeignValue: '101.01',
            MonetaryAmount: [{ForeignValue: '50.00'}, {ForeignValue: '51.01'}]
          }),
          assertFailure("tfv2", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD', TotalForeignValue: '101.01',
            MonetaryAmount: [{ForeignValue: '50.00'}, {ForeignValue: '49.99'}]
          }),
          assertSuccess("tfv2", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD',
            MonetaryAmount: [{ForeignValue: '50.00'}, {ForeignValue: '49.99'}]
          }),

          assertSuccess("tfv3", {
            ReportingQualifier: 'BOPCARD NON RESIDENT', FlowCurrency: 'LSL', TotalForeignValue: '101.01',
            MonetaryAmount: [
              {ForeignCardHoldersPurchasesDomesticValue: '40.00', ForeignCardHoldersCashWithdrawalsDomesticValue: '10.00'},
              {ForeignCardHoldersCashWithdrawalsDomesticValue: '51.01'}
            ]
          }),
          assertFailure("tfv3", {
            ReportingQualifier: 'BOPCARD NON RESIDENT', FlowCurrency: 'LSL', TotalForeignValue: '101.01',
            MonetaryAmount: [{ForeignCardHoldersPurchasesDomesticValue: '50.00'}, {ForeignCardHoldersPurchasesDomesticValue: '49.99'}]
          }),
          assertSuccess("tfv3", {
            ReportingQualifier: 'BOPCARD NON RESIDENT', FlowCurrency: 'LSL',
            MonetaryAmount: [{ForeignCardHoldersPurchasesDomesticValue: '50.00'}, {ForeignCardHoldersPurchasesDomesticValue: '49.99'}]
          }),
          //The TotalForeignValue must be completed and must be greater than 0.00
          assertSuccess("tfv4", {
            ReportingQualifier: 'BOPCUS', TotalForeignValue: '101.01'
          }),
          assertFailure("tfv4", {
            ReportingQualifier: 'BOPCUS', TotalForeignValue: '0.00'
          }),
          assertFailure("tfv4", {
            ReportingQualifier: 'BOPCUS'
          }),
          assertNoRule("tfv4", {
            ReportingQualifier: 'BOPCARD NON RESIDENT', TotalForeignValue: '0.00'
          }),

          //NonResident.Individual.Gender on Category ZZ1
/*          assertFailure("ls_nrgn2", {
            ReportingQualifier: 'NON REPORTABLE',
            NonResident: {
              Individual: {
                Gender: ''
            }}
          }),
          assertSuccess("ls_nrgn2", {
            ReportingQualifier: 'NON REPORTABLE',
            NonResident: {
              Individual: {
                Gender: 'M'
            }}
          }),
          assertFailure("ls_nrgn2", {
            ReportingQualifier: 'NON REPORTABLE',
            NonResident: {Individual: {}}
          }),
*/
          assertNoRule("ls_nrgn2", {
            ReportingQualifier: 'NON REPORTABLE',
            NonResident: {Entity: {}}
          }),
          assertNoRule("ls_nrgn2", {
            ReportingQualifier: 'BOPCUS',
            NonResident: {Individual: {}}
          }),
          assertNoRule("ls_nrgn2", {
            ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {}}
          }),

          assertFailure("ls_fv1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              ForeignCurrencyCode: "LSL",
              DomesticCurrencyCode: 'LSL',
              ForeignValue: '20.00'
            }]
          }),
          assertSuccess("ls_fv1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              ForeignCurrencyCode: "LSL",
              DomesticCurrencyCode: 'LSL',
              ForeignValue: ''
            }]
          }),
          assertSuccess("ls_fv1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              ForeignCurrencyCode: "LSL",
              DomesticCurrencyCode: 'LSL'
            }]
          }),

          assertFailure("ls_fv2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              ForeignCurrencyCode: "USD",
              DomesticCurrencyCode: 'LSL',
              ForeignValue: ''
            }]
          }),
          assertFailure("ls_fv2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              ForeignCurrencyCode: "USD",
              DomesticCurrencyCode: 'LSL',
            }]
          }),
          assertSuccess("ls_fv2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              ForeignCurrencyCode: "USD",
              DomesticCurrencyCode: 'LSL',
              ForeignValue: '20.00'
            }]
          }),
          assertSuccess("ls_fv2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              ForeignCurrencyCode: "USD",
              DomesticCurrencyCode: 'USD',
              ForeignValue: '20.00'
            }]
          }),
          assertFailure("ls_fv2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              ForeignCurrencyCode: "USD",
              DomesticCurrencyCode: 'LSL',
              ForeignValue: '0.00'
            }]
          }),
 
//================================================================================================================================================
//  # Removed (6/6)
//================================================================================================================================================
          //NonResident.Entity.CardMerchantName
          // assertSuccess("b_nrle1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: {CardMerchantName: "My Card"}}}),
          // assertSuccess("b_nrle1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: {CardMerchantName: {}}}}),
          // assertFailure("b_nrle1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: {}}}),

          // //NonResident.Entity.LegalEntityName
          // assertSuccess("b_nrlen1", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {EntityName: 'Synthesis'}}}),
          // assertNoRule("b_nrlen1", {ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: {Entity: {EntityName: 'Synthesis'}}}),
          // assertSuccess("b_nrlen1", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {LegalName: {}}}}),
          // assertFailure("b_nrlen1", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {LegalName: "Test"}}}),

          // assertSuccess("b_nrlen1", {ReportingQualifier: "BOPCUS", Resident: {Entity: {ExceptionName: {}}}}),
          // assertFailure("b_nrlen1", {ReportingQualifier: "BOPCUS", Resident: {Entity: {ExceptionName: ""}}}),

          // Additional.NonResidentData.Country
          // assertSuccess("ls_nrictry1",{ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: {Individual: {Address: {}}}}),
          // assertFailure("ls_nrictry1",{ReportingQualifier: "BOPCARD NON RESIDENT", NonResident:{Individual: {Address:{Country: "South Africa"}}}}),

          //IndividualCustomer.IDNumber
          // assertFailure("ls_icid1",{ReportingQualifier: "BOPCUS", Resident: {Individual:{}}}),
          // assertFailure("ls_icid1",{ReportingQualifier: "BOPCUS", Resident: {Individual:{IDNumber: {}}}}),
          // assertSuccess("ls_icid1",{ReportingQualifier: "BOPCUS", Resident: {Individual:{IDNumber: "9202265116080"}}}),

          // assertFailure("ls_icid1",{ReportingQualifier: "BOPCARD RESIDENT", Resident: {Individual:{}}}),
          // assertFailure("ls_icid1",{ReportingQualifier: "BOPCARD RESIDENT", Resident: {Individual:{IDNumber: {}}}}),
          // assertSuccess("ls_icid1",{ReportingQualifier: "BOPCARD RESIDENT", Resident: {Individual:{IDNumber: "9202265116080"}}}),

          //AdditionalCustomerData.CustomsClientNumber
          // assertSuccess('b_ccn1:1', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: '1234'}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '101', CategorySubCode: '10'}]
          // }),
          // assertFailure('b_ccn1:1', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: ''}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '103', CategorySubCode: '10'}
          //   ]
          // }),
          // assertFailure('b_ccn1:1', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '103', CategorySubCode: '10'}
          //   ]
          // }),
          // assertNoRule('b_ccn1:1', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {CustomsClientNumber: '1234'}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'}
          //   ]
          // }),

          // assertSuccess('b_ccn1:2', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: '1234'}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '101', CategorySubCode: '10'}]
          // }),
          // assertFailure('b_ccn1:2', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: ''}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '103', CategorySubCode: '10'}
          //   ]
          // }),
          // assertFailure('b_ccn1:2', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '103', CategorySubCode: '10'}
          //   ]
          // }),
          // assertNoRule('b_ccn1:2', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {CustomsClientNumber: '1234'}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'}
          //   ]
          // }),
           // //Must be completed
          // assertFailure('ls_ncc1', {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   MonetaryAmount: [{LocalCurrency: 0.00}]
          // }),
          // assertFailure('ls_ncc1', {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   MonetaryAmount: [{}]
          // }),
          // assertFailure('ls_ncc1', {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   MonetaryAmount: [{LocalCurrency: {}}]
          // }),
          // assertFailure('ls_ncc1', {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   MonetaryAmount: [{LocalCurrency: ''}]
          // }),
          // Must not be completed.
          // assertSuccess("ls_ncc5", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LocalCurrency: ''}]}),
          // assertSuccess("ls_ncc5", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}]}),
          // assertFailure("ls_ncc5", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LocalCurrency: 'NAD'}]}),
        ]   
      }
    return testBase;
  }
})
