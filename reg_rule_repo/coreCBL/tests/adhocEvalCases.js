define(function() {
  return function(testBase) {
    var test_cases;
    with (testBase) {
      test_cases = [
        assertAssumptionEnt("CUS Pay Ent RESIDENT LOCAL_ACC paying USD to OTHRLSXXX (I am told CFC account)",
          "SBICLSMX", resStatus.RESIDENT, accType.LOCAL_ACC, null, null, "OTHRLSXXX", null, accType.CFC, "USD",
          { decision: dec.DoNotReport, reportingQualifier: "NON REPORTABLE", flow: flowDir.OUT, reportingSide: drcr.DR, nonResException: "CFC RESIDENT NON REPORTABLE", resSide: drcr.DR, resAccountType: at.RE_OTH }),
      ];
    }
    return testBase;
  }
})