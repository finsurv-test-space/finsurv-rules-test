/**
 * Created by petruspretorius on 24/03/2015.
 */
define(function () {
  var _export = {
    rootTemplate: 'SARB_Summary',
    tabRootTemplate:  'tabs/tabContainer',
    detail: {
      Transaction              : {
        scope: "transaction",
        detail: {
          section: "Transaction" ,
          detail : 'transaction/transaction'
        },
        summary : 'transaction_container',
        fields: ["TotalValue", "TrnReference", "Flow", "ValueDate", "FlowCurrency", "TotalForeignValue", "BranchCode", "BranchName", "HubCode", "HubName", "OriginatingBank", "OriginatingCountry",
          "CorrespondentBank", "CorrespondentCountry", "ReceivingBank", "ReceivingCountry", "ReportingQualifier", "ReplacementTransaction", "ReplacementOriginalReference", "Total{{LocalValue}}"]
      },
      Resident                 : {
        scope: "transaction",
        contextKey: 'Resident',
        detail    : {
          section   : "Resident",
          detail :'resident/',
          subSection: 'general'
        },
        summary   : 'Resident',
        fields    : [ "Resident.Individual.Surname", "Resident.Individual.Name", "Resident.Individual.Gender", "Resident.Individual.DateOfBirth", "Resident.Individual.IDNumber",
          "Resident.Individual.TempResPermitNumber", "Resident.Individual.ForeignIDNumber", "Resident.Individual.ForeignIDCountry", "Resident.Individual.PassportNumber",
          "Resident.Individual.PassportCountry", "Resident.Individual.CustomsClientNumber", "Resident.Individual.TaxNumber", "Resident.Individual.VATNumber",
          "Resident.Individual.TaxClearanceCertificateIndicator", "Resident.Individual.TaxClearanceCertificateReference", "Resident.Exception.ExceptionName",
          "Resident.Exception.Country", "Resident.Entity.EntityName", "Resident.Entity.TradingName", "Resident.Entity.RegistrationNumber",
          "Resident.Entity.InstitutionalSector", "Resident.Entity.IndustrialClassification", "Resident.Entity.CustomsClientNumber", "Resident.Entity.TaxNumber",
          "Resident.Entity.VATNumber", "Resident.Entity.TaxClearanceCertificateIndicator", "Resident.Entity.TaxClearanceCertificateReference",
          "Resident.Individual.BeneficiaryID1", "Resident.Individual.BeneficiaryID2", "Resident.Individual.BeneficiaryID3", "Resident.Individual.BeneficiaryID4"
            , "Resident.Individual.CCN", "Resident.Entity.CCN", "importUndertakingCCN", "Resident.Description" ]
      },
      ResidentAccount          : {
        scope: "transaction",
        contextKey: 'Resident',
        detail    : {
          section   : "Resident",
          detail :'resident/account',
          subSection: 'account'
        },
        summary   : 'Resident.Account',
        fields    : [ "AccountName", "AccountIdentifier", "AccountNumber", "CardNumber", "SupplementaryCardIndicator" ]
      },
      ResidentContact          : {
        scope: "transaction",
        contextKey: 'Resident',
        detail    : {
          section   : "Resident",
          detail :'resident/address',
          subSection: 'contact'
        },
        summary   : 'Resident.ContactDetails',
        fields    : [ "ContactDetails.ContactName", "ContactDetails.ContactSurname", "ContactDetails.Email", "ContactDetails.Fax", "ContactDetails.Telephone",
          "StreetAddress.AddressLine1", "StreetAddress.AddressLine2", "StreetAddress.Suburb", "StreetAddress.City", "StreetAddress.Province", "StreetAddress.PostalCode",
          "PostalAddress.AddressLine1", "PostalAddress.AddressLine2", "PostalAddress.Suburb", "PostalAddress.City", "PostalAddress.Province", "PostalAddress.PostalCode" ]
      },
      NonResident              : {
        scope: "transaction",
        contextKey: 'NonResident',
        detail    : {
          section   : "NonResident",
          detail :'nonResident/',
          subSection: 'general'
        },
        summary   : 'NonResident.Main',
        fields    : [ "NonResident.Individual.Surname", "NonResident.Individual.Name", "NonResident.Individual.Gender", "NonResident.Individual.PassportNumber",
          "NonResident.Individual.PassportCountry", "NonResident.Exception.ExceptionName", "NonResident.Entity.EntityName", "NonResident.Entity.CardMerchantName",
          "NonResident.Entity.CardMerchantCode" ]
      },
      NonResidentAccount       : {
        scope: "transaction",
        contextKey: 'NonResident',
        detail    : {
          section   : "NonResident",
          detail :'nonResident/account',
          subSection: 'account'
        },
        summary   : 'NonResident.Account',
        fields    : [ "AccountIdentifier", "AccountNumber" ]
      },
      NonResidentAddress       : {
        scope: "transaction",
        contextKey: 'NonResident',
        detail    : {
          section   : "NonResident",
          detail :'nonResident/address',
          subSection: 'contact'
        },
        summary   : 'NonResident.Address',
        fields    : [ "Address.AddressLine1", "Address.AddressLine2", "Address.Suburb", "Address.City", "Address.State", "Address.PostalCode", "Address.Country" ]
      },
      Monetary                 : {
        scope: "money",
        base: 'MonetaryAmount',
        detail : {
          section   : "Money",
          detail :'monetary/main',
          subSection: 'general'
        },
        summary: 'Description',
        fields: ["SequenceNumber", "MoneyTransferAgentIndicator", "RandValue", "ForeignValue", "ForeignCurrencyCode","CategoryCode", "CategorySubCode", "Description", "Category", "SWIFTDetails", "StrateRefNumber",
          "LoanTenor", "LoanInterestRate",
          "LoanInterest.BaseRate", "LoanInterest.Term", "LoanInterest.PlusMinus", "LoanInterest.Rate",
          "{{Regulator}}Auth.RulingsSection", "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber", "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate", "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
          "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber", "CannotCategorize", "AdHocRequirement.Subject", "AdHocRequirement.Description", "LocationCountry", "ReversalTrnRefNumber",
          "ReversalTrnSeqNumber", "BOPDIRTrnReference", "BOPDIRADCode", "CardChargeBack", "CardIndicator", "ElectronicCommerceIndicator", "POSEntryMode",
          "CardFraudulentTransactionIndicator", "ForeignCardHoldersPurchasesRandValue", "ForeignCardHoldersCashWithdrawalsRandValue", "Travel", "Travel.Surname",
          "Travel.Name", "Travel.IDNumber", "Travel.DateOfBirth", "Travel.TempResPermitNumber",
          "ThirdPartyKind", 'ThirdParty',
          "IE_UCRHeading", "IE_TDNHeading", "IE_ICNHeading", "IE_CCNHeading", "IE_MRNNotOnIVSHeading", "LoanRefNumber", "IECurrencyCode",
          "{{Regulator}}Auth.AuthFacilitator", "{{Regulator}}Auth.AuthIssuer", "{{Regulator}}Auth.{{RegulatorPrefix}}AuthDate", "{{LocalValue}}"]
      },
      MonetaryThirdParty       : {
        scope: "money",
        base: 'MonetaryAmount',
        detail : {
          section   : "Money",
          detail :'monetary/thirdparty',
          subSection: 'thirdparty'
        },
        summary: 'ThirdParty',
        fields : [ "ThirdParty.Individual.IDType", "ThirdParty.Entity.IDType", "ThirdParty.Individual.Surname", "ThirdParty.Individual.Name", "ThirdParty.Individual.Gender", "ThirdParty.Individual.IDNumber",
          "ThirdParty.Individual.DateOfBirth", "ThirdParty.Individual.TempResPermitNumber", "ThirdParty.Individual.PassportNumber", "ThirdParty.Individual.PassportCountry",
          "ThirdParty.Entity.Name", "ThirdParty.Entity.RegistrationNumber",
          "ThirdParty.CustomsClientNumber", "ThirdParty.TaxNumber",
          "ThirdParty.VATNumber", "ThirdParty.CustomLookup2" , "ThirdParty.sameAsPhysical" ]
      },
      MonetaryThirdPartyContact: {
        scope: "money",
        base: 'MonetaryAmount',
        detail : {
          section   : "Money",
          detail :'monetary/thirdparty_contact',
          subSection: 'contact'
        },
        summary: 'ThirdPartyContact',
        fields : [ "ThirdParty.ContactDetails.ContactSurname", "ThirdParty.ContactDetails.ContactName", "ThirdParty.ContactDetails.Email", "ThirdParty.ContactDetails.Fax",
          "ThirdParty.ContactDetails.Telephone", "ThirdParty.StreetAddress.AddressLine1", "ThirdParty.StreetAddress.AddressLine2", "ThirdParty.StreetAddress.Suburb",
          "ThirdParty.StreetAddress.City", "ThirdParty.StreetAddress.Province", "ThirdParty.StreetAddress.PostalCode", "ThirdParty.PostalAddress.AddressLine1",
          "ThirdParty.PostalAddress.AddressLine2", "ThirdParty.PostalAddress.Suburb", "ThirdParty.PostalAddress.City", "ThirdParty.PostalAddress.Province",
          "ThirdParty.PostalAddress.PostalCode", "ImportExportIs"]
      },
      ImportExport             : {
        scope  : "importexport",
        base: 'ImportExport',
        detail : {
          section   : "Money",
          detail :'monetary/importexport',
          subSection: 'importExport'
        },
        summary: 'ImportExport',
        fields: ["CustomsClientNumber", "ImportControlNumber", "TransportDocumentNumber", "MRNNotOnIVS", "UCR", "PaymentCurrencyCode", "PaymentValue"]
      }

    }
  }
  return _export;
});

