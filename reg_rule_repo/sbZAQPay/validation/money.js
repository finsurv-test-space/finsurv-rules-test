define(function () {
  return function (predef) {
    var stdMoney;
    with (predef) {

      stdMoney = {
        ruleset: "Standard Money Rules",
        scope: "money",
        validations: [
          {
            field: "ThirdParty.CustomsClientNumber",
            rules: [
              //ignore("mtpccn2")
            ]
          },
          {
            field: "{{Regulator}}Auth.AuthFacilitator",
            rules: [
              failure("auf1", 381, "Must be completed if the Flow is OUT and no data is supplied under RulingsSection",
                isEmpty.and(notMoneyField(map("{{Regulator}}Auth.RulingsSection"))))
                .onOutflow().onSection("ABG").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              failure("auf2", 387, "Must be completed if the RegistrationNumber is registered as an IHQ entity or the Subject is IHQnnn",
                isEmpty.and(evalTransactionField("Resident.Entity.RegistrationNumber", isInLookup('ihqCompanies', 'registrationNumber'))
                .or(evalMoneyField("AdHocRequirement.Subject", hasPattern("^IHQ\\d{3}$"))))).onSection("A"),
              failure("auf3", 386, "If the Flow is IN and the Subject is SETOFF, it must be completed",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SETOFF"))).onInflow().onSection("AB"),
              failure("auf4", 382, "May not be completed", notEmpty).onSection("CDEF"),
            ]
          },
          {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              ignore("maian1"),
              ignore("maian2"),
              failure("za_maian3", "IAN", "Must be completed if the AuthIssuer is not regulator",
                isEmpty.and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "THIS BANK")
                .or(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "OTHER BANK")))),
              failure("za_maian4", "IAN", "InternalAuthNumber needs to be 8 numbers",
                notEmpty.and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "THIS BANK")
                .or(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "OTHER BANK"))).and(notPattern(/^\d{8}$/))),

              validate("sb_arm1", "Verify_ARM", notEmpty.and(hasMoneyField(map("{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate")))
                .and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthFacilitator"), "THIS BANK")).and(hasPattern(/^\d{8}$/))).onSection("ABCDEFG")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              ignore("masan1"),
              ignore("masan2"),
              ignore("masan3"),
              ignore("masan4"),
              failure("za_masan5", "AAN", "Must be completed if the AuthIssuer is the regulator",
                isEmpty.and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "REGULATOR"))),
              failure("za_masan6", "AAN", "AuthAppNumber needs to be 8 numbers",
                notEmpty.and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "REGULATOR")).and(notPattern(/^\d{8}$/))),
              
                validate("sb_arm2", "Verify_ARM", notEmpty.and(hasMoneyField(map("{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber")))
                  .and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthFacilitator"), "THIS BANK")).and(hasPattern(/^\d{8}$/))).onSection("ABCDEFG")
            ]
          },
          {
            field: "{{Regulator}}Auth.AuthIssuer",
            rules: [
              failure("aui1", "RAI", "Must be completed if Auth Facilitator is complete",
                isEmpty.and(hasMoneyField(map("{{Regulator}}Auth.AuthFacilitator")))).onOutflow().onSection("ABG")
                .notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              failure("aui2", 387, "If the RegistrationNumber is registered as an IHQ entity or the Subject is IHQnnn, it must be completed with the regulator option",
                notEmpty.and(not(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "REGULATOR"))).and(evalTransactionField("Resident.Entity.RegistrationNumber", isInLookup('ihqCompanies', 'registrationNumber'))
                .or(evalMoneyField("AdHocRequirement.Subject", hasPattern("^IHQ\\d{3}$"))))).onSection("A"),
              failure("aui3", 386, "If the Flow is IN and the Subject is SETOFF, it must be completed with the regulator option",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SETOFF"))
                .and(not(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "REGULATOR")))).onInflow().onSection("AB"),
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthDate",
            rules: [
              failure("sad", "RAD", "Must be completed if the AuthIssuer is the regulator",
                isEmpty.and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "REGULATOR")).and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthFacilitator"), "THIS BANK"))),
            ]
          },
          {
            field: "LoanInterest.Rate",
            rules: [
              warning("lir1", "LIR", "Must be in the format DD.DD (between 0.00 and 99.99)",
              notEmpty.and(notPattern("^\\d{1,2}(\\.\\d{1,2})?$"))).onOutflow().onSection("ABG")
            ]
          }
        ]
      };


    }
    return stdMoney;
  }
});
