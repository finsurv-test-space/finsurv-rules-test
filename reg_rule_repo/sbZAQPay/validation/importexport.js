define(function() {
    return function(predef) {
        var _export;
        with(predef) {
            _export = {
                ruleset: "IVS sbZA Import/Export Rules",
                scope: "importexport",
                validations: [
                    {
                        field: "UCR",
                        rules: [
                            // Disable external validation of CCN in UCR...
                          ignore('ext_ieucr1')
                        ]
                    },
                    {
                    field: "IVSResponseCodes",
                    rules: [
                        validate("sb_ieivs1", "Validate_IVS",
                            evalIEField("ImportControlNumber", isValidICN)
                            .and(evalIEField("TransportDocumentNumber", isTooLong(1)))
                            .and(evalIEField("TransportDocumentNumber", isTooShort(36)))
                            // .and(isButtonPressed("IVSResponseCodes"))
                            .and(
                                evalTransactionField("Resident.Individual.CustomsClientNumber", hasPattern(/^\d{8,13}$/))
                                .or(
                                    evalTransactionField("Resident.Entity.CustomsClientNumber", hasPattern(/^\d{8,13}$/)))
                                .or(
                                    evalMoneyField("ThirdParty.CustomsClientNumber", hasPattern(/^\d{8,13}$/)))
                            )
                            .and(not(hasMoneyField("ThirdParty.CustomsClientNumber")
                            .and(evalMoneyField("ThirdParty.CustomsClientNumber", isInLookup('luClientCCNs', 'ccn'))))
                            .and(not(evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))))
                        )
                        .onOutflow()
                        .onSection("ABG")
                        .notOnCategory("103/11")
                        .onCategory(["103", "105", "106"])
                    ]
                }]
            };
        }

        return _export;
    }
});