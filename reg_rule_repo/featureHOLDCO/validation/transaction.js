define(function () {
  return function (predef) {
    var feature;
    with (predef) {

      feature = {
        ruleset: "HOLDCO Rules",
        scope: "transaction",
        validations: [
          {
            field: "NonResident.Entity.EntityName",
            rules: [
              failure("hc_nren1", "H01", "The Resident Legal Entity name is not equal to the name of HOLDCO in the HOLDCO list",
                notEmpty.and(evalTransactionField('NonResident.Entity.AccountNumber', isInLookup('holdcoCompanies', 'accountNumber'))).
                and(not(hasLookupTransactionFieldValue('holdcoCompanies', 'companyName', 'accountNumber', 'NonResident.Entity.AccountNumber'))).
                and(hasTransactionField("Resident.Individual").or(hasTransactionField("Resident.Entity")))).onSection("A"),
              failure("hc_nren2", "H01", "The Resident Legal Entity name is not equal to the name of HOLDCO in the HOLDCO list",
                notEmpty.and(evalTransactionField('NonResident.Entity.AccountNumber', isInLookup('holdcoCompanies', 'accountNumber'))).
                and(not(hasLookupTransactionFieldValue('holdcoCompanies', 'companyName', 'accountNumber', 'NonResident.Entity.AccountNumber'))).
                and(hasResException("FCA NON RESIDENT NON REPORTABLE"))).onSection("C")
            ]
          },
          {
            field: "NonResident.Entity.AccountIdentifier",
            rules: [
              failure("hc_nrea1", "H02", "Non Resident Account Identifier must be NON RESIDENT FCA for reporting of HOLDCO transactions",
                notValue("NON RESIDENT FCA").and(evalNonResidentField("AccountNumber", isInLookup('holdcoCompanies', 'accountNumber'))).
                and(hasTransactionField("Resident.Individual").or(hasTransactionField("Resident.Entity")))).onSection("A"),
              failure("hc_nrea2", "H02", "Non Resident Account Identifier must be NON RESIDENT FCA for reporting of HOLDCO transactions",
                notValue("NON RESIDENT FCA").and(evalNonResidentField("AccountNumber", isInLookup('holdcoCompanies', 'accountNumber'))).
                and(hasResException("FCA NON RESIDENT NON REPORTABLE"))).onSection("C")
            ]
          },
          {
            field: "Resident.Individual.AccountIdentifier",
            rules: [
              failure("hc_accid1", "H03", "Resident Account Identifier must be FCA RESIDENT for reporting of HOLDCO transactions",
                notValue("FCA RESIDENT").and(evalNonResidentField("AccountNumber", isInLookup('holdcoCompanies', 'accountNumber')))).onSection("A")
            ]
          },
          {
            field: "Resident.Entity.AccountIdentifier",
            rules: [
              failure("hc_accid2", "H03", "Resident Account Identifier must be FCA RESIDENT for reporting of HOLDCO transactions",
                notValue("FCA RESIDENT").and(evalNonResidentField("AccountNumber", isInLookup('holdcoCompanies', 'accountNumber')))).onSection("A")
            ]
          },
          {
            field: "Resident.Exception.Country",
            rules: [
              failure("hc_rec1", "H04", "Country must be linked to the currency used when reporting HOLDCO transactions (EU must be used for EUR payments)",
                notMatchToCurrency.and(evalNonResidentField("AccountNumber", isInLookup('holdcoCompanies', 'accountNumber'))).
                and(hasResException("FCA NON RESIDENT NON REPORTABLE"))).onSection("C")
            ]
          },
          {
            field: "NonResident.Individual.AccountIdentifier",
            rules: [
              failure("hc_nraid1", "H05", "Non Resident Account Identifier must be 'NON RESIDENT RAND' for reporting of HOLDCO transactions",
                notEmpty.and(notValue("NON RESIDENT RAND")).
                and(hasAnyMoneyFieldValue("AdHocRequirement.Subject", "HOLDCO")).
                and(isCurrencyIn("ZAR")).and(hasResException("NON RESIDENT RAND")).
                and(not(evalNonResidentField("AccountNumber", isInLookup('holdcoCompanies', 'accountNumber'))))).onSection("A")
            ]
          },
          {
            field: "NonResident.Entity.AccountIdentifier",
            rules: [
              failure("hc_nraid2", "H05", "Non Resident Account Identifier must be 'NON RESIDENT RAND' for reporting of HOLDCO transactions",
                notEmpty.and(notValue("NON RESIDENT RAND")).
                and(hasAnyMoneyFieldValue("AdHocRequirement.Subject", "HOLDCO")).
                and(isCurrencyIn("ZAR")).and(hasResException("NON RESIDENT RAND")).
                and(not(evalNonResidentField("AccountNumber", isInLookup('holdcoCompanies', 'accountNumber'))))).onSection("A")
            ]
          }
        ]
      };
   }
    return feature;
  }
});
