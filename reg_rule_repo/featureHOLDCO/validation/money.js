define(function () {
  return function (predef) {
    var extMoney;
    with (predef) {

      extMoney = {
        ruleset: "Money Rules for HOLDCO Accounts",
        scope: "money",
        validations: [
          {
            field: "AdHocRequirement.Subject",
            rules: [
              failure("hc_madhs1", "H06", "Subject must contain HOLDCO when reporting transactions for HOLDCO companies",
                evalTransactionField('NonResident.Entity.AccountNumber', isInLookup('holdcoCompanies', 'accountNumber')).
                and(hasTransactionField("Resident.Individual").or(hasTransactionField("Resident.Entity"))).
                and(isEmpty.or(notValue("HOLDCO")))).onSection("A"),
              failure("hc_madhs2", "H07", "Resident Exception name must be 'NON RESIDENT RAND' for reporting of ZAR related HOLDCO transactions",
                hasValue("HOLDCO").and(notResException("NON RESIDENT RAND")).
                and(isCurrencyIn("ZAR")).
                and(not(evalNonResidentField("AccountNumber", isInLookup('holdcoCompanies', 'accountNumber'))))).onSection("A")
            ]
          },
          {
            field: "AdHocRequirement.Description",
            rules: [
              failure("hc_madhd1", "H08", "Description must contain the registration number of the HOLDCO as listed in the HOLDCO table",
                evalTransactionField('NonResident.Entity.AccountNumber', isInLookup('holdcoCompanies', 'accountNumber')).
                and(hasTransactionField("Resident.Individual").or(hasTransactionField("Resident.Entity"))).
                and(isEmpty.or(not(hasLookupTransactionFieldValue('holdcoCompanies', 'registrationNumber', 'accountNumber', 'NonResident.Entity.AccountNumber'))))).onSection("A")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
            rules: [
              failure("hc_maian1", "H09", "{{DealerPrefix}}Internal Auth Number is mandatory for reporting of a HOLDCO transaction",
                evalTransactionField('NonResident.Entity.AccountNumber', isInLookup('holdcoCompanies', 'accountNumber')).
                and(hasTransactionField("Resident.Individual").or(hasTransactionField("Resident.Entity"))).
                and(isEmpty)).onSection("A")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
            rules: [
              failure("hc_masan1", "H10", "{{RegulatorPrefix}}AuthAppNumber is mandatory for reporting of a HOLDCO transaction",
                evalTransactionField('NonResident.Entity.AccountNumber', isInLookup('holdcoCompanies', 'accountNumber')).
                and(hasTransactionField("Resident.Individual").or(hasTransactionField("Resident.Entity"))).
                and(isEmpty)).onSection("A")
            ]
          }
        ]
      };

    }
    return extMoney;
  }
});
