define({
  ReportingQualifier           : ["BOPCUS", "NON REPORTABLE", "BOPCARD RESIDENT", "BOPCARD NON RESIDENT", "NON RESIDENT RAND", "INTERBANK", "BOPDIR"],
  holdcoCompanies : [
    {
      accountNumber : "1234567",
      registrationNumber : "2013/1234567/07",
      companyName : "holdco1"
    },
    {
      accountNumber : "1234568",
      registrationNumber : "2013/1234568/07",
      companyName : "holdco2"
    }
  ]
})