define(function() {
  return function(testBase) {
      with (testBase) {
        var mappings = {
          Regulator: "SARB",
          DealerPrefix: "AD",
          RegulatorPrefix: "SARB"
        };
        
        setMappings(mappings, true);

        var test_cases = [
          //HOLDCO Tests as per the HOLDCO table
          // The Resident Legal Entity name is not equal to the name of HOLDCO in the HOLDCO list
          assertSuccess("hc_nren1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {EntityName: "holdco1", AccountNumber: "1234567"}},
            Resident: {Individual: {}}
          }),
          assertSuccess("hc_nren1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {EntityName: "holdco1", AccountNumber: "1234567"}},
            Resident: {Entity: {}}
          }),
          assertSuccess("hc_nren1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {EntityName: "NotHOLDCO", AccountNumber: "NotHOLDCOAcc"}},
            Resident: {Individual: {}}
          }),
          assertFailure("hc_nren1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {EntityName: "NotHOLDCO", AccountNumber: "1234567"}},
            Resident: {Individual: {}}
          }),
          assertSuccess("hc_nren1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {EntityName: "NotHOLDCO", AccountNumber: "1234567"}},
            Resident: {Exception: {}}
          }),

          assertSuccess("hc_nren2", {ReportingQualifier: 'NON REPORTABLE',
            NonResident: {Entity: {EntityName: "holdco1", AccountNumber: "1234567"}},
            Resident: {Exception: {ExceptionName: "FCA NON RESIDENT NON REPORTABLE"}}
          }),
          assertSuccess("hc_nren2", {ReportingQualifier: 'NON REPORTABLE',
            NonResident: {Entity: {EntityName: "NotHOLDCO", AccountNumber: "NotHOLDCOAcc"}},
            Resident: {Exception: {ExceptionName: "FCA NON RESIDENT NON REPORTABLE"}}
          }),
          assertFailure("hc_nren2", {ReportingQualifier: 'NON REPORTABLE',
            NonResident: {Entity: {EntityName: "NotHOLDCO", AccountNumber: "1234567"}},
            Resident: {Exception: {ExceptionName: "FCA NON RESIDENT NON REPORTABLE"}}
          }),
          assertSuccess("hc_nren2", {ReportingQualifier: 'NON REPORTABLE',
            NonResident: {Entity: {EntityName: "NotHOLDCO", AccountNumber: "1234567"}},
            Resident: {Exception: {ExceptionName: "Other Exception"}}
          }),

          // Non Resident Account Identifier must be NON RESIDENT FCA for reporting of HOLDCO transactions
          assertSuccess("hc_nrea1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "1234567", AccountIdentifier: "NON RESIDENT FCA"}},
            Resident: {Individual: {}}
          }),
          assertSuccess("hc_nrea1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "1234567", AccountIdentifier: "NON RESIDENT FCA"}},
            Resident: {Entity: {}}
          }),
          assertFailure("hc_nrea1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "1234567", AccountIdentifier: "FCA RESIDENT"}},
            Resident: {Individual: {}}
          }),
          assertSuccess("hc_nrea1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "NotHOLDCOAcc", AccountIdentifier: "FCA RESIDENT"}},
            Resident: {Individual: {}}
          }),
          assertSuccess("hc_nrea1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "1234567", AccountIdentifier: "FCA RESIDENT"}},
            Resident: {Exception: {}}
          }),

          // Non Resident Account Identifier must be NON RESIDENT FCA for reporting of HOLDCO transactions
          assertSuccess("hc_nrea2", {ReportingQualifier: 'NON REPORTABLE',
            NonResident: {Entity: {AccountNumber: "1234567", AccountIdentifier: "NON RESIDENT FCA"}},
            Resident: {Exception: {ExceptionName: "FCA NON RESIDENT NON REPORTABLE"}}
          }),
          assertFailure("hc_nrea2", {ReportingQualifier: 'NON REPORTABLE',
            NonResident: {Entity: {AccountNumber: "1234567", AccountIdentifier: "Not NON RESIDENT FCA"}},
            Resident: {Exception: {ExceptionName: "FCA NON RESIDENT NON REPORTABLE"}}
          }),
          assertSuccess("hc_nrea2", {ReportingQualifier: 'NON REPORTABLE',
            NonResident: {Entity: {AccountNumber: "1234567", AccountIdentifier: "Not NON RESIDENT FCA"}},
            Resident: {Exception: {ExceptionName: "Other Exception"}}
          }),
          assertSuccess("hc_nrea2", {ReportingQualifier: 'NON REPORTABLE',
            NonResident: {Entity: {AccountNumber: "NotHOLDCO Acc", AccountIdentifier: "Not NON RESIDENT FCA"}},
            Resident: {Exception: {ExceptionName: "FCA NON RESIDENT NON REPORTABLE"}}
          }),

          // Resident.Exception.Country
          // Country must be linked to the currency used when reporting HOLDCO transactions (EU must be used for EUR payments)
          assertSuccess("hc_rec1", {ReportingQualifier: 'NON REPORTABLE', FlowCurrency: 'USD',
            NonResident: {Entity: {AccountNumber: "1234567"}},
            Resident: {Exception: {ExceptionName: "FCA NON RESIDENT NON REPORTABLE", Country: "US"}}
          }),
          assertFailure("hc_rec1", {ReportingQualifier: 'NON REPORTABLE', FlowCurrency: 'USD',
            NonResident: {Entity: {AccountNumber: "1234567"}},
            Resident: {Exception: {ExceptionName: "FCA NON RESIDENT NON REPORTABLE", Country: "ZA"}}
          }),
          assertSuccess("hc_rec1", {ReportingQualifier: 'NON REPORTABLE', FlowCurrency: 'USD',
            NonResident: {Entity: {AccountNumber: "NotHOLDCO Acc"}},
            Resident: {Exception: {ExceptionName: "FCA NON RESIDENT NON REPORTABLE", Country: "ZA"}}
          }),
          assertSuccess("hc_rec1", {ReportingQualifier: 'NON REPORTABLE', FlowCurrency: 'USD',
            NonResident: {Entity: {AccountNumber: "1234567"}},
            Resident: {Exception: {ExceptionName: "Other Exception", Country: "ZA"}}
          }),


          // SUBJECT must contain HOLDCO when reporting transactions for HOLDCO companies
          assertSuccess("hc_madhs1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "1234567"}},
            Resident: {Individual: {}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'HOLDCO'}}]
          }),
          assertSuccess("hc_madhs1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "1234567"}},
            Resident: {Entity: {}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'HOLDCO'}}]
          }),
          assertFailure("hc_madhs1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "1234567"}},
            Resident: {Individual: {}},
            MonetaryAmount: [{}]
          }),
          assertFailure("hc_madhs1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "1234567"}},
            Resident: {Individual: {}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'SDA'}}]
          }),
          assertSuccess("hc_madhs1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "NotHOLDCOAcc"}},
            Resident: {Individual: {}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'SDA'}}]
          }),

          //AdHocRequirement.Subject: Resident Exception name must be 'NON RESIDENT RAND' for reporting of ZAR related HOLDCO transactions
          assertSuccess("hc_madhs2", {ReportingQualifier: 'BOPCUS', FlowCurrency: "ZAR",
            NonResident: {Entity: {AccountNumber: "NotHOLDCOAcc"}},
            Resident: {Exception: {ExceptionName: "NON RESIDENT RAND"}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'HOLDCO'}}]
          }),
          assertSuccess("hc_madhs2", {ReportingQualifier: 'BOPCUS', FlowCurrency: "ZAR",
            NonResident: {Entity: {AccountNumber: "1234567"}},
            Resident: {Exception: {ExceptionName: "Other Exception"}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'HOLDCO'}}]
          }),
          assertFailure("hc_madhs2", {ReportingQualifier: 'BOPCUS', FlowCurrency: "ZAR",
            NonResident: {Entity: {AccountNumber: "NotHOLDCOAcc"}},
            Resident: {Exception: {ExceptionName: "Other Exception"}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'HOLDCO'}}]
          }),
          assertSuccess("hc_madhs2", {ReportingQualifier: 'BOPCUS', FlowCurrency: "USD",
            NonResident: {Entity: {AccountNumber: "NotHOLDCOAcc"}},
            Resident: {Exception: {ExceptionName: "Other Exception"}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'HOLDCO'}}]
          }),
          assertSuccess("hc_madhs2", {ReportingQualifier: 'BOPCUS', FlowCurrency: "ZAR",
            NonResident: {Entity: {AccountNumber: "NotHOLDCOAcc"}},
            Resident: {Exception: {ExceptionName: "Other Exception"}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'Not HOLDCO'}}]
          }),

          // Description must contain the registration number of the HOLDCO as listed in the HOLDCO table
           assertSuccess("hc_madhd1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Individual: {}},
             MonetaryAmount: [{AdHocRequirement: {Description: '2013/1234567/07'}}]
           }),
           assertSuccess("hc_madhd1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Entity: {}},
             MonetaryAmount: [{AdHocRequirement: {Description: '2013/1234567/07'}}]
           }),
           assertFailure("hc_madhd1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Individual: {}},
             MonetaryAmount: [{}]
           }),
           assertFailure("hc_madhd1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Individual: {}},
             MonetaryAmount: [{AdHocRequirement: {Description: 'Not valid Reg'}}]
           }),
           assertSuccess("hc_madhd1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "NotHOLDCOAcc"}},
             Resident: {Individual: {}},
             MonetaryAmount: [{AdHocRequirement: {Description: 'Not valid Reg'}}]
           }),

          // ADInternal Auth Number is mandatory for reporting of a HOLDCO transaction
           assertSuccess("hc_maian1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Individual: {}},
             MonetaryAmount: [{SARBAuth: {ADInternalAuthNumber: 'AppNo1'}}]
           }),
           assertSuccess("hc_maian1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Entity: {}},
             MonetaryAmount: [{SARBAuth: {ADInternalAuthNumber: 'AppNo1'}}]
           }),
           assertFailure("hc_maian1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Individual: {}},
             MonetaryAmount: [{}]
           }),
           assertFailure("hc_maian1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Individual: {}},
             MonetaryAmount: [{SARBAuth: {ADInternalAuthNumber: ''}}]
           }),
           assertSuccess("hc_maian1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "NotHOLDCOAcc"}},
             Resident: {Individual: {}},
             MonetaryAmount: [{SARBAuth: {ADInternalAuthNumber: ''}}]
           }),
           assertSuccess("hc_maian1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Exception: {}},
             MonetaryAmount: [{SARBAuth: {ADInternalAuthNumber: ''}}]
           }),

          // SARB Auth App Number is mandatory for reporting of a HOLDCO transaction
           assertSuccess("hc_masan1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Individual: {}},
             MonetaryAmount: [{SARBAuth: {SARBAuthAppNumber: 'AppNo1'}}]
           }),
           assertSuccess("hc_masan1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Entity: {}},
             MonetaryAmount: [{SARBAuth: {SARBAuthAppNumber: 'AppNo1'}}]
           }),
           assertFailure("hc_masan1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Individual: {}},
             MonetaryAmount: [{}]
           }),
           assertFailure("hc_masan1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Individual: {}},
             MonetaryAmount: [{SARBAuth: {SARBAuthAppNumber: ''}}]
           }),
           assertSuccess("hc_masan1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "NotHOLDCOAcc"}},
             Resident: {Individual: {}},
             MonetaryAmount: [{SARBAuth: {SARBAuthAppNumber: ''}}]
           }),
           assertSuccess("hc_masan1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Exception: {}},
             MonetaryAmount: [{SARBAuth: {SARBAuthAppNumber: ''}}]
           }),

          // Resident Account Identifier must be FCA RESIDENT for reporting of HOLDCO transactions
           assertSuccess("hc_accid1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Individual: {AccountIdentifier: "FCA RESIDENT"}}
           }),
           assertSuccess("hc_accid2", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Entity: {AccountIdentifier: "FCA RESIDENT"}}
           }),
           assertFailure("hc_accid1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Individual: {AccountIdentifier: "Not FCA"}}
           }),
           assertFailure("hc_accid2", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "1234567"}},
             Resident: {Entity: {AccountIdentifier: "Not FCA"}}
           }),
           assertSuccess("hc_accid1", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "NotHOLDCOAcc"}},
             Resident: {Individual: {AccountIdentifier: "Not FCA"}}
           }),
           assertSuccess("hc_accid2", {ReportingQualifier: 'BOPCUS',
             NonResident: {Entity: {AccountNumber: "NotHOLDCOAcc"}},
             Resident: {Entity: {AccountIdentifier: "Not FCA"}}
           }),

          // NonResident.Individual.AccountIdentifier: Non Resident Account Identifier must be 'NON RESIDENT RAND' for reporting of HOLDCO transactions
           assertSuccess("hc_nraid1", {ReportingQualifier: 'BOPCUS', FlowCurrency: "ZAR",
             NonResident: {Individual: {AccountIdentifier: "NON RESIDENT RAND", AccountNumber: "NotHOLDCOAcc"}},
             Resident: {Exception: {ExceptionName: "NON RESIDENT RAND"}},
             MonetaryAmount: [{AdHocRequirement: {Subject: 'HOLDCO'}}]
           }),
           assertFailure("hc_nraid1", {ReportingQualifier: 'BOPCUS', FlowCurrency: "ZAR",
             NonResident: {Individual: {AccountIdentifier: "Other AccType", AccountNumber: "NotHOLDCOAcc"}},
             Resident: {Exception: {ExceptionName: "NON RESIDENT RAND"}},
             MonetaryAmount: [{AdHocRequirement: {Subject: 'HOLDCO'}}]
           }),
           assertSuccess("hc_nraid1", {ReportingQualifier: 'BOPCUS', FlowCurrency: "ZAR",
             NonResident: {Individual: {AccountIdentifier: "Other AccType", AccountNumber: "1234567"}},
             Resident: {Exception: {ExceptionName: "NON RESIDENT RAND"}},
             MonetaryAmount: [{AdHocRequirement: {Subject: 'HOLDCO'}}]
           }),
           assertSuccess("hc_nraid1", {ReportingQualifier: 'BOPCUS', FlowCurrency: "ZAR",
             NonResident: {Individual: {AccountIdentifier: "Other AccType", AccountNumber: "NotHOLDCOAcc"}},
             Resident: {Exception: {ExceptionName: "NON RESIDENT RAND"}},
             MonetaryAmount: [{AdHocRequirement: {Subject: 'Not HOLDCO'}}]
           }),
           assertSuccess("hc_nraid1", {ReportingQualifier: 'BOPCUS', FlowCurrency: "ZAR",
             NonResident: {Individual: {AccountIdentifier: "Other AccType", AccountNumber: "NotHOLDCOAcc"}},
             Resident: {Exception: {ExceptionName: "Other Exception"}},
             MonetaryAmount: [{AdHocRequirement: {Subject: 'Not HOLDCO'}}]
           }),

          // NonResident.Entity.AccountIdentifier: Non Resident Account Identifier must be 'NON RESIDENT RAND' for reporting of HOLDCO transactions
           assertSuccess("hc_nraid2", {ReportingQualifier: 'BOPCUS', FlowCurrency: "ZAR",
             NonResident: {Entity: {AccountIdentifier: "NON RESIDENT RAND", AccountNumber: "NotHOLDCOAcc"}},
             Resident: {Exception: {ExceptionName: "NON RESIDENT RAND"}},
             MonetaryAmount: [{AdHocRequirement: {Subject: 'HOLDCO'}}]
           }),
           assertFailure("hc_nraid2", {ReportingQualifier: 'BOPCUS', FlowCurrency: "ZAR",
             NonResident: {Entity: {AccountIdentifier: "Other AccType", AccountNumber: "NotHOLDCOAcc"}},
             Resident: {Exception: {ExceptionName: "NON RESIDENT RAND"}},
             MonetaryAmount: [{AdHocRequirement: {Subject: 'HOLDCO'}}]
           }),
           assertSuccess("hc_nraid2", {ReportingQualifier: 'BOPCUS', FlowCurrency: "ZAR",
             NonResident: {Entity: {AccountIdentifier: "Other AccType", AccountNumber: "1234567"}},
             Resident: {Exception: {ExceptionName: "NON RESIDENT RAND"}},
             MonetaryAmount: [{AdHocRequirement: {Subject: 'HOLDCO'}}]
           }),
           assertSuccess("hc_nraid2", {ReportingQualifier: 'BOPCUS', FlowCurrency: "ZAR",
             NonResident: {Entity: {AccountIdentifier: "Other AccType", AccountNumber: "NotHOLDCOAcc"}},
             Resident: {Exception: {ExceptionName: "NON RESIDENT RAND"}},
             MonetaryAmount: [{AdHocRequirement: {Subject: 'Not HOLDCO'}}]
           }),
           assertSuccess("hc_nraid2", {ReportingQualifier: 'BOPCUS', FlowCurrency: "ZAR",
             NonResident: {Entity: {AccountIdentifier: "Other AccType", AccountNumber: "NotHOLDCOAcc"}},
             Resident: {Exception: {ExceptionName: "Other Exception"}},
             MonetaryAmount: [{AdHocRequirement: {Subject: 'Not HOLDCO'}}]
           })
        ]

      }
    return testBase;
  }
})
