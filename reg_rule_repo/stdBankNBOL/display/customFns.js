define(function (require) {
    return function (app, config) {

        var superInit = function () { };

        if (config.initializationFn) superInit = config.initializationFn;

        var _config = {
            initializationFn: function (model, scope) {
                superInit(model, scope);
                //custom scope functions go here...
                scope.totalRandValue = function () {
                    var money = scope.data.MonetaryAmount;
                    var total = _.reduce(money, function (memo, itm) {
                        memo += itm[scope.map('LocalValue')].val ? Number(itm[scope.map('LocalValue')].val) : 0;
                        return memo;
                    }, 0)
                    return total.toFixed(2);
                }

                scope.randOutstanding = function (data, overaCopy, underaCopy) {
                    var total = scope.customData.TotalDomesticAmount - scope.totalRandValue();
                    if (total) return total.toFixed(2);
                }

                scope.updateBranches = function (model, selectedOption) {
                    var searchItem = model.val;
                    var branch = _.find(scope.getLookups().branches, function (item) {
                        return !!_.find(item, function (val) {
                            return val == searchItem;
                        })
                    });
                    if (branch) {
                        model.data.BranchCode = branch.code;
                        model.data.BranchName = branch.name;
                        model.data.HubCode = branch.hubCode;
                        model.data.HubName = branch.hubName;
                        scope.revalidate();
                    }
                }


                scope.checkThirdPartyPostalAddExist = function (ThirdParty) {

                    if (ThirdParty.PostalAddress.AddressLine1.val != undefined) {
                        if (ThirdParty.sameAsPhysical.val == true) {
                            var r = confirm("Warning! Overwrite Postal Address data?");
                            if (r == false) {
                                ThirdParty.sameAsPhysical.val = false;
                            }
                        } else {
                            delete ThirdParty.data.ThirdParty.PostalAddress;
                        }
                    }
                    scope.revalidate();

                };


                scope.clearThirdParty = function (ThirdParty) {
                    delete ThirdParty.data.ThirdParty;
                    delete ThirdParty.data.ThirdPartyKind;
                    scope.revalidate();
                };

                var ThirdPartyCache = {};

                // This is used to set the 3rd Party type - Maintains a cache per monetaryAmount
                scope.setAdhoc3rdPartyType = function (_model, selectedOption) {

                    var moneyIndex = _model.getIndeces()[0];
                    var currentType = _model.data.ThirdParty ? (_model.data.ThirdParty.Individual ? "Individual" :
                        (_model.data.ThirdParty.Entity ? "Entity" : undefined)) : undefined;


                    var cache = ThirdPartyCache[moneyIndex] ?
                        (ThirdPartyCache[moneyIndex]) :
                        (ThirdPartyCache[moneyIndex] = {});

                    if (currentType) {
                        cache.core = _model.data.ThirdParty;
                        cache[currentType] = cache[currentType] ? cache[currentType] :
                            cache[currentType] = _model.data.ThirdParty[currentType];
                    };

                    if (selectedOption == undefined) {
                        delete _model.data.ThirdParty;
                        delete _model.data.ThirdPartyKind;
                    } else {
                        _model.data.ThirdParty = cache.core ? cache.core : {};
                        delete _model.data.ThirdParty.Individual;
                        delete _model.data.ThirdParty.Entity;
                        _model.data.ThirdParty[selectedOption.value] = cache[selectedOption.value] ?
                            cache[selectedOption.value] :
                            cache[selectedOption.value] = {};
                    }
                }

                //when a 3rd party select is triggered...
                scope.setAdhoc3rdParty = function (_model, selectedOption) {
                    debugger;
                    
                    var moneyIndex = _model.getIndeces()[0];
                    console.log("ixd:" + moneyIndex);
                    if (selectedOption && selectedOption.label == '*** Add New ***') {
                    _model.data.ThirdParty = {Individual: {}};
                    model.MonetaryAmount[moneyIndex].allowAdhocBOPThirdParty.val = true;
                    } else {
                    model.MonetaryAmount[moneyIndex].allowAdhocBOPThirdParty.val = false;
                    }
        
                    //set the money.ThirdPartyKind
                    model.MonetaryAmount[moneyIndex].ThirdPartyKind.val
                        = _.isUndefined(_model.data.ThirdParty.Individual) ?
                        "Entity" : "Individual";
                    };
            }
        }

        Object.assign(config,_config);

        return config;

    }
})