/**
 * Created by petruspretorius on 06/01/2016.
 */
define(function () {

  function filterThirdPartyIndividual(value) {
    if (value == '-') {
      return true;
    }
    return (typeof value.Individual !== 'undefined');
  }

  return function (predef) {

    var filterLookupRules;

    with (predef) {
      filterLookupRules = {
        filterLookupTrans: {
          ruleset: "Reporting Transaction Lookup Filter Rules (NBOL)",
          scope: "transaction",
          fields: []
        },

        filterLookupMoney: {
          ruleset: "Reporting Monetary Lookup Filter Rules",
          scope: "money",
          fields: [
            {
              field: 'ThirdParty',
              display: [
                excludeValue(['-'], not(
                  hasCustomValue("allowAdhocBOPThirdPartyCapture", "true")
                    .or(hasCustomValue("allowAdhocBOPThirdPartyCapture", true)))),

                // "If CategoryCode 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, Individual must be completed",
                filterValue(filterThirdPartyIndividual, hasTransactionField("Resident.Entity")).onOutflow().onSection("AB").onCategory(["512", "513"]),

                // "If the category is 256 and the PassportNumber under Resident Individual contains no value, Individual must be completed",
                filterValue(filterThirdPartyIndividual, notTransactionField("Resident.Individual.PassportNumber")).onSection("AB").onCategory("256"),

                // "If the category is 255 or 256 and the Resident Entity element is completed, Individual must be completed",
                filterValue(filterThirdPartyIndividual, hasTransactionField("Resident.Entity")).onSection("AB").onCategory(["255", "256"]),

                // "If the SupplementaryCardIndicator is Y, Individual must be completed"
                filterValue(filterThirdPartyIndividual, hasResidentFieldValue("SupplementaryCardIndicator", "Y")).onSection("E"),

                // "If the Flow is IN and category 303, 304, 305, 306, 416 or 417 is used and Resident Entity is completed, Individual must be completed",
                filterValue(filterThirdPartyIndividual, hasTransactionField("Resident.Entity")).onInflow().onSection("AB").onCategory(["303", "304", "305", "306", "416", "417"]),

                // "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, Individual must be completed",
                filterValue(filterThirdPartyIndividual, hasTransactionField("Resident.Entity")).onInflow().onSection("A").onCategory(["511", "516"])
              ]
            },
            {
              field: "CompoundCategoryCode",
              display: [
                limitValue(['513'], hasNonResidentFieldValue("AccountIdentifier", "FCA RESIDENT")).onOutflow().onSection("A"),
                limitValue(['517'], hasNonResidentFieldValue("AccountIdentifier", "FCA RESIDENT")).onInflow().onSection("A"),
                //#dummyData 18
                excludeValue(['102,104'],
                  notImportUndertakingClient
                    .and(notCustomValue("ignoreLUClient", true)))
                  .onOutflow().onSection("ABG"),

                //#dummyData 19
                //limitValue(['102,104,105,106'],
                //  importUndertakingClient
                //    .and(not(anyMoneyCustomValueDispensationHasNbol("category", "271")))
                //).onOutflow().onSection("ABG"),
                //limitValue(['102,104,105,106,271'],
                //  importUndertakingClient
                //    .and(anyMoneyCustomValueDispensationHasNbol("category", "271"))
                //).onOutflow().onSection("ABG"),
                //limitValue(['105,106'], importUndertakingClient.and(hasAnyMoneyFieldValue("CategoryCode", "105").or(hasAnyMoneyFieldValue("CategoryCode", "106")))),
                //limitValue(['102,104'], importUndertakingClient.and(hasAnyMoneyFieldValue("CategoryCode", "102").or(hasAnyMoneyFieldValue("CategoryCode", "104")))),

                //#dummyData 14
                limitValue(['101,102,103,104,105,106'], hasCustomValue("tradeOnly", true)).onOutflow().onSection("ABG"),
                limitValue(['ZZ1']).onSection("C"),
                excludeValue(['ZZ1', 'ZZ2']).onSection("ABDEF"),
                limitValue(['833'], hasCustomValue('ADLA', true))
                , excludeValue(['833'], notCustomValue('ADLA', true).and(not(hasCustomField("MoneyTransferAgentIndicator")))).onSection("AB")
              ]
            },
            {
              field: "MoneyTransferAgentIndicator",
              display: [
                limitValue(["CARD"]).onSection("EF"),
                limitValue(["BOPDIR"]).onSection("G"),
                limitValue(["ADLA"], dealerTypeADLA).onSection("AB"),
                excludeValue(["AD", "ADLA", "CARD", "BOPDIR"], dealerTypeAD).onSection("ABCD").onCategory("833"),
                excludeValue(["ADLA", "CARD", "BOPDIR"], dealerTypeAD).onSection("ABCD").notOnCategory("833"),
                excludeValue(["ADLA", "AD"], hasCustomValue('ADLA', true)).onSection("ABCD").onCategory("833")
              ]
            },
            {
              field: "ThirdPartyKind",
              display: [
                // "If CategoryCode 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onOutflow().onSection("AB").onCategory(["512", "513"]),

                // "If the category is 256 and the PassportNumber under Resident Individual contains no value, Individual must be completed",
                limitValue(["Individual"], notTransactionField("Resident.Individual.PassportNumber")).onSection("AB").onCategory("256"),

                // "If the category is 255 or 256 and the Resident Entity element is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onSection("AB").onCategory(["255", "256"]),

                // "If the SupplementaryCardIndicator is Y, Individual must be completed"
                limitValue(["Individual"], hasResidentFieldValue("SupplementaryCardIndicator", "Y")).onSection("E"),

                // "If the Flow is IN and category 303, 304, 305, 306, 416 or 417 is used and Resident Entity is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onInflow().onSection("AB").onCategory(["303", "304", "305", "306", "416", "417"]),

                // "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onInflow().onSection("A").onCategory(["511", "516"])
              ]
            }
          ]
        },

        filterLookupImportExport: {
          ruleset: "Reporting Import Export Lookup Filter Rules",
          scope: "importexport",
          fields: []
        }
      }
    }

    return filterLookupRules;
  }
});


