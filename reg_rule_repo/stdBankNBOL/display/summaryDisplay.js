define(function () {
  return function (predef) {
    var summaryTrans, summaryMoney, summaryImportExport;
    with (predef) {
      summaryTrans = {
        ruleset: "Summary StdBank Transaction Display Rules",
        scope: "transaction",
        fields: [
          {
            field: "Resident",
            display: [
              setLabel("Account holder details")
            ]
          },
          {
            field: "NonResident",
            displayOnly: true,
            display: [
              setLabel("Beneficiary details").onOutflow(),
              setLabel("Remitter details").onInflow()
            ]
          },
          {

            field: [
              "Resident.Individual.StreetAddress.AddressLine1",
              "Resident.Entity.StreetAddress.AddressLine1",
              "Resident.Individual.PostalAddress.AddressLine1",
              "Resident.Entity.PostalAddress.AddressLine1",
              "Resident.Individual.StreetAddress.AddressLine2",
              "Resident.Entity.StreetAddress.AddressLine2",
              "Resident.Individual.PostalAddress.AddressLine2",
              "Resident.Entity.PostalAddress.AddressLine2",
              "Resident.Individual.StreetAddress.Suburb",
              "Resident.Entity.StreetAddress.Suburb",
              "Resident.Individual.PostalAddress.Suburb",
              "Resident.Entity.PostalAddress.Suburb",
              "Resident.Individual.StreetAddress.City",
              "Resident.Entity.StreetAddress.City",
              "Resident.Individual.PostalAddress.City",
              "Resident.Entity.PostalAddress.City",
              "Resident.Individual.StreetAddress.Province",
              "Resident.Entity.StreetAddress.Province",
              "Resident.Individual.PostalAddress.Province",
              "Resident.Entity.PostalAddress.Province",
              "Resident.Individual.StreetAddress.PostalCode",
              "Resident.Entity.StreetAddress.PostalCode",
              "Resident.Individual.PostalAddress.PostalCode",
              "Resident.Entity.PostalAddress.PostalCode",
              "Resident.Individual.ContactDetails.ContactSurname",
              "Resident.Entity.ContactDetails.ContactSurname",
              "Resident.Individual.ContactDetails.ContactName",
              "Resident.Entity.ContactDetails.ContactName",
              "Resident.Individual.ContactDetails.Email",
              "Resident.Entity.ContactDetails.Email",
              "Resident.Individual.ContactDetails.Fax",
              "Resident.Entity.ContactDetails.Fax",
              "Resident.Individual.ContactDetails.Telephone",
              "Resident.Entity.ContactDetails.Telephone"
            ],
            display: [
              setValue("", null, notEmpty).onSection("C")
            ]
          }
        ]
      };

      summaryMoney = {
        ruleset: "Summary StdBank Money Display Rules",
        scope: "money",
        fields: [        {
          field      : "ImportExport",
          displayOnly: true,
          display    : [
            hide(),
            show().onSection("ABG").onCategory(["101", "103", "105", "106"])
          ]
        }
        ]
      };

      summaryImportExport = {
        ruleset: "Summary StdBank Import Export Display Rules",
        scope: "importexport",
        fields: []
      };


    }

    return {
      summaryTrans: summaryTrans,
      summaryMoney: summaryMoney,
      summaryImportExport: summaryImportExport
    }

  }
})
;
/**
 * Created by daniel on 15/05/20.
 */
