define(function () {

  //thirdparty entity registration number test
  var regNumTest = /^\d{4}\/\d{6}\/\d{2}/;

  //thirdparty entity trust number test
  var trustNumTest = /^IT\/\d{1,6}\/\d{1,4}/;

  /**
   * Check to see if there is a MonetaryAmount with a dispensation category, and if so, checks to see if there are CCNs available for dispensation
   * The Assumption is that all CCNs will be the same, so just pick the first one.
   * @param context
   * @param value
   * @returns {boolean}
   */
  var hasDispensationCCN = function (context, value) {
    var cats = context.categories;
    var dispensations = _.filter(context.getCustomValue('dispensation'), function (item) {
      return cats.indexOf(item.category) != -1;
    });
    var CCNs = _.filter(dispensations, function (item) {
      return item.CustomsClientNumber;
    })
    return !!CCNs.length;
  }

  /**
   * Get the first available dispensation CCN matching selected categories.
   * @param context
   * @returns the first dispensation CCN
   */
  var getDispensationCCN = function (context) {
    var cats = _.map(context.MonetaryAmount, function (item) {
      return item.Category;
    });
    var dispensations = _.filter(context.getCustomValue('dispensation'), function (item) {
      return cats.indexOf(item.category) != -1;
    });
    var CCNs = _.map(_.filter(dispensations, function (item) {
      return !!item.CustomsClientNumber;
    }), function (item) {
      return item.CustomsClientNumber;
    })
    return CCNs[0];
  }

  var convertInterestRate = function (context) {
    var out = [];
    var bsRate = context.getMoneyField(context.currentMoneyInstance, 'LoanInterest.Index');
    if (bsRate) {
      out.push(bsRate);
    }
    var plsMin = context.getMoneyField(context.currentMoneyInstance, 'LoanInterest.PlusMinus');
    if (plsMin) {
      out.push(plsMin);
    }
    var rate = context.getMoneyField(context.currentMoneyInstance, 'LoanInterest.Rate');

    if (rate) {
      try {
        rate = new Number(rate).toFixed(2).toString();
      } catch (e) {
        rate = "0.00";
      }
      out.push(rate);
    }

    return out.join(' ');
  };


  var rateMatcher = /^([\w\s]+?)?(PLUS|MINUS)? ?(\d+\.\d+)?$/;

  function getInterestRate(context) {
    var rate = context.getMoneyField(context.currentMoneyInstance, 'LoanInterestRate');
    return rateMatcher.exec(rate)[3];
  }

  function getPlusMinus(context) {
    var rate = context.getMoneyField(context.currentMoneyInstance, 'LoanInterestRate');
    var match = rateMatcher.exec(rate);
    if (match[1] == "PLUS" || match[1] == "MINUS") {
      return match[1]
    } else {
      return match[2]
    }
  }

  function getBaseRate(context) {
    var rate = context.getMoneyField(context.currentMoneyInstance, 'LoanInterestRate');
    var match = rateMatcher.exec(rate);
    if (match[1] == "PLUS" || match[1] == "MINUS") {
      return "";
    } else {
      return match[1];
    }
  }

  function getInterestType(context) {
    var rate = context.getMoneyField(context.currentMoneyInstance, 'LoanInterestRate');
    var match = rateMatcher.exec(rate);
    if (typeof match[1] != "undefined") {
      return "INDEXED LINKED";
    } else {
      return "FIXED";
    }
  }

  return function (predef) {
    var display;

    with (predef) {
      display = {
        detailTrans: {
          ruleset: "Std Bank NBOL Transaction Display Rules",
          scope: "transaction",
          fields: [

            //================================================================================
            // RESIDENT
            //================================================================================
            {
              field: [
                "Resident.Individual.AccountName",
                "Resident.Individual.TaxNumber",
                "Resident.Individual.VATNumber",
                "Resident.Individual.ContactDetails.ContactName",
                "Resident.Individual.ContactDetails.ContactSurname",
                "Resident.Individual.ContactDetails.Telephone",
                "Resident.Individual.ContactDetails.Email",
                "Resident.Individual.ContactDetails.Fax",
                "Resident.Individual.StreetAddress.AddressLine1",
                "Resident.Individual.StreetAddress.AddressLine2",
                "Resident.Individual.StreetAddress.Suburb",
                "Resident.Individual.StreetAddress.City",
                "Resident.Individual.StreetAddress.Province",
                "Resident.Individual.StreetAddress.PostalCode",
                "Resident.Individual.PostalAddress.AddressLine1",
                "Resident.Individual.PostalAddress.AddressLine2",
                "Resident.Individual.PostalAddress.Suburb",
                "Resident.Individual.PostalAddress.City",
                "Resident.Individual.PostalAddress.Province",
                "Resident.Individual.PostalAddress.PostalCode"
              ],
              display: [
                show()
                // disable(hasTransactionField('Resident.Individual'))
              ]
            }, {
              field: ["Resident.Individual.Surname", "Resident.Individual.Name"],
              display: [
                disable(),
                show().onSection("ABCEG")
              ]
            }, {
              field: "Resident.Individual.DateOfBirth",
              display: [
                disable(isTransactionFieldProvided("Resident.Individual.DateOfBirth")),
                show().onSection("ABEG")
              ]
            }, {
              field: ["Resident.Individual.Gender", "Resident.Individual.IDNumber"],
              display: [
                disable(),
                show().onSection("ABEG")
              ]
            }, {
              field: "Resident.Individual.IDNumber",
              display: [
                disable(),
                hide(),
                show(notEmpty.and(
                  notTransactionField("Resident.Individual.ForeignIDNumber")
                    .or(notTransactionField("Resident.Individual.PassportNumber"))
                    .or(notTransactionField("Resident.Individual.TempResPermitNumber"))
                )).onSection("ABEG")
              ]
            }, {
              field: [
                "Resident.Individual.TempResPermitNumber",
                "Resident.Individual.ForeignIDNumber",
                "Resident.Individual.ForeignIDCountry"],
              display: [
                disable(),
                hide(),
                show(
                  notTransactionField("Resident.Individual.IDNumber")
                ).onSection("ABEG"),
                hide().onSection("AB").onCategory(["511", "512", "513", "514", "515"]),
                hide().onOutflow().onSection("AB").onCategory("401")
              ]
            }, {
              field: "Resident.Individual.ForeignIDCountry",
              display: [
                enable(not(isTransactionFieldProvided("Resident.Individual.ForeignIDCountry"))),
              ]
            }, {
              field: ["Resident.Individual.PassportNumber", "Resident.Individual.PassportCountry"],
              display: [
                disable(),
                hide(),
                show(notEmpty.and(
                  notTransactionField("Resident.Individual.IDNumber")
                )).onSection("ABEG"),
                hide().onSection("G"),
                hide().onSection("AB").onCategory("255")
              ]
            }, {
              field: "Resident.Entity.EntityName",
              display: [
                disable(),
                hide().onSection("F")
              ]
            }, {
              field: "Resident.Entity.TradingName",
              display: [
                disable(isTransactionFieldProvided("Resident.Entity.TradingName")),
                hide().onSection("F")
              ]
            }, {
              field: "Resident.Entity.RegistrationNumber",
              display: [
                disable(),
                hide().onSection("F")
              ]
            }, {
              field: "Resident.Entity.InstitutionalSector",
              display: [
                disable(isTransactionFieldProvided("Resident.Entity.InstitutionalSector")),
                hide().onSection("F")
              ]
            }, {
              field: "Resident.Entity.IndustrialClassification",
              display: [
                disable(),
                hide().onSection("F")
              ]
            }, {
              field: "Resident.Exception",
              display: [
                disable(),
                hide().onSection("BEF")
              ]
            }, {
              field: "Resident.Exception.ExceptionName",
              display: [
                disable(),
                hide().onSection("BEF")
              ]
            }, {
              field: "Resident.Exception.Country",
              display: [
                hide(),
                show(hasTransactionField("Resident.Exception")).onSection("ACDG")
              ]
            }, {
              field: ["Resident.Individual", "Resident.Entity"],
              display: [
                disable(),
                show()
              ]
            }, {
              field: ["Resident.Individual.AccountName", "Resident.Entity.AccountName"],
              display: [
                disable(),
                show()
              ]
            }, {
              field: ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
              display: [
                disable(),
                hide().onSection("F")
              ]
            }, {
              field: ["Resident.Individual.AccountNumber", "Resident.Entity.AccountNumber"],
              display: [
                disable(),
                hide().onSection("F")
              ]
            }, {
              field: "CCNResponseCode",
              display: [
                hide(),
                disable(),
                enable(evalResidentField("CustomsClientNumber", hasPattern(/^\d{8,13}$/))),
                show(hasResidentField("CustomsClientNumber")).onInflow().onSection("AB").onCategory(["101", "103", "105", "106"]),
                show(hasResidentField("CustomsClientNumber")).onOutflow().onSection("AB").onCategory(["101", "102", "103", "104", "105", "106", "271"]),
                disable(hasResidentFieldValue("CustomsClientNumber", "70707070"))
              ]
            }, {
              field: "Resident.Individual.CustomsClientNumber",
              display: [
                hide(),
                show(hasTransactionField("Resident.Individual"))
                  .onInflow()
                  .onSection("AB")
                  .onCategory(["101", "103", "105", "106"]),
                show(hasTransactionField("Resident.Individual"))
                  .onOutflow()
                  .onSection("AB")
                  .onCategory(["101", "102", "103", "104", "105", "106", "271"]),
               // show(notEmpty),
                setValue(getDispensationCCN,
                  notValue(getDispensationCCN)
                    .and(hasTransactionField("Resident.Individual"))
                    .and(hasDispensationCCN)),
                disable(
                  notEmpty
                    .and(hasTransactionField("Resident.Individual"))
                    .and(hasDispensationCCN))
              ]
            }, {
              field: "Resident.Entity.CustomsClientNumber",
              display: [
                hide(),
                show(hasTransactionField("Resident.Entity"))
                  .onInflow()
                  .onSection("AB")
                  .onCategory(["101", "103", "105", "106"]),
                show(hasTransactionField("Resident.Entity"))
                  .onOutflow()
                  .onSection("AB")
                  .onCategory(["101", "102", "103", "104", "105", "106", "271"]),
              //  show(notEmpty),
                setValue(getDispensationCCN,
                  notValue(getDispensationCCN)
                    .and(hasTransactionField("Resident.Entity"))
                    .and(hasDispensationCCN)),
                disable(
                  notEmpty
                    .and(hasTransactionField("Resident.Entity"))
                    .and(hasDispensationCCN))
              ]
            }, {
              field: "Resident.Individual.TaxNumber",
              display: [
                disable(isTransactionFieldProvided("Resident.Individual.TaxNumber")),
                hide().onSection("F")
              ]
            }, {
              field: "Resident.Individual.VATNumber",
              display: [
                disable(isTransactionFieldProvided("Resident.Individual.VATNumber")),
                hide().onSection("F")
              ]
            }, {
              field: "Resident.Entity.TaxNumber",
              display: [
                disable(isTransactionFieldProvided("Resident.Entity.TaxNumber")),
                hide().onSection("F")
              ]
            }, {
              field: "Resident.Entity.VATNumber",
              display: [
                disable(isTransactionFieldProvided("Resident.Entity.VATNumber")),
                hide().onSection("F")
              ]
            }, {
              field: ["Resident.Individual.TaxClearanceCertificateIndicator", "Resident.Entity.TaxClearanceCertificateIndicator"],
              display: [
                hide(),
                show().onOutflow().onSection("AB").onCategory(['512', '513'])
              ]
            }, {
              field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
              display: [
                hide(),
                show(hasTransactionFieldValue("Resident.Individual.TaxClearanceCertificateIndicator", "Y").or(hasTransactionFieldValue("Resident.Entity.TaxClearanceCertificateIndicator", "Y"))).onOutflow().onSection("AB").onCategory(['512', '513'])
              ]
            }, {
              field: ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1", "Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1"
                , "Resident.Individual.StreetAddress.AddressLine2", "Resident.Entity.StreetAddress.AddressLine2", "Resident.Individual.PostalAddress.AddressLine2", "Resident.Entity.PostalAddress.AddressLine2"
                , "Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb", "Resident.Individual.PostalAddress.Suburb", "Resident.Entity.PostalAddress.Suburb"
                , "Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City", "Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City"
                , "Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province", "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"
                , "Resident.Individual.StreetAddress.PostalCode", "Resident.Entity.StreetAddress.PostalCode"
                , "Resident.Individual.PostalAddress.PostalCode", "Resident.Entity.PostalAddress.PostalCode"
                , "Resident.Individual.ContactDetails.ContactSurname", "Resident.Entity.ContactDetails.ContactSurname", "Resident.Individual.ContactDetails.ContactName", "Resident.Entity.ContactDetails.ContactName"],
              display: [
                disable(),
                hide().onSection("CDF")
              ]
            }, {
              field: "Resident.Entity.ContactDetails.ContactSurname",
              display: [
                disable(isTransactionFieldProvided("Resident.Entity.ContactDetails.ContactSurname")),
                hide().onSection("CDF")
              ]
            }, {
              field: "Resident.Individual.ContactDetails.ContactSurname",
              display: [
                disable(isTransactionFieldProvided("Resident.Individual.ContactDetails.ContactSurname")),
                hide().onSection("CDF")
              ]
            }, {
              field: "Resident.Entity.ContactDetails.ContactName",
              display: [
                disable(isTransactionFieldProvided("Resident.Entity.ContactDetails.ContactName")),
                hide().onSection("CDF")
              ]
            }, {
              field: "Resident.Individual.ContactDetails.ContactName",
              display: [
                disable(isTransactionFieldProvided("Resident.Individual.ContactDetails.ContactName")),
                hide().onSection("CDF")
              ]
            }, {
              field: "Resident.Entity.ContactDetails.Email",
              display: [
                disable(isTransactionFieldProvided("Resident.Entity.ContactDetails.Email")),
                hide().onSection("CDF")
              ]
            }, {
              field: "Resident.Individual.ContactDetails.Email",
              display: [
                disable(isTransactionFieldProvided("Resident.Individual.ContactDetails.Email")),
                hide().onSection("CDF")
              ]
            }, {
              field: "Resident.Individual.ContactDetails.Fax",
              display: [
                disable(isTransactionFieldProvided("Resident.Individual.ContactDetails.Fax")),
                hide().onSection("CDF")
              ]
            }, {
              field: "Resident.Entity.ContactDetails.Fax",
              display: [
                disable(isTransactionFieldProvided("Resident.Entity.ContactDetails.Fax")),
                hide().onSection("CDF")
              ]
            }, {
              field: "Resident.Individual.ContactDetails.Telephone",
              display: [
                disable(isTransactionFieldProvided("Resident.Individual.ContactDetails.Telephone")),
                hide().onSection("CDF")
              ]
            }, {
              field: "Resident.Entity.ContactDetails.Telephone",
              display: [
                disable(isTransactionFieldProvided("Resident.Entity.ContactDetails.Telephone")),
                hide().onSection("CDF")
              ]
            }, {
              field: ["Resident.Individual.CardNumber", "Resident.Entity.CardNumber", "Resident.Individual.SupplementaryCardIndicator", "Resident.Entity.SupplementaryCardIndicator"],
              display: [
                disable(),
                hide().onSection("ABCDFG")
              ]
            },
            //================================================================================
            // TRANSACTION
            //================================================================================

            {
              field: "Flow",
              display: [
                hide()
              ]
            }, {
              field: "ValueDate",
              display: [
                show(),
                disable()
              ]
            }, {
              field: ["FlowCurrency", "TotalForeignValue"],
              display: [
                show(),
                disable()
              ]
            }, {
              field: ["BranchCode", "BranchName", "HubCode", "HubName"],
              display: [
                hide()
              ]
            }, {
              field: "OriginatingBank",
              display: [
                disable(),
                hide(),
                show().onOutflow().onSection("ABCDG"),
                show(notNonResidentFieldValue("AccountIdentifier", "CASH")).onInflow().onSection("ABCDG"),
                show(hasTransactionField("OriginatingCountry")).onInflow().onSection("ABCDG")
              ]
            }, {
              field: "OriginatingCountry",
              display: [
                disable(),
                hide(),
                show().onOutflow().onSection("ABCDG"),
                show(notNonResidentFieldValue("AccountIdentifier", "CASH")).onInflow().onSection("ABCDG"),
                setValue("ZA").onOutflow().onSection("ABG"),
                show(hasTransactionField("OriginatingBank")).onInflow().onSection("ABCDG"),
                //Enable for R21.1
                setValue("%s", function (context) {
                    //var orderingAccountCurrency = context.getCustomValue("orderingAccountCurrency");
                    var currency = context.getTransactionField("FlowCurrency");//orderingAccountCurrency
                    if (currency && (currency!='ZAR')) {
                      return currency.substr(0, 2);
                    }
                    return undefined;
                  },
                  hasTransactionFieldValue("NonResident.Individual.AccountIdentifier", "FCA RESIDENT")
                    .or(hasTransactionFieldValue("NonResident.Entity.AccountIdentifier", "FCA RESIDENT"))
                    .or(hasTransactionFieldValue("Resident.Individual.AccountIdentifier", "FCA RESIDENT"))
                    .or(hasTransactionFieldValue("Resident.Entity.AccountIdentifier", "FCA RESIDENT"))
                ).onInflow()
              ]
            }, {
              field: "CorrespondentBank",
              display: [
                disable(),
                show().onSection("ABCDG"),
                hide().onSection("EF")
              ]
            }, {
              field: "CorrespondentCountry",
              display: [
                disable(),
                show().onSection("ABCDG"),
                hide().onSection("EF")
              ]
            }, {
              field: "ReceivingBank",
              display: [
                disable(),
                hide(),
                show().onInflow().onSection("ABCDG"),
                show(notResidentFieldValue("AccountIdentifier", "CASH")).onOutflow().onSection("ABCDG"),
                hide(hasResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["250", "251"]),
                hide(hasNonResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["255", "256"]),
                show(hasTransactionField("ReceivingCountry")).onOutflow().onSection("ABCDG")

              ]
            }, {
              field: "ReceivingCountry",
              display: [
                disable(),
                hide(),
                show().onInflow().onSection("ABCDG"),
                show(notResidentFieldValue("AccountIdentifier", "CASH")).onOutflow().onSection("ABCDG"),
                hide(hasResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["250", "251"]),
                hide(hasNonResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["255", "256"]),
                show(hasTransactionField("ReceivingBank")).onOutflow().onSection("ABCDG"),
                //Enable for R21
                setValue("%s", function (context) {
                    var currency = context.getTransactionField("FlowCurrency");
                    if (currency) {
                      return currency.substr(0, 2);
                    }
                  },
                  hasTransactionFieldValue("NonResident.Individual.AccountIdentifier", "FCA RESIDENT")
                    .or(hasTransactionFieldValue("NonResident.Entity.AccountIdentifier", "FCA RESIDENT"))
                    .or(hasTransactionFieldValue("Resident.Individual.AccountIdentifier", "FCA RESIDENT"))
                    .or(hasTransactionFieldValue("Resident.Entity.AccountIdentifier", "FCA RESIDENT"))
                ).onOutflow()
              ]
            },
            //================================================================================
            // NON RESIDENT
            //================================================================================

            {
              field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
              display: [
                disable(),
                show().onSection("ABCDG")
              ]
            }, {
              field: "NonResident.Individual.Surname",
              display: [
                disable(),
                show().onSection("ABCDG"),
                enable(not(isTransactionFieldProvided("NonResident.Individual.Surname")))
              ]
            }, {
              field: "NonResident.Individual.Name",
              display: [
                disable(),
                show().onSection("ABCDG"),
                enable(not(isTransactionFieldProvided("NonResident.Individual.Name")))
              ]
            }, {
              field: "NonResident.Individual.Gender",
              display: [
                disable(),
                show().onSection("ABCDG"),
                enable(not(isTransactionFieldProvided("NonResident.Individual.Gender")))
              ]
            }, {
              field: "NonResident.Individual.PassportCountry",
              display: [
                disable(),
                show().onSection("AB").onCategory(["250", "251"]),
                enable(not(isTransactionFieldProvided("NonResident.Individual.PassportCountry"))).onSection("AB").onCategory(["250", "251"])
              ]
            }, {
              field: "NonResident.Individual.PassportNumber",
              display: [
                disable(),
                show().onSection("AB").onCategory(["250", "251"]),
                enable(not(isTransactionFieldProvided("NonResident.Individual.PassportNumber"))).onSection("AB").onCategory(["250", "251"])
              ]
            }, {
              field: "NonResident.Entity.EntityName",
              display: [
                disable(),
                show().onSection("ABCDG"),
                hide().onSection("E"),
                enable(not(isTransactionFieldProvided("NonResident.Entity.EntityName")))
              ]
            }, {
              field: ["NonResident.Entity.CardMerchantName", "NonResident.Entity.CardMerchantCode"],
              display: [
                disable(),
                show().onSection("E"),
                hide().onSection("ABCDG")
              ]
            }, {
              field: "NonResident.Exception",
              display: [
                disable(),
                hide(hasTransactionField("Resident.Exception")).onSection("A"),
                hide().onSection("BEG")
              ]
            }, {
              field: "NonResident.Exception.ExceptionName",
              display: [
                disable(),
                hide(hasTransactionField("Resident.Exception")).onSection("A"),
                hide().onSection("BEG"),
                setValue('MUTUAL PARTY', null, isEmpty).onSection('A').onInflow().onCategory("252"),
                enable(not(isTransactionFieldProvided("NonResident.Exception.ExceptionName")))
              ]
            }, {
              field: "NonResident.Individual.AccountIdentifier",
              display: [
                disable(),
                setValue("NON RESIDENT RAND", null, isEmpty).onSection("B")
              ]
            }, {
              field: ["NonResident.Individual.AccountNumber", "NonResident.Entity.AccountNumber"],
              display: [
                disable(),
                show(),
                enable(not(isTransactionFieldProvided("NonResident.Individual.AccountNumber")
                  .or(isTransactionFieldProvided("NonResident.Entity.AccountNumber"))))
              ]
            }, {
              field: [
                "NonResident.Entity.Address.AddressLine1",
                "NonResident.Entity.Address.AddressLine2",
                "NonResident.Entity.Address.Suburb",
                "NonResident.Entity.Address.City",
                "NonResident.Entity.Address.State",
                "NonResident.Entity.Address.PostalCode"
              ],
              display: [
                disable(),
                hide().onSection("E"),
                enable(not(isTransactionFieldProvided("NonResident.Entity.Address")))
              ]
            }, {
              field: [
                "NonResident.Individual.Address.AddressLine1",
                "NonResident.Individual.Address.AddressLine2",
                "NonResident.Individual.Address.Suburb",
                "NonResident.Individual.Address.City",
                "NonResident.Individual.Address.State",
                "NonResident.Individual.Address.PostalCode"
              ],
              display: [
                disable(),
                hide().onSection("E"),
                enable(not(isTransactionFieldProvided("NonResident.Individual.Address")))
              ]
            }, {
              field: ["NonResident.Individual.Address.Country", "NonResident.Entity.Address.Country"],
              display: [
                disable(),
                show().onSection("ABCDEG"),
                enable(hasTransactionFieldValue('FlowCurrency','EUR')).notOnCategory(['513','517']),
                //Enable for R21
                setValue("%s", function (context) {
                    var currency = context.getTransactionField("FlowCurrency");
                    if (currency && (currency!='ZAR')) {
                        return currency.substr(0, 2);
                    }
                  },notTransactionFieldValue('FlowCurrency','EUR')
                ),
                setValue("EU",hasTransactionFieldValue('FlowCurrency','EUR')
                ).onCategory(['513','517']),
                enable(not(isTransactionFieldProvided("NonResident.Individual.Address.Country")
                  .or(isTransactionFieldProvided("NonResident.Entity.Address.Country"))))
              ]
            },
          ]
        },
        //================================================================================
        // MONEY SCOPE
        //================================================================================

        detailMoney: {
          ruleset: "Std Bank NBOL Money Display Rules",
          scope: "money",
          fields: [{
            field: "ForeignValueAuthAgg",
            display: [
              setValue('%s', function (ctx) {
                var adIntAuthNum = ctx.getMoneyField(ctx.currentMoneyInstance, "{{Regulator}}Auth.ADInternalAuthNumber");
                if (!adIntAuthNum) {
                  return ctx.getMoneyField(ctx.currentMoneyInstance, "ForeignValue");
                }

                var val = 0;
                var len = ctx.getMoneySize();

                for (var i = 0; i < len; i++) {
                  var fv = 0;
                  var tmp = ctx.getMoneyField(i, "{{Regulator}}Auth.ADInternalAuthNumber");

                  if (tmp && tmp == adIntAuthNum)
                    try {
                      fv = new Number(ctx.getMoneyField(i, "ForeignValue"));
                    } catch (e) {
                    }

                  val = val + fv;
                }

                return val.toString();
              })
            ]
          }, {
            field: "{{Regulator}}Auth.RulingsSection",
            display: [
              hide(),
              setValue(function(context,moneyInd){
                  var cat = context.MonetaryAmount[moneyInd].Category;
                  var dispensation = _.find(context.getCustomValue('dispensation'),function (item) {
                    return item.category == cat
                  });
                  return dispensation.SectionOfRulings;
                },
                isEmpty.and(evalMoneyField("Category",hasLookupValue("Dispensation","category","SectionOfRulings")))),
              show(evalMoneyField("Category",hasLookupValue("Dispensation","category","SectionOfRulings"))),
              disable()
            ]
          }, {
            field: "RandValue",
            display: []
          }, {
            field: "RandValue",
            display: [
              setValue("%s", "ForeignValue", hasTransactionFieldValue("FlowCurrency", "ZAR"))
            ]
          }, {
            field: "CategoryCode",
            display: [
              hide().onSection("DEF"),

              setValue(undefined, null,
                notEmpty.and(notImportUndertakingClient))
                .onSection("ABCG").onCategory(["102", "104"]),

              setValue(undefined, null,
                notEmpty.and(importUndertakingClient))
                .onSection("ABCG").onCategory(["101", "103"]),

              setValue('ZZ1').onSection("C"),
              setValue("833", null, hasCustomValue('ADLA', true))
            ]
          }, {
            field: "CategorySubCode",
            display: [
              hide().onSection("DEF"),
              setValue(undefined, null,
                notEmpty.and(notImportUndertakingClient))
                .onSection("ABCG").onCategory(["102", "104"]),
              setValue(undefined, null,
                notEmpty.and(importUndertakingClient))
                .onSection("ABCG").onCategory(["101", "103"])
            ]
          }, {
            field: "SWIFTDetails",
            display: [

              hide()
            ]
          }, {
            field: "MoneyTransferAgentIndicator",
            display: [
              disable().notOnCategory('833'),
              disable(hasCustomValue('ADLA', true).or(hasCustomField("MoneyTransferAgentIndicator"))).onCategory('833'),
              setValue("AD", notCustomValue('ADLA', true)).notOnCategory('833'),
              setValue(function(context){
                return context.getCustomValue("MoneyTransferAgentIndicator");
              }, notCustomValue('ADLA', true).and(hasCustomField("MoneyTransferAgentIndicator"))).onCategory('833'),
              setValue("AD", notCustomValue('ADLA', true).and(not(hasCustomField("MoneyTransferAgentIndicator")))).notOnCategory('833'),
              setValue(function(context){
                return context.getCustomValue("MoneyTransferAgentIndicator");
              }, hasCustomValue('ADLA', true).and(hasCustomField("MoneyTransferAgentIndicator"))).onCategory('833')
            ]
          },

            //================================================================================
            // SBFIN-514 - exemption value. (This needs to be before {{Regulator}}Auth display rules)
            //================================================================================


            {
              field : "exemption",
              display : [
                setValue(function(context,moneyInd){

                  var cat = context.MonetaryAmount[moneyInd].Category;
                  var dispensations = context.getCustomValue('dispensation');
                  if(!dispensations) return false;
                  
                  var dispensation = dispensations.find(function (item) {
                    return item.category == cat
                  });

                  if(!dispensation) return false;

                  //map comparison.

                  var AuthIssuer = context.getMoneyField(moneyInd,'{{Regulator}}Auth.AuthIssuer');
                  var ADInternalAuthNumberDate = context.getMoneyField(moneyInd,'{{Regulator}}Auth.ADInternalAuthNumberDate');
                  var ADInternalAuthNumber = context.getMoneyField(moneyInd,'{{Regulator}}Auth.ADInternalAuthNumber');
                  var SARBAuthAppNumber = context.getMoneyField(moneyInd,'{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber');
                  var SARBAuthRefNumber = context.getMoneyField(moneyInd,'{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber');


                  return (
                    (dispensation.AuthIssuer  == AuthIssuer) &&
                    (dispensation.ADInternalAuthNumberDate  == ADInternalAuthNumberDate) &&
                    (dispensation.ADInternalAuthNumber  == ADInternalAuthNumber) &&
                    (dispensation.SARBAuthAppNumber  == SARBAuthAppNumber) &&
                    (dispensation.SARBAuthRefNumber  == SARBAuthRefNumber)
                  )

                })
              ]
            },

            //================================================================================
            // SARB AUTH
            //================================================================================

            {
              field: "{{Regulator}}Auth.AuthIssuer",// Custom rules added for "exCon"
              display: [
                hide(),
                show().onSection("ABG").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
                setValue(function (context, moneyInd) {
                    var cat = context.MonetaryAmount[moneyInd].Category;
                    var dispensation = _.find(context.getCustomValue('dispensation'),function (item) {
                      return item.category == cat
                    });
                    return dispensation.AuthIssuer;
                  },
                  isEmpty.and(evalMoneyField("Category", hasLookupValue("Dispensation", "category", "AuthIssuer")))),
                show(hasCustomValue("exCon", true)).onSection("C"),
                show(hasCustomValue("exCon", "true")).onSection("C"),
                hide(evalMoneyField("Category",hasLookupValue("Dispensation","category","SectionOfRulings")))
              ]
            }, {
              field: "{{Regulator}}Auth.ADInternalAuthNumber",
              display: [
                hide(),
                setValue(function (context, moneyInd) {
                    var cat = context.MonetaryAmount[moneyInd].Category;
                    var dispensation = _.find(context.getCustomValue('dispensation'),function (item) {
                      return item.category == cat
                    });
                    return dispensation.ADInternalAuthNumber;
                  },
                  isEmpty.and(evalMoneyField("Category", hasLookupValue("Dispensation", "category", "ADInternalAuthNumber")))),
                disable(notEmpty.and(evalMoneyField("Category",hasLookupValue("Dispensation","category","ADInternalAuthNumber")))),
                show(hasMoneyFieldValue("{{Regulator}}Auth.AuthIssuer", ["016"])).onSection("ABG").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
                show(hasMoneyFieldValue("{{Regulator}}Auth.AuthIssuer", ["016"])).onSection("C")
              ]
            }, {
              field: "{{Regulator}}Auth.ADInternalAuthNumberDate",
              display: [
                hide(),
                setValue(function (context, moneyInd) {
                    var cat = context.MonetaryAmount[moneyInd].Category;
                    var dispensation = _.find(context.getCustomValue('dispensation'),function (item) {
                      return item.category == cat
                    });
                    return dispensation.ADInternalAuthNumberDate;
                  },
                  isEmpty.and(evalMoneyField("Category", hasLookupValue("Dispensation", "category", "ADInternalAuthNumberDate")))),
                disable(notEmpty.and(evalMoneyField("Category",hasLookupValue("Dispensation","category","ADInternalAuthNumberDate")))),
                show(hasMoneyFieldValue("{{Regulator}}Auth.AuthIssuer", ["016"])).onSection("ABG").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
                show(hasMoneyFieldValue("{{Regulator}}Auth.AuthIssuer", ["016"])).onSection("C")
              ]
            }, {
              field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
              display: [
                hide(),
                setValue(function (context, moneyInd) {
                    var cat = context.MonetaryAmount[moneyInd].Category;
                    var dispensation = _.find(context.getCustomValue('dispensation'),function (item) {
                      return item.category == cat
                    });
                    return dispensation.SARBAuthAppNumber;
                  },
                  isEmpty.and(evalMoneyField("Category", hasLookupValue("Dispensation", "category", "{{RegulatorPrefix}}AuthAppNumber")))),
                disable(notEmpty.and(evalMoneyField("Category",hasLookupValue("Dispensation","category","{{RegulatorPrefix}}AuthAppNumber")))),
                show(hasMoneyFieldValue("{{Regulator}}Auth.AuthIssuer", ["024"])).onSection("ABG").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
                show(hasMoneyFieldValue("{{Regulator}}Auth.AuthIssuer", ["024"])).onSection("C"),

              ]
            }, {
              field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
              display: [
                hide(),
                setValue(function (context, moneyInd) {
                    var cat = context.MonetaryAmount[moneyInd].Category;
                    var dispensation = _.find(context.getCustomValue('dispensation'),function (item) {
                      return item.category == cat
                    });
                    return dispensation.SARBAuthRefNumber;
                  },
                  isEmpty.and(evalMoneyField("Category", hasLookupValue("Dispensation", "category", "{{RegulatorPrefix}}AuthAppNumber")))),
                disable(notEmpty.and(evalMoneyField("Category",hasLookupValue("Dispensation","category","{{RegulatorPrefix}}AuthRefNumber")))),
                show(hasMoneyFieldValue("{{Regulator}}Auth.AuthIssuer", ["024"])).onSection("ABG").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
                show(hasMoneyFieldValue("{{Regulator}}Auth.AuthIssuer", ["024"])).onSection("C")
              ]
            },




            //================================================================================
            // LOANS
            //================================================================================

            {
              field: "LoanInterest.InterestType",
              display: [
                hide(),
                show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
                show().onSection("AB").onCategory(["801", "802", "803", "804"]),
                show().onOutflow().onSection("AB").onCategory([/*"106",*/ "309/04", "309/05", "309/06", "309/07"]),
                setValue('%s', getInterestType, hasCustomValue("readOnly", true))
              ]
            }, {
              field: "LoanInterest.Index",
              display: [
                hide(),
                show(hasMoneyFieldValue("LoanInterest.InterestType", ["INDEXED LINKED"])),
                setValue('%s', getBaseRate, hasCustomValue("readOnly", true))
              ]
            }, {
              field: "LoanInterest.PlusMinus",
              display: [
                hide(),
                show(hasMoneyFieldValue("LoanInterest.InterestType", ["INDEXED LINKED"])),
                setValue('%s', getPlusMinus, hasCustomValue("readOnly", true))

              ]
            }, {
              field: "LoanInterest.Rate",
              display: [
                hide(),
                show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
                show().onSection("AB").onCategory(["801", "802", "803", "804"]),
                show().onOutflow().onSection("ABG").onCategory([/*"106",*/ "309/04", "309/05", "309/06", "309/07"]),
                show().onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),

                setValue('%s', getInterestRate, hasCustomValue("readOnly", true)).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
                setValue('%s', getInterestRate, hasCustomValue("readOnly", true)).onSection("AB").onCategory(["801", "802", "803", "804"]),
                setValue('%s', getInterestRate, hasCustomValue("readOnly", true)).onOutflow().onSection("ABG").onCategory([/*"106",*/ "309/04", "309/05", "309/06", "309/07"]),
                setValue('%s', getInterestRate, hasCustomValue("readOnly", true)).onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"])
              ]
            }, {
              field: "LoanInterestRate",
              display: [
                //setValue(undefined),
                setValue('%s', convertInterestRate, notCustomValue("readOnly", true)).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
                setValue('%s', convertInterestRate, notCustomValue("readOnly", true)).onCategory(["801", "802", "803", "804"]),
                setValue('%s', convertInterestRate, notCustomValue("readOnly", true)).onOutflow().onSection("ABG").onCategory([/*"106",*/ "309/04", "309/05", "309/06", "309/07"]),
                setValue('%s', convertInterestRate, notCustomValue("readOnly", true)).onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"])
              ]
            }, {
              field: "LoanRefNumber",
              display: [
                hide().onSection("CDEF"),
                hide().onSection("AB").notOnCategory(["801", "802", "803", "804", "106", "309/04", "309/05", "309/06", "309/07"]),
                //show().onSection("AB").onCategory(["801", "802", "803", "804"]),
                setValue("99012301230123", null,
                  isEmpty.and(hasMoneyFieldValue("LocationCountry", "LS"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
                setValue("99456745674567", null,
                  isEmpty.and(hasMoneyFieldValue("LocationCountry", "SZ"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
                setValue("99789078907890", null,
                  isEmpty.and(hasMoneyFieldValue("LocationCountry", "NA"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
                //show().onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
                setValue("99012301230123", null,
                  isEmpty.and(hasMoneyFieldValue("LocationCountry", "LS"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
                setValue("99456745674567", null,
                  isEmpty.and(hasMoneyFieldValue("LocationCountry", "SZ"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
                setValue("99789078907890", null,
                  isEmpty.and(hasMoneyFieldValue("LocationCountry", "NA"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"])
              ]
            }, {
              field: "LoanTenorType",
              display: [
                hide(),
                show().onSection("AB").onCategory(["801", "802", "803", "804"]),
                show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"])
              ]
            }, {
              field: "LoanTenor",
              display: [
                clearValue(),
                setValue('%s', function (ctx) {
                  var val = ctx.getMoneyField(ctx.currentMoneyInstance, "LoanTenorType");
                  //console.log(val);
                  if (val == "ON DEMAND") {
                    return val;
                  }
                  if (val == "MATURITY DATE") {
                    return ctx.getMoneyField(ctx.currentMoneyInstance, "LoanTenorMaturityDate");
                  }
                  return undefined;
                })
              ]
            }, {
              field: "LoanTenorMaturityDate",
              display: [
                hide(),
                show(hasMoneyFieldValue("LoanTenorType", ["MATURITY DATE"])).onSection("AB").onCategory(["801", "802", "803", "804"]),
                show(hasMoneyFieldValue("LoanTenorType", ["MATURITY DATE"])).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"])
              ]
            }, {
              field: "LoanRefNumberResponseCode",
              display: [
                hide(),
                show(notResidentFieldValue("AccountIdentifier", "VOSTRO").and(notMoneyFieldValue("LocationCountry",["SZ","LS","NA"]))).onSection("AB").onCategory(["801", "802", "803", "804"]),
                show(notResidentFieldValue("AccountIdentifier", "VOSTRO").and(notMoneyFieldValue("LocationCountry",["SZ","LS","NA"]))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
                disable(),
                enable(
                  notResidentFieldValue("AccountIdentifier", "VOSTRO").and(
                    evalMoneyField("LoanRefNumber", hasPattern(/^\d{2,}$/))
                      .and(
                      hasMoneyFieldValue("{{Regulator}}Auth.AuthIssuer", "016")
                        .and(notMoneyField("{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber"))
                        .and(
                        evalMoneyField("ADInternalAuthResponse.status", hasValue("pass"))
                          .or(evalMoneyField("ADInternalAuthResponse.status", hasValue("warning")))
                      )
                        .or(
                        hasMoneyFieldValue("{{Regulator}}Auth.AuthIssuer", "024")
                          .and(notMoneyField("{{Regulator}}Auth.ADInternalAuthNumber"))
                          .and(evalMoneyField("{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber", hasPattern(/^\S{2,}$/)))
                          .and(evalMoneyField("{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber", hasPattern(/^\S{2,}$/)))
                      )
                    )
                  ))
              ]
            }, {
              field: "AdHocRequirement.Description",
              display: [
                hide(),
                show(hasMoneyField("AdHocRequirement.Subject")).onSection("ABG"),
                setValue("NONE", null,
                  hasMoneyFieldValue("AdHocRequirement.Subject", "NO")).onSection("A")
              ]
            }, {
              field: "AdHocRequirement.Subject",
              display: [
                hide().onSection("CDEF")
              ]
            },
            //================================================================================
            // THIRD PARTY
            //================================================================================

            {
              field: "ThirdParty",
              display: [
                hide(),
                show().notOnCategory(['ZZ1']).onSection("ABDEFGH")
              ]
            },
            //--------------------
            // for sameAsPhysical
            {
              field: "ThirdParty.PostalAddress.AddressLine1",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.AddressLine1", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true"))))
              ]
            }, {
              field: "ThirdParty.PostalAddress.AddressLine1",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.AddressLine1", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true"))))
              ]
            }, {
              field: "ThirdParty.PostalAddress.AddressLine2",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.AddressLine2", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true"))))
              ]
            }, {
              field: "ThirdParty.PostalAddress.Suburb",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.Suburb", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true"))))
              ]
            }, {
              field: "ThirdParty.PostalAddress.City",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.City", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true"))))
              ]
            }, {
              field: "ThirdParty.PostalAddress.Province",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.Province", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true"))))
              ]
            }, {
              field: "ThirdParty.PostalAddress.PostalCode",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.PostalCode", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true"))))
              ]
            }, {
              field: ["ThirdPartyKind", "ThirdParty.Individual.Surname", "ThirdParty.Individual.Name", "ThirdParty.Individual.Gender",
                "ThirdParty.Individual.DateOfBirth", "ThirdParty.CustomsClientNumber",
                "ThirdParty.StreetAddress.AddressLine1", "ThirdParty.StreetAddress.AddressLine2",
                "ThirdParty.StreetAddress.Suburb", "ThirdParty.StreetAddress.City", "ThirdParty.StreetAddress.Province",
                "ThirdParty.StreetAddress.PostalCode", "ThirdParty.ContactDetails.ContactSurname", "ThirdParty.ContactDetails.ContactName",
                "ThirdParty.ContactDetails.Email", "ThirdParty.ContactDetails.Fax", "ThirdParty.ContactDetails.Telephone", "ThirdParty.Entity.Name"
              ],
              display: [
                show(),
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true"))))
              ]
            }, {
              field: ["ThirdParty.CustomsClientNumber"],
              display: [
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true")))),
                hide().onSection("CDEF")
              ]
              // displays driven off of ThirdParty IDType
            }, {
              field: "ThirdParty.TaxNumber",
              display: [
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true")))),
                disable(hasMoneyFieldValue("ThirdParty.Entity.IDType", "Tax number")),
                setValue('%s', "ThirdParty.Entity.RegistrationNumber", hasMoneyFieldValue("ThirdParty.Entity.IDType", "Tax number")),
                hide().onSection("CDEF")
              ]
            }, {
              field: "ThirdParty.VATNumber",
              display: [
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true")))),
                disable(hasMoneyFieldValue("ThirdParty.Entity.IDType", "VAT number")),
                setValue('%s', "ThirdParty.Entity.RegistrationNumber", hasMoneyFieldValue("ThirdParty.Entity.IDType", "VAT number")),
                hide().onSection("CDEF")
              ]
            }, {
              field: "ThirdParty.Individual.IDNumber",
              display: [
                hide(notMoneyFieldValue("ThirdParty.Individual.IDType", "Identification number")),
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true")))),
              ]
            }, {
              field: "ThirdParty.Individual.TempResPermitNumber",
              display: [
                hide(notMoneyFieldValue("ThirdParty.Individual.IDType", "Temporary resident permit")),
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true")))),
              ]
            }, {
              field: ["ThirdParty.Individual.PassportNumber", "ThirdParty.Individual.PassportCountry"],
              display: [
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true")))),
                enable(isEmpty).onSection("AB").onCategory(['255', '256'])
              ]
            }, {
              field: "ThirdParty.Entity.RegistrationNumber",
              display: [
                setValue("%s", "ThirdParty.Entity.IDType", evalMoneyField("ThirdParty.Entity.IDType", hasValueIn(["CLUB", "EMBASSY", "GOVERNMENT", "RELIGIOUS BODY", "UNIVERSITY"]))),
                disable(evalMoneyField("ThirdParty.Entity.IDType", hasValueIn(["CLUB", "EMBASSY", "GOVERNMENT", "RELIGIOUS BODY", "UNIVERSITY"]))),
                clearValue(hasValueIn(["CLUB", "EMBASSY", "GOVERNMENT", "RELIGIOUS BODY", "UNIVERSITY"])
                  .and(evalMoneyField("ThirdParty.Entity.IDType", notValueIn(["CLUB", "EMBASSY", "GOVERNMENT", "RELIGIOUS BODY", "UNIVERSITY"])))),
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true"))))
              ]
            }, {
              field: "ThirdParty.Entity.IDType",
              display: [
                setValue(function (context, moneyInd) {
                  var ThirdParty = context.getMoneyField(moneyInd, "ThirdParty");
                  if (ThirdParty.Entity && ThirdParty.Entity.RegistrationNumber) {
                    // Tax Number
                    if (ThirdParty.Entity.RegistrationNumber == ThirdParty.TaxNumber)
                      return "Tax number";
                    // VAT Number
                    if (ThirdParty.Entity.RegistrationNumber == ThirdParty.VATNumber)
                      return "VAT number";
                    // "CLUB", "EMBASSY", "GOVERNMENT", "RELIGIOUS BODY", "UNIVERSITY"
                    if (["CLUB", "EMBASSY", "GOVERNMENT", "RELIGIOUS BODY", "UNIVERSITY"].indexOf(ThirdParty.Entity.RegistrationNumber) != -1)
                      return ThirdParty.Entity.RegistrationNumber;
                    //registration number test
                    if (regNumTest.exec(ThirdParty.Entity.RegistrationNumber))
                      return "Registration number";
                    // trust number test
                    if (trustNumTest.exec(ThirdParty.Entity.RegistrationNumber))
                      return "Trust number";
                  }
                  return "Registration number";//default to this so validations can kick in.
                }, isEmpty.and(hasMoneyField("ThirdParty.Entity"))),
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true"))))
              ]
            }, {
              field: "ThirdParty.Individual.IDType",
              display: [
                setValue(function (context, moneyInd) {
                  var ThirdParty = context.getMoneyField(moneyInd, "ThirdParty");
                  if (ThirdParty.Individual) {
                    if (ThirdParty.Individual.IDNumber)
                      return "Identification number";

                    //if (ThirdParty.Individual.PassportNumber)
                    //  return "Passport";

                    if (ThirdParty.Individual.TempResPermitNumber)
                      return "Temporary resident permit";
                  }
                  return "Identification number"; //default to this for validation's sake.
                }, isEmpty.and(hasMoneyField("ThirdParty.Individual"))),
                disable(not(hasMoneyFieldValue('allowAdhocBOPThirdParty', true).or(hasMoneyFieldValue('allowAdhocBOPThirdParty', "true"))))
              ]
            },
            // The four heading fields below don't actually exist as fields but rather are used
            //  for checking if the headings in the import/export table should be displayed or hidden.
            {
              field: "IE_ICNHeading",
              display: [
                hide(),
                show().onOutflow().onSection("ABG").onCategory(["101", "103", "105", "106"]),
                setLabel("Import Control Number (or MRN)"),
                setLabel("Invoice Number (or MRN)").onOutflow().onSection("ABG").onCategory("101"),
                setLabel("Movement Reference Number (MRN)").onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
              ]
            }, {
              field: "IE_TDNHeading",
              display: [
                hide(),
                show().onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
              ]
            }, {
              field: "IE_UCRHeading",
              display: [
                hide(),
                show().onInflow().onSection("ABG").onCategory(["101", "103", "105", "106"])
              ]
            }, {
              field: "IE_ValidationStatus",
              display: [
                hide(),
                show(notImportUndertakingClient)
                  .onOutflow()
                  .onSection("ABG")
                  .notOnCategory("103/11")
                  .onCategory(["103", "105", "106"])
              ]
            }, {
              field: "ImportExport",
              display: [
                hide(importUndertakingClient).onSection("ABG").onCategory(["105", "106"])
              ]
            }
          ]
        },
        detailImportExport: {
          ruleset: "Std Bank NBOL Import/Export Display Rules",
          scope: "importexport",
          fields: [{
            field: "IVSResponseCodes",
            display: [
              hide(),

              show()
                .onOutflow()
                .onSection("ABG")
                .notOnCategory("103/11")
                .onCategory(["103", "105", "106"]),

              disable(),

              enable(
                evalIEField("ImportControlNumber", isValidICN)
                  .and(evalIEField("TransportDocumentNumber", isTooLong(1)))
                  .and(evalIEField("TransportDocumentNumber", isTooShort(36)))
                  .and(
                  evalTransactionField("Resident.Individual.CustomsClientNumber", hasPattern(/^\d{8,13}$/).and(notValue("70707070")))
                    .or(
                    evalTransactionField("Resident.Entity.CustomsClientNumber", hasPattern(/^\d{8,13}$/)).and(notValue("70707070")))
                    .or(
                    evalMoneyField("ThirdParty.CustomsClientNumber", hasPattern(/^\d{8,13}$/)).and(notValue("70707070")))
                )
                  .and(notImportUndertakingClient)
              ).onOutflow().onSection("ABG")
                .notOnCategory("103/11")
                .onCategory(["103"]),

              enable(
                evalIEField("ImportControlNumber", isValidICN)
                  .and(
                  evalTransactionField("Resident.Individual.CustomsClientNumber", hasPattern(/^\d{8,13}$/).and(notValue("70707070")))
                    .or(
                    evalTransactionField("Resident.Entity.CustomsClientNumber", hasPattern(/^\d{8,13}$/)).and(notValue("70707070")))
                    .or(
                    evalMoneyField("ThirdParty.CustomsClientNumber", hasPattern(/^\d{8,13}$/)).and(notValue("70707070")))
                )
                  .and(notImportUndertakingClient)
              ).onOutflow().onSection("ABG")
                .onCategory(["105", "106"])
            ]
          }, {
            field: "TransportDocumentNumber",
            display: [
              hide(),
              show().onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
            ]
          }, {
            field: "ImportControlNumber",
            display: [
              hide(),
              show().onOutflow().onSection("ABG").onCategory(["101", "103", "105", "106"]),
              setValue("INV", null, notPattern(/^INV.+$/))
                .onOutflow().onSection("ABG").onCategory("101")
            ]
          }]
        }
      }
    }
    return display;
  }
});