/**
 * Created by petruspretorius on 09/12/2016.
 */
define(function () {
  return function (predef) {

    var rateMatcher = /^([\w\s]+?)?(PLUS|MINUS)? ?(\d+\.\d+)?$/;

    predef = Object.assign(predef, {
      /**
       * Check to see if there is a MonetaryAmount with a dispensation category, and if so, checks to see if there are CCNs available for dispensation
       * The Assumption is that all CCNs will be the same, so just pick the first one.
       * @param context
       * @param value
       * @returns {boolean}
       */
      hasDispensationCCN: function (context, value) {
        var cats = context.categories;
        var dispensations = _.filter(context.getCustomValue('dispensation'), function (item) {
          return cats.indexOf(item.category) != -1;
        });
        var CCNs = _.filter(dispensations, function (item) {
          return item.CustomsClientNumber;
        })
        return !!CCNs.length;
      },

      /**
       * Get the first available dispensation CCN matching selected categories.
       * @param context
       * @returns the first dispensation CCN
       */
      getDispensationCCN: function (context) {
        var cats = _.map(context.MonetaryAmount, function (item) {
          return item.Category;
        });
        var dispensations = _.filter(context.getCustomValue('dispensation'), function (item) {
          return cats.indexOf(item.category) != -1;
        });
        var CCNs = _.map(_.filter(dispensations, function (item) {
          return !!item.CustomsClientNumber;
        }), function (item) {
          return item.CustomsClientNumber;
        })
        return CCNs[0];
      },

      convertInterestRate: function (context) {
        var out = [];
        var bsRate = context.getMoneyField(context.currentMoneyInstance, 'LoanInterest.Index');
        if (bsRate) {
          out.push(bsRate);
        }
        var plsMin = context.getMoneyField(context.currentMoneyInstance, 'LoanInterest.PlusMinus');
        if (plsMin) {
          out.push(plsMin);
        }
        var rate = context.getMoneyField(context.currentMoneyInstance, 'LoanInterest.Rate');

        if (rate) {
          try {
            rate = new Number(rate).toFixed(2).toString();
          } catch (e) {
            rate = "0.00";
          }
          out.push(rate);
        }

        return out.join(' ');
      },

      getInterestRate: function (context) {
        var rate = context.getMoneyField(context.currentMoneyInstance, 'LoanInterestRate');
        return rateMatcher.exec(rate)[3];
      },

      getPlusMinus: function (context) {
        var rate = context.getMoneyField(context.currentMoneyInstance, 'LoanInterestRate');
        var match = rateMatcher.exec(rate);
        if (match[1] == "PLUS" || match[1] == "MINUS") {
          return match[1]
        } else {
          return match[2]
        }
      },

      getBaseRate: function (context) {
        var rate = context.getMoneyField(context.currentMoneyInstance, 'LoanInterestRate');
        var match = rateMatcher.exec(rate);
        if (match[1] == "PLUS" || match[1] == "MINUS") {
          return "";
        } else {
          return match[1];
        }
      },

      getInterestType: function (context) {
        var rate = context.getMoneyField(context.currentMoneyInstance, 'LoanInterestRate');
        var match = rateMatcher.exec(rate);
        if (typeof match[1] != "undefined") {
          return "INDEXED LINKED";
        } else {
          return "FIXED";
        }
      },
      //thirdparty entity registration number test
      regNumTest: /^\d{4}\/\d{6}\/\d{2}/,

      //thirdparty entity trust number test
      trustNumTest: /^IT\/\d{1,6}\/\d{1,4}/,

      notHasDispensation: function (context) {
        var cat = context.categories[context.currentMoneyInstance];
        var dispensation = _.find(context.getCustomValue('dispensation'), function (item) {
          return item.category == cat;
        });
        return !dispensation;
      },
      stdBankValidZAVATNumber: function (context, value) {
        var isChecksumCorrect = true;
        if (!(value && (typeof value == "string" || value.trim().length > 0))) {
          isChecksumCorrect = false;
        }

        if (value === "NO VAT NUMBER")
          return true;

        //- The length must be 10: 4090103146
        //- Must start with a 4:  4090103146
        //- The 4th digit must be 0: 4090103146
        //- The 5th digit must either be 1 or 2: 4090103146 or 4410229209

        if (value.match(/[^0-9]+/g, '')) {
          isChecksumCorrect = false;
        }

        var vatNumber = value.split('');

        if (vatNumber.length != 10) {
          isChecksumCorrect = false;
        }
        else if (vatNumber[0] !== "4") {
          isChecksumCorrect = false;
        }
        else if (vatNumber[4 - 1] !== "0") {
          isChecksumCorrect = false;
        }
        else if (vatNumber[5 - 1] !== "1" && vatNumber[5 - 1] !== "2") {
          isChecksumCorrect = false;
        }

        //console.log(isChecksumCorrect, vatNumber.length, vatNumber[4 - 1], vatNumber[5 - 1]);
        return isChecksumCorrect;
      },

      stdBankValidZATaxNumber: function (context, val) {
        var isChecksumCorrect = false;

        if (!(val && (typeof val == "string" || val.trim().length > 0)))
          return isChecksumCorrect; //invalid
        /*
         0254/089/06/3,
         * Ignore all non-numeric values.
         * Take each digit in an ODD position and multiply it by 2.
         * Total A (0x2) + (5x2) +(0x2) + (9x2) + (6x2)
         0    +  1+0   +   0    +  1+8  + 1+2
         Add these values giving Total A.   13
    
         Add all the digits in EVEN positions giving Total B.
         Total B 2 + 4 + 8 + 0 = 14 (ignore last digit, as that is the check digit)
    
         Add Total A and Total B together to give you Total C Total C 13 + 14 = 27
    
         Subtract Total C from the next highest multiple of 10, giving the check digit.
    
         Check digit 30 � 27 = 3
         The last digit i.e. 3 is the check digit.
    
         */

        var taxNumber = val
          .replace(/[^0-9]+/g, '')
          .split('');

        function checksum(value, digits) {
          var totA = 0;
          for (var i = 0; i < digits.length - 1; i = i + 2) {
            var numerics = (digits[i] * 2).toString().split('');
            for (var k = 0; k < numerics.length; k++) {
              totA = totA + (numerics[k] * 1);
            }
          }

          var totB = 0;
          for (var j = 1; j < digits.length - 1; j = j + 2) {
            totB = totB + (digits[j] * 1);
          }

          var totC = totA + totB;

          //console.log(totA, totB, totC);
          return 10 - (totC % 10);
        }

        isChecksumCorrect = checksum(val, taxNumber) === taxNumber[taxNumber.length - 1] * 1;

        //console.log(taxNumber, checksum);

        return isChecksumCorrect;
      },

      anyMoneyCustomValueDispensationHasNbol: function (field, constant) {
        return function (context) {
          var dispensation = context.getCustomValue("dispensation");

          if (!dispensation)
            return false;

          for (var i = 0; i < dispensation.length; i++) {

            var ma = dispensation[i];
            if ((ma[field] ? ma[field].toString() : "").indexOf(constant) >= 0)
              return true;
          }
          return false;
        };
      },
      filterThirdPartyIndividual: function (value) {
        //console.log("filterThirdPartyIndividual", value);
        if (value == '-') {
          return true;
        }
        return (typeof value.Individual !== 'undefined');
      }
    });

    return predef;
  }
});