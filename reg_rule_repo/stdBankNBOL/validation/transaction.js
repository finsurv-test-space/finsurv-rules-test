/**
 * Created by petruspretorius on 26/06/2015.
 */
define(function () {
  return function (predef) {
    var transaction;
    with (predef) {
      transaction = {
        ruleset: "Transaction Rules for Standard Bank",
        scope: "transaction",
        validations: [
          {
            field: "NonResident.Type",
            rules: [
              failure("stdNBOL_nnrtyp0", "S99", "Non-resident type required",
                notValueIn(["Individual", "Entity", "Exception"])
              )
            ]
          },
          {
            field: "ValueDate",
            rules: [
              ignore('vd2')
            ]
          },
          {
            field: ["Resident.Individual.TaxNumber", "Resident.Entity.TaxNumber"],
            rules: [
              failure('std_resitn', 3221, "Invalid tax number",
                notEmpty.and(not(stdBankValidZATaxNumber))
              )
            ]
          },
          {
            field: ["Resident.Individual.VATNumber", "Resident.Entity.VATNumber"],
            len: 10,
            rules: [
              failure('std_resivn', 3222, "Invalid VAT number",
                notEmpty.and(not(stdBankValidZAVATNumber))
              )
            ]
          },
          {
            field: "Resident.Entity.TradingName",
            rules: [
              failure("std_retn1", 3304, "Must not contain special characters = ! \" % _ & * < > ; { } @ # $ ^ * [ ] | ~ \\",
                notEmpty.and(hasPattern(/\=|\!|\"|\%|\_|\&|\*|\<|\>|\;|\{|\}|\@|\#|\$|\^|\*|\[|\]|\||\~|\\/)))
            ]
          },

          {
            field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],
            rules: [

              failure('stdNBOL_ccn4', 320, 'CustomsClientNumber must be numeric and contain bewteen 8 and 13 digits',
                notEmpty.and(notPattern(/^\d{8,13}$/)))
                .onOutflow()
                .onSection("AB")
                .onCategory(["101", "102", "104", "103", "105", "106", "271"]),
              ignore("ccn7"),
              warning("stdNBOL_ccn7", "S09", "Unless the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106 or 271, this need not be provided and if invalid will cause the SARB to reject transaction",
                notEmpty)
                .onSection("AB")
                .notOnCategory(["101", "102", "104", "103", "105", "106", "271"])
            ]
          },
          {
            field: "TrnReference",
            rules: [
              ignore("tref1")
            ]
          },

          // HOLDO Rules


          {
            field: "NonResident.Entity.EntityName",
            rules: [
              failure("hc_nren1", "H01", "The Resident Legal Entity name is not equal to the name of HOLDCO in the HOLDCO list",
                notEmpty.and(hasCustomValue('holdCo',true)).
                  and(not(hasLookupTransactionFieldValue('holdcoCompanies', 'companyName', 'accountNumber', 'NonResident.Entity.AccountNumber'))).
                  and(hasTransactionField("Resident.Individual").or(hasTransactionField("Resident.Entity")))).onSection("A"),
              failure("hc_nren2", "H01", "The Resident Legal Entity name is not equal to the name of HOLDCO in the HOLDCO list",
                notEmpty.and(evalTransactionField('NonResident.Entity.AccountNumber', isInLookup('holdcoCompanies', 'accountNumber'))).
                  and(not(hasLookupTransactionFieldValue('holdcoCompanies', 'companyName', 'accountNumber', 'NonResident.Entity.AccountNumber'))).
                  and(hasResException("FCA NON RESIDENT NON REPORTABLE"))).onSection("C")
            ]
          },
          {
            field: "NonResident.Entity.AccountIdentifier",
            rules: [
              failure("hc_nrea1", "H02", "Non Resident Account Identifier must be NON RESIDENT FCA for reporting of HOLDCO transactions",
                notValue("NON RESIDENT FCA").and(hasCustomValue('holdCo',true)).
                  and(hasTransactionField("Resident.Individual").or(hasTransactionField("Resident.Entity")))).onSection("A"),
              failure("hc_nrea2", "H02", "Non Resident Account Identifier must be NON RESIDENT FCA for reporting of HOLDCO transactions",
                notValue("NON RESIDENT FCA").and(evalNonResidentField("AccountNumber", isInLookup('holdcoCompanies', 'accountNumber'))).
                  and(hasResException("FCA NON RESIDENT NON REPORTABLE"))).onSection("C")
            ]
          },
          {
            field: "Resident.Individual.AccountIdentifier",
            rules: [
              failure("hc_accid1", "H03", "Resident Account Identifier must be FCA RESIDENT for reporting of HOLDCO transactions",
                notValue("FCA RESIDENT").and(hasCustomValue('holdCo',true))).onSection("A")
            ]
          },
          {
            field: "Resident.Entity.AccountIdentifier",
            rules: [
              failure("hc_accid2", "H03", "Resident Account Identifier must be FCA RESIDENT for reporting of HOLDCO transactions",
                notValue("FCA RESIDENT").and(hasCustomValue('holdCo',true))).onSection("A")
            ]
          },
          {
            field: "Resident.Exception.Country",
            rules: [
              failure("hc_rec1", "H04", "Country must be linked to the currency used when reporting HOLDCO transactions (EU must be used for EUR payments)",
                notMatchToCurrency.and(hasCustomValue('holdCo',true)).
                  and(hasResException("FCA NON RESIDENT NON REPORTABLE"))).onSection("C")
            ]
          },
          {
            field: "NonResident.Individual.AccountIdentifier",
            rules: [
              failure("hc_nraid1", "H05", "Non Resident Account Identifier must be 'NON RESIDENT RAND' for reporting of HOLDCO transactions",
                notEmpty.and(notValue("NON RESIDENT RAND")).
                  and(hasAnyMoneyFieldValue("AdHocRequirement.Subject", "HOLDCO")).
                  and(isCurrencyIn("ZAR")).and(hasResException("NON RESIDENT RAND")).
                  and(not(hasCustomValue('holdCo',true)))).onSection("A")
            ]
          },
          {
            field: "NonResident.Entity.AccountIdentifier",
            rules: [
              failure("hc_nraid2", "H05", "Non Resident Account Identifier must be 'NON RESIDENT RAND' for reporting of HOLDCO transactions",
                notEmpty.and(notValue("NON RESIDENT RAND")).
                  and(hasAnyMoneyFieldValue("AdHocRequirement.Subject", "HOLDCO")).
                  and(isCurrencyIn("ZAR")).and(hasResException("NON RESIDENT RAND")).
                  and(not(hasCustomValue('holdCo',true)))).onSection("A")
            ]
          }
        ]
      }
    }

    return transaction;
  }
});