﻿/**
 * Created by Sasha Slankamenac on 2015-06-04.
 */
define(function () {

  //thirdparty entity registration number test
  var regNumTest = /^\d{4}\/\d{6}\/\d{2}/;

  //thirdparty entity trust number test
  var trustNumTest = /^IT\/\d{1,6}\/\d{1,4}/;

  var notHasDispensation = function (context) {
    var cat = context.categories[context.currentMoneyInstance];
    var dispensation = _.find(context.getCustomValue('dispensation'), function (item) {
      return item.category == cat;
    });
    return !dispensation;
  }

  return function (predef) {
    var money;

    with (predef) {
      money = {
        ruleset: "Standard Money Rules",
        scope: "money",
        validations: [
          {
            field: "CategoryCode",
            rules:[
              failure("stdbcc1","SB1","No dispensation found for this category",
                notEmpty.and(notHasDispensation)
              ).onSection("A").onCategory(['102','104']),
              failure("stdbcc2","SB2","No dispensation found for this category",
                notEmpty.and(importUndertakingClient).and(notHasDispensation)
              ).onSection("A").onCategory(['105','106'])
            ]
          },
          {
            field: "SequenceNumber",
            rules: [
              ignore("mseq1")
            ]
          },
          {
            field: [
              "ThirdParty.TaxNumber",
              "ThirdParty.ContactDetails.ContactName", "ThirdParty.ContactDetails.ContactSurname",
              "ThirdParty.StreetAddress.AddressLine1", "ThirdParty.StreetAddress.Suburb",
              "ThirdParty.StreetAddress.Province", "ThirdParty.StreetAddress.City", "ThirdParty.StreetAddress.PostalCode",
              "ThirdParty.PostalAddress.AddressLine1", "ThirdParty.PostalAddress.Suburb",
              "ThirdParty.PostalAddress.Province", "ThirdParty.PostalAddress.City", "ThirdParty.PostalAddress.PostalCode"

            ],
            rules: [
              failure('std_tpmand1', 322, "Mandatory if saving 3rd party",
                isEmpty.and(
                  hasMoneyFieldValue("saveAdhocBOPThirdParty", true)
                ))
            ]
          },
          {
            field: ["ThirdParty.Individual.TaxNumber", "ThirdParty.Entity.TaxNumber"],
            rules: [
              failure('std_tptn', 3220, "Invalid tax number",
                notEmpty.and(not(stdBankValidZATaxNumber))
              )
            ]
          },
          {
            field: ["ThirdParty.Individual.VATNumber", "ThirdParty.Entity.VATNumber"],
            len: 10,
            rules: [
              failure('std_tpvn', 3223, "Invalid VAT number",
                notEmpty.and(not(stdBankValidZAVATNumber))
              )
            ]
          },
          {
            field: "ThirdParty.Individual.Surname",
            rules: [
              failure('std_tpmmand1', 322, "Mandatory if specifying 3rd party.",
                isEmpty.and(hasMoneyField("ThirdParty.Individual"))
              )
            ]
          },
          {
            field: "ThirdParty.Entity.Name",
            rules: [
              failure('std_tpmmand2', 322, "Mandatory if specifying 3rd party.",
                isEmpty.and(hasMoneyField("ThirdParty.Entity"))
              )
            ]
          },
          {
            field: [
              "ThirdParty.Individual.Name", "ThirdParty.Individual.Gender", "ThirdParty.Individual.DateOfBirth"
            ],
            rules: [
              failure('std_tpmand2', 322, "Mandatory if saving 3rd party",
                isEmpty.and(
                  hasMoneyField("ThirdParty.Individual")
                    .and(hasMoneyFieldValue("saveAdhocBOPThirdParty", true))
                ))
            ]
          },
          {
            field: ["ThirdParty.Entity.RegistrationNumber", "ThirdParty.Entity.Name"],
            rules: [
              failure('std_tpmand3', 322, "Mandatory if saving 3rd party",
                isEmpty.and(
                  hasMoneyField("ThirdParty.Entity")
                    .and(hasMoneyFieldValue("saveAdhocBOPThirdParty", true))
                ))
            ]
          },
          {
            field: ["ThirdParty.ContactDetails.Email", "ThirdParty.ContactDetails.Telephone", "ThirdParty.ContactDetails.Fax"],
            rules: [
              failure('std_tpmand4', 322, "Mandatory if saving 3rd party - At least one of Email, Telephone numer or Fax number should be provided.",
                not(
                  hasMoneyField("ThirdParty.ContactDetails.Email")
                    .or(hasMoneyField("ThirdParty.ContactDetails.Telephone"))
                    .or(hasMoneyField("ThirdParty.ContactDetails.Fax"))
                ).and(hasMoneyFieldValue("saveAdhocBOPThirdParty", true)))
            ]
          },
          {
            field: ["ThirdParty.ContactDetails.Telephone", "ThirdParty.ContactDetails.Fax"],
            rules: [
              failure('std_tpnump1', 322, "May only contain numbers and the characters: +,-,(,)",
                notEmpty.and(notPattern(/^(\+|\(|\)|-|\d)+$/)))
            ]
          },
          {
            field: "ThirdParty.Entity.RegistrationNumber",
            rules: [
              failure('std_tpern1', 322, 'Invalid Registration Number: should be of the form YYYY/NNNNNN/NN (please check number)',
                notPattern(regNumTest).and(hasMoneyFieldValue("ThirdParty.Entity.IDType", "Registration number"))),
              failure('std_tpern2', 322, "Invalid Trust Number: should be of the form IT/n[nnnnn]/n[nnn]: E.g. IT/12/76",
                notPattern(trustNumTest).and(hasMoneyFieldValue("ThirdParty.Entity.IDType", "Trust number"))),
              failure('std_tpern3', 322, "Must be completed",
                isEmpty.and(
                  hasMoneyFieldValue("ThirdParty.Entity.IDType", "Tax number")
                    .or(hasMoneyFieldValue("ThirdParty.Entity.IDType", "VAT number"))
                )
              ),
              failure('std_tpern4', 322, 'Predefined Third Party with that Registration number and CCN already exists. Rather select it from the list.',
                notEmpty.and(function (context, value) {
                  // check if this thirdparty is from the list...
                  var tps = context.lookups.lookups.thirdparties;
                  var thisThirdParty = context.getMoneyField(context.currentMoneyInstance, 'ThirdParty');

                  var match = _.find(tps, function (item) {
                    return item.value == thisThirdParty;
                  });
                  if (!!match) return false;

                  //filter out the entities...
                  match = _.find(tps, function (item) {
                    if (item.value.Entity && (item.value.Entity.RegistrationNumber == value)) {
                      return item.value.CustomsClientNumber == thisThirdParty.CustomsClientNumber;
                    }
                    return false;
                  });
                  return !!match;

                }))

            ]
          },
          {
            field: "ThirdParty.Individual.IDNumber",
            rules: [
              failure('std_tpiid1', 322, 'Must be completed',
                isEmpty.and(hasMoneyFieldValue("ThirdParty.Individual.IDType", "Identification number"))),
              failure('std_tpiid2', 322, 'Predefined Third Party with that IDNumber number and CCN already exists. Rather select it from the list.',
                notEmpty.and(function (context, value) {
                  // check if this thirdparty is from the list...
                  var tps = context.lookups.lookups.thirdparties;
                  var thisThirdParty = context.getMoneyField(context.currentMoneyInstance, 'ThirdParty');

                  var match = _.find(tps, function (item) {
                    return item.value == thisThirdParty;
                  });

                  if (!!match) return false;

                  //filter out the entities...
                  match = _.find(tps, function (item) {
                    if (item.value.Individual && (item.value.Individual.IDNumber == value)) {
                      return item.value.CustomsClientNumber == thisThirdParty.CustomsClientNumber;
                    }
                    return false;
                  });
                  return !!match;

                }))
            ]
          },
          {
            field: "ThirdParty.Individual.PassportNumber",
            rules: [
              failure('std_tpipn1', 322, 'Must be completed',
                isEmpty.and(hasMoneyFieldValue("ThirdParty.Individual.IDType", "Passport")))
            ]
          },
          {
            field: "ThirdParty.Individual.TempResPermitNumber",
            rules: [
              failure('std_tpitrpn1', 322, 'Must be completed',
                isEmpty.and(hasMoneyFieldValue("ThirdParty.Individual.IDType", "Temporary resident permit"))),
              failure('std_tpitrpn2', 322, 'Predefined Third Party with that Passport Number and CCN already exists. Rather select it from the list.',
                notEmpty.and(function (context, value) {
                  // check if this thirdparty is from the list...
                  var tps = context.lookups.lookups.thirdparties;
                  var thisThirdParty = context.getMoneyField(context.currentMoneyInstance, 'ThirdParty');

                  var match = _.find(tps, function (item) {
                    return item.value == thisThirdParty;
                  });
                  if (!!match) return false;

                  //filter out the entities...
                  match = _.find(tps, function (item) {
                    if (item.value.Individual && (item.value.Individual.TempResPermitNumber == value)) {
                      return item.value.CustomsClientNumber == thisThirdParty.CustomsClientNumber;
                    }
                    return false;
                  });
                  return !!match;

                }))
            ]
          },
          {
            field: "ThirdParty.ContactDetails.Email",
            rules: [
              failure("std_tpeml", "s99", "This email is not valid", notEmpty.and(notValidEmail))
            ]
          },
          {
            field: "LoanRefNumber",
            rules: []
          },
          {
            field: "LoanInterestRate",
            rules: [
              ignore("mlir1"),
              ignore("mlir2"),
              ignore("mlir3"),
              //ignore("mlir4"),
              ignore("mlir5"),
              //ignore("mlir6"),
              ignore("mlir7")
              //ignore("mlir8")
            ]
          },
          {
            field: "LoanInterest.Rate",
            minLen: 1,
            maxLen: 25,
            rules: [
              failure("std_mlir1", 378, "If the Flow is Out and CategoryCode 810 or 815 or 816 or 817 or 818 or 819 is used, must contain a value of reflecting interest rate percentage of the loan in the proper format. E.g. 0.00, 5.12",
                notPattern("^(\\d+(\\.\\d{1,2}))$")
                  .and(notMoneyFieldValue("LoanInterest.InterestType", ["INDEXED LINKED"]).or(hasMoneyField("LoanInterest.PlusMinus")))
                //
              )
                .onOutflow().onSection("AB")
                .onCategory(["810", "815", "816", "817", "818", "819"])

              , failure("std_mlir2", 379, "May not be completed",
                notEmpty).onSection("CDEF"),

              failure("std_mlir3", 380, "If the Flow is Out and CategoryCode 106 or 309/04 to 309/07 is used, must be completed reflecting the percentage interest paid",
                notPattern("^\\d{1,3}\\.\\d{1,2}?$")
              )
                .onOutflow().onSection("ABG")
                .onCategory([/*"106",*/ "309/04", "309/05", "309/06", "309/07"])

              , warning("std_mlir5", "S12", "It is unlikely that an interest rate greater than 100% is being charged",
                hasPattern("^\\d{3}\\.\\d{2}?$"))
                .onOutflow().onSection("ABG")
                .onCategory([/*"106",*/ "309/04", "309/05", "309/06", "309/07"])

              , failure("std_mlir7", 379, "May not be completed unless a loan related transaction is being captured",
                notEmpty)
                .onOutflow().onSection("ABG")
                .notOnCategory([/*"801", "802", "803", "804",*/ "810", "815", "816", "817", "818", "819", "309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07" /*, "106"*/])
            ]
          },

          {
            field: "AdHocRequirement.Subject",
            rules: [
              failure("hc_madhs1", "H06", "Subject must contain HOLDCO when reporting transactions for HOLDCO companies",
                hasCustomValue('holdCo',true).
                  and(hasTransactionField("Resident.Individual").or(hasTransactionField("Resident.Entity"))).
                  and(isEmpty.or(notValue("HOLDCO")))).onSection("A"),
              failure("hc_madhs2", "H07", "Resident Exception name must be 'NON RESIDENT RAND' for reporting of ZAR related HOLDCO transactions",
                hasValue("HOLDCO").and(notResException("NON RESIDENT RAND")).
                  and(isCurrencyIn("ZAR")).
                  and(not(hasCustomValue('holdCo',true)))).onSection("A")
            ]
          }, {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
            rules: [
              ignore("maian2"),
              failure("maian2c", 382, "May not be completed",
                notEmpty.and(notCustomValue("exCon", true))).onSection("CDEF"),
            ]
          }, {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
            rules: [
              ignore("maiad2"),
              failure("maiad2c", 382, "May not be completed",
                notEmpty.and(notCustomValue("exCon", true))).onSection("CDEF")
            ]
          }, {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
            rules: [

              ignore("masan3"),
              failure("masan3c", 382, "May not be completed",
                notEmpty.and(notCustomValue("exCon", true))).onSection("CDEF"),
            ]
          }, {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
            rules: [
              ignore("masar3"),
              failure("masar3c", 382, "May not be completed",
                notEmpty.and(notCustomValue("exCon", true))).onSection("CDEF"),
              failure("sb_masar2", 389, "Must be in the format CCYY-nnnnnnnn where CC is the Century, YY is the year and nnnnnnnn is the e-docs number or any other SARB authorisation number allocated by the SARB in the reply to the application",
                      notEmpty.and(notPattern("^(19|20)\\d{2}-\\d{8}$"))).onSection("ABG")
            ]
          }, {
            field: "{{Regulator}}Auth.RulingsSection",
            minLen: 2,
            maxLen: 30,
            rules: [
              ignore("mars1")
              , failure("std_mars1", 381, "Must be completed if the Flow is OUT and no data is supplied under either {{DealerPrefix}}InternalAuthNumber or {{RegulatorPrefix}}AuthAppNumber",
                isEmpty
                  .and(evalMoneyField("Category",hasLookupValue("Dispensation","category","SectionOfRulings")))
                  .and(notMoneyField("{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber")
                    .and(notMoneyField("{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber")))
              ).onOutflow().onSection("ABG").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),

            ]
          },{
            field: "SARBAuth.AuthIssuer",
            rules:[
              failure("hc_maian1", "H09", "Mandatory for reporting of a HOLDCO transaction",
                hasCustomValue('holdCo',true).
                  and(hasTransactionField("Resident.Individual").or(hasTransactionField("Resident.Entity"))).
                  and(isEmpty)).onSection("A")
            ]
          }, {
            field: "ImportExport",
            rules: [
              ignore("mtie4"),
              failure("stdBank_mtie4a", 490, "If outflow and the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, the ImportExport element must be completed unless a) the Subject is SDA or REMITTANCE DISPENSATION or b) category 106 and import undertaking client",
                emptyImportExport
                  .and(notMoneyFieldValue("AdHocRequirement.Subject", ["SDA", "REMITTANCE DISPENSATION"])
                    .and(not(hasMoneyFieldValue("CategoryCode", "106")
                      .and(importUndertakingClient))))
              ).onOutflow().onSection("A").onCategory(["101", "103"])
                .notOnCategory(["101/11", "103/11"]),

              failure("stdBank_mtie4b", 490, "If outflow and the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, the ImportExport element must be completed unless a) the Subject is SDA or REMITTANCE DISPENSATION or b) category 106 and import undertaking client",
                notImportUndertakingClient
                  .and(emptyImportExport)
                  .and(notMoneyFieldValue("AdHocRequirement.Subject", ["SDA", "REMITTANCE DISPENSATION"])
                    .and(not(hasMoneyFieldValue("CategoryCode", "106")
                      .and(importUndertakingClient))))
              ).onOutflow().onSection("A").onCategory(["105", "106"])
            ]
          },{
            field: "ReversalTrnSeqNumber",
            rules:[
              failure("sb_mrtrs1", 285, "May not be 0",
                notEmpty.and(hasValueIn(['0','00','000']))),
              failure("sb_mrtrs2",285, "Should be numeric",
                notEmpty.and(notPattern(/^\d*$/)))
            ]
          }
        ]
      }
    }

    return money;
  }
});