﻿define(function () {
  return function (predef) {
    var _export;
    with (predef) {
      _export = {
        ruleset: "Standard Import/Export Rules",
        scope: "importexport",
        validations: [
          {
            field: "IVSResponseCodes",
            rules: [
              validate("stdCE_ieicn1", "IVS_Validate",
                evalIEField("ImportControlNumber", isValidICN)
                  .and(evalIEField("TransportDocumentNumber", isTooLong(1)))
                  .and(evalIEField("TransportDocumentNumber", isTooShort(36)))
                  .and(isButtonPressed("IVSResponseCodes"))
                  .and(notImportUndertakingClient)
                  .and(
                  evalTransactionField("Resident.Individual.CustomsClientNumber", hasPattern(/^\d{8,13}$/))
                    .or(
                    evalTransactionField("Resident.Entity.CustomsClientNumber", hasPattern(/^\d{8,13}$/)))
                    .or(
                    evalMoneyField("ThirdParty.CustomsClientNumber", hasPattern(/^\d{8,13}$/)))
                )
              )
                .onOutflow()
                .onSection("ABG")
                .notOnCategory("103/11")
                .onCategory(["103", "105", "106"])
            ]
          }]
      };
    }

    return _export;
  }
});