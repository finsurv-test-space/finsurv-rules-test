define(function () {
  return function (evaluationEx) {
    var assumptions;
    with (evaluationEx) {
      assumptions = [
        {
          knownSide: drcr.DR,
          resStatus: resStatus.NONRES,
          accType: [accType.NOSTRO, accType.VOSTRO],
          rules: [
            use(resStatus.IHQ, accType.LOCAL_ACC, all)
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.RESIDENT,
          accType: [accType.NOSTRO],
          rules: [
            use(resStatus.IHQ, accType.LOCAL_ACC, all)
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.NONRES,
          accType: [accType.CFC],
          rules: [
            use(resStatus.RESIDENT, accType.CFC, all)
          ]
        },
        {
          knownSide: drcr.DR,
          resStatus: resStatus.RESIDENT,
          accType: [accType.VOSTRO],
          rules: [
            use(resStatus.RESIDENT, accType.CFC, all)
          ]
        }
      ]
    }
    return assumptions;
  }
})