define(function () {
  return function (evaluationEx) {
    var evaluations;
    with (evaluationEx) {
      evaluations = [
        { // NOTE: This was added to support nBOL's 'NOT SUPPORTED' assumptions. They get mapped to this, but this decision on its own is meaningless.
          drResStatus: resStatus.NONRES,
          drAccType: [accType.NOSTRO, accType.VOSTRO],
          crResStatus: resStatus.IHQ,
          crAccType: [accType.LOCAL_ACC],
          validFrom: "2014-12-19",
          decision: {
            reportable: rep.UNSUPPORTED
          }
        },
        { // NOTE: This was added to support nBOL's 'NOT SUPPORTED' assumptions. They get mapped to this, but this decision on its own is meaningless.
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.IHQ,
          crAccType: [accType.LOCAL_ACC],
          validFrom: "2014-12-19",
          decision: {
            reportable: rep.UNSUPPORTED
          }
        }
        
      ];
    }
    return evaluations;
  }
});