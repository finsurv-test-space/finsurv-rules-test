define(function () {
  return function (predef) {
    var extMoney;
    with (predef) {

      extMoney = {
        ruleset: "SARB Manual B4 Money Rules (defined outside of E1 spec)",
        scope: "money",
        validations: [
          {
            field: "LocationCountry",
            rules: [
              failure("sb4_mlc1", "Q10", "For IHQ companies (based on company reg no) if flow is OUT, the location country should be that of the receiving country code [SARB Manual B4 A(i)f]",
                notEmpty.and(evalTransactionField('Resident.Entity.RegistrationNumber', isInLookup('ihqCompanies', 'registrationNumber'))).
                and(notMatchesTransactionField("ReceivingCountry"))).onOutflow().onSection("A"),
              failure("sb4_mlc2", "Q11", "For IHQ companies (based on company reg no) if flow is IN, the location country should be that of the originating country code [SARB Manual B4 A(i)f]",
                notEmpty.and(evalTransactionField('Resident.Entity.RegistrationNumber', isInLookup('ihqCompanies', 'registrationNumber'))).
                and(notMatchesTransactionField("OriginatingCountry"))).onInflow().onSection("A")
            ]
          },
          {
            field: "AdHocRequirement.Subject",
            rules: [
              failure("sb4_madhs1", "Q12", "For IHQ companies (based on company reg no) the Subject must be IHQ followed by the number of the head quarter company allocated by the Financial Surveillance Department",
                evalTransactionField('Resident.Entity.RegistrationNumber', isInLookup('ihqCompanies', 'registrationNumber')).
                and(not(hasLookupTransactionFieldValue('ihqCompanies', 'ihqCode', 'registrationNumber', 'Resident.Entity.RegistrationNumber')))).onSection("A")
            ]
          }
        ]
      };

    }
    return extMoney;
  }
});
