define(function () {
  return function (predef) {
    var feature;
    with (predef) {

      feature = {
        ruleset: "SARB Manual B4 Transaction Rules (defined outside of E1 spec)",
        scope: "transaction",
        validations: [
          {
            field: "NonResident.Entity.EntityName",
            rules: [
              failure("sb4_nren1", "Q01", "For IHQ companies (based on company reg no) the Non Resident Entity name cannot be a name that belongs to an IHQ company in the IHQ table [SARB Manual B4 A(i)b]",
                notEmpty.and(evalTransactionField('Resident.Entity.RegistrationNumber', isInLookup('ihqCompanies', 'registrationNumber'))).
                and(isInLookup('ihqCompanies', 'companyName'))).onSection("A")
            ]
          },
          {
            field: "NonResident.Exception.ExceptionName",
            rules: [
              failure("sb4_nrexn1", "Q02", "Local transactions involving an IHQ company and a local company must be classified as BOPCUS",
                hasValue("IHQ").and(hasTransactionField("Resident.Entity"))).onSection("BCDEFG"),
            ]
          },
          {
            field: "NonResident.Entity.AccountNumber",
            rules: [
              failure("sb4_nrian1", "Q03", "Local transactions involving a HOLDCO company (based on account no) and a local company must be classified as BOPCUS",
                notEmpty.and(isInLookup('holdcoCompanies', 'accountNumber')).and(hasTransactionField("Resident.Entity"))).onOutflow().onSection("BCDEFG")
            ]
          },
          {
            field: "Resident.Entity.EntityName",
            rules: [
              failure("sb4_ren1", "Q04", "For HOLDCO companies (based on nonres account no) the Resident Entity name cannot be a name that belongs to a HOLDCO company in the HOLDCO table [SARB Manual B4 B(i)b]",
                notEmpty.and(evalTransactionField('NonResident.Entity.AccountNumber', isInLookup('holdcoCompanies', 'accountNumber'))).
                and(isInLookup('holdcoCompanies', 'companyName'))).onSection("A")
            ]
          },
          {
            field: "Resident.Entity.RegistrationNumber",
            rules: [
              failure("sb4_rern1", "Q05", "For HOLDCO companies (based on nonres account no) the Resident Entity registration no. cannot be one that belongs to a HOLDCO company in the HOLDCO table [SARB Manual B4 B(i)b]",
                notEmpty.and(evalTransactionField('NonResident.Entity.AccountNumber', isInLookup('holdcoCompanies', 'accountNumber'))).
                and(isInLookup('holdcoCompanies', 'registrationNumber'))).onSection("A")
            ]
          }
        ]
      };
    }
    return feature;
  }
});

