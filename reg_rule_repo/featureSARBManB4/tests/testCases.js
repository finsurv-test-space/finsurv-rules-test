define(function() {
  return function(testBase) {
      with (testBase) {
        var mappings = {
          Regulator: "SARB",
          DealerPrefix: "AD",
          RegulatorPrefix: "SARB"
        };
        var test_cases = [
          // For IHQ companies (based on company reg no) the Non Resident Entity name cannot be a name that belongs to an IHQ company in the IHQ table [SARB Manual B4 A(i)b]
          assertSuccess("sb4_nren1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {EntityName: "local company"}},
            Resident: {Entity: {RegistrationNumber: '2013/1234567/07'}}
          }),
          assertFailure("sb4_nren1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {EntityName: "ihq comp 1"}},
            Resident: {Entity: {RegistrationNumber: '2013/1234567/07'}}
          }),
          assertSuccess("sb4_nren1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {EntityName: "ihq comp 1"}},
            Resident: {Entity: {RegistrationNumber: 'other/reg'}}
          }),

          // Local transactions involving an IHQ company and a local company must be classified as BOPCUS
          assertSuccess("sb4_nrexn1", {ReportingQualifier: 'NON REPORTABLE',
            NonResident: {Entity: {EntityName: "local company"}},
            Resident: {Entity: {}}
          }),
          assertFailure("sb4_nrexn1", {ReportingQualifier: 'NON REPORTABLE',
            NonResident: {Exception: {ExceptionName: "IHQ"}},
            Resident: {Entity: {}}
          }),
          assertNoRule("sb4_nrexn1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Exception: {ExceptionName: "IHQ"}},
            Resident: {Entity: {}}
          }),

          // Local transactions involving a HOLDCO company (based on account no) and a local company must be classified as BOPCUS
          assertSuccess("sb4_nrian1", {ReportingQualifier: 'NON REPORTABLE',
            NonResident: {Entity: {AccountNumber: "other"}},
            Resident: {Entity: {}}
          }),
          assertFailure("sb4_nrian1", {ReportingQualifier: 'NON REPORTABLE',
            NonResident: {Entity: {AccountNumber: "1234568"}},
            Resident: {Entity: {}}
          }),
          assertNoRule("sb4_nrian1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "1234568"}},
            Resident: {Entity: {}}
          }),

          // For HOLDCO companies (based on nonres account no) the Resident Entity name cannot be a name that belongs to a HOLDCO company in the HOLDCO table [SARB Manual B4 B(i)b]
          assertSuccess("sb4_ren1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "1234568"}},
            Resident: {Entity: {EntityName: "other"}}
          }),
          assertFailure("sb4_ren1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "1234568"}},
            Resident: {Entity: {EntityName: "holdco1"}}
          }),
          assertSuccess("sb4_ren1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "other"}},
            Resident: {Entity: {EntityName: "holdco1"}}
          }),

          // For HOLDCO companies (based on nonres account no) the Resident Entity registration no. cannot be one that belongs to a HOLDCO company in the HOLDCO table [SARB Manual B4 B(i)b]
          assertSuccess("sb4_rern1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "1234568"}},
            Resident: {Entity: {RegistrationNumber: "other"}}
          }),
          assertFailure("sb4_rern1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "1234568"}},
            Resident: {Entity: {RegistrationNumber: "2013/1234567/07"}}
          }),
          assertFailure("sb4_rern1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "1234568"}},
            Resident: {Entity: {RegistrationNumber: "2013/1234568/07"}}
          }),
          assertSuccess("sb4_rern1", {ReportingQualifier: 'BOPCUS',
            NonResident: {Entity: {AccountNumber: "other"}},
            Resident: {Entity: {RegistrationNumber: "2013/1234567/07"}}
          }),

          // LocationCountry: For IHQ companies (based on company reg no) if flow is OUT, the location country should be that of the receiving country code [SARB Manual B4 A(i)f]
          assertSuccess("sb4_mlc1", {ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            ReceivingCountry: 'US',
            NonResident: {Entity: {EntityName: "local company"}},
            Resident: {Entity: {RegistrationNumber: '2013/1234567/07'}},
            MonetaryAmount: [{LocationCountry: 'US'}]
          }),
          assertFailure("sb4_mlc1", {ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            ReceivingCountry: 'GB',
            NonResident: {Entity: {EntityName: "local company"}},
            Resident: {Entity: {RegistrationNumber: '2013/1234567/07'}},
            MonetaryAmount: [{LocationCountry: 'US'}]
          }),
          assertSuccess("sb4_mlc1", {ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            ReceivingCountry: 'GB',
            NonResident: {Entity: {EntityName: "local company"}},
            Resident: {Entity: {RegistrationNumber: 'other/reg'}},
            MonetaryAmount: [{LocationCountry: 'US'}]
          }),

          // LocationCountry: For IHQ companies (based on company reg no) if flow is IN, the location country should be that of the originating country code [SARB Manual B4 A(i)f]
          assertSuccess("sb4_mlc2", {ReportingQualifier: 'BOPCUS', Flow: 'IN',
            OriginatingCountry: 'US',
            NonResident: {Entity: {EntityName: "local company"}},
            Resident: {Entity: {RegistrationNumber: '2013/1234567/07'}},
            MonetaryAmount: [{LocationCountry: 'US'}]
          }),
          assertFailure("sb4_mlc2", {ReportingQualifier: 'BOPCUS', Flow: 'IN',
            OriginatingCountry: 'GB',
            NonResident: {Entity: {EntityName: "local company"}},
            Resident: {Entity: {RegistrationNumber: '2013/1234567/07'}},
            MonetaryAmount: [{LocationCountry: 'US'}]
          }),
          assertSuccess("sb4_mlc2", {ReportingQualifier: 'BOPCUS', Flow: 'IN',
            OriginatingCountry: 'GB',
            NonResident: {Entity: {EntityName: "local company"}},
            Resident: {Entity: {RegistrationNumber: 'other/reg'}},
            MonetaryAmount: [{LocationCountry: 'US'}]
          }),

          // AdHocRequirement.Subject: For IHQ companies (based on company reg no) the Subject must be IHQ followed by the number of the head quarter company allocated by the Financial Surveillance Department
          assertSuccess("sb4_madhs1", {ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {RegistrationNumber: '2013/1234567/07'}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'IHQ123'}}]
          }),
          assertFailure("sb4_madhs1", {ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {RegistrationNumber: '2013/1234567/07'}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'IHQ999'}}]
          }),


        ]

      }
    return testBase;
  }
})
