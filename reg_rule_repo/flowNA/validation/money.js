define(function () {
  return function (predef) {
    var stdMoney;
    with (predef) {

      stdMoney = {
        ruleset: "Standard Money Rules",
        scope: "money",
        validations: [
          {
            field: "ThirdParty.CustomsClientNumber",
            rules: [
              //ignore("mtpccn2")
            ]
          }
]
      };


    }
    return stdMoney;
  }
});