define(function(require) {
  return function(app, config) {
    app.controller("md_custom_dialog", [
      "$scope",
      "$mdDialog",
      function($scope, $mdDialog) {
        // just believe...
        var scope = $scope.$parent.$parent;

        function DialogController($scope, $mdDialog) {
          for (var key in scope) {
            if (key[0] != "$" && scope.hasOwnProperty(key))
              $scope[key] = scope[key];
          }

          $scope.hide = function() {
            $mdDialog.hide();
          };

          $scope.cancel = function() {
            $mdDialog.cancel();
          };

        }

        $scope.showTabDialog = function(ev) {
          $mdDialog
            .show({
              controller: DialogController,
              templateUrl: "tabDialog.tmpl.html",
              parent: angular.element(document.body),
              targetEvent: ev,
              clickOutsideToClose: true,
              fullscreen: true
            })
            .then(
              function(answer) {
                $scope.status =
                  'You said the information was "' + answer + '".';
              },
              function() {
                $scope.status = "You cancelled the dialog.";
              }
            );
        };
      }
    ]);

    var _config = {};

    return config;
  };
});
