define(function () {
  return function (predef) {
   
    //var convertValue = ;

    var display;

    with (predef) {
      display = {
        detailTrans: {
          ruleset: "Flow Transaction Display Rules",
          scope: "transaction",
          fields: []
        },
        detailMoney: {
          ruleset: "Flow Money Display Rules",
          scope: "money",
          fields: [
            
            {
              field: "Monetary.ThirdParty.Heading",
              display: [
                //setValue("Remove All Fields", null),
                clearValue(),
                setValue("Traveller's Details").onSection("AB")
                  .onCategory(["255", "256"])
                //,setValue("Third Party Details")
                //    .onOutflow().onSection("AB")
                //    .onCategory(["511", "512/01", "512/02", "512/03", "512/05", "512/06", "512/07", "513"])
              ]
            },
            {
              field: "ThirdParty",
              display: [
                hide(),
                show().onCategory(["255", "256"]),
                show(hasMoneyField('ThirdParty'))
                //, show()//{ "Individual": {} })//, null, notMoneyField("ThirdParty.Individual.Surname"))
                //    .onOutflow()
                //    .onSection("AB")
                //    .onCategory(["512", "513"])
                //    .notOnCategory(["512/04"])
                //    ,
                //clearValue().notOnCategory(["255", "256", "512/01", "512/02", "512/03", "512/05", "512/06", "512/07", "513"])

              ]
            },
            //{
            //    field: "ThirdParty.Individual",
            //    display: [

            //      ]
            //},
            {
              field: "TravelMode.TravellerStatus",
              display: [
                hide(),
                show().onCategory(["255", "256"])
              ]
            },
            {
              field: "ThirdParty.Individual.Surname",
              display: [
                hide(),
                show(hasMoneyFieldValue("TravelMode.TravellerStatus", [
                  'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
                ]))
                  .onCategory(["255", "256"])
                //, show().onOutflow().onSection("AB")
                //    .onCategory(["512", "513"])
                //    .notOnCategory(["512/04"])
              ]
            },
            {
              field: "ThirdParty.Individual.Name",
              display: [
                hide(),
                show(hasMoneyFieldValue("TravelMode.TravellerStatus", [
                  'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
                ]))
                  .onCategory(["255", "256"])
                //, show().onOutflow().onSection("AB")
                //    .onCategory(["512", "513"])
                //    .notOnCategory(["512/04"])
              ]
            },
            //{
            //    field: "ThirdParty.Individual.MiddleNames",
            //    display: [
            //      hide(),
            //      show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"]),
            //      show().onOutflow().onSection("AB")
            //          .onCategory(["512/01", "512/02", "512/03", "512/05", "512/06", "512/07", "513"])
            //    ]
            //},
            {
              field: "ThirdParty.Individual.Gender",
              display: [
                hide(),
                show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"])))
                  .onCategory(["255", "256"]),
                //show().onOutflow().onSection("AB")
                //    .onCategory(["512", "513"])
                //    .notOnCategory(["512/04"]),
                setValue("%s", genderFromSAID("ThirdParty.Individual.IDNumber"), isEmpty.and(hasMoneyField("ThirdParty.Individual.IDNumber")))
              ]
            },
            {
              field: "ThirdParty.Individual.IDNumber",
              display: [
                hide(),
                show(hasMoneyField("TravelMode.TravellerStatus").and(hasMoneyFieldValue("TravelMode.TravellerStatus", ["South African Resident"])))
                  .onCategory(["255", "256"])
                //, show().onOutflow().onSection("AB")
                //    .onCategory(["512", "513"])
                //    .notOnCategory(["512/04"])
              ]
            },
            {
              field: "ThirdParty.Individual.DateOfBirth",
              display: [
                hide(),
                show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"])))
                  .onCategory(["255", "256"]),
                //show().onOutflow().onSection("AB")
                //    .onCategory(["512/01", "512/02", "512/03", "512/05", "512/06", "512/07", "513"]),
                setValue("%s", dateOfBirthFromSAID("ThirdParty.Individual.IDNumber"), isEmpty.and(hasMoneyField("ThirdParty.Individual.IDNumber")))
              ]
            },
            {
              field: "ThirdParty.Individual.TempResPermitNumber",
              display: [
                hide(),
                show(hasMoneyField("TravelMode.TravellerStatus").and(hasMoneyFieldValue("TravelMode.TravellerStatus", ["Foreign Temporary Resident"]))).onCategory(["255", "256"])
              ]
            },
            {
              field: "ThirdParty.Individual.TempResExpiryDate",
              display: [
                hide(),
                show(hasMoneyField("TravelMode.TravellerStatus").and(hasMoneyFieldValue("TravelMode.TravellerStatus", ["Foreign Temporary Resident"]))).onCategory(["255", "256"])
              ]
            },
            {
              field: "ThirdParty.Individual.PassportNumber",
              display: [
                hide(),
                show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"])
              ]
            },
            {
              field: "ThirdParty.Individual.PassportCountry",
              display: [
                hide(),
                show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"])
              ]
            },
            {
              field: "ThirdParty.Individual.PassportExpiryDate",
              display: [
                hide(),
                show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"])
              ]
            },
            {
              field: "ThirdParty.Entity.Name",
              display: [
                hide()
              ]
            },
            {
              field: "ThirdParty.Entity.RegistrationNumber",
              display: [
                hide()
              ]
            },
            {
              field: "ThirdParty.CustomsClientNumber",
              display: [
                hide()
              ]
            },
            {
              field: "ThirdParty.TaxNumber",
              display: [
                hide()
                //,show(hasTransactionField("Resident.Entity"))
                //    .onOutflow().onSection("AB").onCategory(["512", "513"])
                //    .notOnCategory(["512/04"])
              ]
            },
            {
              field: "ThirdParty.VATNumber",
              display: [
                hide()
              ]
            },
            {
              field: "ThirdParty.StreetAddress.AddressLine1",
              display: [
                hide()
                //,show(hasTransactionField("Resident.Entity"))
                //    .onOutflow().onSection("AB").onCategory(["512", "513"])
                //    .notOnCategory(["512/04"])
              ]
            },
            {
              field: "ThirdParty.StreetAddress.AddressLine2",
              display: [
                hide()
                //,show(hasTransactionField("Resident.Entity"))
                //    .onOutflow().onSection("AB").onCategory(["512", "513"])
                //    .notOnCategory(["512/04"])
              ]
            },
            {
              field: "ThirdParty.StreetAddress.Suburb",
              display: [
                hide()
                //,show(hasTransactionField("Resident.Entity"))
                //    .onOutflow().onSection("AB").onCategory(["512", "513"])
                //    .notOnCategory(["512/04"])
              ]
            },
            {
              field: "ThirdParty.StreetAddress.City",
              display: [
                hide()
                //,show(hasTransactionField("Resident.Entity"))
                //    .onOutflow().onSection("AB").onCategory(["512", "513"])
                //    .notOnCategory(["512/04"])
              ]
            },
            {
              field: "ThirdParty.StreetAddress.Province",
              display: [
                hide()
                //,show(hasTransactionField("Resident.Entity"))
                //    .onOutflow().onSection("AB").onCategory(["512", "513"])
                //    .notOnCategory(["512/04"])
              ]
            },
            {
              field: "ThirdParty.StreetAddress.PostalCode",
              display: [
                hide()
                //,show(hasTransactionField("Resident.Entity"))
                //    .onOutflow().onSection("AB").onCategory(["512", "513"])
                //    .notOnCategory(["512/04"])
              ]
            },
            {
              field: "ThirdParty.PostalAddress.AddressLine1",
              display: [
                hide()
              ]
            },
            {
              field: "ThirdParty.PostalAddress.AddressLine2",
              display: [
                hide()
              ]
            },
            {
              field: "ThirdParty.PostalAddress.Suburb",
              display: [
                hide()
              ]
            },
            {
              field: "ThirdParty.PostalAddress.City",
              display: [
                hide()
              ]
            },
            {
              field: "ThirdParty.PostalAddress.Province",
              display: [
                hide()
              ]
            },
            {
              field: "ThirdParty.PostalAddress.PostalCode",
              display: [
                hide()
              ]
            },
            {
              field: "ThirdParty.ContactDetails.ContactSurname",
              display: [
                hide()
              ]
            },
            {
              field: "ThirdParty.ContactDetails.ContactName",
              display: [
                hide()
              ]
            },
            {
              field: "ThirdParty.ContactDetails.Email",
              display: [
                hide()
              ]
            },
            {
              field: "ThirdParty.ContactDetails.Fax",
              display: [
                hide()
              ]
            },
            {
              field: "ThirdParty.ContactDetails.Telephone",
              display: [
                hide()
              ]
            },
            
          ]
        },
        detailImportExport: {
          ruleset: "Flow Import/Export Display Rules",
          scope: "importexport",
          fields: [{
            field: "TransportDocumentNumber",
            display: [
              //clearValue().onInflow(),
              //clearValue().onOutflow().onSection("CDEF"),
              //clearValue().onOutflow().onSection("ABG")
              //    .onCategory(["103/11"])
              //    .notOnCategory(["103", "105", "106"]),
              hide(),
              show().onOutflow().onSection("ABG")
                .onCategory(["103", "105", "106"])
                .notOnCategory(["103/11"])
            ]
          },
            {
              field: "ImportControlNumberLabel",
              display: [
                setValue("Invoice Number", null, isEmpty).onOutflow().onSection("ABG").onCategory("101"),
                setValue("Movement Reference Number (MRN)", null, isEmpty).onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
              ]
            },
            {
              field: 'PaymentCurrencyCode',
              display: [
                setValue('%s', 'transaction::FlowCurrency', isEmpty.or(notMatchesTransactionField("FlowCurrency").and(notTransactionField("ZAREquivalent")))),
                setValue('ZAR', null, isEmpty.or(notValue("ZAR").and(hasTransactionField("ZAREquivalent"))))

                //setValue('%s', 'transaction::FlowCurrency', isEmpty.or(notMatchesTransactionField("FlowCurrency")))//.and(notTransactionField("ZAREquivalent")))),
                ////setValue('ZAR', null, isEmpty.or(notValue("ZAR").and(hasTransactionField("ZAREquivalent"))))
              ]
            },
            {
              field: "MRNNotOnIVS",
              display: [
                hide()
              ]
            }
          ]

        }
      };
    }

    return display;
  }
});