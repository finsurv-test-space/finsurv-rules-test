var testData = [
  {
    name: "simple valid entity",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Entity: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG",
              Country: "ZA"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            EntityName: "John",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            VATNumber: "66666",
            RegistrationNumber: "12341234"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG",
              State: "GAUTENG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Gender: "M",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "407",
            SARBAuth: { AuthIssuer: "024", SARBAuthRefNumber: "1234" },
            TravelMode: {},
            ThirdParty: {},
            ForeignValue: "1000.00",
            LocationCountry: "US",
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalValue: 1000,
        TrnReference: "TestRef12414",
        AccountHolderStatus: "South African Resident",
        LocationCountry: "NZ",
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        PaymentDetail: {
          IsCorrespondentBank: "N",
          BeneficiaryBank: {
            BankName: "Foo Bank",
            BranchCode: "1234",
            Address: "Foo",
            City: "Boo"
          },
          SWIFTDetails: "Payment"
        },
        TransactionCurrency: "USD",
        RateConfirmation: "N",
        IsFCA: "N",
        CounterpartyStatus: "Non Resident"
      },
      customData: {}
    }
  },

  {
    name: "Mandate example",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Entity: {
            AccountNumber: "1100203024194",
            EntityName: "Compo Agencies",
            TradingName: "",
            RegistrationNumber: "1973/003642/07",
            TaxNumber: "9135070846",
            VATNumber: "4410119830",
            StreetAddress: {
              AddressLine1: "ypbyoitbmk",
              Suburb: "Western Extension",
              City: "Benoni",
              Province: "GAUTENG",
              PostalCode: "1501",
              Country: "ZA"
            },
            ContactDetails: {
              ContactSurname: "eeXXXXre",
              ContactName: "viXXXXav",
              Email: "JUciL@someweb.co.za",
              Telephone: "0115558228",
              Fax: ""
            }
          },
          Type: "Entity"
        },
        NonResident: {
          Entity: {
            EntityName: "anita",
            Address: { Country: "AD" },
            AccountNumber: "123456789"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "101",
            MoneyTransferAgentIndicator: "AD",
            SARBAuth: {},
            TravelMode: {},
            CategorySubCode: "01",
            ForeignValue: 120,
            ImportExport: [
              {
                SubsequenceNumber: "1",
                PaymentCurrencyCode: "USD",
                ImportControlNumber: "INV12345",
                PaymentValue: 120
              }
            ],
            SequenceNumber: "1"
          }
        ],
        ReceivingCountry: "AD",
        FlowCurrency: "USD",
        OriginatingBank: "",
        TotalForeignValue: "120",
        TrnReference: "090f4a1180755759",
        OriginatingCountry: "",
        ValueDate: "2018-02-07",
        ReceivingBank: "IVESZAJJ0",
        LocationCountry: "AD",
        PaymentDetail: {
          IsCorrespondentBank: "N",
          BeneficiaryBank: {
            BankName: "VBS",
            BranchCode: "123",
            Address: "address 1",
            City: "Lagos"
          }
        },
        IsFCA: "N",
        RateConfirmation: "N",
        TransactionCurrency: "USD",
        AccountHolderStatus: "South African Resident",
        CounterpartyStatus: "Non Resident"
      },
      customData: {
        DealerType: "AD",

      }
    }
  }
];

// Since this dummy data needs to be tested in the Dev source and the Built source,
// it needs to work with requirejs and without it. So, cannot assume requirejs' 'define' will be
// defined.

if (typeof define !== "undefined")
  define({
    data: testData
  });
