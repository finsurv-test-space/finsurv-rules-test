define(function () {
  return function (predef) {

    var display;

    with (predef) {
      display = {
        detailTrans: {
          ruleset: "Std Bank Transaction Display Rules",
          scope: "transaction",
          fields: []
        },
        detailMoney: {
          ruleset: "Std Bank Money Display Rules",
          scope: "money",
          fields: [
            {
              field: ["ReversalTrnSeqNumber", "ReversalTrnRefNumber"],
              display: [
                hide(),
                show().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
                clearValue(not(hasMoneyFieldValue("CategoryCode", ["100", "200", "300", "400", "500", "600", "700", "800"]))),
              ]
            }
          ]
        },
        detailImportExport: {
          ruleset: "Std Bank Import/Export Display Rules",
          scope: "importexport",
          fields: []
        }

      }
    }
    return display;
  }
});