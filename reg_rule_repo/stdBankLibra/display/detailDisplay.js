define(function () {
  return function (predef) {
    var Display;

    //thirdparty entity registration number test
    var regNumTest = /^\d{4}\/\d{6}\/\d{2}/;

    //thirdparty entity trust number test
    var trustNumTest = /^IT\/\d{1,6}\/\d{1,4}/;

    with (predef) {
      Display = {
        detailTrans: {
          ruleset: "Std Bank NBOL Transaction Display Rules",
          scope: "transaction",
          fields: [
            {
              field: "BranchCode",
              display: [
                show()
              ]
            },
            {
              field: "BranchName",
              display: [
                show()
              ]
            },
            {
              field: "HubCode",
              display: [
                show()
              ]
            },
            {
              field: "HubName",
              display: [
                show()
              ]
            },
            {
              field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],
              display: [
                show()
              ]
            }

          ]
        },
        //================================================================================
        // MONEY SCOPE
        //================================================================================

        detailMoney: {
          ruleset: "Std Bank NBOL Money Display Rules",
          scope: "money",
          fields: [
            {
              field: "RandValue",
              display: [
                show().onSection("ABCDEG"),
                disable(isCurrencyIn("ZAR")).onSection("ABCDEG"),
                hide().onSection("F"),
                hide(isCurrencyIn(["ZAR"])),
                setValue("%s", "ForeignValue", hasTransactionFieldValue("FlowCurrency", "ZAR")),
                disable(hasCustomField('TotalRandAmount')),
                setValue(function (context, ind) {
                  var exchangeRatio = Number(context.getCustomValue("TotalRandAmount")) /
                    Number(context.getTransactionField("TotalForeignValue"));
                  var result = Number(context.getMoneyField(ind, "ForeignValue")) * exchangeRatio;
                  return result.toFixed(2);
                }, hasCustomField('TotalRandAmount').and(notCurrencyIn(["ZAR"]))),
                setValue(function (context, ind) {
                  var result = Number(context.getMoneyField(ind, "ForeignValue"));
                  return result.toFixed(2);
                }, hasCustomField('TotalRandAmount').and(isCurrencyIn(["ZAR"])))
              ]
            },
            {
              /*mtaAccounts item
               {
               accountNumber : "12345678",
               MTA : "GLOBAL",
               rulingSection : "baz",
               ADLALevel : "2",
               isADLA : true
               }
               */
              field: "MoneyTransferAgentIndicator",
              display: [
                setValue(function (context, ind) {
                    var AccountNumber =
                      context.getTransactionField('Resident.Individual.AccountNumber') |
                      context.getTransactionField('Resident.Entity.AccountNumber');

                    var mtaAccount =
                      context.getLookups().mtaAccounts.find( function (item) {
                        return item.accountNumber == AccountNumber;
                      });

                    return (!!mtaAccount ? mtaAccount.MTA : "");

                  }, evalTransactionField("Resident.Individual.AccountNumber", isInLookup('mtaAccounts', 'accountNumber'))
                    .or(evalTransactionField("Resident.Entity.AccountNumber", isInLookup('mtaAccounts', 'accountNumber')))
                ).onCategory(['833']).onSection("A"),
                setValue('AD', null, isEmpty).onSection("C")
              ]
            },
            {
              field: "{{Regulator}}Auth.RulingsSection",
              display: [
                setValue(function (context, ind) {
                  var AccountNumber =
                    context.getTransactionField('Resident.Individual.AccountNumber') |
                    context.getTransactionField('Resident.Entity.AccountNumber');

                  var mtaAccount =
                    context.getLookups().mtaAccounts.find( function (item) {
                      return item.accountNumber == AccountNumber;
                    });

                  return ((!!mtaAccount && (mtaAccount.isADLA)) ? mtaAccount.rulingSection : "");

                }, isEmpty.and(
                  evalTransactionField("Resident.Individual.AccountNumber", isInLookup('mtaAccounts', 'accountNumber'))
                    .or(evalTransactionField("Resident.Entity.AccountNumber", isInLookup('mtaAccounts', 'accountNumber')))
                )).onCategory(['833']).onSection("A")
              ]
            },
            {
              field: "SWIFTDetails",
              display: [

                show()
              ]
            },
            {
              field: "LoanTenorType",
              display: [
                hide(),
                show().onSection("AB").onCategory(["801", "802", "803", "804"]),
                show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"])
              ]
            },
            {
              field: "LoanTenorMaturityDate",
              display: [
                hide(),
                show(hasMoneyFieldValue("LoanTenorType", ["MATURITY DATE"])).onSection("AB").onCategory(["801", "802", "803", "804"]),
                show(hasMoneyFieldValue("LoanTenorType", ["MATURITY DATE"])).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"])
              ]
            },
            {
              field: "AdHocRequirement.Description",
              display: [
                hide(),
                show(hasMoneyField("AdHocRequirement.Subject")).onSection("ABG"),
                setValue("NONE", null,
                  hasMoneyFieldValue("AdHocRequirement.Subject", "NO")).onSection("A")
              ]
            },
            {
              field: "AdHocRequirement.Subject",
              display: [
                hide().onSection("CDEF")

              ]
            },
            //================================================================================
            // THIRD PARTY
            //================================================================================

            {
              field: "ThirdParty",
              display: [
                hide().onCategory(['ZZ1'])
              ]
            },
            //--------------------
            // for sameAsPhysical
            {
              field: "ThirdParty.PostalAddress.AddressLine1",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.AddressLine1", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false))
              ]
            },
            {
              field: "ThirdParty.PostalAddress.AddressLine1",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.AddressLine1", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false))
              ]
            },
            {
              field: "ThirdParty.PostalAddress.AddressLine2",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.AddressLine2", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false))
              ]
            },
            {
              field: "ThirdParty.PostalAddress.Suburb",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.Suburb", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false))
              ]
            },
            {
              field: "ThirdParty.PostalAddress.City",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.City", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false))
              ]
            },
            {
              field: "ThirdParty.PostalAddress.Province",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.Province", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false))
              ]
            },
            {
              field: "ThirdParty.PostalAddress.PostalCode",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.PostalCode", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false))
              ]
            },
            {
              field: "ThirdPartyKind",
              display: [
                show(),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false)),
                setValue('Individual', null, isEmpty.and(hasMoneyField('ThirdParty.Individual'))),
                setValue('Entity', null, isEmpty.and(hasMoneyField('ThirdParty.Entity')))
              ]
            },


            // The four heading fields below don't actually exist as fields but rather are used
            //  for checking if the headings in the import/export table should be displayed or hidden.
            {
              field: "IE_ICNHeading",
              display: [
                hide(),
                show().onOutflow().onSection("ABG").onCategory(["101", "103", "105", "106"]),
                setLabel("Import Control Number (or MRN)"),
                setLabel("Invoice Number (or MRN)").onOutflow().onSection("ABG").onCategory("101"),
                setLabel("Movement Reference Number (MRN)").onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
              ]
            },
            {
              field: "IE_TDNHeading",
              display: [
                hide(),
                show().onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
              ]
            },
            {
              field: "IE_UCRHeading",
              display: [
                hide(),
                show().onInflow().onSection("ABG").onCategory(["101", "103", "105", "106"])
              ]
            },
            {
              field: "IE_MRNNotOnIVSHeading",
              display: [
                hide(),
                show().onOutflow().onSection("ABG").onCategory(["101", "103", "105", "106"])
              ]
            }
          ]
        },
        detailImportExport: {
          ruleset: "Std Bank NBOL Import/Export Display Rules",
          scope: "importexport",
          fields: [
            {
              field: "TransportDocumentNumber",
              display: [
                hide(isEmpty),
                show().onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
              ]
            }
          ]
        }

      }
    }
    return Display;
  }

});