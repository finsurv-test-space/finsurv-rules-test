/**
 * Created by petruspretorius on 06/01/2016.
 */
define(function () {
  return function (predef) {

    var filterLookupRules;
    with (predef) {
      filterLookupRules = {
        filterLookupTrans: {
          ruleset: "Reporting Transaction Lookup Filter Rules (NBOL)",
          scope: "transaction",
          fields: []
        },

        filterLookupMoney: {
          ruleset: "Reporting Monetary Lookup Filter Rules",
          scope: "money",
          fields: [
            {
              field: 'ThirdParty',
              display: [
                excludeValue(['-'], not(
                  hasCustomValue("allowAdhocBOPThirdPartyCapture", "true")
                    .or(hasCustomValue("allowAdhocBOPThirdPartyCapture", true)))),

                // "If CategoryCode 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, Individual must be completed",
                filterValue(filterThirdPartyIndividual, hasTransactionField("Resident.Entity")).onOutflow().onSection("AB").onCategory(["512", "513"]),

                // "If the category is 256 and the PassportNumber under Resident Individual contains no value, Individual must be completed",
                filterValue(filterThirdPartyIndividual, notTransactionField("Resident.Individual.PassportNumber")).onSection("AB").onCategory("256"),

                // "If the category is 255 or 256 and the Resident Entity element is completed, Individual must be completed",
                filterValue(filterThirdPartyIndividual, hasTransactionField("Resident.Entity")).onSection("AB").onCategory(["255", "256"]),

                // "If the SupplementaryCardIndicator is Y, Individual must be completed"
                filterValue(filterThirdPartyIndividual, hasResidentFieldValue("SupplementaryCardIndicator", "Y")).onSection("E"),

                // "If the Flow is IN and category 303, 304, 305, 306, 416 or 417 is used and Resident Entity is completed, Individual must be completed",
                filterValue(filterThirdPartyIndividual, hasTransactionField("Resident.Entity")).onInflow().onSection("AB").onCategory(["303", "304", "305", "306", "416", "417"]),

                // "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, Individual must be completed",
                filterValue(filterThirdPartyIndividual, hasTransactionField("Resident.Entity")).onInflow().onSection("A").onCategory(["511", "516"])

              ]
            },
            //{
            //  field: "CompoundCategoryCode",
            //  display: [
            //    limitValue(['513'], hasNonResidentFieldValue("AccountIdentifier", "FCA RESIDENT")).onOutflow().onSection("A"),
            //    limitValue(['517'], hasNonResidentFieldValue("AccountIdentifier", "FCA RESIDENT")).onInflow().onSection("A"),
            //    //#dummyData 18
            //    excludeValue(['102,104'],
            //      notImportUndertakingClient
            //        .and(notCustomValue("ignoreLUClient", true)))
            //      .onOutflow().onSection("ABG"),
            //
            //    //#dummyData 19
            //    limitValue(['102,104'], importUndertakingClient).onOutflow().onSection("ABG"),
            //
            //    //#dummyData 14
            //    excludeValue(['514,515,ZZ1,ZZ2,100,200,201,202,203,204,205,210,211,212,213,220,221,225,226,230,231,232,233,234,235,236,240,240,241,242,243,250,251,255,256,260,261,265,266,270,271,272,273,275,276,280,281,282,285,287,288,289,290,291,292,293,294,295,296,297,300,301,302,303,304,305,306,307,308,309,312,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,500,501,502,503,504,510,511,512,513,530,600,601,602,603,605,610,611,612,615,616,700,701,702,703,704,705,800,801,802,803,804,810,815,816,817,818,819,830,831,833'], hasCustomValue("tradeOnly", true)).onOutflow().onSection("ABG"),
            //    limitValue(['ZZ1']).onSection("C"),
            //    //excludeValue(['ZZ1', 'ZZ2']).onSection("ABDEF"),
            //    //   limitValue(['833'],hasCustomValue('ADLA',true)),
            //    excludeValue(['833'], not(hasCustomValue('ADLA', true))),
            //    excludeValue(['ZZ1'], notCustomValue('AllowZZ1',true)),
            //    limitValue(['ZZ1'], hasCustomValue('ForceZZ1',true))
            //  ]
            //},
            {
              field: "MoneyTransferAgentIndicator",
              display: [
                limitValue(["CARD"]).onSection("EF"),
                limitValue(["BOPDIR"]).onSection("G"),
                //limitValue(["ADLA"], dealerTypeADLA).onSection("AB"),
                //excludeValue(["AD", "ADLA", "CARD", "BOPDIR"], dealerTypeAD).onSection("ABCD").onCategory("833"),
                //excludeValue(["ADLA", "CARD", "BOPDIR"], dealerTypeAD).onSection("ABCD").notOnCategory("833"),
                //excludeValue(["ADLA", "AD"], hasCustomValue('ADLA', true)).onSection("ABCD").onCategory("833")
              ]
            },
            {
              field: "ThirdPartyKind",
              display: [
                // "If CategoryCode 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onOutflow().onSection("AB").onCategory(["512", "513"]),

                // "If the category is 256 and the PassportNumber under Resident Individual contains no value, Individual must be completed",
                limitValue(["Individual"], notTransactionField("Resident.Individual.PassportNumber")).onSection("AB").onCategory("256"),

                // "If the category is 255 or 256 and the Resident Entity element is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onSection("AB").onCategory(["255", "256"]),

                // "If the SupplementaryCardIndicator is Y, Individual must be completed"
                limitValue(["Individual"], hasResidentFieldValue("SupplementaryCardIndicator", "Y")).onSection("E"),

                // "If the Flow is IN and category 303, 304, 305, 306, 416 or 417 is used and Resident Entity is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onInflow().onSection("AB").onCategory(["303", "304", "305", "306", "416", "417"]),

                // "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onInflow().onSection("A").onCategory(["511", "516"])


              ]
            }
          ]
        },

        filterLookupImportExport: {
          ruleset: "Reporting Import Export Lookup Filter Rules",
          scope: "importexport",
          fields: []
        }
      }
    }

    return filterLookupRules;
  }
});


