define(function() {
    return function(predef) {
        var money;
        with(predef) {
            money = {
                ruleset: "ARM Auth Validation Rules for Standard Bank",
                scope: "money",
                validations: [{
                        field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
                        rules: [
                            validate("sb_arm1", "Verify_ARM", notEmpty.and(hasMoneyField("{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate"))).onSection("ABCDEFG")
                        ]
                    },
                    {
                        field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
                        rules: [
                            validate("sb_arm2", "Verify_ARM", notEmpty.and(hasMoneyField("{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber"))).onSection("ABCDEFG")
                        ]
                    }
                ]
            }
            
        }
        return money;
    }
});