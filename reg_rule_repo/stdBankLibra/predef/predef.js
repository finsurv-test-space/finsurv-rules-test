/**
 * Created by petruspretorius on 09/12/2016.
 */
define(function () {
  return function (predef) {
    predef = Object.assign(predef, {
      filterThirdPartyIndividual: function (value) {
        //console.log("filterThirdPartyIndividual", value);
        if (value == '-') {
          return true;
        }
        return (typeof value.Individual !== 'undefined');
      }
    })
    return predef;
  }
})