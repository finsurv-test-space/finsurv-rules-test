define(function() {
  return function(testBase) {
    with (testBase) {
      var mappings = {
        LocalCurrencySymbol: "F",
        LocalCurrencyName: "Fantasy",
        LocalCurrency: "FAN",
        LocalValue: "FantasyValue",
        Regulator: "FRB",
        DealerPrefix: "AD",
        RegulatorPrefix: "FRB"
      };

      setMappings(mappings);

      var test_cases = [
        assertSuccess("fl_riidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'SDA' },
            FantasyValue: '49999.00'
          }]
        }),

        assertFailure("fl_riidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'SDA' },
            FantasyValue: '1000001.00'
          }]
        }),

        assertFailure("fl_riidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'SDA' },
            FantasyValue: '550000.00'
          },
          {
            AdHocRequirement: { Subject: 'SDA' },
            FantasyValue: '500000.00'
          }]
        }),

        assertSuccess("fl_riidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'SDA' },
            FantasyValue: '800000.00'
          },
          {
            FantasyValue: '500001.00'
          }]
        }),

        assertSuccess("fl_riidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'SDA' },
            FantasyValue: '800000.00'
          },
          {
            AdHocRequirement: { Subject: 'SDA' },
            ThirdParty: { Individual: { IDNumber: "4444567890554" } },
            FantasyValue: '500001.00'
          }]
        }),

        assertSuccess("fl_riidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'SDA' },
            FantasyValue: '999,999.99'
          }]
        }),

        assertSuccess("fl_riidn1", {
          ReportingQualifier: "BOPCUS",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'SDA' },
            FantasyValue: '999,999.'
          }]
        }),

        assertFailure("fl_riidn1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'SDA' },
            FantasyValue: '800000.00'
          },
          {
            AdHocRequirement: { Subject: 'SDA' },
            FantasyValue: '500001.00'
          }]
        }),

        assertSuccess("fl_riidn1", {
          ReportingQualifier: "BOPCARD RESIDENT",
          Resident: { Individual: { IDNumber: "1234567890123" } },
          MonetaryAmount: [{
            AdHocRequirement: { Subject: 'SDA' },
            FantasyValue: '800000.00'
          },
          {
            AdHocRequirement: { Subject: 'SDA' },
            ThirdParty: { Individual: { IDNumber: "4444567890554" } },
            FantasyValue: '500001.00'
          }]
        })
      ]
    }
    return testBase;
  }
})


// validate("ext_mlval1", "Validate_IndividualSDA",
// notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA")).
// and(hasMoneyField("ThirdParty.Individual.IDNumber").or(hasTransactionField("Resident.Individual.IDNumber"))).
// and(hasTransactionField("ValueDate"))).onSection("A"),
// validate("ext_mlval2", "Validate_IndividualFIA",
// notEmpty.and(not(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).
// and(hasMoneyField("ThirdParty.Individual.IDNumber").or(hasTransactionField("Resident.Individual.IDNumber"))).
// and(hasTransactionField("ValueDate"))).onSection("A").onOutflow().onCategory(["511","512"]),
// validate("ext_mlval3", "Validate_RemittanceDispensation",
// notEmpty.and(hasSumLocalValue('<=', "3000")).and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION")).
// and(hasMoneyField("ThirdParty.Individual.IDNumber").or(hasTransactionField("Resident.Individual.IDNumber")).or(hasTransactionField("Resident.Individual.TempResPermitNumber"))).
// and(hasTransactionField("ValueDate"))).onSection("A")
