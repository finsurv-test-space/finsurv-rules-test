define(function () {
  return function (predef) {
    var extTrans;
    with (predef) {

      extTrans = {
        ruleset: "External Money Rules for Limits",
        scope: "money",
        validations: [
          {
            field: "ThirdParty.Individual.IDNumber",
            rules: [
              validate("ext_mlval2", "Validate_IndividualSDA",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))
                .and(hasTransactionField("ValueDate"))
                .and(hasSDAMonetaryValue)).onOutflow().onSection("A"),
              // validate("ext_mlval4", "Validate_IndividualFIA",
              //   notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "FIA"))
              //   .and(hasTransactionField("ValueDate"))
              //   .and(hasFIAMonetaryValue)).onOutflow().onSection("A"),
              validate("ext_mlval4", "Validate_IndividualFIA",
                notEmpty.and(hasTransactionField("ValueDate")).and(hasFIAMonetaryValue)).onOutflow().onSection("A").onCategory(["512", "513"]),
              failure("fl_mtpiid1", "DAL", "ID Number has exceeded the discretionary amount limit of R1,000,000 within this transaction", 
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA")).and(not(isValidSDAAmount))).onOutflow()
            ]
          },
          {
            field: "{{LocalValue}}",
            rules: [
              // Not needed anymore
              // ============================================================================================================================================
              // validate("ext_mlval1", "Validate_IndividualSDA",
              //   notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA")).
              //   and(hasMoneyField("ThirdParty.Individual.IDNumber").or(hasTransactionField("Resident.Individual.IDNumber"))).
              //   and(hasTransactionField("ValueDate"))).onSection("A"),
              // ============================================================================================================================================
              
              // validate("ext_mlval2", "Validate_IndividualFIA",
              //   notEmpty.and(not(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).
              //   and(hasMoneyField("ThirdParty.Individual.IDNumber").or(hasTransactionField("Resident.Individual.IDNumber"))).
              //   and(hasTransactionField("ValueDate"))).onSection("A").onOutflow().onCategory(["511","512"]),
              // validate("ext_mlval3", "Validate_RemittanceDispensation",
              //   notEmpty.and(hasSumLocalValue('<=', "5000")).and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION")).
              //   and(hasMoneyField("ThirdParty.Individual.IDNumber").or(hasTransactionField("Resident.Individual.IDNumber")).or(hasTransactionField("Resident.Individual.TempResPermitNumber"))).
              //   and(hasTransactionField("ValueDate"))).onSection("A")
            ]
          }
        ]
      };

    }
    return extTrans;
  }
});
