define(function() {
  //THIS IS the Transaciotn rules
  return function(predef) {
    var extTrans;
    with (predef) {
			extTrans = {
        ruleset: "External Transaction Rules for Limits",
        scope: "transaction",
        validations: [
          {
            field: "Resident.Individual.IDNumber",
            rules: [
              validate("ext_mlval1", "Validate_IndividualSDA",
                notEmpty.and(hasTransactionField("ValueDate")).and(hasSDAMonetaryValue)).onOutflow().onSection("A"),
              // validate("ext_mlval3", "Validate_IndividualFIA", 
              //   notEmpty.and(hasTransactionField("ValueDate")).and(hasFIAMonetaryValue)).onOutflow().onSection("A"),
              validate("ext_mlval3", "Validate_IndividualFIA", 
                notEmpty.and(hasTransactionField("ValueDate")).and(hasFIAMonetaryValue)).onOutflow().onSection("A").onCategory(["512", "513"]),
              failure("fl_riidn1", "DAL", "ID Number has exceeded the discretionary amount limit of R1,000,000 within this transaction", 
                notEmpty.and(not(isValidSDAAmount))).onOutflow()
            ]
          },
        ]
      };
    }
    return extTrans;
  };
});
