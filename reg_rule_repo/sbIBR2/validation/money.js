define(function () {
  return function (predef) {
    var money;

    with (predef) {
      money = {
        ruleset: "Money Rules for Flow Form",
        scope: "money",
        validations: [
          {
            field: "",
            rules: [
              // FIN-1417: BOTTS needs external LUClient CCN validation
              // field: "CategoryCode",
              ignore('mcc8'),
              ignore('mcc9'),
              //field: "ImportExport",
              ignore('mtie1'),
              ignore('mtie4'),
              ignore('mtie5'),
            ]
          },
          {
            field: "CategoryCode",
            rules: [
              failure("ibr_cc1","IB1", "This category is not allowed for this product.", returnTrue).onCategory(['830','833']),
              failure("ibr_cc2", "IB2", "This category is not allowed for import undertaking clients. Please use 102 or 104.", importUndertakingClient).onCategory(['101','103']),
              failure('ibr_mcc8', 323, 'If the Flow is OUT then the categories 102/01 to 102/10 or 104/01 to 104/10 can only be used for import undertaking customers',
                notEmpty.and(notImportUndertakingClient)).onOutflow().onSection("AB").onCategory(['102', '104']).notOnCategory(['102/11', '104/11']),
              failure('ibr_mcc9', 323, 'If the Flow is OUT then the categories 101/01 to 101/10 or 103/01 to 103/10 may not be used for import undertaking customers',
                notEmpty.and(importUndertakingClient)).onOutflow().onSection("AB").onCategory(['101', '103']).notOnCategory(['101/11', '103/11'])
            ]
          },
          {
            field: "ThirdParty.Individual.Surname",
            rules: [
              failure("ibr_tpisn1", "IB3", "This is required", isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).onOutflow(),
              failure('ibr_tpisn2', "IB4", "This is required", isEmpty.and(hasMoneyFieldValue("ThirdPartyKind", "Individual")).and(hasMoneyField("ThirdParty")))
            ]
          },
          {
            field: "ThirdParty.Entity.Name",
            rules: [ 
              failure('ibr_tpeen1', "IB5", "This is required", isEmpty.and(hasMoneyFieldValue("ThirdPartyKind", "Entity")).and(hasMoneyField("ThirdParty")))
            ]
          },
          {
            field:  "{{Regulator}}Auth.IsSARBAuth" ,
            rules: [
              failure('ibr_sa1', "IB7", "This is required", hasValue(undefined).and(hasMoneyFieldValue('haveAuth',true)))

            ]
          },
          {
            field: "ImportExport",
            rules: [
              failure("ibr_mtie1", 490, "If Inflow and the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, the ImportExport element must be completed",
                emptyImportExport.and(not(hasMoneyFieldValue("CategoryCode", "106").and(not(notImportUndertakingClient))))).onInflow().onSection("AB").onCategory(["101", "103", "105", "106"]).notOnCategory(["101/11", "103/11"]),
              failure("ibr_mtie4", 490, "If outflow and the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, the ImportExport element must be completed unless a) the Subject is SDA or REMITTANCE DISPENSATION or b) category 106 and import undertaking client",
                emptyImportExport.and(notMoneyFieldValue("AdHocRequirement.Subject", ["SDA", "REMITTANCE DISPENSATION"]).and(not(hasMoneyFieldValue("CategoryCode", "106").and(importUndertakingClient))))).onOutflow().onSection("A").onCategory(["101", "103", "105", "106"]).notOnCategory(["101/11", "103/11"]),
              failure("ibr_mtie5", 491, "For any category other than 101/01 to 101/11 or 103/01 to 103/11 or 105 or 106, the ImportExport element must not be completed",
                notEmptyImportExport).onSection("ABG").notOnCategory(["101", "103", "105", "106"])
            ]
          },
          {
            field: "LoanInterestRate",
            rules: [
              failure("sb_mlir4", 380, "If the Flow is In and CategoryCode 309/01 to 309/07 is used, must be completed reflecting the percentage interest paid in the format 0.00",
                notPattern("^\\d{1,3}\\.\\d{2}?$")).onInflow().onSection("AB").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
              warning("sb_mlir6", "S12", "It is unlikely that an interest rate greater than 100% is being charged",
                hasPattern("^\\d{3}\\.\\d{2}?$")).onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
              failure("sb_mlir8", 379, "May not be completed unless a loan related transaction is being captured",
                notEmpty).onInflow().onSection("AB").notOnCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"])
            ]
          },
        ]
      }
    };

    return money;
  }
})