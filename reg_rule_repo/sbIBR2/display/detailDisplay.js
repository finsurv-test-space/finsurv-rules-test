define(function () {

   return function (predef) {

    var display;

    with (predef) {
      display = {
        detailTrans: {
          ruleset: "Flow Transaction Display Rules",
          scope: "transaction",
          fields: [
            {
              field: "TrnReference",
              display: [
                setValue(function() {
                   // from https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
                  var cryptoObj = window.crypto || window.msCrypto;
                  //[1e7]+-1e3+-4e3+-8e3+-1e11
                  return ([1e7]+-1e3+-4e3+-8e3+-1e5).replace(/[018]/g, function(c) {
                      return (c ^ cryptoObj.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
                    });
                }, isEmpty)
              ]
            },
            // {
            //   field: ["Resident.Entity.InstitutionalSector","Resident.Entity.IndustrialClassification"],
            //   display: [
            //     disable(),
            //     hide()
            //   ]
            // }
          ]
        },
        detailMoney: {
          ruleset: "Flow Money Display Rules",
          scope: "money",
          fields: [
            {
              field: "ForeignValue",
              display: [
                hide(),
                show(notCustomValue("inZAR",true)),
                // clearValue(isCurrencyIn("ZAR").or(evalTransactionField("ZAREquivalent", notEmpty))),
                hide(isCurrencyIn("ZAR").or(evalTransactionField("ZAREquivalent", notEmpty))),
                // setValue('%s','transaction::TotalForeignValue', hasCustomValue("inZAR",false).and(singleMonetaryAmount)),
                disable(singleMonetaryAmount),
                //, setValue(undefined, null, isCurrencyIn("ZAR").or(evalTransactionField("ZAREquivalent", notEmpty)))
                //, setValue(undefined, null, evalTransactionField("ZAREquivalent", notEmpty))
                //, setValue(undefined, null, evalTransactionField("ZAREquivalent", notEmpty).or(isCurrencyIn("ZAR")))

                // setValue(function (context, ind) {
                //   var exchangeRatio = Number(context.getTransactionField("TotalForeignValue")) /
                //     Number(context.getCustomValue("TotalRandAmount"));
                //   var result = Number(context.getMoneyField(ind, map('{{LocalValue}}')() )) * exchangeRatio;
                //   return result.toFixed(2);
                // }, hasCustomField('TotalRandAmount').and(hasCustomValue("inZAR",true)))
              ]
            },
            {
              field: "{{LocalValue}}",
              display: [
                hide(),
                show(hasCustomValue("inZAR",true).or(hasTransactionFieldValue("FlowCurrency", "ZAR"))),
                //, clearValue(evalTransactionField("ZAREquivalent", isEmpty))

                //, show(evalTransactionField("{{LocalCurrency}}Equivalent", notEmpty).or(isCurrencyIn("ZAR"))),
                //show(evalTransactionField("ZAREquivalent", notEmpty).or(hasTransactionFieldValue("TransactionCurrency", "ZAR")))

                //, clearValue(notEmpty.and(evalMoneyField("ForeignValue", notEmpty)))
                setValue(function(context){
                  return context.getCustomValue('TotalRandAmount');
                },hasCustomValue("inZAR",true).and(singleMonetaryAmount)),
                
                disable(singleMonetaryAmount),

                setValue(function (context, ind) {
                  var exchangeRatio = Number(context.getCustomValue("TotalRandAmount")) /
                    Number(context.getTransactionField("TotalForeignValue"));
                  var result = Number(context.getMoneyField(ind, "ForeignValue")) * exchangeRatio;
                  return result.toFixed(2);
                }, hasCustomField('TotalRandAmount').and(hasCustomValue("inZAR",false)))
              ]
            },
            {
              field: "MoneyTransferAgentIndicator",
              display: [
                hide(),
                setValue('AD').notOnCategory(['833']),
                setValue('',null,hasValue('AD')).onCategory(['833']),
                show().onCategory(['833']),
              ]
            },
            {
              field: "ThirdParty.ContactDetails.ContactName",
              display: [
                hide(hasMoneyField('ThirdParty.Individual')),
                setValue('%s','money::ThirdParty.Individual.Name',hasMoneyField('ThirdParty.Individual.Name'))//.onOutflow().onCategory(['255','256'])
              ]
            },
            {
              field: "ThirdParty.ContactDetails.ContactSurname",
              display: [
                hide(hasMoneyField('ThirdParty.Individual')),
                setValue('%s','money::ThirdParty.Individual.Surname',hasMoneyField('ThirdParty.Individual.Surname'))//.onOutflow().onCategory(['255','256'])
              ]
            },
            {
              field : "allowance",
              display: [
                hide().onOutflow().onCategory(['100','200','400','500','600','700','800','511']),
                setValue('N',undefined,isEmpty),
                setValue('SDA',undefined,returnTrue).onCategory('511'),
                setValue('N',undefined,returnTrue).onCategory(['100','200','400','500','600','700','800'])
              ]
            },
            {
              field: "AdHocRequirement.Subject",
              display: [
                hide(),
                setValue("NO", undefined, // hack to not have to show the Subject
                  hasTransactionField("Resident.Entity").and(notValueIn(["YES", "NO"]))).onOutflow().onSection("A").onCategory(["512", "513"]),
                setValue("SDA",undefined,
                  returnTrue).onCategory("511")
                
              ]
            },
            {
              field: "AdHocRequirement.Description",
              display: [
                hide(),
                setValue("NONE", undefined, // hack to not have to show the Subject
                  hasTransactionField("Resident.Entity").and(hasMoneyFieldValue("AdHocRequirement.Subject", "NO"))).onOutflow().onSection("A").onCategory(["512", "513"]),
                setValue("Single Discretionary Allowance",undefined,
                  returnTrue).onCategory("511")
              ]
            },
            {
              field: "LoanInterestRate",
              display: [
                hide(),
                show().onInflow().onSection("AB").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
                clearValue().onInflow().onSection("AB").notOnCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"])
              ]
            }
          ]
        },
        detailImportExport: {
          ruleset: "Flow Import/Export Display Rules",
          scope: "importexport",
          fields: []

        }
      };
    }

    return display;
  }
});