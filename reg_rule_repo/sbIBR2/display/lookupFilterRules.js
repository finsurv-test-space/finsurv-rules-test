define(function () {
  return function (predef) {
    var filterLookupRules;
    with (predef) {
      filterLookupRules = {
        filterLookupTrans: {
          ruleset: "Reporting Transaction Lookup Filter Rules",
          scope: "transaction",
          fields: [

          ]
        },

        filterLookupMoney: {
          ruleset: "Reporting Monetary Lookup Filter Rules",
          scope: "money",
          fields: [

            {
              field: "CompoundCategoryCode",
              display: [
                limitValue(['ZZ1']).onSection("C"),
                excludeValue(['ZZ1', 'ZZ2']).onSection("ABDEF"),
                //Daniela's list of exclusions.
                //excludeValue(["601", "602", "603", "605", "610", "611", "612", ]), 
                //LIBRA-2165
                //excludeValue(["250", "255", "260"]), 
                excludeValue(["511", "512", "513"], hasTransactionFieldValue("AccountHolderStatus", "Foreign Temporary Resident")).onSection("AB"),

                // BITTS EXCLUDED CATEGORIES
                //excludeValue(["100", "200", "300", "400", "500", "600", "700", "800", "900"]).onInflow(), 
                //excludeValue(["801", "802", "803", "804", "810", "815", "816", "817", "818", "819", "833", "830", "832"]).onInflow(), 
                
                //excludeValue(["256"],hasTransactionField("Resident.Entity")).onSection("AB"),
                excludeValue(["255"], hasTransactionField("Resident.Individual")).onSection("AB"),
                

                // BITTS Removed Stuff
                excludeValue(["401"], hasTransactionFieldValue("AccountHolderStatus", "Foreign Temporary Resident")).onOutflow().onSection("AB"),
                excludeValue(["401"], hasTransactionField("Resident.Entity").or(hasTransactionField("NonResident.Entity")))
              ]
            },
            {
              field: "LoanInterest.BaseRate",
              display: [
                excludeValue(["FIXED"])
              ]
            },
            {
              field: "ThirdPartyKind",
              display: [
                // "If CategoryCode 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onOutflow().onSection("AB").onCategory(["511", "512", "513"]),

                // "If the category is 256 and the PassportNumber under Resident Individual contains no value, Individual must be completed",
                limitValue(["Individual"], notTransactionField("Resident.Individual.PassportNumber")).onSection("AB").onCategory("256"),

                // "If the category is 255 or 256 and the Resident Entity element is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onSection("AB").onCategory(["255", "256"]),

                // "If the SupplementaryCardIndicator is Y, Individual must be completed"
                limitValue(["Individual"], hasResidentFieldValue("SupplementaryCardIndicator", "Y")).onSection("E"),

                // "If the Flow is IN and category 303, 304, 305, 306, 416 or 417 is used and Resident Entity is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onInflow().onSection("AB").onCategory(["303", "304", "305", "306", "416", "417"]),

                // "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onInflow().onSection("A").onCategory(["511", "516"]),

                // If Resident Entity and Subject SDA, limit to Individual.
                limitValue(["Individual"], hasTransactionField("Resident.Entity").and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).onOutflow().onSection("A")

              ]
            },
            {
              field: "allowance",
              display: [
                excludeValue(['FIA']).onOutflow().notOnCategory(['512', '513'])
              ]
            }
          ]
        },

        filterLookupImportExport: {
          ruleset: "Reporting Import Export Lookup Filter Rules",
          scope: "importexport",
          fields: []
        }
      }
    }

    return filterLookupRules;
  }
});


