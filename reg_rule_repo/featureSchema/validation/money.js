define(function () {
  return function (predef) {
    var money;
    with (predef) {

      money = {
        ruleset: "Schema-based Money Rules",
        scope: "money",
        validations: [
          {
            field: ["MoneyTransferAgentIndicator",
                    "SWIFTDetails",
                    "StrateRefNumber",
                    "LoanRefNumber",
                    "LoanTenor",
                    "LoanInterestRate",
                    "{{Regulator}}Auth.RulingsSection",
                    "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
                    "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
                    "CannotCategorize",
                    "AdHocRequirement.Subject",
                    "AdHocRequirement.Description",
                    "LocationCountry",
                    "ReversalTrnRefNumber",
                    "BOPDIRTrnReference",
                    "BOPDIR{{DealerPrefix}}Code",
                    "ThirdParty.Individual.Surname",
                    "ThirdParty.Individual.Name",
                    "ThirdParty.Individual.Gender",
                    "ThirdParty.Individual.TempResPermitNumber",
                    "ThirdParty.Individual.PassportNumber",
                    "ThirdParty.Individual.PassportCountry",
                    "ThirdParty.Entity.Name",
                    "ThirdParty.Entity.RegistrationNumber",
                    "ThirdParty.CustomsClientNumber",
                    "ThirdParty.TaxNumber",
                    "ThirdParty.VATNumber",
                    "ThirdParty.StreetAddress.AddressLine1",
                    "ThirdParty.StreetAddress.AddressLine2",
                    "ThirdParty.StreetAddress.Suburb",
                    "ThirdParty.StreetAddress.City",
                    "ThirdParty.StreetAddress.State",
                    "ThirdParty.StreetAddress.PostalCode",
                    "ThirdParty.PostalAddress.AddressLine1",
                    "ThirdParty.PostalAddress.AddressLine2",
                    "ThirdParty.PostalAddress.Suburb",
                    "ThirdParty.PostalAddress.City",
                    "ThirdParty.PostalAddress.State",
                    "ThirdParty.PostalAddress.PostalCode",
                    "ThirdParty.ContactDetails.ContactSurname",
                    "ThirdParty.ContactDetails.ContactName",
                    "ThirdParty.ContactDetails.Email",
                    "ThirdParty.ContactDetails.Fax",
                    "ThirdParty.ContactDetails.Telephone",
                    "CardChargeBack",
                    "CardIndicator",
                    "ElectronicCommerceIndicator",
                    "POSEntryMode",
                    "CardFraudulentTransactionIndicator"
                   ],
            rules: [
              failure("sch_mstr", "SCS", "This value must be a string",
                notSimpleValue)
            ]
          },
          {
            field: ["{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
                    "ThirdParty.Individual.DateOfBirth"
                   ],
            rules: [
              failure("sch_mdate", "SCD", "This value must be a valid date",
                notEmpty.and(notDatePattern))
            ]
          },
          {
            field: ["SequenceNumber",
                    "{{LocalValue}}",
                    "ForeignValue",
                    "ReversalTrnSeqNumber",
                    "ForeignCardHoldersPurchases{{LocalValue}}",
                    "ForeignCardHoldersCashWithdrawals{{LocalValue}}"
                   ],
            rules: [
              failure("sch_mnum", "SCN", "This value must be a number",
                notEmpty.and(notValidNumber))
            ]
          },
          {
            field: ["{{Regulator}}Auth",
                    "AdHocRequirement",
                    "ThirdParty",
                    "ThirdParty.Individual",
                    "ThirdParty.Entity",
                    "ThirdParty.StreetAddress",
                    "ThirdParty.PostalAddress",
                    "ThirdParty.ContactDetails"
                   ],
            rules: [
              failure("sch_mcmp", "SCC", "This value must be an object",
                notComplexValue)
            ]
          },
          {
            field: "ImportExport",
            rules: [
              failure("sch_marr", "SCA", "This value must be an array",
                notArrayValue)
            ]
          }
        ]
      };


    }
    return money;
  }
});
