define(function () {
  return function (predef) {
    var trans;
    with (predef) {
      trans = {
        ruleset: "Schema-based Transaction Rules",
        scope: "transaction",
        validations: [
          {
            field: ["ReplacementTransaction",
                    "ReplacementOriginalReference",
                    "ReportingQualifier",
                    "Flow",
                    "FlowCurrency",
                    "TrnReference",
                    "OriginatingBank",
                    "OriginatingCountry",
                    "CorrespondentBank",
                    "CorrespondentCountry",
                    "ReceivingBank",
                    "ReceivingCountry",
                    "NonResident.Individual.Surname",
                    "NonResident.Individual.Name",
                    "NonResident.Individual.Gender",
                    "NonResident.Individual.PassportNumber",
                    "NonResident.Individual.PassportCountry",
                    "NonResident.Individual.AccountIdentifier",
                    "NonResident.Individual.AccountNumber",
                    "NonResident.Individual.Address.AddressLine1",
                    "NonResident.Individual.Address.AddressLine2",
                    "NonResident.Individual.Address.Suburb",
                    "NonResident.Individual.Address.City",
                    "NonResident.Individual.Address.State",
                    "NonResident.Individual.Address.PostalCode",
                    "NonResident.Individual.Address.Country",
                    "NonResident.Entity.EntityName",
                    "NonResident.Entity.CardMerchantName",
                    "NonResident.Entity.CardMerchantCode",
                    "NonResident.Entity.AccountIdentifier",
                    "NonResident.Entity.AccountNumber",
                    "NonResident.Entity.Address.AddressLine1",
                    "NonResident.Entity.Address.AddressLine2",
                    "NonResident.Entity.Address.Suburb",
                    "NonResident.Entity.Address.City",
                    "NonResident.Entity.Address.State",
                    "NonResident.Entity.Address.PostalCode",
                    "NonResident.Entity.Address.Country",
                    "NonResident.Exception.ExceptionName",
                    "Resident.Individual.Surname",
                    "Resident.Individual.Name",
                    "Resident.Individual.Gender",
                    "Resident.Individual.TempResPermitNumber",
                    "Resident.Individual.ForeignIDNumber",
                    "Resident.Individual.ForeignIDCountry",
                    "Resident.Individual.PassportNumber",
                    "Resident.Individual.PassportCountry",
                    "Resident.Individual.AccountName",
                    "Resident.Individual.AccountIdentifier",
                    "Resident.Individual.AccountNumber",
                    "Resident.Individual.CustomsClientNumber",
                    "Resident.Individual.TaxNumber",
                    "Resident.Individual.VATNumber",
                    "Resident.Individual.TaxClearanceCertificateIndicator",
                    "Resident.Individual.TaxClearanceCertificateReference",
                    "Resident.Individual.StreetAddress.AddressLine1",
                    "Resident.Individual.StreetAddress.AddressLine2",
                    "Resident.Individual.StreetAddress.Suburb",
                    "Resident.Individual.StreetAddress.City",
                    "Resident.Individual.StreetAddress.State",
                    "Resident.Individual.StreetAddress.PostalCode",
                    "Resident.Individual.PostalAddress.AddressLine1",
                    "Resident.Individual.PostalAddress.AddressLine2",
                    "Resident.Individual.PostalAddress.Suburb",
                    "Resident.Individual.PostalAddress.City",
                    "Resident.Individual.PostalAddress.State",
                    "Resident.Individual.PostalAddress.PostalCode",
                    "Resident.Individual.ContactDetails.ContactSurname",
                    "Resident.Individual.ContactDetails.ContactName",
                    "Resident.Individual.ContactDetails.Email",
                    "Resident.Individual.ContactDetails.Fax",
                    "Resident.Individual.ContactDetails.Telephone",
                    "Resident.Individual.CardNumber",
                    "Resident.Individual.SupplementaryCardIndicator",
                    "Resident.Entity.EntityName",
                    "Resident.Entity.TradingName",
                    "Resident.Entity.RegistrationNumber",
                    "Resident.Entity.AccountName",
                    "Resident.Entity.AccountIdentifier",
                    "Resident.Entity.AccountNumber",
                    "Resident.Entity.CustomsClientNumber",
                    "Resident.Entity.TaxNumber",
                    "Resident.Entity.VATNumber",
                    "Resident.Entity.TaxClearanceCertificateIndicator",
                    "Resident.Entity.TaxClearanceCertificateReference",
                    "Resident.Entity.StreetAddress.AddressLine1",
                    "Resident.Entity.StreetAddress.AddressLine2",
                    "Resident.Entity.StreetAddress.Suburb",
                    "Resident.Entity.StreetAddress.City",
                    "Resident.Entity.StreetAddress.State",
                    "Resident.Entity.StreetAddress.PostalCode",
                    "Resident.Entity.PostalAddress.AddressLine1",
                    "Resident.Entity.PostalAddress.AddressLine2",
                    "Resident.Entity.PostalAddress.Suburb",
                    "Resident.Entity.PostalAddress.City",
                    "Resident.Entity.PostalAddress.State",
                    "Resident.Entity.PostalAddress.PostalCode",
                    "Resident.Entity.ContactDetails.ContactSurname",
                    "Resident.Entity.ContactDetails.ContactName",
                    "Resident.Entity.ContactDetails.Email",
                    "Resident.Entity.ContactDetails.Fax",
                    "Resident.Entity.ContactDetails.Telephone",
                    "Resident.Entity.CardNumber",
                    "Resident.Entity.SupplementaryCardIndicator",
                    "Resident.Exception.ExceptionName",
                    "Resident.Exception.Country"
                   ],
            rules: [
              failure("sch_str", "SCS", "This value must be a string",
                notSimpleValue)
            ]
          },
          {
            field: ["ValueDate",
                    "Resident.Individual.DateOfBirth"
                   ],
            rules: [
              failure("sch_date", "SCD", "This value must be a valid date",
                notEmpty.and(notDatePattern))
            ]
          },
          {
            field: ["TotalForeignValue",
                    "Resident.Entity.InstitutionalSector",
                    "Resident.Entity.IndustrialClassification"
                   ],
            rules: [
              failure("sch_num", "SCN", "This value must be a number",
                notEmpty.and(notValidNumber))
            ]
          },
          {
            field: ["NonResident",
                    "NonResident.Individual",
                    "NonResident.Individual.Address",
                    "NonResident.Entity",
                    "NonResident.Entity.Address",
                    "NonResident.Exception",
                    "Resident",
                    "Resident.Individual",
                    "Resident.Individual.StreetAddress",
                    "Resident.Individual.PostalAddress",
                    "Resident.Individual.ContactDetails",
                    "Resident.Entity",
                    "Resident.Entity.StreetAddress",
                    "Resident.Entity.PostalAddress",
                    "Resident.Entity.ContactDetails",
                    "Resident.Exception"
                   ],
            rules: [
              failure("sch_cmp", "SCC", "This value must be an object",
                notComplexValue)
            ]
          },
          {
            field: "MonetaryAmount",
            rules: [
              failure("sch_arr", "SCA", "This value must be an array",
                notArrayValue)
            ]
          }
        ]
      };
    }
    return trans;
  }
});
