define(function () {
  return function (predef) {
    var stdImportExport;
    with (predef) {
      stdImportExport = {
        ruleset: "Standard Import/Export Rules",
        scope: "importexport",
        validations: [
          {
            field: ["ImportControlNumber",
                    "TransportDocumentNumber",
                    "UCR",
                    "PaymentCurrencyCode",
                    "MRNNotOnIVS"
                   ],
            rules: [
              failure("sch_iestr", "SCS", "This value must be a string",
                notSimpleValue)
            ]
          },
          {
            field: ["PaymentValue"
                   ],
            rules: [
              failure("sch_ienum", "SCN", "This value must be a number",
                notEmpty.and(notValidNumber))
            ]
          }
        ]
      };
    }

    return stdImportExport;
  }
});
