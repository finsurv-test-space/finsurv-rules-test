define(function() {
  return function(testBase) {
      with (testBase) {
        var mappings = {
            LocalValue: "RandValue",
            Regulator: "SARB",
            DealerPrefix: "AD",
            RegulatorPrefix: "SARB"
        };

        setMappings(mappings, true);
        
        var test_cases = [
          assertSuccess("sch_str:1", {
            ReplacementTransaction: 'replacingme'
          }),
          assertSuccess("sch_str:1", {
            ReplacementTransaction: ''
          }),
          assertSuccess("sch_str:1", {
          }),
          assertFailure("sch_str:1", {
            ReplacementTransaction: {}
          }),

          assertSuccess("sch_date:1", {
            ValueDate: '2017-01-01'
          }),
          assertSuccess("sch_date:1", {
            ValueDate: ''
          }),
          assertSuccess("sch_date:1", {
          }),
          assertFailure("sch_date:1", {
            ValueDate: {}
          }),

          assertSuccess("sch_num:1", {
            TotalForeignValue: 10.34
          }),
          assertSuccess("sch_num:1", {
            TotalForeignValue: 10
          }),
          assertSuccess("sch_num:1", {
            "TotalForeignValue": 144000.81
          }),
          assertSuccess("sch_num:1", {
            TotalForeignValue: '10'
          }),
          assertSuccess("sch_num:1", {
          }),
          assertFailure("sch_num:1", {
            TotalForeignValue: 'num'
          }),
          assertFailure("sch_num:1", {
            TotalForeignValue: {}
          }),

          assertSuccess("sch_cmp:1", {
            NonResident: {}
          }),
          assertSuccess("sch_cmp:1", {
          }),
          assertFailure("sch_cmp:1", {
            NonResident: 'Me'
          }),
          assertFailure("sch_cmp:1", {
            NonResident: true
          }),

          assertSuccess("sch_arr", {
            MonetaryAmount: []
          }),
          assertSuccess("sch_arr", {
          }),
          assertFailure("sch_arr", {
            MonetaryAmount: 'Me'
          }),
          assertFailure("sch_arr", {
            MonetaryAmount: true
          }),
          assertFailure("sch_arr", {
            MonetaryAmount: {}
          }),

          assertSuccess("sch_mstr:1", {
            MonetaryAmount: [{MoneyTransferAgentIndicator: 'stuff'}]
          }),
          assertSuccess("sch_mstr:1", {
            MonetaryAmount: [{MoneyTransferAgentIndicator: ''}]
          }),
          assertSuccess("sch_mstr:1", {
            MonetaryAmount: [{}]
          }),
          assertFailure("sch_mstr:1", {
            MonetaryAmount: [{MoneyTransferAgentIndicator: {}}]
          }),

          assertSuccess("sch_mdate:1", {
            MonetaryAmount: [{SARBAuth: {ADInternalAuthNumberDate: '2017-01-01'}}]
          }),
          assertSuccess("sch_mdate:1", {
            MonetaryAmount: [{SARBAuth: {ADInternalAuthNumberDate: ''}}]
          }),
          assertSuccess("sch_mdate:1", {
            MonetaryAmount: [{}]
          }),
          assertFailure("sch_mdate:1", {
            MonetaryAmount: [{SARBAuth: {ADInternalAuthNumberDate: {}}}]
          }),

          assertSuccess("sch_mnum:1", {
            MonetaryAmount: [{SequenceNumber: 1}]
          }),
          assertSuccess("sch_mnum:1", {
            MonetaryAmount: [{SequenceNumber: '1'}]
          }),
          assertSuccess("sch_mnum:1", {
            MonetaryAmount: [{}]
          }),
          assertFailure("sch_mnum:1", {
            MonetaryAmount: [{SequenceNumber: 'num'}]
          }),
          assertFailure("sch_mnum:1", {
            MonetaryAmount: [{SequenceNumber: {}}]
          }),

          assertSuccess("sch_mnum:2", {
            MonetaryAmount: [{RandValue: 10.34}]
          }),
          assertSuccess("sch_mnum:2", {
            MonetaryAmount: [{RandValue: 10}]
          }),
          assertSuccess("sch_mnum:2", {
            MonetaryAmount: [{RandValue: '10'}]
          }),
          assertSuccess("sch_mnum:2", {
            MonetaryAmount: [{}]
          }),
          assertFailure("sch_mnum:2", {
            MonetaryAmount: [{RandValue: 'num'}]
          }),
          assertFailure("sch_mnum:2", {
            MonetaryAmount: [{RandValue: {}}]
          }),

          assertSuccess("sch_mcmp:1", {
            MonetaryAmount: [{SARBAuth: {}}]
          }),
          assertSuccess("sch_mcmp:1", {
            MonetaryAmount: [{}]
          }),
          assertFailure("sch_mcmp:1", {
            MonetaryAmount: [{SARBAuth: 'Me'}]
          }),
          assertFailure("sch_mcmp:1", {
            MonetaryAmount: [{SARBAuth: true}]
          }),

          assertSuccess("sch_marr", {
            MonetaryAmount: [{ImportExport: []}]
          }),
          assertSuccess("sch_marr", {
            MonetaryAmount: [{}]
          }),
          assertFailure("sch_marr", {
            MonetaryAmount: [{ImportExport: 'Me'}]
          }),
          assertFailure("sch_marr", {
            MonetaryAmount: [{ImportExport: true}]
          }),
          assertFailure("sch_marr", {
            MonetaryAmount: [{ImportExport: {}}]
          }),

          assertSuccess("sch_iestr:1", {
            MonetaryAmount: [{ImportExport: [{ImportControlNumber: 'stuff'}]}]
          }),
          assertSuccess("sch_iestr:1", {
            MonetaryAmount: [{ImportExport: [{ImportControlNumber: ''}]}]
          }),
          assertSuccess("sch_iestr:1", {
            MonetaryAmount: [{ImportExport: [{}]}]
          }),
          assertFailure("sch_iestr:1", {
            MonetaryAmount: [{ImportExport: [{ImportControlNumber: {}}]}]
          }),

          assertSuccess("sch_ienum", {
            MonetaryAmount: [{ImportExport: [{PaymentValue: 10.34}]}]
          }),
          assertSuccess("sch_ienum", {
            MonetaryAmount: [{ImportExport: [{PaymentValue: 10}]}]
          }),
          assertSuccess("sch_ienum", {
            MonetaryAmount: [{ImportExport: [{PaymentValue: '10'}]}]
          }),
          assertSuccess("sch_ienum", {
            MonetaryAmount: [{ImportExport: [{}]}]
          }),
          assertFailure("sch_ienum", {
            MonetaryAmount: [{ImportExport: [{PaymentValue: {}}]}]
          }),
          assertFailure("sch_ienum", {
            MonetaryAmount: [{ImportExport: [{PaymentValue: 'not num'}]}]
          }),
          assertFailure("sch_ienum", {
            MonetaryAmount: [{ImportExport: [{PaymentValue: true}]}]
          })
        ]

      }
    return testBase;
  }
})
