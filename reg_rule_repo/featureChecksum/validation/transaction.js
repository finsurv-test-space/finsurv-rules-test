define(function () {
  return function (predef) {
    var feature;
    with (predef) {

      feature = {
        ruleset: "Transaction Checksum Rules",
        scope: "transaction",
        validations: [
          {
            field : "TotalValue",
            rules : [
              failure("totv1", 244, "RandValue + ForeignValue must equal TotalValue",
                  isDefined.and(notSumTotalValue)).onSection("ABCDEG")
            ]
          }
        ]
      };
   }
    return feature;
  }
});
