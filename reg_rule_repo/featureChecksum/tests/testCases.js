define(function() {
  return function(testBase) {
      with (testBase) {

        var test_cases = [
          //TotalValue
          assertSuccess("totv1", {ReportingQualifier: 'BOPCUS'}),
          assertSuccess("totv1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{RandValue: '50.00', ForeignValue: '5.00'}, {RandValue: '40.00', ForeignValue: '4.00'}]
          }),
          assertSuccess("totv1", {
            ReportingQualifier: 'BOPCUS', TotalValue: '90.00',
            MonetaryAmount: [{RandValue: '50.00'}, {RandValue: '40.00'}]
          }),
          assertSuccess("totv1", {
            ReportingQualifier: 'BOPCUS', TotalValue: '99.00',
            MonetaryAmount: [{RandValue: '50.00', ForeignValue: '5.00'}, {RandValue: '40.00', ForeignValue: '4.00'}]
          }),
          assertFailure("totv1", {
            ReportingQualifier: 'BOPCUS', TotalValue: '89.00',
            MonetaryAmount: [{RandValue: '50.00'}, {RandValue: '40.00'}]
          }),
          assertFailure("totv1", {
            ReportingQualifier: 'BOPCUS', TotalValue: '100.00',
            MonetaryAmount: [{RandValue: '50.00', ForeignValue: '5.00'}, {RandValue: '40.00', ForeignValue: '4.00'}]
          })
        ]

      }
    return testBase;
  }
})
