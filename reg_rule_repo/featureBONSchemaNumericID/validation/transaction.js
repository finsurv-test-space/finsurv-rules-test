define(function () {
  return function (predef) {
    var trans;
    with (predef) {
      trans = {
        ruleset: "Schema-based Transaction Rules",
        scope: "transaction",
        validations: [
          {
            field: ["Resident.Individual.IDNumber"
                   ],
            rules: [
              failure("nm_sch_num", "SCN", "This value must be a number",
                notEmpty.and(notPattern(/^\d*$/)))
            ]
          }
        ]
      };
    }
    return trans;
  }
});
