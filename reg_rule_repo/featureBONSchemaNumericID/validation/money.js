define(function () {
  return function (predef) {
    var money;
    with (predef) {

      money = {
        ruleset: "Schema-based Money Rules",
        scope: "money",
        validations: [
          {
            field: [
                    "ThirdParty.Individual.IDNumber"
                   ],
            rules: [
              failure("nm_sch_mnum", "SCN", "This value must be a number",
                notEmpty.and(notPattern(/^\d*$/)))
            ]
          }
        ]
      };
    }
    return money;
  }
});
