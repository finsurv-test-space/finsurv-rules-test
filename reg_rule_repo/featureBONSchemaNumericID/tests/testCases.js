define(function() {
  return function(testBase) {
      with (testBase) {
        // var mappings = {
        //     LocalValue: "RandValue",
        //     Regulator: "SARB",
        //     DealerPrefix: "AD",
        //     RegulatorPrefix: "SARB"
        // };
        var mappings = {
          LocalValue: "DomesticValue",
          Regulator: "Regulator",
          DealerPrefix: "RE",
          RegulatorPrefix: "CB"
        };

        var test_cases = [
          assertSuccess("nm_sch_mnum", {
            MonetaryAmount: [{ThirdParty:{Individual:{IDNumber: 12345678}}}]
          }),
          assertSuccess("nm_sch_mnum", {
            MonetaryAmount: [{ThirdParty:{Individual:{IDNumber: '12345678'}}}]
          }),
          assertFailure("nm_sch_mnum", {
            MonetaryAmount: [{ThirdParty:{Individual:{IDNumber: '12345678EFG'}}}]
          }),


          assertSuccess("nm_sch_num", {
            Resident: {Individual:{IDNumber: 12345678}}
          }),
          assertSuccess("nm_sch_num", {
            Resident:{Individual:{IDNumber: '12345678'}}
          }),
          assertFailure("nm_sch_num", {
            Resident:{Individual:{IDNumber: '12345678EFG'}}
          })
        ]

      }
    return testBase;
  }
})
