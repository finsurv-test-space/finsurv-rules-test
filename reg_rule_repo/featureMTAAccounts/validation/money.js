define(function () {
  return function (predef) {
    var extTrans;
    with (predef) {

      extTrans = {
        ruleset: "Money Rules for MTA Accounts",
        scope: "money",
        validations: [
          {
            field: "CategoryCode",
            rules: [
              failure('mtaa_cc1', 'CC1', "Category 833 cannot be used for this transaction since the Resident Account Number is not an MTA account",
                notEmpty.and(not(evalResidentField("AccountNumber", isInLookup('mtaAccounts', 'accountNumber'))))).onSection("A").onCategory("833"),
              failure('mtaa_cc2', 'CC2', "Category 833 must be used for this transaction since the Resident Account Number is an MTA (ADLA) account",
                evalResidentField("AccountNumber", hasLookupValue('mtaAccounts', 'accountNumber', 'isADLA', true))
                  .and(evalResidentField("AccountNumber", isInLookup('mtaAccounts', 'accountNumber')))).onSection("A").notOnCategory("833")
            ]
          }
        ]
      };

    }
    return extTrans;
  }
});
