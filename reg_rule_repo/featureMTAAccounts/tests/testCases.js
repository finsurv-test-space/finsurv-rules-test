define(function() {
  return function(testBase) {
      with (testBase) {

        var test_cases = [
          //money[CategoryCode] Category 833 cannot be used for this transaction since the Resident Account Number is not an MTA account
          assertSuccess("mtaa_cc1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {AccountNumber: "12345678"}},
            MonetaryAmount: [{CategoryCode: '833'}]
          }),
          assertFailure("mtaa_cc1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {AccountNumber: "notanMTAaccount"}},
            MonetaryAmount: [{CategoryCode: '833'}]
          }),
          assertNoRule("mtaa_cc1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {AccountNumber: "notanMTAaccount"}},
            MonetaryAmount: [{CategoryCode: '401'}]
          }),
          assertSuccess("mtaa_cc1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {AccountNumber: "12345678"}},
            MonetaryAmount: [{CategoryCode: '833'}]
          }),
          assertFailure("mtaa_cc1", {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {AccountNumber: "notanMTAaccount"}},
            MonetaryAmount: [{CategoryCode: '833'}]
          }),

          //money[CategoryCode] Category 833 must be used for this transaction since the Resident Account Number is an MTA (ADLA) account
          assertSuccess("mtaa_cc2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {AccountNumber: "notanMTAaccount"}},
            MonetaryAmount: [{CategoryCode: '401'}]
          }),
          assertFailure("mtaa_cc2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {AccountNumber: "12345678"}},
            MonetaryAmount: [{CategoryCode: '401'}]
          }),
          assertNoRule("mtaa_cc2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {AccountNumber: "12345678"}},
            MonetaryAmount: [{CategoryCode: '833'}]
          }),
          assertSuccess("mtaa_cc2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {AccountNumber: "notanMTAaccount"}},
            MonetaryAmount: [{CategoryCode: '401'}]
          }),
          assertFailure("mtaa_cc2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {AccountNumber: "12345678"}},
            MonetaryAmount: [{CategoryCode: '401'}]
          }),
          assertNoRule("mtaa_cc2", {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {AccountNumber: "12345678"}},
            MonetaryAmount: [{CategoryCode: '833'}]
          })

        ]

      }
    return testBase;
  }
})
