define({
  ReportingQualifier           : ["BOPCUS", "NON REPORTABLE", "BOPCARD RESIDENT", "BOPCARD NON RESIDENT", "NON RESIDENT RAND", "INTERBANK", "BOPDIR"],
  mtaAccounts : [
    {
      accountNumber : "12341234",
      MTA : "RANDBUREAU",
      rulingSection : "foo",
      ADLALevel : "3",
      isADLA : false
    },
    {
     accountNumber : "12345678",
      MTA : "GLOBAL",
      rulingSection : "baz",
      ADLALevel : "2",
      isADLA : true
    }
  ]
})
