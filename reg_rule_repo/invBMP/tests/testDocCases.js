define(function() {
  return function(testBase) {
    with (testBase) {
      var mappings = {
        LocalCurrencySymbol: "R",
        LocalCurrencyName: "Rand",
        LocalCurrency: "ZAR",
        LocalValue: "RandValue",
        Regulator: "SARB",
        DealerPrefix: "AD",
        RegulatorPrefix: "SARB"
      };

      var test_cases = [
        // 101/01 - Out
        assertDocuments("101/01-Out", ["SupplierInvoice", "Mandate"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT', TrnReference: 'MyRef',
          MonetaryAmount: [
          {
            CategoryCode: '101', CategorySubCode: '01',
            ImportExport: [{ImportControlNumber: 'XYZ'}]
          }]
        })
      ];
    }
  }
})
