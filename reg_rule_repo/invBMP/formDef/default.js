/**
 * Created by petruspretorius on 24/03/2015.
 */
define(function () {
  
    var _export = {
      rootTemplate: 'SARB_Summary',
      tabRootTemplate:  'tabs/tabContainer',
      detail: {
          Transaction: {
              scope: "transaction",
              detail: {
                  section: "Transaction",
                  detail: 'transaction/transaction'
              },
              summary: 'transaction_container',
              fields: [
                  "ValueDate",
                  "FlowCurrency",
                  "TotalForeignValue",
                  "ZAREquivalent",
                  "BranchCode",
                  "BranchName",
                  "HubCode",
                  "HubName",
                  "OriginatingBank",
                  "OriginatingCountry",
                  "CorrespondentBank",
                  "CorrespondentCountry",
                  "ReceivingBank",
                  "ReceivingCountry",
                  "LocationCountry"
              ]
          },
          PaymentDetail: {
            scope: "transaction",
            detail: {
                section: "PaymentDetail",
                detail: 'transaction/transaction'
            },
            summary: 'transaction_container',
            fields: [
                "PaymentDetail.Charges",
                "PaymentDetail.SWIFTDetails",
                "PaymentDetail.BeneficiaryBank.SWIFTBIC",
                "PaymentDetail.BeneficiaryBank.BankName",
                "PaymentDetail.BeneficiaryBank.BranchCode",
                "PaymentDetail.BeneficiaryBank.Address",
                "PaymentDetail.BeneficiaryBank.City",
                "PaymentDetail.IsCorrespondentBank",
                "PaymentDetail.CorrespondentBank.SWIFTBIC",
                "PaymentDetail.CorrespondentBank.BankName",
                "PaymentDetail.CorrespondentBank.BranchCode",
                "PaymentDetail.CorrespondentBank.Address",
                "PaymentDetail.CorrespondentBank.City"
            ]
        },
        Resident                 : {
          scope: "transaction",
          contextKey: 'Resident',
          detail    : {
            section   : "Resident",
            detail :'resident/',
            subSection: 'general'
          },
          summary   : 'Resident',
          fields: ["Resident.Individual.Surname", "Resident.Individual.Name", "Resident.Individual.Gender",
              "Resident.Individual.DateOfBirth", "Resident.Individual.IDNumber",
            "Resident.Individual.TempResPermitNumber", "Resident.Individual.TempResExpiryDate",
            "Resident.Individual.ForeignIDNumber", "Resident.Individual.ForeignIDCountry",
            "Resident.Individual.PassportNumber", "Resident.Individual.PassportCountry", "Resident.Individual.PassportExpiryDate",
            "Resident.Individual.CustomsClientNumber", "Resident.Individual.TaxNumber", "Resident.Individual.VATNumber",
            "Resident.Individual.TaxClearanceCertificateIndicator", "Resident.Individual.TaxClearanceCertificateReference", "Resident.Exception.ExceptionName",
            "Resident.Exception.Country", "Resident.Entity.EntityName", "Resident.Entity.TradingName", "Resident.Entity.RegistrationNumber",
            "Resident.Entity.InstitutionalSector", "Resident.Entity.IndustrialClassification", "Resident.Entity.CustomsClientNumber", "Resident.Entity.TaxNumber",
            "Resident.Entity.VATNumber", "Resident.Entity.TaxClearanceCertificateIndicator", "Resident.Entity.TaxClearanceCertificateReference",
            "AccountHolderStatus", "Resident.Type"
       ]
        },
        ResidentAccount          : {
          scope: "transaction",
          contextKey: 'Resident',
          detail    : {
            section   : "Resident",
            detail :'resident/account',
            subSection: 'account'
          },
          summary   : 'Resident.Account',
          fields    : [ "AccountName", "AccountIdentifier", "AccountNumber", "CardNumber", "SupplementaryCardIndicator" ]
        },
        /** Contact, ContactStreet and ContactPostal share the same partial, but are in different subsections */
        ResidentContact          : {
          scope: "transaction",
          contextKey: 'Resident',
          detail    : {
            section   : "Resident",
            detail :'resident/address',
            subSection: 'contact'
          },
          summary   : 'Resident.ContactDetails',
          fields    : [ 
            "ContactDetails.ContactName", "ContactDetails.ContactSurname", "ContactDetails.Email", "ContactDetails.Fax", "ContactDetails.Telephone" 
          ]
        },
        ResidentContactStreet          : {
          scope: "transaction",
          contextKey: 'Resident',
          detail    : {
            section   : "Resident",
            detail :'resident/address',
            subSection: 'contactStreet'
          },
          summary   : 'Resident.ContactDetails',
          fields    : [ 
            "StreetAddress.AddressLine1", "StreetAddress.AddressLine2", "StreetAddress.Suburb", "StreetAddress.City", "StreetAddress.Province", "StreetAddress.PostalCode"
          ]
        },
        ResidentContactPostal          : {
          scope: "transaction",
          contextKey: 'Resident',
          detail    : {
            section   : "Resident",
            detail :'resident/address',
            subSection: 'contactPostal'
          },
          summary   : 'Resident.ContactDetails',
          fields    : [ 
            "PostalAddress.AddressLine1", "PostalAddress.AddressLine2", "PostalAddress.Suburb", "PostalAddress.City", "PostalAddress.Province", "PostalAddress.PostalCode" 
          ]
        },
        NonResident              : {
          scope: "transaction",
          contextKey: 'NonResident',
          detail    : {
            section   : "NonResident",
            detail :'nonResident/',
            subSection: 'general'
          },
          summary   : 'NonResident.Main',
          fields    : [ "NonResident.Individual.Surname", "NonResident.Individual.Name", "NonResident.Individual.Gender", "NonResident.Individual.PassportNumber",
            "NonResident.Individual.PassportCountry", "NonResident.Exception.ExceptionName", "NonResident.Entity.EntityName", "NonResident.Entity.CardMerchantName",
            "NonResident.Entity.CardMerchantCode", "NonResident.Individual.IsMutualParty", "NonResident.Entity.IsMutualParty",
            "CounterpartyStatus", "NonResident.Type"]
        },
        NonResidentAccount       : {
          scope: "transaction",
          contextKey: 'NonResident',
          detail    : {
            section   : "NonResident",
            detail :'nonResident/account',
            subSection: 'account'
          },
          summary   : 'NonResident.Account',
          fields    : [ "AccountIdentifier", "AccountNumber" ]
        },
        NonResidentAddress       : {
          scope: "transaction",
          contextKey: 'NonResident',
          detail    : {
            section   : "NonResident",
            detail :'nonResident/address',
            subSection: 'contact'
          },
          summary   : 'NonResident.Address',
          fields    : [ "Address.AddressLine1", "Address.AddressLine2", "Address.Suburb", "Address.City", "Address.State", "Address.PostalCode", "Address.Country" ]
        },
        Monetary                 : {
          scope: "money",
          detail : {
            section   : "Money",
            detail :'monetary/main',
            subSection: 'general'
          },
          summary: 'Description',
          fields: ["MoneyTransferAgentIndicator", "RandValue", "ForeignValue"
                      , "{{Regulator}}Auth.IsSARBAuth",
              "CategoryCode", "CategorySubCode", "SWIFTDetails", "StrateRefNumber",
              "{{Regulator}}Auth.RulingsSection", "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
              "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate", "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
              "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber", "{{Regulator}}Auth.SARBAuthDate",
            "CannotCategorize", "AdHocRequirement.Subject",
            "AdHocRequirement.Description", "LocationCountry", "ReversalTrnRefNumber",
            "ReversalTrnSeqNumber", "BOPDIRTrnReference", "BOPDIRADCode", "CardChargeBack",
            "CardIndicator", "ElectronicCommerceIndicator", "POSEntryMode",
            "CardFraudulentTransactionIndicator", "ForeignCardHoldersPurchasesRandValue",
            "ForeignCardHoldersCashWithdrawalsRandValue",
            "LoanTenor", "LoanInterestRate",
            "LoanTenorMaturityDate", "LoanInterest.InterestType",
            "LoanInterest.BaseRate", "LoanInterest.Term",
            "LoanInterest.PlusMinus", "LoanInterest.Rate", "LoanTenorType",
            "{{Regulator}}Auth.AuthIssuer", "ThirdParties", "ThirdPartyKind", 'ThirdParty',
            "ADInternalAuthResponseCode",
            "CustomsClientNumber", "LoanRefNumber", "LoanRefNumberResponseCode",
            "TaxClearanceCertificateIndicator" , "TravelMode.TravellerStatus",
            "TravelMode.Mode", "TravelMode.BorderPost", "TravelMode.TicketNumber",
            "TravelMode.DepartureDate", "TravelMode.DestinationCountry"
          ]
        },
        MonetaryThirdParty       : {
          scope: "money",
          detail : {
            section   : "Money",
            detail :'monetary/thirdparty',
            subSection: 'thirdparty'
          },
          summary: 'ThirdParty',
          fields: ["ThirdParty.Individual.Surname", "ThirdParty.Individual.Name",
              "ThirdParty.Individual.Gender", "ThirdParty.Individual.IDNumber",
            "ThirdParty.Individual.DateOfBirth", "ThirdParty.Individual.TempResPermitNumber",
            "ThirdParty.Individual.TempResExpiryDate",
            "ThirdParty.Individual.PassportNumber", "ThirdParty.Individual.PassportCountry",
            "ThirdParty.Individual.PassportExpiryDate",
            "ThirdParty.Entity.Name", "ThirdParty.Entity.RegistrationNumber",
            "ThirdParty.CustomsClientNumber", "ThirdParty.TaxNumber",
            "ThirdParty.VATNumber", "ThirdParty.CustomLookup2", "ThirdParty.Type" ]
        },
        MonetaryThirdPartyContact: {
          scope: "money",
          detail : {
            section   : "Money",
            detail :'monetary/thirdparty_contact',
            subSection: 'contact'
          },
          summary: 'ThirdPartyContact',
          fields : [ "ThirdParty.ContactDetails.ContactSurname", "ThirdParty.ContactDetails.ContactName", "ThirdParty.ContactDetails.Email", "ThirdParty.ContactDetails.Fax",
            "ThirdParty.ContactDetails.Telephone" ]
        },
        MonetaryThirdPartyContactStreet: {
          scope: "money",
          detail : {
            section   : "Money",
            detail :'monetary/thirdparty_contact',
            subSection: 'contactStreet'
          },
          summary: 'ThirdPartyContact',
          fields : [ "ThirdParty.StreetAddress.AddressLine1", "ThirdParty.StreetAddress.AddressLine2", "ThirdParty.StreetAddress.Suburb",
            "ThirdParty.StreetAddress.City", "ThirdParty.StreetAddress.Province", "ThirdParty.StreetAddress.PostalCode" ]
        },
        MonetaryThirdPartyContactPostal: {
          scope: "money",
          detail : {
            section   : "Money",
            detail :'monetary/thirdparty_contact',
            subSection: 'contactPostal'
          },
          summary: 'ThirdPartyContact',
          fields : [ "ThirdParty.PostalAddress.AddressLine1", "ThirdParty.PostalAddress.AddressLine2", "ThirdParty.PostalAddress.Suburb", "ThirdParty.PostalAddress.City", 
            "ThirdParty.PostalAddress.Province", "ThirdParty.PostalAddress.PostalCode" ]
        },
        ImportExport             : {
          scope  : "importexport",
          detail : {
            section   : "Money",
            detail :'monetary/importexport',
            subSection: 'importExport'
          },
          summary: 'ImportExport',
          fields: ["ImportControlNumber", "TransportDocumentNumber", "MRNNotOnIVS", "UCR", "PaymentCurrencyCode", "PaymentValue"]
        }
  
      }
    }
    return _export;
  });
  
  