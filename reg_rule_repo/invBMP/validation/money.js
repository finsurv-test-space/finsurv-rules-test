
          
define(function () {
  return function (predef) {
    var extTrans;
    with (predef) {

      extTrans = {
        ruleset: "Common Money Rules for invBMP form",
        scope: "money",
        validations: [
          {
            field: ["ThirdParty.ContactDetails.ContactName", "ThirdParty.ContactDetails.ContactSurname", 
            "ThirdParty.ContactDetails.Email"],
            rules: [
              failure("inv_cdm1","I??", "Third Party Contact details are mandatory", isEmpty.and(hasMoneyField("ThirdParty.Individual").or(hasMoneyField("ThirdParty.Entity"))))
            ]
          }
        ]
      };

    }
    return extTrans;
  }
});
