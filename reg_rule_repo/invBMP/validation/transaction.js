define(function () {
  //THIS IS the Transaciotn rules
  return function (predef) {
    var transaction;

    with (predef) {
      transaction = {
        ruleset: "Transaction Rules for invBMP form",
        scope: "transaction",
        validations: [
          {
            field: ["Resident.Individual.StreetAddress.Country", "Resident.Entity.StreetAddress.Country"],
            rules: [
              ignore('inv_rsac1'),
              ignore('inv_rsac2')
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.Mandate", "Resident.Entity.StreetAddress.Mandate"],
            rules: [
              ignore("inv_rsam1"),
              ignore("inv_rsam2"),
              ignore("inv_rsam3")
            ]
          },
          {
            field: "FeesIncluded",
            rules: [
              ignore("inv_fees1")
            ]
          },
          {
            field: "IsInvestecFCA",
            rules: [
              ignore("inv_iisa1"),
              ignore("inv_iisa2")
            ]
          },
          {
            field: "InvestecFCA",
            len: 13,
            rules: [
              ignore("inv_isa1"),
              ignore("inv_isa2"),
              ignore('inv_isa3'),
              ignore('inv_isa4'),
              ignore('inv_isa5')
            ]
          },
          {
            field: "RateConfirmation",
            rules: [
              ignore("inv_rate1")
            ]
          },
          {
            field: "NonResident.Individual.IsMutualParty",
            rules: [
              ignore("inv_nrimp")
            ]
          },
          {
            field: "NonResident.Entity.IsMutualParty",
            rules: [
              ignore("inv_nremp")
            ]
          },
          {
            field: ["NonResident.Individual.Address.AddressLine1","NonResident.Entity.Address.AddressLine1"],
            rules: [
              failure("inv_nral1","I??", "Address Line 1 is mandatory", isEmpty)
            ]
          },
          {
            field: ["NonResident.Individual.Address.Suburb", "NonResident.Entity.Address.Suburb"],
            rules: [
              failure("inv_nrsb","I??", "Suburb is mandatory", isEmpty)
            ]
          },
          {
            field: ["NonResident.Individual.Address.City","NonResident.Entity.Address.City"],
            rules: [
              failure("inv_nrc","I??", "City is mandatory", isEmpty)
            ]
          },
          {
            field: ["NonResident.Individual.Address.State", "NonResident.Entity.Address.State"],
            rules: [
              failure("inv_nrst","I??", "Province/State is mandatory", isEmpty)
            ]
          },
          {
            field: ["NonResident.Individual.Address.PostalCode", "NonResident.Entity.Address.PostalCode"],
            rules: [
              failure("inv_nrpc","I??", "Postal/Zip code is mandatory", isEmpty)
            ]
          },
        ]
      }
    };

    return transaction;
  }

});

