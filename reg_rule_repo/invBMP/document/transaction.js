define(function () {
  return function (predef) {
    var docs;
    with (predef) {
      docs = {
        ruleset: "Transaction Documents",
        scope: "transaction",
        validations: [
          {
            field: ["TrnReference"],
            rules: [
              document('dt-inv-rot', "Mandate", "Please provide a mandate for the transaction",
                notEmpty).onOutflow().onSection("AB")
           ]
          }
        ]
      };
    }
    return docs;
  }
});