/**
 * Created by petruspretorius on 09/12/2016.
 */
define(function () {
  return function (predef) {
    predef = Object.assign(predef, {
      isSameAsFirst : function (field){
        return function(context,value){
          if(context.currentMoneyInstance == 0) return false;
          var firstVal = context.getMoneyField(0,field);
          return firstVal == value;
        }          
      }
  
    })
    return predef;
  }
})