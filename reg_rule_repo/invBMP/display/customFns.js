define(function (require) {
    return function (app, config) {

      app.directive('scrollOnClick', ['$window', function($window) {
        return {
          restrict: 'A',
          link: function(scope, $elm) {
            $elm.on('click', function() {
              setTimeout(function(){

                $window.scrollTo(0,$elm[0].offsetTop);
              },300)
            });
          }
        }
      }]);

        var superInit = function () { };

        if (config.initializationFn) superInit = config.initializationFn;

        var _config = {
            initializationFn: function (model, scope) {
                superInit(model, scope);
                //custom scope functions go here...

            },

            useTabs: true,
            //noCss: true,
            clearHidden: false,
            clearHiddenOnErrorOnly: false,
            snapShotValidate: true
        }

        Object.assign(config, _config);






        return config;
    }
})