define(function() {
  return function(testBase) {
      with (testBase) {

        var test_cases = [

//================================================================================================================================================
//  # TRANSACTION (1/3)
//================================================================================================================================================
          //NonResident
          assertSuccess("m_nr1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: "Synthesis"}}),
          assertSuccess("m_nr1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: {}}}),
          assertFailure("m_nr1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {}}),


          //NonResident.Entity.CardMerchantName
          assertSuccess("m_nrle1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: {CardMerchantName: "My Card"}}}),
          assertSuccess("m_nrle1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: {CardMerchantName: {}}}}),
          assertFailure("m_nrle1", {ReportingQualifier: 'BOPCARD RESIDENT', NonResident: {Entity: {}}}),

          // //NonResident.Entity.LegalEntityName
          assertSuccess("m_nrlen1", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {EntityName: 'Synthesis'}}}),
          assertNoRule("m_nrlen1", {ReportingQualifier: "BOPCARD NON RESIDENT", NonResident: {Entity: {EntityName: 'Synthesis'}}}),
          assertSuccess("m_nrlen1", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {LegalName: {}}}}),
          // assertFailure("m_nrlen1", {ReportingQualifier: "BOPCUS", NonResident: {Entity: {LegalName: "Test"}}}),

          // assertSuccess("m_nrlen1", {ReportingQualifier: "BOPCUS", Resident: {Entity: {ExceptionName: {}}}}),
          // assertFailure("m_nrlen1", {ReportingQualifier: "BOPCUS", Resident: {Entity: {ExceptionName: ""}}}),

          // Additional.NonResidentData.Country
          // assertSuccess("m_anrdc1",{ReportingQualifier: "BOPCUS", NonResident: {Individual: {Address: {}}}}),
          // assertFailure("m_anrdc1",{ReportingQualifier: "BOPCUS", NonResident:{Individual: {Address:{Country: "South Africa"}}}}),

          //IndividualCustomer.IDNumber
          // assertSuccess("m_icid1",{ReportingQualifier: "BOPCUS", Resident: {Individual:{}}}),
          // assertFailure("m_icid1",{ReportingQualifier: "BOPCUS", Resident: {Individual:{IDNumber: {}}}}),
          // assertFailure("m_icid1",{ReportingQualifier: "BOPCUS", Resident: {Individual:{IDNumber: "9202265116080"}}}),

          // assertSuccess("m_icid1",{ReportingQualifier: "BOPCARD RESIDENT", Resident: {Individual:{}}}),
          // assertFailure("m_icid1",{ReportingQualifier: "BOPCARD RESIDENT", Resident: {Individual:{IDNumber: {}}}}),
          // assertFailure("m_icid1",{ReportingQualifier: "BOPCARD RESIDENT", Resident: {Individual:{IDNumber: "9202265116080"}}}),

          //AdditionalCustomerData.CustomsClientNumber
          // assertSuccess('m_ccn1:1', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: '1234'}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '101', CategorySubCode: '10'}]
          // }),
          // assertFailure('m_ccn1:1', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {CustomsClientNumber: ''}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '103', CategorySubCode: '10'}
          //   ]
          // }),
          // assertFailure('m_ccn1:1', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '103', CategorySubCode: '10'}
          //   ]
          // }),
          // assertNoRule('m_ccn1:1', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {CustomsClientNumber: '1234'}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'}
          //   ]
          // }),

          // assertSuccess('m_ccn1:2', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: '1234'}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '101', CategorySubCode: '10'}]
          // }),
          // assertFailure('m_ccn1:2', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {CustomsClientNumber: ''}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '103', CategorySubCode: '10'}
          //   ]
          // }),
          // assertFailure('m_ccn1:2', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'},
          //     {CategoryCode: '103', CategorySubCode: '10'}
          //   ]
          // }),
          // assertNoRule('m_ccn1:2', {
          //   ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {CustomsClientNumber: '1234'}},
          //   MonetaryAmount: [
          //     {CategoryCode: '101', CategorySubCode: '11'}
          //   ]
          // }),

//================================================================================================================================================
//  # MONEY (2/3)
//================================================================================================================================================
          // Must contain a sequential SequenceNumber entries 
          assertSuccess('m_mseq1', {
            ReportingQualifier: 'BOPCARD',
            MonetaryAmount: [{SequenceNumber: "1"}, {SequenceNumber: "2"}]
          }),
          assertFailure('m_mseq1', {
            ReportingQualifier: 'BOPCARD',
            MonetaryAmount: [{SequenceNumber: "2"}]
          }),

          //If DomesticCurrencyCode is MWK and the ForeignCurrencyCode is ZAR, the DomesticValue must be equal to the ForeignValue
          // assertSuccess("m_nvl1", {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   LocalCurrency: 'MWK',
          //   ForeignCurrencyCode:'ZAR',
          //   MonetaryAmount: [{
          //     DomesticValue:0.00,
          //     ForeignValue: 0.00}]
          // }),
          // assertSuccess("m_nvl1", {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   LocalCurrency: 'MWK',
          //   ForeignCurrencyCode:'US',
          //   MonetaryAmount: [{
          //     DomesticValue:0.20,
          //     ForeignValue: 0.00}]
          // }),

          // //Must be completed
          assertFailure('m_ncc1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{LocalCurrency: 0.00}]
          }),
          assertFailure('m_ncc1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{}]
          }),
          assertFailure('m_ncc1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{LocalCurrency: {}}]
          }),
          assertFailure('m_ncc1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            MonetaryAmount: [{LocalCurrency: ''}]
          }),

          //Invalid SWIFT currency code.
          assertSuccess("m_ncc2", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{LocalCurrency: 'MWK'}]}),
         // assertFailure("m_ncc2:1", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{LocalCurrency: 'MWK'}]}),
          assertSuccess("m_ncc2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{LocalCurrency: 'MWK'}]}),
         // assertFailure("m_ncc2:2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{LocalCurrency: 'MWK'}]}),
          
          //SWIFT currecy code must only be MWK
          assertSuccess("m_ncc3", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{LocalCurrency: 'MWK'}]}),
          //assertFailure("m_ncc3:1", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{LocalCurrency: 'ZAR'}]}),
          assertSuccess("m_ncc3", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{LocalCurrency: 'MWK'}]}),
          //assertFailure("m_ncc3:2", {ReportingQualifier: 'BOPCARD RESIDENT', MonetaryAmount: [{LocalCurrency: 'ZAR'}]}),
          
          //If DomesticCurrencyCode is MWK and the ForeignCurrency is ZAR, the DomesticValue must be equal to the ForeignValue.
          assertSuccess("m_ncc4", {
            ReportingQualifier: 'BOPCARD RESIDENT',
            LocalCurrency: 'MWK',
            ForeignCurrencyCode:'ZAR',
            MonetaryAmount: [{
              DomesticValue:0.00,
              ForeignValue: 0.00}]
          }),
          // assertFailure("m_ncc4", {
          //   ReportingQualifier: 'BOPCARD RESIDENT',
          //   LocalCurrency: 'MWK',
          //   ForeignCurrencyCode:'ZAR',
          //   MonetaryAmount: [{
          //     DomesticValue:0.20,
          //     ForeignValue: 0.00}]
          // }),

          //Must not be completed.
          assertSuccess("m_ncc5", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LocalCurrency: ''}]}),
          assertSuccess("m_ncc5", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{}]}),
          //assertFailure("m_ncc5", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LocalCurrency: 'MWK'}]}),
          

          //If the BoPCategory 801, or 802, or 803, or 804, or 810 or 815 or 816 or 817 or 818 or 819 is used. must be completed.
          assertSuccess("m_mlrn1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '801', LoanRefNumber: 'XYZ'}]
          }),
          assertFailure("m_mlrn1", {
            ReportingQualifier: 'BOPCUS', 
            MonetaryAmount: [{CategoryCode: '801'}]}),

          //For any other BoPCategory other than 801, or 802, or 803 or 804 or 810 or 815 or 816 or 817 or 818 or 819 or 106 or 309, it must not be completed
          assertNoRule("m_mlrn3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '801', LoanRefNumber: ''}]
          }),
          assertFailure("m_mlrn3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '309',CategorySubCode: '08', LoanRefNumber: 'XYZ'}]
          }),
          assertNoRule("m_mlrn3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '801', LoanRefNumber: 'XYZ'}]
          }),

         //If the BoPCategory 309/01 to 309/07 is used, must be completed reflecting the percentage interest paid.
          assertSuccess("m_mlir1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '309',CategorySubCode: '01', LoanInterestRate: '0.0'}]
          }),
          // assertSuccess("m_mlir1", {
          //   ReportingQualifier: 'BOPCUS',
          //   MonetaryAmount: [{CategoryCode: '309',CategorySubCode: '08'}]
          // }),
          assertFailure("m_mlir1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '309',CategorySubCode: '01'}]
          }),
          assertNoRule("m_mlir1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{CategoryCode: '309',CategorySubCode: '08', LoanInterestRate: '0.0'}]
          }),

          //If the BoPCategory 309/01 to 309/07 must be completed in the format 0.00
          assertSuccess("m_mlir2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '01',
              LoanInterestRate: '5.12'
            }]
          }),
          assertFailure("m_mlir2", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '01',
              LoanInterestRate: ''
            }]
          }),
          assertFailure("m_mlir3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '01',
              LoanInterestRate: '2000.10'
            }]
          }),
          assertFailure("m_mlir3", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{
              CategoryCode: '309',
              CategorySubCode: '01',
              LoanInterestRate: 'BASE PLUS 5.12'
            }]
          }),

          // //SWIFT country code may not be MW
          assertSuccess("m_mlc1", {ReportingQualifier: 'BOPCUS', MonetaryAmount: [{LocationCountry: 'US'}]}),
          assertFailure("m_mlc1", {
            ReportingQualifier: 'BOPCUS',
            MonetaryAmount: [{LocationCountry: 'MW'}]
          }),
          assertSuccess("m_mlc1", {ReportingQualifier: 'BOPCARD NON RESIDENT', MonetaryAmount: [{LocationCountry: 'US'}]}),
          assertFailure("m_mlc1", {
            ReportingQualifier: 'BOPCARD NON RESIDENT',
            MonetaryAmount: [{LocationCountry: 'MW'}]
          }),

//================================================================================================================================================
//  # IMPORT/EXPORT (3/3)
//================================================================================================================================================
          // ImportControlNumber
          //Must be completed if the Flow is OUT and the BoPCategory is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106.
          assertSuccess("m_icn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '04', ImportExport: [{ImportControlNumber: 'XYZ'}]}]
          }),
          assertFailure("m_icn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '04', ImportExport: [{ImportControlNumber: ''}]}]
          }),

          // TotalForeignValue
          assertSuccess("tfv1", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'MWK', TotalForeignValue: '101.01',
            MonetaryAmount: [{DomesticValue: '50.00'}, {DomesticValue: '51.01'}]
          }),
          assertFailure("tfv1", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'MWK', TotalForeignValue: '101.01',
            MonetaryAmount: [{DomesticValue: '50.00'}, {DomesticValue: '49.99'}]
          }),
          assertSuccess("tfv1", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'MWK',
            MonetaryAmount: [{DomesticValue: '50.00'}, {DomesticValue: '49.99'}]
          }),

          assertSuccess("tfv2", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD', TotalForeignValue: '101.01',
            MonetaryAmount: [{ForeignValue: '50.00'}, {ForeignValue: '51.01'}]
          }),
          assertFailure("tfv2", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD', TotalForeignValue: '101.01',
            MonetaryAmount: [{ForeignValue: '50.00'}, {ForeignValue: '49.99'}]
          }),
          assertSuccess("tfv2", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD',
            MonetaryAmount: [{ForeignValue: '50.00'}, {ForeignValue: '49.99'}]
          }),

          assertSuccess("tfv3", {
            ReportingQualifier: 'BOPCARD NON RESIDENT', FlowCurrency: 'MWK', TotalForeignValue: '101.01',
            MonetaryAmount: [
              {ForeignCardHoldersPurchasesDomesticValue: '40.00', ForeignCardHoldersCashWithdrawalsDomesticValue: '10.00'},
              {ForeignCardHoldersCashWithdrawalsDomesticValue: '51.01'}
            ]
          }),
          assertFailure("tfv3", {
            ReportingQualifier: 'BOPCARD NON RESIDENT', FlowCurrency: 'MWK', TotalForeignValue: '101.01',
            MonetaryAmount: [{ForeignCardHoldersPurchasesDomesticValue: '50.00'}, {ForeignCardHoldersPurchasesDomesticValue: '49.99'}]
          }),
          assertSuccess("tfv3", {
            ReportingQualifier: 'BOPCARD NON RESIDENT', FlowCurrency: 'MWK',
            MonetaryAmount: [{ForeignCardHoldersPurchasesDomesticValue: '50.00'}, {ForeignCardHoldersPurchasesDomesticValue: '49.99'}]
          }),
          //The TotalForeignValue must be completed and must be greater than 0.00
          assertSuccess("tfv4", {
            ReportingQualifier: 'BOPCUS', TotalForeignValue: '101.01'
          }),
          assertFailure("tfv4", {
            ReportingQualifier: 'BOPCUS', TotalForeignValue: '0.00'
          }),
          assertFailure("tfv4", {
            ReportingQualifier: 'BOPCUS'
          }),
          assertNoRule("tfv4", {
            ReportingQualifier: 'BOPCARD NON RESIDENT', TotalForeignValue: '0.00'
          }),
        ]
      }
    return testBase;
  }
})
