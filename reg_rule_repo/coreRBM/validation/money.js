define(function () {
    return function (predef) {
      var stdMoney;
      with (predef) {
  
        stdMoney = {
          ruleset: "Standard RBM Money Rules",
          scope: "money",
          validations: [
            {
              field: "",
              rules: [
                // field: "SequenceNumber",
                message('mseq1','443', null),
                // field: "MoneyTransferAgentIndicator",
                message('mta1','206', 'Must contain one of the following values: MONEYGRAM, MUKURU, WESTERNUNION, RAPIDTRANSFER, ADB, ,HELLO PAISA, EXCHANGE 4FREE, RIA FINANCIAL SERVICES, AMERIT OPERATIONS, AIRTEL CROSS BORDER TRANSFER, INTERNATIONAL FINANCIAL SYSTEM, EXPRESS LINK, SMALL WORLD FINANCIAL SERVICES GROUP'),
                ignore('mta2'),
                ignore('mta3'),
                message('mta4','206', null),
                message('mta5','310', null),
                // field: "{{LocalValue}}",
                message('mrv2','311', null),
                ignore('mrv3'),
                message('mrv4','313', 'If a ForeignValue is completed and a DomesticValue is reported, the reported DomesticValue must be within a 15% variance with the applicable mid-rate calculated by the RBM.'),
                message('mrv5','319', null),
                ignore('mrv6'),
                ignore('mrv7'),
                //message('mrv7','422', 'If BoPCategory 107 is used, the value must not exceed USD5000 (To be cleared with MRA by RBM)'),
                message('mrv8','304', null),
                ignore('mrv9'),
                // field: "ForeignValue",
                message('mfv1','308', 'At least one of DomesticValue or ForeignValue must be present'),
                message('mfv2','315', null),
                message('mfv3','314', null),
                message('mfv4','320', null),
                message('mfv5','304', null),
                // field: "CategoryCode",
                message('mcc1','323', null),
                message('mcc2','324', null),
                message('mcc3','325', null),
                ignore('mcc5'),
                ignore('mcc6'),
                ignore('mcc7'),
                ignore('mcc8'),
                ignore('mcc9'),
                // field: "CategorySubCode",
                message('msc1','369', null),
                message('msc2','327', null),
                // field: "SWIFTDetails",
                // field: "StrateRefNumber",
                ignore('msrn1'),
                ignore('msrn2'),
                ignore('msrn3'),
                ignore('msrn4'),
                // field: "LoanRefNumber",
                ignore('mlrn1'),
                ignore('mlrn2'),
                ignore('mlrn3'),
                ignore('mlrn4'),
                ignore('mlrn5'),
                ignore('mlrn6'),
                ignore('mlrn7'),
                ignore('mlrn8'),
                ignore('mlrn9'),
                ignore('mlrn11'),
                message('mlrn10','331', null),
                // message('mlrn11','331', 'For any other BoPCategory and SubBoPCategory other than 801, or 802, or 803 or 804 or 810 or 815 or 816 or 817 or 818 or 819 or 106 or 309/01 or 309/02 or 309003 or 309/04 or 309/05 or 309/06 or 309/07, it must not be completed'),
                // field: "LoanTenor",
                ignore('mlt1'),
                ignore('mlt2'),
                message('mlt3','332', null),
                // field: "LoanInterestRate",
                ignore('mlir1'),
                message('mlir2','334', null),
                ignore('mlir3'),
                ignore('mlir4'),
                message('mlir7','334', 'Except for BoPCategory and SubBoPCategory 309/01, 309/02, 309/03, 309/04, 309/05, 309/06, 309/07 it must not be completed'),
                ignore('mlir8'),
                // field: "{{Regulator}}Auth.RulingsSection",
                ignore('mars1'),
                //message('mars2','336', null),
                ignore('mars3'),
                // field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
                ignore('maian1'),
                // message('maian2','336', null),
                // field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
                ignore('maiad1'),
                // message('maiad2','336', null),
                ignore('maiad3'),
                // field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
                ignore('masan1'),
                ignore('masan2'),
                //   message('masan3','336', null),
                ignore('masan4'),
                // field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
                ignore('masar1'),
                // message('masar3','336', null),
                // field: "CannotCategorize",
                message('mexc1','340', 'If BoPCategory 830 is used, must be completed'),
                message('mexc2','343', null),
                // field: "AdHocRequirement.Subject",
                ignore('madhs1'),
                message('madhs2','344', null),
                ignore('madhs3'),
                ignore('madhs4'),
                ignore('madhs5'),
                ignore('madhs6'),
                ignore('madhs7'),
                ignore('madhs8'),
                ignore('madhs9'),
                ignore('madhs10'),
                ignore('madhs11'),
                ignore('madhs12'),
                ignore('madhs13'),
                ignore('madhs14'),
                ignore('madhs15'),
                ignore('madhs16'), 
                ignore('madhs17'),
                // field: "AdHocRequirement.Description",
                ignore('madhd1'),
                ignore('madhd2'),
                message('madhd3','347', null),
                // field: "LocationCountry",
                message('mlc1','348', null),
                message('mlc2','229', null),
                message('mlc3','267', 'SWIFT country code must not be MW'),
                ignore('mlc4'),
                message('mlc5','350', null),
                ignore('mlc6'),
                ignore('mlc8'),
                // field: "ReversalTrnRefNumber",
                message('mrtrn1','351', null),
                message('mrtrn2','349', null),
                message('mrtrn3','263', null),
                message('mrtrn4','217', null),
                message('mrtrn5','351', null),
                // field: "ReversalTrnSeqNumber",
                message('mrtrs1','351', null),
                message('mrtrs2','352', null),
                message('mrtrs3','263', null),
                // field: "BOPDIRTrnReference",
                message('mdirtr1','354', null),
                // field: "BOPDIR{{DealerPrefix}}Code",
                message('mdircd1','356', null),
                // field: "ThirdParty.Individual",
                // field: "ThirdParty.Individual.Surname",
                ignore('mtpisn1'),
                message('mtpisn2','357', null),
                message('mtpisn3','357', null),
                message('mtpisn4','357', null),
                message('mtpisn5','358', null),
                message('mtpisn6','357', null),
                message('mtpisn7','359', null),
                ignore('mtpisn8'),
                ignore('mtpisn9'),
                ignore('mtpisn10'),
                // field: "ThirdParty.Individual.Name",
                message('mtpinm1','360', null),
                message('mtpinm2','360', null),
                message('mtpinm3','360', null),
                message('mtpinm4','358', null),
                message('mtpinm5','361', null),
                message('mtpinm6','360', null),
                ignore('mtpinm7'),
                // field: "ThirdParty.Individual.Gender",
                message('mtpig1','362', null),
                message('mtpig2','363', null),
                message('mtpig3','362', null),
                message('mtpig4','362', null),
                message('mtpig5','364', null),
                message('mtpig6','362', null),
                ignore('mtpig7'),
                ignore('mtpig8'),
                // field: "ThirdParty.Individual.IDNumber",
                message('mtpiid1','365', 'If IndividualThirdPartySurname contains a value, either IndividualThirdPartyIDNumber or IndividualThirdPartyTempResPermit Number must be completed. IDnumber refers to a passport number, voter registration number or drivers lisence number.'),
                ignore('mtpiid2'),
                ignore('mtpiid3'),
                message('mtpiid4','366', null),
                message('mtpiid5','365', null),
                message('mtpiid6','446', null),
                ignore('mtpiid7'),
                ignore('mtpiid8'),
                ignore('mtpiid9'),
                // field: "ThirdParty.Individual.DateOfBirth",
                message('mtpibd1','367', null),
                message('mtpibd2','367', null),
                message('mtpibd3','367', null),
                message('mtpibd4','368', null),
                message('mtpibd5','367', null),
                ignore('mtpibd7'),
                ignore('mtpibd8'),
                // field: "ThirdParty.Individual.TempResPermitNumber",
                message('mtpitp1','365', null),
                ignore('mtpitp2'),
                message('mtpitp3','365', null),
                message('mtpitp4','369', null),
                message('mtpitp5','447', null),
                ignore('mtpitp6'),
                message('mtpitp7','261', 'If IndividualThirdPartySurname contains a value and the BoPCategory is 25000 and the Flow is OUT, either IndividualThirdPartyTempResPermit Number or IndividualThirdPartyPassportNumber must be completed. (This rule caters for non-residents, but excluding contract workers, traveling o.b.o. of a Malawian entity)'),
                // field: "ThirdParty.Individual.PassportNumber",
                message('mtpipn1','370', null),
                message('mtpipn2','370', null),
                message('mtpipn3','371', null),
                message('mtpipn4','448', null),
                ignore('mtpipn5'),
                ignore('mtpipn6'),
                // field: "ThirdParty.Individual.PassportCountry",
                message('mtpipc1','372', null),
                message('mtpipc2','373', null),
                message('mtpipc3','229', null),
                ignore('mtpipc4'),
                // field: "ThirdParty.Entity.Name",
                message('mtpenm2','374', null),
                ignore('mtpenm3'),
                // field: "ThirdParty.Entity.RegistrationNumber",
                message('mtpern1','375', null),
                message('mtpern2','449', null),
                ignore('mtpern3'),
                // field: "ThirdParty.CustomsClientNumber",
                message('mtpccn3','291', null),
                ignore('mtpccn4'),
                ignore('mtpccn5'),
                // field: "ThirdParty.TaxNumber",
                ignore('mtptx1'),
                message('mtptx2','376', null),
                message('mtptx3','450', null),
                ignore('mtptx4'),
                ignore('mtptx5'),
                // field: "ThirdParty.VATNumber",
                message('mtpvn1','452', null),
                ignore('mtpvn2'),
                ignore('mtpvn3'),
                // field: "ThirdParty.StreetAddress.AddressLine1",
                ignore('mtpsal11'),
                message('mtpsal12','377', null),
                ignore('mtpsal13'),
                ignore('mtpsal14'),
                // field: "ThirdParty.StreetAddress.AddressLine2",
                message('mtpsal21','378', null),
                ignore('mtpsal22'),
                // field: "ThirdParty.StreetAddress.Suburb",
                ignore('mtpsas1'),
                message('mtpsas2','379', null),
                ignore('mtpsas3'),
                // field: "ThirdParty.StreetAddress.City",
                ignore('mtpsac1'),
                message('mtpsac2','380', null),
                ignore('mtpsac3'),
                // field: "ThirdParty.StreetAddress.Province",
                ignore('mtpsap1'),
                message('mtpsap2','381', null),
                ignore('mtpsap3'),
                ignore('mtpsap4'),
                // field: "ThirdParty.StreetAddress.PostalCode",
                ignore('mtpsaz1'),
                message('mtpsaz2','382', null),
                ignore('mtpsaz3'),
                ignore('mtpsaz4'),
                // field: "ThirdParty.PostalAddress.AddressLine1",
                ignore('mtppal11'),
                message('mtppal12','383', null),
                ignore('mtppal13'),
                ignore('mtppal14'),
                // field: "ThirdParty.PostalAddress.AddressLine2",
                message('mtppal21','384', null),
                ignore('mtppal22'),
                // field: "ThirdParty.PostalAddress.Suburb",
                ignore('mtppas1'),
                message('mtppas2','385', null),
                ignore('mtppas3'),
                // field: "ThirdParty.PostalAddress.City",
                ignore('mtppac1'),
                message('mtppac2','386', null),
                ignore('mtppac3'),
                // field: "ThirdParty.PostalAddress.Province",
                ignore('mtppap1'),
                message('mtppap2','387', null),
                ignore('mtppap3'),
                ignore('mtppap4'),
                // field: "ThirdParty.PostalAddress.PostalCode",
                ignore('mtppaz1'),
                ignore('mtppaz3'),
                ignore('mtppaz4'),
                // field: "ThirdParty.ContactDetails.ContactSurname",
                ignore('mtpcds1'),
                message('mtpcds2','388', null),
                ignore('mtpcds3'),
                // field: "ThirdParty.ContactDetails.ContactName",
                ignore('mtpcdn1'),
                message('mtpcdn2','389', null),
                ignore('mtpcdn3'),
                // field: "ThirdParty.ContactDetails.Email",
                ignore('mtpcde1'),
                message('mtpcde2','390', 'May not be completed'),
                ignore('mtpcde3'),
                // field: "ThirdParty.ContactDetails.Fax",
                ignore('mtpcdf1'),
                message('mtpcdf2','390', null),
                ignore('mtpcdf3'),
                // field: "ThirdParty.ContactDetails.Telephone",
                ignore('mtpcdt1'),
                message('mtpcdt2','390', null),
                ignore('mtpcdt3'),
                // field: "CardChargeBack",
                message('mcrdcb1','391', null),
                message('mcrdcb2','392', 'Must be blank or contain a value "Y" or "N"'),
                // field: "CardIndicator",
                message('mcrdci1','393', null),
                message('mcrdci2','394', 'Must contain AMEX or DINERS or ELECTRON or MAESTRO or MASTER or VISA or BOCEXPRESS (To be confirmed by RBM)'),
                // field: "ElectronicCommerceIndicator",
                ignore('mcrdec1'),
                ignore('mcrdec2'),
                ignore('mcrdec3'),
                // field: "POSEntryMode",
                ignore('mcrdem1'),
                ignore('mcrdem2'),
                ignore('mcrdem3'),
                ignore('mcrdem4'),
                // field: "CardFraudulentTransactionIndicator",
                message('mcrdft1','396', null),
                ignore('mcrdft2'),
                // field: "ForeignCardHoldersPurchases{{LocalValue}}",
                message('mcrdfp1','397', null),
                message('mcrdfp2','398', null),
                message('mcrdfp3','311', null),
                // field: "ForeignCardHoldersCashWithdrawals{{LocalValue}}",
                message('mcrdfw1','399', null),
                message('mcrdfw2','400', null),
                message('mcrdfw3','311', null),
                // field: "ImportExport",
                message('mtie1','401', 'If the Flow is IN and BoPCategory and SubBoPCategory is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, the ImportExportData Element must be completed.'),
                message('mtie2','402', null),
                message('mtie3','434', 'Total PaymentValue of all the SubSequences may not exceed a 1% variance with the MWKValue or ForeignValue (NOTE: PaymentValue of a currency must be consistent in respect of all the SubSequences i.e. may not be a USD value in SubSequence 1 and a MWK value in SubSequrence 2.)'),
                message('mtie4','401', 'If the Flow is OUT and BoPCategory and SubBoPCategory is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, ImportExportData Element must be completed.'),
                message('mtie5','402', null),
                message('mtie6','404', null),
                message('mtie6','405', null),
              ]
            },
//================================================================================================================================================
// CoreRBM specific money
//================================================================================================================================================
            {
                field: "SequenceNumber",
                rules: [
                  failure("m_mseq1", 443, "Must contain a sequential number that must start with the value 1.",
                   notValidSequenceNumbers)
                  ]
              },
              {
                field: "{{LocalValue}}",
                rules: [
                  failure("m_nvl1", 322, "If DomesticCurrencyCode is MWK and the ForeignCurrencyCode is the same, the DomesticValue must be equal to ForeignValue.",
                    notEmpty.and(hasMoneyFieldValue("DomesticCurrencyCode","MWK")).and(hasTransactionFieldValue('ForeignCurrencyCode','MWK'))).onSection("E"),
                  failure("m_nvl4", 312, "Must not equal ForeignValue",
                    notEmpty.and(equalsMoneyField("ForeignValue").and(notCurrencyIn(["MWK"])))).onSection("A"),
                  ]
              },
              {
                field: "DomesticCurrencyCode",
                rules: [
                  failure("m_ncc1", 337, "Must be completed.",
                    isEmpty).onSection("E"),
                  failure("m_ncc2", 318, "Invalid SWIFT currency code.",
                    notEmpty.and(hasInvalidSWIFTCurrency)).onSection("AE"),
                  failure("m_ncc3", 345, "SWIFT currency code must only be MWK.",
                    notEmpty.and(notValue("MWK"))).onSection("AE"),
                  failure("m_ncc4", 322, "If DomesticCurrencyCode is MWK and the ForeignCurrency is MWK, the DomesticValue must be equal to the ForeignValue.",
                    notEmpty.and(hasValue('MWK')).and(hasTransactionFieldValue('ForeignCurrencyCode','MWK'))).onSection("E"),
                  failure("m_ncc5", 322, "Must not be completed.",
                    notEmpty).onSection("F"),  
                  ]
              },
              {
                field: "LoanRefNumber",
                rules: [
                  failure("m_mlrn1", 329, "If cat 801, or 802, or 803 or 804 or 810 or 815 or 816 or 817 or 818 or 819 is used, must be completed.",
                    isEmpty).onCategory(['801','802','803','804','810','815','816','817','818','819']).onSection("A"),
                  failure("m_mlrn2", 329, "If BoPCategory and SubBopCategory is 106 or 309/01 or 309/02 or 309/03 or 309/04 or 309/05 or 309/06 or 309/07, it must be completed.",
                    isEmpty).onCategory(['106','309']).notOnCategory(['309/08']).onSection("A"),
                  failure("m_mlrn3", 331, "For any other BoPCategory and SubBoPCategory other than 801, or 802, or 803 or 804 or 810 or 815 or 816 or 817 or 818 or 819 or 106 or 309/01 or 309/02 or 309003 or 309/04 or 309/05 or 309/06 or 309/07, it must not be completed.",
                    notEmpty).notOnCategory(['801','802','803','804','810','815','816','817','818','819','106','309/01','309/02','309/03','309/04','309/05','309/06','309/07']).onSection("A")
                        ]
              },
              {
                field: "LoanInterestRate",
                rules: [
                  failure("m_mlir1", 333, "If the BoPCategory 309/01 to 309/07 is used, must be completed reflecting the percentage interest paid.",
                    isEmpty).onCategory(['309']).notOnCategory(['309/08']).onSection("A"),
                  failure("m_mlir2", 335, "If the BoPCategory 309/01 to 309/07 must be completed in the format 0.00",
                    isEmpty.and(notPattern("^\\d{1,3}\\.\\d{2}?$"))).onCategory(['309']).notOnCategory(['309/08']).onSection("A"),
                  failure("m_mlir3", 335, "If the BoPCategory 30901 to 30907 must be completed in the format 0.00",
                    notEmpty.and(notPattern("^\\d{1,3}\\.\\d{2}?$"))).onCategory(['309']).notOnCategory(['309/08']).onSection("A")
                  ]
              },
              {
                field: "LocationCountry",
                rules: [
                  failure("m_mlc1", 367, "SWIFT country code must not be 'MW'.",
                    notEmpty.and(hasValue("MW"))).onSection("AF"),
                  ]
              },
              {
                field: "ThirdParty.CustomsClientNumber",
                minLen: 2,
                maxLen: 15,
                rules: [
                  failure("mtpccn1", 292, "Invalid CustomsClientNumber",
                    notEmpty.and(notPattern(/^\d{2,15}$/))).onSection("A"),
                  failure("mtpccn2", 292, "If it contains a value, the CustomsClientNumber must contain a valid Customs Client number",
                    notEmpty.and(notPattern(/^\d{2,15}$/))).onSection("A")
                ]
              },
//              {
//                field: "ThirdParty.Individual.IDNumber",
//                minLen: 2,
//                maxLen: 20,
//                rules: [
//                   failure("mtpiid3", 273, "The format must be one of the following: Passport Number: MWnnnnnn where n is a number from 0 to 9 Drivers lisence:CCYYMMDDnnnnnn where n is a number from 0 to 9 Voters roll registration: nnnnnnnn where n is a number from 0 to 9",
//                        notEmpty.and(notPattern(/(^MW\d{6}$)|(^(19|20|21)\d{2}(0\d|10|11|12)(0[1-9]|1\d|2\d|3[01])\d{6}$)|(^\d{8}$)/))).onSection("AE")
//                ]
//              },
              {
                field: "SWIFTDetails",
                minLen: 2,
                maxLen: 100,
                rules: [
                  failure("mswd1", 269, "Must not be completed",
                    notEmpty).onSection("AEF")
                ]
              },
              {
                field: "{{Regulator}}Auth.RulingsSection",
                minLen: 2,
                maxLen: 30,
                rules: [
                 failure("mars2", 336, "Must not be completed",
                    notEmpty).onSection("AEF")
                ]
              },
               {
                field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
                minLen: 2,
                maxLen: 15,
                rules: [
                 failure("maian2", 336, "Must not be completed",
                    notEmpty).onSection("AEF")
                ]
               },
               {
                 field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
                 rules: [

                   failure("maiad2", 336, "Must not be completed",
                     notEmpty).onSection("AEF")
                 ]
               },
               {
                 field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
                 minLen: 2,
                 maxLen: 15,
                 rules: [
                   failure("masan3", 336, "Must not be completed",
                     notEmpty).onSection("AEF")
                   ]
               },
               {
                 field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
                 minLen: 2,
                 maxLen: 15,
                 rules: [
                   failure("masar3", 336, "Must not be completed",
                     notEmpty).onSection("AEF")
                 ]
               },
               {
                 field: "ElectronicCommerceIndicator",
                 minLen: 1,
                 maxLen: 2,
                 rules: [
                   failure("m_mcrdec2", 395, "Must not be completed",
                     notEmpty).onSection("AEF"),
                 ]
               },
               {
                 field: "POSEntryMode",
                 len: 2,
                 rules: [
                   failure("m_mcrdem1", 395, "May not be completed",
                     notEmpty).onSection("AEF")
                 ]
               },
        ]        
        };
    }
    return stdMoney;
 }
});