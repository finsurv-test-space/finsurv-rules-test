define(function () {
    return function (predef) {
      var stdImportExport;
      with (predef) {
        stdImportExport = {
          ruleset: "RBM Import/Export Rules",
          scope: "importexport",       
          validations: [
            {
              field: "",
              rules: [
                 // field: "ImportControlNumber",
                ignore('ieicn1'),
                message('ieicn2','407', null),
                ignore('ieicn3'),
                // message('ieicn3','410', 'If the Flow is OUT and the BoPCategory is 101/01 to 101/10, the first 3 characters must be INV followed by the invoice number. The minimum total number of characters must be 4.(To be discussed with MRA by RBM)'),
                ignore('ieicn4'),
                //message('ieicn4','409', 'If the Flow is OUT and the BoPCategory and SubBopCategory is 103/01 to 103/10 or 105 or 106, the first 3 characters must be a valid customs office code.(To be discussed with MRA by RBM)'),
                message('ieicn5','217', null),
                message('ieicn6','406', null),
                // field: "TransportDocumentNumber",
                ignore('ietdn1'),
                message('ietdn2','413', null),
                message('ietdn3','412', null),
                ignore('ietdn4'),
                // field: "UCR",
                message('ieucr1','414', 'If UCR contains a value and the Flow is IN, the minimum characters is 12 but not exceeding 35 characters and is in the following format: nMW12345678a...35a where n = last digit of the year NA = Fixed character 12345678 = Valid Customs Client Number a = unique alpha numeric consignment number.(To be discussed with MRA by RBM)'),
                message('ieucr2','415', 'Must be completed if the Flow is IN and the BoPCategory and SubBopCategory is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106. and the value of export exceeds USD5000. (To be discussed with MRA by RBM)'),
                message('ieucr3, ieucr4','416', null),
                // field: "PaymentCurrencyCode",
                message('iepcc1','435', null),
                message('iepcc2','318', null),
                message('iepcc2','436', 'PaymentCurrencyCode of all SubSequence is not MWK or does not match ForeignCurrencyCode under MonetaryDetails element. (NOTE: CurrencyCode must be consistent in respect of all the SubSequences i.e. may not be a USD in SubSequence 1 and a MWK in SubSequrence 2.)'),
                // field: "PaymentValue",
                message('iepv1','417', null),
                // field: "MRNNotOnIVS",
                message('iemrn1','203', 'The value must only be Y or N'),    
              ]
            },
//================================================================================================================================================
// CoreRBM specific import/export
//================================================================================================================================================
            {
              field: "ImportControlNumber",
              rules: [
                failure("m_icn1", 406, "Must be completed if the Flow is OUT and the BoPCategory is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106.",
                  isEmpty).onOutflow().onCategory(['101','103','105','106']).notOnCategory(['101/11','101/12','101/13','101/14','101/15','101/16','101/17','101/18','101/19','101/20','101/21','101/22','101/23','103/11','103/12','103/13','103/14','103/15','103/16','103/17','103/18','103/19','103/20','103/21','103/22','103/23']).onSection("A"),
                warning("mw_ieicn3", 499, "If the Flow is OUT and the category is 101/01 to 101/10, the first 3 characters must be INV followed by the invoice number. The minimum total number of characters must be 4",
                  notPattern(/^INV.+$/)).onOutflow().onSection("ABG").onCategory("101").notOnCategory(["101/11",'101/12','101/13','101/14','101/15','101/16','101/17','101/18','101/19','101/20','101/21','101/22','101/23']),
                // failure('m_ieicn4',410, 'If the Flow is OUT and the BoPCategory is 103/01 to 103/10 or 105 or 106,, the format is as follows: AAACCYYMMDD0000000 where AAA is a valid customs office code in alpha format; CC is the century of import, YY is the year of import, MM is the month of import, DD is the day of import, and 0000000 is the 7 digit unique bill of entry number allocated by Inland Revenue as part of the MRN.',
                //   notEmpty.and(notPattern(/^(\w){3}(19|20|21)\d{2}(0\d|10|11|12)(0[1-9]|1\d|2\d|3[01])(\d){7}$/))).onOutflow().onSection("A").notOnCategory("103/11").onCategory(["103", "105", "106"]),
                ]
            },
            {
              field: "TransportDocumentNumber",
              rules: [
                failure("m_tdn1", 412, "Must be completed if the Flow is OUT and the BoPCategory and SubBopCategory is 103/01 to 103/10.(To be discussed with MRN by RBM)",
                  isEmpty).onOutflow().onCategory(['103']).notOnCategory(['103/11','103/12','103/13','103/14','103/15','103/16','103/17','103/18','103/19','103/20','103/21','103/22','103/23']).onSection("A"),
                ]
            },
        ]
    };
  }

  return stdImportExport;
}
});
