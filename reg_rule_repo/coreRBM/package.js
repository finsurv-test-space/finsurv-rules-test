define({
  engine: { major: "1", minor: "0" },
  dependsOn: "coreSADC",
  features: ["featureBranchHub", "featureHOLDCO"],
  mappings: {
    LocalCurrencySymbol: "MK", 
    LocalCurrencyName: "Kwacha", 
    LocalCurrency: "MWK",
    Locale: "MW",
    LocalValue: "DomesticValue",
    Regulator: "CB",
    DealerPrefix: "RE",
    RegulatorPrefix: "CB",
    StateName: "Region",
    _minLenErrorType: "ERROR", // SUCCESS, ERROR, WARNING
    _maxLenErrorType: "ERROR",
    _lenErrorType: "ERROR"
  }
});