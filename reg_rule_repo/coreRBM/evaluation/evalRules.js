define(function () {
  return function (evaluationEx) {
    var evaluations;
    with (evaluationEx) {
      evaluations = [
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA, accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_OTH],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_FCDA, at.RE_FCDA, at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.VOSTRO],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          crDecision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            nonResSide: drcr.DR,
            nonResAccountType: [at.VOSTRO],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_FCDA, at.RE_FCDA]
          },
          drDecision: {
            manualSection: "CFERS Manual Section B - (b) page 11 (2nd diag)",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            nonResSide: drcr.DR,
            nonResAccountType: [at.VOSTRO],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_FCDA, at.RE_FCDA],
            drSideSwift: "NTNRC"
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.REPORTABLE,
            reportingSide: drcr.DR,
            flow: flowDir.OUT,
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_OTH],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_FCDA, at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Not stated in RBM specification, but seems similar to CFERS Manual Section B - (a) page 9 (res Kwacha to non res nostro)",
            reportable: rep.REPORTABLE,
            reportingSide: drcr.DR,
            flow: flowDir.OUT,
            nonResSide: drcr.CR,
            nonResAccountType: [at._CASH_],
            resSide: drcr.DR,
            resAccountType: [at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.VOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.VOSTRO],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_FCDA, at.RE_FCDA]
          }
        },
        {
          // [Resident.Exception.ExceptionName] 315: May not be completed except if the NonResident AccountIdentifier is NON RESIDENT KWACHA, VISA NET or MASTER SEND
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.VOSTRO, accType.NOSTRO, accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.NONREPORTABLE
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.FCA, accType.VOSTRO, accType.NOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.NONREPORTABLE
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Not defined in manual explicitly",
            reportable: rep.NONREPORTABLE,
            drSideSwift: "NTNRC"
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.NONREPORTABLE,
            drSideSwift: "DTCUS"
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.LOCAL_ACC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_KWT],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at._CASH_],
            resSide: drcr.CR,
            resAccountType: [at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Direct cash exchange between residents and non-residents is not reportable",
            reportable: rep.NONREPORTABLE
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_KWT],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at._CASH_],
            resSide: drcr.DR,
            resAccountType: [at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Direct cash exchange between residents and non-residents is not reportable",
            reportable: rep.NONREPORTABLE
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_CURR],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "SARB Manual Section B2 - A (i) a and b  (for IN) - no RBM specification",
            reportable: rep.REPORTABLE,
            category: ['200', '250', '251'],
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at._CASH_],
            resException: 'MUTUAL PARTY'
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "SARB Manual Section B2 - A (i) a and b  (for OUT) - no RBM specification",
            reportable: rep.REPORTABLE,
            category: ['200', '250', '251'],
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.DR,
            nonResAccountType: [at._CASH_],
            resException: 'MUTUAL PARTY'
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_CURR],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "SARB Manual Section B2 - A (i) c, d, e, f, g and h  (for IN) - no RBM specification",
            reportable: rep.REPORTABLE,
            category: ['200', '252', '255', '256', '530/05'],
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResException: 'MUTUAL PARTY',
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "SARB Manual Section B2 - A (i) c, d, e, f, g and h  (for OUT) - no RBM specification",
            reportable: rep.REPORTABLE,
            category: ['200', '252', '255', '256', '530/05'],
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResException: 'MUTUAL PARTY',
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_CURR],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "SARB Manual Section B2 - A (i) c, d, e, f, g and h  (for IN) - no RBM specification",
            reportable: rep.REPORTABLE,
            category: ['200', '252', '255', '256', '530/05'],
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResException: 'MUTUAL PARTY',
            resSide: drcr.CR,
            resAccountType: [at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "SARB Manual Section B2 - A (i) c, d, e, f, g and h  (for OUT) - no RBM specification",
            reportable: rep.REPORTABLE,
            category: ['200', '252', '255', '256', '530/05'],
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResException: 'MUTUAL PARTY',
            resSide: drcr.DR,
            resAccountType: [at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResException: ['FCDA RESIDENT NON REPORTABLE'],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResException: ['FCDA RESIDENT NON REPORTABLE'],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "SARB Manual Section B2 - B (iii) (for OUT) - no RBM specification",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.RE_FBC],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_FCDA, at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "SARB Manual Section B2 - B (iii) (for IN) - no RBM specification",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.RE_FBC],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_FCDA, at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CFC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "SARB Manual Section B2 - C i, ii  (for OUT) - no RBM specification",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResException: ['FCDA RESIDENT NON REPORTABLE'],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CFC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "SARB Manual Section B2 - C i, ii  (for IN) - no RBM specification",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResException: ['FCDA RESIDENT NON REPORTABLE'],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.FCA],
          validFrom: "2013-07-18",
          drDecision: {
            manualSection: "SARB Manual Section B2 - C (or OUT) - no RBM specification",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResException: ['FCDA RESIDENT NON REPORTABLE'],
            resSide: drcr.DR,
            resAccountType: [at.RE_FCDA]
          },
          crDecision: {
            manualSection: "SARB Manual Section B2 - C (or IN) - no RBM specification",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResException: ['FCDA RESIDENT NON REPORTABLE'],
            resSide: drcr.CR,
            resAccountType: [at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CFC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CFC],
          validFrom: "2013-07-18",
          drDecision: { // No compliance on debits
            manualSection: "SARB Manual Section B2 - C (for OUT) - no RBM specification",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            nonResSide: drcr.CR,
            nonResException: ['FCDA RESIDENT NON REPORTABLE'],
            resSide: drcr.DR,
            resAccountType: [at.RE_FCDA]
          },
          crDecision: {
            manualSection: "SARB Manual Section B2 - C (for IN) - no RBM specification",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            nonResSide: drcr.DR,
            nonResException: ['FCDA RESIDENT NON REPORTABLE'],
            resSide: drcr.CR,
            resAccountType: [at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CFC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (b) page 10 shows this as a Not Reportable (ZZ1 converted to not reportable)",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            category: '513',
            nonResSide: drcr.CR,
            nonResAccountType: [at.RE_FCDA],
            resSide: drcr.DR,
            resAccountType: [at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CFC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (b) page 10 shows this as a Not Reportable (ZZ1 converted to not reportable)",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            category: '517',
            nonResSide: drcr.DR,
            nonResAccountType: [at.RE_FCDA],
            resSide: drcr.CR,
            resAccountType: [at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.FCA, accType.CFC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at._CASH_, at._CASH_, at.NR_KWT],
            resSide: drcr.DR,
            resAccountType: [at.RE_FCDA, at.RE_FCDA]
          }
        },
        // {
        //   drResStatus: resStatus.NONRES,
        //   drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
        //   crResStatus: resStatus.RESIDENT,
        //   crAccType: [accType.FCA],
        //   validFrom: "2013-07-18",
        //   decision: {
        //     manualSection: "B and T Section B.1 (C) - page 15",
        //     reportable: rep.ZZ1REPORTABLE,
        //     flow: flowDir.OUT,
        //     reportingSide: drcr.CR,
        //     nonResSide: drcr.DR,
        //     nonResAccountType: [at.NR_KWT, at.NR_OTH, at.NR_KWT],
        //     resSide: drcr.CR,
        //     resAccountType: [at.RE_FCDA]
        //   }
        // },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_FCDA],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_FCDA],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          drDecision: {
            manualSection: "CFERS Manual Section B - (b) page 10 (reportable on Credit side)",
            reportable: rep.NONREPORTABLE,
            drSideSwift: "NTNRC"
          },
          crDecision: {
            manualSection: "CFERS Manual Section B - (b) page 10",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_FCDA],
            resSide: drcr.CR,
            resAccountType: [at.RE_FCDA, at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CFC, accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (b) page 10",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_FCDA],
            resSide: drcr.DR,
            resAccountType: [at.RE_FCDA, at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.FCA],
          validFrom: "2013-08-19",
          drDecision: {
            manualSection: "SARB Operations Manual Section B.2 - Page 11 - no RBM specification",
            reportable: rep.ZZ1REPORTABLE,
            reportingSide: drcr.DR,
            nonResSide: drcr.DR,
            flow: flowDir.OUT,
            nonResAccountType: [at.NR_FCDA],
            resException: ['FCDA NON RESIDENT NON REPORTABLE']
          },
          crDecision: {
            manualSection: "SARB Operations Manual Section B.2 - Page 11 - no RBM specification",
            reportable: rep.ZZ1REPORTABLE,
            reportingSide: drcr.CR,
            nonResSide: drcr.CR,
            flow: flowDir.IN,
            nonResAccountType: [at.NR_FCDA],
            resException: ['FCDA NON RESIDENT NON REPORTABLE']
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.VOSTRO, accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Std Bank Workshop - Assume Resident Nostro refers to bank's CFC account",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_OTH],
            resSide: drcr.DR,
            resAccountType: [at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_CURR, accType.CASH_LOCAL, accType.CFC, accType.FCA, accType.LOCAL_ACC, accType.VOSTRO],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.VOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a)(b) page 9,10",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.VOSTRO],
            //resSide: drcr.CR,
            resException: 'FCDA NON RESIDENT NON REPORTABLE'
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.VOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a)(b) page 9,10",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.VOSTRO],
            //resSide: drcr.DR,
            resException: 'FCDA NON RESIDENT NON REPORTABLE'
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.VOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.VOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.VOSTRO],
            //resSide: drcr.CR,
            resException: 'VOSTRO NON REPORTABLE'
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_FCDA],
            //resSide: drcr.DR,
            resException: ['FCDA NON RESIDENT NON REPORTABLE']
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_FCDA],
            //resSide: drcr.CR,
            resException: ['FCDA NON RESIDENT NON REPORTABLE']
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.VOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.VOSTRO],
            //resSide: drcr.CR,
            resException: ['VOSTRO NON REPORTABLE']
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.VOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.VOSTRO],
            //resSide: drcr.CR,
            resException: ['VOSTRO NON REPORTABLE']
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.VOSTRO],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.VOSTRO, accType.NOSTRO, accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Resident Vostro accounts are not valid - use Resident Kwacha or Non Resident VOSTRO instead",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.VOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.VOSTRO, accType.NOSTRO, accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Resident Vostro accounts are not valid - use Resident Kwacha or Non Resident VOSTRO instead",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.NOSTRO, accType.CFC, accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.VOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Resident Vostro accounts are not valid - use Resident Kwacha or Non Resident VOSTRO instead",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.VOSTRO, accType.NOSTRO, accType.CFC, accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.VOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Resident Vostro accounts are not valid - use Resident Kwacha or Non Resident VOSTRO instead",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CFC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Non Resident CFC accounts are not valid",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CFC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Non Resident CFC accounts are not valid",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA, accType.VOSTRO, accType.NOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CFC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Non Resident CFC accounts are not valid",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CFC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.FCA, accType.VOSTRO, accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Non Resident CFC accounts are not valid",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_CURR],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Direct foreign cash exchange between residents is not reportable",
            reportable: rep.NONREPORTABLE
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Direct foreign cash exchange between residents and non residents is not reportable",
            reportable: rep.NONREPORTABLE
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_CURR],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Direct foreign cash exchange between residents and non residents is not reportable",
            reportable: rep.NONREPORTABLE
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_CURR],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Direct foreign cash exchange between non residents is not reportable",
            reportable: rep.NONREPORTABLE
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Direct foreign cash exchange between non residents is not reportable",
            reportable: rep.NONREPORTABLE
          }
        },
        // {
        //   drResStatus: resStatus.RESIDENT,
        //   drAccType: [accType.CFC],
        //   crResStatus: resStatus.NONRES,
        //   crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
        //   validFrom: "2013-07-18",
        //   decision: {
        //     manualSection: "Not explicitly stated in SARB specification",
        //     reportable: rep.ILLEGAL
        //   }
        // },
         {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "CFERS Manual Section B - (a) page 9",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_KWT, at.NR_OTH, at.NR_KWT],
            resSide: drcr.CR,
            resAccountType: [at.RE_FCDA, at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.IHQ,
          drAccType: [accType.CFC, accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.VOSTRO, accType.NOSTRO, accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "SARB Manual Section B4 - A i (Cross Border transactions: Outflow; IHQ to NonResident) - no RBM specification",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            subject: 'IHQnnn',
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_KWT, at.NR_OTH, at.NR_FCDA],
            resSide: drcr.DR,
            resAccountType: [at.RE_FCDA, at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.VOSTRO, accType.NOSTRO, accType.FCA],
          crResStatus: resStatus.IHQ,
          crAccType: [accType.CFC, accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "SARB Manual Section B4 - A i (Cross Border transactions: Inflow; NonResident to IHQ) - no RBM specification",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            subject: 'IHQnnn',
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_KWT, at.NR_OTH, at.NR_FCDA],
            resSide: drcr.CR,
            resAccountType: [at.RE_FCDA, at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.IHQ,
          drAccType: [accType.LOCAL_ACC, accType.CFC, accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "SARB Manual Section B4 - A ii (Local transactions: Inflow; IHQ to Resident) - no RBM specification",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            subject: 'IHQnnn',
            locationCountry: 'ZA',
            nonResException: 'IHQ',
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_FCDA, at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          crResStatus: resStatus.IHQ,
          crAccType: [accType.LOCAL_ACC, accType.CFC, accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "SARB Manual Section B4 - A ii (Local transactions: Outflow; Resident to IHQ) - no RBM specification",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            subject: 'IHQnnn',
            locationCountry: 'ZA',
            nonResException: 'IHQ',
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_FCDA, at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.HOLDCO,
          drAccType: [accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "SARB Manual Section B4 - B i b (HOLDCO FCA to Resident) - no RBM specification",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            subject: 'HOLDCO',
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_FCDA],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at.RE_OTH, at.RE_FCDA, at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          crResStatus: resStatus.HOLDCO,
          crAccType: [accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "SARB Manual Section B4 - B i b (Resident to HOLDCO FCA) - no RBM specification",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            subject: 'HOLDCO',
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_FCDA],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at.RE_OTH, at.RE_FCDA, at.RE_FCDA]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.LOCAL_ACC, accType.CFC, accType.FCA, accType.NOSTRO],
          crResStatus: resStatus.HOLDCO,
          crAccType: [accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "SARB Manual Section B4 - B i c (Non Resident to HOLDCO FCA) - no RBM specification",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_KWT, at.NR_FCDA, at.NR_FCDA, at.NR_FCDA],
            resException: 'FCDA NON RESIDENT NON REPORTABLE'
          }
        },
        {
          drResStatus: resStatus.HOLDCO,
          drAccType: [accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.LOCAL_ACC, accType.CFC, accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "SARB Manual Section B4 - B i c (HOLDCO FCA to Non Resident) - no RBM specification",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_KWT, at.NR_FCDA, at.NR_FCDA],
            resException: 'FCDA NON RESIDENT NON REPORTABLE'
          }
        },
        {
          drResStatus: resStatus.HOLDCO,
          drAccType: [accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC, accType.NOSTRO, accType.VOSTRO, accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "SARB Manual Section B4 - B i d (HOLDCO Kwacha to Non Resident) - no RBM specification",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            subject: 'HOLDCO',
            nonResSide: drcr.CR,
            nonResAccountType: [at._CASH_, at.NR_KWT, at.NR_OTH, at.NR_KWT, at.NR_FCDA],
            resException: 'NON RESIDENT KWACHA'
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC, accType.NOSTRO, accType.VOSTRO, accType.FCA],
          crResStatus: resStatus.HOLDCO,
          crAccType: [accType.LOCAL_ACC],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "SARB Manual Section B4 - B i d (Non Resident to HOLDCO Kwacha) - no RBM specification",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            subject: 'HOLDCO',
            nonResSide: drcr.DR,
            nonResAccountType: [at._CASH_, at.NR_KWT, at.NR_OTH, at.NR_KWT, at.NR_FCDA],
            resException: 'NON RESIDENT KWACHA'
          }
        }
      ];
    }
    return evaluations;
  }
});