define(function () {
  return function (evaluationEx) {
    return {
      localCountryRegex: "[Mm][Ww]",
      localCurrencyRegex: "[Mm][Ww][Kk]",
      whoAmIRegex: "SB..MW.*",
      onshoreRegex: "....MW.*",
      onshoreADRegex: [
        "ABSAMWJJ.*", // ABSA BANK
        "BKCHMWJJ.*", // BANK OF CHINA
        "BNPAMWJJ.*", // BNP PARIBAS JHB
        "CITIMWJX.*", // CITIBANK JHB
        "FIRNMWJJ.*", // FIRSTRAND BANK
        "IVESMWJJ.*", // INVESTEC
        "MGTCMWJJ.*", // JP MORGAN JHB
        "NEDSMWJJ.*", // NEDBANK
        "SOGEMWJJ.*", // SOCIETY GENERAL JHB
        "SCBLMWJJ.*", // STANCHART BANK JHB
        "SBINMWJJ.*", // STATE BANK OF INDIA
        "SBSAMWJJ.*"], // STANDARD BANK
      ZZ1Reportability : false
    };
  }
})
