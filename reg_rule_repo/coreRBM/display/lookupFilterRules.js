define(function () {
  return function (predef) {
    var filterLookupRules;
    with (predef) {
      filterLookupRules = {
        filterLookupTrans: {
          ruleset: "Reporting Transaction Lookup Filter Rules",
          scope: "transaction",
          fields: [
            {
             field  : "NonResident.Exception.ExceptionName",
             display: [
               limitValue(["MUTUAL PARTY"]).onInflow().onSection("A").onCategory("252"),
               excludeValue(["MUTUAL PARTY"]).onSection("A").notOnCategory(["200", "252", "255", "256", "530/05"]),
               excludeValue(["BULK INTEREST"]).onSection("A").notOnCategory(["300", "309/08"]),
               excludeValue(["BULK VAT REFUNDS"]).onSection("A").notOnCategory(["400", "411/02"]),
               excludeValue(["BULK BANK CHARGES"]).onSection("A").notOnCategory(["200", "275"]),
               excludeValue(["BULK PENSIONS"]).onSection("A").notOnCategory(["400", "407"]),
             ]
            },
            {
              field: ["NonResident.Individual.Address.Country", "NonResident.Entity.Address.Country"],
              display: [
                excludeValue([getMap("Locale")]).onInflow().onSection("ABCDG"),
                excludeValue([getMap("Locale")], notTransactionFieldValue("Resident.Individual.ForeignIDCountry", ["NA", "ZA", "LS", "SZ"])).onSection("E"),
                excludeValue(["EU"]).onOutflow().onSection("A").notOnCategory("513"),
                excludeValue(["EU"]).onInflow().onSection("A").notOnCategory("517")
              ]
            },
            {
              field: ["Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province", "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
              display: [
                //override...
                         ]
            },
            {
              field: ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
              display: [
                //override...
              ]
            },
            {
              field: "Resident.Exception.ExceptionName",
              display: [
                limitValue(["MUTUAL PARTY"]).onSection("A").onCategory(["250", "251"]),
                excludeValue(["MUTUAL PARTY"]).onSection("A").notOnCategory(["200","250", "251"]),
                excludeValue(["BULK PENSIONS"]).onSection("A").notOnCategory(["400", "407"]),
                excludeValue(["BULK INTEREST"]).onSection("A").notOnCategory(["309/08", "300"]),
                excludeValue(["BULK DIVIDENDS"]).onSection("A").notOnCategory(["301", "300"]),
                excludeValue(["BULK BANK CHARGES"]).onSection("A").notOnCategory(["275", "200"]),
              ]
            },
          ]
        },

        filterLookupMoney: {
          ruleset: "Reporting Monetary Lookup Filter Rules",
          scope: "money",
          fields: []
        },

        filterLookupImportExport: {
          ruleset: "Reporting Import Export Lookup Filter Rules",
          scope: "importexport",
          fields: []
        }
      }
    }

    return filterLookupRules;
  }
});


