define(function () {
  return function (predef) {
    var detailTrans, detailMoney, detailImportExport;
    with(predef) {

      detailMoney = {
        ruleset: "Reporting RBM Money Display Rules",
        scope: "money",
        fields: [

          {
            field: "LoanRefNumber",
            display: [
              hide(),
              show().onSection("A").onCategory(["801", "802", "803", "804", "810", "815", "816", "817", "818", "819"]),
              show().onCategory(['106', '309']).notOnCategory(['309/08']).onSection("A"),

            ]
          },
          {
            field: "ThirdParty",
            display: [
              hide().onSection('CF')
            ]
          }
        ]
      };

      detailImportExport = {
        ruleset: "RBM Import/Export Display Rules",
        scope: "importexport",
        fields: [
          {
            field: "ImportControlNumber",
            display: [
              hide(),
              show().onOutflow().onSection("ABG").onCategory(["101", "103", "105", "106"])
            ]
          }
        ]
      };

    }

    return {
      detailTrans: {
        ruleset: "RBM Transaction Display Rules",
        scope: "transaction",
        fields: []
      },
      detailMoney: detailMoney,
      detailImportExport: detailImportExport
    }
  }

});