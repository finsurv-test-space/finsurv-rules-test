define(function () {
  return function (predef) {
    var extTrans;
    with (predef) {

      extTrans = {
        ruleset: "External Transaction Rules",
        scope: "transaction",
        validations: [
          {
            field: "",
            rules: [
              // External Validation message overrides
              // This is included in the coreRBMExternal to override featureBranchHub rules that are included in coreBON
              // TODO: Create featureRBMBranch, inherit from featureBranchHub, check if JAVA rules cater for suggested functionality
              ignore('bh_brn3'),
              message('bh_brc1','220', null),
              message('bh_brc2','221', null),
              message('bh_brc3','222', null),
              message('bh_brn1','223', null),
              message('bh_brn2','224', null),
              message('bh_hubc1','221', null),
              message('bh_hubc2','217', null),
              message('bh_hubn2','223', null),
              message('bh_hubn1','224', null),
            ]
          },
          //  Could not find external CCN validation for resident individual/entity in current RBM docs
          // {
          //   field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],
          //   rules: [
          //     validate("ext_ccn1", "Validate_ImportUndertakingCCN", //323, "Not a registered import undertaking client (the Flow is OUT and category is 102/01 to 102/10 or 104/01 to 104/10 is used)"
          //       notEmpty).onOutflow().onSection("AB").notOnCategory(['102/11', '104/11']).onCategory(['102', '104']),
          //     validate("ext_ccn2", "Validate_ValidCCN", //322, "Not a registered customs client number"
          //       notEmpty.and(hasPattern(/\d{8}/))).onSection("A")
          //   ]
          // },
          {
            field: "ReplacementOriginalReference",
            minLen: 1,
            maxLen: 30,
            rules: [
              validate("ext_repot1", "Validate_ReplacementTrnReference", // 211, "Transaction reference of cancelled transaction not stored on RBM database."
                notEmpty.and(hasTransactionFieldValue("ReplacementTransaction", "Y"))).onSection("H")
            ]
          }
        ]
      };

    }
    return extTrans;
  }
});

