define(function () {
  return function (predef) {
    if (!Array.prototype.find) {
      Array.prototype.find = function (predicate) {
        if (this === null) {
          throw new TypeError('Array.prototype.find called on null or undefined');
        }
        if (typeof predicate !== 'function') {
          throw new TypeError('predicate must be a function');
        }
        var list = Object(this);
        var length = list.length >>> 0;
        var thisArg = arguments[1];
        var value;

        for (var i = 0; i < length; i++) {
          value = list[i];
          if (predicate.call(thisArg, value, i, list)) {
            return value;
          }
        }
        return undefined;
      };
    }

    var util = {
      flow: {
        curr: 'ZAR',
        findCurrency: function (itm, idx, arr) {
          //console.log(["itm", itm]);
          //console.log(util.flow.curr);
          return itm.curr === util.flow.curr;
        },
        currencyFinder: function (setCurr) {
          //console.log(["currencyFinder", util.flow]);
          util.flow.curr = setCurr;

          return util.flow.findCurrency;
        }
      }
    };

    var convertCurrency = function (context) {
      var rates = context.getTransactionField("ForeignValueRatesInZAR");
      var curr = context.getTransactionField("FlowCurrency");

      if (!rates)
        return 1;

      var conversion = rates.find(util.flow.currencyFinder(curr));

      if (!conversion.rate)
        return 1;

      return new Number(conversion.rate);
    };

    //var convertValue = ;

    var display;

    with (predef) {
      display = {
        detailTrans: {
          ruleset: "Flow Transaction Display Rules",
          scope: "transaction",
          fields: [{
            field: "ForeignValueRatesInZAR",
            display: [
              disable()
            ]
          },
            {
              field: "TrnReference",
              display: [
                disable()
              ]
            },
            {
              field: "FlowCurrency",
              display: [
                disable()
              ]
            },
            {
              field: "TransactionCurrency",
              display: [

                setValue("%s", "FlowCurrency")
              ]
            },
            {
              field: "TotalForeignValue",
              display: [
                show(),
                // setValue(undefined, null,
                //   notEmpty
                //     .and(
                //     hasTransactionField("ZAREquivalent")
                //       .and(not(isFocusOnField("TotalForeignValue")))
                //   )
                // )
              ]
            },
            {
              field: "ZAREquivalent",
              display: [
                show(),
                hide(hasTransactionFieldValue("FlowCurrency", "ZAR")),
                disable(),
                setValue(function(context){
                  return context.getCustomValue('TotalDomesticAmount');
                },isEmpty.and(hasCustomField('TotalDomesticAmount')).and(hasCustomValue('inZAR', true)))
              ]
            },
            {
              field: "FeesIncluded",
              display: [
                hide(),
                show(isCurrencyIn("ZAR").or(hasTransactionField("ZAREquivalent")))
                  .onOutflow().onSection("A"),
                setValue(undefined, null, not(isCurrencyIn("ZAR").or(hasTransactionField("ZAREquivalent"))).and(notEmpty))
                  .onOutflow().onSection("A"),
                setValue('N', null, isEmpty.and(isCurrencyIn("ZAR").or(hasTransactionField("ZAREquivalent"))))
                  .onOutflow().onSection("A")
              ]
            },
            {
              field: "RateConfirmation",
              display: [
                show(),
                setValue("N", null, notTransactionField("RateConfirmation"))
              ]
            },
            {
              field: "BranchCode",
              display: [
                disable()
              ]
            },
            {
              field: "BranchName",
              display: [
                disable()
              ]
            },
            {
              field: "OriginatingBank",
              display: [
                disable()
              ]
            },
            {
              field: "OriginatingCountry",
              display: [
                disable()
              ]
            },
            {
              field: "AccountHolderStatus",
              display: [
                show(hasTransactionField("Resident.Individual"))
              ]
            },
            {
              field: "CounterpartyStatus",
              display: [
                show(),
                disable(isTransactionFieldProvided("CounterpartyStatus")).onOutflow()
              ]
            },
            {
              field: "IsFCA",
              display: [
                show(),
                hide(isCurrencyIn("ZAR")),
                setValue('N', null, isCurrencyIn("ZAR").and(notMoneyField("FCA"))),
                setValue('Y', null, isEmpty.and(notCurrencyIn("ZAR")).and(hasMoneyField("FCA"))),
                setValue('N', null, isEmpty.and(notCurrencyIn("ZAR")).and(notMoneyField("FCA")))
              ]
            },
            {
              field: "FCA",
              display: [
                setValue(undefined, null, hasTransactionFieldValue("IsFCA", 'N').or(notTransactionField("IsFCA")))
              ]
            },
            {
              field: "LocationCountry",
              display: [
                hide().onSection("CDEF")
              ]
            },
            // {
            //   field: "PaymentDetail.Charges",
            //   display: [
            //     disable(isTransactionFieldProvided("PaymentDetail.Charges")).onOutflow()
            //   ]
            // },
            // {
            //   field: "PaymentDetail.SWIFTDetails",
            //   display: [
            //     show().onOutflow().onSection("A")
            //   ]
            // },
            // {
            //   field: "PaymentDetail.BeneficiaryBank.SWIFTBIC",
            //   display: [
            //     show().onOutflow().onSection("A"),
            //     disable(isTransactionFieldProvided("PaymentDetail.BeneficiaryBank.SWIFTBIC")),
            //     setValue("IVESZAJJXXX", null, isEmpty.and(hasTransactionFieldValue("IsFCA", "Y"))).onOutflow().onSection("A"),
            //     clearValue(hasValue("IVESZAJJXXX").and(hasTransactionFieldValue("IsFCA", "N"))).onOutflow().onSection("A")
            //   ]
            // },
            // {
            //   field: "PaymentDetail.BeneficiaryBank.BankName",
            //   display: [
            //     show().onOutflow().onSection("A"),
            //     disable(isTransactionFieldProvided("PaymentDetail.BeneficiaryBank.BankName")),
            //   ]
            // },
            // {
            //   field: "PaymentDetail.BeneficiaryBank.BranchCode",
            //   display: [
            //     disable(isTransactionFieldProvided("PaymentDetail.BeneficiaryBank.BranchCode")),
            //     show().onOutflow().onSection("A")
            //   ]
            // },
            // {
            //   field: "PaymentDetail.BeneficiaryBank.Address",
            //   display: [
            //     disable(isTransactionFieldProvided("PaymentDetail.BeneficiaryBank.Address")),
            //     show().onOutflow().onSection("A")
            //   ]
            // },
            // {
            //   field: "PaymentDetail.BeneficiaryBank.City",
            //   display: [
            //     disable(isTransactionFieldProvided("PaymentDetail.BeneficiaryBank.City")),
            //     show().onOutflow().onSection("A")
            //   ]
            // },
            {
              field: "ReceivingCountry",
              display: [
                show().onOutflow().onSection("A"),
                disable(isTransactionFieldProvided("ReceivingCountry")),
                setValue("ZA", null, isEmpty.and(hasTransactionFieldValue("IsFCA", "Y"))).onOutflow().onSection("A"),
                //setValue("%s", countryFromSWIFTBIC("PaymentDetail.BeneficiaryBank.SWIFTBIC"), isEmpty.and(hasTransactionField("PaymentDetail.BeneficiaryBank.SWIFTBIC"))).onOutflow().onSection("A")
              ]
            },
            // {
            //   field: "PaymentDetail.IsCorrespondentBank",
            //   display: [
            //     show(hasTransactionFieldValue("IsFCA", "N")).onOutflow().onSection("A"),
            //     setValue("N", null, isEmpty).onOutflow().onSection("A"),
            //     disable(hasTransactionFieldValue("IsFCA", "Y")).onInflow(),
            //     disable().onOutflow()
            //   ]
            // },
            // {
            //   field: "PaymentDetail.CorrespondentBank.SWIFTBIC",
            //   display: [
            //     show(hasTransactionFieldValue("PaymentDetail.IsCorrespondentBank", "Y")).onOutflow().onSection("A"),
            //     disable().onOutflow().onSection("A")
            //   ]
            // },
            // {
            //   field: "PaymentDetail.CorrespondentBank.BankName",
            //   display: [
            //     show(hasTransactionFieldValue("PaymentDetail.IsCorrespondentBank", "Y")).onOutflow().onSection("A"),
            //     disable().onOutflow().onSection("A")
            //   ]
            // },
            // {
            //   field: "PaymentDetail.CorrespondentBank.BranchCode",
            //   display: [
            //     show(hasTransactionFieldValue("PaymentDetail.IsCorrespondentBank", "Y")).onOutflow().onSection("A"),
            //     disable().onOutflow().onSection("A")
            //   ]
            // },
            // {
            //   field: "PaymentDetail.CorrespondentBank.Address",
            //   display: [
            //     show(hasTransactionFieldValue("PaymentDetail.IsCorrespondentBank", "Y")).onOutflow().onSection("A"),
            //     disable().onOutflow().onSection("A")
            //   ]
            // },
            // {
            //   field: "PaymentDetail.CorrespondentBank.City",
            //   display: [
            //     show(hasTransactionFieldValue("PaymentDetail.IsCorrespondentBank", "Y")).onOutflow().onSection("A"),
            //     disable().onOutflow().onSection("A")
            //   ]
            // },
            {
              field: "CorrespondentCountry",
              display: [
                //show(hasTransactionFieldValue("PaymentDetail.IsCorrespondentBank", "Y")).onOutflow().onSection("A"),
                disable().onOutflow().onSection("A")
              ]
            },
            {
              field: "Resident.Individual.DateOfBirth",
              display: [
                show().onSection("ABCEG"),
                setValue("%s", dateOfBirthFromSAID("Resident.Individual.IDNumber"), isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", ["South African Resident"])))
              ]
            },
            {
              field: "Resident.Individual.Gender",
              display: [
                show().onSection("ABCEG"),
                setValue("%s", genderFromSAID("Resident.Individual.IDNumber"), isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", ["South African Resident"])))
              ]
            },
            {
              field: "Resident.Individual.IDNumber",
              display: [
                show(hasTransactionFieldValue("AccountHolderStatus", ["South African Resident"])
                  .and(hasTransactionField("Resident.Individual")))
                  .onSection("ABEG")
              ]
            },
            {
              field: "Resident.Individual.TempResPermitNumber",
              display: [
                show().onSection("ABEG"),
                hide().onSection("AB").onCategory(["511", "512", "513"]),
                hide().onOutflow().onSection("AB").onCategory("401"),
                hide(hasTransactionFieldValue("AccountHolderStatus", ["Non Resident", "South African Resident"])).onSection("ABCEG")
              ]
            },
            {
              field: "Resident.Individual.TempResExpiryDate",
              display: [
                show().onSection("ABEG"),
                hide().onSection("AB").onCategory(["511", "512", "513"]),
                hide().onOutflow().onSection("AB").onCategory("401"),
                hide(hasTransactionFieldValue("AccountHolderStatus", ["Non Resident", "South African Resident"])).onSection("ABCEG")
              ]
            },
            {
              field: "Resident.Individual.PassportNumber",
              display: [
                show(hasTransactionFieldValue("AccountHolderStatus", "South African Resident")
                  .and(hasTransactionField("Resident.Individual")))
                  .onSection("ABEG").onCategory("256"),

                show(hasTransactionFieldValue("AccountHolderStatus", ['Non Resident'])
                  .and(hasTransactionField("Resident.Individual")))
                  .onSection("ABEG")
              ]
            },
            {
              field: "Resident.Individual.PassportExpiryDate",
              display: [
                show(hasTransactionFieldValue("AccountHolderStatus", "South African Resident")
                  .and(hasTransactionField("Resident.Individual")))
                  .onSection("ABEG").onCategory("256"),

                show(hasTransactionFieldValue("AccountHolderStatus", ['Non Resident'])
                  .and(hasTransactionField("Resident.Individual")))
                  .onSection("ABEG"),
                disable(isTransactionFieldProvided("Resident.Individual.PassportExpiryDate"))
              ]
            },
            {
              field: "Resident.Individual.PassportCountry",
              display: [
                hide(),
                show(hasTransactionFieldValue("AccountHolderStatus", "South African Resident")
                  .and(hasTransactionField("Resident.Individual")))
                  .onSection("ABEG").onCategory("256"),

                show(hasTransactionFieldValue("AccountHolderStatus", ['Non Resident'])
                  .and(hasTransactionField("Resident.Individual")))
                  .onSection("ABEG")
              ]
            },
            {
              field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],
              display: [
                show().onInflow().onSection("AB").onCategory(["101", "103", "105", "106"]),
                show().onOutflow().onSection("AB")
                  .notOnCategory(['101/11', '102/11', '103/11', '104/11'])
                  .onCategory(['101', '102', '103', '104', '105', '106'])
              ]
            },
            {
              field: ["Resident.Individual.VATNumber", "Resident.Entity.VATNumber"],
              display: [
                show(hasTransactionField("Resident.Entity")).onSection("AB")
              ]
            },
            {
              field: "Resident.Individual.TaxClearanceCertificateIndicator",
              display: [
                setValue(undefined, null, hasTransactionField("Resident.Individual")),
               // show(hasTransactionField("Resident.Individual").and(notValue("Y").and(hasAnyMoneyFieldValue("TaxClearanceCertificateIndicator", "Y")))),
                setValue("Y", null, hasTransactionField("Resident.Individual").and(notValue("Y").and(hasAnyMoneyFieldValue("TaxClearanceCertificateIndicator", "Y")))),
               // show(hasTransactionField("Resident.Individual").and(notValue("N").and(not(hasAnyMoneyFieldValue("TaxClearanceCertificateIndicator", "Y"))))),
                setValue("N", null, hasTransactionField("Resident.Individual").and(notValue("N").and(hasAnyMoneyFieldValue("TaxClearanceCertificateIndicator", "N"))))
              ]
            },
            {
              field: "Resident.Entity.TaxClearanceCertificateIndicator",
              display: [
                hide(),
                setValue('N', null, isEmpty.and(hasTransactionField("Resident.Entity"))).onCategory(['512','513']),
               // show(hasTransactionField("Resident.Individual").and(notValue("Y").and(hasAnyMoneyFieldValue("TaxClearanceCertificateIndicator", "Y")))),
                setValue("Y", null, hasTransactionField("Resident.Entity").and(hasAnyMoneyFieldValue("TaxClearanceCertificateIndicator", "Y"))),
               // show(hasTransactionField("Resident.Entity").and(notValue("N").and(not(hasAnyMoneyFieldValue("TaxClearanceCertificateIndicator", "Y"))))),
                setValue("N", null, hasTransactionField("Resident.Entity").and(not(hasAnyMoneyFieldValue("TaxClearanceCertificateIndicator", "Y"))))
              ]
            },
            {
              field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
              display: [
                show(hasResidentFieldValue("TaxClearanceCertificateIndicator", "Y")).onSection("AB")
              ]
            },
            {
              field: ["Resident.Individual.AccountName", "Resident.Entity.AccountName"],
              display: [
              ]
            },
            {
              field: ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
              display: [
              ]
            },
            {
              field: "Resident.Individual.AccountNumber",
              display: [
                disable(isTransactionFieldProvided("Resident.Individual.AccountNumber"))
              ]
            },
            {
              field: "Resident.Entity.AccountNumber",
              display: [
            //    hide(),
                disable(isTransactionFieldProvided("Resident.Entity.AccountNumber"))
              ]
            },
            {
              field: ["Resident.Individual.StreetAddress.Country", "Resident.Entity.StreetAddress.Country"],
              display: [
                show()
              ]
            },
            {
              field: ["Resident.Individual.StreetAddress.Mandate", "Resident.Entity.StreetAddress.Mandate"],
              display: [
                show(hasResidentField("StreetAddress.Country").and(notResidentFieldValue("StreetAddress.Country", "ZA")).
                  and(hasTransactionFieldValue("AccountHolderStatus", ["South African Resident", "Foreign Temporary Resident"])))
              ]
            },
            {
              field: "NonResident.Entity.EntityName",
              display: [
                show().onSection("ABCDG"),
                appendValue("%s", "Resident.Entity.EntityName", notMatchesTransactionField("Resident.Entity.EntityName").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y"))),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Entity.EntityName"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Entity.EntityName"))))
              ]
            },
            {
              field: "NonResident.Individual.Surname",
              display: [
                show().onSection("ABCDG"),
                appendValue("%s", "Resident.Individual.Surname", notMatchesTransactionField("Resident.Individual.Surname").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y"))),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Individual.Surname"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Individual.Surname"))))
              ]
            },
            {
              field: "NonResident.Individual.Name",
              display: [
                show().onSection("ABCDG"),
                appendValue("%s", "Resident.Individual.Name", notMatchesTransactionField("Resident.Individual.Name").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y"))),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Individual.Name"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Individual.Name"))))
              ]
            },
            {
              field: "NonResident.Individual.MiddleNames",
              display: [
                show().onSection("ABCDG"),
                appendValue("%s", "Resident.Individual.MiddleNames", notMatchesTransactionField("Resident.Individual.MiddleNames").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y"))),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Individual.MiddleNames"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Individual.MiddleNames"))))
              ]
            },
            {
              field: "NonResident.Individual.Gender",
              display: [
                show().onSection("ABCDG"),
                appendValue("%s", "Resident.Individual.Gender", notMatchesTransactionField("Resident.Individual.Gender").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y"))),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").or(isTransactionFieldProvided("NonResident.Individual.Gender"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").and(not(isTransactionFieldProvided("NonResident.Individual.Gender"))))
              ]
            },
            {
              field: "NonResident.Individual.AccountNumber",
              display: [
                show(hasTransactionFieldValue("IsFCA", "N").or(notTransactionField("IsFCA"))).onOutflow()
                //,disable(isTransactionFieldProvided("NonResident.Individual.AccountNumber"))
              ]
            },
            {
              field: "NonResident.Individual.PassportNumber",
              display: [
              ]
            },
            {
              field: "NonResident.Individual.PassportCountry",
              display: [
              ]
            },
            {
              field: "NonResident.Individual.IsMutualParty",
              display: [
                show(hasTransactionField("Resident.Individual")).onInflow(),
                disable(isTransactionFieldProvided("NonResident.Individual.IsMutualParty")).onOutflow()
              ]
            },
            {
              field: "NonResident.Entity.IsMutualParty",
              display: [
                show(hasTransactionField("Resident.Entity")).onInflow(),
                disable(isTransactionFieldProvided("NonResident.Entity.IsMutualParty")).onOutflow()
              ]
            },
            {
              field: "NonResident.Entity.AccountNumber",
              display: [
                show(hasTransactionFieldValue("IsFCA", "N").or(notTransactionField("IsFCA"))).onOutflow()
                //,disable(isTransactionFieldProvided("NonResident.Entity.AccountNumber"))
              ]
            },
            {
              field: "NonResident.Individual.Address.AddressLine1",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.AddressLine1", notMatchesTransactionField("Resident.Individual.StreetAddress.AddressLine1").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Individual.Address.AddressLine2",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.AddressLine2", notMatchesTransactionField("Resident.Individual.StreetAddress.AddressLine2").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Individual.Address.Suburb",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.Suburb", notMatchesTransactionField("Resident.Individual.StreetAddress.Suburb").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Individual.Address.City",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.City", notMatchesTransactionField("Resident.Individual.StreetAddress.City").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Individual.Address.State",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.Province", notMatchesTransactionField("Resident.Individual.StreetAddress.Province").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Individual.Address.PostalCode",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.PostalCode", notMatchesTransactionField("Resident.Individual.StreetAddress.PostalCode").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Individual.Address.Country",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.Country", notMatchesTransactionField("Resident.Individual.StreetAddress.Country").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                setValue("%s", "LocationCountry", isEmpty.and(notMatchesTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(hasResidentFieldValue("StreetAddress.Country", "ZA"))))),
                setValue("%s", "LocationCountry", isEmpty.and(hasTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N")))),
                disable(isTransactionFieldProvided("NonResident.Individual.Address.Country").or(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                enable(not(isTransactionFieldProvided("NonResident.Individual.Address.Country")).and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))))
              ]
            },
            {
              field: "NonResident.Entity.Address.AddressLine1",
              display: [
                setValue("%s", "Resident.Entity.StreetAddress.AddressLine1", notMatchesTransactionField("Resident.Entity.StreetAddress.AddressLine1").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Entity.Address.AddressLine2",
              display: [
                setValue("%s", "Resident.Entity.StreetAddress.AddressLine2", notMatchesTransactionField("Resident.Entity.StreetAddress.AddressLine2").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Entity.Address.Suburb",
              display: [
                setValue("%s", "Resident.Entity.StreetAddress.Suburb", notMatchesTransactionField("Resident.Entity.StreetAddress.Suburb").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Entity.Address.City",
              display: [
                setValue("%s", "Resident.Entity.StreetAddress.City", notMatchesTransactionField("Resident.Entity.StreetAddress.City").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Entity.Address.State",
              display: [
                setValue("%s", "Resident.Entity.StreetAddress.Province", notMatchesTransactionField("Resident.Entity.StreetAddress.Province").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Entity.Address.PostalCode",
              display: [
                setValue("%s", "Resident.Entity.StreetAddress.PostalCode", notMatchesTransactionField("Resident.Entity.StreetAddress.PostalCode").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                show(),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            },
            {
              field: "NonResident.Entity.Address.Country",
              display: [
                setValue("%s", "Resident.Entity.StreetAddress.Country", notMatchesTransactionField("Resident.Entity.StreetAddress.Country").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                setValue("%s", "LocationCountry", isEmpty.and(notMatchesTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(hasResidentFieldValue("StreetAddress.Country", "ZA"))))),
                setValue("%s", "LocationCountry", isEmpty.and(hasTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N")))),
                disable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(hasTransactionFieldValue("NonResident.Entity.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA")))
              ]
            }, {
              field: "Resident.Individual.TaxNumber",
              display: [
                hide(),
                show().onSection("AB").onCategory(['512', '513']),
                show(notEmpty).onSection("F")
              ]
            }, {
              field: "Resident.Individual.VATNumber",
              display: [
                hide(),
                show(notEmpty).onSection("F")
              ]
            }, {
              field: "Resident.Entity.VATNumber",
              display: [
                hide(),
                show().onSection("AB")
                  .notOnCategory(['101/11', '102/11', '103/11', '104/11'])
                  .onCategory(['101', '102', '103', '104', '105', '106']),
                show(notEmpty).onSection("F")
              ]
            }, {
              field: "Resident.Entity.TaxNumber",
              display: [
                hide(),
                show().onSection("AB")
                  .notOnCategory(['101/11', '102/11', '103/11', '104/11'])
                  .onCategory(['101', '102', '103', '104', '105', '106']),
                show(notEmpty).onSection("F")
              ]
            },
            {
              field: ["Resident.Individual.ContactDetails.Telephone",
              "Resident.Entity.ContactDetails.Telephone"],
              display: [
                setValue(function(context,moneyInd, ImportExportInd ,modelContext, field){
                  var matcher = /\d+/g;
                  if(field.val){
                    var matches = field.val.match(matcher);
                    return matches.join('');
                  }
                },notEmpty.and(isTooLong(10)).and(notPattern(/\d+/)))
              ]
            }
          ]
        },
        detailMoney: {
          ruleset: "Flow Money Display Rules",
          scope: "money",
          fields: [
            {
              field: "MoneyTransferAgentIndicator",
              display: [
                hide(isMoneyFieldProvided("MoneyTransferAgentIndicator"))
              ]
            },
            {
              field: "ForeignValue",
              display: [
                show()//.onSection("F"),
                , clearValue(isCurrencyIn("ZAR").or(evalTransactionField("ZAREquivalent", notEmpty)))

                , hide(isCurrencyIn("ZAR").or(evalTransactionField("ZAREquivalent", notEmpty)))
                //, setValue(undefined, null, isCurrencyIn("ZAR").or(evalTransactionField("ZAREquivalent", notEmpty)))
                //, setValue(undefined, null, evalTransactionField("ZAREquivalent", notEmpty))
                //, setValue(undefined, null, evalTransactionField("ZAREquivalent", notEmpty).or(isCurrencyIn("ZAR")))
              ]
            },
            {
              field: "{{LocalValue}}",
              display: [
                hide()
                //, clearValue(evalTransactionField("ZAREquivalent", isEmpty))

                , show(evalTransactionField("{{LocalCurrency}}Equivalent", notEmpty).or(isCurrencyIn("ZAR")))
                //show(evalTransactionField("ZAREquivalent", notEmpty).or(hasTransactionFieldValue("TransactionCurrency", "ZAR")))

                //, clearValue(notEmpty.and(evalMoneyField("ForeignValue", notEmpty)))
              ]
            },
            {
              field: "TaxClearanceCertificateIndicator",
              display: [
                hide(),
                show().onOutflow().onCategory(["512", "513"])
              ]
            },
            {
              field: "StrateRefNumber",
              display: [
                hide(),
                show().onCategory(["601/01", "601/03"])
              ]
            },
            {
              field: "{{Regulator}}Auth.RulingsSection",
              display: [
                hide()
              ]
            },
            // {
            //   field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
            //   display: [
            //     hide()
            //   ]
            // },
            // {
            //   field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
            //   display: [
            //     hide()
            //   ]
            // },
            // {
            //   field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
            //   display: [
            //     hide()
            //   ]
            // },
            {
              field: "{{Regulator}}Auth.SARBAuthOptional",
              display: [
                show(),
                //hide(hasMoneyField("{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber").or(hasMoneyField("{{Regulator}}Auth.SARBAuthDate"))),
                hide().onOutflow().onSection("ABG").onCategory(["105", "106"])
              ]
            },
            // {
            //   field: "{{Regulator}}Auth.IsSARBAuth",
            //   display: [
            //     hide().onSection("CDEF"),
            //     setValue("",null,function(context,value){
            //       return typeof value == 'undefined' || value == null;
            //     }),
            //     setValue("Y").onOutflow().onSection("ABG").onCategory(["105", "106"]),
            //     hide().onOutflow().onSection("ABG").onCategory(["105", "106"]),

            //     setValue("").onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
            //     hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"])
            //   ]
            // },
            {
              field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
              display: [
                // clearValue( notMoneyFieldValue("{{Regulator}}Auth.IsSARBAuth", "Y")),
                hide().onSection("CDEF"),
                hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"])
              ]
            },
            {
              field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
              display: [
                // clearValue( notMoneyFieldValue("{{Regulator}}Auth.IsSARBAuth", "Y")),
                hide().onSection("CDEF"),
                hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"])
              ]
            },
            {
              field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
              display: [
                // clearValue( hasMoneyFieldValue("{{Regulator}}Auth.IsSARBAuth", "Y")),
                hide().onSection("CDEF"),
                hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"])
              ]
            },
            {
              field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
              display: [
                // clearValue( hasMoneyFieldValue("{{Regulator}}Auth.IsSARBAuth", "Y")),
                hide().onSection("CDEF"),
                hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"])
              ]
            },
            {
              field: "AdHocRequirement.Subject",
              display: [
                hide()
              ]
            },
            {
              field: "AdHocRequirement.Description",
              display: [
                hide()
              ]
            },
            {
              field: "ReversalTrnRefNumber",
              display: [
                hide(),
                show().onCategory(["100","200","300","400","500","600","700","800"])
              ]
            },
            {
              field: "ReversalTrnSeqNumber",
              display: [
                hide(),
                show().onCategory(["100","200","300","400","500","600","700","800"])
              ]
            },
            {
              field: "BOPDIRTrnReference",
              display: [
                hide()
              ]
            },
            {
              field: "BOPDIRADCode",
              display: [
                hide()
              ]
            },
            {
              field: "SWIFTDetails",
              display: [
                hide(),
                disable()
              ]
            },
            {
              field: "LoanInterestRate",
              display: [
                hide(),
                clearValue().onOutflow().onSection("AB").notOnCategory(["810", "815", "816", "817", "818", "819"])
              ]
            },
            {
              field: "LoanInterest.BaseRate",
              display: [
                hide(),
                show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
                clearValue().onOutflow().onSection("AB").notOnCategory(["810", "815", "816", "817", "818", "819"])
              ]
            },
            {
              field: "LoanInterest.Term",
              display: [
                hide(),
                show(hasMoneyField("LoanInterest.BaseRate").and(notMoneyFieldValue("LoanInterest.BaseRate", ["FIXED", "PRIME"]))).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
                clearValue().onOutflow().onSection("AB").notOnCategory(["810", "815", "816", "817", "818", "819"])
              ]
            },
            {
              field: "LoanInterest.PlusMinus",
              display: [
                hide(),
                show(hasMoneyField("LoanInterest.BaseRate").and(notMoneyFieldValue("LoanInterest.BaseRate", ["FIXED"]))).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
                clearValue().onOutflow().onSection("AB").notOnCategory(["810", "815", "816", "817", "818", "819"])
              ]
            },
            {
              field: "LoanInterest.Rate",
              display: [
                hide(),
                show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
                show().onOutflow().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
                show().onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
                clearValue().onOutflow().onSection("AB").notOnCategory(["309", "810", "815", "816", "817", "818", "819"])
              ]
            },
            {
              field: "LoanTenorType",
              display: [
                hide(),
                show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
                clearValue().onOutflow().onSection("AB").notOnCategory(["810", "815", "816", "817", "818", "819"])
              ]
            },
            {
              field: "LoanTenorMaturityDate",
              display: [
                hide(),
                show(hasMoneyFieldValue("LoanTenorType", 'MATURITY DATE')).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
                setValue(undefined, null, hasMoneyFieldValue("LoanTenorType", 'ON DEMAND').or(notMoneyField("LoanTenorType"))),
                clearValue().onOutflow().onSection("AB").notOnCategory(["810", "815", "816", "817", "818", "819"])
              ]
            },
            {
              field: "LoanTenor",
              display: [
                disable(),
                hide(),
                show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
                clearValue(notMoneyFieldValue("LoanTenorType", 'MATURITY DATE').and(notMoneyFieldValue("LoanTenorType", 'ON DEMAND')).and(notMoneyField("LoanTenorMaturityDate"))),
                setValue(undefined, null, notMoneyField("LoanTenorType")),
                setValue('%s', "LoanTenorType", hasMoneyFieldValue("LoanTenorType", 'ON DEMAND')),
                setValue('%s', "LoanTenorMaturityDate", hasMoneyFieldValue("LoanTenorType", 'MATURITY DATE').and(hasMoneyField("LoanTenorMaturityDate")))
              ]
            },
            {
              field: "Monetary.ThirdParty.Heading",
              display: [
                //setValue("Remove All Fields", null),
                clearValue(),
                setValue("Traveller's Details").onSection("AB")
                  .onCategory(["255", "256"])
                ,setValue("Third Party Details")
                   .onOutflow().onSection("AB")
                   .onCategory(["511", "512/01", "512/02", "512/03", "512/05", "512/06", "512/07", "513"])
              ]
            },
            {
              field: "ThirdPartyKind",
              display: [
                hide().onSection("C")
              ]
            },
            {
              field: "ThirdParty.TaxNumber",
              minLen: 2,
              maxLen: 15,
              display: [
                hide(),
                show(hasTransactionField("Resident.Entity")).onOutflow().onSection("AB").onCategory(["512", "513"]),
                show(hasTransactionField("Resident.Entity")).onInflow().onSection("AB").onCategory(["511", "516"]),
                hide(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION")).onSection("A")
              ]
            },
            {
              field: "ThirdParty.VatNumber",
              minLen: 2,
              maxLen: 15,
              display: [
                hide(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION")).onSection("A")
              ]
            },
            {
              field: "ThirdParty",
              display: [
                hide(),
                show().onCategory(["255", "256"]),
                show()
                   .onOutflow()
                   .onSection("AB")
                   .onCategory(["512", "513"])
                   .notOnCategory(["512/04"]),
                show(hasMoneyField('ThirdParty'))
              ]
            },
            {
              field: "ThirdParty.ContactDetails.Telephone",
              display: [
                setValue(function(context,moneyInd, ImportExportInd ,modelContext, field){
                  var matcher = /\d+/g;
                  if(field.val){
                    var matches = field.val.match(matcher);
                    return matches.join('');
                  }
                },notEmpty.and(isTooLong(10)).and(notPattern(/\d+/)))
              ]
            }
            //,
            // {
            //   field: "TravelMode.TravellerStatus",
            //   display: [
            //     hide(),
            //     show().onCategory(["255", "256"])
            //   ]
            // },
            // {
            //   field: "ThirdParty.Individual.TempResPermitNumber",
            //   display: [
            //     hide(),
            //     show(hasMoneyField("TravelMode.TravellerStatus").and(hasMoneyFieldValue("TravelMode.TravellerStatus", ["Foreign Temporary Resident"]))).onCategory(["255", "256"])
            //   ]
            // },
            // {
            //   field: "ThirdParty.Individual.TempResExpiryDate",
            //   display: [
            //     hide(),
            //     show(hasMoneyField("TravelMode.TravellerStatus").and(hasMoneyFieldValue("TravelMode.TravellerStatus", ["Foreign Temporary Resident"]))).onCategory(["255", "256"])
            //   ]
            // },
            // {
            //   field: "ThirdParty.Individual.PassportNumber",
            //   display: [
            //     hide(),
            //     show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"])
            //   ]
            // },
            // {
            //   field: "ThirdParty.Individual.PassportCountry",
            //   display: [
            //     hide(),
            //     show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"])
            //   ]
            // },
            // {
            //   field: "ThirdParty.Individual.PassportExpiryDate",
            //   display: [
            //     hide(),
            //     show(hasMoneyField("TravelMode.TravellerStatus").and(notMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onCategory(["255", "256"])
            //   ]
            // },
            // {
            //   field: "TravelMode.Mode",
            //   display: [
            //     hide(),
            //     show().onOutflow().onCategory(["255", "256"]),
            //     setValue("%s", firstMoneyFieldValue("TravelMode.Mode"), isEmpty.and(hasAnyMoneyField("TravelMode.Mode"))).onOutflow().onCategory(["255", "256"])
            //   ]
            // },
            // {
            //   field: "TravelMode.BorderPost",
            //   display: [
            //     hide(),
            //     show(hasMoneyFieldValue("TravelMode.Mode", "ROAD")).onOutflow().onCategory(["255", "256"]),
            //     setValue("%s", firstMoneyFieldValue("TravelMode.BorderPost"), isEmpty.and(hasMoneyField("TravelMode.Mode", "ROAD").and(hasAnyMoneyField("TravelMode.BorderPost")))).onOutflow().onCategory(["255", "256"])
            //   ]
            // },
            // {
            //   field: "TravelMode.TicketNumber",
            //   display: [
            //     hide(),
            //     show(hasMoneyFieldValue("TravelMode.Mode", ["AIR", "SEA", "RAIL"])).onOutflow().onCategory(["255", "256"])
            //   ]
            // },
            // {
            //   field: "TravelMode.DepartureDate",
            //   display: [
            //     hide(),
            //     show().onOutflow().onCategory(["255", "256"]),
            //     setValue("%s", firstMoneyFieldValue("TravelMode.DepartureDate"), isEmpty.and(hasAnyMoneyField("TravelMode.DepartureDate"))).onOutflow().onCategory(["255", "256"])
            //   ]
            // },
            // {
            //   field: "TravelMode.DestinationCountry",
            //   display: [
            //     hide(),
            //     show().onOutflow().onCategory(["255", "256"]),
            //     setValue("%s", firstMoneyFieldValue("TravelMode.DestinationCountry"), isEmpty.and(hasAnyMoneyField("TravelMode.DestinationCountry"))).onOutflow().onCategory(["255", "256"])
            //   ]
            // }
          ]
        },
        detailImportExport: {
          ruleset: "Flow Import/Export Display Rules",
          scope: "importexport",
          fields: [{
            field: "TransportDocumentNumber",
            display: [
              //clearValue().onInflow(),
              //clearValue().onOutflow().onSection("CDEF"),
              //clearValue().onOutflow().onSection("ABG")
              //    .onCategory(["103/11"])
              //    .notOnCategory(["103", "105", "106"]),
              hide(),
              show().onOutflow().onSection("ABG")
                .onCategory(["103", "105", "106"])
                .notOnCategory(["103/11"])
            ]
          },
            {
              field: "ImportControlNumberLabel",
              display: [
                setValue("Invoice Number", null, isEmpty).onOutflow().onSection("ABG").onCategory("101"),
                setValue("Movement Reference Number (MRN)", null, isEmpty).onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
              ]
            },
            {
              field: 'PaymentCurrencyCode',
              display: [
                setValue('%s', 'transaction::FlowCurrency', isEmpty.or(notMatchesTransactionField("FlowCurrency").and(notTransactionField("ZAREquivalent")))),
                setValue('ZAR', null, isEmpty.or(notValue("ZAR").and(hasTransactionField("ZAREquivalent"))))

                //setValue('%s', 'transaction::FlowCurrency', isEmpty.or(notMatchesTransactionField("FlowCurrency")))//.and(notTransactionField("ZAREquivalent")))),
                ////setValue('ZAR', null, isEmpty.or(notValue("ZAR").and(hasTransactionField("ZAREquivalent"))))
              ]
            },
            {
              field: "MRNNotOnIVS",
              display: [
                hide()
              ]
            }
          ]

        }
      };
    }

    return display;
  }
});