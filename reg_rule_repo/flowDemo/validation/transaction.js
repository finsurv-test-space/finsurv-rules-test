define(function () {
  //THIS IS the Transaciotn rules
  return function (predef) {
    var transaction;

    with (predef) {
      transaction = {
        ruleset: "Transaction Rules for Flow form",
        scope: "transaction",
        validations: [
          // {
          //   field: "",
          //   rules: [
          //     //ignore("field.maxLen"),
          //     message("vd4", null, "Submission Date must be equal to or after the 2013-08-19"),
          //     message("ocntry1", null, "If Originating Bank is completed, this must also be completed"),
          //     message("ocntry4", null, "SWIFT country code may not be ZA for Inwards"),
          //     message("cbank1", null, "If Intermediary Bank Country is completed, this must also be completed"),
          //     message("ccntry1", null, "If Intermediary Bank Name  is completed, this must also be completed"),
          //     message("rbank3", null, "If Receiving Country is completed, must be completed"),
          //     message("rcntry1", null, "If Beneficiary Bank is completed, this must also be completed"),
          //     message("rcntry4", null, "SWIFT country code must be ZA for Inwards"),
          //     message("nrgn1", null, "Invalid gender"),
          //     message("nr1", null, "Must contain one of Individual or Entity Sender Type").onInflow(),
          //     message("nr1", null, "Must contain one of Individual or Entity Beneficiary Type").onOutflow(),
          //     message("nr6", null, "Not required for category chosen"),
          //     message("nrsn1", null, "If Sender Type Individual is completed, must be completed").onInflow(),
          //     message("nrsn1", null, "If Beneficiary Type Individual is completed, must be completed").onOutflow(),
          //     message("nrnm1", null, "Name is required"),
          //     message("nrlen1", null, "Entity Name is required"),
          //     message("nrpc2", null, "Invalid Passport Country of Issue"),
          //     message("nrian2", null, "Cannot be the same account details as the remitter").onInflow(),
          //     message("nrian2", null, "Cannot be the same account details as the beneficiary").onOutflow(),
          //     message("ripc1", null, "Country is required"),
          //     message("tma", null, "At least one Sequence Amount must be provided"),
          //     message("ritrpn1", null, "Not required for category chosen"),
          //     message("ritrpn2", null, "Not required for category chosen"),
          //     message("ritrpn3", null, "For an Individual at least one of ID Number or Temporary Resident Permit Number or Passport Number must be completed"),
          //     message("nriz3", null, "Code is too long"),
          //     message("nrictry1", null, "Country is required"),
          //     message("nrictry3", null, "Country cannot be ZA"),
          //     message("nrictry7", null, "Currency does not correspond to country specified"),
          //     message("rig1", null, "Gender is required"),
          //     message("rig2", null, "Invalid Gender"),
          //     message("ridob1", null, "Date of birth is required"),
          //     message("ridob3", null, "Date of birth does not match the ID number provided"),
          //     message("riidn2", null, "ID number is required for this category"),
          //     message("rifidn1", null, "Not required for category chosen"),
          //     message("rifidn2", null, "Not required for category chosen"),
          //     message("rifidc1", null, "Country is required"),
          //     message("g1", null, "An email address, fax number or phone number is required"),
          //     message("accno3", null, "Applicant account number and Sender account number cannot be the same").onInflow(),
          //     message("accno3", null, "Applicant account number and Beneficiary account number cannot be the same").onOutflow(),
          //     message("accno4", null, "Account number is required"),
          //     message("ccn2", null, "Customs Client Number is required for the category code selected"),
          //     message("ccn4", null, "Customs Client Number must be between 8 and 13 digits"),
          //     message("ccn5", null, "This number is not allowed"),
          //     message("tn1", null, "Tax number should pass the SARS validation (please check number)"),
          //     message("tcci1", null, "Tax Clearance Certificate Indicator is required for the category code selected"),
          //     message("tccr1", null, "Tax Clearance Certificate Reference number is required"),
          //     message("cn1", null, "Contact name is required"),
          //     message("cnte1", null, "Contact details are required"),
          //     message("cntft1", null, "At least one of the Email, Fax or Telephone fields need to be completed"),
          //     message("ripn1", null, "If category 255 is used, Traveller Passport Number must be completed"),
          //     message("OriginatingBank.minLen", null, "Bank name is too short"),
          //     message("LocationCountry.minLen", null, "A valid country code is 2 characters long"),
          //     message("Resident.Individual.IDNumber.minLen", null, "ID number is too short"),
          //     message("Resident.Individual.TempResPermitNumber.minLen", null, "Permit number is too short"),
          //     message("Resident.Individual.ForeignIDNumber.minLen", null, "ID number is too short"),
          //     message("Resident.Individual.ForeignIDCountry.minLen", null, "Country is too short"),
          //     message("Resident.Individual.PassportNumber.minLen", null, "Passport number is too short"),
          //     message("Resident.Individual.PassportCountry.minLen", null, "Country is too short"),
          //     message("Resident.Individual.AccountName.minLen", null, "Name is too short"),
          //     message("Resident.Individual.AccountNumber.minLen", null, "Account number is too short"),
          //     //message("Resident.Individual.CustomsClientNumber.minLen", null, "Client number is too short"),
          //     ignore("Resident.Individual.CustomsClientNumber.minLen"),
          //     message("Resident.Individual.TaxNumber.minLen", null, "Tax number is too short"),
          //     message("Resident.Individual.VATNumber.minLen", null, "VAT number is too short"),
          //     message("Resident.Individual.TaxClearanceCertificateReference.minLen", null, "Reference is too short"),
          //     //ignore("Resident.Individual.TaxClearanceCertificateReference.minLen"),
          //     message("Resident.Individual.StreetAddress.AddressLine1.minLen", null, "Address is too short"),
          //     message("Resident.Individual.StreetAddress.AddressLine2.minLen", null, "Address is too short"),
          //     message("Resident.Individual.StreetAddress.Suburb.minLen", null, "Suburb is too short"),
          //     message("Resident.Individual.StreetAddress.City.minLen", null, "City is too short"),
          //     message("Resident.Individual.StreetAddress.State.minLen", null, "State is too short"),
          //     message("Resident.Individual.StreetAddress.PostalCode.minLen", null, "Code is too short"),
          //     message("Resident.Individual.ContactDetails.ContactSurname.minLen", null, "Surname is too short"),
          //     message("Resident.Individual.ContactDetails.ContactName.minLen", null, "Name is too short"),
          //     message("Resident.Individual.ContactDetails.Email.minLen", null, "Email address is too short"),
          //     message("Resident.Individual.ContactDetails.Fax.minLen", null, "Fax number is too short"),
          //     message("Resident.Individual.ContactDetails.Telephone.minLen", null, "Phone number is too short"),
          //     message("NonResident.Individual.PassportNumber.minLen", null, "Passport number is too short"),
          //     message("NonResident.Individual.AccountNumber.minLen", null, "Number is too short"),
          //     message("NonResident.Entity.AccountNumber.minLen", null, "Number is too short"),
          //     message("NonResident.Entity.EntityName.minLen", null, "Entity Name is too short"),
          //     message("NonResident.Individual.Address.AddressLine1.minLen", null, "Address is too short"),
          //     message("NonResident.Entity.Address.AddressLine1.minLen", null, "Address is too short"),
          //     message("NonResident.Individual.Address.AddressLine2.minLen", null, "Address is too short"),
          //     message("NonResident.Entity.Address.AddressLine2.minLen", null, "Address is too short"),
          //     message("NonResident.Individual.Address.Suburb.minLen", null, "Suburb is too short"),
          //     message("NonResident.Entity.Address.Suburb.minLen", null, "Suburb is too short"),
          //     message("NonResident.Individual.Address.City.minLen", null, "City is too short"),
          //     message("NonResident.Entity.Address.City.minLen", null, "City is too short"),
          //     message("NonResident.Individual.Address.State.minLen", null, "State is too short"),
          //     message("NonResident.Entity.Address.State.minLen", null, "State is too short"),
          //     message("NonResident.Individual.Address.PostalCode.minLen", null, "Code is too short"),
          //     message("NonResident.Entity.Address.PostalCode.minLen", null, "Code is too short"),
          //     message("NonResident.Individual.Address.Country.minLen", null, "Country code is too short"),
          //     message("NonResident.Entity.Address.Country.minLen", null, "Country code is too short"),
          //     // message("PaymentDetail.BeneficiaryBank.SWIFTBIC.len", null, "SWIFT BIC has to be 11 characters"),
          //     // message("PaymentDetail.BeneficiaryBank.BankName.minLen", null, "Bank name is too short"),
          //     // message("PaymentDetail.BeneficiaryBank.BranchCode.minLen", null, "Branch code is too short"),
          //     // message("PaymentDetail.BeneficiaryBank.Address.minLen", null, "Address is too short"),
          //     // message("PaymentDetail.BeneficiaryBank.City.minLen", null, "City is too short"),
          //     // message("PaymentDetail.CorrespondentBank.SWIFTBIC.minLen", null, "SWIFT BIC has a minimum of 8 characters"),
          //     // message("PaymentDetail.CorrespondentBank.BankName.minLen", null, "Bank name is too short"),
          //     // message("PaymentDetail.CorrespondentBank.BranchCode.minLen", null, "Branch code is too short"),
          //     // message("PaymentDetail.CorrespondentBank.Address.minLen", null, "Address is too short"),
          //     // message("PaymentDetail.CorrespondentBank.City.minLen", null, "City is too short"),
          //     message("fcurr1", null, "Please select a currency").onInflow(),
          //     message('tni1', null, 'Account holder tax number required')
          //   ]
          // },
          // {
          //   field: "TrnReference",
          //   rules: [
          //     ignore("tref1")
          //   ]
          // },
          {
            field: "OriginatingBank",
            rules: [
              ignore("obank1"),
              ignore("obank3"),
              ignore("obank4"),
              ignore("obank5"),
              ignore("obank6"),
              ignore("obank7"),
              ignore("obank8"),
              ignore("obank9")
            ]
          },
          {
            field: "ReceivingBank",
            rules: [
              ignore("rbank1"),
              ignore("rbank3"),
              ignore("rbank4"),
              ignore("rbank5"),
              ignore("rbank6"),
              ignore("rbank7"),
              ignore("rbank8"),
              ignore("rbank9")
            ]
          },
          {
            field: "CorrespondentBank",
            rules: [
              ignore("cbank1")
            ]
          },
          {
            field: "ReceivingCountry",
            rules: [
              ignore("rcntry4"),
              // failure("rcntry5", 234, "Country code may not be ZA",
              //   notEmpty.and(hasValue("ZA")).and(hasTransactionFieldValue("IsFCA", "N"))).onOutflow().onSection("ABG")
            ]
          },
          // {
          //   field: "IsFCA",
          //   rules: [
          //     failure("flw_iisa1", "I02", "This must be completed",
          //       isEmpty.and(hasTransactionField("FlowCurrency")).and(notCurrencyIn("ZAR"))).onSection("A"),
          //     failure("flw_iisa2", "I??", "This field may not be completed for ZAR payments",
          //       notValue("N").and(hasTransactionField("FlowCurrency")).and(isCurrencyIn("ZAR"))).onSection("A")
          //   ]
          // },
          // {
          //   field: "FCA",
          //   len: 13,
          //   rules: [
          //     failure("flw_isa1", "I03", "This must be completed",
          //       isEmpty.and(hasTransactionFieldValue("IsFCA", "Y"))).onSection("A"),
          //     failure("flw_isa2", "I36", "Foreign Currency Account (FCA) is not required and should be deleted",
          //       notEmpty.and(notTransactionFieldValue("IsFCA", "Y"))).onSection("A"),
          //     failure('flw_isa3', "I??", "Foreign Currency Account and Account to be Debitted cannot be the same",
          //       notEmpty.and(matchesResidentField("AccountNumber"))).onSection("ABCDG"),
          //     failure('flw_isa4', "I??", "FCA account number needs to start with 1100, 1101 or 1300", notEmpty.and(notPattern(/^(1100|1101|1300)/))),
          //     failure('flw_isa5', "I??", "FCA account number needs to be numeric", notEmpty.and(notPattern(/^\d+$/)))

          //   ]
          // },
          // {
          //   field: "AccountHolderStatus",
          //   rules: [
          //     failure("flw_ahs1", "I04", "This must be completed",
          //       isEmpty).onSection("A"),
          //     failure("flw_ahs2", "I15", "Non Residents must use categories 250 or 251 for travel and not 255 or 256",
          //       hasValueIn(["Non Resident"])).onSection("A").onCategory(["255", "256"]),
          //     failure("flw_ahs3", "I16", "Travel categories 250, 251 are reserved for Non Residents only",
          //       hasValueIn(["South African Resident"])).onSection("A").onCategory(["250", "251"])
          //   ]
          // },
          // {
          //   field: "CounterpartyStatus",
          //   rules: [
          //     failure("flw_cps1", "I05", "This must be completed",
          //       isEmpty).onSection("A")
          //   ]
          // },
          // {
          //   field: "LocationCountry",
          //   len: 2,
          //   rules: [
          //     failure("flw_lc0", 405, "Please select source of funds",
          //       isEmpty).onInflow().onSection("ABG"),
          //     failure("flw_lc1", 405, "Must be completed",
          //       isEmpty).onOutflow().onSection("ABG"),
          //     failure("flw_lc2", 238, "Country where service was requested from or where the merchandise was exported to, is not valid",
          //       notEmpty.and(hasInvalidSWIFTCountry)).onSection("ABG"),
          //     failure("flw_lc3", 290, "Originating Country of Services or Merchandise",
          //       notEmpty.and(hasValue("ZA"))).onSection("ABG"),
          //     failure("flw_lc4", 238, "Country where service was requested from or where merchandise was exported to may only be EU if the Category is 513",
          //       notEmpty.and(hasValue("EU"))).onOutflow().onSection("AB").notOnCategory("513"),
          //     failure("flw_lc5", 407, "May not be completed",
          //       notEmpty).onSection("CDEF")
          //   ]
          // },
          // //  {
          // //      field: "PaymentDetail.BeneficiaryBank.SWIFTBIC",
          // //      minLen: 8,
          // //      maxLen: 11,
          // //      rules: [
          // //       failure ('flw_swiftbic', "ISB1", "The first 6 characters of a SWIFTBIC must be uppercase letters", notEmpty.and(notPattern (/^(.[A-Z]{5})/)))
          // //      ]
          // //  },
          // // {
          // //     field: "PaymentDetail.BeneficiaryBank.BankName",
          // //     minLen: 2,
          // //     maxLen: 50,
          // //     rules: [
          // //         failure("flw_bbnm1", "I61", "Either Beneficiary bank name or SWIFTBIC must be provided",
          // //             isEmpty.and(notTransactionField("PaymentDetail.BeneficiaryBank.SWIFTBIC"))).onOutflow().onSection("A")
          // //     ]
          // // },
          // // {
          // //     field: "PaymentDetail.BeneficiaryBank.BranchCode",
          // //     minLen: 2,
          // //     maxLen: 30,
          // //     rules: [
          // //       failure("flw_bbbc1", "I61", "If Beneficiary Bank Name is provided, Beneficiary Bank Branch Code must be provided",
          // //         isEmpty.and(hasTransactionField("PaymentDetail.BeneficiaryBank.BankName"))).onOutflow().onSection("A")
          // //     ]
          // // },
          // // {
          // //     field: "PaymentDetail.BeneficiaryBank.Address",
          // //     minLen: 2,
          // //     maxLen: 200,
          // //     rules: [
          // //       failure("flw_bba1", "I61", "If Beneficiary Bank Name is provided, Beneficiary Bank Address must be provided",
          // //         isEmpty.and(hasTransactionField("PaymentDetail.BeneficiaryBank.BankName"))).onOutflow().onSection("A")
          // //     ]
          // // },
          // // {
          // //     field: "PaymentDetail.BeneficiaryBank.City",
          // //     minLen: 2,
          // //     maxLen: 100,
          // //     rules: [
          // //       failure("flw_bbc1", "I61", "If Beneficiary Bank Name is provided, Beneficiary Bank City must be provided",
          // //         isEmpty.and(hasTransactionField("PaymentDetail.BeneficiaryBank.BankName"))).onOutflow().onSection("A")
          // //     ]
          // // },
          // // {
          // //     field: "ReceivingCountry",
          // //     rules: [
          // //         failure("flw_bbc", "I62", "The country must be provided for the beneficiary bank",
          // //             isEmpty.and(hasTransactionField("PaymentDetail.BeneficiaryBank.SWIFTBIC").
          // //                 or(hasTransactionField("PaymentDetail.BeneficiaryBank.BankName")).
          // //                 or(hasTransactionField("PaymentDetail.BeneficiaryBank.BranchCode")).
          // //                 or(hasTransactionField("PaymentDetail.BeneficiaryBank.Address")).
          // //                 or(hasTransactionField("PaymentDetail.BeneficiaryBank.City")))).onOutflow().onSection("A")
          // //     ]
          // // },
          // // {
          // //     field: "CorrespondentCountry",
          // //     rules: [
          // //         failure("flw_cbc1", "I65", "The country must be provided for the beneficiary bank",
          // //             isEmpty.and(hasTransactionField("PaymentDetail.CorrespondentBank.SWIFTBIC").
          // //                 or(hasTransactionField("PaymentDetail.CorrespondentBank.BankName")).
          // //                 or(hasTransactionField("PaymentDetail.CorrespondentBank.BranchCode")).
          // //                 or(hasTransactionField("PaymentDetail.CorrespondentBank.Address")).
          // //                 or(hasTransactionField("PaymentDetail.CorrespondentBank.City")))).onOutflow().onSection("A")
          // //     ]
          // // },
          // {
          //   field: "NonResident.Individual.IsMutualParty",
          //   rules: [
          //     failure("flw_nrimp", "I38", "This must be completed",
          //       isEmpty.and(hasTransactionField("Resident.Individual"))).onInflow().onSection("A")
          //   ]
          // },
          // {
          //   field: "NonResident.Entity.IsMutualParty",
          //   rules: [
          //     failure("flw_nremp", "I39", "This must be completed",
          //       isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("A")
          //   ]
          // },
          // {
          //   field: "NonResident.Individual.Surname",
          //   rules: [
          //     ignore("nrsn2"),
          //     failure("flw_nrsn_l1", "I55", "Surname limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
          //   ]

          // },
          // // {
          // //     field: "NonResident.Individual.MiddleNames",
          // //     rules: [
          // //         failure("flw_nrmn_l1", "I55", "Middle Name is limited to 50 characters", notEmpty.and(isTooLong(50))).onSection("A")
          // //     ]
          // // },
          // {
          //   field: "NonResident.Individual.Name",
          //   rules: [
          //     failure("flw_nrnm1", "I17", "The Name must be completed",
          //       isEmpty).onSection("ABCDG"),
          //     failure("flw_nrnm_l1", "I55", "Name is limited to 50 characters", notEmpty.and(isTooLong(50))).onSection("A")
          //   ]
          // },
          // // {
          // //     field: "NonResident.Individual.Gender",
          // //     rules: [
          // //         failure("flw_nrgn1", "I18", "The Gender must be completed",
          // //             isEmpty).onSection("ABCDG")
          // //     ]
          // // },
          // {
          //   field: "NonResident.Entity.EntityName",
          //   rules: [
          //     ignore("nrlen3"),
          //     failure("flw_nrlen_l1", "I90", "Entity Name is limited to 70 characters", notEmpty.and(isTooLong(70))).onSection("A")
          //   ]
          // },
          {
            field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
            rules: [
              ignore("nriaid1"),
              ignore("nriaid4"),
              ignore("nriaid5"),
              ignore("nriaid6"),
              ignore("nriaid7"),
              ignore("nriaid9"),
              ignore("nriaid10"),
              ignore("nriaid11")
            ]
          },
          // {
          //   field: ["NonResident.Individual.AccountNumber", "NonResident.Entity.AccountNumber"],
          //   rules: [
          //     ignore("nrian1"),

          //     failure("flw_nrian1", "I??", "The beneficiary account number or IBAN is mandatory",
          //       isEmpty.and(notTransactionFieldValue("IsFCA", "Y")))
          //       .onSection("ABCDG")
          //       .onOutflow(),

          //     // warning("flw_nrian2", "I??", "This country requires an IBAN number. The provided number fails the IBAN validation check and may result in payment processing delays",
          //     //     notEmpty.and(isTransactionFieldIBANReqCountry("ReceivingCountry").and(notValidIBAN)))
          //     //     .onSection("ABCDG")
          //     //     .onOutflow(),

          //     // warning("flw_nrian3", "I??", "The provided IBAN number fails the IBAN validation check and may result in payment processing delays",
          //     //     notEmpty.and(notTransactionFieldIBANReqCountry("ReceivingCountry").and(isTransactionFieldIBANCountry("ReceivingCountry")).and(startsWithTransactionField("ReceivingCountry").and(notValidIBAN))))
          //     //     .onSection("ABCDG")
          //     //     .onOutflow(),

          //     failure("flw_nrian4", "I??", "Account number is limited to 34 characters",
          //       notEmpty.and(isTooLong(34)))
          //       .onSection("A")
          //       .onOutflow(),

          //     failure("flw_nrian5", "I??", "The beneficiary account number/IBAN is not required when paying an internal Foreign Currency account",
          //       notEmpty.and(hasTransactionFieldValue("IsFCA", "Y")))
          //       .onSection("A")
          //       .onOutflow()
          //   ]
          // },
          // {
          //   field: "NonResident.Individual.PassportNumber",
          //   rules: [
          //     ignore("nrpn1")
          //   ]
          // },
          // {
          //   field: "NonResident.Individual.PassportCountry",
          //   rules: [
          //     ignore("nrpc1")
          //   ]
          // },
          // // {
          // //     field: ["NonResident.Individual.Address.AddressLine1", "NonResident.Entity.Address.AddressLine1"],
          // //     rules: [
          // //         failure("flw_nrial11", "I??", "The AddressLine1 must be completed", isEmpty).onSection("A"),
          // //         failure("flw_nrial12", "I40", "Address is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
          // //     ]
          // // },
          // // {
          // //     field: ["NonResident.Individual.Address.AddressLine2", "NonResident.Entity.Address.AddressLine2"],
          // //     rules: [
          // //         failure("flw_nrial21", "I41", "Address is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
          // //     ]
          // // },
          // // {
          // //     field: ["NonResident.Individual.Address.Suburb", "NonResident.Entity.Address.Suburb"],
          // //     rules: [
          // //         failure("flw_nrial31", "I??", "The Suburb must be completed", isEmpty).onSection("A"),
          // //         failure("flw_nrial32", "I42", "Suburb is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
          // //     ]
          // // },
          // // {
          // //     field: ["NonResident.Individual.Address.City", "NonResident.Entity.Address.City"],
          // //     rules: [
          // //         failure("flw_nric1", "I??", "The City must be completed", isEmpty).onSection("A"),
          // //         failure("flw_nric2", "I43", "City is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
          // //     ]
          // // },
          // // {
          // //     field: ["NonResident.Individual.Address.State", "NonResident.Entity.Address.State"],
          // //     rules: [
          // //         failure("flw_nris1", "I??", "The State must be completed", isEmpty).onSection("A"),
          // //         failure("flw_nris2", "I44", "State is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
          // //     ]
          // // },
          // // {
          // //     field: ["NonResident.Individual.Address.PostalCode", "NonResident.Entity.Address.PostalCode"],
          // //     rules: [
          // //         failure("flw_nriz1", "I??", "The Code must be completed", isEmpty).onSection("A")
          // //     ]
          // // },
          // // {
          // //     field: "Resident",
          // //     rules: [
          // //         failure("rg1", 277, "Must contain one of either an Individual or Entity Applicant Type",
          // //             notTransactionField("Resident.Individual").and(notTransactionField("Resident.Entity"))).onSection("ABCEG"),
          // //         failure("rg2", 291, "If category 255 is used, an Entity Applicant Type must be completed",
          // //             notTransactionField("Resident.Entity")).onCategory("255").onSection("AB"),
          // //         failure("rg3", 293, "If category 256 is used, an Individual Applicant Type must be completed",
          // //             notTransactionField("Resident.Individual")).onCategory("256").onSection("AB"),
          // //         failure("rg4", 292, "If category 511/01 to 511/07 is specified an Entity Applicant Type may not be used",
          // //             hasTransactionField("Resident.Entity")).onCategory(["511"]).onSection("A"),
          // //         failure("rg6", 292, "If category 303, 304, 305, 306, 416 or 417 is used, an Entity Applicant Type may not be used",
          // //             hasTransactionField("Resident.Entity").or(hasTransactionField("Resident.Exception"))).onInflow().onCategory(["303", "304", "305", "306", "416", "417"]).onSection("AB")
          // //     ]
          // // },
          // {
          //   field: "Resident.Individual.Surname",
          //   rules: [
          //     failure("risn1", 254, "Surname is required", isEmpty).onSection("ABCEG"),
          //     ignore("risn2"),
          //     failure("flw_risn1", "I55", "Surname limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
          //   ]
          // },
          // {
          //   field: "Resident.Individual.Name",
          //   rules: [
          //     failure("rin1", 256, "Name is required", isEmpty.and(hasTransactionField("Resident.Individual"))).onSection("ABCEG"),
          //     failure("flw_rin1", "I56", "Name is limited to 50 characters", notEmpty.and(isTooLong(50))).onSection("A")
          //   ]
          // },
          // {
          //   field: "Resident.Individual.MiddleNames",
          //   rules: [
          //     failure("flw_rimn1", "I57", "Middle Name is limited to 50 characters", notEmpty.and(isTooLong(50))).onSection("A")
          //   ]
          // },
          // {
          //   field: "Resident.Individual.IDNumber",
          //   rules: [
          //     failure("riidn1", 298, "The ID Number must be completed",
          //       isEmpty).onSection("AB").onCategory(["511", "512", "513"]),
          //     failure("riidn3", 297, "ID number is not valid",
          //       notEmpty.and(notValidRSAID)).onSection("ABEG"),
          //     failure("riidn4", 294, "ID number is required for South African residents",
          //       isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "South African Resident"))).onSection("ABE"),
          //     failure("flw_riidn1", "I08", "ID number is only required for South African residents",
          //       notEmpty.and(notTransactionFieldValue("AccountHolderStatus", "South African Resident"))).onSection("ABE")
          //   ]
          // },
          // {
          //   field: "Resident.Individual.DateOfBirth",
          //   rules: [
          //     failure("ridob2", 215, "Must be in a valid date format: YYYY-MM-DD",
          //       notEmpty.and(notPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/))).onSection("ABEG"),
          //     failure("flw_ridob1", "I35", "Date of birth not valid",
          //       notEmpty.and(hasPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/)).and(isDaysInFuture(0))).onSection("ABEG")
          //   ]
          // },
          // {
          //   field: "Resident.Individual.TempResPermitNumber",
          //   rules: [
          //     failure("flw_ritrpn1", "I09", "Temporary resident permit number is only required for foreign temporary residents",
          //       notEmpty.and(notTransactionFieldValue("AccountHolderStatus", "Foreign Temporary Resident"))).onSection("AB"),
          //     failure("flw_ritrpn2", "I53", "Permit number limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A"),

          //     ignore("ritrpn3"),
          //     failure("flw_ritrpn3o", 294, "For a Foreign Temporary Resident the Temporary Residence Permit Number must be completed",
          //       isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "Foreign Temporary Resident")))
          //       .onSection("AB")
          //       .onOutflow()
          //       .notOnCategory(["511", "512", "513", "514", "515", "401"]),
          //     failure("flw_ritrpn3i", 294, "For a Foreign Temporary Resident the Temporary Residence Permit Number must be completed",
          //       isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "Foreign Temporary Resident")))
          //       .onSection("AB")
          //       .onInflow()
          //       .notOnCategory(["511", "512", "513", "514", "515"])
          //   ]
          // },
          // {
          //   field: "Resident.Individual.TempResExpiryDate",
          //   rules: [
          //     failure("flw_ritrpe1", "I21", "For a Foreign Temporary Resident the Temporary Residence Permit Expiry Date must be completed",
          //       isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "Foreign Temporary Resident"))).onSection("AB"),
          //     failure("flw_ritrpe2", "I22", "Only a Foreign Temporary Resident can provide a Temporary Residence Permit Expiry Date",
          //       notEmpty.and(notTransactionFieldValue("AccountHolderStatus", "Foreign Temporary Resident"))).onSection("AB"),
          //     failure("flw_ritrpe3", "I23", "Must be in a valid date format: YYYY-MM-DD",
          //       notEmpty.and(notDatePattern)).onSection("ABG"),
          //     failure("flw_ritrpe4", "I24", "This Temporary Resident Permit Number has expired",
          //       notEmpty.and(hasDatePattern).and(isDaysInPast(0))).onSection("ABG")
          //   ]
          // },
          // {
          //   field: "Resident.Individual.ForeignIDNumber",
          //   rules: [
          //     ignore("rifidn3"),
          //     ignore("rifidn4")
          //   ]
          // },
          // {
          //   field: "Resident.Individual.PassportNumber",
          //   rules: [
          //     failure("flw_ripn1", 204, "If the category is 250 or 251, the Passport Number must be completed",
          //       isEmpty).onSection("A").onCategory(["250", "251"]),
          //     failure("flw_ripn2", 294, "For a Non Resident Individual the Passport Number must be completed",
          //       isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "Non Resident")))
          //       .onSection("AB"),
          //     failure("flw_ripn3", "I10", "If category 256 is used, for a South African Resident Individual the Passport Number must be completed",
          //       isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "South African Resident")))
          //       .onSection("AB").onCategory("256"),
          //     failure("flw_ripn4", "I52", "This field is too long and must be shortened to 15 characters",
          //       notEmpty.and(isTooLong(15)))
          //       .onSection("A")
          //   ]
          // },
          // {
          //   field: "Resident.Individual.PassportCountry",
          //   rules: [
          //     failure("flw_ripc1", 204, "If the category is 250 or 251, the Passport Country must be completed",
          //       isEmpty).onSection("A").onCategory(["250", "251"]),
          //     failure("flw_ripc2", 294, "For a Non Resident Individual the Passport Country must be completed",
          //       isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "Non Resident")))
          //       .onSection("AB"),
          //     failure("flw_ripc3", "I10", "If category 256 is used, for a South African Resident Individual the Passport Country must be completed",
          //       isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "South African Resident")))
          //       .onSection("AB").onCategory("256")
          //   ]
          // },
          // {
          //   field: "Resident.Individual.PassportExpiryDate",
          //   rules: [
          //     failure("flw_ripe1", "I11", "If category 255 is used, Traveller Passport Expiry Date must be completed",
          //       notEmpty).onSection("AB").onCategory("255"),
          //     failure("flw_ripe2", "I12", "If the category is 250 or 251, the Passport Expiry must be completed",
          //       isEmpty).onSection("A").onCategory(["250", "251"]),
          //     failure("flw_ripe3", "I13", "For a Non Resident the Passport Expiry must be completed",
          //       isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "Non Resident"))).onSection("AB"),
          //     failure("flw_ripe4", "I14", "If category 256 is used, for a South African Resident the Passport Expiry must be completed",
          //       isEmpty.and(hasTransactionFieldValue("AccountHolderStatus", "South African Resident"))).onSection("AB").onCategory("256"),
          //     failure("flw_ripe5", "I20", "Must be in a valid date format: YYYY-MM-DD",
          //       notEmpty.and(notDatePattern)).onSection("ABG"),
          //     failure("flw_ripe6", "I21", "We are unable to proceed with your request as it appears your passport has expired. \nPlease contact your Private Banker or the 24/7 global Client Support Centre to update your passport details.",
          //       notEmpty.and(hasDatePattern).and(isDaysInPast(0))).onSection("ABG")
          //   ]
          // },
          // {
          //   field: "Resident.Entity.EntityName",
          //   rules: [
          //     failure("relen1", 304, "Required field for entities",
          //       isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("ABCE"),
          //     failure("flw_relen1", "I54", "Entity Name is limited to 70 characters", notEmpty.and(isTooLong(70))).onSection("A")
          //   ]
          // },
          // {
          //   field: "Resident.Entity.TradingName",
          //   rules: [
          //     ignore("retn1")
          //   ]
          // },
          // {
          //   field: "Resident.Entity.RegistrationNumber",
          //   rules: [
          //     failure("rern1", 306, "Required field for entities",
          //       isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("ABEG")
          //   ]
          // },
          // {
          //   field: "Resident.Entity.InstitutionalSector",
          //   rules: [
          //     ignore("reis1")
          //   ]
          // },
          // {
          //   field: "Resident.Entity.IndustrialClassification",
          //   rules: [
          //     ignore("reic1")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.AccountName", "Resident.Entity.AccountName"],
          //   rules: [
          //     ignore("an1")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
          //   rules: [
          //     ignore("accid1"),
          //     ignore("accid5"),
          //     ignore("accid6"),
          //     ignore("accid7")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.AccountNumber", "Resident.Entity.AccountNumber"],
          //   rules: [
          //     failure('flw_accno1', 279, "Must be completed",
          //       isEmpty).onInflow().onSection("ABCDG")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],

          //   rules: [
          //     ignore("ccn1"),
          //     ignore("ccn3"),
          //     failure('flw_ccn1', 320, 'Must be completed if category is 101/01 to 101/10, 103/01 to 103/10, 105 or 106',
          //       isEmpty)
          //       .onInflow().onSection("AB")
          //       .notOnCategory(['101/11', '103/11'])
          //       .onCategory(['101', '103', '105', '106']),
          //     failure('flw_ccn2', 322, 'CustomsClientNumber must be numeric and contain exactly 8 digits',
          //       notEmpty.and(notValidCCN))
          //       .onInflow().onSection("AB")
          //       .notOnCategory(['101/11', '103/11'])
          //       .onCategory(['101', '103', '105', '106']),
          //     ignore("ccn7"),
          //     ignore("ccn9")
          //     //message("Resident.Individual.CustomsClientNumber.minLen", null, "helloworld")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.TaxNumber", "Resident.Entity.TaxNumber"],
          //   rules: [
          //     failure("flw_tn1", "I45", "Tax number is limited to 30 characters", notEmpty.and(isTooLong(30))).onSection("A"),
          //     failure("flw_tn2", "I46", "Tax number can only contain numeric values", notEmpty.and(notPattern('^\\d+$'))).onSection("A")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.VATNumber", "Resident.Entity.VATNumber"],
          //   rules: [
          //     failure('vn1', 326, 'VAT number is required for the category chosen',
          //       isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("AB").notOnCategory(['101/11', '102/11', '103/11', '104/11']).onCategory(['101', '102', '103', '104', '105', '106'])
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.TaxClearanceCertificateIndicator", "Resident.Entity.TaxClearanceCertificateIndicator"],
          //   rules: [
          //     ignore('tcci1')
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
          //   rules: [
          //     failure('flw_tccr1', "I??", 'The Tax Clearance Certificate Reference number is not required',
          //       notEmpty.and(not(hasAnyMoneyFieldValue('TaxClearanceCertificateIndicator', 'Y')))).onSection("AB"),
          //     failure('flw_tccr2', "I??", 'Tax Clearance Certificate Reference number can only contain "/" and alphanumeric characters e.g. 0700/3/2016/A000033069',
          //       notEmpty.and(notPattern('^([A-Z0-9]+\\/{0,1}){0,}[A-Z0-9]+$')))
          //       .onSection("AB"),
          //     failure('flw_tccr3', "I??", 'The Tax Clearance Certificate Reference number is required',
          //       isEmpty.and(hasAnyMoneyFieldValue('TaxClearanceCertificateIndicator', 'Y'))).onSection("AB")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1"],
          //   rules: [
          //     ignore('a1_1'),
          //     ignore('a1_4')
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb"],
          //   rules: [
          //     ignore('s1'),
          //     ignore('s4')
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City"],
          //   rules: [
          //     ignore('c1'),
          //     ignore('c4')
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1"],
          //   rules: [
          //     failure('flw_a1_1', 332, 'Must be completed', isEmpty).onSection("ABEG"),
          //     failure("flw_a1_2", "I47", "Address is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A"),
          //     failure('flw_a1_3', 332, 'Must be completed', isEmpty).onInflow().onSection("A"),
          //     failure("flw_a1_4", 332, 'Must be completed (unless REMITTANCE DISPENSATION)',
          //       isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.StreetAddress.AddressLine2", "Resident.Entity.StreetAddress.AddressLine2"],
          //   rules: [
          //     failure("flw_a2_1", "I48", "Address is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb"],
          //   rules: [
          //     failure('flw_s1', 333, 'Must be completed',
          //       isEmpty.and(hasResidentFieldValue("StreetAddress.Country", "ZA"))).onSection("ABEG"),
          //     failure("flw_s2", "I49", "Suburb is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A"),
          //     failure('flw_s4', 333, 'Must be completed (unless REMITTANCE DISPENSATION)',
          //       isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City"],
          //   rules: [
          //     failure('flw_c1', 334, 'Must be completed', isEmpty).onSection("ABEG"),
          //     failure("flw_c2", "I50", "City is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A"),
          //     failure('flw_c4', 334, 'Must be completed (unless REMITTANCE DISPENSATION)',
          //       isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A")
          //   ]
          // },
          // {
          //   field: "Resident.Entity.StreetAddress.Province",
          //   rules: [
          //     failure('flw_resap1', 336, 'Must be valid South African province',
          //       notEmpty
          //         .and(notValueIn(["GAUTENG", "LIMPOPO", "NORTH WEST", "WESTERN CAPE", "EASTERN CAPE", "NORTHERN CAPE", "FREE STATE", "MPUMALANGA", "KWAZULU NATAL"])))
          //       .onSection("ABG"),
          //     failure('flw_p10', 336, 'Must be valid South African province',
          //       isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))
          //         .and(hasTransactionFieldValue("Resident.Entity.StreetAddress.Country", "ZA"))
          //     ).onOutflow().onSection("AB")
          //   ]
          // },
          // {
          //   field: "Resident.Individual.StreetAddress.Province",
          //   rules: [
          //     failure('flw_risap1', 336, 'Must be valid South African province',
          //       notEmpty
          //         .and(notValueIn(["GAUTENG", "LIMPOPO", "NORTH WEST", "WESTERN CAPE", "EASTERN CAPE", "NORTHERN CAPE", "FREE STATE", "MPUMALANGA", "KWAZULU NATAL"])))
          //       .onSection("ABG"),
          //     failure('flw_p10', 336, 'Must be valid South African province',
          //       isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))
          //         .and(hasTransactionFieldValue("Resident.Individual.StreetAddress.Country", "ZA"))
          //     ).onOutflow().onSection("AB")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province"],
          //   rules: [
          //     ignore('p1'),
          //     ignore('p8'),
          //     ignore('p9'),
          //     ignore('p10'),
          //     failure('flw_p2', "I34", 'Must be completed',
          //       isEmpty).onSection("ABG"),
          //     failure("flw_p3", "I51", "Province is limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.StreetAddress.PostalCode", "Resident.Entity.StreetAddress.PostalCode"],
          //   rules: [
          //     failure('spc1', 338, 'Invalid postal code',
          //       notEmpty.and(notPattern(/^([0-9]){4}$/)).and(hasResidentFieldValue("StreetAddress.Country", "ZA"))).onSection("ABEG"),
          //     failure('flw_spc1', "I33", 'Must be completed',
          //       isEmpty.and(hasResidentFieldValue("StreetAddress.Country", "ZA"))).onSection("ABEG"),
          //     failure("flw_spc_l1", "I51.1", "Postal code is limited to 10 characters",
          //       notEmpty.and(isTooLong(10))).onSection("A")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1"],
          //   rules: [
          //     ignore('a1_1'),
          //     ignore('a1_3'),
          //     ignore('a1_4')
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.PostalAddress.Suburb", "Resident.Entity.PostalAddress.Suburb"],
          //   rules: [
          //     ignore('s1'),
          //     ignore('s3'),
          //     ignore('s4')
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City"],
          //   rules: [
          //     ignore('c1'),
          //     ignore('c3'),
          //     ignore('c4')
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.PostalAddress.PostalCode", "Resident.Entity.PostalAddress.PostalCode"],
          //   rules: [
          //     ignore('pc2'),
          //     ignore('pc6'),
          //     ignore('pc7')
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.StreetAddress.Country", "Resident.Entity.StreetAddress.Country"],
          //   rules: [
          //     failure('flw_rsac1', "I06", 'Must be completed',
          //       isEmpty).onSection("ABG"),
          //     failure('flw_rsac2', "I??", 'Please accept the address mandate or provide a South African address',
          //       notEmpty.and(notValue("ZA").and(notResidentFieldValue("StreetAddress.Mandate", "ACCEPT")).
          //         and(hasTransactionFieldValue("AccountHolderStatus", ["South African Resident", "Foreign Temporary Resident"])))).onSection("ABG")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.StreetAddress.Mandate", "Resident.Entity.StreetAddress.Mandate"],
          //   rules: [
          //     failure('flw_rsam1', "I??", 'Must be completed',
          //       isEmpty.and(hasResidentField("StreetAddress.Country")).and(notResidentFieldValue("StreetAddress.Country", "ZA")).
          //         and(hasTransactionFieldValue("AccountHolderStatus", ["South African Resident", "Foreign Temporary Resident"]))).onSection("ABG"),
          //     failure('flw_rsam2', "I??", 'You must either accept the use of the Bank\'s address or provide a valid South African address',
          //       notEmpty.and(notValue("ACCEPT")).and(notResidentFieldValue("StreetAddress.Country", "ZA")).
          //         and(hasTransactionFieldValue("AccountHolderStatus", ["South African Resident", "Foreign Temporary Resident"]))).onSection("ABG"),
          //     failure('flw_rsam3', "I??", 'Must not be provided for a Resident South African address or Non Resident addresses',
          //       notEmpty.and(hasResidentFieldValue("StreetAddress.Country", "ZA").or(hasTransactionFieldValue("AccountHolderStatus", ["Non Resident"])))).onSection("ABG")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.ContactDetails.ContactSurname", "Resident.Entity.ContactDetails.ContactSurname"],
          //   rules: [
          //     failure("flw_csn1", "I58", "Surname limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.ContactDetails.ContactName", "Resident.Entity.ContactDetails.ContactName"],
          //   rules: [
          //     failure("flw_cn1", "I59", "Name is limited to 50 characters", notEmpty.and(isTooLong(50))).onSection("A")
          //   ]
          // },
          // {
          //   field: ["Resident.Individual.ContactDetails.Email", "Resident.Entity.ContactDetails.Email"],
          //   rules: [
          //     ignore('cnte3'),
          //     failure('flw_cnte1', "I34", 'Invalid email address',
          //       notEmpty.and(notValidEmail)).onSection("ABEG")
          //   ]
          // }
        ]
      }
    };

    return transaction;
  }

});

