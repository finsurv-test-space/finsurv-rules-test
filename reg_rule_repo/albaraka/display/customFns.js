define(function (require) {
    return function (app, config) {

        var superInit = function () { };

        if (config.initializationFn) superInit = config.initializationFn;

        var _config = {
            initializationFn: function (model, scope) {
                superInit(model, scope);
                //custom scope functions go here...
                scope.updateRQ = function(model,value){
                  app.setData(app.getData());
                }

                scope.updateFlow = function(){
                  app.setData(app.getData());
                }

                scope.totalRandValue = function () {
                    var money = scope.data.MonetaryAmount;
                    var total = _.reduce(money, function (memo, itm) {
                        memo += itm[scope.map('LocalValue')].val ? Number(itm[scope.map('LocalValue')].val) : 0;
                        return memo;
                    }, 0)
                    return total.toFixed(2);
                }

                scope.randOutstanding = function (data, overaCopy, underaCopy) {
                    var total = scope.customData.TotalRandAmount - scope.totalRandValue();
                    if (total) return total.toFixed(2);
                }

                scope.updateBranches = function (model, selectedOption) {
                    var searchItem = model.val;
                    var branch = _.find(scope.getLookups().branches, function (item) {
                        return !!_.find(item, function (val) {
                            return val == searchItem;
                        })
                    });
                    if (branch) {
                        model.data.BranchCode = branch.code;
                        model.data.BranchName = branch.name;
                        model.data.HubCode = branch.hubCode;
                        model.data.HubName = branch.hubName;
                        scope.revalidate();
                    }
                }


                scope.checkThirdPartyPostalAddExist = function (ThirdParty) {

                    if (ThirdParty.PostalAddress.AddressLine1.val != undefined) {
                        if (ThirdParty.sameAsPhysical.val == true) {
                            var r = confirm("Warning! Overwrite Postal Address data?");
                            if (r == false) {
                                ThirdParty.sameAsPhysical.val = false;
                            }
                        } else {
                            delete ThirdParty.data.ThirdParty.PostalAddress;
                        }
                    }
                    scope.revalidate();

                };


                scope.clearThirdParty = function (ThirdParty) {
                    delete ThirdParty.data.ThirdParty;
                    delete ThirdParty.data.ThirdPartyKind;
                    scope.revalidate();
                };

                var ThirdPartyCache = {};

                // This is used to set the 3rd Party type - Maintains a cache per monetaryAmount
                scope.setAdhoc3rdPartyType = function (_model, selectedOption) {

                    var moneyIndex = _model.getIndeces()[0];
                    var currentType = _model.data.ThirdParty ? (_model.data.ThirdParty.Individual ? "Individual" :
                        (_model.data.ThirdParty.Entity ? "Entity" : undefined)) : undefined;


                    var cache = ThirdPartyCache[moneyIndex] ?
                        (ThirdPartyCache[moneyIndex]) :
                        (ThirdPartyCache[moneyIndex] = {});

                    if (currentType) {
                        cache.core = _model.data.ThirdParty;
                        cache[currentType] = cache[currentType] ? cache[currentType] :
                            cache[currentType] = _model.data.ThirdParty[currentType];
                    };

                    if (selectedOption == undefined) {
                        delete _model.data.ThirdParty;
                        delete _model.data.ThirdPartyKind;
                    } else {
                        _model.data.ThirdParty = cache.core ? cache.core : {};
                        delete _model.data.ThirdParty.Individual;
                        delete _model.data.ThirdParty.Entity;
                        _model.data.ThirdParty[selectedOption.value] = cache[selectedOption.value] ?
                            cache[selectedOption.value] :
                            cache[selectedOption.value] = {};
                    }
                }

                scope.updateThirdPartyCIF = function(_scope) {
                    
                    // "_scope" is curently not being passed to this method, needs to be investigated,
                    // current page scope is working fine in current scenarious
                    if (scope.current && scope.current.money) {

                        //CIF number from ThirdParty details tab
                        var params = {
                            cif : scope.current.money.ThirdPartyCIF
                        };

                        if (window["thirdPartyApiCall"]){
                            window["thirdPartyApiCall"](params, function(status, code, message){
                                scope.current.money.ciffeedback = undefined;
                                    if(code == "200"){
                                        if(message.hasOwnProperty('ThirdPartyKind')){
                                            scope.mapCIFThirdPartyDetails(scope.current.money, message);
                                        }
                                        else{
                                            scope.current.money.ciffeedback = "No thirdparty information associated with the provided cif number."
                                        }
                                    }
                                    else if(code == "500"){
                                        scope.current.money.ciffeedback = "A technical fault prevents the retrieval of third party details."
                                    }
                                    else{
                                        scope.current.money.ciffeedback = "Cannot currently retrieve the thirdparty details."
                                    }
                            });              
                        } else {
                            console.log("'thirdPartyApiCall' function is not defined or accessible.");
                        }
                    } 
                }

                function set(scope, path, value) {
                    //Splits up the dot separated path and uses it to set the
                    //property at the end of that 'tree' if available
                    var dataset = scope;
                    var pathArray = path.split('.');
                    var len = pathArray.length;
                    for(var i = 0; i < len-1; i++) {
                        var elem = pathArray[i];
                        if( !dataset[elem] ) dataset[elem] = {}
                        dataset = dataset[elem];
                    }

                    var objectReference = dataset[pathArray[len-1]];

                    if(objectReference !== undefined){
                        objectReference.val = value;
                    }
                }

                function getFlatObject(object) {
                    function iter(o, p) {

                        var path = {};

                        //support for arrays, not really needed at this point
                        if (Array.isArray(o) ){
                            o.forEach(function (a, i) {
                                iter(a, p.concat(i));
                            });
                            return;
                        }
                        if (o !== null && typeof o === 'object') {
                            Object.keys(o).forEach(function (k) {
                                iter(o[k], p.concat(k));
                            });
                            return;
                        }
                        
                        //glue each property found together with a dot
                        //and push it to the array
                        path[p.join('.')] = o;
                        pathMap.push(path);
                    }
                
                    var pathMap = [];
                    
                    iter(object, []);
                    return pathMap;
                }
                
                scope.mapCIFThirdPartyDetails = function(_current, cifThirdPartyDetails) {
                    getFlatObject(cifThirdPartyDetails)
                        .forEach(function (path) {
                            var lookupKey = Object.keys(path)[0];
                            var lookupValue = path[lookupKey];

                            if(lookupKey === 'ThirdPartyKind'){
                                scope.setAdhoc3rdPartyType(_current.ThirdParty, lookupKey);
                            }

                            //set finction takes the model, the path to the property you want to
                            //modify as dot notation and the value you want to set it to
                            set(_current, lookupKey, lookupValue);
                    });
                       
                    scope.revalidate();
                }
            }
        }

        Object.assign(config,_config);

        return config;

    }
})