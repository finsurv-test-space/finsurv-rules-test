define(function () {
  return function (predef) {
    var detailTrans, detailMoney, detailImportExport;
    with (predef) {

      detailTrans = {
        ruleset: "Reporting Transaction Display Rules",
        scope: "transaction",
        fields: [
          {
           field  : 'TrnReference',
           display: [
             disable()
            ]
          },
          {
            field: "TotalForeignValue",
            display: [
              disable()
            ]
          },
          {
            field: "ReportingQualifier",
            display: [
              disable()
            ]
          },
          {
            field: "Flow",
            display: [
             disable()
            ]
          },
          {
            field: "ValueDate",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "FlowCurrency",
            display: [
              show(),
              disable()
            ]
          },
        {
          field  : "TotalValue",
          display: [
            hide(),
            show(isDefined),
            disable()
          ]
        },
          {
            field: "BranchCode",
            display: [
            show(),
            disable()
          ]
          },
          {
            field: "BranchName",
            display: [
            show(),
            disable()
          ]
          },
          {
            field: "HubCode",
            display: [
            show(),
            disable()
          ]
          },
          {
            field: "HubName",
            display: [
            show(),
            disable()
          ]
          },
          {
            field: "ReplacementTransaction",
            display: [
            show()
          ]
          },
          {
            field: "ReplacementOriginalReference",
            display: [
            show()
          ]
          },
          {
            field: "OriginatingBank",
            display: [
            hide(),
              show().onSection("ABCDG"),
              show(notNonResidentFieldValue("AccountIdentifier", "CASH")).onInflow().onSection("ABCDG"),
              show(hasTransactionField("OriginatingCountry")).onInflow().onSection("ABCDG"),
              disable()
            ]
          },
          {
            field: "OriginatingCountry",
            display: [
            hide(),
              show().onSection("ABCDG"),
              show(notNonResidentFieldValue("AccountIdentifier", "CASH")).onInflow().onSection("ABCDG"),
              show(hasTransactionField("OriginatingBank")).onInflow().onSection("ABCDG"),
              disable()
            ]
          },
          {
            field: "CorrespondentBank",
            display: [
              show().onSection("ABCDG"),
              hide().onSection("EF"),
              disable()
            ]
          },
          {
            field: "CorrespondentCountry",
            display: [
              show().onSection("ABCDG"),
              hide().onSection("EF"),
              disable()
            ]
          },
          {
            field: "ReceivingBank",
            display: [
            hide().onSection("EF"),
              hide(hasResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["250", "251"]),
              hide(hasNonResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["255", "256"]),
            disable()
          ]
          },
          {
            field: "ReceivingCountry",
            display: [
            hide().onSection("EF"),
              hide(hasResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["250", "251"]),
              hide(hasNonResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["255", "256"]),
              show(hasTransactionField("ReceivingBank")).onOutflow().onSection("ABCDG"),
              disable()
            ]
          },
          {
            field: "NonResident",
            display: [
              setValue(undefined).onSection("F")
            ]
          },
          {
            field: "Resident",
            display: [
              setValue(undefined).onSection("F")
            ]
          },
          {
            field: "NonResident.Individual.Surname",
            display: [
              show().onSection("ABCDG")
            ]
          },
          {
            field: "NonResident.Individual.Name",
            display: [
              show().onSection("ABCDG")
            ]
          },
          {
            field: "NonResident.Individual.Gender",
            display: [
              show().onSection("ABCDG")
            ]
          },
          {
            field: "NonResident.Individual.PassportNumber",
            display: [
              show().onSection("AB").onCategory(["250", "251"]),
              hide().onSection("CEF")
            ]
          },
          {
            field: "NonResident.Individual.PassportCountry",
            display: [
              show().onSection("AB").onCategory(["250", "251"]),
              hide().onSection("CEF")
            ]
          },
          {
            field: "NonResident.Entity.EntityName",
            display: [
              show().onSection("ABCDG"),
              hide().onSection("E")
            ]
          },
          {
            field: "NonResident.Entity.CardMerchantName",
            display: [
              show().onSection("E"),
              hide().onSection("ABCDG"),
              disable()
            ]
          },
          {
            field: "NonResident.Entity.CardMerchantCode",
            display: [
              show().onSection("E"),
              hide().onSection("ABCDG"),
              disable()
            ]
          },
          {
            field: "NonResident.Exception",
            display: [
              hide(hasTransactionField("Resident.Exception")).onSection("A"),
              hide().onSection("BEG")
            ]
          },
          {
            field: "NonResident.Exception.ExceptionName",
            display: [
              hide(hasTransactionField("Resident.Exception")).onSection("A"),
              hide().onSection("BEG"),
              setValue('MUTUAL PARTY', null, isEmpty).onSection('A').onInflow().onCategory("252")
            ]
          },
          {
            field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
            display: [
              setValue("NON RESIDENT RAND", null, isEmpty).onSection("B")
            ]
          },
          {
            field: ["NonResident.Individual.AccountNumber", "NonResident.Entity.AccountNumber"],
            display: [
              show()
            ]
          },
          {
            field: [

              "NonResident.Individual.Address.AddressLine1",
              "NonResident.Individual.Address.AddressLine2",
              "NonResident.Individual.Address.Suburb",
              "NonResident.Individual.Address.City",
              "NonResident.Individual.Address.State",
              "NonResident.Individual.Address.PostalCode",
              "NonResident.Entity.Address.AddressLine1",
              "NonResident.Entity.Address.AddressLine2",
              "NonResident.Entity.Address.Suburb",
              "NonResident.Entity.Address.City",
              "NonResident.Entity.Address.State",
              "NonResident.Entity.Address.PostalCode"
            ],
            display: [
              hide().onSection("E")
            ]
          },
          {
            field: ["NonResident.Individual.Address.Country", "NonResident.Entity.Address.Country"],
            display: [
              show().onSection("ABCDEG")
            ]
          },
          {
            field: "Resident.Individual.Surname",
            display: [
              show().onSection("ABCEG"),
              disable()
            ]
          },
          {
            field: "Resident.Individual.Name",
            display: [
              show().onSection("ABCEG"),
              disable()
            ]
          },
          {
            field: "Resident.Individual.Gender",
            display: [
              show().onSection("ABEG"),
              disable()
            ]
          },
          {
            field: "Resident.Individual.DateOfBirth",
            display: [
              show().onSection("ABEG"),
              disable()
            ]
          },
          {
            field: "Resident.Individual.IDNumber",
            display: [
              show().onSection("ABEG"),
              disable()
            ]
          },
          {
            field: "Resident.Individual.BeneficiaryID1",
            display: [
              hide()
            ]
          },
          {
            field: "Resident.Individual.BeneficiaryID2",
            display: [
              hide()
            ]
          },
          {
            field: "Resident.Individual.BeneficiaryID3",
            display: [
              hide()
            ]
          },
          {
            field: "Resident.Individual.BeneficiaryID4",
            display: [
              hide()
            ]
          },
          {
            field: "Resident.Individual.TempResPermitNumber",
            display: [
              show().onSection("ABEG"),
            hide().onSection("AB").onCategory(["511", "512", "513", "514", "515"]),
              hide().onOutflow().onSection("AB").onCategory("401"),
              disable()
            ]
          },
          {
            field: "Resident.Individual.ForeignIDNumber",
            display: [
              show().onSection("ABEG"),
            hide().onSection("AB").onCategory(["511", "512", "513", "514", "515"]),
              hide().onOutflow().onSection("AB").onCategory("401"),
              disable()
            ]
          },
          {
            field: "Resident.Individual.ForeignIDCountry",
            display: [
              show().onSection("ABEG"),
            hide().onSection("AB").onCategory(["511", "512", "513", "514", "515"]),
              hide().onOutflow().onSection("AB").onCategory("401"),
              disable()
            ]
          },
          {
            field: "Resident.Individual.PassportNumber",
            display: [
              show().onSection("AB"),
              hide().onSection("CDEFG"),
              hide().onSection("AB").onCategory("255"),
              // disable()
            ]
          },
          {
            field: "Resident.Individual.PassportCountry",
            display: [
              show().onSection("AB"),
              hide().onSection("CDEFG"),
              hide().onSection("AB").onCategory("255"),
              // disable()
            ]
          },
          {
            field: "Resident.Entity.EntityName",
            display: [
              hide().onSection("F"),
              disable()
            ]
          },
          {
            field: "Resident.Entity.TradingName",
            display: [
              hide().onSection("F"),
              disable()
            ]
          },
          {
            field: "Resident.Entity.RegistrationNumber",
            display: [
              hide().onSection("F"),
              disable()
            ]
          },
          {
            field: "Resident.Entity.InstitutionalSector",
            display: [
              hide().onSection("F"),
              disable()
            ]
          },
          {
            field: "Resident.Entity.IndustrialClassification",
            display: [
              hide().onSection("F"),
              disable()
            ]
          },
          {
            field: "Resident.Exception",
            display: [
              hide().onSection("BEF")
            ]
          },
          {
            field: "Resident.Exception.ExceptionName",
            display: [
              hide().onSection("BEF")
            ]
          },
          {
            field: "Resident.Exception.Country",
            display: [
              hide().onSection("ABEFG")
            ]
          },
          {
            field: ["Resident.Individual", "Resident.Entity"],
            display: [
              show(),
            ]
          },
          {
            field: ["Resident.Individual.AccountName", "Resident.Entity.AccountName"],
            display: [
              show(),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
            display: [
              hide().onSection("F")
            ]
          },
          {
            field: ["Resident.Individual.AccountNumber", "Resident.Entity.AccountNumber"],
            display: [
              hide().onSection("F")
            ]
          },
          {
            field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],
            display: [
              hide(),
              show().onInflow().onSection("AB").onCategory(["101", "103", "105", "106"]),
              show().onOutflow().onSection("AB").onCategory(["101", "102", "103", "104", "105", "106"]),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.TaxNumber", "Resident.Entity.TaxNumber"],
            display: [
              hide().onSection("F"),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.VATNumber", "Resident.Entity.VATNumber"],
            display: [
              hide().onSection("F"),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.TaxClearanceCertificateIndicator", "Resident.Entity.TaxClearanceCertificateIndicator"],
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(['512', '513'])
            ]
          },
          {
            field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
            display: [
              hide(),
            show().onOutflow().onSection("AB").onCategory(['512', '513']),
            show(hasResidentFieldValue('TaxClearanceCertificateIndicator', 'Y')).onSection("AB")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1", "Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1"],
            display: [
              hide().onSection("DF"),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.AddressLine2", "Resident.Entity.StreetAddress.AddressLine2", "Resident.Individual.PostalAddress.AddressLine2", "Resident.Entity.PostalAddress.AddressLine2"],
            display: [
              hide().onSection("DF"),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb", "Resident.Individual.PostalAddress.Suburb", "Resident.Entity.PostalAddress.Suburb"],
            display: [
              hide().onSection("DF"),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City", "Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City"],
            display: [
              hide().onSection("DF"),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province", "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
            display: [
              hide().onSection("DF"),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.PostalCode", "Resident.Entity.StreetAddress.PostalCode"],
            display: [
              hide().onSection("DF"),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.PostalAddress.PostalCode", "Resident.Entity.PostalAddress.PostalCode"],
            display: [
              hide().onSection("DF"),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.ContactSurname", "Resident.Entity.ContactDetails.ContactSurname", "Resident.Individual.ContactDetails.ContactName", "Resident.Entity.ContactDetails.ContactName"],
            display: [
              hide().onSection("DF"),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.Email", "Resident.Entity.ContactDetails.Email"],
            display: [
              hide().onSection("DF"),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.Fax",
              "Resident.Entity.ContactDetails.Fax",
              "Resident.Individual.ContactDetails.Telephone",
              "Resident.Entity.ContactDetails.Telephone"],
            display: [
              hide().onSection("DF"),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.CardNumber", "Resident.Entity.CardNumber"],
            display: [
              hide().onSection("ABCDFG"),
              disable()
            ]
          },
          {
            field: ["Resident.Individual.SupplementaryCardIndicator", "Resident.Entity.SupplementaryCardIndicator"],
            display: [
              hide().onSection("ABCDFG"),
              disable()
            ]
          }
        ]
      };

      detailMoney = {
        ruleset: "Reporting Money Display Rules",
        scope: "money",
        fields: [
          {
            field: "SequenceNumber",
            display: [
              show(),
              setValue(function(context, moneyInd){
                return Number(moneyInd)+1;
              }, isEmpty)
            ]
          },
          {
            field: "MoneyTransferAgentIndicator",
            display: [
              show(),
              setValue('AD', null, isEmpty)
            ]
          },
          {
            field: "{{LocalValue}}",
            display: [
            show().onSection("ABCDEG"),
            disable(isCurrencyIn(map('LocalCurrency'))).onSection("ABCDEG"),
            setValue('%s','ForeignValue',isCurrencyIn(map('LocalCurrency'))).onSection("ABCDEG"),
            hide().onSection("F"),
            setValue(function (context, ind) {
              var exchangeRatio = Number(context.getCustomValue("TotalRandAmount")) /
                Number(context.getTransactionField("TotalForeignValue"));
              var result = Number(context.getMoneyField(ind, "ForeignValue")) * exchangeRatio;
              return result.toFixed(2);
            }, hasCustomField('TotalRandAmount').and(notCurrencyIn(["ZAR"]))),
            setValue(function (context, ind) {
              var result = Number(context.getMoneyField(ind, "ForeignValue"));
              return result.toFixed(2);
            }, hasCustomField('TotalRandAmount').and(isCurrencyIn(["ZAR"])))
            ]
          },
        {
         field  : "DomesticCurrencyCode",
         display: [
           hide(),
           setValue(map('LocalCurrency')).onSection('ABCDEG'),
           setValue(undefined).onSection('F')
         ]
        },
        {
          field: "CompoundCategoryCode",
          display: [
            hide().onSection("DEF")
          ]
        },
          {
            field: "CategoryCode",
            display: [
              hide().onSection("DEF")
            ]
          },
          {
            field: "CategorySubCode",
            display: [
              hide().onSection("DEF")
            ]
          },
          {
            field: "SWIFTDetails",
            display: [
              show()
            ]
          },
          {
            field: "StrateRefNumber",
            display: [
              hide(),
              show(hasResException("STRATE")).onSection("AB"),
              show().onSection("AB").onCategory(["601/01", "603/01"])
            ]
          },
          {
            field: "Travel",
            display: [
              hide()
            ]
          },
          {
            field: "Travel.Surname",
            display: [
              hide()
            ]
          },
          {
            field: "Travel.Name",
            display: [
              hide()
            ]
          },
          {
            field: "Travel.IDNumber",
            display: [
              hide()
            ]
          },
          {
            field: "Travel.DateOfBirth",
            display: [
              hide()
            ]
          },
          {
            field: "Travel.TempResPermitNumber",
            display: [
              hide()
            ]
          },
          {
            field: "LoanRefNumber",
            display: [
              hide(),
              show().onSection("AB").onCategory(["801", "802", "803", "804"]),
              show().onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              setValue("99012301230123", null,
                isEmpty.and(hasMoneyFieldValue("LocationCountry", "LS"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
              setValue("99456745674567", null,
                isEmpty.and(hasMoneyFieldValue("LocationCountry", "SZ"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
              setValue("99789078907890", null,
                isEmpty.and(hasMoneyFieldValue("LocationCountry", "NA"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
              setValue("99012301230123", null,
                isEmpty.and(hasMoneyFieldValue("LocationCountry", "LS"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              setValue("99456745674567", null,
                isEmpty.and(hasMoneyFieldValue("LocationCountry", "SZ"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              setValue("99789078907890", null,
                isEmpty.and(hasMoneyFieldValue("LocationCountry", "NA"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"])
            ]
          },
          {
            field: "LoanTenor",
            display: [
            hide(),
            show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"])
            ]
          },
          {
            field: "LoanInterestRate",
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
              show().onOutflow().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
              show().onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"])
            ]
          },
          {
            field: "{{Regulator}}Auth.RulingsSection",
            display: [
              hide().onSection("CDEF"),
            hide().onSection("ABG").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"])
            ]
          },
          {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
            display: [
              hide().onSection("CDEF"),
              hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              show(hasMoneyField(map("{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate"))).onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"])
            ]
          },
          {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
            display: [
              hide().onSection("CDEF"),
              hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              show(hasMoneyField(map("{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber"))).onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"])
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
            display: [
              hide().onSection("CDEF"),
              hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"])
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
            display: [
              hide().onSection("CDEF"),
              hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"])
            ]
          },
          {
            field: "CannotCategorize",
            display: [
              hide(),
              show().onSection("AB").onCategory("830")
            ]
          },
          {
            field: "AdHocRequirement.Subject",
            display: [
              hide().onSection("CDEF")
            ]
          },
          {
            field: "AdHocRequirement.Description",
            display: [
              hide().onSection("CDEF")
            ]
          },
          {
            field: "LocationCountry",
            display: [
              hide().onSection("CDE")
            ]
          },
          {
            field: "ReversalTrnRefNumber",
            display: [
              hide(),
              show().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              show(hasMoneyFieldValue("CardChargeBack", "Y")).onSection("E")
            ]
          },
          {
            field: "ReversalTrnSeqNumber",
            display: [
              hide(),
              show().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              show(hasMoneyField('ReversalTrnRefNumber')).onSection("E")
            ]
          },
          {
            field: "BOPDIRTrnReference",
            display: [
              hide().onSection("ABCDEF")
            ]
          },
          {
            field: "BOPDIR{{DealerPrefix}}Code",
            display: [
              hide().onSection("ABCDEF")
            ]
          },
          {
            field: "ThirdPartyAddrCopy",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdPartyKind",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.Individual.Surname",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.Individual.Name",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.Individual.Gender",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.Individual.IDNumber",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.Individual.DateOfBirth",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.Individual.TempResPermitNumber",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.Individual.PassportNumber",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.Individual.PassportCountry",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.Entity.Name",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.Entity.RegistrationNumber",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.CustomsClientNumber",
            display: [
              disable(),
              hide().onSection("CDEF")
            ]
          },
          {
            field: "ThirdParty.TaxNumber",
            display: [
              disable(),
              hide().onSection("CDEF")
            ]
          },
          {
            field: "ThirdParty.VATNumber",
            display: [
              disable(),
              hide().onSection("CDEF")
            ]
          },
          {
            field: "ThirdParty.StreetAddress.AddressLine1",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.StreetAddress.AddressLine2",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.StreetAddress.Suburb",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.StreetAddress.City",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.StreetAddress.Province",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.StreetAddress.PostalCode",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.PostalAddress.AddressLine1",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.PostalAddress.AddressLine2",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.PostalAddress.Suburb",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.PostalAddress.City",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.PostalAddress.Province",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.PostalAddress.PostalCode",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.ContactDetails.ContactSurname",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.ContactDetails.ContactName",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.ContactDetails.Email",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.ContactDetails.Fax",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "ThirdParty.ContactDetails.Telephone",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "CardChargeBack",
            display: [
              hide().onSection("ABCDFG")
            ]
          },
          {
            field: "CardIndicator",
            display: [
              hide().onSection("ABCDG")
            ]
          },
          {
            field: "ElectronicCommerceIndicator",
            display: [
              hide().onSection("ABCDFG")
            ]
          },
          {
            field: "POSEntryMode",
            display: [
              hide().onSection("ABCDFG")
            ]
          },
          {
            field: "CardFraudulentTransactionIndicator",
            display: [
              hide().onSection("ABCDFG")
            ]
          },
          {
            field: "ForeignCardHoldersPurchases{{LocalValue}}",
            display: [
              hide().onSection("ABCDEG")
            ]
          },
          {
            field: "ForeignCardHoldersCashWithdrawals{{LocalValue}}",
            display: [
              hide().onSection("ABCDEG")
            ]
          },
          {
            field: "ImportExport",
            display: [
            hide(emptyImportExport),
              show().onSection("AB").onCategory(["101", "103", "105", "106"])
            ]
          },
          {
            field: "IECurrencyCode",
            display: [
              setValue('%s','transaction::FlowCurrency',isEmpty)
            ]
          },
          // The four heading fields below don't actually exist as fields but rather are used
          //  for checking if the headings in the import/export table should be displayed or hidden.
          {
            field: "IE_ICNHeading",
            display: [
              hide(),
              show().onOutflow().onSection("ABG").onCategory(["101", "103", "105", "106"]),
              setLabel("Import Control Number (or MRN)"),
              setLabel("Invoice Number (or MRN)").onOutflow().onSection("ABG").onCategory("101"),
              setLabel("Movement Reference Number (MRN)").onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
            ]
          },
          {
            field: "IE_TDNHeading",
            display: [
              hide(),
              show().onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
            ]
          },
          {
            field: "IE_UCRHeading",
            display: [
              hide(),
              show().onInflow().onSection("ABG").onCategory(["101", "103", "105", "106"])
            ]
          },
          {
            field: "IE_MRNNotOnIVSHeading",
            display: [
              hide(),
              show().onOutflow().onSection("ABG").onCategory(["101", "103", "105", "106"])
            ]
          },

        ]
      };

      detailImportExport = {
        ruleset: "Reporting Import/Export Display Rules",
        scope: "importexport",
        fields: [

          
          {
            field: "ImportControlNumber",
            display: [
              hide(),
              show().onOutflow().onSection("ABG").onCategory(["101", "103", "105", "106"]),
              setValue("INV", null, notPattern(/^INV.+$/))
                .onOutflow().onSection("ABG").onCategory("101"),
              setValue("", null, hasValue("INV")).onOutflow().notOnCategory("101")
            ]
          },
          {
            field: "TransportDocumentNumber",
            display: [
            hide(isEmpty),
              show().onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
            ]
          },
          {
            field: "MRNNotOnIVS",
            display: [
            hide(isEmpty),
              show().onOutflow().onSection("ABG").onCategory(["101", "103", "105", "106"])
            ]
          },
          {
            field: "UCR",
            display: [
            hide(isEmpty),
              show().onInflow().onSection("ABG").onCategory(["101", "103", "105", "106"])
            ]
          },
          {
            field: "PaymentCurrencyCode",
            display: [
              show(),
              setValue('%s','money::IECurrencyCode',returnTrue)
            ]
          },
          {
            field: "PaymentValue",
            display: [
              show()
            ]
          }
        ]
      };

    }

    return {
      detailTrans: detailTrans,
      detailMoney: detailMoney,
      detailImportExport: detailImportExport
    }
  }

});

