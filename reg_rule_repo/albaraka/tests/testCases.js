define(function() {
  return function(testBase) {
      with (testBase) {

        var test_cases = [
          //If the Flow is OUT and the category is 101/01 to 101/10, the first 3 characters must be INV followed by the invoice number. The minimum total number of characters must be 4
          assertFailure("ieicn3a", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{
              CategoryCode: '101',
              CategorySubCode: '04',
              ImportExport: [{ImportControlNumber: 'INV1'}]
            }]
          }),
          assertSuccess("ieicn3a", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{
              CategoryCode: '101',
              CategorySubCode: '04',
              ImportExport: [{ImportControlNumber: 'INV1234'}]
            }]
          }),
          assertSuccess("ieicn3a", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{
              CategoryCode: '101',
              CategorySubCode: '04',
              ImportExport: [{ImportControlNumber: 'INV1246KJDKJF'}]
            }]
          }),
          assertFailure("ieicn3a", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '04', ImportExport: [{ImportControlNumber: 'INV'}]}]
          }),
          assertFailure("ieicn3a", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '04', ImportExport: [{ImportControlNumber: 'PS'}]}]
          }),
          assertSuccess("ieicn3a", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{
              CategoryCode: '101',
              CategorySubCode: '11',
              ImportExport: [{ImportControlNumber: 'NOT INV'}]
            },{
              CategoryCode: '101',
              CategorySubCode: '10',
              ImportExport: [{ImportControlNumber: 'INV1246KJDKJF'}]
            }]
          }),
          assertNoRule("ieicn3a", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            MonetaryAmount: [{
              CategoryCode: '101',
              CategorySubCode: '11',
              ImportExport: [{ImportControlNumber: 'NOT INV'}]
            }]
          })
        ]
      }
    return testBase;
  }
})
