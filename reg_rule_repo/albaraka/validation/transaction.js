define(function () {
  return function (predef) {
    var extTrans;
    with (predef) {

      extTrans = {
        ruleset: "External Transaction Rules",
        scope: "money",
        validations: [
          {
            field: ["ThirdPartyCIF"],
            rules: [
              failure("ext_cif1", "CIF01","The CIF number used must be numerical only.",
                notEmpty.and(notPattern(/\d+$/)))
            ]
          }
        ]
      };

    }
    return extTrans;
  }
});

