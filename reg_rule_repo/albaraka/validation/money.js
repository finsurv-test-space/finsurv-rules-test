define(function () {
  return function (predef) {
    var extTrans;
    with (predef) {

      extTrans = {
        ruleset: "External Money Rules",
        scope: "money",
        validations: [
           {
             field : "ReversalTrnRefNumber",
             rules : [
               //410, "Original transaction and SequenceNumber combination, with an opposite flow, not stored on database"
               //411, "Incorrect reversal category used with original transaction category"
               validate("ext_mrtrn1", "Validate_ReversalTrnRef",
                     notEmpty.and(hasMoneyField('ReversalTrnSeqNumber'))).onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]).onSection("ABG")
             ]
           },
           {
            field : "ReversalTrnSeqNumber",
            rules : [
              //410, "Original transaction and SequenceNumber combination, with an opposite flow, not stored on database"
              //411, "Incorrect reversal category used with original transaction category"
              validate("ext_mrtrn1", "Validate_ReversalTrnRef",
                    notEmpty.and(hasMoneyField('ReversalTrnRefNumber'))).onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]).onSection("ABG")
            ]
          }
        ]
      };

    }
    return extTrans;
  }
});
