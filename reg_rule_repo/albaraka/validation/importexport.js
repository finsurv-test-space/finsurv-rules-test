define(function () {
  return function (predef) {
    var stdImportExport;
    with (predef) {
      stdImportExport = {
        ruleset: "Standard Import/Export Rules",
        scope: "importexport",
        validations: [
          {
            field: "ImportControlNumber",
            minLen: 7,
            maxLen: 35,
            rules: [
              ignore("ieicn3"),
              failure("ieicn3a", 499, "If the Flow is OUT and the category is 101/01 to 101/10, the first 3 characters must be INV followed by the invoice number. The minimum total number of characters must be 7",
                notPattern(/^INV.{4,31}$/)).onOutflow().onSection("ABG").onCategory("101").notOnCategory("101/11")
            ]
          }
        ]
      };
    }

    return stdImportExport;
  }
});
