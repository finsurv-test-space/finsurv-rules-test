define({
  engine: {major: "1", minor: "0"},
  dependsOn: "coreSARB",
  features: ["featureSchema"],
  mappings: {
    LocalValue: "DomesticValue",
    Regulator: "Regulator",
    DealerPrefix: "RE",
    RegulatorPrefix: "CB",
    _minLenErrorType: "ERROR", // SUCCESS, ERROR, WARNING
    _maxLenErrorType: "ERROR",
    _lenErrorType: "ERROR"
  }
})