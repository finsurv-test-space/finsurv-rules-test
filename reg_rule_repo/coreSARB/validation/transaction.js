define(function () {
  return function (predef) {
    var feature;
    with (predef) {

      feature = {
        ruleset: "Specific SARB Transaction rules",
        scope: "transaction",
        validations: [
          {
            field: ["Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province", "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('p1', 336, 'Must be valid South African province',
                notValidProvince).onSection("BG"),
              failure('p2', 527, 'May not be completed',
                notEmpty).onSection("DF"),
              ignore('p3'),
              failure('p4', 336, 'Must be valid province (including NAMIBIA, LESOTHO or SWAZILAND)',
                notValidProvince.and(notValueIn(["NAMIBIA", "LESOTHO", "SWAZILAND"]))).onSection("E"),
              failure('p5', 287, 'Must be set to NAMIBIA because ForeignIDCountry is NA',
                hasTransactionFieldValue("Resident.Individual.ForeignIDCountry", "NA").and(notValueIn("NAMIBIA"))).onSection("E"),
              failure('p6', 287, 'Must be set to LESOTHO because ForeignIDCountry is LS',
                hasTransactionFieldValue("Resident.Individual.ForeignIDCountry", "LS").and(notValueIn("LESOTHO"))).onSection("E"),
              failure('p7', 287, 'Must be set to SWAZILAND because ForeignIDCountry is SZ',
                hasTransactionFieldValue("Resident.Individual.ForeignIDCountry", "SZ").and(notValueIn("SWAZILAND"))).onSection("E"),
              failure('p8', 336, 'Must be valid South African province',
                notValidProvince).onInflow().onSection("A"),
              failure('p9', 336, 'Must be valid South African province',
                notEmpty.and(notValidProvince)).onOutflow().onSection("A"),
              failure('p10', 336, 'If the flow is OUT and the Subject is REMITTANCE DISPENSATION, this must be valid South African province',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A"),
              failure('p11', 336, 'If the Flow is IN and the category is 400 and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION, this must be valid South African province',
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onCategory('400').onSection("A"),

            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.Email",
              "Resident.Entity.ContactDetails.Email"],
            rules: [
              warning("cnte3", "E01", "This is not a valid email address",
                  notEmpty.and(notValidEmail))
            ]
          },
        ]
      };
   }
    return feature;
  }
});
