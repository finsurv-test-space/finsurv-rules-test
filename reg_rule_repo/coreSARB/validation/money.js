define(function () {
    return function (predef) {
        var stdMoney;
        with (predef) {

            stdMoney = {
                ruleset: "Standard SARB Money Rules",
                scope: "money",
                validations: [
                    {
                        field: ["Resident.Individual.ContactDetails.Email",
                            "Resident.Entity.ContactDetails.Email"],
                        rules: [
                            warning("cnte4", "E01", "This is not a valid email address",
                                notEmpty.and(notValidEmail))
                        ]
                    },
                ]
            };
        }
        return stdMoney;
    }
});