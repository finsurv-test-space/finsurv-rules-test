define(function() {
  return function(testBase) {
      with (testBase) {
        var mappings = {
          _maxLenErrorType: "ERROR"
        };
        
        setMappings(mappings, true);
        var test_cases = [
          // TotalForeignValue
          assertSuccess("tfv1", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'ZAR', TotalForeignValue: '101.01',
            MonetaryAmount: [{RandValue: '50.00'}, {RandValue: '51.01'}]
          }),
          assertFailure("tfv1", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'ZAR', TotalForeignValue: '101.01',
            MonetaryAmount: [{RandValue: '50.00'}, {RandValue: '49.99'}]
          }),
          assertSuccess("tfv1", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'ZAR',
            MonetaryAmount: [{RandValue: '50.00'}, {RandValue: '49.99'}]
          }),

          assertSuccess("tfv2", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD', TotalForeignValue: '101.01',
            MonetaryAmount: [{ForeignValue: '50.00'}, {ForeignValue: '51.01'}]
          }),
          assertFailure("tfv2", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD', TotalForeignValue: '101.01',
            MonetaryAmount: [{ForeignValue: '50.00'}, {ForeignValue: '49.99'}]
          }),
          assertSuccess("tfv2", {
            ReportingQualifier: 'BOPCUS', FlowCurrency: 'USD',
            MonetaryAmount: [{ForeignValue: '50.00'}, {ForeignValue: '49.99'}]
          }),

          assertSuccess("tfv3", {
            ReportingQualifier: 'BOPCARD NON RESIDENT', FlowCurrency: 'ZAR', TotalForeignValue: '101.01',
            MonetaryAmount: [
              {ForeignCardHoldersPurchasesRandValue: '40.00', ForeignCardHoldersCashWithdrawalsRandValue: '10.00'},
              {ForeignCardHoldersCashWithdrawalsRandValue: '51.01'}
            ]
          }),
          assertFailure("tfv3", {
            ReportingQualifier: 'BOPCARD NON RESIDENT', FlowCurrency: 'ZAR', TotalForeignValue: '101.01',
            MonetaryAmount: [{ForeignCardHoldersPurchasesRandValue: '50.00'}, {ForeignCardHoldersPurchasesRandValue: '49.99'}]
          }),
          assertSuccess("tfv3", {
            ReportingQualifier: 'BOPCARD NON RESIDENT', FlowCurrency: 'ZAR',
            MonetaryAmount: [{ForeignCardHoldersPurchasesRandValue: '50.00'}, {ForeignCardHoldersPurchasesRandValue: '49.99'}]
          }),
          //The TotalForeignValue must be completed and must be greater than 0.00
          assertSuccess("tfv4", {
            ReportingQualifier: 'BOPCUS', TotalForeignValue: '101.01'
          }),
          assertFailure("tfv4", {
            ReportingQualifier: 'BOPCUS', TotalForeignValue: '0.00'
          }),
          assertFailure("tfv4", {
            ReportingQualifier: 'BOPCUS'
          }),
          assertNoRule("tfv4", {
            ReportingQualifier: 'BOPCARD NON RESIDENT', TotalForeignValue: '0.00'
          }),

          // Resident.Individual.StreetAddress.Province
          assertSuccess('p1:1', {
            ReportingQualifier: 'BOPDIR',
            Resident: {Individual: {StreetAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p1:1', {
            ReportingQualifier: 'BOPDIR',
            Resident: {Individual: {StreetAddress: {Province: 'GHOWTENG'}}}
          }),
          assertFailure('p1:1', {
            ReportingQualifier: 'BOPDIR',
            Resident: {Individual: {StreetAddress: {Province: 'NAMIBIA'}}}
          }),
          assertFailure('p1:1', {
            ReportingQualifier: 'BOPDIR',
            Resident: {Individual: {StreetAddress: {Province: ''}}}
          }),
          assertNoRule('p1:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {Province: 'WHATEVER'}}}
          }),
          assertSuccess('p2:1', {
            ReportingQualifier: 'INTERBANK',
            Resident: {Individual: {StreetAddress: {Province: ''}}}
          }),
          assertFailure('p2:1', {
            ReportingQualifier: 'INTERBANK',
            Resident: {Individual: {StreetAddress: {Province: 'WHATEVER'}}}
          }),
          assertSuccess('p3:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {Province: 'GAUTENG'}}}
          }),
          assertSuccess('p3:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {Province: 'GHOWTENG'}}}
          }),
          assertSuccess('p3:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {Province: 'NAMIBIA'}}}
          }),
          assertSuccess('p3:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {Province: ''}}}
          }),
          assertNoRule('p3:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {StreetAddress: {Province: 'WHATEVER'}}}
          }),
          assertSuccess('p4:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p4:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {Province: 'GHOWTENG'}}}
          }),
          assertSuccess('p4:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {Province: 'NAMIBIA'}}}
          }),
          assertFailure('p4:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {StreetAddress: {Province: ''}}}
          }),
          assertNoRule('p4:1', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {StreetAddress: {Province: 'WHATEVER'}}}
          }),
          //Must be set to NAMIBIA, LESOTHO or SWAZILAND because ForeignIDCountry is NA, LS or SZ respectively
          assertSuccess('p5:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {ForeignIDCountry: 'LS', StreetAddress: {Province: 'LESOTHO'}}}
          }),
          assertSuccess('p5:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {ForeignIDCountry: 'NA', StreetAddress: {Province: 'NAMIBIA'}}}
          }),
          assertFailure('p5:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {ForeignIDCountry: 'NA', StreetAddress: {Province: 'LESOTHO'}}}
          }),
          assertFailure('p5:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {ForeignIDCountry: 'NA', StreetAddress: {Province: 'GAUTENG'}}}
          }),
          assertSuccess('p8:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {StreetAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p8:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {StreetAddress: {Province: 'GHOWTENG'}}}
          }),
          assertFailure('p8:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {StreetAddress: {Province: 'NAMIBIA'}}}
          }),
          assertFailure('p8:1', {
            ReportingQualifier: 'BOPCUS',
            Flow: 'IN',
            Resident: {Individual: {StreetAddress: {Province: ''}}}
          }),
          assertNoRule('p8:1', {
            ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'IN',
            Resident: {Individual: {StreetAddress: {Province: 'WHATEVER'}}}
          }),
          assertSuccess('p9:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Individual: {StreetAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p9:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Individual: {StreetAddress: {Province: 'GHOWTENG'}}}
          }),
          assertFailure('p9:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Individual: {StreetAddress: {Province: 'NAMIBIA'}}}
          }),
          assertSuccess('p9:1', {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            Resident: {Individual: {StreetAddress: {Province: ''}}}
          }),
          assertSuccess('p10:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Individual: {StreetAddress: {Province: ''}}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),
          assertFailure('p10:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Individual: {StreetAddress: {Province: ''}}}
          }),
          assertSuccess('p11:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {StreetAddress: {Province: 'TestProvince'}}},
            MonetaryAmount: [{CategoryCode: '400', AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),
          assertFailure('p11:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {StreetAddress: {Province: ''}}},
            MonetaryAmount: [{CategoryCode: '400', AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),
          assertNoRule('p11:1', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {StreetAddress: {Province: 'TestProvince'}}},
            MonetaryAmount: [{CategoryCode: '100', AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),
          assertSuccess('p6:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: { Individual: { ForeignIDCountry: "LS", StreetAddress: {Province: 'LESOTHO'}}}
          }),
          assertFailure('p6:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {ForeignIDCountry: "LS", StreetAddress: {Province: 'GAUTENG'}}}
          }),
          assertSuccess('p7:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: { Individual: { ForeignIDCountry: "SZ", StreetAddress: {Province: 'SWAZILAND'}}}
          }),
          assertFailure('p7:1', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {ForeignIDCountry: "SZ", StreetAddress: {Province: 'GAUTENG'}}}
          }),



          // Resident.Entity.StreetAddress.Province
          assertSuccess('p1:2', {
            ReportingQualifier: 'BOPDIR',
            Resident: {Entity: {StreetAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p1:2', {
            ReportingQualifier: 'BOPDIR',
            Resident: {Entity: {StreetAddress: {Province: 'GHOWTENG'}}}
          }),
          assertFailure('p1:2', {ReportingQualifier: 'BOPDIR', Resident: {Entity: {StreetAddress: {Province: ''}}}}),
          assertSuccess('p2:2', {ReportingQualifier: 'INTERBANK', Resident: {Entity: {StreetAddress: {Province: ''}}}}),
          assertFailure('p2:2', {
            ReportingQualifier: 'INTERBANK',
            Resident: {Entity: {StreetAddress: {Province: 'WHATEVER'}}}
          }),
          assertSuccess('p4:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p4:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {Province: 'GHOWTENG'}}}
          }),
          assertSuccess('p4:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {Province: 'NAMIBIA'}}}
          }),
          assertFailure('p4:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {Province: ''}}}
          }),
          assertNoRule('p4:2', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {StreetAddress: {Province: 'WHATEVER'}}}
          }),
          //Must be set to NAMIBIA, LESOTHO or SWAZILAND because ForeignIDCountry is NA, LS or SZ
          assertSuccess('p5:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {Province: 'LESOTHO'}}}
          }),
          assertSuccess('p5:2', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {StreetAddress: {Province: 'GAUTENG'}}}
          }),
          assertSuccess('p8:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {StreetAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p8:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {StreetAddress: {Province: 'GHOWTENG'}}}
          }),
          assertFailure('p8:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {StreetAddress: {Province: 'NAMIBIA'}}}
          }),
          assertFailure('p8:2', {
            ReportingQualifier: 'BOPCUS',
            Flow: 'IN',
            Resident: {Entity: {StreetAddress: {Province: ''}}}
          }),
          assertNoRule('p8:2', {
            ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'IN',
            Resident: {Entity: {StreetAddress: {Province: 'WHATEVER'}}}
          }),
          assertSuccess('p9:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Entity: {StreetAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p9:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Entity: {StreetAddress: {Province: 'GHOWTENG'}}}
          }),
          assertFailure('p9:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Entity: {StreetAddress: {Province: 'NAMIBIA'}}}
          }),
          assertSuccess('p9:2', {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            Resident: {Entity: {StreetAddress: {Province: ''}}}
          }),
          assertSuccess('p10:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Entity: {StreetAddress: {Province: ''}}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),
          assertFailure('p10:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Entity: {StreetAddress: {Province: ''}}}
          }),
          assertSuccess('p11:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {StreetAddress: {Province: 'TestProvince'}}},
            MonetaryAmount: [{CategoryCode: '400', AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),
          assertFailure('p11:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {StreetAddress: {Province: ''}}},
            MonetaryAmount: [{CategoryCode: '400', AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),
          assertNoRule('p11:2', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {StreetAddress: {Province: 'TestProvince'}}},
            MonetaryAmount: [{CategoryCode: '100', AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),

          // Resident.Individual.PostalAddress.Province
          assertSuccess('p1:3', {
            ReportingQualifier: 'BOPDIR',
            Resident: {Individual: {PostalAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p1:3', {
            ReportingQualifier: 'BOPDIR',
            Resident: {Individual: {PostalAddress: {Province: 'GHOWTENG'}}}
          }),
          assertFailure('p1:3', {
            ReportingQualifier: 'BOPDIR',
            Resident: {Individual: {PostalAddress: {Province: ''}}}
          }),
          assertSuccess('p2:3', {
            ReportingQualifier: 'INTERBANK',
            Resident: {Individual: {PostalAddress: {Province: ''}}}
          }),
          assertFailure('p2:3', {
            ReportingQualifier: 'INTERBANK',
            Resident: {Individual: {PostalAddress: {Province: 'WHATEVER'}}}
          }),
          assertSuccess('p3:3', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {PostalAddress: {Province: 'GAUTENG'}}}
          }),
          assertSuccess('p3:3', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {PostalAddress: {Province: 'GHOWTENG'}}}
          }),
          assertSuccess('p3:3', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {PostalAddress: {Province: 'NAMIBIA'}}}
          }),
          assertSuccess('p3:3', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {PostalAddress: {Province: ''}}}
          }),
          assertNoRule('p3:3', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {PostalAddress: {Province: 'WHATEVER'}}}
          }),
          assertSuccess('p4:3', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {PostalAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p4:3', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {PostalAddress: {Province: 'GHOWTENG'}}}
          }),
          assertSuccess('p4:3', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {PostalAddress: {Province: 'NAMIBIA'}}}
          }),
          assertFailure('p4:3', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {PostalAddress: {Province: ''}}}
          }),
          assertNoRule('p4:3', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Individual: {PostalAddress: {Province: 'WHATEVER'}}}
          }),
          //Must be set to NAMIBIA, LESOTHO or SWAZILAND because ForeignIDCountry is NA, LS or SZ respectively
          assertSuccess('p5:3', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {ForeignIDCountry: 'LS', PostalAddress: {Province: 'LESOTHO'}}}
          }),
          assertSuccess('p5:3', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {ForeignIDCountry: 'NA', PostalAddress: {Province: 'NAMIBIA'}}}
          }),
          assertFailure('p5:3', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {ForeignIDCountry: 'NA', PostalAddress: {Province: 'LESOTHO'}}}
          }),
          assertFailure('p5:3', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Individual: {ForeignIDCountry: 'NA', PostalAddress: {Province: 'GAUTENG'}}}
          }),
          assertSuccess('p8:3', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {PostalAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p8:3', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {PostalAddress: {Province: 'GHOWTENG'}}}
          }),
          assertFailure('p8:3', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {PostalAddress: {Province: 'NAMIBIA'}}}
          }),
          assertFailure('p8:3', {
            ReportingQualifier: 'BOPCUS',
            Flow: 'IN',
            Resident: {Individual: {PostalAddress: {Province: ''}}}
          }),
          assertNoRule('p8:3', {
            ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'IN',
            Resident: {Individual: {PostalAddress: {Province: 'WHATEVER'}}}
          }),
          assertSuccess('p9:3', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Individual: {PostalAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p9:3', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Individual: {PostalAddress: {Province: 'GHOWTENG'}}}
          }),
          assertFailure('p9:3', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Individual: {PostalAddress: {Province: 'NAMIBIA'}}}
          }),
          assertSuccess('p9:3', {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            Resident: {Individual: {PostalAddress: {Province: ''}}}
          }),
          assertSuccess('p10:3', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Individual: {PostalAddress: {Province: ''}}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),
          assertFailure('p10:3', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Individual: {PostalAddress: {Province: ''}}}
          }),
          assertSuccess('p11:3', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {PostalAddress: {Province: 'TestProvince'}}},
            MonetaryAmount: [{CategoryCode: '400', AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),
          assertFailure('p11:3', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {PostalAddress: {Province: ''}}},
            MonetaryAmount: [{CategoryCode: '400', AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),
          assertNoRule('p11:3', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Individual: {PostalAddress: {Province: 'TestProvince'}}},
            MonetaryAmount: [{CategoryCode: '100', AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),

          // Resident.Entity.PostalAddress.Province"
          assertSuccess('p1:4', {
            ReportingQualifier: 'BOPDIR',
            Resident: {Entity: {PostalAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p1:4', {
            ReportingQualifier: 'BOPDIR',
            Resident: {Entity: {PostalAddress: {Province: 'GHOWTENG'}}}
          }),
          assertFailure('p1:4', {ReportingQualifier: 'BOPDIR', Resident: {Entity: {PostalAddress: {Province: ''}}}}),
          assertSuccess('p2:4', {ReportingQualifier: 'INTERBANK', Resident: {Entity: {PostalAddress: {Province: ''}}}}),
          assertFailure('p2:4', {
            ReportingQualifier: 'INTERBANK',
            Resident: {Entity: {PostalAddress: {Province: 'WHATEVER'}}}
          }),
          assertSuccess('p4:4', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {PostalAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p4:4', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {PostalAddress: {Province: 'GHOWTENG'}}}
          }),
          assertSuccess('p4:4', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {PostalAddress: {Province: 'NAMIBIA'}}}
          }),
          assertFailure('p4:4', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {PostalAddress: {Province: ''}}}
          }),
          assertNoRule('p4:4', {
            ReportingQualifier: 'BOPCUS',
            Resident: {Entity: {PostalAddress: {Province: 'WHATEVER'}}}
          }),
          //Must be set to NAMIBIA, LESOTHO or SWAZILAND because ForeignIDCountry is NA, LS or SZ
          assertSuccess('p5:4', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {PostalAddress: {Province: 'LESOTHO'}}}
          }),
          assertSuccess('p5:4', {
            ReportingQualifier: 'BOPCARD RESIDENT',
            Resident: {Entity: {PostalAddress: {Province: 'GAUTENG'}}}
          }),
          assertSuccess('p8:4', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {PostalAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p8:4', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {PostalAddress: {Province: 'GHOWTENG'}}}
          }),
          assertFailure('p8:4', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {PostalAddress: {Province: 'NAMIBIA'}}}
          }),
          assertFailure('p8:4', {
            ReportingQualifier: 'BOPCUS',
            Flow: 'IN',
            Resident: {Entity: {PostalAddress: {Province: ''}}}
          }),
          assertNoRule('p8:4', {
            ReportingQualifier: 'BOPCARD RESIDENT', Flow: 'IN',
            Resident: {Entity: {PostalAddress: {Province: 'WHATEVER'}}}
          }),
          assertSuccess('p9:4', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Entity: {PostalAddress: {Province: 'GAUTENG'}}}
          }),
          assertFailure('p9:4', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Entity: {PostalAddress: {Province: 'GHOWTENG'}}}
          }),
          assertFailure('p9:4', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Entity: {PostalAddress: {Province: 'NAMIBIA'}}}
          }),
          assertSuccess('p9:4', {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            Resident: {Entity: {PostalAddress: {Province: ''}}}
          }),
          assertSuccess('p10:4', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Entity: {PostalAddress: {Province: ''}}},
            MonetaryAmount: [{AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),
          assertFailure('p10:4', {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT',
            Resident: {Entity: {PostalAddress: {Province: ''}}}
          }),
          assertSuccess('p11:4', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {PostalAddress: {Province: 'TestProvince'}}},
            MonetaryAmount: [{CategoryCode: '400', AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),
          assertFailure('p11:4', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {PostalAddress: {Province: ''}}},
            MonetaryAmount: [{CategoryCode: '400', AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),
          assertNoRule('p11:4', {
            ReportingQualifier: 'BOPCUS', Flow: 'IN',
            Resident: {Entity: {PostalAddress: {Province: 'TestProvince'}}},
            MonetaryAmount: [{CategoryCode: '100', AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}}]
          }),
          assertFailure("NonResident.Individual.Address.City.maxLen", {
             ReportingQualifier: "BOPCUS",
             NonResident: {Individual: {Address: {City: "LONGHUA DISTRICT,SHENZHEN CITY CHINA AND PADDING FOR GOOD MEASURE"}}}
           }),
        ]
      }
    return testBase;
  }
})
