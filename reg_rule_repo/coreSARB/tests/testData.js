var testData = [
  {
    name: "Empty transaction",
    data: {}
  },

  {
    name: "Minimal individuals",
    data: {
      Flow: "OUT",
      Resident: {
        Individual: {}
      },
      NonResident: {
        Individual: {}
      }
    }
  },

  {
    name: "Minimal information INV",

    data: {
      transaction: {
        TrnReference: "InfoProvidedInv2",
        Flow: "OUT",
        ValueDate: "2017-05-10",
        ReportingQualifier: "BOPCUS",
        OriginatingBank: "IVESZAJJ0",
        OriginatingCountry: "ZA",
        FlowCurrency: "USD",
        TotalForeignValue: "1000.00",
        ZAREquivalent: "1234",
        Resident: {
          Entity: {
            PostalAddress: {
              AddressLine2: "ACME Bank",
              AddressLine1: "Ground Floor",
              Suburb: "Johannesburg",
              PostalCode: "2001",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            StreetAddress: {
              AddressLine2: "ACME Bank",
              AddressLine1: "Ground Floor",
              Suburb: "Johannesburg",
              PostalCode: "2001",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            EntityName: "ACME BK OF ZA LTD",
            ContactDetails: {
              ContactSurname: "Fudd",
              Email: "elma.fudd@acme.co.za",
              Telephone: "27116365094",
              ContactName: "Elma"
            },
            AccountIdentifier: "RESIDENT OTHER",
            IndustrialClassification: "08",
            InstitutionalSector: "01",
            TradingName: "ACME BK OF ZA LTD",
            RegistrationNumber: "196200073806",
            AccountName: "CONTRA MTSS TREAS DIV",
            AccountNumber: "9864660"
          }
        }
      }
    }
  },

  {
    name: "Minimal entities",
    data: {
      Flow: "OUT",
      Resident: {
        Entity: {}
      },
      NonResident: {
        Entity: {}
      }
    }
  },
  {
    name: "zz1 with 3rd party",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "NON REPORTABLE",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "",
              AddressLine2: "",
              Suburb: "",
              City: "",
              PostalCode: "",
              Province: ""
            },
            PostalAddress: {
              AddressLine1: "",
              AddressLine2: "",
              Suburb: "",
              City: "",
              PostalCode: "",
              Province: ""
            },
            ContactDetails: {
              ContactName: "",
              ContactSurname: "",
              Email: "",
              Fax: "",
              Telephone: ""
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          },
          Description: "John, Johnson"
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "ZZ1",
            ForeignValue: "1000",
            LocationCountry: "",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "",
              SARBAuthRefNumber: ""
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1",
            Description: "Report this BOPCUS transaction as a NON REPORTABLE",
            ThirdParty: { Individual: { DateOfBirth: "2016-09-04" } }
          }
        ],
        TotalValue: 1000,
        TrnReference: "TestRef12414",
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX"
      },
      customData: {
        AllowZZ1: true
      }
    }
  },
  {
    name: "Loan Validation test",
    data: {
      transaction: {
        Version: "FINSURV",
        TrnReference: "SBOT16257ZA0003524001",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        ValueDate: "2016-09-14",
        FlowCurrency: "USD",
        TotalForeignValue: 6,
        BranchCode: "ZA01",
        BranchName: "Foreign Trade Services Central Gauteng",
        OriginatingBank: "SBZAZAJ0XXX",
        OriginatingCountry: "ZA",
        CorrespondentBank: "CHASUSU0XXX",
        CorrespondentCountry: "US",
        ReceivingBank: "SCBLUS33XXX",
        ReceivingCountry: "US",
        IsADLA: "N",
        Resident: {
          Entity: {
            EntityName: "Compair (S A) (Pty) Ltd.",
            TradingName: "test",
            RegistrationNumber: "1927/000445/07",
            InstitutionalSector: "02",
            IndustrialClassification: "03",
            AccountName: "Columbus Stainless (Pty) Ltd.",
            AccountIdentifier: "FCA RESIDENT",
            AccountNumber: "0000000090401247",
            TaxNumber: "9700/156/71/5",
            VATNumber: "NO VAT NUMBER",
            CustomsClientNumber: "21143776",
            StreetAddress: {
              Province: "GAUTENG",
              AddressLine1: "Stand 255",
              AddressLine2: "Berrange Road",
              Suburb: "Wadeville",
              City: "Germiston",
              PostalCode: "1428"
            },
            PostalAddress: {
              Province: "GAUTENG",
              AddressLine1: "56 treetop street",
              AddressLine2: "applegate avenue",
              Suburb: "BRYANSTAN",
              City: "Johannesburg",
              PostalCode: "2001"
            },
            ContactDetails: {
              ContactName: "lucy",
              ContactSurname: "lucy",
              Email: "sandeep@gmail.com"
            }
          },
          Description: "Compair (S A) (Pty) Ltd., t/a test"
        },
        NonResident: {
          Entity: {
            EntityName: "Cross Border Bene",
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "0000000090637704",
            Address: { Country: "US" }
          }
        },
        MonetaryAmount: [
          {
            MoneyTransferAgentIndicator: "AD",
            ForeignValue: 6,
            SARBAuth: { RulingsSection: "B.13(A)" },
            CategoryCode: "810",
            CategorySubCode: "",
            ThirdParty: {
              Individual: {
                Surname: "mishra",
                Name: "sandeep",
                Gender: "M",
                IDNumber: "33454445",
                TempResPermitNumber: "444444444",
                PassportNumber: "j665565665",
                PassportCountry: "US",
                IDType: "Identification number"
              },
              TaxNumber: "22222222",
              VATNumber: "44444444",
              CustomsClientNumber: "70707070",
              StreetAddress: {
                Province: "GAUTENG",
                AddressLine1: "st pauls",
                AddressLine2: "paulshof",
                Suburb: "rodepoort",
                City: "joburg",
                PostalCode: "2191"
              },
              PostalAddress: {
                Province: "GAUTENG",
                AddressLine1: "st peters",
                AddressLine2: "paulshof",
                Suburb: "roodepoort",
                City: "joburg",
                PostalCode: "2195"
              },
              ContactDetails: {
                ContactName: "rahul",
                ContactSurname: "das",
                Fax: "0785987812"
              }
            },
            CannotCategorize: "otherBopDetails",
            LoanRefNumber: "99456745674567",
            LoanTenor: "ON DEMAND",
            LoanInterestRate: "13.89 13.89",
            LocationCountry: "US",
            SequenceNumber: 1,
            Description:
              "Loan made by a resident to a resident temporarily abroad",
            ThirdPartyKind: "Individual"
          }
        ]
      },
      customData: {
        DealerType: "AD",
        ShowOldCodes: true,
        AccountDrCr: "CR",
        snapShot: {
          Version: "FINSURV",
          TrnReference: "SBOT16257ZA0003524001",
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          ValueDate: "2016-09-14",
          FlowCurrency: "USD",
          TotalForeignValue: 6,
          BranchCode: "ZA01",
          BranchName: "Foreign Trade Services Central Gauteng",
          OriginatingBank: "SBZAZAJ0XXX",
          OriginatingCountry: "ZA",
          CorrespondentBank: "CHASUSU0XXX",
          CorrespondentCountry: "US",
          ReceivingBank: "SCBLUS33XXX",
          ReceivingCountry: "US",
          IsADLA: "N",
          Resident: {
            Entity: {
              EntityName: "Compair (S A) (Pty) Ltd.",
              TradingName: "test",
              RegistrationNumber: "1927/000445/07",
              InstitutionalSector: "02",
              IndustrialClassification: "03",
              AccountName: "Columbus Stainless (Pty) Ltd.",
              AccountIdentifier: "FCA RESIDENT",
              AccountNumber: "0000000090401247",
              TaxNumber: "9700/156/71/5",
              VATNumber: "NO VAT NUMBER",
              CustomsClientNumber: "21143776",
              StreetAddress: {
                Province: "GAUTENG",
                AddressLine1: "Stand 255",
                AddressLine2: "Berrange Road",
                Suburb: "Wadeville",
                City: "Germiston",
                PostalCode: "1428"
              },
              PostalAddress: {
                Province: "GAUTENG",
                AddressLine1: "56 treetop street",
                AddressLine2: "applegate avenue",
                Suburb: "BRYANSTAN",
                City: "Johannesburg",
                PostalCode: "2001"
              },
              ContactDetails: {
                ContactName: "lucy",
                ContactSurname: "lucy",
                Email: "sandeep@gmail.com"
              }
            }
          },
          NonResident: {
            Entity: {
              EntityName: "Cross Border Bene",
              AccountIdentifier: "NON RESIDENT OTHER",
              AccountNumber: "0000000090637704",
              Address: { Country: "US" }
            }
          },
          MonetaryAmount: [
            {
              MoneyTransferAgentIndicator: "AD",
              ForeignValue: 6,
              SARBAuth: { RulingsSection: "B.13(A)" },
              CategoryCode: "810",
              CategorySubCode: "",
              ThirdParty: {
                Individual: {
                  Surname: "mishra",
                  Name: "sandeep",
                  Gender: "M",
                  IDNumber: "33454445",
                  TempResPermitNumber: "444444444",
                  PassportNumber: "j665565665",
                  PassportCountry: "US"
                },
                Entity: { Name: "sandeep", RegistrationNumber: "54555556" },
                TaxNumber: "22222222",
                VATNumber: "44444444",
                CustomsClientNumber: "70707070",
                StreetAddress: {
                  Province: "GAUTENG",
                  AddressLine1: "st pauls",
                  AddressLine2: "paulshof",
                  Suburb: "rodepoort",
                  City: "joburg",
                  PostalCode: "2191"
                },
                PostalAddress: {
                  Province: "GAUTENG",
                  AddressLine1: "st peters",
                  AddressLine2: "paulshof",
                  Suburb: "roodepoort",
                  City: "joburg",
                  PostalCode: "2195"
                },
                ContactDetails: {
                  ContactName: "rahul",
                  ContactSurname: "das",
                  Fax: "0785987812"
                }
              },
              CannotCategorize: "otherBopDetails",
              LoanRefNumber: "99456745674567",
              LoanTenor: "ON DEMAND",
              LoanInterestRate: "13.89 13.89",
              LocationCountry: "US",
              SequenceNumber: 1
            }
          ]
        }
      }
    }
  },
  {
    name: "Std Bank Test",
    data: {
      customData: {
        readOnly: true
      },
      transaction: {
        TrnReference: "TestRef12414",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        TotalForeignValue: "1001",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        ReplacementTransaction: "Y",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "407",
            ForeignValue: "1000",
            LocationCountry: "US",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "2"
          }
        ]
      } //,
      // customData: {readOnly: true}
    }
  },
  {
    name: "simple valid",
    data: {
      transaction: {
        TotalValue: 1000,
        TrnReference: "TestRef12414",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        AccountHolderStatus: "South African Resident",
        LocationCountry: "NZ",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG",
              Country: "ZA"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG",
              State: "GAUTENG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Gender: "M",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "407",
            ForeignValue: "1000",
            LocationCountry: "US",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        PaymentDetail: {
          IsCorrespondentBank: "N",
          BeneficiaryBank: { BankName: "Foo Bank" },
          SWIFTDetails: "Payment"
        }
      },
      customData: {
        TotalDomesticAmount: 199.13
      }
    }
  },
  {
    name: "simple valid entity",
    data: {
      transaction: {
        TotalValue: 1000,
        TrnReference: "TestRef12414",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        AccountHolderStatus: "South African Resident",
        LocationCountry: "NZ",
        Resident: {
          Entity: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG",
              Country: "ZA"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            EntityName: "John",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG",
              State: "GAUTENG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Gender: "M",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "407",
            ForeignValue: "1000",
            LocationCountry: "US",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        PaymentDetail: {
          IsCorrespondentBank: "N",
          BeneficiaryBank: { BankName: "Foo Bank" },
          SWIFTDetails: "Payment"
        }
      },
      customData: {
        TotalDomesticAmount: 199.13
      }
    }
  },
  {
    name: "Valid importExport",
    data: {
      customData: {
        LUClient: false,
        TotalDomesticAmount: 200.22,
        readOnly: true
      },
      transaction: {
        TrnReference: "TestRef12424",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Entity: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360978",
              Telephone: "2761305978"
            },
            EntityName: "John Trading",
            RegistrationNumber: "12341234",
            IndustrialClassification: "03",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080",
            CustomsClientNumber: "12345661",
            InstitutionalSector: "02",
            TradingName: "Foo Bar's"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "US"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "103",
            ForeignValue: "500",
            LocationCountry: "US",
            CategorySubCode: "01",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 200,
                ImportControlNumber: "DBN200101011234567",
                TransportDocumentNumber: "124124",
                CustomsClientNumber: "64562345",
                SubsequenceNumber: "1",
                IVSResponse: { status: "pass" }
              },
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 300,
                ImportControlNumber: "DBN200101011234567",
                TransportDocumentNumber: "235523",
                CustomsClientNumber: "95245830",
                SubsequenceNumber: "2",
                IVSResponse: { status: "pass" }
              }
            ],
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          },
          {
            CategoryCode: "101",
            ForeignValue: "500",
            LocationCountry: "US",
            CategorySubCode: "03",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 200,
                ImportControlNumber: "INV1234567",
                SubsequenceNumber: "1"
              },
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 300,
                ImportControlNumber: "INV0101234564",
                SubsequenceNumber: "2"
              }
            ],
            MoneyTransferAgentIndicator: "AD",
            SARBAuth: {
              AuthIssuer: "016",
              ADInternalAuthNumberDate: "2016-01-06",
              ADInternalAuthNumber: "12341234"
            },
            ADInternalAuthResponse: { status: "pass" },
            SequenceNumber: "2"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        CCNResponse: {
          status: "warning",
          errorCode: "100",
          errorMessage: "Warning."
        }
      }
    }
  },
  {
    name: "valid Loan",
    data: {
      TrnReference: "TestRef12411",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Individual: {
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "2761360597",
            Telephone: "2761365278"
          },
          Name: "John",
          Surname: "Johnson",
          AccountIdentifier: "RESIDENT OTHER",
          AccountName: "Account_NT 1 p",
          AccountNumber: "221495398",
          //"TaxNumber": "123456",
          //"VATNumber": "66666",
          Gender: "M",
          DateOfBirth: "1955-02-29",
          IDNumber: "5502290001080"
        }
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "addressline1",
            AddressLine2: "addressline2",
            Suburb: "suurb",
            City: "townname",
            PostalCode: "postalcode",
            Country: "US"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "9032243812587",
          Name: "Fred",
          Surname: "Georgeson"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "309",
          ForeignValue: "1000",
          LocationCountry: "US",
          CategorySubCode: "06",
          LoanRefNumber: "3333333",
          LoanInterest: { InterestType: "FIXED", Rate: "0.3" },
          LoanInterestRate: "0.30",
          SARBAuth: {
            AuthIssuer: "024",
            SARBAuthAppNumber: "1234",
            SARBAuthRefNumber: "1234"
          },
          MoneyTransferAgentIndicator: "PAYPAL",
          SequenceNumber: "1"
        }
      ],
      TotalForeignValue: "1000",
      ValueDate: "2015-06-26",
      FlowCurrency: "USD",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "UG",
      ReceivingBank: "SBICUGKX"
    }
  },
  {
    name: "valid 3rd party",
    data: {
      TrnReference: "TestRef12454",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Individual: {
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "2761360578",
            Telephone: "2613605978"
          },
          Name: "John",
          Surname: "Johnson",
          AccountIdentifier: "RESIDENT OTHER",
          AccountName: "Account_NT 1 p",
          AccountNumber: "221495398",
          TaxNumber: "123456",
          VATNumber: "66666",
          Gender: "M",
          DateOfBirth: "1955-02-29",
          IDNumber: "5502290001080"
        }
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "addressline1",
            AddressLine2: "addressline2",
            Suburb: "suurb",
            City: "townname",
            PostalCode: "postalcode",
            Country: "US"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "9032243812587",
          Name: "Fred",
          Surname: "Georgeson"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "256",
          ForeignValue: "1000",
          LocationCountry: "US",
          ThirdParties: "Name1, Surname1",
          ThirdParty: {
            Individual: {
              Surname: "Surname1",
              Name: "Name1",
              Gender: "M",
              DateOfBirth: "2006-05-04",
              IDNumber: "8902205150088",
              PassportNumber: "12312312312312",
              PassportCountry: "ZA"
            },
            TaxNumber: "TaxNumber0",
            VATNumber: "VATNumber0",
            CustomsClientNumber: "12341234",
            StreetAddress: {
              AddressLine1: "AddressLine10",
              AddressLine2: "AddressLine20",
              Suburb: "Suburb0",
              City: "City0",
              PostalCode: "1244",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "AddressLine11",
              AddressLine2: "AddressLine21",
              Suburb: "Suburb1",
              City: "City1",
              PostalCode: "1234",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "ContactName0",
              ContactSurname: "ContactSurname0",
              Email: "Email0",
              Fax: "Fax1111111",
              Telephone: "Telephone0"
            }
          },
          SARBAuth: { AuthIssuer: "123" },
          MoneyTransferAgentIndicator: "WESTERNUNION",
          SequenceNumber: "1"
        }
      ],
      TotalForeignValue: "1000",
      ValueDate: "2015-06-26",
      FlowCurrency: "USD",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "UG",
      ReceivingBank: "SBICUGKX"
    }
  },
  {
    name: "simple bad",
    data: {
      TrnReference: "TestRef22414",
      Version: "FOO",
      ReportingQualifier: "BAR",
      Flow: "BAD",
      Resident: {
        Individual: {
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196#",
            Province: "GAUTENGS"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM FOO",
            Fax: "27613605978zzz",
            Telephone: "27613605978zz"
          },
          Name: "John",
          Surname: "Johnson",
          AccountIdentifier: "RESIDENT OTHER ZZ",
          AccountName: "Account_NT 1 p",
          AccountNumber: "221495398",
          TaxNumber: "123456",
          VATNumber: "66666",
          Gender: "S",
          DateOfBirth: "2070-02-29",
          IDNumber: "7002290001080"
        }
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "addressline1",
            AddressLine2: "addressline2",
            Suburb: "suurb",
            City: "townname",
            PostalCode: "postalcode",
            Country: "US"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "9032243812587",
          Name: "Fred",
          Surname: "Georgeson"
        }
      },
      TotalForeignValue: "-1000",
      ValueDate: "-2015-06-26",
      FlowCurrency: "USDS",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZAS",
      ReceivingCountry: "UGS",
      ReceivingBank: "SBICUGKX"
    }
  },
  {
    name: "BAD importExport",
    data: {
      TrnReference: "TestRef1244B",
      Version: "BOPCUS",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Individual: {
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196BAD",
            Province: "GAUTENGBAD"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196BAAD",
            Province: "GAUTENGBAD"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM BAD",
            Fax: "27613605978BAD",
            Telephone: "27613605978BAD"
          },
          Name: "John",
          Surname: "Johnson",
          AccountIdentifier: "RESIDENT OTHER BAD",
          AccountName: "Account_NT 1 p",
          AccountNumber: "221495398BAD",
          TaxNumber: "1234BAD56",
          VATNumber: "66666",
          Gender: "BAD",
          DateOfBirth: "BAD-1955-02-29",
          IDNumber: "5502290001080BAD"
        }
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "addressline1",
            AddressLine2: "addressline2",
            Suburb: "suurb",
            City: "townname",
            PostalCode: "postalcode",
            Country: "USBAD"
          },
          AccountIdentifier: "NON RESIDENT OTHER BAD",
          AccountNumber: "9032243812587",
          Name: "Fred",
          Surname: "Georgeson"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "103",
          ForeignValue: "1000",
          LocationCountry: "USBAD",
          CategorySubCode: "01",
          ImportExport: [
            {
              PaymentCurrencyCode: "USDBAD",
              PaymentValue: "345BAD",
              ImportControlNumber: "DBN200101011234567BAD",
              TransportDocumentNumber: "124124",
              CustomsClientNumber: "64562345BAD"
            },
            {
              PaymentCurrencyCode: "USDBAD",
              PaymentValue: "655BAD",
              ImportControlNumber: "DBN200101011234567BAD",
              TransportDocumentNumber: "235523",
              CustomsClientNumber: "95245834BAD"
            }
          ]
        }
      ],
      TotalForeignValue: "1000BAD",
      ValueDate: "BAD-2015-06-26",
      FlowCurrency: "USDBAD",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZABAD",
      ReceivingCountry: "UGBAD",
      ReceivingBank: "SBICUGKX"
    }
  },
  {
    name: "Bad Loan",
    data: {
      TrnReference: "TestRef18414",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Individual: {
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196BAD",
            Province: "GAUTENGBAD"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196BAD",
            Province: "GAUTENGBAD"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "27613605978",
            Telephone: "27613605978"
          },
          Name: "John",
          Surname: "Johnson",
          AccountIdentifier: "RESIDENT OTHER BAD",
          AccountName: "Account_NT 1 p",
          AccountNumber: "221495398",
          TaxNumber: "123456",
          VATNumber: "66666",
          Gender: "M",
          DateOfBirth: "1955-02-29-BAD",
          IDNumber: "5502290001080BAD"
        }
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "addressline1",
            AddressLine2: "addressline2",
            Suburb: "suurb",
            City: "townname",
            PostalCode: "postalcode",
            Country: "USBAD"
          },
          AccountIdentifier: "NON RESIDENT OTHER BAD",
          AccountNumber: "9032243812587",
          Name: "Fred",
          Surname: "Georgeson"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "309",
          ForeignValue: "1000",
          LocationCountry: "USBAD",
          CategorySubCode: "06",
          LoanRefNumber: "EE3333",
          LoanInterest: { InterestType: "FIXED", Rate: "0.3" }
        }
      ],
      TotalForeignValue: "1000",
      ValueDate: "2015-06-26",
      FlowCurrency: "USD",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "UG",
      ReceivingBank: "SBICUGKX"
    }
  },
  {
    name: "Bad 3rd party - no NonRes",
    data: {
      TrnReference: "TestRef52414",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Individual: {
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196BAD",
            Province: "GAUTENGBAD"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196BAD",
            Province: "GAUTENGBAD"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM BAD",
            Fax: "27613605978BADS",
            Telephone: "27613605978BAD"
          },
          Name: "John",
          Surname: "Johnson",
          AccountIdentifier: "RESIDENT OTHER BAD",
          AccountName: "Account_NT 1 p",
          AccountNumber: "221495398",
          TaxNumber: "123456",
          VATNumber: "66666",
          Gender: "M",
          DateOfBirth: "1955-02-29",
          IDNumber: "5502290001080BAD"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "256",
          ForeignValue: "1000",
          LocationCountry: "USBAD",
          LoanRefNumber: "",
          LoanInterest: { InterestType: null, Rate: "0.3" },
          ThirdParties: "Name1, Surname1",
          ThirdParty: {
            Individual: {
              Surname: "Surname1",
              Name: "Name1",
              Gender: "M",
              DateOfBirth: "2006-05-04",
              IDNumber: "0000000000000BAD",
              PassportNumber: "12312312312312BAD",
              PassportCountry: "ZABAD"
            },
            TaxNumber: "TaxNumber0",
            VATNumber: "VATNumber0",
            CustomsClientNumber: "12341234",
            StreetAddress: {
              AddressLine1: "AddressLine10",
              AddressLine2: "AddressLine20",
              Suburb: "Suburb0",
              City: "City0",
              PostalCode: "1244BAD",
              Province: "GAUTENGBAD"
            },
            PostalAddress: {
              AddressLine1: "AddressLine11",
              AddressLine2: "AddressLine21",
              Suburb: "Suburb1",
              City: "City1",
              PostalCode: "1234BAD",
              Province: "GAUTENGBAD"
            },
            ContactDetails: {
              ContactName: "ContactName0",
              ContactSurname: "ContactSurname0",
              Email: "Email0",
              Fax: "Fax1111111",
              Telephone: "Telephone0"
            }
          }
        }
      ],
      TotalForeignValue: "1000BAD",
      ValueDate: "2015BAD-06-26",
      FlowCurrency: "USDBAD",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZABAD",
      ReceivingCountry: "UGBAD",
      ReceivingBank: "SBICUGKX"
    }
  },
  {
    name: "Own Transfer",
    data: {
      TrnReference: "TestRef22414",
      Version: "FINSURV",
      ReportingQualifier: "NON REPORTABLE",
      Flow: "IN",
      Resident: {
        Individual: {
          Name: "Verma",
          Surname: "VERMA SVRINDER KUMAR",
          Gender: "M",
          DateOfBirth: "1990-09-05",
          StreetAddress: {},
          PostalAddress: {},
          ContactDetails: {},
          AccountIdentifier: "CFC RESIDENT",
          AccountName: "BoP Account 3",
          AccountNumber: "000090452046",
          TaxNumber: "12345",
          VATNumber: "2468"
        }
      },
      NonResident: {
        Individual: {
          Name: "Account NT 4",
          Surname: "Smith",
          Address: {
            AddressLine1: "2 Rissik Street",
            AddressLine2: "2 Rissik Street",
            City: "Johannesburg",
            PostalCode: "4001",
            Country: "US"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "000040452"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "ZZ1",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1"
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "USD",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "US",
      ReceivingBank: "SBZAZAJJ"
    }
  },
  {
    name: "ADLA",
    data: {
      TrnReference: "TestRef82414",
      customData: { ADLA: true },
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "CFC RESIDENT",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "US"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "401",
            ForeignValue: "1000",
            LocationCountry: "US",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX"
      }
    }
  },
  {
    name: "Own Transfer FCA",
    data: {
      TrnReference: "TestRef92414",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Entity: {
          TradingName: "Verma",
          EntityName: "VERMA SVRINDER KUMAR",
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "2761360578",
            Telephone: "2761305978"
          },
          AccountIdentifier: "RESIDENT OTHER",
          AccountName: "BoP Account 3",
          AccountNumber: "000090452046",
          RegistrationNumber: "12345",
          IndustrialClassification: "03",
          InstitutionalSector: "02",
          TaxNumber: "12345",
          VATNumber: "2468",
          LocationCountry: "ZA",
          TaxClearanceCertificateIndicator: "N"
        }
      },
      NonResident: {
        Entity: {
          EntityName: "Foo",
          // "Surname": "Smith",
          Address: {
            AddressLine1: "2 Rissik Street",
            AddressLine2: "2 Rissik Street",
            City: "Johannesburg",
            PostalCode: "4001",
            Country: "ZA"
          },
          AccountIdentifier: "FCA RESIDENT",
          AccountNumber: "000040452"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "513",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1",
          LocationCountry: "US",
          ThirdParty: {
            Individual: {
              Surname: "Data",
              Name: "Good",
              Gender: "M",
              DateOfBirth: "2006-05-04",
              IDNumber: "8810225493083"
            },
            TaxNumber: "TaxNumber0",
            VATNumber: "VATNumber0",
            CustomsClientNumber: "12341234",
            StreetAddress: {
              AddressLine1: "AddressLine10",
              AddressLine2: "AddressLine20",
              Suburb: "Suburb0",
              City: "City0",
              PostalCode: "1234",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "AddressLine11",
              AddressLine2: "AddressLine21",
              Suburb: "Suburb1",
              City: "City1",
              PostalCode: "1234",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "ContactName0",
              ContactSurname: "ContactSurname0",
              Email: "email@mail.com",
              Fax: "1231231234",
              Telephone: "1231234123"
            },
            $$hashKey: "object:59"
          },
          AdHocRequirement: { Subject: "NO", Description: "NONE" }
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "USD",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "ZA",
      ReceivingBank: "SBZAZAJJ"
    }
  },
  {
    name: "Own Transfer CFC",
    data: {
      TrnReference: "TestRef44414",
      Version: "FINSURV",
      ReportingQualifier: "NON REPORTABLE",
      Flow: "IN",
      Resident: {
        Entity: {
          TradingName: "Verma",
          EntityName: "VERMA SVRINDER KUMAR",
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "2761360578",
            Telephone: "2761305978"
          },
          AccountIdentifier: "RESIDENT OTHER",
          AccountName: "BoP Account 3",
          AccountNumber: "000090452046",
          RegistrationNumber: "12345",
          IndustrialClassification: "03",
          InstitutionalSector: "02",
          TaxNumber: "12345",
          VATNumber: "2468",
          LocationCountry: "US",
          TaxClearanceCertificateIndicator: "N"
        }
      },
      NonResident: {
        Exception: {
          ExceptionName: "CFC RESIDENT NON REPORTABLE"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "513",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1",
          LocationCountry: "US"
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "ZAR",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "ZA",
      ReceivingBank: "SBZAZAJJ"
    }
  },
  {
    name: "Future Date",
    data: {
      transaction: {
        TrnReference: "TestRef72414",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "US"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "407",
            ForeignValue: "1000",
            LocationCountry: "US",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2050-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX"
      } //,
      // customData: {readOnly: true}
    }
  },
  {
    name: "AuthTest",
    data: {
      transaction: {
        TrnReference: "TestRef12499",
        ReportingQualifier: "BOPCUS",
        TotalForeignValue: "1",
        Flow: "OUT",
        ValueDate: "25/02/2016",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "NG",
        ReceivingBank: "SBICNGLX",
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: "100",
            MoneyTransferAgentIndicator: "TRAVELEX",
            ImportExport: [],
            ForeignValue: "1",
            ReversalTrnRefNumber: "23445",
            LocationCountry: "AG",
            OriginatingBank: "SBZAZAJJ",
            OriginatingCountry: "ZA",
            ReceivingCountry: "NG",
            ReceivingBank: "SBICNGLX"
          }
        ],
        Resident: {
          Entity: {
            TradingName: "Grinaker Lta International Holdings Ltd ",
            RegistrationNumber: "C20326",
            EntityName: "Grinaker Lta International Holdings Ltd ",
            IndustrialClassification: "10",
            InstitutionalSector: "03",
            StreetAddress: {
              AddressLine1: "Jet Park Road ",
              AddressLine2: "Kempton Park ",
              Suburb: "Ruimsig",
              City: "Port Louis",
              PostalCode: "1620",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "P O Box 1517",
              AddressLine2: "Kempton Park ",
              Suburb: "Kempton Park",
              City: "JOHANNESBURG",
              PostalCode: "1620",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "OOSTHUIZEN ",
              ContactSurname: "MARGARET ",
              Telephone: "1234567890"
            },
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Mr Dan Rensburg",
            AccountNumber: "0000000022652418",
            TaxNumber: "43897888",
            VATNumber: "287945613"
          }
        },
        NonResident: {
          Individual: {
            Name: "abcd",
            Surname: "cdf",
            Gender: "M",
            PassportNumber: "9009050462086",
            Address: {
              AddressLine1: "35 vale Aveune",
              AddressLine2: "35 vale Aveune",
              Suburb: "NIG",
              City: "nigeria",
              PostalCode: "9001",
              State: "NG",
              Country: "NG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "0000876679"
          }
        }
      },
      customData: {
        readOnly: true,
        Valid: false,
        ADLA: false
      },
      ivsSuccessCacheMap: {}
    }
  },
  {
    name: "Own Transfer ZZ1",
    data: {
      TrnReference: "TestRef654321",
      Version: "FINSURV",
      ReportingQualifier: "NON REPORTABLE",
      Flow: "IN",
      Resident: {
        Entity: {
          TradingName: "Verma",
          EntityName: "VERMA SVRINDER KUMAR",
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "2761360578",
            Telephone: "2761305978"
          },
          AccountIdentifier: "RESIDENT OTHER",
          AccountName: "BoP Account 3",
          AccountNumber: "000090452046",
          RegistrationNumber: "12345",
          IndustrialClassification: "03",
          InstitutionalSector: "02",
          TaxNumber: "12345",
          VATNumber: "2468",
          LocationCountry: "US",
          TaxClearanceCertificateIndicator: "N"
        }
      },
      NonResident: {
        Exception: {
          ExceptionName: "CFC RESIDENT NON REPORTABLE"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "ZZ1",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1",
          LocationCountry: "US"
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "ZAR",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "ZA",
      ReceivingBank: "SBZAZAJJ"
    }
  },
  {
    name: "AuthIssuer in Auth",
    data: {
      transaction: {
        TrnReference: "TestRef223344",
        ReportingQualifier: "BOPCUS",
        TotalForeignValue: "1",
        Flow: "OUT",
        ValueDate: "01/03/2016",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "ZA",
        ReceivingBank: "SBZAZAJJ",
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: "ZZ1",
            SWIFTDetails: "as",
            MoneyTransferAgentIndicator: "MOMENTUM",
            ImportExport: [],
            ForeignValue: "1",
            OriginatingBank: "SBZAZAJJ",
            OriginatingCountry: "ZA",
            ReceivingCountry: "ZA",
            ReceivingBank: "SBZAZAJJ"
          }
        ],
        Resident: {
          Individual: {
            Name: "JOHN",
            Surname: "PETER",
            Gender: "M",
            IDNumber: "9009050462086",
            ForeignIDNumber: "9009050462086",
            DateOfBirth: "",
            StreetAddress: {
              AddressLine1: "83 Club Street",
              Suburb: "Linksfield",
              City: "Johannesburg",
              PostalCode: "2192",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "246 Abbortsford avenue",
              AddressLine2: "abbotsford",
              Suburb: "Kempton Park",
              City: "JOHANNESBURG",
              PostalCode: "2192",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "Golan",
              ContactSurname: "Golan",
              Telephone: "1234567890"
            },
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "GolanDoron",
            AccountNumber: "0000000022606068"
          }
        },
        NonResident: {
          Exception: {
            ExceptionName: "CFC RESIDENT NON REPORTABLE",
            AccountNumber: "000021701229"
          }
        }
      },
      customData: { readOnly: false, Valid: false, ADLA: false },
      ivsSuccessCacheMap: {}
    }
  },
  {
    name: "simple valid IN Entity",
    data: {
      transaction: {
        TrnReference: "TestRef98765",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "IN",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Entity: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            EntityName: "Fred Inc."
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "407",
            ForeignValue: "1000",
            LocationCountry: "US",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "US",
        ReceivingCountry: "ZA",
        ReceivingBank: "SBICUGKX"
      } //,
      // customData: {readOnly: true}
    }
  },
  {
    name: "CFC ZAR",
    data: {
      TrnReference: "TestRef098765",
      Version: "FINSURV",
      ReportingQualifier: "NON REPORTABLE",
      Flow: "OUT",
      Resident: {
        Entity: {
          EntityName: "Verma",
          StreetAddress: {},
          PostalAddress: {},
          ContactDetails: {},
          AccountIdentifier: "CFC RESIDENT",
          AccountName: "BoP Account 3",
          AccountNumber: "000090452046",
          TaxNumber: "12345",
          VATNumber: "2468"
        }
      },
      NonResident: {
        Individual: {
          Name: "Account NT 4",
          Surname: "Smith",
          Address: {
            AddressLine1: "2 Rissik Street",
            AddressLine2: "2 Rissik Street",
            City: "Johannesburg",
            PostalCode: "4001",
            Country: "US"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "000040452"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "401",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1"
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "ZAR",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "US",
      ReceivingBank: "SBZAZAJJ"
    }
  },
  {
    name: "Test for Resident Rand to Resident FCA",
    data: {
      transaction: {
        TrnReference: "TestRef55555",
        ReportingQualifier: "BOPCUS",
        TotalForeignValue: "1.24",
        Flow: "OUT",
        ValueDate: "2016-04-15",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "MU",
        ReceivingBank: "SBZAZAJJ",
        Resident: {
          Entity: {
            RegistrationNumber: "1998/020171/07",
            EntityName: "Grains for Africa Commodity Brokers (Pty) Ltd.",
            IndustrialClassification: "01",
            StreetAddress: {
              AddressLine1: "3 Seventeenth Street",
              AddressLine2: "Orange Grove",
              Suburb: "Abbotsford",
              City: "Johannesburg",
              PostalCode: "2192",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "24 abbotsford road",
              AddressLine2: "abbotford",
              Suburb: "BoksburgWest",
              City: "Johannesburg",
              PostalCode: "2192",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "benny",
              ContactSurname: "benny",
              Telephone: "1234567890"
            },
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "NBOL TEST VOST ACC 4",
            AccountNumber: "0000000002355280"
          }
        },
        NonResident: {
          Entity: {
            EntityName: "Mr Pieter van Rensburg",
            Address: {
              AddressLine1: "Jet Park Road ",
              AddressLine2: "Kempton Park ",
              Suburb: "Ruimsig",
              City: "Port Louis",
              PostalCode: "1620",
              Country: "MU"
            },
            AccountIdentifier: "FCA RESIDENT",
            AccountNumber: "000090056574"
          }
        }
      },
      customData: {
        ADLA: false
      }
    }
  },
  {
    name: "Interest rate",
    data: {
      transaction: {
        TrnReference: "TestRef54321",
        ReportingQualifier: "BOPCUS",
        TotalForeignValue: "1",
        LoanInterestRate: "0.5",
        Flow: "OUT",
        ValueDate: "19/04/2016",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "NA",
        ReceivingBank: "SBNMNANX",
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: "801",
            MoneyTransferAgentIndicator: "AYOBA",
            ImportExport: [],
            SARBAuth: {
              ADInternalAuthNumber: "12800025",
              ADInternalAuthNumberDate: "2016-04-20",
              AuthIssuer: "016"
            },
            ADInternalAuthNumber: "12800025",
            ForeignValue: "1.00",
            LocationCountry: "US",
            OriginatingBank: "SBZAZAJJ",
            OriginatingCountry: "ZA",
            ReceivingCountry: "NA",
            ReceivingBank: "SBNMNANX",
            LoanInterestRate: "1.0",
            LoanRefNumber: "123456789"
          }
        ],
        Resident: {
          Individual: {
            Name: "Adam",
            Surname: "Gilchrist",
            Gender: "M",
            IDNumber: "8006269818083",
            ForeignIDNumber: "8006269818083",
            ForeignIDCountry: "ZA",
            DateOfBirth: "1980-06-26",
            StreetAddress: {
              AddressLine1: "4 hill street",
              AddressLine2: "Winsent Avenue",
              Suburb: "Soweto",
              City: "Pimville",
              PostalCode: "1724",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "4 hill street",
              AddressLine2: "Winsent Avenue",
              Suburb: "Soweto",
              City: "Pimville",
              PostalCode: "1724",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "Reigns",
              ContactSurname: "Roman",
              Email: "Roman.Rei@co.za",
              Fax: "4567890987",
              Telephone: "5678901234"
            },
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Acc24/04/2015",
            AccountNumber: "654650004",
            TaxNumber: "PK888",
            VATNumber: "4565"
          }
        },
        NonResident: {
          Individual: {
            Name: "crossbene",
            Surname: "ravi",
            Gender: "M",
            PassportNumber: "76756567",
            Address: {
              AddressLine1: "cfddgdgd",
              City: "gfgdfgdg",
              State: "NA",
              Country: "NA"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "655247939"
          }
        }
      },
      customData: {
        readOnly: true,
        Valid: false,
        ADLA: false,
        savePredefinedBOPThirdParty: true,
        saveAdhocBOPThirdParty: true
      },
      ivsSuccessCacheMap: {}
    }
  },
  {
    name: "simple valid IN Entity - no details for remitter",
    data: {
      transaction: {
        TrnReference: "TestRef24623626",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "IN",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "407",
            ForeignValue: "1000",
            LocationCountry: "US",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "US",
        ReceivingCountry: "ZA",
        ReceivingBank: "SBICUGKX"
      } //,
      // customData: {readOnly: true}
    }
  },

  {
    name: "LARGE_DATA",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "0254/089/06/3",
            VATNumber: "4090103146",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "103",
            ForeignValue: "500",
            LocationCountry: "US",
            CategorySubCode: "01",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 200,
                ImportControlNumber: "DBN200101011234567",
                TransportDocumentNumber: "124124",
                CustomsClientNumber: "64562345",
                IVSResponse: { status: "pass" }
              }
            ],
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX"
      }
    }
  },
  {
    name: "BOPCARD NON RESIDENT",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCARD NON RESIDENT",
        Flow: "IN",
        Resident: {
          Individual: {
            Surname: "lhv Etbldb",
            Name: "Skmwp Yixojb",
            IDNumber: "7910125082084"
          }
        },
        NonResident: { Individual: {} },
        MonetaryAmount: [
          {
            MoneyTransferAgentIndicator: "CARD",
            SARBAuth: {},
            CategoryCode: "",
            CategorySubCode: "",
            SequenceNumber: 1,
            CardIndicator: "MASTER",
            ForeignCardHoldersPurchasesRandValue: "123",
            ForeignCardHoldersCashWithdrawalsRandValue: "0",
            LocationCountry: "AE"
          }
        ],
        TrnReference: "LEG3369868",
        ValueDate: "2013-02-17",
        FlowCurrency: "ZAR",
        TotalForeignValue: 75,
        IsADLA: false
      }
    }
  },
  {
    name: "Weidt ext call",
    data: {
      transaction: {
        Version: "FINSURV",
        TrnReference: "1606210155SC7722",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        ValueDate: "2016-06-21",
        FlowCurrency: "USD",
        TotalForeignValue: 167,
        BranchCode: "01015500",
        BranchName: "TPS PAYMENTS PROCESSING",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingBank: "CHASUS33",
        ReceivingCountry: "US",
        ReplacementTransaction: "N",
        Resident: {
          Individual: {
            Surname: "SGTURWG",
            Name: "PJZMTPI",
            Gender: "F",
            DateOfBirth: "1958-01-17",
            IDNumber: "5801173580081",
            AccountName: "CXHWLNG NSLWSUM",
            AccountIdentifier: "RESIDENT OTHER",
            AccountNumber: "55102824548",
            TaxNumber: "1347624938",
            VATNumber: "9579654840P",
            TaxClearanceCertificateIndicator: "N",
            CustomsClientNumber: "00032392",
            StreetAddress: {
              Province: "GAUTENG",
              AddressLine1: "GEXJZUNYC LFG 502",
              AddressLine2: "QKFDLCGL",
              Suburb: "BROOKLYN",
              City: "PRETORIA",
              PostalCode: "0181"
            },
            PostalAddress: {
              Province: "GAUTENG",
              AddressLine1: "GEXJZUNYC LFG 502",
              AddressLine2: "QKFDLCGL",
              Suburb: "BROOKLYN",
              City: "PRETORIA",
              PostalCode: "0181"
            },
            ContactDetails: {
              ContactName: "IWCGV",
              ContactSurname: "OWUKJIUW",
              Telephone: "8760841861"
            }
          }
        },
        NonResident: {
          Individual: {
            Surname: "UEGJ",
            Name: "UEGJ",
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "46876543",
            Address: { Country: "US" }
          }
        },
        MonetaryAmount: [
          {
            MoneyTransferAgentIndicator: "AD",
            RandValue: 1032.13,
            ForeignValue: 100,
            SARBAuth: { RulingsSection: "B.1(G)(V)" },
            CategoryCode: "106",
            CategorySubCode: "",
            LoanRefNumber: "99789078907890",
            LocationCountry: "NA",
            SequenceNumber: 1
          },
          {
            MoneyTransferAgentIndicator: "AD",
            RandValue: 691.53,
            ForeignValue: 67,
            SARBAuth: { RulingsSection: "B.1(G)(V)" },
            CategoryCode: "106",
            CategorySubCode: "",
            LoanRefNumber: "8200008343461",
            LocationCountry: "US",
            SequenceNumber: 2
          }
        ]
      },
      customData: {
        DealerType: "AD",
        ShowOldCodes: false,
        AccountDrCr: "CR",
        TotalDomesticAmount: 1723.66
      }
    }
  },
  {
    name: "BOPCARD RESIDENT",
    data: {
      transaction: {
        Version: "FINSURV",
        TrnReference: "8FD229D540DB1EE7A3AE5C0FAD249A",
        ReportingQualifier: "BOPCARD RESIDENT",
        Flow: "IN",
        ValueDate: "2017-08-29",
        FlowCurrency: "ZAR",
        TotalForeignValue: 810.66,
        ReplacementTransaction: "N",
        Resident: {
          Individual: {
            Surname: "Stavrinos",
            Name: "Angie",
            Gender: "F",
            DateOfBirth: "1980 - 02 - 14",
            IDNumber: "8002140092089",
            TempResPermitNumber: "0000000000000",
            AccountName: "DISCOVERY PLATINUM CARD",
            AccountIdentifier: "CREDIT CARD",
            AccountNumber: "08307185352376372",
            StreetAddress: {
              AddressLine1: "155 West",
              AddressLine2: "Sandton",
              Suburb: "",
              City: "Johannesburg",
              PostalCode: "2146"
            },
            ContactDetails: {
              ContactName: "Angie",
              ContactSurname: "Stavrinos",
              Email: "Angie@testing.co.za",
              Telephone: ""
            }
          },
          Description: "Angie, Stavrinos"
        },
        NonResident: {
          Entity: {
            CardMerchantName: "VISA",
            CardMerchantCode: "asss",
            Address: {}
          }
        },
        MonetaryAmount: [
          {
            MoneyTransferAgentIndicator: "CARD",
            RandValue: 810.66,
            ForeignValue: 810.66,
            SARBAuth: {},
            CategoryCode: "",
            CategorySubCode: "",
            ThirdParty: {
              Individual: {},
              StreetAddress: {},
              PostalAddress: {},
              ContactDetails: {}
            },
            CardChargeBack: "N",
            CardIndicator: "VISA",
            ElectronicCommerceIndicator: "0",
            POSEntryMode: "09",
            SequenceNumber: 1,
            Description:
              "Adjustments / Reversals / Refunds applicable to merchandise",
            ThirdPartyKind: "Individual"
          }
        ]
      },
      customData: {
        DealerType: "AD",
        ShowOldCodes: true,
        AccountDrCr: "CR",
        TotalDomesticAmount: 810.66,
        snapShot: {
          Version: "FINSURV",
          TrnReference: "8FD229D540DB1EE7A3AE5C0FAD249A",
          ReportingQualifier: "BOPCARD RESIDENT",
          Flow: "IN",
          ValueDate: "2017 - 08 - 29",
          FlowCurrency: "ZAR",
          TotalForeignValue: 810.66,
          ReplacementTransaction: "N",
          Resident: {
            Individual: {
              Surname: "Stavrinos",
              Name: "Angie",
              Gender: "F",
              DateOfBirth: "1980 - 02 - 14",
              IDNumber: "8002140092089",
              TempResPermitNumber: "0000000000000",
              AccountName: "DISCOVERY PLATINUM CARD",
              AccountIdentifier: "CREDIT CARD",
              AccountNumber: "08307185352376372",
              StreetAddress: {
                AddressLine1: "155 West",
                AddressLine2: "Sandton",
                Suburb: "",
                City: "Johannesburg",
                PostalCode: "2146"
              },
              ContactDetails: {
                ContactName: "Angie",
                ContactSurname: "Stavrinos",
                Email: "Angie@testing.co.za",
                Telephone: ""
              }
            }
          },
          NonResident: {
            Entity: { CardMerchantName: "", CardMerchantCode: "", Address: {} }
          },
          MonetaryAmount: [
            {
              MoneyTransferAgentIndicator: "CARD",
              RandValue: 810.66,
              ForeignValue: 810.66,
              SARBAuth: {},
              CategoryCode: "",
              CategorySubCode: "",
              ThirdParty: {
                Individual: {},
                StreetAddress: {},
                PostalAddress: {},
                ContactDetails: {}
              },
              CardChargeBack: "N",
              CardIndicator: "VISA",
              ElectronicCommerceIndicator: "0",
              POSEntryMode: "09",
              CardFraudulentTransactionIndicator: "N",
              SequenceNumber: 1
            }
          ]
        }
      }
    }
  },
  {
    name: "ieBork",
    data: {
      customData: {
        DealerType: "AD",
        Corporate: "Acme Ltd",
        Division: "West",
        OVID: "123456789",
        edited: true,
        isValid: true
      },
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG",
              Country: "ZA"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080",
            CustomsClientNumber: "12341234"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "101",
            ForeignValue: "999",
            LocationCountry: "US",
            SARBAuth: {},
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1",
            TravelMode: {},
            ThirdParty: { Individual: {} },
            CategorySubCode: "01",
            ImportExport: [
              {
                SubsequenceNumber: "1",
                PaymentCurrencyCode: "USD",
                ImportControlNumber: "INVsf",
                PaymentValue: "999"
              }
            ]
          },
          {
            CategoryCode: "101",
            CategorySubCode: "02",
            ThirdParty: { Individual: {} },
            SequenceNumber: "2",
            SARBAuth: {},
            ForeignValue: "1",
            ImportExport: [
              {
                SubsequenceNumber: "1",
                PaymentCurrencyCode: "USD",
                ImportControlNumber: "INVfdsf",
                PaymentValue: "1"
              }
            ],
            TravelMode: {}
          }
        ],
        TotalValue: 1000,
        TrnReference: "TestRef12414",
        AccountHolderStatus: "South African Resident",
        LocationCountry: "NZ",
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG",
              State: "GAUTENG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Gender: "M",
            Surname: "Georgeson"
          }
        },
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        PaymentDetail: {
          IsCorrespondentBank: "N",
          BeneficiaryBank: { BankName: "Foo Bank" },
          SWIFTDetails: "Payment"
        },
        TransactionCurrency: "USD",
        RateConfirmation: "Y",
        IsInvestecFCA: "N"
      }
    }
  },
  {
    name: "InfoProvidedInv1",
    data: {
      customData: { OVID: "123456789" },
      transaction: {
        TrnReference: "InfoProvidedInv1",
        Flow: "OUT",
        ValueDate: "2017-05-10",
        ReportingQualifier: "BOPCUS",
        OriginatingBank: "IVESZAJJ0",
        OriginatingCountry: "ZA",
        FlowCurrency: "USD",
        TotalForeignValue: "1000.00",
        Resident: {
          Entity: {
            PostalAddress: {
              AddressLine2: "ACME Bank",
              AddressLine1: "Ground Floor",
              Suburb: "Johannesburg",
              PostalCode: "2001",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            StreetAddress: {
              AddressLine2: "ACME Bank",
              AddressLine1: "Ground Floor",
              Suburb: "Johannesburg",
              PostalCode: "2001",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            EntityName: "ACME BK OF ZA LTD",
            ContactDetails: {
              ContactSurname: "Fudd",
              Email: "elma.fudd@acme.co.za",
              Telephone: "27116365094",
              ContactName: "Elma"
            },
            AccountIdentifier: "RESIDENT OTHER",
            IndustrialClassification: "08",
            InstitutionalSector: "01",
            TradingName: "ACME BK OF ZA LTD",
            RegistrationNumber: "196200073806",
            AccountName: "CONTRA MTSS TREAS DIV",
            AccountNumber: "9864660"
          }
        }
      }
    }
  },
  {
    name: "InfoMissingInv1",
    data: {
      customData: { OVID: "123456789" },
      transaction: {
        ReportingQualifier: "BOPCUS",
        TrnReference: "InfoMissingInv1",
        Flow: "OUT",
        ValueDate: "2017-05-10",
        OriginatingBank: "IVESZAJJ0",
        OriginatingCountry: "ZA",
        FlowCurrency: "USD",
        TotalForeignValue: "1000.00",
        Resident: {
          Entity: {
            TradingName: "ACME BK OF ZA LTD",
            RegistrationNumber: "196200073806",
            PostalAddress: {
              AddressLine1: "Ground Floor",
              AddressLine2: "ACME Bank",
              Suburb: "Johannesburg",
              PostalCode: "",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            StreetAddress: {
              AddressLine1: "Ground Floor",
              AddressLine2: "ACME Bank",
              Suburb: "Johannesburg",
              PostalCode: "2001",
              City: "Johannesburg",
              Province: ""
            },
            EntityName: "ACME BK OF ZA LTD",
            ContactDetails: {
              ContactSurname: "Fudd",
              Email: "elma.fudd@acme.co.za",
              Telephone: "27116365094",
              ContactName: "Elma"
            },
            AccountIdentifier: "",
            AccountName: "",
            AccountNumber: ""
          }
        }
      }
    }
  },
  {
    name: "CARD errors",
    data: {
      customData: { OVID: "123456789" },
      transaction: {
        ReportingQualifier: "BOPCARD RESIDENT",
        TrnReference: "InfoMissingInv1",
        Flow: "IN",
        ValueDate: "2017-05-10",
        OriginatingBank: "IVESZAJJ0",
        OriginatingCountry: "ZA",
        FlowCurrency: "USD",
        TotalForeignValue: "1000.00",
        Resident: {
          Entity: {
            TradingName: "ACME BK OF ZA LTD",
            RegistrationNumber: "196200073806",
            PostalAddress: {
              AddressLine1: "Ground Floor",
              AddressLine2: "ACME Bank",
              Suburb: "Johannesburg",
              PostalCode: "",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            StreetAddress: {
              AddressLine1: "Ground Floor",
              AddressLine2: "ACME Bank",
              Suburb: "Johannesburg",
              PostalCode: "2001",
              City: "Johannesburg",
              Province: ""
            },
            EntityName: "ACME BK OF ZA LTD",
            ContactDetails: {
              ContactSurname: "Fudd",
              Email: "elma.fudd@acme.co.za",
              Telephone: "27116365094",
              ContactName: "Elma"
            },
            AccountIdentifier: "",
            AccountName: "",
            AccountNumber: ""
          }
        }
      }
    }
  },
    {
    name: "SDA Test Set",
    data: {
      customData: {
        PaymentType: "ADVANCE_PAYMENT"
      },
      transaction: {
        TrnReference: "TestRef12424",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "Uma-Looma",
            Surname: "Johnson",
            CustomsClientNumber:"00034786",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "0254/089/06/3",
            VATNumber: "4090103146",
            Gender: "F",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {

            },
          }
        },
        MonetaryAmount: [
          {
            ForeignValue: "700",
            RandValue: "900000",
            LocationCountry: "US",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 700,
                SubsequenceNumber: "1"
              }
            ],
            SARBAuth: {

            },
            SequenceNumber: "1",
            AdHocRequirement: { Subject: "SDA" }
          },
          {
            ForeignValue: "700",
            RandValue: "100000",
            LocationCountry: "US",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 700,
                SubsequenceNumber: "1"
              }
            ],
            SARBAuth: {

            },
            SequenceNumber: "1",
            AdHocRequirement: { Subject: "SDA" }
          },
          {
            ForeignValue: "700",
            RandValue: "1000001",
            LocationCountry: "US",
            ThirdPartyKind: "Individual",
            ThirdParty: {
              Individual: {
                Surname: "Surname1",
                Name: "Name1",
                Gender: "M",
                DateOfBirth: "2006-05-04",
                IDNumber: "8902205150088",
                PassportNumber: "12312312312312",
                PassportCountry: "ZA"
              }
            },
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 700,
                SubsequenceNumber: "1"
              }
            ],
            SARBAuth: {

            },
            SequenceNumber: "1",
          }
        ],
        TotalForeignValue: "700",
        TotalDomesticValue: "1000001",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
      }
    }
  },
  {
    name: "NBOL Testing",
    data: {
      customData: {
        allowAdhocBOPThirdPartyCapture: "true",
        allowSaveOfAdhocThirdParty: "true"
      },
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2613605978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "US"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "101/01",
            ForeignValue: "1000",
            LocationCountry: "US",
            ThirdPartyKind: "Individual",
            ThirdParty: {
              Individual: {
                Surname: "Surname1",
                Name: "Name1",
                Gender: "M",
                DateOfBirth: "2006-05-04",
                IDNumber: "8902205150088",
                PassportNumber: "12312312312312",
                PassportCountry: "ZA"
              },
              TaxNumber: "TaxNumber0",
              VATNumber: "VATNumber0",
              CustomsClientNumber: "12341234",
              StreetAddress: {
                AddressLine1: "AddressLine10",
                AddressLine2: "AddressLine20",
                Suburb: "Suburb0",
                City: "City0",
                PostalCode: "1244",
                Province: "GAUTENG"
              },
              PostalAddress: {
                AddressLine1: "AddressLine11",
                AddressLine2: "AddressLine21",
                Suburb: "Suburb1",
                City: "City1",
                PostalCode: "1234",
                Province: "GAUTENG"
              },
              ContactDetails: {
                ContactName: "ContactName0",
                ContactSurname: "ContactSurname0",
                Email: "Email0",
                Fax: "Fax1111111",
                Telephone: "Telephone0"
              }
            },
            Category: "100",
            SARBAuth: { AuthIssuer: "123" },
            MoneyTransferAgentIndicator: "WESTERNUNION",
            SequenceNumber: "1",
            allowAdhocBOPThirdParty: "true"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX"
      }
    }
  }
];

// Since this dummy data needs to be tested in the Dev source and the Built source,
// it needs to work with requirejs and without it. So, cannot assume requirejs' 'define' will be
// defined.

if (typeof define !== "undefined")
  define({
    data: testData
  });
