define(function() {
  return function(testBase) {
    with (testBase) {
      var mappings = {
        LocalCurrencySymbol: "R",
        LocalCurrencyName: "Rand",
        LocalCurrency: "ZAR",
        LocalValue: "RandValue",
        Regulator: "SARB",
        DealerPrefix: "AD",
        RegulatorPrefix: "SARB"
      };

      setMappings(mappings);

      var test_cases = [
        // 100 - Out
        assertDocuments("100-Out", ["OriginalTransaction"], {
         ReportingQualifier: 'BOPCUS', Flow: 'OUT',
         MonetaryAmount: [{CategoryCode: '100', ReversalTrnRefNumber: "OrigRef01"}]
        }),
        // 100 - IN
        assertDocuments("100-IN", ["OriginalTransaction"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{CategoryCode: '100', ReversalTrnRefNumber: "OrigRef01"}]
         }),
        // 101/01 - Out
        assertDocuments("101/01-Out", ["SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            CategoryCode: '101', CategorySubCode: '01',
            ImportExport: [{ImportControlNumber: 'XYZ'}]
          }]
        }),
        // 101/11 - Out
        assertDocuments("101/11-Out", ["SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            CategoryCode: '101', CategorySubCode: '11',
            ImportExport: [{ImportControlNumber: 'XYZ'}]
          }]
        }),
        // 103 - Out
        assertDocuments("103-Out", ["1:1:CustomsDeclaration", "1:1:TransportDocument", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          { 
            CategoryCode: '103', CategorySubCode:'01',            
            ImportExport: [{ImportControlNumber: 'INV123', TransportDocumentNumber: 'XYZ'}], 
            TransportDocumentNumber:'123'
          }]
        }),
        // 103 - IN
        assertDocuments("103-IN", ["1:1:ConsignmentReference"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
            CategoryCode: '103',  CategorySubCode:'01',          
             ImportExport: [{UCR: 'XYZ'}],         
            }]
          }),
        // 105 - Out
        assertDocuments("105-Out", ["SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            CategoryCode: '105',
            ImportExport: [{ImportControlNumber: 'INV123'}]
          }]
        }),
        // 105 - In
        assertDocuments("105-In", ["ConsignmentReference"], {
        ReportingQualifier: 'BOPCUS', Flow: 'IN',
        MonetaryAmount: [
          {
            CategoryCode: '105',                    
            ImportExport: [{UCR: '8ZA12345678ABCDE'}],
          }]
        }),
        // 106 - Out
        assertDocuments("106-Out", ["RegulatorApproval","FinancierInvoice", "SupplierInvoice", "TransportDocument"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            CategoryCode: '106',
            ImportExport: [{ImportControlNumber: 'INV123', TransportDocumentNumber: 'XYZ'}],
            SARBAuth: {SARBAuthRefNumber: 'Ref01'},
          }]
        }),
        // 106 - Out - LUClient
        assertDocuments("106-Out-LU", [], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            CategoryCode: '106',
            ImportExport: [{ImportControlNumber: 'INV123', TransportDocumentNumber: 'XYZ'}],
            SARBAuth: {SARBAuthRefNumber: 'Ref01'},
          }]
        }, {
          LUClient:"Y"
        }),
          // 106 - IN
        assertDocuments("106-IN", ["1:1:ConsignmentReference"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',   
          MonetaryAmount: [
            {
              CategoryCode: '106',
              ImportExport: [{UCR: 'XYZ'}],
            }]
          }),
          // 106 - IN - LUClient
        assertDocuments("106-IN-LU", [], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',   
          MonetaryAmount: [
            {
              CategoryCode: '106',
              ImportExport: [{UCR: 'XYZ'}],
            }]
          }, {
            LUClient:"Y"
          }),
        // 107 - Out
        assertDocuments("107-Out", [], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            CategoryCode: '107',
            ImportExport: [{ImportControlNumber: 'INV123'}]
          }]
        }),
        // 108 - Out
        assertDocuments("108-Out", ["SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '108'
          }]
        }),
        // 109 - In
        assertDocuments("109-In", ["ExportDeclaration"], {
        ReportingQualifier: 'BOPCUS', Flow: 'IN',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '109'
          }]
        }),
        // 109 - Out
        assertDocuments("109-Out", ["SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '109' 
          }]
        }),
        // 110 - Out
        assertDocuments("110-Out", ["SupplierInvoice", "ClientInvoice", "ProofOfFunds","Agreement",  "RegulatorApproval"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '110',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 110 - IN
        assertDocuments("110-IN", ["SupplierInvoice", "ProofOfFunds","Agreement", "RegulatorApproval"], {
        ReportingQualifier: 'BOPCUS', Flow: 'IN',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '110',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 200 - Out
        assertDocuments("200-Out", ["OriginalTransaction"], {
         ReportingQualifier: 'BOPCUS', Flow: 'OUT',
         MonetaryAmount: [{CategoryCode: '200', ReversalTrnRefNumber: "OrigRef01"}]
        }),
        // 200 - IN
        assertDocuments("200-IN", ["OriginalTransaction"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [{CategoryCode: '200', ReversalTrnRefNumber: "OrigRef01"}]
         }),
        // 201 - Out
        assertDocuments("201-Out", ["RegulatorApproval", "Contract", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '201',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 201 - IN
        assertDocuments("201-IN", ["RegulatorApproval", "Contract", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'IN',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '201',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 202 - Out
        assertDocuments("202-Out", ["RegulatorApproval","Contract", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '202',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 202 - IN
        assertDocuments("202-IN", ["RegulatorApproval","Contract", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '202',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 203 - Out
        assertDocuments("203-Out", ["RegulatorApproval","Contract", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '203',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}            
          }]
        }),
        // 203 - IN
        assertDocuments("203-IN", ["RegulatorApproval","Contract", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'IN',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '203',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 204 - Out
        assertDocuments("204-Out", ["RegulatorApproval","Contract", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '204',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 204 - IN
        assertDocuments("204-IN", ["RegulatorApproval","Contract", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'IN',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '204',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 205 - Out
        assertDocuments("205-Out", ["RegulatorApproval", "Contract", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '205',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 205 - IN
        assertDocuments("205-IN", ["RegulatorApproval", "Contract", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'IN',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '205',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 210 - Out
        assertDocuments("210-Out", ["RegulatorApproval","Contract", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '210',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
        }),
        // 210 - IN
        assertDocuments("210-IN", ["RegulatorApproval","Contract", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'IN',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '210',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 211 - Out
        assertDocuments("211-Out", ["RegulatorApproval","Contract", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '211',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
        }),
        // 211 - IN
        assertDocuments("211-IN", ["RegulatorApproval","Contract", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'IN',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '211',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 212 - Out
        assertDocuments("212-Out", ["Contract", "SupplierInvoice","RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '212',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
        }),
        // 212 - IN
        assertDocuments("212-IN", ["Contract", "SupplierInvoice","RegulatorApproval"], {
        ReportingQualifier: 'BOPCUS', Flow: 'IN',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '212',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 213 - Out
        assertDocuments("213-Out", ["Contract","RegulatorApproval", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '213',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 213 - IN
        assertDocuments("213-IN", ["Contract","RegulatorApproval", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '213',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 220 - Out
        assertDocuments("220-Out", ["Contract","RegulatorApproval", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '220',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 220 - IN
        assertDocuments("220-IN", ["Contract","RegulatorApproval", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '220',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 221 - Out
        assertDocuments("221-Out", ["Contract","RegulatorApproval", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '221',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 221 - IN
        assertDocuments("221-IN", ["Contract","RegulatorApproval", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '221',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
        }),
        // 225 - Out
        assertDocuments("225-Out", ["Contract","RegulatorApproval", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '225',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 225 - IN
        assertDocuments("225-IN", ["Contract","RegulatorApproval", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '225',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 226 - Out
        assertDocuments("226-Out", ["Contract","RegulatorApproval", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '226',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 226 - IN
        assertDocuments("226-IN", ["Contract","RegulatorApproval", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '226',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
        }),
        // 230 - Out
        assertDocuments("230-Out", ["Contract","RegulatorApproval", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '230',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 230 - IN
        assertDocuments("230-IN", ["Contract","RegulatorApproval", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '230',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 231 - Out
        assertDocuments("231-Out", ["SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '231'
          }]
        }),
        // 231 - IN
        assertDocuments("231-IN", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '231'
            }]
          }),
        // 232 - Out
        assertDocuments("232-Out", ["Contract",,"RegulatorApproval", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '232',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 232 - IN
        assertDocuments("232-IN", ["Contract","RegulatorApproval", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '232',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 233 - Out
        assertDocuments("233-Out", ["Contract","RegulatorApproval", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '233',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 233 - IN
        assertDocuments("233-IN", ["Contract","RegulatorApproval", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '233',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 234 - Out
        assertDocuments("234-Out", ["Contract","RegulatorApproval", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '234',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 234 - IN
        assertDocuments("234-IN", ["Contract","RegulatorApproval", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '234',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 235 - Out
        assertDocuments("235-Out", ["Contract","RegulatorApproval", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '235',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 235 - IN
        assertDocuments("235-IN", ["Contract","RegulatorApproval", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '235',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 236 - Out
        assertDocuments("236-Out", ["SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '236'
          }]
        }),
        // 236 - IN
        assertDocuments("236-IN", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '236'
            }]
          }),
        // 240 - Out
        assertDocuments("240-Out", ["Contract","SupplierInvoice", "RegulatorApproval"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '240', CategorySubCode: '01',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 240 - IN
        assertDocuments("240-IN", ["Contract","SupplierInvoice", "RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '240', CategorySubCode: '01',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 241 - Out
        assertDocuments("241-Out", ["RegulatorApproval","Contract", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '241',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 241 - IN
        assertDocuments("241-IN", ["RegulatorApproval","Contract", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '241',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 242 - Out
        assertDocuments("242-Out", ["RegulatorApproval","Contract", "SupplierInvoice"], {
        ReportingQualifier: 'BOPCUS', Flow: 'OUT',
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: '242',
            SARBAuth: {SARBAuthRefNumber: 'Ref01'}
          }]
        }),
        // 242 - IN
        assertDocuments("242-IN", ["RegulatorApproval","Contract", "SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '242',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
          }),
        // 243 - Out
        assertDocuments("243-Out", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '243',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
        }),
        // 243 - IN
        assertDocuments("243-IN", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '243',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }]
        }),
        // 250 - Out
        assertDocuments("250-Out", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '250'
            }]
        }),
        // 250 - IN
        assertDocuments("250-IN", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '250'
            }]
        }),
        // 251 - Out
        assertDocuments("251-Out", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '251'
            }]
        }),
        // 251 - IN
        assertDocuments("251-IN", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '251'
            }]
        }),
        // 255 - Out
        assertDocuments("255-Out", ["PassportDocument", "ProofOfTravel"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: {Entity: {}},
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '255',
              ThirdParty: {Individual: {PassportNumber: '12345', Name: "John", Surname: "Doe"}}
            }]
        }),
        // 255 - IN
        assertDocuments("255-IN", ["PassportDocument", "ProofOfTravel"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: {Entity: {}},
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '255',
              ThirdParty: {Individual: {PassportNumber: '12345', Name: "John", Surname: "Doe"}}
            }]
        }),

        // 256 - Out 
        assertDocuments("256-Out", ["PassportDocument", "ProofOfTravel"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: {Individual: {PassportNumber: '987633', Name: "Res", Surname: "Individual"}},
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '256'
            }
          ]
        }),
        // 256 - Out 
        assertDocuments("256-Out", ["PassportDocument", "ProofOfTravel"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: {Entity: {}},
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '256',
              ThirdParty: {Individual: {PassportNumber: '12345', Name: "Third", Surname: "Party"}}
            }
          ]
        }),
        //256 - Out
        assertDocuments("256-Out", ["1:PassportDocument", "1:ProofOfTravel", "2:PassportDocument", "2:ProofOfTravel", "3:RegulatorApproval","3:SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: {Individual: {PassportNumber: '987633', Name: "Res", Surname: "Individual"}},
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '256'
            },
            {
              SequenceNumber: "2",
              CategoryCode: '256',
              ThirdParty: {Individual: {PassportNumber: '12345', Name: "Third", Surname: "Party"}}
            },
            {
              SequenceNumber: "3",
              CategoryCode: '411',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'},
              CategorySubCode: "01"
            }
          ]
        }),
        // 256 - IN
        assertDocuments("256-IN", ["1:PassportDocument", "1:ProofOfTravel", "2:PassportDocument", "2:ProofOfTravel", "3:RegulatorApproval","3:SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          Resident: {Individual: {PassportNumber: '987633', Name: "Res", Surname: "Individual"}},
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '256'
            },
            {
              SequenceNumber: "2",
              CategoryCode: '256',
              ThirdParty: {Individual: {PassportNumber: '12345', Name: "Third", Surname: "Party"}}
            },
            {
              SequenceNumber: "3",
              CategoryCode: '411',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'},
              CategorySubCode: "01"
            }
          ]
        }),
        // 260 - Out
        assertDocuments("260-Out", ["AgentInvoice", "ForeignInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '260'
            }
          ]
        }),
         // 261 - Out
         assertDocuments("261-Out", ["AgentInvoice", "ForeignInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '261'
            }
          ]
        }),
        // 265 - Out
        assertDocuments("265-Out", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '265',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 265 - IN
        assertDocuments("265-IN", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '265',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 266 - Out
        assertDocuments("266-Out", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '266',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 266 - IN
        assertDocuments("266-IN", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '266',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 270 - Out
        assertDocuments("270-Out", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '270'}
          ]
        }),
        // 270 - IN
        assertDocuments("270-IN", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '270'}
          ]
        }),
        // 271 - Out
        assertDocuments("271-Out", ["SupplierInvoice","ImportInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '271'}
          ]
        }),
        // 271 - IN
        assertDocuments("271-IN", ["ExportInvoice","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '271'}
          ]
        }),
        // 272 - Out
        assertDocuments("272-Out", ["SupplierInvoice","ImportInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '272'
            }
          ]
        }),
        // 272 - IN
        assertDocuments("272-IN", ["SupplierInvoice","ExportInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '272'}
          ]
        }),
        // 273 - Out
        assertDocuments("273-Out", ["SupplierInvoice"], {
           ReportingQualifier: 'BOPCUS', Flow: 'OUT',
           MonetaryAmount: [
             {
                SequenceNumber: "1",
                CategoryCode: '273'
             }
           ]
          }),
        // 273 - IN
        assertDocuments("273-IN", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '273'
            }
          ]
         }),
        // 275 - Out
        assertDocuments("275-Out", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '275'
            }
          ]
         }),
         // 275 - IN
        assertDocuments("275-IN", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '275'
            }
          ]
         }),
         // 276 - Out
        assertDocuments("276-Out", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '276',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 276 - IN
        assertDocuments("276-IN", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '276',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 280 - Out
        assertDocuments("280-Out", ["SupplierInvoice"], {
             ReportingQualifier: 'BOPCUS', Flow: 'OUT',
             MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '275'}
          ]
        }),
        // 280 - IN
        assertDocuments("280-IN", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
          {
           SequenceNumber: "1",
           CategoryCode: '275'
          }
        ]
       }),
        // 281 - IN
        assertDocuments("281-IN", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '281',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 282 - OUT
        assertDocuments("282-OUT", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '282'
            }
          ]
        }),
        // 282 - IN
        assertDocuments("282-IN", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '282'
            }
          ]
        }),
        // 285 - OUT
        assertDocuments("285-OUT", ["EnrollmentConfirmation","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '285'
            }
          ]
        }),
        // 285 - IN
        assertDocuments("285-IN", ["EnrollmentConfirmation","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '285'
            }
          ]
        }),
        // 287 - OUT
        assertDocuments("287-OUT", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '287'
            }
          ]
        }),
        // 287 - IN
        assertDocuments("287-IN", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '287'
            }
          ]
        }),
        // 288 - OUT
        assertDocuments("288-OUT", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '288',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 288 - IN
        assertDocuments("288-IN", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '288',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 289 - OUT
        assertDocuments("289-OUT", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '289',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 289 - IN
        assertDocuments("289-IN", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '289',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 290 - OUT
        assertDocuments("290-OUT", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '290',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 290 - IN
        assertDocuments("290-IN", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '290',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 291 - OUT
        assertDocuments("291-OUT", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '291',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 291 - IN
        assertDocuments("291-IN", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '291',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 292 - OUT
        assertDocuments("292-OUT", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '292',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 292 - IN
        assertDocuments("292-IN", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '292',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 293 - OUT
        assertDocuments("293-OUT", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '293',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 293 - IN
        assertDocuments("293-IN", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '293',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 294 - OUT
        assertDocuments("294-OUT", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '294'
            }
          ]
        }),
        // 294 - IN
        assertDocuments("294-IN", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '294'
            }
          ]
        }),
        // 295 - OUT
        assertDocuments("295-OUT", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '295',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 295 - IN
        assertDocuments("295-IN", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '295',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 296 - OUT
        assertDocuments("296-OUT", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '296'
            }
          ]
        }),
        // 296 - IN
        assertDocuments("296-IN", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '296'
            }
          ]
        }),
        // 297 - OUT
        assertDocuments("297-OUT", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '297',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 297 - IN
        assertDocuments("297-IN", ["RegulatorApproval","Contract","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '297',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 300 - OUT
        assertDocuments("300-OUT", ["OriginalTransaction"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              ReversalTrnRefNumber: "Ref02",
              CategoryCode: '300'
            }
          ]
        }),
        // 300 - IN
        assertDocuments("300-IN", ["OriginalTransaction"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              ReversalTrnRefNumber: "Ref02",
              CategoryCode: '300'
            }
          ]
        }),
        // 301 - OUT
        assertDocuments("301-OUT", ["ShareCertificate","AuditorLetter","BoardResolution"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '301'
            }
          ]
        }),
         // 302 - OUT
        assertDocuments("302-OUT", ["AuditorLetter","BoardResolution"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '302'
            }
          ]
        }),
        // 302 - OUT
        assertDocuments("302-OUT", ["AuditorLetter","BoardResolution"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '302'
            }
          ]
        }),
        // 303 - OUT
        assertDocuments("303-OUT", ["RegulatorApproval","Contract"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '303',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 304 - OUT
        assertDocuments("304-OUT", ["PassportDocument","Contract"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '304',
              NonResident	: {Individual: {PassportNumber: '12345678'}}
            }
          ]
        }),
        // 305 - OUT
        assertDocuments("305-OUT", ["PassportDocument","ProofOfEarnings","FTRDeclaration"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '305',
              NonResident	: {Individual: {PassportNumber: '12345678'}}
            }
          ]
        }),
        // 306 - OUT
        assertDocuments("306-OUT", ["PassportDocument","ProofOfEarnings","FTRDeclaration"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '306',
              NonResident	: {Individual: {PassportNumber: '12345678'}}
            }
          ]
        }),
        // 307 - OUT
        assertDocuments("307-OUT", ["SupplierInvoice","RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '307',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 308 - OUT
        assertDocuments("308-OUT", ["RentalAgreement"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '308'
            }
          ]
        }),
        // 309 - IN
        assertDocuments("309-IN", ["InvoiceOrCalculations","RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '309',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 309 - OUT
        assertDocuments("309-OUT", ["InvoiceOrCalculations","RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '309',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 312 - IN
        assertDocuments("312-IN", ["SupplierInvoice","RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '312',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 312 - OUT
        assertDocuments("312-OUT", ["SupplierInvoice","RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '312',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 400 - OUT
        assertDocuments("400-OUT", ["OriginalTransaction"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              ReversalTrnRefNumber: "Ref02",
              CategoryCode: '400'
            }
          ]
        }),
        // 400 - IN
        assertDocuments("400-IN", ["OriginalTransaction"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              ReversalTrnRefNumber: "Ref02",
              CategoryCode: '400'
            }
          ]
        }),
        // 402 - OUT
        assertDocuments("402-OUT", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '402'
            }
          ]
        }),
        // 402 - IN
        assertDocuments("402-IN", ["SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '402'
            }
          ]
        }),
        // 403 - OUT
        assertDocuments("403-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '403',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 403 - IN
        assertDocuments("403-IN", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              CategoryCode: '403',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 404 - OUT
        assertDocuments("404-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '404',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 404 - IN
        assertDocuments("404-IN", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              CategoryCode: '404',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 405 - OUT
        assertDocuments("405-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '405',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 405 - IN
        assertDocuments("405-IN", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              CategoryCode: '405',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 406 - OUT
        assertDocuments("406-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '406',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 406 - IN
        assertDocuments("406-IN", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              CategoryCode: '406',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 407 - OUT
        assertDocuments("407-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '407',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 407 - IN
        assertDocuments("407-IN", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              CategoryCode: '407',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 408 - OUT
        assertDocuments("408-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '408',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 408 - IN
        assertDocuments("408-IN", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              CategoryCode: '408',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 409 - OUT
        assertDocuments("409-OUT", ["PassportDocument","DeathCertificate","Will","ExecutorLetter"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '409',
              NonResident	: {Individual: {PassportNumber: '12345678'}}
            }
          ]
        }),
        // 409 - IN
        assertDocuments("409-IN", ["PassportDocument","DeathCertificate","Will","ExecutorLetter"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '409',
              NonResident	: {Individual: {PassportNumber: '12345678'}}
            }
          ]
        }),
        // 410 - OUT
        assertDocuments("410-OUT", ["CourtOrder"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              
              SequenceNumber: "1",
              CategoryCode: '410',
            }
          ]
        }),
        // 410 - IN
        assertDocuments("410-IN", ["CourtOrder"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              
              SequenceNumber: "1",
              CategoryCode: '410',
            }
          ]
        }),
        // 411 - OUT
        assertDocuments("411-OUT", ["RegulatorApproval","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '411',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 411 - IN
        assertDocuments("411-IN", ["RegulatorApproval","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '411',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 412 - OUT
        assertDocuments("412-OUT", ["RegulatorApproval","FSBApproval","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",              
              CategoryCode: '412',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 412 - IN
        assertDocuments("412-IN", ["RegulatorApproval","FSBApproval","SupplierInvoice"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",              
              CategoryCode: '412',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 413 - OUT
        assertDocuments("413-OUT", ["RegulatorApproval","FSBApproval","SupplierInvoice","Policy","ClaimSupport"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '413',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 413 - IN
        assertDocuments("413-IN", ["RegulatorApproval","FSBApproval","SupplierInvoice","Policy","ClaimSupport"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '413',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 414 - OUT
        assertDocuments("414-OUT", ["RegulatorApproval","SupplierInvoice","FSBApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '414',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 414 - IN
        assertDocuments("414-IN", ["RegulatorApproval","SupplierInvoice","FSBApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '414',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 415 - OUT
        assertDocuments("415-OUT", ["RegulatorApproval","SupplierInvoice","FSBApproval","ClaimSupport","Policy"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '415',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 415 - IN
        assertDocuments("415-IN", ["RegulatorApproval","SupplierInvoice","FSBApproval","ClaimSupport","Policy"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              SequenceNumber: "1",
              CategoryCode: '415',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 500 - OUT
        assertDocuments("500-OUT", ["OriginalTransaction"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '500',
              ReversalTrnRefNumber:'Ref01'
            }
          ]
        }),
        // 500 - IN
        assertDocuments("500-IN", ["OriginalTransaction"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              CategoryCode: '500',
              ReversalTrnRefNumber:'Ref01'
            }
          ]
        }),
        // 501 - OUT
        assertDocuments("501-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '501',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 501 - IN
        assertDocuments("501-IN", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              CategoryCode: '501',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 502 - OUT
        assertDocuments("502-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '502',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 502 - IN
        assertDocuments("502-IN", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              CategoryCode: '502',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 503 - OUT
        assertDocuments("503-OUT", ["FairValue","ProofOfFunds","Agreement"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '503',
            }
          ]
        }),
        // 504 - OUT
        assertDocuments("504-OUT", ["RegulatorApproval","Agreement"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '504',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 510/01 - OUT
        assertDocuments("510/01-OUT", ["StatementOfAccount","FairValue","ProofOfFunds","Agreement"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '510/01',
            }
          ]
        }),
        // 511 - OUT
        assertDocuments("511-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '511',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 512 - OUT
        assertDocuments("512-OUT", ["TaxClearance","PINLetter"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: {Individual: {TaxClearanceCertificateReference:'Ref03'}}, //or Resident.Entity.TaxClearanceCertificateReference 
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '512',
            }
          ]
        }),
        // 512 - OUT
        assertDocuments("512/04-OUT", ["TaxClearance"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: {Individual: {TaxClearanceCertificateIndicator:'N'}}, //or Resident.Entity.TaxClearanceCertificateIndicator
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '512/04',
            }
          ]
        }),
        // 513 - OUT
        assertDocuments("513-OUT", ["TaxClearance","PINLetter"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          Resident: {Individual: {TaxClearanceCertificateReference:'Ref03'}}, //or Resident.Entity.TaxClearanceCertificateReference 
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '513'
            }
          ]
        }),
        // 530 - OUT
        assertDocuments("530-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '530',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 600 - OUT
        assertDocuments("600-OUT", ["OriginalTransaction"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '600',
              ReversalTrnRefNumber:'Ref01'
            }
          ]
        }),
        // 600 - IN
        assertDocuments("600-IN", ["OriginalTransaction"], {
          ReportingQualifier: 'BOPCUS', Flow: 'IN',
          MonetaryAmount: [
            {
              CategoryCode: '600',
              ReversalTrnRefNumber:'Ref01'
            }
          ]
        }),
        
        //601/01 - OUT
        assertDocuments("601/01-OUT", ["BrokerNote"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '601/01',
            }
          ]
        }),

        //601/02 - OUT
        assertDocuments("601/02-OUT", ["AuditorLetter","Agreement","ShareCertificate","ProofOfFunds"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '601/02',
            }
          ]
        }),

        //603/02 - OUT
        assertDocuments("603/02-OUT", ["BrokerNote","FairValue","TitleDocument","ProofOfFunds"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '603/02',
            }
          ]
        }),

        // 605 - OUT
        assertDocuments("605-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '605',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),

        // 610 - OUT
        assertDocuments("610-OUT", ["RegulatorApproval","1:BankApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '610',
              SARBAuth: {SARBAuthRefNumber: 'Ref01',ADInternalAuthNumber:'01'},                         
            }
          ]
        }),

        // 611 - OUT
        assertDocuments("611-OUT", ["RegulatorApproval","1:BankApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '611',
              SARBAuth: {SARBAuthRefNumber: 'Ref01',ADInternalAuthNumber:'01'},                         
            }
          ]
        }),
        
        // 612 - OUT
        assertDocuments("612-OUT", ["RegulatorApproval","BankApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '612',
              SARBAuth: {SARBAuthRefNumber: 'Ref01',ADInternalAuthNumber:'01'},                         
            }
          ]
        }),

        // 615 - OUT
        assertDocuments("615-OUT", ["FSBCertificate","AssetAllocation"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '615',
            }
          ]
        }),

        // 616 - OUT
        assertDocuments("616-OUT", ["RegulatorApproval","BankApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '616',
              SARBAuth: {SARBAuthRefNumber: 'Ref01',ADInternalAuthNumber:'01'},                         
            }
          ]
        }),
        // 700 - OUT
        assertDocuments("700-OUT", ["OriginalTransaction"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '700',
              ReversalTrnRefNumber: 'Ref01'
            }
          ]
        }),
        // 700 - IN
        assertDocuments("700-IN", ["OriginalTransaction"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '700',
              ReversalTrnRefNumber: 'Ref01'
            }
          ]
        }),
        // 701 - OUT
        assertDocuments("701-OUT", ["RegulatorApproval","FSBCertificate","AssetAllocation"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '701',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}              
            }
          ]
        }),

        // 702 - OUT
        assertDocuments("702-OUT", ["RegulatorApproval","FSBCertificate","AssetAllocation"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '702',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}              
            }
          ]
        }),

        // 703 - OUT
        assertDocuments("703-OUT", ["RegulatorApproval","FSBCertificate","AssetAllocation"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '703',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}              
            }
          ]
        }),

        // 704 - OUT
        assertDocuments("704-OUT", ["RegulatorApproval","FSBCertificate","AssetAllocation"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '704',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}              
            }
          ]
        }),

        // 705 - OUT
        assertDocuments("705-OUT", ["RegulatorApproval","FSBCertificate","AssetAllocation"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',
              CategoryCode: '705',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}              
            }
          ]
        }),
        // 800 - OUT
        assertDocuments("800-OUT", ["OriginalTransaction"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '800',
              ReversalTrnRefNumber: 'Ref01'
            }
          ]
        }),
        // 800 - IN
        assertDocuments("800-IN", ["OriginalTransaction"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '800',
              ReversalTrnRefNumber: 'Ref01'
            }
          ]
        }),
        
        // 801 - OUT
        assertDocuments("801-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '801',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 802 - OUT
        assertDocuments("802-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '802',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 803 - OUT
        assertDocuments("803-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '803',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 804 - OUT
        assertDocuments("804-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '804',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 810 - OUT
        assertDocuments("810-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '810',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 816 - OUT
        assertDocuments("816-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '816',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 817 - OUT
        assertDocuments("817-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '817',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 818 - OUT
        assertDocuments("818-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '818',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),
        // 819 - OUT
        assertDocuments("819-OUT", ["RegulatorApproval"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              CategoryCode: '819',
              SARBAuth: {SARBAuthRefNumber: 'Ref01'}
            }
          ]
        }),

        // 830 - OUT
        assertDocuments("830-OUT", ["SupportDocumentation"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',              
              CategoryCode: '830' 
            }
          ]
        }),

        // 833 - OUT
        assertDocuments("833-OUT", ["ADLASettlement"], {
          ReportingQualifier: 'BOPCUS', Flow: 'OUT',
          MonetaryAmount: [
            {
              SequenceNumber:'1',              
              CategoryCode: '833' 
            }
          ]
        }),
      ];
    }
  }
})
