define(function() {
  return function(testBase) {
    var test_cases;
    with (testBase) {
      test_cases = [
        assertAssumptionIdv("CUS Pay Idv RESIDENT LOCAL_ACC paying USD to OTHRZAXXX",
         "SBZAZAJJ", resStatus.RESIDENT, accType.LOCAL_ACC, null, null, "OTHRZAXXX", null, null, "USD",
          { decision: dec.ReportToRegulator, reportingQualifier: "BOPCUS", flow: flowDir.OUT, reportingSide: drcr.DR, nonResSide: drcr.CR, nonResAccountType: at.RE_FCA, resSide: drcr.DR, resAccountType: at.RE_OTH }),
        assertAssumptionIdv("CUS Pay Idv RESIDENT LOCAL_ACC paying USD to OTHRZAXXX (I am told CFC)",
         "SBZAZAJJ", resStatus.RESIDENT, accType.LOCAL_ACC, null, null, "OTHRZAXXX", null, accType.CFC, "USD",
          { decision: dec.ReportToRegulator, reportingQualifier: "NON REPORTABLE", flow: flowDir.OUT, reportingSide: drcr.DR, nonResException: "CFC RESIDENT NON REPORTABLE", resSide: drcr.DR, resAccountType: at.RE_OTH }),
        assertAssumptionIdv("CUS Pay Idv RESIDENT LOCAL_ACC paying USD to OTHRZAXXX (I am told NOSTRO)",
         "SBZAZAJJ", resStatus.RESIDENT, accType.LOCAL_ACC, null, null, "OTHRZAXXX", null, accType.NOSTRO, "USD",
          { decision: dec.ReportToRegulator, reportingQualifier: "NON REPORTABLE", flow: flowDir.OUT, reportingSide: drcr.DR, nonResException: "NOSTRO NON REPORTABLE", resSide: drcr.DR, resAccountType: at.RE_OTH }),
      ];
    }
    return testBase;
  }
})