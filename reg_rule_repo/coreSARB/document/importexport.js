define(function () {
  return function (predef) {
    var docs;
    with (predef) {
      docs = {
        ruleset: "Import/Export Documents",
        scope: "importexport",
        validations: [
          {
            field: "ImportControlNumber",
            rules: [
              document("dicn1", "SupplierInvoice", "Advance payments for imports require the client to provide a SupplierInvoice",
                notEmpty).onOutflow().onSection("ABG").onCategory("101"),
              document("dicn2", "SupplierInvoice", "Payments for imports require the client to provide a SupplierInvoice",
                notEmpty).onOutflow().onSection("ABG").onCategory(["103", "105", "108", "109"]),
              document("dicn3", "CustomsDeclaration", "SARS Customs Declaration bearing the MRN (EDI / SAD 500)",
                notEmpty).onOutflow().onSection("ABG").onCategory(["103"]),
              document("dicn4", "FinancierInvoice", "When using a trade financier the Trade Financier invoice must be provided",
                notEmpty.and(not(importUndertakingClient))).onSection("ABG").onCategory(["106"]),
              // document("dicn4", "FinancierInvoice", "When using a trade financier the Trade Financier invoice must be provided",
              //   notEmpty.and(notMoneyField("{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber"))).onSection("ABG").onCategory(["106"]),
              document("dicn5", "SupplierInvoice", "Payments for imports require the client to provide a SupplierInvoice",
                notEmpty.and(not(importUndertakingClient))).onOutflow().onSection("ABG").onCategory(["106"])
              // document("dicn5", "SupplierInvoice", "Payments for imports require the client to provide a SupplierInvoice",
              //   notEmpty.and(notMoneyField("{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber"))).onOutflow().onSection("ABG").onCategory(["106"])
            ]
          },
          {
            field: "TransportDocumentNumber",
            rules: [
              document("dtdn1", "TransportDocument", "Transport Documentation evidencing transportation of goods to SA",
                notEmpty.and(not(importUndertakingClient))).onOutflow().notOnCategory("103/11").onCategory(["103","106"]).onSection("ABG"),
              document("dtdn2", "TransportDocument", "Transport Documentation evidencing transportation of goods to SA",

                notEmpty.and(not(importUndertakingClient)).and(notMoneyField("{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber"))).onOutflow().onCategory(["106"]).onSection("ABG")
              ]
          },
          {
            field: "UCR",
            rules: [
              document("ducr1", "ConsignmentReference", "Proof of consignment reference required for exports",
                notEmpty).onSection("ABG").onInflow().notOnCategory(['101/11', '103/11']).onCategory(['101', '103', '105']),
              document("ducr1b", "ConsignmentReference", "Proof of consignment reference required for exports",
                notEmpty.and(not(importUndertakingClient))).onSection("ABG").onInflow().notOnCategory(['101/11', '103/11']).onCategory(['106'])
            ]
          },
          {
            field: "PaymentValue",
            rules: [
              document('dtsupplier', "Invoice_from_Supplier", "Please provide an invoice from the supplier", returnTrue).onOutflow()
                .onSection("ABG").onCategory("110"),
              document('dtclient', "Invoice_to_buyer_from_client", "Please provide an invoice to the buyer", returnTrue).onOutflow()
                .onSection("ABG").onCategory("110"),
              document('dtintrooffunds', "Proof_of_introduction_of_funds", "Please provide proof of introduction of funds", returnTrue).onOutflow()
                .onSection("ABG").onCategory("110"), // TODO: if prepaid (how to determine)
              document('dtsarb', "SARB_Approval", "Please provide SARB approval for the transaction", returnTrue).onOutflow()
                .onSection("ABG").onCategory(["110"]), // TODO: if finsurb approval (how to determine)
              document('dtsarbb', "SARB_Approval", "Please provide SARB approval for the transaction", 
                not(importUndertakingClient)).onOutflow().onSection("ABG").onCategory(["106"])
            ]
          }
        ]
      };
    }
    return docs;
  }
});
