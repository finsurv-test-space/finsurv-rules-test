define(function () {
  return function (evaluationEx) {
    var evaluations;
    with (evaluationEx) {
      evaluations = [
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA, accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - page 1",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_OTH],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_CFC, at.RE_FCA, at.RE_CFC]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.VOSTRO],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          crDecision: {
            manualSection: "B and T Section B.1 (A and B) - pages 1, 2, 3",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            nonResSide: drcr.DR,
            nonResAccountType: [at.VOSTRO],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_CFC, at.RE_FCA]
          },
          drDecision: {
            manualSection: "B and T Section B.1 (B ix) - page 7",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            nonResSide: drcr.DR,
            nonResAccountType: [at.VOSTRO],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_CFC, at.RE_FCA],
            drSideSwift: "NTNRC"
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - page 1",
            reportable: rep.REPORTABLE,
            reportingSide: drcr.DR,
            flow: flowDir.OUT,
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_OTH],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_CFC, at.RE_FCA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Not stated in SARB specification, but seems similar to B and T Section B.1 (A) - page 1 (res rand to non res nostro)",
            reportable: rep.REPORTABLE,
            reportingSide: drcr.DR,
            flow: flowDir.OUT,
            nonResSide: drcr.CR,
            nonResAccountType: [at._CASH_],
            resSide: drcr.DR,
            resAccountType: [at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.VOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A and B) - pages 1, 2, 3",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.VOSTRO],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_CFC, at.RE_FCA]
          }
        },
        {
          // [Resident.Exception.ExceptionName] 315: May not be completed except if the NonResident AccountIdentifier is NON RESIDENT RAND, VISA NET or MASTER SEND
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.VOSTRO, accType.NOSTRO, accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - pages 1; B and T Section B.1 (C) - pages 14, 15",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.DR, // TODO: Check with compliance
            nonResAccountType: [at.NR_RND, at.NR_RND, at.NR_RND],
            //resSide: drcr.CR, TODO: Resident Excption needs a mapping side for purposes of country
            resException: 'NON RESIDENT RAND'
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.FCA, accType.VOSTRO, accType.NOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (C) - pages 14, 15; Manual Section B2 - A (iv) (a)",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.CR, // TODO: Check with compliance
            nonResAccountType: [at.NR_RND, at.NR_RND, at.NR_RND],
            resException: 'NON RESIDENT RAND'
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Not defined in manual explicitly",
            reportable: rep.NONREPORTABLE,
            drSideSwift: "NTNRC"
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A, B and C) - all pages",
            reportable: rep.NONREPORTABLE,
            drSideSwift: "DTCUS"
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - page 1",
            reportable: rep.NONREPORTABLE,
            drSideSwift: "NTNRC"
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - page 1; B and T Section B.1 (C) - page 14, 15",
            reportable: rep.NONREPORTABLE
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_CURR],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Manual Section B2 - A (i) a and b  (for IN)",
            reportable: rep.REPORTABLE,
            category: ['200', '250', '251'],
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at._CASH_],
            resException: 'MUTUAL PARTY'
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Manual Section B2 - A (i) a and b  (for OUT)",
            reportable: rep.REPORTABLE,
            category: ['200', '250', '251'],
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.DR,
            nonResAccountType: [at._CASH_],
            resException: 'MUTUAL PARTY'
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_CURR],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Manual Section B2 - A (i) c, d, e, f, g and h  (for IN)",
            reportable: rep.REPORTABLE,
            category: ['200', '252', '255', '256', '530/05'],
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResException: 'MUTUAL PARTY',
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Manual Section B2 - A (i) c, d, e, f, g and h  (for OUT)",
            reportable: rep.REPORTABLE,
            category: ['200', '252', '255', '256', '530/05'],
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResException: 'MUTUAL PARTY',
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_CURR],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Manual Section B2 - A (i) c, d, e, f, g and h  (for IN)",
            reportable: rep.REPORTABLE,
            category: ['200', '252', '255', '256', '530/05'],
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResException: 'MUTUAL PARTY',
            resSide: drcr.CR,
            resAccountType: [at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Manual Section B2 - A (i) c, d, e, f, g and h  (for OUT)",
            reportable: rep.REPORTABLE,
            category: ['200', '252', '255', '256', '530/05'],
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResException: 'MUTUAL PARTY',
            resSide: drcr.DR,
            resAccountType: [at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          category: ['517'],
          validFrom: "2013-07-18",
          decision: {
            accStatusFilter: accStatus.Individual,
            manualSection: "Manual Section B2 - B (i) and (ii) (for IN) (investment funds)",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.RE_FCA],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          notCategory: ['517'],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - page 1 (non investment funds)",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResException: ['FCA RESIDENT NON REPORTABLE'],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.FCA],
          category: ['513'],
          validFrom: "2013-07-18",
          decision: {
            accStatusFilter: accStatus.Individual,
            manualSection: "Manual Section B2 - B (ii) (for OUT) (investment funds)",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.RE_FCA],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.FCA],
          notCategory: ['513'],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - page 1 (non investment funds)",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResException: ['FCA RESIDENT NON REPORTABLE'],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Manual Section B2 - B (iii) (for OUT)",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.RE_FBC],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_CFC, at.RE_FCA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Manual Section B2 - B (iii) (for IN)",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.RE_FBC],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_CFC, at.RE_FCA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CFC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Manual Section B2 - C i, ii  (for OUT)",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResException: ['CFC RESIDENT NON REPORTABLE'],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CFC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Manual Section B2 - C i, ii  (for IN)",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResException: ['CFC RESIDENT NON REPORTABLE'],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.FCA],
          validFrom: "2013-07-18",
          drDecision: {
            manualSection: "Manual Section B2 - C (or OUT)",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResException: ['FCA RESIDENT NON REPORTABLE'],
            resSide: drcr.DR,
            resAccountType: [at.RE_FCA]
          },
          crDecision: {
            manualSection: "Manual Section B2 - C (or IN)",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResException: ['FCA RESIDENT NON REPORTABLE'],
            resSide: drcr.CR,
            resAccountType: [at.RE_FCA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CFC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CFC],
          validFrom: "2013-07-18",
          drDecision: { // No compliance on debits
            manualSection: "Manual Section B2 - C (for OUT)",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            nonResSide: drcr.CR,
            nonResException: ['CFC RESIDENT NON REPORTABLE'],
            resSide: drcr.DR,
            resAccountType: [at.RE_CFC]
          },
          crDecision: {
            manualSection: "Manual Section B2 - C (for IN)",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            nonResSide: drcr.DR,
            nonResException: ['CFC RESIDENT NON REPORTABLE'],
            resSide: drcr.CR,
            resAccountType: [at.RE_CFC]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CFC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Manual Section B1 - Page 3 of section 1 shows this as a ZZ1",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            category: '513',
            nonResSide: drcr.CR,
            nonResAccountType: [at.RE_FCA],
            resSide: drcr.DR,
            resAccountType: [at.RE_CFC]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CFC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Manual Section B1 - Page 3 of section 1 shows this as a ZZ1",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            category: '517',
            nonResSide: drcr.DR,
            nonResAccountType: [at.RE_FCA],
            resSide: drcr.CR,
            resAccountType: [at.RE_CFC]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.FCA, accType.CFC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - page 1",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at._CASH_, at._CASH_, at.NR_RND],
            resSide: drcr.DR,
            resAccountType: [at.RE_FCA, at.RE_CFC]
          }
        },
        // {
        //   drResStatus: resStatus.NONRES,
        //   drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
        //   crResStatus: resStatus.RESIDENT,
        //   crAccType: [accType.FCA],
        //   validFrom: "2013-07-18",
        //   decision: {
        //     manualSection: "B and T Section B.1 (C) - page 15",
        //     reportable: rep.ZZ1REPORTABLE,
        //     flow: flowDir.OUT,
        //     reportingSide: drcr.CR,
        //     nonResSide: drcr.DR,
        //     nonResAccountType: [at.NR_RND, at.NR_OTH, at.NR_RND],
        //     resSide: drcr.CR,
        //     resAccountType: [at.RE_FCA]
        //   }
        // },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - page 1",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_FCA],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - page 1",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_FCA],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          drDecision: {
            manualSection: "B and T Section B.1 (B) - page 3 (reportable on Credit side)",
            reportable: rep.NONREPORTABLE,
            drSideSwift: "NTNRC"
          },
          crDecision: {
            manualSection: "B and T Section B.1 (B) - page 3",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_FCA],
            resSide: drcr.CR,
            resAccountType: [at.RE_CFC, at.RE_FCA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CFC, accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (B) - page 3",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_FCA],
            resSide: drcr.DR,
            resAccountType: [at.RE_CFC, at.RE_FCA]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.FCA],
          validFrom: "2013-08-19",
          drDecision: {
            manualSection: "Operations Manual Section B.2 - Page 11",
            reportable: rep.ZZ1REPORTABLE,
            reportingSide: drcr.DR,
            nonResSide: drcr.DR,
            flow: flowDir.OUT,
            nonResAccountType: [at.NR_FCA],
            resException: ['FCA NON RESIDENT NON REPORTABLE']
          },
          crDecision: {
            manualSection: "Operations Manual Section B.2 - Page 11",
            reportable: rep.ZZ1REPORTABLE,
            reportingSide: drcr.CR,
            nonResSide: drcr.CR,
            flow: flowDir.IN,
            nonResAccountType: [at.NR_FCA],
            resException: ['FCA NON RESIDENT NON REPORTABLE']
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.VOSTRO, accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Std Bank Workshop - Assume Resident Nostro refers to bank's CFC account",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_OTH],
            resSide: drcr.DR,
            resAccountType: [at.RE_OTH]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_CURR, accType.CASH_LOCAL, accType.CFC, accType.FCA, accType.LOCAL_ACC, accType.VOSTRO],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.VOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A, B) - page 1, 3",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.VOSTRO],
            //resSide: drcr.CR,
            resException: 'FCA NON RESIDENT NON REPORTABLE'
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.VOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A, B) - page 1, 3",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.VOSTRO],
            //resSide: drcr.DR,
            resException: 'FCA NON RESIDENT NON REPORTABLE'
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.VOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.VOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - page 1",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.VOSTRO],
            //resSide: drcr.CR,
            resException: 'VOSTRO NON REPORTABLE'
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - page 1",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_FCA],
            //resSide: drcr.DR,
            resException: ['FCA NON RESIDENT NON REPORTABLE']
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - page 1",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_FCA],
            //resSide: drcr.CR,
            resException: ['FCA NON RESIDENT NON REPORTABLE']
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.VOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - page 1",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.VOSTRO],
            //resSide: drcr.CR,
            resException: ['VOSTRO NON REPORTABLE']
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.VOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (A) - page 1",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.VOSTRO],
            //resSide: drcr.CR,
            resException: ['VOSTRO NON REPORTABLE']
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.VOSTRO],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.VOSTRO, accType.NOSTRO, accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Resident Vostro accounts are not valid - use Resident RAND or Non Resident VOSTRO instead",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.VOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.VOSTRO, accType.NOSTRO, accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Resident Vostro accounts are not valid - use Resident RAND or Non Resident VOSTRO instead",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.NOSTRO, accType.CFC, accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.VOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Resident Vostro accounts are not valid - use Resident RAND or Non Resident VOSTRO instead",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.VOSTRO, accType.NOSTRO, accType.CFC, accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.VOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Resident Vostro accounts are not valid - use Resident RAND or Non Resident VOSTRO instead",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CFC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Non Resident CFC accounts are not valid",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CFC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Non Resident CFC accounts are not valid",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA, accType.VOSTRO, accType.NOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CFC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Non Resident CFC accounts are not valid",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CFC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.FCA, accType.VOSTRO, accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Illegal scenario: Non Resident CFC accounts are not valid",
            reportable: rep.ILLEGAL
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_CURR],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Direct foreign cash exchange between residents is not reportable",
            reportable: rep.NONREPORTABLE
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Direct foreign cash exchange between residents and non residents is not reportable",
            reportable: rep.NONREPORTABLE
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_CURR],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Direct foreign cash exchange between residents and non residents is not reportable",
            reportable: rep.NONREPORTABLE
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_CURR],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_CURR],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Direct foreign cash exchange between non residents is not reportable",
            reportable: rep.NONREPORTABLE
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.NOSTRO],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.NOSTRO],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "Direct foreign cash exchange between non residents is not reportable",
            reportable: rep.NONREPORTABLE
          }
        },
        // {
        //   drResStatus: resStatus.RESIDENT,
        //   drAccType: [accType.CFC],
        //   crResStatus: resStatus.NONRES,
        //   crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
        //   validFrom: "2013-07-18",
        //   decision: {
        //     manualSection: "Not explicitly stated in SARB specification",
        //     reportable: rep.ILLEGAL
        //   }
        // },
         {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CFC, accType.FCA],
          validFrom: "2013-07-18",
          decision: {
            manualSection: "B and T Section B.1 (C) - page 1",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_RND, at.NR_OTH, at.NR_RND],
            resSide: drcr.CR,
            resAccountType: [at.RE_CFC, at.RE_FCA]
          }
        },
        {
          drResStatus: resStatus.IHQ,
          drAccType: [accType.CFC, accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.VOSTRO, accType.NOSTRO, accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "Manual Section B4 - A i (Cross Border transactions: Outflow; IHQ to NonResident)",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            subject: 'IHQnnn',
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_RND, at.NR_OTH, at.NR_FCA],
            resSide: drcr.DR,
            resAccountType: [at.RE_FCA, at.RE_FCA]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.VOSTRO, accType.NOSTRO, accType.FCA],
          crResStatus: resStatus.IHQ,
          crAccType: [accType.CFC, accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "Manual Section B4 - A i (Cross Border transactions: Inflow; NonResident to IHQ)",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            subject: 'IHQnnn',
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_RND, at.NR_OTH, at.NR_FCA],
            resSide: drcr.CR,
            resAccountType: [at.RE_FCA, at.RE_FCA]
          }
        },
        {
          drResStatus: resStatus.IHQ,
          drAccType: [accType.LOCAL_ACC, accType.CFC, accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "Manual Section B4 - A ii (Local transactions: Inflow; IHQ to Resident)",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            subject: 'IHQnnn',
            locationCountry: 'ZA',
            nonResException: 'IHQ',
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_CFC, at.RE_FCA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.CASH_CURR, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          crResStatus: resStatus.IHQ,
          crAccType: [accType.LOCAL_ACC, accType.CFC, accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "Manual Section B4 - A ii (Local transactions: Outflow; Resident to IHQ)",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            subject: 'IHQnnn',
            locationCountry: 'ZA',
            nonResException: 'IHQ',
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at._CASH_, at.RE_OTH, at.RE_CFC, at.RE_FCA]
          }
        },
        {
          drResStatus: resStatus.HOLDCO,
          drAccType: [accType.FCA],
          crResStatus: resStatus.RESIDENT,
          crAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "Manual Section B4 - B i b (HOLDCO FCA to Resident)",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            subject: 'HOLDCO',
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_FCA],
            resSide: drcr.CR,
            resAccountType: [at._CASH_, at.RE_OTH, at.RE_CFC, at.RE_FCA]
          }
        },
        {
          drResStatus: resStatus.RESIDENT,
          drAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC, accType.CFC, accType.FCA],
          crResStatus: resStatus.HOLDCO,
          crAccType: [accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "Manual Section B4 - B i b (Resident to HOLDCO FCA)",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            subject: 'HOLDCO',
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_FCA],
            resSide: drcr.DR,
            resAccountType: [at._CASH_, at.RE_OTH, at.RE_CFC, at.RE_FCA]
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.LOCAL_ACC, accType.CFC, accType.FCA, accType.NOSTRO],
          crResStatus: resStatus.HOLDCO,
          crAccType: [accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "Manual Section B4 - B i c (Non Resident to HOLDCO FCA)",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.DR,
            nonResAccountType: [at.NR_RND, at.NR_FCA, at.NR_FCA, at.NR_FCA],
            resException: 'FCA NON RESIDENT NON REPORTABLE'
          }
        },
        {
          drResStatus: resStatus.HOLDCO,
          drAccType: [accType.FCA],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.LOCAL_ACC, accType.CFC, accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "Manual Section B4 - B i c (HOLDCO FCA to Non Resident)",
            reportable: rep.ZZ1REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            nonResSide: drcr.CR,
            nonResAccountType: [at.NR_RND, at.NR_FCA, at.NR_FCA],
            resException: 'FCA NON RESIDENT NON REPORTABLE'
          }
        },
        {
          drResStatus: resStatus.HOLDCO,
          drAccType: [accType.LOCAL_ACC],
          crResStatus: resStatus.NONRES,
          crAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC, accType.NOSTRO, accType.VOSTRO, accType.FCA],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "Manual Section B4 - B i d (HOLDCO Rand to Non Resident)",
            reportable: rep.REPORTABLE,
            flow: flowDir.OUT,
            reportingSide: drcr.DR,
            subject: 'HOLDCO',
            nonResSide: drcr.CR,
            nonResAccountType: [at._CASH_, at.NR_RND, at.NR_OTH, at.NR_RND, at.NR_FCA],
            resException: 'NON RESIDENT RAND'
          }
        },
        {
          drResStatus: resStatus.NONRES,
          drAccType: [accType.CASH_LOCAL, accType.LOCAL_ACC, accType.NOSTRO, accType.VOSTRO, accType.FCA],
          crResStatus: resStatus.HOLDCO,
          crAccType: [accType.LOCAL_ACC],
          validFrom: "2014-12-19",
          decision: {
            manualSection: "Manual Section B4 - B i d (Non Resident to HOLDCO Rand)",
            reportable: rep.REPORTABLE,
            flow: flowDir.IN,
            reportingSide: drcr.CR,
            subject: 'HOLDCO',
            nonResSide: drcr.DR,
            nonResAccountType: [at._CASH_, at.NR_RND, at.NR_OTH, at.NR_RND, at.NR_FCA],
            resException: 'NON RESIDENT RAND'
          }
        }
      ];
    }
    return evaluations;
  }
});