define(function () {
  return function (predef) {
    var ie;

    with (predef) {
      ie = {
        ruleset: "Import/Export Rules for Flow Form",
        scope: "importexport",
        validations: [
        ]
      }
    };

    return ie;
  }
});