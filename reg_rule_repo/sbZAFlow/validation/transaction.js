define(function () {
  //THIS IS the Transaciotn rules
  return function (predef) {
    var transaction;

    with (predef) {
      transaction = {
        ruleset: "Transaction Rules for Flow form",
        scope: "transaction",
        validations: [
          {
            field: "OriginatingBank",
            rules: [
              ignore("obank1"),
              ignore("obank3"),
              ignore("obank4"),
              ignore("obank5"),
              ignore("obank6"),
              ignore("obank7"),
              ignore("obank8"),
              ignore("obank9")
            ]
          },
          {
            field: "ReceivingBank",
            rules: [
              ignore("rbank1"),
              ignore("rbank3"),
              ignore("rbank4"),
              ignore("rbank5"),
              ignore("rbank6"),
              ignore("rbank7"),
              ignore("rbank8"),
              ignore("rbank9")
            ]
          },
          {
            field: "CorrespondentBank",
            rules: [
              ignore("cbank1")
            ]
          },
          {
            field: "ReceivingCountry",
            rules: [
              ignore("rcntry4"),
              // failure("rcntry5", 234, "Country code may not be ZA",
              //   notEmpty.and(hasValue("ZA")).and(hasTransactionFieldValue("IsFCA", "N"))).onOutflow().onSection("ABG")
            ]
          },
          {
            field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
            rules: [
              ignore("nriaid1"),
              ignore("nriaid4"),
              ignore("nriaid5"),
              ignore("nriaid6"),
              ignore("nriaid7"),
              ignore("nriaid9"),
              ignore("nriaid10"),
              ignore("nriaid11")
            ]
          }
        ]
      }
    };

    return transaction;
  }

});

