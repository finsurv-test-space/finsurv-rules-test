define(function () {
  return function (predef) {
    var money;

    with (predef) {
      money = {
        ruleset: "Money Rules for Flow Form",
        scope: "money",
        validations: [
          {
            field: "CategoryCode",
            rules: [
              failure("flw_CCnr6", 253, "If category 250 or 251 is used, Non Resident Entity, or NonResident Exception element may not be completed",
                notTransactionField("NonResident.Individual")).onSection("A").onCategory(["250", "251"])
            ]
          },
          {
            field: "LoanInterestRate",
            rules: [
            ignore("mlir1"),
            ignore("mlir3"),
            ignore("mlir4")
            ]
          },
          {
            field: "LoanTenor",
            rules: [
            failure("flw_mlt1", "I??", "Loan Tenor required due to category selected",
            notEmpty).onOutflow().onSection("AB").notOnCategory(["810", "815", "816", "817", "818", "819"])
            ]
          },
          {
            field: "LoanInterest.BaseRate",
            rules: [
            failure("flw_mlibr1", "I??", "Base rate required due to category selected",
            notEmpty).onOutflow().onSection("AB").notOnCategory(["810", "815", "816", "817", "818", "819"])
            ]
          },
          {
            field: "LoanInterest.Term",
            rules: [
            failure("flw_mlit1", "I??", "Term required as Base Rate is not Prime Rate",
            isEmpty.and(hasMoneyField("LoanInterest.BaseRate").and(notMoneyFieldValue("LoanInterest.BaseRate", ["FIXED", "PRIME"])))),
            failure("flw_mlit2", "I??", "Unless the Interest Base Rate is used (and this rate needs a term) this must not be provided",
            notEmpty.and(notMoneyField("LoanInterest.BaseRate").or(hasMoneyFieldValue("LoanInterest.BaseRate", ["FIXED", "PRIME"]))))
            ]
          },
          {
            field: "LoanInterest.PlusMinus",
            rules: [
            failure("flw_mlipm1", "I??", "Required field for loans",
            isEmpty.and(hasMoneyField("LoanInterest.BaseRate")).and(notMoneyFieldValue("LoanInterest.BaseRate", ["FIXED", "PRIME"]))),
            failure("flw_mlipm12", "I??", "Unless the Interest Base Rate is used this must not be provided",
            notEmpty.and(notMoneyField("LoanInterest.BaseRate").or(hasMoneyFieldValue("LoanInterest.BaseRate", ["FIXED", "PRIME"]))))
            ]
          },
          {
            field: "LoanInterest.Rate",
            rules: [
            failure("flw_mlir1", 378, "Rate required due to category selected",
            notPattern("^\\d+(\\.\\d{2})$")).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
            failure("flw_mlir3", 380, "Rate required due to category selected",
            notPattern("^\\d{1,3}\\.\\d{2}?$")).onOutflow().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
            failure("flw_mlir4", 380, "If the Flow is In and CategoryCode 309/01 to 309/07 is used, must be completed reflecting the percentage interest paid e.g. 7.20",
            notPattern("^\\d{1,3}\\.\\d{2}?$")).onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
            warning("flw_mlir5", "S12", "Rate should not be greater than 100%",
            hasPattern("^\\d{3}\\.\\d{2}?$")).onOutflow().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
            warning("flw_mlir6", "S12", "It is unlikely that an interest rate greater than 100% is being charged",
            hasPattern("^\\d{3}\\.\\d{2}?$")).onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
            failure("flw_mlir7", "I??", "Unless the category 810, 815, 816, 817, 818, 819, 309/04, 309/05, 309/06 or 309/07 is used is used this must not be provided",
            notEmpty).onOutflow().onSection("AB").notOnCategory(["810", "815", "816", "817", "818", "819", "309/04", "309/05", "309/06", "309/07"]),
            failure("flw_mlir8", "I??", "Unless the category 309/01, 309/02, 309/03, 309/04, 309/05, 309/06 or 309/07 is used is used this must not be provided",
            notEmpty).onInflow().onSection("ABG").notOnCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"])
            ]
          },
          {
            field: "LocationCountry",
            rules: [
            ignore("mlc1"),
            ignore("mlc2"),
            ignore("mlc3"),
            ignore("mlc4")
            ]
          }, 
          
          {
            field: "{{Regulator}}Auth.RulingsSection",
            rules: [
              ignore("mars1")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
            rules: [
              ignore("maian1")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
            rules: [
              ignore("maiad1")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
            rules: [
              ignore("masan1"),
              failure("flw_masan1", "I33", "If the SARB Auth Date is captured then SARB Auth Number must be provided",
              isEmpty.and(hasMoneyField("{{Regulator}}Auth.SARBAuthDate"))).onSection("ABG"),
              // failure("flw_masan2", "I??", "The SARB Auth Number must be provided for category 105 and 106",
              // isEmpty).onOutflow().onSection("ABG").onCategory(["105", "106"])
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
            rules: [
              ignore("masar1")
            ]
          },

          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthDate",
            rules: [
            failure("flw_masad1", "I12", "If the SARB Auth Number is captured then SARB Auth Date must be provided",
            isEmpty.and(hasMoneyField("{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber"))).onSection("ABG"),
            failure("flw_masad2", "I26", "Must be in a valid date format: YYYY-MM-DD",
            notEmpty.and(notPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/))).onSection("ABG"),
            warning("flw_masad3", "I27", "This SARB Auth Date is in the future",
            notEmpty.and(isDaysInFuture(0))).onSection("ABG")
            ]
          },
        ]
      }
    };

    return money;
  }
});
