define({
	ReportingQualifier : ["BOPCUS", "NON REPORTABLE", "BOPCARD RESIDENT", "BOPCARD NON RESIDENT", "NON RESIDENT RAND", "INTERBANK", "BOPDIR"],
  facilitators : [
		{
			label: 'Standard Bank',
			value: "THIS BANK"
		},
		{
			label: 'Another Bank',
			value: 'OTHER BANK'
		},
	],
  issuers : [
		{
			label: 'Standard Bank',
			value: "THIS BANK"
		},
		{
			label: 'Another Bank',
			value: 'OTHER BANK'
		},
		{
			label: 'SARB',
			value: 'REGULATOR'
		}
	],
	loanBase : [
		{value:'FIXED', label:'Fixed'},
		{value:'BA', label:'Ba'},
		{value:'BASE', label:'Base'},
		{value:'BBSW', label:'Bbsw'},
		{value:'CIBOR', label:'Cibor'},
		{value:'EURIBOR', label:'Euribor'},
		{value:'HIBOR', label:'Hibor'},
		{value:'JIBAR', label:'Jibar'},
		{value:'LIBOR', label:'Libor'},
		{value:'OIBOR', label:'Oibor'},
		{value:'POLO', label:'Polo'},
		{value:'PRIBOR', label:'Pribor'},
		{value:'PRIME', label:'Prime'},
		{value:'REPO', label:'Repo'},
		{value:'TELBOR', label:'Telbor'},
		{value:'WIBOR', label:'Wibor'}
	],
	loanTerm : [
		{	label: '1 month', value: '1' },
		{	label: '2 month', value: '2' },
		{	label: '3 month', value: '3' },
		{	label: '4 month', value: '4' },
		{	label: '5 month', value: '5' },
		{	label: '6 month', value: '6' },
		{	label: '7 month', value: '7' },
		{	label: '8 month', value: '8' },
		{	label: '9 month', value: '9' },
		{	label: '10 month', value: '10' },
		{	label: '11 month', value: '11' },
		{	label: '12 month', value: '12' }
	],
	LoanSign : [
		{ label: 'Minus',	value: 'MINUS' },
		{ label: 'Plus',	value: 'PLUS' }
	]
})
