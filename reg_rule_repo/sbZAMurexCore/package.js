define({
  engine: {major: "1", minor: "0"},
  dependsOn: "stdSARB",
  mappings: {
    LocalCurrencySymbol: "R",
    LocalCurrencyName: "Rand",
    LocalCurrency: "ZAR",
    Locale: "ZA",
    LocalValue: "DomesticValue",
    Regulator: "Regulator",
    DealerPrefix: "RE",
    RegulatorPrefix: "CB",
    StateName: "Province",
    _minLenErrorType: "ERROR", // SUCCESS, ERROR, WARNING
    _maxLenErrorType: "ERROR",
    _lenErrorType: "ERROR"   
  },
})