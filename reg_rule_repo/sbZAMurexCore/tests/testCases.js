define(function() {
  return function(testBase) {
      with (testBase) {
        var mappings = {
          LocalCurrencySymbol: "R",
          LocalCurrencyName: "Rand",
          LocalCurrency: "ZAR",
          LocalValue: "RandValue",
          Regulator: "SARB",
          DealerPrefix: "AD",
          RegulatorPrefix: "SARB"
        };

        setMappings(mappings);

        var test_cases = [

					//Money: SARBAuth.AuthFacilitator
					//Must be completed if the Flow is OUT and no data is supplied under RulingsSection
					assertSuccess("auf1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{CategoryCode: '101', SARBAuth: {AuthFacilitator: 'THIS BANK'}}]
					}),
					assertSuccess("auf1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{CategoryCode: '101', SARBAuth: {RulingsSection: 'XYZ'}}]
					}),
					assertNoRule("auf1", {ReportingQualifier: 'BOPCUS', Flow: 'OUT', MonetaryAmount: [{CategoryCode: '100'}]}),
					assertFailure("auf1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{CategoryCode: '101', SARBAuth: {}}]
					}),
					assertFailure("auf1", {ReportingQualifier: 'BOPCUS', Flow: 'OUT', MonetaryAmount: [{CategoryCode: '101'}]}),

					//RegistrationNumber is registered as an IHQ entity or the Subject is IHQnnn
					assertSuccess("auf2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234567/07"}},
						MonetaryAmount: [{
							SARBAuth: {AuthFacilitator: 'THIS BANK'}
						}]
					}),
					assertSuccess("auf2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234568/07"}},
						MonetaryAmount: [{
							SARBAuth: {AuthFacilitator: 'OTHER BANK'}
						}]
					}),
					assertFailure("auf2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234568/07"}},
						MonetaryAmount: [{
							SARBAuth: {AuthFacilitator: ''}
						}]
					}),
					assertSuccess("auf2", {
						ReportingQualifier: 'BOPCUS',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'IHQ123'},
							SARBAuth: {AuthFacilitator: 'OTHER BANK'}
						}]
					}),
					assertFailure("auf2", {
						ReportingQualifier: 'BOPCUS',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'IHQ123'},
							SARBAuth: {AuthFacilitator: ''}
						}]
					}),
					assertNoRule("auf2", {ReportingQualifier: "NON REPORTABLE"}),
				
					//If the Flow is IN and the Subject is SETOFF, it must be completed
					assertSuccess("auf3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							SARBAuth: {AuthFacilitator: 'THIS BANK'}
						}]
					}),
					assertSuccess("auf3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							SARBAuth: {AuthFacilitator: 'OTHER BANK'}
						}]
					}),
					assertFailure("auf3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							SARBAuth: {AuthFacilitator: ''}
						}]
					}),
					assertFailure("auf3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{AdHocRequirement: {Subject: 'SETOFF'}, SARBAuth: {}}]
					}),

					//May not be completed
					assertSuccess("auf4", {ReportingQualifier: 'NON REPORTABLE', MonetaryAmount: [{}]}),
					assertFailure("auf4", {
						ReportingQualifier: 'NON REPORTABLE',
						MonetaryAmount: [{SARBAuth: {AuthFacilitator: 'XYZ'}}]
					}),

					//Money: SARBAuth.AuthIssuer
					//Must be complete if AuthFacilitator is
					assertSuccess("aui1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{
								CategoryCode: '101', 
								SARBAuth: {
									AuthFacilitator: 'THIS BANK', 
									AuthIssuer: "THIS BANK"
								}
							}]
					}),
					assertSuccess("aui1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{
								CategoryCode: '101', 
								SARBAuth: {
									AuthFacilitator: 'OTHER BANK', 
									AuthIssuer: "OTHER BANK"
								}
							}]
					}),
					assertSuccess("aui1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{
								CategoryCode: '101', 
								SARBAuth: {
									AuthFacilitator: 'THIS BANK', 
									AuthIssuer: "REGULATOR"
								}
							}]
					}),
					assertFailure("aui1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{
								CategoryCode: '101', 
								SARBAuth: {
									AuthFacilitator: 'THIS BANK', 
									AuthIssuer: ""
								}
							}]
					}),
					assertFailure("aui1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{
								CategoryCode: '101', 
								SARBAuth: {
									AuthFacilitator: 'THIS BANK', 
									AuthIssuer: ""
								}
							}]
					}),
					assertFailure("aui1", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'OUT',
						MonetaryAmount: [{
								CategoryCode: '101', 
								SARBAuth: {
									AuthFacilitator: 'OTHER BANK'
								}
							}]
					}),

					//RegistrationNumber is registered as an IHQ entity or the Subject is IHQnnn
					assertSuccess("aui2", {
						ReportingQualifier: 'BOPCUS',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'IHQ123'},
							SARBAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'REGULATOR'
							}
						}]
					}),
					assertSuccess("aui2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234567/07"}},
						MonetaryAmount: [{
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: "REGULATOR"
							}
						}]
					}),
					assertSuccess("aui2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234568/07"}},
						MonetaryAmount: [{
							SARBAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'REGULATOR'
							}
						}]
					}),
					assertFailure("aui2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234568/07"}},
						MonetaryAmount: [{
							SARBAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'OTHER BANK'
							}
						}]
					}),
					assertFailure("aui2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234568/07"}},
						MonetaryAmount: [{
							SARBAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'THIS BANK'
							}
						}]
					}),
					assertFailure("aui2", {
						ReportingQualifier: 'BOPCUS',
						Resident: {Entity: {RegistrationNumber: "2013/1234568/07"}},
						MonetaryAmount: [{
							SARBAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'OTHER BANK'
							}
						}]
					}),
					assertSuccess("aui2", {
						ReportingQualifier: 'BOPCUS',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'IHQ123'},
							SARBAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'REGULATOR'
							}
						}]
					}),
					assertFailure("aui2", {
						ReportingQualifier: 'BOPCUS',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'IHQ123'},
							SARBAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'THIS BANK'
							}
						}]
					}),
					assertFailure("aui2", {
						ReportingQualifier: 'BOPCUS',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'IHQ123'},
							SARBAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'OTHER BANK'
							}
						}]
					}),
					assertFailure("aui2", {
						ReportingQualifier: 'BOPCUS',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'IHQ123'},
							SARBAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'THIS BANK'
							}
						}]
					}),

					//If the Flow is IN and the Subject is SETOFF, it must be completed
					assertSuccess("aui3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR'
							}
						}]
					}),
					assertSuccess("aui3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR'
							}
						}]
					}),
					assertFailure("aui3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK'
							}
						}]
					}),
					assertFailure("aui3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'THIS BANK'
							}
						}]
					}),
					assertFailure("aui3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'},
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK'
							}
						}]
					}),
					assertFailure("aui3", {
						ReportingQualifier: 'BOPCUS',
						Flow: 'IN',
						MonetaryAmount: [{
							AdHocRequirement: {Subject: 'SETOFF'}, 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK'
							}
						}]
					}),

					 // Money: SARBAuth.ADInternalAuthNumber
          // Must be completed if the AuthIssuer is 'OTHER BANK' OR 'THIS BANK'
					assertSuccess("za_maian3", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK',
								ADInternalAuthNumber: '12345678'
							}
						}]
					}),
					assertSuccess("za_maian3", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'THIS BANK',
								ADInternalAuthNumber: '12345678'
							}
						}]
					}),
					assertFailure("za_maian3", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'THIS BANK',
								ADInternalAuthNumber: ''
							}
						}]
					}),
					assertFailure("za_maian3", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK',
								ADInternalAuthNumber: ''
							}
						}]
					}),
					
					// Must be 8 characters in length
					assertSuccess("za_maian4", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK',
								ADInternalAuthNumber: '12345678'
							}
						}]
					}),
					assertSuccess("za_maian4", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'THIS BANK',
								ADInternalAuthNumber: '12345678'
							}
						}]
					}),

					assertFailure("za_maian4", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'THIS BANK',
								ADInternalAuthNumber: '123'
							}
						}]
					}),
					assertFailure("za_maian4", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK',
								ADInternalAuthNumber: 'XYZ'
							}
						}]
          }),

					// Money: SARBAuth.SARBAuthAppNumber
          // Must be completed if the AuthIssuer is 'REGULATOR'
					assertSuccess("za_masan5", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
								SARBAuthAppNumber: '12345678'
							}
						}]
					}),
					assertFailure("za_masan5", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
								SARBAuthAppNumber: ''
							}
						}]
					}),
					assertFailure("za_masan5", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
							}
						}]
					}),
					
					// Must be 8 characters in length
					assertSuccess("za_masan6", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
								SARBAuthAppNumber: '12345678'
							}
						}]
					}),
					assertFailure("za_masan6", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
								SARBAuthAppNumber: '123'
							}
						}]
					}),
					assertFailure("za_masan6", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
								SARBAuthAppNumber: 'ABCDEFGH'
							}
						}]
          }),


					// Money: SARBAuth.SARBAuthDate
          // Must be completed if the AuthIssuer is 'REGULATOR'
					assertSuccess("sad", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
								SARBAuthDate: '2020-06-05'
							}
						}]
					}),
					assertSuccess("sad", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'OTHER BANK',
								AuthIssuer: 'REGULATOR',
								SARBAuthDate: ''
							}
						}]
					}),
					assertSuccess("sad", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
						MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'OTHER BANK',
								SARBAuthDate: ''
							}
						}]
					}),
					assertFailure("sad", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
								SARBAuthDate: ''
							}
						}]
					}),
					assertFailure("sad", {
            ReportingQualifier: 'BOPCUS',
            Flow: 'OUT',
            MonetaryAmount: [{
							CategoryCode: '101', 
							SARBAuth: {
								AuthFacilitator: 'THIS BANK',
								AuthIssuer: 'REGULATOR',
							}
						}]
					}),

				]
			}
			return testBase;
		}
	})