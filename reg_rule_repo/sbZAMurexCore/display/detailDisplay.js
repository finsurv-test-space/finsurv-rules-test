define(function () {

    return function (predef) {
 
     var display;
 
     with (predef) {
       display = {
         detailTrans: {
           ruleset: "Flow Transaction Display Rules",
           scope: "transaction",
           fields: [

           ]
         },
         detailMoney: {
           ruleset: "Flow Money Display Rules",
           scope: "money",
           fields: [
            {
                field: "CategoryCode",
                display: [
                    setValue('ZZ1', null, hasTransactionFieldValue('ReportingQualifier', 'NON REPORTABLE')),
                    hide(hasTransactionFieldValue('ReportingQualifier', 'NON REPORTABLE')),
                    hide().onSection("DEF")
                ]
            },
            {
              field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
                display: [
                    disable(),
                    hide(),
                    clearValue(not(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "REGULATOR").or(notMoneyField(map("{{Regulator}}Auth.AuthIssuer"))))),
                    show(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "REGULATOR")),
                    enable(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "REGULATOR"))
                ]
            },
            {
              field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
                display: [
                    hide(),
                    disable(),
                    clearValue(not(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "THIS BANK").or(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "OTHER BANK"))
                    .or(notMoneyField(map("{{Regulator}}Auth.AuthIssuer"))))),
                    show(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "THIS BANK").or(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "OTHER BANK"))),
                    enable(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "THIS BANK").or(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "OTHER BANK")))
                    
                ]
            },
            {
              field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
                display: [
                    hide(),
                    disable(),
                    clearValue(not(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "REGULATOR").or(notMoneyField(map("{{Regulator}}Auth.AuthIssuer"))))),
                    show(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "REGULATOR")),
                    enable(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "REGULATOR"))
                ]
            },
            {
              field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
                display: [
                    hide(),
                    disable(),
                    clearValue(not(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "THIS BANK").or(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "OTHER BANK"))
                    .or(notMoneyField(map("{{Regulator}}Auth.AuthIssuer"))))),
                    show(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "THIS BANK").or(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "OTHER BANK"))),
                    enable(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "THIS BANK").or(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "OTHER BANK")))
                ]
            },
            {
              field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthDate",
              display: [
                disable(),
                hide(),
                clearValue(not(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "REGULATOR").and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthFacilitator"), "THIS BANK")))),
                show(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "REGULATOR").and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthFacilitator"), "THIS BANK"))),
                enable(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "REGULATOR").and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthFacilitator"), "THIS BANK")))
              ]
            },
            {
              field: "LoanInterestRate",
              display: [
                hide(),
                disable(),
                enable().notOnCategory(["810", "815", "816", "817", "818", "819"]),
                show().onSection("ABG").onCategory(["810", "815", "816", "817", "818", "819"]),
                show().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
                show().onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03"])
              ]
            },
            {
              field: "LoanInterest.BaseRate",
              display: [
                disable(),
                hide(),
                enable().onSection("ABG").onCategory(["810", "815", "816", "817", "818", "819"]),
                show().onSection("ABG").onCategory(["810", "815", "816", "817", "818", "819"]),
                setValue(undefined, null, notEmpty).notOnCategory(["810", "815", "816", "817", "818", "819"])
              ]
            },
            {
              field: "LoanInterest.Term",
              display: [
                disable(),
                hide(),
                enable(hasMoneyField("LoanInterest.BaseRate").and(notMoneyFieldValue("LoanInterest.BaseRate", ["FIXED", "PRIME"]))).onSection("ABG").onCategory(["810", "815", "816", "817", "818", "819"]),
                show(hasMoneyField("LoanInterest.BaseRate").and(notMoneyFieldValue("LoanInterest.BaseRate", ["FIXED", "PRIME"]))).onSection("ABG").onCategory(["810", "815", "816", "817", "818", "819"]),
                setValue(undefined, null, hasMoneyFieldValue("LoanInterest.BaseRate", ["FIXED", "PRIME"])),
                setValue(undefined, null, notEmpty).notOnCategory(["810", "815", "816", "817", "818", "819"])
              ]
            },
            {
              field: "LoanInterest.PlusMinus",
              display: [
                disable(),
                hide(),
                enable(hasMoneyField("LoanInterest.BaseRate").and(notMoneyFieldValue("LoanInterest.BaseRate", ["FIXED"]))).onSection("ABG").onCategory(["810", "815", "816", "817", "818", "819"]),
                show(hasMoneyField("LoanInterest.BaseRate").and(notMoneyFieldValue("LoanInterest.BaseRate", ["FIXED"]))).onSection("ABG").onCategory(["810", "815", "816", "817", "818", "819"]),
                setValue(undefined, null, hasMoneyFieldValue("LoanInterest.BaseRate", "FIXED")),
                setValue(undefined, null, notEmpty).notOnCategory(["810", "815", "816", "817", "818", "819"])
              ]
            },
            {
              field: "LoanInterest.Rate",
              display: [
                disable(),
                hide(),
                enable().onSection("ABG").onCategory(["810", "815", "816", "817", "818", "819"]),
                show().onSection("ABG").onCategory(["810", "815", "816", "817", "818", "819"]),
                setValue(undefined, null, notEmpty).notOnCategory(["810", "815", "816", "817", "818", "819"])
                // show().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
                // show().onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03"]),
                // setValue(undefined, null, notEmpty).onSection("ABG").notOnCategory(["810", "815", "816", "817", "818", "819", "309/04", "309/05", "309/06", "309/07"]) 
              ]
            },
            {
              /*mtaAccounts item
               {
               accountNumber : "12345678",
               MTA : "GLOBAL",
               rulingSection : "baz",
               ADLALevel : "2",
               isADLA : true
               }
               */
              field: "MoneyTransferAgentIndicator",
              display: [
                setValue(function (context, ind) {
                  var AccountNumber =
                    context.getTransactionField('Resident.Individual.AccountNumber') |
                    context.getTransactionField('Resident.Entity.AccountNumber');
   
                  var mtaAccount =
                    context.getLookups().mtaAccounts.find(function (item) {
                      return item.accountNumber == AccountNumber;
                    });
   
                  return (!!mtaAccount ? mtaAccount.MTA : "");
   
                }, evalTransactionField("Resident.Individual.AccountNumber", isInLookup('mtaAccounts', 'accountNumber'))
                  .or(evalTransactionField("Resident.Entity.AccountNumber", isInLookup('mtaAccounts', 'accountNumber')))
                ).onCategory(['833']).onSection("A"),
                setValue('AD', null, isEmpty).notOnCategory(['833'])
              ]
            }
          ]
         },
         detailImportExport: {
           ruleset: "Flow Import/Export Display Rules",
           scope: "importexport",
           fields: []
 
         }
       };
     }
 
     return display;
   }
});