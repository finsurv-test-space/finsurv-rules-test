define(function () {
    return function (predef) {
      var filterLookupRules;
      with (predef) {
        filterLookupRules = {
          filterLookupTrans: {
            ruleset: "Reporting Transaction Lookup Filter Rules",
            scope: "transaction",
            fields: [

            ]
          },
  
          filterLookupMoney: {
            ruleset: "Reporting Monetary Lookup Filter Rules",
            scope: "money",
            fields: [
              {
                field: "CompoundCategoryCode",
                display: [
                    limitValue(['ZZ1'], hasTransactionFieldValue('ReportingQualifier', 'NON REPORTABLE')),
                    excludeValue(['ZZ1', 'ZZ2'], hasTransactionFieldValue('ReportingQualifier', 'BOPCUS')),
                ]
              },
              {
                field: "{{Regulator}}Auth.AuthIssuer",
                display: [
                  excludeValue(["OTHER BANK"], hasMoneyFieldValue(map("{{Regulator}}Auth.AuthFacilitator"), "THIS BANK")),
                  excludeValue(["THIS BANK"], hasMoneyFieldValue(map("{{Regulator}}Auth.AuthFacilitator"), "OTHER BANK")),
                ]
              }
            ]
          },
  
          filterLookupImportExport: {
            ruleset: "Reporting Import Export Lookup Filter Rules",
            scope: "importexport",
            fields: []
          }
        }
      }
  
      return filterLookupRules;
    }
  });
  
  
  