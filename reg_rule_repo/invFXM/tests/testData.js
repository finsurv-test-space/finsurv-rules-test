var testData = [
  {
    name: "Empty transaction",
    data: {}
  },
  {
    name: "Outwards transaction",
    data:{
      "customData": {
        "TradeReference": "TRN42151324",
        "SourceSystem": "ECOMMERCE-FXM",
        "EcommerceName": "FREDDYHIRSCHGROUP",
        "BeneficiaryAccountId": 461,
        "snapShot": {
          "Version": "FINSURV",
          "Flow": "OUT",
          "FlowCurrency": "USD",
          "MonetaryAmount": [
            {
              "ForeignValue": "594095"
            }
          ],
          "ReceivingCountry": "ZA",
          "ReportingQualifier": "BOPCUS",
          "Resident": {
            "Entity": {
              "AccountHolderStatus": "South African Resident",
              "AccountIdentifier": "RESIDENT OTHER",
              "AccountName": "XXXXXXXX",
              "AccountNumber": "30887131871",
              "ContactDetails": {
                "ContactName": "",
                "ContactSurname": ""
              },
              "CustomsClientNumber": "00034786",
              "EntityName": "XXXXXXXX",
              "IndustrialClassification": "03",
              "InstitutionalSector": "02",
              "PostalAddress": {
                "AddressLine1": " BXXXXo ",
                "AddressLine2": "",
                "City": "Cape Town",
                "PostalCode": "8000",
                "Province": "WESTERN CAPE"
              },
              "RegistrationNumber": "1956/000899/07",
              "StreetAddress": {
                "AddressLine1": "thXXXX1t",
                "AddressLine2": "orXXXXoo",
                "City": "Cape Town",
                "PostalCode": "7404",
                "Province": "WESTERN CAPE"
              },
              "TaxNumber": "123456789",
              "TradingName": "",
              "VATNumber": "123456789"
            }
          },
          "TotalForeignValue": "594095",
          "ZAREquivalent": "50000",
          "TrnReference": "18051500009500",
          "ValueDate": "2018-05-15",
          "AccountHolderStatus": "South African Resident",
          "PaymentDetail": {
            
          }
        },
        "isValid": false
      },
      "transaction": {
        "Version": "FINSURV",
        "ReportingQualifier": "BOPCUS",
        "Flow": "OUT",
        "Resident": {
          "Entity": {
            "AccountHolderStatus": "South African Resident",
            "AccountIdentifier": "RESIDENT OTHER",
            "AccountName": "XXXXXXXX",
            "AccountNumber": "30887131871",
            "ContactDetails": {
              "ContactName": "",
              "ContactSurname": ""
            },
            "CustomsClientNumber": "00034786",
            "EntityName": "XXXXXXXX",
            "IndustrialClassification": "03",
            "InstitutionalSector": "02",
            "PostalAddress": {
              "AddressLine1": " BXXXXo ",
              "AddressLine2": "",
              "City": "Cape Town",
              "PostalCode": "8000",
              "Province": "WESTERN CAPE"
            },
            "RegistrationNumber": "1956/000899/07",
            "StreetAddress": {
              "AddressLine1": "thXXXX1t",
              "AddressLine2": "orXXXXoo",
              "City": "Cape Town",
              "PostalCode": "7404",
              "Province": "WESTERN CAPE"
            },
            "TaxNumber": "123456789",
            "TradingName": "",
            "VATNumber": "123456789"
          },
          "Type": "Entity"
        },
        "NonResident": {
          "Entity": {
            
          },
          "Type": "Entity"
        },
        "MonetaryAmount": [
          {
            "ImportExport": [
              
            ],
            "SARBAuth": {
              
            },
            "TravelMode": {
              
            },
            "ThirdParty": {
              
            },
            "ForeignValue": "594095.00",
            "SequenceNumber": "1"
          }
        ],
        "FlowCurrency": "USD",
        "ReceivingCountry": "ZA",
        "TotalForeignValue": "594095",
        "ZAREquivalent": "50000",
        "TrnReference": "18051500009500",
        "ValueDate": "2018-05-15",
        "AccountHolderStatus": "South African Resident",
        "PaymentDetail": {
          "IsCorrespondentBank": "N"
        }
      }
    }
  },  
  {
    name: "Inwards Remitter Entity",
    data: {
      "customData": {
        "edited": true,
        "isValid": false,
        "snapShot": {
          "Version": "FINSURV",
          "ReportingQualifier": "BOPCUS",
          "Flow": "IN",
          "Resident": {
            "Entity": {
              "VATNumber": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
              "InstitutionalSector": "02",
              "EntityName": "XXXXXXXX",
              "IndustrialClassification": "03",
              "AccountNumber": "1300035937370",
              "PostalAddress": {
                "AddressLine2": "",
                "AddressLine1": " BXXXXo ",
                "PostalCode": "8000",
                "City": "Cape Town",
                "Province": "WESTERN CAPE"
              },
              "StreetAddress": {
                "AddressLine2": "orXXXXoo",
                "AddressLine1": "thXXXX1t",
                "PostalCode": "7404",
                "City": "Cape Town",
                "Province": "WESTERN CAPE"
              },
              "ContactDetails": {
                "ContactSurname": "",
                "ContactName": ""
              },
              "CustomsClientNumber": "00034786",
              "AccountIdentifier": "RESIDENT OTHER",
              "RegistrationNumber": "1956/000899/07",
              "TradingName": "",
              "TaxNumber": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
              "AccountHolderStatus": "South African Resident",
              "AccountName": "XXXXXXXX"
            },
            "Type": "Entity"
          },
          "NonResident": {
            "Type": "Individual",
            "Individual": {
              "Address": {
                "AddressLine1": "Address 1",
                "Suburb": "suburb",
                "State": "province",
                "PostalCode": "8888",
                "Country": "MZ",
                "City": "city"
              },
              "Surname": "Surname",
              "Name": "First name"
            }
          },
          "MonetaryAmount": [
            {
              "ImportExport": [
                
              ],
              "SARBAuth": {
                
              },
              "TravelMode": {
                
              },
              "ThirdParty": {
                
              },
              "ForeignValue": "100.00",
              "RandValue": "100.00",
              "SequenceNumber": "1"
            }
          ],
          "CounterpartyStatus": "South African Resident",
          "ReceivingCountry": "ZA",
          "FlowCurrency": "ZAR",
          "TotalForeignValue": "100.00",
          "TrnReference": "18062200014084",
          "PaymentDetail": {
            
          },
          "ValueDate": "2018-06-27",
          "AccountHolderStatus": "South African Resident",
          "ZAREquivalent": "100.00"
        },
        "TradeReference": "42157832",
        "SourceSystem": "ECOMMERCE-FXM",
        "EcommerceName": "FREDDYHIRSCHGROUP",
        "BeneficiaryAccountId": "309",
        "DealerType": "AD"
      },
      "transaction": {
        "Version": "FINSURV",
        "ReportingQualifier": "BOPCUS",
        "Flow": "IN",
        "Resident": {
          "Entity": {
            "VATNumber": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
            "InstitutionalSector": "02",
            "EntityName": "XXXXXXXX",
            "IndustrialClassification": "03",
            "AccountNumber": "1300035937370",
            "PostalAddress": {
              "AddressLine2": "",
              "AddressLine1": " BXXXXo ",
              "PostalCode": "8000",
              "City": "Cape Town",
              "Province": "WESTERN CAPE"
            },
            "StreetAddress": {
              "AddressLine2": "orXXXXoo",
              "AddressLine1": "thXXXX1t",
              "PostalCode": "7404",
              "City": "Cape Town",
              "Province": "WESTERN CAPE"
            },
            "ContactDetails": {
              "ContactSurname": "gfdg",
              "ContactName": "sfd",
              "Email": "sfdsdf@sfsdf.com",
              "Telephone": ""
            },
            "CustomsClientNumber": "00034786",
            "AccountIdentifier": "RESIDENT OTHER",
            "RegistrationNumber": "1956/000899/07",
            "TradingName": "",
            "TaxNumber": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
            "AccountHolderStatus": "South African Resident",
            "AccountName": "XXXXXXXX"
          },
          "Type": "Entity"
        },
        "NonResident": {
          "Type": "Individual",
          "Individual": {
            "Address": {
              "AddressLine1": "Address 1",
              "Suburb": "",
              "State": "province",
              "PostalCode": "8888",
              "Country": "MZ",
              "City": "city"
            },
            "Surname": "Surname",
            "Name": "First name"
          }
        },
        "MonetaryAmount": [
          {
            "ImportExport": [
              
            ],
            "SARBAuth": {
              
            },
            "TravelMode": {
              
            },
            "ThirdParty": {
              
            },
            "ForeignValue": "100.00",
            "RandValue": "100.00",
            "SequenceNumber": "1",
            "CategoryCode": "100"
          }
        ],
        "CounterpartyStatus": "South African Resident",
        "ReceivingCountry": "ZA",
        "FlowCurrency": "ZAR",
        "TotalForeignValue": "100.00",
        "TrnReference": "18062200014084",
        "PaymentDetail": {
          
        },
        "ValueDate": "2018-06-27",
        "AccountHolderStatus": "South African Resident",
        "ZAREquivalent": "100.00",
        "LocationCountry": "GB"
      }
    }
  },  
  {
    name: "Inwards Remitter Individual",
    data: {
      "customData": {
        "edited": true,
        "isValid": false,
        "snapShot": {
          "Version": "FINSURV",
          "ReportingQualifier": "BOPCUS",
          "Flow": "IN",
          "Resident": {
            "Entity": {
              "VATNumber": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
              "InstitutionalSector": "02",
              "EntityName": "XXXXXXXX",
              "IndustrialClassification": "03",
              "AccountNumber": "1300035937370",
              "PostalAddress": {
                "AddressLine2": "",
                "AddressLine1": " BXXXXo ",
                "PostalCode": "8000",
                "City": "Cape Town",
                "Province": "WESTERN CAPE"
              },
              "StreetAddress": {
                "AddressLine2": "orXXXXoo",
                "AddressLine1": "thXXXX1t",
                "PostalCode": "7404",
                "City": "Cape Town",
                "Province": "WESTERN CAPE"
              },
              "ContactDetails": {
                "ContactSurname": "",
                "ContactName": ""
              },
              "CustomsClientNumber": "00034786",
              "AccountIdentifier": "RESIDENT OTHER",
              "RegistrationNumber": "1956/000899/07",
              "TradingName": "",
              "TaxNumber": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
              "AccountHolderStatus": "South African Resident",
              "AccountName": "XXXXXXXX"
            },
            "Type": "Entity"
          },
          "MonetaryAmount": [
            {
              "ImportExport": [
                
              ],
              "SARBAuth": {
                
              },
              "TravelMode": {
                
              },
              "ThirdParty": {
                
              },
              "ForeignValue": "100.00",
              "RandValue": "100.00",
              "SequenceNumber": "1"
            }
          ],
          "ReceivingCountry": "ZA",
          "FlowCurrency": "ZAR",
          "TotalForeignValue": "100.00",
          "TrnReference": "18062200014084",
          "PaymentDetail": {
            
          },
          "ValueDate": "2018-06-27",
          "AccountHolderStatus": "South African Resident",
          "ZAREquivalent": "100.00"
        },
        "TradeReference": "42157832",
        "SourceSystem": "ECOMMERCE-FXM",
        "EcommerceName": "FREDDYHIRSCHGROUP",
        "BeneficiaryAccountId": "309",
        "DealerType": "AD"
      },
      "transaction": {
        "Version": "FINSURV",
        "ReportingQualifier": "BOPCUS",
        "Flow": "IN",
        "Resident": {
          "Entity": {
            "VATNumber": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
            "InstitutionalSector": "02",
            "EntityName": "XXXXXXXX",
            "IndustrialClassification": "03",
            "AccountNumber": "1300035937370",
            "PostalAddress": {
              "AddressLine2": "",
              "AddressLine1": " BXXXXo ",
              "PostalCode": "8000",
              "City": "Cape Town",
              "Province": "WESTERN CAPE"
            },
            "StreetAddress": {
              "AddressLine2": "orXXXXoo",
              "AddressLine1": "thXXXX1t",
              "PostalCode": "7404",
              "City": "Cape Town",
              "Province": "WESTERN CAPE"
            },
            "ContactDetails": {
              "ContactSurname": "",
              "ContactName": ""
            },
            "CustomsClientNumber": "00034786",
            "AccountIdentifier": "RESIDENT OTHER",
            "RegistrationNumber": "1956/000899/07",
            "TradingName": "",
            "TaxNumber": "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
            "AccountHolderStatus": "South African Resident",
            "AccountName": "XXXXXXXX"
          },
          "Type": "Entity"
        },
        "NonResident": {
          "Individual": {
            "Address": {
              "AddressLine1": "Address 1",
              "Suburb": "suburb",
              "City": "city",
              "State": "province",
              "PostalCode": "8888",
              "Country": "MZ"
            },
            "Surname": "Surname",
            "Name": "First name"
          },
          "Type": "Individual"
        },
        "MonetaryAmount": [
          {
            "ImportExport": [
              
            ],
            "SARBAuth": {
              
            },
            "TravelMode": {
              
            },
            "ThirdParty": {
              
            },
            "ForeignValue": "100.00",
            "RandValue": "100.00",
            "SequenceNumber": "1"
          }
        ],
        "ReceivingCountry": "ZA",
        "FlowCurrency": "ZAR",
        "TotalForeignValue": "100.00",
        "TrnReference": "18062200014084",
        "PaymentDetail": {
          
        },
        "ValueDate": "2018-06-27",
        "AccountHolderStatus": "South African Resident",
        "ZAREquivalent": "100.00",
        "CounterpartyStatus": "South African Resident"
      }
    }
  }
];

// Since this dummy data needs to be tested in the Dev source and the Built source,
// it needs to work with requirejs and without it. So, cannot assume requirejs' 'define' will be
// defined.

if (typeof define !== "undefined")
  define({
    data: testData
  });
