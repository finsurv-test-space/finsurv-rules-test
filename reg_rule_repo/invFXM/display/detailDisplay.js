define(function () {
  return function (predef) {
    var display;

    with (predef) {
      display = {
        detailTrans: {
          ruleset: "invFXM Transaction Display Rules",
          scope: "transaction",
          fields: [
            {
              field: "Resident.Individual.AccountNumber",
              display: [
                hide().onSection("F"),
                disable(isTransactionFieldProvided("Resident.Individual.AccountNumber")).onInflow()
              ]
            },
            {
              field: "Resident.Entity.AccountNumber",
              display: [
                hide().onSection("F"),
                disable(isTransactionFieldProvided("Resident.Individual.AccountNumber")).onInflow()
              ]
            }
            // ,
            // {
            //   field: "Resident.Individual.StreetAddress.Suburb",
            //   display: [
            //     disable(isTransactionFieldProvided("Resident.Individual.StreetAddress.Suburb")).onOutFlow(),
            //     disable(isTransactionFieldProvided("Resident.Individual.StreetAddress.Suburb")).onInFlow(),
            //   ]
            // },
            // {
            //   field: "Resident.Entity.StreetAddress.Suburb",
            //   display: [
            //     disable(isTransactionFieldProvided("Resident.Entity.StreetAddress.Suburb")).onOutFlow(),
            //     disable(isTransactionFieldProvided("Resident.Entity.StreetAddress.Suburb")).onInFlow()
            //   ]
            // }
          ]
        }
      };
    }

    return display;
  }
});
