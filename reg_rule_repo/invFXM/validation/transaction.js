define(function () {
  return function (predef) {
    var transaction;

    with (predef) {
      transaction = {
        ruleset: "Transaction Rules for invFXM form",
        scope: "transaction",
        validations: [
          {
            field: ["Resident.Individual.ContactDetails.Email",
              "Resident.Entity.ContactDetails.Email"],
            rules: [
              failure('cnte1', 340, 'Email is mandatory.',
                isEmpty).onSection("ABEG"),
            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.Fax",
              "Resident.Entity.ContactDetails.Fax",
              "Resident.Individual.ContactDetails.Telephone",
              "Resident.Entity.ContactDetails.Telephone"],
            rules: [
              ignore('cntft1')
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.Suburb"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('inv_s5', 333, 'Must be completed', isEmpty).onInflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Entity.StreetAddress.Suburb"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('inv_s5', 333, 'Must be completed', isEmpty).onInflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Individual.PostalAddress.Suburb"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('inv_ps5', 333, 'Must be completed', 
              isEmpty.and(evalTransactionField("Resident.Individual.PostalAddress.AddressLine1", notEmpty))).onInflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Entity.PostalAddress.Suburb"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('inv_ps5', 333, 'Must be completed', 
              isEmpty.and(evalTransactionField("Resident.Entity.PostalAddress.AddressLine1", notEmpty))).onInflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Individual.PostalAddress.City"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('inv_c3', 334, 'Must be completed', 
              isEmpty.and(evalTransactionField("Resident.Individual.PostalAddress.AddressLine1", notEmpty))).onInflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Entity.PostalAddress.City"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('inv_c3', 334, 'Must be completed',
                isEmpty.and(evalTransactionField("Resident.Entity.PostalAddress.AddressLine1", notEmpty))).onInflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Individual.PostalAddress.PostalCode"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('inv_spc1', 338, 'Must be completed',
                isEmpty.and(evalTransactionField("Resident.Individual.PostalAddress.AddressLine1", notEmpty)))
                .onInflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Entity.PostalAddress.PostalCode"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('inv_spc1', 338, 'Must be completed',
              isEmpty.and(evalTransactionField("Resident.Entity.PostalAddress.AddressLine1", notEmpty)))
              .onInflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Individual.PostalAddress.Province"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('fxm_p1', 338, 'Must be completed',
                isEmpty.and(evalTransactionField("Resident.Individual.PostalAddress.AddressLine1", notEmpty)))
                .onInflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Entity.PostalAddress.Province"],
            minLen: 2,
            maxLen: 35,
            rules: [
              failure('fxm_p1', 338, 'Must be completed',
              isEmpty.and(evalTransactionField("Resident.Entity.PostalAddress.AddressLine1", notEmpty)))
              .onInflow().onSection("A")
            ]
          }
        ]
      }
    };

    return transaction;
  }
});

