define({
  engine: {major: "1", minor: "0"},
  dependsOn: "sbZAFlow",
  features: [
    "featureLimit", 
    "featureEntity511"
  ],
  mappings: {
    LocalCurrencySymbol: "R",
    LocalCurrencyName: "Local Currency",
    LocalCurrency: "ZAR",
    Locale: "ZA",
    LocalValue: "DomesticValue",
    Regulator: "Regulator",
    DealerPrefix: "RE",
    RegulatorPrefix: "CB",
    StateName: "Province",
      _minLenErrorType: "WARNING", // SUCCESS, ERROR, WARNING
      _maxLenErrorType: "WARNING",
      _lenErrorType: "WARNING"
  }

})