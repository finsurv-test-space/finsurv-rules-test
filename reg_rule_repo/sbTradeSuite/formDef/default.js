/**
 * Created by petruspretorius on 24/03/2015.
 */
define(function () {
  
    var _export = {
      rootTemplate: 'SARB_Summary',
      tabRootTemplate:  'tabs/tabContainer',
      detail: {
          Transaction: {
              scope: "transaction",
              detail: {
                  section: "Transaction",
                  detail: 'transaction/transaction'
              },
              summary: 'transaction_container',
              fields: [
                  "DisplayTradeSuiteFields",
                  "ValueDate",
                  "FlowCurrency",
                  "TotalForeignValue",
                  "ZAREquivalent",
                  "ConvertValue",
                  "ConversionRate",
                  "ForeignValueRatesInZAR",
                  "TransactionTotal",
                  "BranchCode",
                  "BranchName",
                  "HubCode",
                  "HubName",
                  "OriginatingBank",
                  "OriginatingCountry",
                  "CorrespondentBank",
                  "CorrespondentCountry",
                  "ReceivingBank",
                  "ReceivingCountry",
                  "ZAREquivalentCurrency",
                  "TransactionCurrency",
                  "RateConfirmation",
                  "LocationCountry",
                  "TrnReference",
                  "ReportingQualifier",
                  "Flow",
                  "TotalValue",
                  "ReplacementTransaction", 
                  "ReplacementOriginalReference"
              ]
          },
        Resident                 : {
          scope: "transaction",
          contextKey: 'Resident',
          detail    : {
            section   : "Resident",
            detail :'resident/',
            subSection: 'general'
          },
          summary   : 'Resident',
          fields: ["Resident.Type", "Resident.Individual.Surname", "Resident.Individual.Name", "Resident.Individual.Gender",
              "Resident.Individual.DateOfBirth", "Resident.Individual.IDNumber",
            "Resident.Individual.TempResPermitNumber", "Resident.Individual.TempResExpiryDate",
            "Resident.Individual.ForeignIDNumber", "Resident.Individual.ForeignIDCountry",
            "Resident.Individual.PassportNumber", "Resident.Individual.PassportCountry", "Resident.Individual.PassportExpiryDate",
            "Resident.Individual.CustomsClientNumber", "Resident.Individual.TaxNumber", "Resident.Individual.VATNumber",
            "Resident.Individual.TaxClearanceCertificateIndicator", "Resident.Individual.TaxClearanceCertificateReference", "Resident.Exception.ExceptionName",
            "Resident.Exception.Country", "Resident.Entity.EntityName", "Resident.Entity.TradingName", "Resident.Entity.RegistrationNumber",
            "Resident.Entity.InstitutionalSector", "Resident.Entity.IndustrialClassification", "Resident.Entity.CustomsClientNumber", "Resident.Entity.TaxNumber",
            "Resident.Entity.VATNumber", "Resident.Entity.TaxClearanceCertificateIndicator", "Resident.Entity.TaxClearanceCertificateReference",
            "Resident.Individual.BeneficiaryID1", "Resident.Individual.BeneficiaryID2", "Resident.Individual.BeneficiaryID3", "Resident.Individual.BeneficiaryID4",
            "Resident.Individual.CCN", "Resident.Entity.CCN", "CCNResponseCode"]
        },
        ResidentAccount          : {
          scope: "transaction",
          contextKey: 'Resident',
          detail    : {
            section   : "Resident",
            detail :'resident/account',
            subSection: 'account'
          },
          summary   : 'Resident.Account',
          fields    : [ "AccountName", "AccountIdentifier", "AccountNumber", "CardNumber", "SupplementaryCardIndicator" ]
        },
        ResidentContact          : {
          scope: "transaction",
          contextKey: 'Resident',
          detail    : {
            section   : "Resident",
            detail :'resident/address',
            subSection: 'contact'
          },
          summary   : 'Resident.ContactDetails',
          fields    : [ "ContactDetails.ContactName", "ContactDetails.ContactSurname", "ContactDetails.Email", "ContactDetails.Fax", "ContactDetails.Telephone",
            "StreetAddress.AddressLine1", "StreetAddress.AddressLine2", "StreetAddress.Suburb", "StreetAddress.City", "StreetAddress.Province", "StreetAddress.PostalCode",
            "PostalAddress.AddressLine1", "PostalAddress.AddressLine2", "PostalAddress.Suburb", "PostalAddress.City", "PostalAddress.Province", "PostalAddress.PostalCode", 
            "Individual.StreetAddress.AddressLine1",
            "Individual.StreetAddress.AddressLine2",
            "Individual.StreetAddress.Suburb",
            "Individual.StreetAddress.City",
            "Individual.StreetAddress.PostalCode",
            "Individual.StreetAddress.Province",
            "Individual.StreetAddress.Country",
            "Individual.StreetAddress.Mandate",
            "Individual.PostalAddress.AddressLine1",
            "Individual.PostalAddress.AddressLine2",
            "Individual.PostalAddress.Suburb",
            "Individual.PostalAddress.City",
            "Individual.PostalAddress.PostalCode",
            "Individual.PostalAddress.Province",
  
            "Individual.ContactDetails.ContactSurname",
            "Individual.ContactDetails.ContactName",
            "Individual.ContactDetails.Email",
            "Individual.ContactDetails.Telephone",
            "Individual.ContactDetails.Fax",
  
            "Entity.StreetAddress.AddressLine1",
            "Entity.StreetAddress.AddressLine2",
            "Entity.StreetAddress.Suburb",
            "Entity.StreetAddress.City",
            "Entity.StreetAddress.PostalCode",
            "Entity.StreetAddress.Province",
            "Entity.StreetAddress.Country",
            "Entity.StreetAddress.Mandate",
            "Entity.PostalAddress.AddressLine1",
            "Entity.PostalAddress.AddressLine2",
            "Entity.PostalAddress.Suburb",
            "Entity.PostalAddress.City",
            "Entity.PostalAddress.PostalCode",
            "Entity.PostalAddress.Province"
          ]
        },
        NonResident              : {
          scope: "transaction",
          contextKey: 'NonResident',
          detail    : {
            section   : "NonResident",
            detail :'nonResident/',
            subSection: 'general'
          },
          summary   : 'NonResident.Main',
          fields    : [ "NonResident.Type", "NonResident.Individual.Surname", "NonResident.Individual.Name", "NonResident.Individual.Gender", "NonResident.Individual.PassportNumber",
            "NonResident.Individual.PassportCountry", "NonResident.Exception.ExceptionName", "NonResident.Entity.EntityName", "NonResident.Entity.CardMerchantName",
            "NonResident.Entity.CardMerchantCode", "NonResident.Individual.IsMutualParty", "NonResident.Entity.IsMutualParty"]
        },
        NonResidentAccount       : {
          scope: "transaction",
          contextKey: 'NonResident',
          detail    : {
            section   : "NonResident",
            detail :'nonResident/account',
            subSection: 'account'
          },
          summary   : 'NonResident.Account',
          fields    : [ "AccountIdentifier", "AccountNumber" ]
        },
        NonResidentAddress       : {
          scope: "transaction",
          contextKey: 'NonResident',
          detail    : {
            section   : "NonResident",
            detail :'nonResident/address',
            subSection: 'contact'
          },
          summary   : 'NonResident.Address',
          fields    : [ "Address.AddressLine1", "Address.AddressLine2", "Address.Suburb", "Address.City", "Address.State", "Address.PostalCode", "Address.Country" ]
        },
        Monetary                 : {
          scope: "money",
          detail : {
            section   : "Money",
            detail :'monetary/main',
            subSection: 'general'
          },
          summary: 'Description',
          fields: ["SequenceNumber", "MoneyTransferAgentIndicator", "{{LocalValue}}", "ForeignValue"
                      , "{{Regulator}}Auth.IsSARBAuth",
              "CategoryCode", "CategorySubCode", "CategoryDescription", "SWIFTDetails", "StrateRefNumber",
              "{{Regulator}}Auth.AuthIssuer", "{{Regulator}}Auth.AuthFacilitator", "haveAuth",

              "{{Regulator}}Auth.RulingsSection", "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber", 
              "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate", "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
          "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber", "{{Regulator}}Auth.{{RegulatorPrefix}}AuthDate",
            "CannotCategorize", "AdHocRequirement.Subject",
            "AdHocRequirement.Description", "LocationCountry", "ReversalTrnRefNumber",
            "ReversalTrnSeqNumber", "BOPDIRTrnReference", "BOPDIRADCode", "CardChargeBack",
            "CardIndicator", "ElectronicCommerceIndicator", "POSEntryMode",
            "CardFraudulentTransactionIndicator", "ForeignCardHoldersPurchases{{LocalValue}}",
            "ForeignCardHoldersCashWithdrawals{{LocalValue}}", 
            "LoanTenor", "LoanInterestRate",
            "LoanTenorMaturityDate", "LoanInterest.InterestType",
            "LoanInterest.BaseRate", "LoanInterest.Term",
            "LoanInterest.PlusMinus", "LoanInterest.Rate", "LoanTenorType",
            "ThirdParties", "ThirdPartyKind", 'ThirdParty',
            "ADInternalAuthResponseCode", "IE_UCRHeading", "IE_TDNHeading", "IE_ICNHeading", "IE_ValidationStatus",
            "CustomsClientNumber", "LoanRefNumber", "LoanRefNumberResponseCode",
            "TaxClearanceCertificateIndicator", "allowance"
          ]
        },
        MonetaryThirdParty       : {
          scope: "money",
          detail : {
            section   : "Money",
            detail :'monetary/thirdparty',
            subSection: 'thirdparty'
          },
          summary: 'ThirdParty',
          fields: ["ThirdParty.Individual.Surname", "ThirdParty.Individual.Name",
              "ThirdParty.Individual.Gender", "ThirdParty.Individual.IDNumber",
            "ThirdParty.Individual.DateOfBirth", "ThirdParty.Individual.TempResPermitNumber",
            "ThirdParty.Individual.TempResExpiryDate",
            "ThirdParty.Individual.PassportNumber", "ThirdParty.Individual.PassportCountry",
            "ThirdParty.Individual.PassportExpiryDate",
            "ThirdParty.Entity.Name", "ThirdParty.Entity.RegistrationNumber",
            "ThirdParty.CustomsClientNumber", "ThirdParty.TaxNumber",
            "ThirdParty.VATNumber", "ThirdParty.CustomLookup2"]
        },
        MonetaryThirdPartyContact: {
          scope: "money",
          detail : {
            section   : "Money",
            detail :'monetary/thirdparty_contact',
            subSection: 'contact'
          },
          summary: 'ThirdPartyContact',
          fields : [ "ThirdParty.ContactDetails.ContactSurname", "ThirdParty.ContactDetails.ContactName", "ThirdParty.ContactDetails.Email", "ThirdParty.ContactDetails.Fax",
            "ThirdParty.ContactDetails.Telephone", "ThirdParty.StreetAddress.AddressLine1", "ThirdParty.StreetAddress.AddressLine2", "ThirdParty.StreetAddress.Suburb",
            "ThirdParty.StreetAddress.City", "ThirdParty.StreetAddress.Province", "ThirdParty.StreetAddress.PostalCode", "ThirdParty.PostalAddress.AddressLine1",
            "ThirdParty.PostalAddress.AddressLine2", "ThirdParty.PostalAddress.Suburb", "ThirdParty.PostalAddress.City", "ThirdParty.PostalAddress.Province",
            "ThirdParty.PostalAddress.PostalCode"
          ]
        },
        ImportExport             : {
          scope  : "importexport",
          detail : {
            section   : "Money",
            detail :'monetary/importexport',
            subSection: 'importExport'
          },
          summary: 'ImportExport',
          fields: ["CustomsClientNumber", "IVSResponseCodes", "ImportControlNumber", "TransportDocumentNumber", "MRNNotOnIVS", "UCR", "PaymentCurrencyCode", "PaymentValue"]
        }
  
      }
    }
    return _export;
  });
  
  