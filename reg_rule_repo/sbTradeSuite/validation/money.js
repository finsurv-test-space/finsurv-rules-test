define(function () {
  return function (predef) {
    var money;

    with (predef) {
      money = {
        ruleset: "Money Rules for Flow Form",
        scope: "money",
        validations: [
          {
            field: "SequenceNumber",
            rules: [
              warning("mseq1", 547, "Please enter the required information",
                isEmpty)
            ]
          },
          {
            field: "MoneyTransferAgentIndicator",
            minLen: 2,
            maxLen: 35,
            rules: [
              // NOTE: Updated this message
              warning("mta1", 351, "Must contain one of the following values: AD, or ADLA, CARD or MONEYGRAM, or WESTERNUNION, or PAYPAL, or EXCHANGE4FREE, or MUKURU, or MONEYTRANS or XPRESSMONEY or ZMT or ESKOM or SANLAM or MOMENTUM or TOURVEST or TOWER or IMALI or TRAVELEX or INTERAFRICA or GLOBAL or SIKHONA or FOREXWORLD or ACE or AYOBA or MASTERCURRENCY or INTERCHANGE or HELLO PAISA or TRAVEL CARD or TRAVELLERS CHEQUE or SOUTH EAST or MAMA MONEY or SHOPRITE or DAYTONA or FLASH or PEP or AFROCOIN or ECONET or PAYMENT PARTNER.",
                notEmpty.and(notMoneyTransferAgent)).onSection("ABCD"),
              warning("mta2", 351, "Must contain the value BOPDIR",
                notEmpty.and(notValueIn("BOPDIR"))).onSection("G"),
              warning("mta3", 213, "If the CategoryCode is 833, the MoneyTransferAgentIndicator may not be AD or ADLA",
                notEmpty.and(hasValueIn(["AD", "ADLA"]))).onSection("AB").onCategory("833"),
              warning("mta4", 351, "Must contain the value CARD",
                notEmpty.and(notValueIn("CARD"))).onSection("EF"),
              warning("mta5", 350, "Please enter the required information",
                isEmpty),
              warning("mta6", "S13", "This dealer is an AD and therefore may not specify ADLA",
                notEmpty.and(dealerTypeAD).and(hasValue("ADLA"))).onSection("ABCD"),
              warning("mta7", "S14", "This dealer is an ADLA and therefore only ADLA may be used",
                notEmpty.and(dealerTypeADLA).and(notValue("ADLA"))).onSection("ABCD")
            ]
          },
          {
            field: "ForeignValue",
            rules: [
              warning("mfv1", 348, "At least one of {{LocalValue}} or ForeignValue must be present",
                isEmpty.and(notMoneyField(map('LocalValue')))).onSection("ABCDEG"),
              warning("mfv2", 357, "This field shouldn't contain a negative value",
                isNegative).onSection("ABCDEG"),
              warning("mfv3", 355, "Please enter the required information",
                isEmpty.or(not(isGreaterThan(0.00))).and(notCurrencyIn(map("LocalCurrency")))).onSection("ABCDEG"),
              warning("mfv4", 362, "Not required",
                notEmpty).onSection("F"),
              warning("mfv5", 343, "Foreign value shouldn't exceed 20 digits",
                isTooLong(20)).onSection("ABCDEG"),
                // NOTE: Removed category D
              warning("mfv6", 355, "Please enter the required information",
                isEmpty.or(not(isGreaterThan(0.00))).and(hasResidentFieldValue("AccountIdentifier", "CFC RESIDENT"))).onSection("ABC")
            ]
          },
          {
            field: "CategoryCode",
            len: 3,
            rules: [
              failure("mcc1", 366, "Please enter the required information",
                isEmpty).onSection("ABG"),
              failure("mcc2", 367, "Not required",
                notEmpty).onSection("DEF"),
              // Note: Changed message
              failure("mcc3", 368, "Please enter a valid Finsurv category",
                notEmpty.and(hasInvalidCategory)).onSection("ABG"),
              failure("mcc5", 292, "If the Flow is IN and category 303, 304, 305, 306, 416 or 417 is used, the Entity element under Resident is completed, the ThirdParty Individual attributes must be completed.",
                hasTransactionField("Resident.Entity").and(notMoneyField("ThirdParty.Individual.Surname"))).onInflow().onCategory(["303", "304", "305", "306", "416", "417"]).onSection("AB"),
                failure("mcc6", 416, "Please enter the required information",
              hasTransactionField("Resident.Entity").and(
                  notMoneyField("ThirdParty.Individual.Surname").or(notMoneyField("ThirdParty.StreetAddress.AddressLine1")).or(notMoneyField("ThirdParty.PostalAddress.AddressLine1"))
                ).and(hasMoneyFieldValue("AdHocRequirement.Subject","SDA"))).onOutflow().onSection("A").onCategory(["511", "512", "513"]),
              failure("mcc7", 416, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, the third party individual and address details must be completed",
                hasTransactionField("Resident.Entity").and(
                  notMoneyField("ThirdParty.Individual.Surname").or(notMoneyField("ThirdParty.StreetAddress.AddressLine1")).or(notMoneyField("ThirdParty.PostalAddress.AddressLine1"))
                )).onInflow().onSection("A").onCategory(["511", "516"]),
              failure('mcc8', 323, 'Only required for Import undertaking customers',
                notEmpty.and(not(hasMoneyField("ThirdParty.CustomsClientNumber").and(evalMoneyField("ThirdParty.CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))).
                         and(not(evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn'))))
                )).onOutflow().onSection("AB").onCategory(['102', '104']).notOnCategory(['102/11', '104/11']),
              // LIBRA-947
              failure('mcc9', 323, 'Only required for Import undertaking customers',
                notEmpty.and(hasMoneyField("ThirdParty.CustomsClientNumber").and(evalMoneyField("ThirdParty.CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))).
                         or(not(hasMoneyField("ThirdParty.CustomsClientNumber")).and(evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))
                )).onOutflow().onSection("AB").onCategory(['101', '103']).notOnCategory(['101/11', '103/11'])
            ]
          },
          {
            field: "CategorySubCode",
            rules: [
              failure("msc1", 369, "Please enter a valid sub category code",
                notEmpty.and(hasInvalidSubCategory.and(notValue("*")))).onSection("ABG"),
                // Note: Removed category F and changed message
              failure("msc2", 370, "Not required",
                notEmpty).onSection("CDE"),
              // NOTE: This message is not in the SARB Spec
              failure("msc3", 368, "This field contains the old category code. Please select a new Finsurv category",
                notEmpty.and(hasValue("*"))).onSection("ABG")
            ]
          },
          {
            field: "SWIFTDetails",
            minLen: 2,
            maxLen: 100,
            rules: [
              warning("mswd1", 293, "Not required",
                notEmpty).onSection("EF")
            ]

          },
          {
            field: "StrateRefNumber",
            minLen: 1,
            maxLen: 30,
            rules: [
              warning("msrn1", 371, "Should be completed with the wording ON MARKET or OFF MARKET if the category is 601/01 or 603/01",
                isEmpty).onSection("AB").onCategory(["601/01", "603/01"]),
              warning("msrn2", 371, "Should be completed with the wording ON MARKET or OFF MARKET if the Resident ExceptionName is STRATE",
                isEmpty.and(hasResException("STRATE"))).onSection("AB"),
                // NOTE: Removed section G
              warning("msrn3", 372, "Not required",
                notEmpty).onSection("CDEF"),
              warning("msrn4", 565, "Should only contain ON MARKET or OFF MARKET",
                notEmpty.and(notValueIn(["ON MARKET", "OFF MARKET"]))).onSection("AB")
            ]
          },
          {
            field: ["Travel", "Travel.Surname", "Travel.Name", "Travel.IDNumber", "Travel.DateOfBirth", "Travel.TempResPermitNumber"],
            rules: [
              deprecated("mtvl1", "S11", "Please add third party details for new Finsurv",
                notEmpty)
            ]
          },
          {
            field: "LoanRefNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              warning("mlrn1", 373, "Please enter the required information",
                isEmpty).onSection("AB").onCategory(["801", "802", "803", "804"]),
              warning("mlrn2", 374, "Loan reference number must be 99012301230123",
                notValue("99012301230123").and(hasMoneyFieldValue("LocationCountry", "LS"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
              warning("mlrn3", 374, "Loan reference number must be 99456745674567",
                notValue("99456745674567").and(hasMoneyFieldValue("LocationCountry", "SZ"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
              warning("mlrn4", 374, "Loan reference number must be 99789078907890",
                notValue("99789078907890").and(hasMoneyFieldValue("LocationCountry", "NA"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
              warning("mlrn5", 374, "Loan reference number must be 99012301230123",
                notValue("99012301230123").and(hasMoneyFieldValue("LocationCountry", "LS"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              warning("mlrn6", 374, "Loan reference number must be 99456745674567",
                notValue("99456745674567").and(hasMoneyFieldValue("LocationCountry", "SZ"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              warning("mlrn7", 374, "Loan reference number must be 99789078907890",
                notValue("99789078907890").and(hasMoneyFieldValue("LocationCountry", "NA"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              warning("mlrn8", 373, "Please enter the required information",
                isEmpty).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              warning("mlrn9", 375, "Not required",
                notEmpty).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
              // NOTE: Removed category F on mlrn10
              warning("mlrn10", 375, "Not required",
                notEmpty).onSection("CDE"),
              warning("mlrn11", 375, "Not required",
                notEmpty).onSection("AB").notOnCategory(["801", "802", "803", "804", "106", "309/04", "309/05", "309/06", "309/07"]),
              warning("mlrn12", 374, "The LoanRefNumber must contain only characters 0-9",
                notEmpty.and(notPattern(/^\d+$/))).onSection("ABG")
            ]
          },
          {
            field: "LoanTenor",
            minLen: 9,
            maxLen: 10,
            rules: [
              warning("mlt1", 376, "Please enter a valid date format: YYYY-MM-DD",
                isEmpty.or(notValue("ON DEMAND").and(notDatePattern))).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
              warning("mlt2", 346, "Please enter a future date in the format: CCYY-MM-DD",
                notEmpty.and(hasDatePattern).and(isDaysInPast(-1))).onSection("ABG"),
                // NOTE: Removed section E and F
              warning("mlt3", 377, "Not required",
                notEmpty).onSection("CD")
            ]
          },
          {
            field: "LoanInterestRate",
            minLen: 1,
            maxLen: 25,
            rules: [
              warning("mlir1", 378, "If the Flow is Out and CategoryCode 810 or 815 or 816 or 817 or 818 or 819 is used, must contain a value of reflecting interest rate percentage of the loan in the proper format. E.g. 0.00, 5.12, BASE PLUS 1.25, BASE MINUS 1.00, 12 LIBOR, 12 LIBOR PLUS 1.25, 6 LIBOR MINUS 0.5, 12 JIBAR, 9 JIBAR PLUS 1.25, 6 JIBAR MINUS 0.5",
                notPattern("^(\\d+(\\.\\d{1,2})?|BASE PLUS \\d+(\\.\\d{1,2})?|BASE MINUS \\d+(\\.\\d{1,2})?|\\d{1,2} [A-Z]{4,8}|\\d{1,2} [A-Z]{4,8} PLUS \\d+(\\.\\d{1,2})|\\d{1,2} [A-Z]{4,8} MINUS \\d+(\\.\\d{1,2})|[A-Z]{4,8})$")).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
              // NOTE: Removed section F
              warning("mlir2", 379, "Not required",
                notEmpty).onSection("CDE"),
              warning("mlir3", 380, "If the Flow is Out and CategoryCode 309/04 to 309/07 is used, must be completed reflecting the percentage interest paid in the format 0.00",
                notPattern("^\\d{1,3}\\.\\d{2}?$")).onOutflow().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
              warning("mlir4", 380, "Please enter the required information in the format 0.00",
                notPattern("^\\d{1,3}\\.\\d{2}?$")).onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
              warning("mlir5", "S12", "Interest rate cannot be greater than 100%",
                hasPattern("^\\d{3}\\.\\d{2}?$")).onOutflow().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
              warning("mlir6", "S12", "It is unlikely that an interest rate greater than 100% is being charged",
                hasPattern("^\\d{3}\\.\\d{2}?$")).onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
              warning("mlir7", 379, "Not required",
                notEmpty).onOutflow().onSection("ABG").notOnCategory(["810", "815", "816", "817", "818", "819", "309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
              warning("mlir8", 379, "May not be completed unless a loan related transaction is being captured",
                notEmpty).onInflow().onSection("ABG").notOnCategory(["810", "815", "816", "817", "818", "819", "309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"])
            ]
          },
          {
            field: "{{Regulator}}Auth.RulingsSection",
            minLen: 2,
            maxLen: 30,
            rules: [
              warning("mars1", 381, "Must be completed if the Flow is OUT and no data is supplied under either {{DealerPrefix}}InternalAuthNumber or {{RegulatorPrefix}}AuthAppNumber",
                isEmpty.and(notMoneyField(map("{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber")).and(notMoneyField(map("{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber"))))).onOutflow().onSection("ABG").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              warning("mars2", 382, "Not required",
                notEmpty).onSection("CDEF"),
              warning("mars3", 566, "If the flow is OUT and the Subject is REMITTANCE DISPENSATION then the RulingsSection must be 'Circular 06/2019'",
                hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION").and(notValueIn(["Circular 06/2019", "CIRCULAR 06/2019"]))).onOutflow().onSection("A"),
              warning("mars4", 566, "If the flow is IN and the Subject is REMITTANCE DISPENSATION (for category 400) then the RulingsSection must be 'Circular 06/2019'",
                hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION").and(notValueIn(["Circular 06/2019", "CIRCULAR 06/2019"]))).onInflow().onSection("A"),
              warning("mars5", 566, "If the flow is IN and the RulingsSection is 'Circular 06/2019', then the Subject must be REMITTANCE DISPENSATION (for category 400)",
                notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION").and(hasValueIn(["Circular 06/2019", "CIRCULAR 06/2019"]))).onInflow().onSection("A")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              warning("maian1", 381, "Must be completed if the Flow is OUT and no data is supplied under either RulingsSection or {{RegulatorPrefix}}AuthAppNumber",
                isEmpty.and(notMoneyField(map("{{Regulator}}Auth.RulingsSection")).and(notMoneyField(map("{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber"))).and(hasResidentField("CustomsClientNumber").and(hasMoneyField("CategoryCode"))))).onOutflow().onSection("ABG").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              failure("maian2", 382, "Not required",
                notEmpty).onSection("CDEF")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
            rules: [
              failure("maiad1", 385, "If {{DealerPrefix}}InternalAuthNumber has a value, it must be completed",
                isEmpty.and(hasMoneyField(map("{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber")))).onSection("ABG"),
              failure("maiad2", 382, "Not required",
                notEmpty).onSection("CDEF"),
              failure("maiad3", 215, "Please enter a valid date format: CCYY-MM-DD",
                notEmpty.and(notPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/))).onSection("ABG")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              warning("masan1", 381, "Must be completed if the Flow is OUT and no data is supplied under either RulingsSection or {{DealerPrefix}}InternalAuthNumber",
                isEmpty.and(notMoneyField(map("{{Regulator}}Auth.RulingsSection")).and(notMoneyField(map("{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber"))))).onOutflow().onSection("ABG").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              warning("masan2", 386, "If the Flow is IN and the Subject is SETOFF, it must be completed",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SETOFF"))).onInflow().onSection("AB"),
              //TODO: Must be completed if the RegistrationNumber is registered as an IHQ entity
              warning("masan3", 382, "Not required",
                notEmpty).onSection("CDEF"),
              warning("masan4", 387, "Please enter the required information",
                isEmpty.and(evalTransactionField("Resident.Entity.RegistrationNumber", isInLookup('ihqCompanies', 'registrationNumber')).or(evalMoneyField("AdHocRequirement.Subject", hasPattern("^IHQ\\d{3}$"))))).onSection("A")
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              failure("masar1", 388, "If {{RegulatorPrefix}}AuthAppNumber has a value, it must be completed",
                isEmpty.and(hasMoneyField(map("{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber")))).onSection("ABG"),
              //warning("masar2", 389, "Must be in the format CCYY-nnnnnnnn where CC is the Century, YY is the year and nnnnnnnn is the e-docs number or any other {{Regulator}} authorisation number allocated by the {{Regulator}} in the reply to the application",
              //        notEmpty.and(notPattern("^(19|20)\\d{2}-\\d+$"))).onSection("ABG"),
              warning("masar3", 382, "Not required",
                notEmpty).onSection("CDEF")
            ]
          },
          {
            field: "CannotCategorize",
            minLen: 2,
            maxLen: 100,
            rules: [
              // NOTE: Added section G
              warning("mexc1", 390, "Please enter the required information",
                isEmpty).onSection("ABG").onCategory("830"),
              warning("mexc2", 392, "Not required",
                notEmpty).onSection("CDEF")
            ]
          },
          {
            field: "AdHocRequirement.Subject",
            minLen: 2,
            maxLen: 30,
            rules: [
              warning("madhs1", 393, "If the Subject contains a value, it must be INVALIDIDNUMBER, AIRPORT, IHQnnn (where nnn is numeric) or SETOFF or ZAMBIAN GRAIN or YES or NO or HOLDCO or SDA",
                notEmpty.and(notValueIn(["INVALIDIDNUMBER", "AIRPORT", "SETOFF", "ZAMBIAN GRAIN", "YES", "NO", "HOLDCO", "SDA", "REMITTANCE DISPENSATION"]).and(notPattern("^IHQ\\d{3}$")))).onSection("AB"),
              // NOTE: Removed section F
              warning("madhs2", 394, "Not required",
                notEmpty).onSection("CDE"),
              warning("madhs3", 395, "If the Subject contains the value AIRPORT the Flow must be IN",
                notEmpty.and(hasValue("AIRPORT")).and(notInflow)).onSection("AB"),
              //warning("madhs4", 396, "If the Subject is IHQnnn the AccountIdentifier must be FCA RESIDENT",
              //  notEmpty.and(hasPattern("^IHQ\\d{3}$")).and(notResidentFieldValue("AccountIdentifier", "FCA RESIDENT"))).onSection("AB"),
              //If the Subject contains the value IHQnnn, and the RegistrationNumber under Resident EntityCustomer is equal to a registered IHQ registration number, the value of SUBJECT must be equal to the IHQ code
              //Invalid IHQ number
              warning("madhs5", 400, "If the value is SETOFF, the CategoryCode and SubCategory must be 100 or 101/01 to 101/11 or 102/01 to 102/11 103/01 to 103/11 or 104/01 to 104/11 or 105 or 106 or 107 or 108",
                notEmpty.and(hasValue("SETOFF"))).onSection("AB").notOnCategory(["100", "101", "102", "102", "103", "104", "105", "106", "107", "108"]),
              warning("madhs6", 401, "If the value is SETOFF, the Flow must be IN",
                notEmpty.and(hasValue("SETOFF")).and(notInflow)).onSection("AB"),
              warning("madhs7", 448, "If the Value is ZAMBIAN GRAIN, the CategoryCode must be 101/01 or 109/01 or 110",
                notEmpty.and(hasValue("ZAMBIAN GRAIN"))).onSection("AB").notOnCategory(["101/01", "109/01", "110"]),
              warning("madhs8", 393, "If the Flow is OUT and the CategoryCode is 512/01 to 512/07 or 513 and the Resident Entity element is used, the Subject must be YES or NO",
                hasTransactionField("Resident.Entity").and(notValueIn(["YES", "NO"]))).onOutflow().onSection("A").onCategory(["512", "513"]),
              warning("madhs9", 403, "If Subject is AIRPORT, the CategoryCode must be 830",
                notEmpty.and(hasValue("AIRPORT"))).onSection("AB").notOnCategory("830"),
              warning("madhs10", 403, "If Subject is AIRPORT, the Resident Entity RegistrationNumber must be GOVERNMENT",
                notEmpty.and(hasValue("AIRPORT")).and(notTransactionFieldValue("Resident.Entity.RegistrationNumber", "GOVERNMENT"))).onSection("AB").onCategory("830"),
              warning("madhs11", 403, "If Subject is AIRPORT, the Resident EntityName must be CORPORATION FOR PUBLIC DEPOSITS",
                notEmpty.and(hasValue("AIRPORT")).and(notTransactionFieldValue("Resident.Entity.EntityName", "CORPORATION FOR PUBLIC DEPOSITS"))).onSection("AB").onCategory("830"),
              warning("madhs12", 567, "Amount shouldn't exceed R3 000",
                notEmpty.and(hasValue("REMITTANCE DISPENSATION")).and(hasSumLocalValue('>', "5000"))).onOutflow().onSection("A"),
              warning("madhs13", 393, "Not allowed for Remittance dispensation",
                notEmpty.and(hasValue("REMITTANCE DISPENSATION"))).onSection("C"),
              warning("madhs14", 437, "If NonResident ExceptionName is IHQ, the value must be IHQnnn (where nnn is numeric)",
                hasTransactionFieldValue("NonResident.Exception.ExceptionName", "IHQ").and(isEmpty.or(not(hasPattern(/^IHQ\d{3}$/))))).onSection("A"),
              //warning("madhs15", 566, "If the Flow is OUT and the RulingsSection is Circular 06/2019 the {{LocalValue}} must be equal to or less than 3000.00 and the Subject must be REMITTANCE DISPENSATION. ",
              // evalMoneyField(map("{{Regulator}}Auth.RulingsSection"), hasPattern(/^Circular.*$/)).and(notValue("REMITTANCE DISPENSATION"))).onOutflow().onSection("A"),
              warning("madhs16", 397, "If the RegistrationNumber is equal to a registration number of an IHQ entity as per the IHQ table, the Subject must be IHQnnn related to the IHQ entity. Must be completed if the RegistrationNumber is registered as an IHQ entity or the Subject is IHQnnn",
               evalTransactionField("Resident.Entity.RegistrationNumber", isInLookup('ihqCompanies', 'registrationNumber')).
                 and(not(hasLookupTransactionFieldValue("ihqCompanies", "ihqCode", "registrationNumber", "Resident.Entity.RegistrationNumber")))).onSection("A"),
              warning("madhs17", 399, "Please enter a valid IHQ number",
               notEmpty.and(hasPattern("^IHQ\\d{3}$")).and(not(isInLookup('ihqCompanies', 'ihqCode')))).onSection("A"),
              // warning("madhs18", 437, "If Resident Registration Number is an IHQ, the value must be IHQnnn (where nnn is the number of the associated IHQ company)",
              //   evalTransactionField('Resident.Entity.RegistrationNumber', isInLookup('ihqCompanies', 'registrationNumber')).
              //   and(isEmpty.or(not(hasLookupTransactionFieldValue('ihqCompanies', 'ihqCode', 'registrationNumber', 'Resident.Entity.RegistrationNumber'))))).onSection("A"),
              warning("madhs19", 437, "If Resident Registration Number is not an IHQ, the value can only be be IHQnnn if Non Resident Exception is IHQ",
                not(evalTransactionField('Resident.Entity.RegistrationNumber', isInLookup('ihqCompanies', 'registrationNumber'))).
                and(notNonResException("IHQ")).
                and(hasPattern("^IHQ\\d{3}$"))).onSection("A"),
              warning("madhs20", 292, "If Flow is OUT and category is 511/01 to 511/07 and Resident Entity is used and Third Party Individual details are provided then Subject must be SDA",
                notValue("SDA").and(hasTransactionField("Resident.Entity")).and(hasMoneyField("ThirdParty.Individual"))).onOutflow().onCategory("511").onSection("A"),
              warning("madhs21", 567, "If the Flow is IN and the Subject is REMITTANCE DISPENSATION, then category 400 must be used",
                notEmpty.and(hasValue("REMITTANCE DISPENSATION"))).onInflow().notOnCategory("400").onSection("A")
            ]
          },
          {
            field: "AdHocRequirement.Description",
            minLen: 2,
            maxLen: 100,
            rules: [
              warning("madhd1", 402, "Please enter the required information",
                isEmpty.and(hasMoneyField("AdHocRequirement.Subject"))).onSection("ABG"),
              warning("madhd2", 454, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "NO").and(notValue("NONE")))).onSection("A"),
              // NOTE: Removed category F
              warning("madhd3", 404, "Not required",
                notEmpty).onSection("CDE")
            ]
          },
          {
            field: "LocationCountry",
            len: 2,
            rules: [
              warning("mlc1", 405, "Must be completed",
                isEmpty).onSection("ABG"),
              warning("mlc2", 238, "Invalid SWIFT country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("ABFG"),
              warning("mlc3", 290, "SWIFT country code may not be {{Locale}} (except if the Non Resident Exception is IHQ)",
                notEmpty.and(hasValue(map("Locale")).and(not(hasNonResException("IHQ"))))).onSection("ABFG"),
              warning("mlc4", 238, "The LocationCountry EU may only be used if the CategoryCode is 513",
                notEmpty.and(hasValue("EU"))).onSection("AB").notOnCategory("513"),
              warning("mlc5", 407, "Not required",
                notEmpty).onSection("CDE"),
              warning("mlc6", 405, "Please enter the required information",
                isEmpty.and(hasMoneyCardValue)).onSection("F"),
              warning("mlc7", 407, "Please enter the required information",
                notEmpty.and(not(hasMoneyCardValue))).onSection("F"),
              warning("mlc8", 398, "Value must be {{Locale}} if the Non Resident Exception is IHQ",
                notEmpty.and(not(hasValue(map("Locale"))).and(hasNonResException("IHQ")))).onSection("A")
            ]
          },
          {
            field: "ReversalTrnRefNumber",
            minLen: 1,
            maxLen: 30,
            rules: [
              warning("mrtrn1", 408, "Please enter the required information",
                isEmpty).onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              warning("mrtrn2", 406, "If ReversalTrnRefNumber has a value, the CardChargeBack must be Y",
                notEmpty.and(notMoneyFieldValue("CardChargeBack", "Y"))).onSection("E"),
              warning("mrtrn3", 285, "Not required",
                notEmpty).onSection("F"),
              warning("mrtrn4", 219, "Additional spaces identified in data content",
                notEmpty.and(hasAdditionalSpaces)).onSection("ABG"),
                // TODO:  This rule has been updated in the SARB spec
                //        * It is te inverse of mtrn1
                //        * According to the SARB spec it should be completed when the Reporting entity code is 301?
              warning("mrtrn5", 408, "Not required",
                notEmpty).onSection("AB").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"])
               //TODO: 410 Original transaction and SequenceNumber combination, with an opposite flow, not stored on database
            ]
          },
          {
            field: "ReversalTrnSeqNumber",
            minLen: 1,
            maxLen: 3,
            rules: [
              warning("mrtrs1", 408, "Please enter the required information",
                isEmpty).onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              // NOTE: Removed '(And not otherwise)' from the message
              warning("mrtrs2", 409, "Please enter the required information",
                isEmpty.and(hasMoneyField("ReversalTrnRefNumber")).or(notEmpty.and(notMoneyField("ReversalTrnRefNumber")))).onSection("E"),
              warning("mrtrs3", 285, "Not required",
                notEmpty).onSection("F")
            ]
          },
          {
            field: "BOPDIRTrnReference",
            minLen: 1,
            maxLen: 30,
            rules: [
              // NOTE: Removed section AEF
              warning("mdirtr1", 413, "Not required",
                notEmpty).onSection("BCD")
            ]
          },
          {
            field: "BOPDIR{{DealerPrefix}}Code",
            len: 3,
            rules: [
              // TODO: This rule has completely been changed in the SARB spec
              warning("mdircd1", 415, "Not required",
                notEmpty).onSection("ABCDEF"),
              warning("mdircd2", 415, "Must not be completed",
                notEmpty).onSection("BCDE"),
            ]
          },
          {
            field: "ThirdParty.Individual",
            rules:[
              // TODO: This rule has completely been changed in the SARB spec
              warning("mtpi1", 292, "Please enter the required information",
                isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject","SDA"))).onOutflow().onCategory("511").onSection("A")
            
            ]
          },
          {
            field: "ThirdParty.Individual.Surname",
            minLen: 1,
            maxLen: 35,
            rules: [
              warning("mtpisn1", 416, "Please enter the required information",
                isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject","SDA"))).onOutflow().onSection("AB").onCategory(["511", "512", "513"]),
              warning("mtpisn2", 416, "Not required",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Name"))).onSection("AB"),
              warning("mtpisn3", 416, "Please enter the required information",
                isEmpty.and(notTransactionField("Resident.Individual.PassportNumber"))).onSection("AB").onCategory("256"),
              warning("mtpisn4", 416, "Please enter the required information",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("AB").onCategory(["255", "256"]),
              warning("mtpisn5", 417, "Surname shouldn't be the same as the applicant",
                notEmpty.and(matchesTransactionField("Resident.Entity.EntityName"))).onSection("ABE"),
              warning("mtpisn6", 416, "Please enter the required information",
                isEmpty.and(hasResidentFieldValue("SupplementaryCardIndicator", "Y"))).onSection("E"),
              // NOTE: Removed section F
              warning("mtpisn7", 418, "Not required",
                notEmpty).onSection("CD"),
              warning("mtpisn8", 416, "If the Flow is IN and category 303, 304, 305, 306, 416 or 417 is used and Resident Entity is completed, then must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("AB").onCategory(["303", "304", "305", "306", "416", "417"]),
              warning("mtpisn9", 416, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("A").onCategory(["511", "516"]),
              warning("mtpisn10", 418, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              warning("mtpisn11", 564, "If the Subject is SDA and the EntityCustomer element is completed in respect of any category the ThirdParty Individual Surname must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).onSection("AB"),
            ]
          },
          {
            field: "ThirdParty.Individual.Name",
            minLen: 1,
            maxLen: 35,
            rules: [
              warning("mtpinm1", 419, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname"))).onSection("AB"),
              warning("mtpinm2", 419, "Please enter the required information",
                isEmpty.and(notTransactionField("Resident.Individual.PassportNumber"))).onSection("AB").onCategory("256"),
              warning("mtpinm3", 419, "Please enter the required information",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("AB").onCategory(["255", "256"]),
              warning("mtpinm4", 417, "Name shouldn't be the same as the applicant",
                notEmpty.and(matchesTransactionField("Resident.Entity.EntityName"))).onSection("ABE"),
              warning("mtpinm5", 420, "Not required",
                notEmpty).onSection("CDF"),
              warning("mtpinm6", 419, "Please enter the required information",
                isEmpty.and(hasResidentFieldValue("SupplementaryCardIndicator", "Y"))).onSection("E"),
              warning("mtpinm7", 420, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.Individual.Gender",
            len: 1,
            rules: [
              warning("mtpig1", 421, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname"))).onSection("AB"),
              warning("mtpig2", 422, "Please enter valid gender",
                notEmpty.and(notValueIn(["M", "F"]))).onSection("ABEG"),
              warning("mtpig3", 421, "Please enter the required information",
                isEmpty.and(notTransactionField("Resident.Individual.PassportNumber"))).onSection("AB").onCategory("256"),
              warning("mtpig4", 421, "Please enter the required information",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("AB").onCategory("255"),
              warning("mtpig5", 423, "Not required",
                notEmpty).onSection("CDF"),
              warning("mtpig6", 421, "Please enter the required information",
                isEmpty.and(hasResidentFieldValue("SupplementaryCardIndicator", "Y"))).onSection("E"),
              warning("mtpig7", 423, "Please enter the required information",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              warning("mtpig8", "S11", "The gender should match the ID Number",
                notEmpty.and(hasMoneyField("ThirdParty.Individual.IDNumber").and(notMatchesGenderToIDNumber("ThirdParty.Individual.IDNumber")))).onSection("ABEG")
            ]
          },
          {
            field: "ThirdParty.Individual.IDNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              warning("mtpiid1", 424, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname")).and(notMoneyField("ThirdParty.Individual.TempResPermitNumber"))).onSection("AB"),
              warning("mtpiid2", 425, "Please enter the required information",
                isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject","SDA"))).onOutflow().onSection("AB").onCategory(["511", "512", "513"]),
              warning("mtpiid3", 426, "Please enter a valid ID number",
                notEmpty.and(notValidRSAID).and(notMoneyFieldValue("AdHocRequirement.Subject", "INVALIDIDNUMBER"))).onSection("ABEG"),
              warning("mtpiid4", 427, "Not required",
                notEmpty).onSection("CDF"),
              warning("mtpiid5", 424, "Please enter the required information",
                isEmpty.and(hasResidentFieldValue("SupplementaryCardIndicator", "Y")).and(notMoneyField("ThirdParty.Individual.TempResPermitNumber"))).onSection("E"),
              warning("mtpiid6", 553, "ID number shouldn't be the same as the applicant",
                notEmpty.and(matchesTransactionField("Resident.Individual.IDNumber"))).onSection("ABEG"),
              warning("mtpiid7", 427, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              warning("mtpiid8", 564, "Please enter the required information",
                isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).onSection("AB"),
              warning("mtpiid9", 425, "If category or 511/01 to 511/07 or 516 is used and flow is IN in cases where the Entity Customer element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("AB").onCategory(["511", "516"])
            ]
          },
          {
            field: "ThirdParty.Individual.DateOfBirth",
            rules: [
              warning("mtpibd1", 428, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname"))).onSection("AB"),
              warning("mtpibd2", 428, "Please enter the required information",
                isEmpty.and(notTransactionField("Resident.Individual.PassportNumber"))).onSection("AB").onCategory("256"),
              warning("mtpibd3", 428, "Please enter the required information",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("AB").onCategory("255"),
              warning("mtpibd4", 429, "Not required",
                notEmpty).onSection("CDF"),
              warning("mtpibd5", 428, "Please enter the required information",
                isEmpty.and(hasResidentFieldValue("SupplementaryCardIndicator", "Y"))).onSection("E"),
              warning("mtpibd6", 428, "Please enter a valid date format: YYYY-MM-DD",
                notEmpty.and(notPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/).or(isDaysInFuture(0)))).onSection("ABEG"),
              warning("mtpibd7", 429, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              warning("mtpibd8", "S11", "The date of birth should match the ID Number",
                notEmpty.and(hasMoneyField("ThirdParty.Individual.IDNumber").and(notMatchThirdPartyDateToIDNumber))).onSection("ABEG")

            ]
          },
          {
            field: "ThirdParty.Individual.TempResPermitNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              warning("mtpitp1", 424, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname")).and(notMoneyField("ThirdParty.Individual.IDNumber"))).onSection("AB"),
              warning("mtpitp2", 299, "Not required",
                notEmpty).onSection("AB").onCategory(["511", "512", "513", "516"]),
              warning("mtpitp3", 424, "Please enter the required information",
                isEmpty.and(hasResidentFieldValue("SupplementaryCardIndicator", "Y")).and(notMoneyField("ThirdParty.Individual.IDNumber"))).onSection("E"),
              warning("mtpitp4", 430, "Not required",
                notEmpty).onSection("CDF"),
              warning("mtpitp5", 554, "Must not be equal to TempResPermitNumber under Resident IndividualCustomer",
                notEmpty.and(matchesTransactionField("Resident.Individual.TempResPermitNumber"))).onSection("ABEG"),
              warning("mtpitp6", 430, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              warning("mtpitp7", 282, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname")).and(notMoneyField("ThirdParty.Individual.PassportNumber"))).onOutflow().onCategory("250").onSection("AB")
            ]
          },
          {
            field: "ThirdParty.Individual.PassportNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              warning("mtpipn1", 431, "Please enter the required information",
                isEmpty.and(notTransactionField("Resident.Individual.PassportNumber"))).onSection("AB").onCategory("256"),
              warning("mtpipn2", 431, "If the category is 255 and the EntityCustomer element is completed, IndividualThirdPartyPassportNumber must be completed.",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("AB").onCategory("255"),
              warning("mtpipn3", 432, "Not required",
                notEmpty).onSection("CDEFG"),
              warning("mtpipn4", 555, "Passport number shouldn't be the same as the applicant",
                notEmpty.and(matchesTransactionField("Resident.Individual.PassportNumber"))).onSection("AB"),
              warning("mtpipn5", 432, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              warning("mtpipn6", 282, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname")).and(notMoneyField("ThirdParty.Individual.TempResPermitNumber"))).onOutflow().onCategory("250").onSection("AB")
            ]
          },
          {
            field: "ThirdParty.Individual.PassportCountry",
            len: 2,
            rules: [
              warning("mtpipc1", 433, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.PassportNumber"))).onSection("AB"),
              warning("mtpipc2", 434, "Not required",
                notEmpty).onSection("CDEFG"),
              warning("mtpipc3", 238, "Please enter a valid SWIFT code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("AB"),
              warning("mtpipc4", 434, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.Entity.Name",
            minLen: 1,
            maxLen: 50,
            rules: [
              //warning("mtpenm1", "F01", "In the case of Business travel, category 255, this attribute must be used", // See wording of field
              //        isEmpty).onSection("AB").onCategory("255"),
              // NOTE: Removed section F
              warning("mtpenm2", 435, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtpenm3", 435, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.Entity.RegistrationNumber",
            minLen: 2,
            maxLen: 30,
            rules: [
              // NOTE: Removed section F
              warning("mtpern1", 436, "Not required",
                notEmpty).onSection("CDE"),
              // NOTE: Removed section E
              warning("mtpern2", 556, "Registration number shouldn't be the same as the companies registration number",
                notEmpty.and(matchesTransactionField("Resident.Entity.RegistrationNumber"))).onSection("ABG"),
              warning("mtpern3", 436, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.CustomsClientNumber",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("mtpccn1", 322, "Customs Client Number must be numeric and contain between 8 and 13 digits. If your number is 5 digits, please add 000 before your number i.e. 00012345",
                notEmpty.and(notPattern(/^\d{8,13}$/))).onSection("ABG"),
              warning("mtpccn2", 322, "Please enter a valid Customs client number",
                notEmpty.and(hasValue("70707070"))).onSection("ABG"),
              // Removed section F
              warning("mtpccn3", 321, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtpccn4", 321, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              warning('mtpccn5', 322, 'CustomsClientNumber should pass one of the validations for either CCN, ID Number or Tax Number',
                notEmpty.and(hasPattern(/^\d{8,13}$/)).and(not(isValidCCN.or(isValidRSAID.or(isValidZATaxNumber)))))

            ]
          },
          {
            field: "ThirdParty.TaxNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              warning("mtptx1", 439, "If category 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onOutflow().onSection("AB").onCategory(["512", "513"]),
                // NOTE: Removed section E and F
              warning("mtptx2", 438, "Not required",
                notEmpty).onSection("CD"),
              // NOTE: Removed section E
              warning("mtptx3", 557, "Tax number shouldn't be the same as the applicant",
                notEmpty.and(matchesTransactionField("Resident.Individual.TaxNumber").or(matchesTransactionField("Resident.Entity.TaxNumber")))).onSection("ABG"),
              warning("mtptx4", 439, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("AB").onCategory(["511", "516"]),
              warning("mtptx5", 438, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.VATNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              // NOTE: Removed section E and F
              warning("mtpvn1", 569, "Not required",
                notEmpty).onSection("CD"),
              warning("mtpvn2", 558, "VAT number shouldn't be the same as the applicants",
                notEmpty.and(matchesTransactionField("Resident.Individual.VATNumber").or(matchesTransactionField("Resident.Entity.VATNumber")))).onSection("ABEG"),
              warning("mtpvn3", 569, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.StreetAddress.AddressLine1",
            minLen: 2,
            maxLen: 70,
            rules: [
              warning("fe_mtpsal11", 456, "Please enter the required information",
                isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject","SDA"))).onOutflow().onSection("A").onCategory(["511", "512", "513"]),
              // NOTE: Removed section E and F
              warning("mtpsal12", 441, "Must not be completed",
                notEmpty).onSection("CD"),
              warning("mtpsal13", 456, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("A").onCategory(["511", "516"]),
              warning("mtpsal14", 441, "If the Subject is REMITTANCE DISPENSATION the ThirdParty Street AddressLine1 must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.StreetAddress.AddressLine2",
            minLen: 2,
            maxLen: 70,
            rules: [
              // NOTE: Removed section F
              warning("mtpsal21", 442, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtpsal22", 442, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.StreetAddress.Suburb",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("mtpsas1", 458, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.StreetAddress.AddressLine1"))).onSection("AB"),
              // NOTE: Removed section F
              warning("mtpsas2", 443, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtpsas3", 443, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.StreetAddress.City",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("mtpsac1", 459, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.StreetAddress.AddressLine1"))).onSection("AB"),
              // NOTE: Removed section F
              warning("mtpsac2", 444, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtpsac3", 444, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.StreetAddress.Province",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("mtpsap1", 485, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.StreetAddress.AddressLine1"))).onSection("AB"),
                // NOTE: Removed section F
                warning("mtpsap2", 445, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtpsap3", 336, "Please enter a valid province",
                notEmpty.and(notValidProvince)).onSection("ABG"),
              warning("mtpsap4", 445, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.StreetAddress.PostalCode",
            minLen: 2,
            maxLen: 10,
            rules: [
              warning("mtpsaz1", 501, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.StreetAddress.AddressLine1"))).onSection("AB"),
              // NOTE: Removed section F
              warning("mtpsaz2", 449, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtpsaz3", 338, "Please enter a valid postal code",
                notEmpty.and(notValidPostalCode)).onSection("AB"),
              warning("mtpsaz4", 449, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.PostalAddress.AddressLine1",
            minLen: 2,
            maxLen: 70,
            rules: [
              warning("fe_mtppal11", 512, "Please enter the required information",
                isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject","SDA"))).onOutflow().onSection("A").onCategory(["511", "512", "513"]),
              // NOTE: Removed section E and F
              warning("mtppal12", 450, "Not required",
                notEmpty).onSection("CD"),
              warning("mtppal13", 512, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("A").onCategory(["511", "516"]),
              warning("mtppal14", 450, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.PostalAddress.AddressLine2",
            minLen: 2,
            maxLen: 70,
            rules: [
              // NOTE: Removed category F
              warning("mtppal21", 451, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtppal22", 451, "Please enter the required information",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.PostalAddress.Suburb",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("mtppas1", 534, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.PostalAddress.AddressLine1"))).onSection("AB"),
              // NOTE: Removed category F
              warning("mtppas2", 453, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtppas3", 453, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.PostalAddress.City",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("mtppac1", 535, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.PostalAddress.AddressLine1"))).onSection("AB"),
              // NOTE: Removed section F
              warning("mtppac2", 455, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtppac3", 455, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.PostalAddress.Province",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("mtppap1", 536, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.PostalAddress.AddressLine1"))).onSection("AB"),
              // NOTE: Removed section F
              warning("mtppap2", 457, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtppap3", 336, "Please enter a valid province",
                notEmpty.and(notValidProvince)).onSection("ABG"),
              warning("mtppap4", 457, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.PostalAddress.PostalCode",
            minLen: 2,
            maxLen: 10,
            rules: [
              warning("mtppaz1", 537, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.PostalAddress.AddressLine1"))).onSection("AB"),
              // NOTE: Removed section F
              warning("mtppaz2", 262, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtppaz3", 338, "Please enter a valid postal code",
                notEmpty.and(notValidPostalCode)).onSection("ABG"),
              warning("mtppaz4", 262, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.ContactDetails.ContactSurname",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("mtpcds1", 460, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname").or(hasMoneyField("ThirdParty.Entity.Name")))).onSection("ABG"),
              // NOTE: Removed section F
              warning("mtpcds2", 461, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtpcds3", 461, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.ContactDetails.ContactName",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("mtpcdn1", 462, "Not required",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname").or(hasMoneyField("ThirdParty.Entity.Name")))).onSection("ABG"),
              // NOTE: Removed category F
              warning("mtpcdn2", 463, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtpcdn3", 463, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.ContactDetails.Email",
            minLen: 2,
            maxLen: 120,
            rules: [
              warning("mtpcde1", 464, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname").or(hasMoneyField("ThirdParty.Entity.Name"))).and(notMoneyField("ThirdParty.ContactDetails.Fax")).and(notMoneyField("ThirdParty.ContactDetails.Telephone"))).onSection("ABG"),
              // NOTE: Removed category E and F
              warning("mtpcde2", 465, "Not required",
                notEmpty).onSection("CD"),
              warning("mtpcde3", 465, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              warning("cnte4", "E01", "This is not a valid email address",
                notEmpty.and(notValidEmail))

            ]
          },
          {
            field: "ThirdParty.ContactDetails.Fax",
            minLen: 2,
            maxLen: 15,
            rules: [
              warning("mtpcdf1", 464, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname").or(hasMoneyField("ThirdParty.Entity.Name"))).and(notMoneyField("ThirdParty.ContactDetails.Email")).and(notMoneyField("ThirdParty.ContactDetails.Telephone"))).onSection("ABG"),
              // NOTE: Removed section F
              warning("mtpcdf2", 465, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtpcdf3", 465, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "ThirdParty.ContactDetails.Telephone",
            minLen: 2,
            maxLen: 15,
            rules: [
              warning("mtpcdt1", 464, "Please enter the required information",
                isEmpty.and(hasMoneyField("ThirdParty.Individual.Surname").or(hasMoneyField("ThirdParty.Entity.Name"))).and(notMoneyField("ThirdParty.ContactDetails.Fax")).and(notMoneyField("ThirdParty.ContactDetails.Email"))).onSection("ABG"),
              // NOTE: Removed section F
              warning("mtpcdt2", 465, "Not required",
                notEmpty).onSection("CDE"),
              warning("mtpcdt3", 465, "Not required",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A")
            ]
          },
          {
            field: "CardChargeBack",
            rules: [
              warning("mcrdcb1", 466, "Not required",
                notEmpty).onSection("ABCDFG"),
              warning("mcrdcb2", 477, "Must be set to either Y or N (blank assumed as N)",
                notEmpty.and(notValueIn(['Y', 'N']))).onSection("E"),
              warning("mcrdcb3", 383, "If CardChargeBack is Y, the Flow must be IN or the Non Resident Account Identifier must be VISA NET or MASTER SEND",
                notEmpty.and(hasValue("Y")).and(notNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onOutflow().onSection("E")
            ]
          },
          {
            field: "CardIndicator",
            minLen: 2,
            maxLen: 20,
            rules: [
              warning("mcrdci1", 478, "Not required",
                notEmpty).onSection("ABCDG"),
              warning("mcrdci2", 479, "Must contain AMEX or DINERS or ELECTRON or MAESTRO or MASTER or VISA or BOCEXPRESS",
                notValidCardType).onSection("EF")
            ]
          },
          {
            field: "ElectronicCommerceIndicator",
            minLen: 1,
            maxLen: 2,
            rules: [
              warning("mcrdec1", 538, "Please enter the required information",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onSection("E"),
              warning("mcrdec2", 480, "Not required",
                notEmpty).onSection("ABCDFG"),
              warning("mcrdec3", 539, "The {{Regulator}} mandates that only certain codes are applicable",
                notEmpty.and(notValidECI)).onSection("E")
              //wut the actual F? - warning("", "TBA", "If the Flow is OUT, and the MerchantCode is 7995, 7800, 7801 or 7802 and the CardIndicator is,……., the transaction is foreign gambling")
            ]
          },
          {
            field: "POSEntryMode",
            len: 2,
            rules: [
              warning("mcrdem1", 480, "Not required",
                notEmpty).onSection("ABCDFG"),
              warning("mcrdem2", 481, "Please enter the required information",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onSection("E"),
              warning("mcrdem3", 482, "The {{Regulator}} mandates that only certain codes are applicable",
                notEmpty.and(notValidPOSEntryMode)).onSection("E"),
              warning("mcrdem4", 364, "This transaction is regarded as online foreign lottery and gambling",
                notEmpty.and(isForeignGambling)).onSection("E")
            ]
          },
          {
            field: "CardFraudulentTransactionIndicator",
            rules: [
              warning("mcrdft1", 483, "Not required",
                notEmpty).onSection("ABCDFG"),
              warning("mcrdft2", 484, "Must contain a value Y or N",
                isEmpty.or(notValueIn(["Y", "N"]))).onSection("E")
            ]
          },
          {
            field: "ForeignCardHoldersPurchases{{LocalValue}}",
            rules: [
              warning("mcrdfp1", 486, "Not required",
                notEmpty).onSection("ABCDEG"),
              warning("mcrdfp2", 487, "Please enter the required information",
                isEmpty).onSection("F"),
              warning("mcrdfp3", 352, "This field shouldn't contain a negative value",
                notEmpty.and(isNegative)).onSection("F")
            ]
          },
          {
            field: "ForeignCardHoldersCashWithdrawals{{LocalValue}}",
            rules: [
              warning("mcrdfw1", 488, "Not required",
                notEmpty).onSection("ABCDEG"),
              warning("mcrdfw2", 489, "Please enter the required information",
                isEmpty).onSection("F"),
              warning("mcrdfw3", 352, "This field shouldn't contain a negative value",
                notEmpty.and(isNegative)).onSection("F")
            ]
          },
          {
            field: "ImportExport",
            rules: [
              warning("mtie1", 490, "Please enter the required information",
                emptyImportExport.and(
                  not(hasMoneyFieldValue("CategoryCode", "106").
                    and(
                      hasMoneyField("ThirdParty.CustomsClientNumber").and(evalMoneyField("ThirdParty.CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))
                    )
                  ).and(
                    not(hasMoneyFieldValue("CategoryCode", "106").
                      and(
                        not(hasMoneyField("ThirdParty.CustomsClientNumber")).and(evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))
                      )
                    )
                  )
                )).onInflow().onSection("AB").onCategory(["101", "103", "105", "106"]).notOnCategory(["101/11", "103/11"]),
              warning("mtie2", 491, "Not required",
                notEmptyImportExport).onSection("CD"),
              warning("mtie3", 529, "Total PaymentValue of all ImportExport entries may not exceed a 1% variance with the {{LocalValue}} or ForeignValue",
                notChecksumImportExport).onSection("ABG"),
              warning("mtie4", 490, "If outflow and the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, the ImportExport element must be completed unless a) the Subject is SDA or REMITTANCE DISPENSATION or b) category 106 and import undertaking client",
                emptyImportExport.and(notMoneyFieldValue("AdHocRequirement.Subject", ["SDA", "REMITTANCE DISPENSATION"]).and(
                  not(hasMoneyFieldValue("CategoryCode", "106").
                    and(
                      hasMoneyField("ThirdParty.CustomsClientNumber").and(evalMoneyField("ThirdParty.CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))
                    )
                  ).and(
                    not(hasMoneyFieldValue("CategoryCode", "106").
                      and(
                        not(hasMoneyField("ThirdParty.CustomsClientNumber")).and(evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))
                      )
                    )
                  )
                ))).onOutflow().onSection("A").onCategory(["101", "103", "105", "106"]).notOnCategory(["101/11", "103/11"]),
              warning("mtie5", 491, "Not required",
                notEmptyImportExport).onSection("ABG").notOnCategory(["101", "103", "105", "106"]),
              warning('mtie6', 494, 'Must contain a sequential SubSequence entries that must start with the value 1',
                notValidSubSequenceNumbers)
            ]
          }

        ]
      }
    };

    return money;
  }
})