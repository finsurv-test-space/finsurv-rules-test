define(function () {
  //THIS IS THE TRANSACTION RULES
  return function (predef) {
    var transaction;

    with (predef) {
      transaction = {
        ruleset: "Transaction Rules for Flow form",
        scope: "transaction",
        validations: [
          {
            field: "ReplacementTransaction",
            rules: [
              warning("repyn1", 543, "ReplacementTransaction must be completed",
                isEmpty.and(hasTransactionField("ReplacementOriginalReference"))),
              warning("repyn2", 209, "ReplacementTransaction must contain a value Y or N",
                notEmpty.and(notValueIn(["Y", "N"])))
            ]
          },
          {
            field: "ReplacementOriginalReference",
            rules: [
              warning("repor1", 210, "If ReplacementTransaction is Y, must be completed",
                isEmpty.and(hasTransactionFieldValue("ReplacementTransaction", "Y"))),
              warning("repor2", 211, "Must not be completed if ReplacementTransactionIndicator is N",
                notEmpty.and(hasTransactionFieldValue("ReplacementTransaction", "N"))),
              warning("repor3", 219, "Additional spaces identified in data content",
                notEmpty.and(hasAdditionalSpaces))
            ]
          },
          {
            field: "ReportingQualifier",
            minLen: 6,
            maxLen: 25,
            rules: [
              failure("repq1", 541, "Must be completed", isEmpty),
              failure("repq2", 207, "Must contain a valid value",
                notEmpty.and(notReportingQualifier))
            ]
          },
          {
            field: "Flow",
            rules: [
              failure("flow1", 542, "Must be completed", isEmpty),
              failure("flow2", 208, "Must contain the value IN or OUT",
                notEmpty.and(notValueIn(["IN", "OUT"]))).onSection("ABCDEG"),
              failure("flow3", 208, "Must only contain the value IN",
                notEmpty.and(notValue("IN"))).onSection("F"),
              failure("flow4", "F01", "The flow direction of the report data must match the flow on the account entry. Please ask IT support to investigate",
                notEmpty.and(notAccountFLow)).onSection("ABDEFG"),
              failure("flow5", "F01", "The flow direction of the report data must match the flow on the account entry unless this is a VOSTRO related transaction. Please ask IT support to investigate",
                notEmpty.and(notAccountFLow.and(notResException("VOSTRO NON REPORTABLE").and(notNonResException("VOSTRO NON REPORTABLE"))))).onSection("C")
            ]
          },
          {
            field: "ValueDate",
            rules: [
              warning("vd1", 544, "Must be completed", isEmpty),
              warning("vd2", 216, "May not exceed today's date plus 10 days", notEmpty.and(isDaysInFuture(10))),
              warning("vd3", 217, "Old transaction if the ValueDate is today's date less 3 days", notEmpty.and(isDaysInPast(4))),
              warning("vd4", 218, "ValueDate must be equal to or after the 2013-08-19",
                notEmpty.and(isBeforeGoLive)),
              warning("vd5", 214, "Date format incorrect (Date format is CCYY-MM-DD)",
                notEmpty.and(notDatePattern))
            ]
          },
          {
            field: "FlowCurrency",
            rules: [
              warning("fcurr1", "S01", "Must be completed", isEmpty),
              warning("fcurr2", 360, "Invalid SWIFT currency code", notEmpty.and(hasInvalidSWIFTCurrency))
            ]
          },
          {
            field: "TotalForeignValue",
            rules: [
              warning("tfv1", "244", "If the FlowCurrency is {{LocalCurrency}} then the sum of the {{LocalCurrencyName}} Monetary Amounts must add up to the TotalForeignValue",
                notEmpty.and(isCurrencyIn(map("LocalCurrency")).and(notSumLocalValue))).onSection("ABCDEG"),
              warning("tfv2", "244", "If the FlowCurrency is not {{LocalCurrency}} then the sum of the Foreign Monetary Amounts must add up to the TotalForeignValue",
                notEmpty.and(notCurrencyIn(map("LocalCurrency")).and(notSumForeignValue))).onSection("ABCDEG"),
              warning("tfv3", "551", "ForeignCardHoldersPurchases{{LocalValue}} + ForeignCardHoldersCashWithdrawals{{LocalValue}} must equal TotalForeignValue",
                notEmpty.and(notSumCardValue)).onSection("F"),
              warning("tfv4", "245", "The TotalForeignValue must be completed and must be greater than 0.00",
                isEmpty.or(not(isGreaterThan(0.00)))).onSection("ABCDEG")
            ]
          },
          {
            field: "TrnReference",
            minLen: 1,
            maxLen: 30,
            rules: [
              warning("tref1", 545, "Must be completed", isEmpty),
              warning("tref2", 219, "Additional spaces identified in data content",
                notEmpty.and(hasAdditionalSpaces))
            ]
          },
          {
            field: "OriginatingBank",
            minLen: 2,
            maxLen: 50,
            rules: [
              warning("obank1", 228, "If Flow is OUT, must be completed",
                isEmpty).onOutflow().onSection("ABCDG"),
              warning("obank2", 229, "May not be completed",
                notEmpty).onSection("EF"),
              warning("obank3", 230, "If Flow is IN, must be completed except if the Non-Resident AccountIdentifier is CASH",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", "CASH"))).onInflow().onSection("CDG"),
              warning("obank4", 229, "If category 250 or 251, is used and the Flow is IN and the Resident ExceptionName is MUTUAL PARTY, the OriginatingBank may not be completed",
                notEmpty.and(hasResException("MUTUAL PARTY"))).onInflow().onSection("AB").onCategory(["250", "251"]),
              warning("obank5", 229, "If category 252, 255 or 256 is used and the Flow is IN and the NonResident ExceptionName is MUTUAL PARTY, the OriginatingBank may not be completed",
                notEmpty.and(hasNonResException("MUTUAL PARTY"))).onInflow().onSection("AB").onCategory(["252", "255", "256"]),
              warning("obank6", 230, "If OriginatingCountry is completed, must be completed",
                isEmpty.and(hasTransactionField("OriginatingCountry"))).onSection("ABCDG"),
              warning("obank7", 230, "If Flow is IN, must be completed except if the Non-Resident AccountIdentifier is CASH",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", "CASH"))).onInflow().onSection("AB").notOnCategory(["250", "251", "252", "255", "256"]),
              warning("obank8", 230, "If Flow is IN, must be completed except if the Non-Resident AccountIdentifier is CASH or Resident ExceptionName is MUTUAL PARTY",
                isEmpty.and(notResException("MUTUAL PARTY").and(notNonResidentFieldValue("AccountIdentifier", "CASH")))).onInflow().onSection("AB").onCategory(["250", "251"]),
              warning("obank9", 230, "If Flow is IN, must be completed except if the Non-Resident AccountIdentifier is CASH or NonResident ExceptionName is MUTUAL PARTY",
                isEmpty.and(notNonResException("MUTUAL PARTY").and(notNonResidentFieldValue("AccountIdentifier", "CASH")))).onInflow().onSection("AB").onCategory(["252", "255", "256"])
            ]
          },
          {
            field: "OriginatingCountry",
            rules: [
              warning("ocntry1", 231, "If OriginatingBank is completed, must be completed",
                isEmpty.and(hasTransactionField("OriginatingBank"))).onSection("ABCDG"),
              warning("ocntry2", 229, "May not be completed",
                notEmpty).onSection("EF"),
              warning("ocntry3", 233, "If Flow is OUT, SWIFT country code must be {{Locale}}",
                notEmpty.and(notValue(map("Locale")))).onOutflow().onSection("ABG"),
              warning("ocntry4", 234, "If Flow is IN, SWIFT country code may not be {{Locale}}",
                notEmpty.and(hasValue(map("Locale")))).onInflow().onSection("ABG"),
              warning("ocntry5", 238, "Invalid SWIFT country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("ABCDG"),
              warning("ocntry6", 229, "If category 250 or 251, is used and the Flow is IN and the Resident ExceptionName is MUTUAL PARTY, the OriginatingCountry may not be completed",
                notEmpty.and(hasResException("MUTUAL PARTY"))).onInflow().onSection("AB").onCategory(["250", "251"]),
              warning("ocntry7", 229, "If category 252, 255 or 256 is used and the Flow is IN and the NonResident ExceptionName is MUTUAL PARTY, the OriginatingCountry may not be completed",
                notEmpty.and(hasNonResException("MUTUAL PARTY"))).onInflow().onSection("AB").onCategory(["252", "255", "256"])
            ]
          },
          {
            field: "CorrespondentBank",
            minLen: 2,
            maxLen: 50,
            rules: [
              warning("cbank1", 235, "If CorrespondentCountry is completed, must be completed",
                isEmpty.and(hasTransactionField("CorrespondentCountry"))).onSection("ABCDG"),
              warning("cbank2", 236, "May not be completed",
                notEmpty).onSection("EF")
            ]
          },
          {
            field: "CorrespondentCountry",
            rules: [
              warning("ccntry1", 237, "If CorrespondentBank is completed, must be completed",
                isEmpty.and(hasTransactionField("CorrespondentBank"))).onSection("ABCDG"),
              warning("ccntry2", 236, "May not be completed",
                notEmpty).onSection("EF"),
              warning("ccntry3", 233, "Invalid SWIFT country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("ABCDG")
            ]
          },
          {
            field: "ReceivingBank",
            minLen: 2,
            maxLen: 50,
            rules: [
              warning("rbank1", 239, "If Flow is IN, must be completed",
                isEmpty).onInflow().onSection("ABCDG"),
              warning("rbank2", 240, "Must not be completed",
                notEmpty).onSection("EF"),
              warning("rbank3", 241, "If ReceivingCountry is completed, must be completed",
                isEmpty.and(hasTransactionField("ReceivingCountry"))).onSection("ABCDG"),
              warning("rbank4", 241, "If Flow is OUT, must be completed except if the Non-Resident AccountIdentifier is CASH, VISA NET or MASTER SEND",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["CASH", "VISA NET", "MASTER SEND"]))).onOutflow().onSection("CDG"),
              warning("rbank5", 240, "If category 200, 250 or 251 is used and the Flow is OUT and the Resident ExceptionName is MUTUAL PARTY and the Non Resident AccountIdentifier is CASH, the ReceivingBank may not be completed",
                notEmpty.and(hasResException("MUTUAL PARTY")).and(hasNonResidentFieldValue("AccountIdentifier", "CASH"))).onOutflow().onSection("AB").onCategory(["200", "250", "251"]),
              warning("rbank6", 240, "If category 255, 256 or 530/05 is used and the Flow is OUT and the NonResident ExceptionName is MUTUAL PARTY, the ReceivingBank may not be completed",
                notEmpty.and(hasNonResException("MUTUAL PARTY"))).onOutflow().onSection("AB").onCategory(["200", "255", "256", "530/05"]),
              warning("rbank7", 241, "If Flow is OUT, must be completed except if the Non-Resident AccountIdentifier is CASH, VISA NET or MASTER SEND",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["CASH", "VISA NET", "MASTER SEND"]))).onOutflow().onSection("AB").notOnCategory(["200", "250", "251", "255", "256", "530/05"]),
              warning("rbank8", 241, "If Flow is OUT, must be completed except if the Non-Resident AccountIdentifier is CASH, VISA NET or MASTER SEND",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["CASH", "VISA NET", "MASTER SEND"])).and(notResException("MUTUAL PARTY"))).onOutflow().onSection("AB").onCategory(["200", "250", "251"]),
              warning("rbank9", 241, "If Flow is OUT, must be completed except if the Non-Resident AccountIdentifier is CASH, VISA NET or MASTER SEND",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["CASH", "VISA NET", "MASTER SEND"])).and(notNonResException("MUTUAL PARTY"))).onOutflow().onSection("AB").onCategory(["255", "256", "530/05"])
            ]
          },
          {
            field: "ReceivingCountry",
            rules: [
              warning("rcntry1", 242, "If ReceivingBank is completed must be completed",
                isEmpty.and(hasTransactionField("ReceivingBank"))).onSection("ABCDG"),
              warning("rcntry2", 240, "Must not be completed",
                notEmpty).onSection("EF"),
              warning("rcntry3", 238, "Invalid SWIFT country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("ABCDG"),
              warning("rcntry4", 243, "If Flow is IN, SWIFT country code must be {{Locale}}",
                notValue(map("Locale"))).onInflow().onSection("ABG"),
              warning("rcntry5", 331, "If Flow is OUT, SWIFT country code may not be {{Locale}}",
                notEmpty.and(hasValue(map("Locale")))).onOutflow().onSection("ABG"),
              warning("rcntry6", 240, "If category 200, 250 or 251 is used and the Flow is OUT and the Resident ExceptionName is MUTUAL PARTY, the ReceivingCountry may not be completed",
                notEmpty.and(hasResException("MUTUAL PARTY"))).onOutflow().onSection("AB").onCategory(["200", "250", "251"]),
              warning("rcntry7", 240, "If category 255, 256 or 530/05 is used and the Flow is OUT and the NonResident ExceptionName is MUTUAL PARTY, the ReceivingCountry may not be completed",
                notEmpty.and(hasNonResException("MUTUAL PARTY"))).onOutflow().onSection("AB").onCategory(["255", "256", "530/05"])
            ]
          },
          {
            field: "NonResident",
            rules: [
              warning("nr1", 246, "Must contain one of Individual, Entity, or Exception elements",
                isMissingField(["Individual", "Entity", "Exception"])).onSection("ACDG"),
              warning("nr2", 247, "Must contain only one of Individual or Entity elements",
                isMissingField(["Individual", "Entity"])).onSection("BE"),
              warning("nr3", 248, "If the Reporting Entity Code is 304 or 305 only Non Resident Entity element may be completed",
                isMissingField("Entity")).onSection("G").onCategory(["304", "305"]),
              warning("nr4", 248, "Non Resident Entity element must be completed unless the Non Resident Account Identifier is VISA NET or MASTER SEND",
                isMissingField("Entity").and(notNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onSection("E"),
              warning("nr5", 250, "NonResident and AdditionalNonResidentData elements must not be completed",
                notEmpty).onSection("F"),
              warning("nr6", 253, "If category 250 or 251 is used, Non Resident Entity, or NonResident Exception element may not be completed",
                isMissingField("Individual")).onSection("A").onCategory(["250", "251"])
            ]
          },
          {
            field: "NonResident.Individual.Surname",
            minLen: 1,
            maxLen: 35,
            rules: [
              warning("nrsn1", 254, "If NonResident Individual is completed, must be completed",
                isEmpty).onSection("ABCDEG"),
              // NOTE: Added excluding STRATE in message
              warning("nrsn2", 255, "The words specified under Non-resident or Resident ExceptionName, excluding STRATE, must not be used.",
                notEmpty.and(isExceptionName)).onSection("ABCDG"),
              warning("nrsn3", 254, "If the Flow is OUT and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then the Surname must have a value",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A"),
              warning("nrsn4", 254, "If the Flow is IN and the category is 400 and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then the Surname must have a value",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onCategory('400').onSection("A")
            ]
          },
          {
            field: "NonResident.Individual.Name",
            minLen: 1,
            maxLen: 50,
            rules: [
              warning("nrnm1", 256, "If Flow is OUT, and NonResident Individual is completed, must be completed",
                isEmpty).onOutflow().onSection("ABCDG"),
              warning("nrnm2", 256, "If the Flow is OUT and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then the Name must have a value",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A"),
              warning("nrnm3", 254, "If the Flow is IN and the category is 400 and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then the Name must have a value",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onCategory('400').onSection("A"),
              warning("nrnm4", 255, "The words specified under Non-resident or Resident ExceptionName, excluding STRATE, must not be used.",
                  notEmpty.and(isExceptionName)).onSection("ABCDG"),
            ]
          },
          {
            field: "NonResident.Individual.Gender",
            rules: [
              warning("nrgn1", 295, "Invalid gender value",
                notEmpty.and(notValueIn(["M", "F"]))).onOutflow().onSection("ABCDG")
            ]
          },
          {
            field: "NonResident.Individual.PassportNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              warning("nrpn1", 258, "If category 250 or 251 is completed, must be completed",
                isEmpty).onSection("AB").onCategory(["250", "251"]),
              warning("nrpn2", 219, "Additional spaces identified in data content",
                notEmpty.and(hasAdditionalSpaces)).onSection("AB")
            ]
          },
          {
            field: "NonResident.Individual.PassportCountry",
            rules: [
              warning("nrpc1", 259, "If category 250 or 251 is completed, must be completed",
                isEmpty).onSection("AB").onCategory(["250", "251"]),
              warning("nrpc2", 238, "Invalid CountryCode",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("AB")
            ]
          },
          {
            field: "NonResident.Entity.EntityName",
            minLen: 2,
            maxLen: 70,
            rules: [
              warning("nrlen1", 260, "If NonResident Entity is completed, must be completed",
                isEmpty).onSection('ABCDG'),
              warning("nrlen2", 559, "Must not be completed (other than for VISA NET or MASTER SEND)",
                notEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onSection("E"),
              // NOTE: Added excluding STRATE to message
              warning("nrlen3", 255, "The words specified under Non-resident or Resident ExceptionName, excluding STRATE, must not be used.",
                notEmpty.and(isExceptionName)).onSection("ABCDG"),
              warning("nrlen4", 261, "If the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then an Entity must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              warning("nrlen5", 559, "This field or CardMerchantName must be completed (Only for VISA NET or MASTER SEND)",
                isEmpty.and(notTransactionField("NonResident.Entity.CardMerchantName")).and(hasNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onSection("E")
            ]
          },
          {
            field: "NonResident.Entity.CardMerchantName",
            minLen: 2,
            maxLen: 70,
            rules: [
              warning("nrcmn1", 559, "Must be completed (other than for VISA NET or MASTER SEND)",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onSection("E"),
              warning("nrcmn2", 263, "May not be completed", notEmpty).onSection("ABCDG"),
              warning("nrcmn3", 219, "Additional spaces identified in data content",
                notEmpty.and(hasAdditionalSpaces)).onSection("E"),
              warning("nrcmn4", 559, "This field or EntityName must be completed",
                isEmpty.and(notTransactionField("NonResident.Entity.EntityName")).and(hasNonResidentFieldValue("AccountIdentifier", ["VISA NET", "MASTER SEND"]))).onSection("E")
            ]
          },
          {
            // TODO: 266 Invalid CardMerchantCode not present
            field: "NonResident.Entity.CardMerchantCode",
            minLen: 4,
            maxLen: 6,
            rules: [
              warning("nrcmc1", 264, "Must be completed",
                isEmpty).onSection("E"),
              warning("nrcmc2", 265, "Must not be completed", notEmpty).onSection("ABCDG")]
          },
          {
            field: "NonResident.Exception",
            rules: [
              warning("nrex1", 268, "If it has a value, ResidentCustomerAccountHolder Exception must not be completed",
                notEmpty.and(hasTransactionField("Resident.Exception"))).onSection("A"),
              warning("nrex2", 269, "Must not be completed",
                notEmpty).onSection("BEFG")
            ]
          },
          {
            field: "NonResident.Exception.ExceptionName",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("nrexn1", 267, "May only contain one of the specified values",
                notNonResExceptionName).onSection("ACD"),
              warning("nrexn2", 270, "If the ExceptionName is MUTUAL PARTY, the BoPCategory must only be 200, 252, 255, 256 or 530/05",
                hasValue("MUTUAL PARTY")).onSection("A").notOnCategory(["200", "252", "255", "256", "530/05"]),
              warning("nrexn3", 270, "If BoPCategory is 252 and the Flow is IN, must only contain the value MUTUAL PARTY under NonResident ExceptionName",
                notEmpty.and(notValue("MUTUAL PARTY"))).onInflow().onSection("A").onCategory("252"),
              warning("nrexn4", 270, "Must only be completed if BoPCategory is 300 and the original transaction was reported with BoPCategory and SubBoPCategory 309/08 with a Non Resident ExceptionName BULK INTEREST",
                notEmpty.and(hasValue("BULK INTEREST"))).onSection("A").notOnCategory(["300", "309/08"]),
              warning("nrexn5", 270, "ExceptionName of BULK VAT REFUNDS may only be used for category 400 or 411/02",
                notEmpty.and(hasValue("BULK VAT REFUNDS"))).onSection("A").notOnCategory(["400", "411/02"]),
              warning("nrexn6", 270, "ExceptionName of BULK BANK CHARGES may only be used for category 200 or 275",
                notEmpty.and(hasValue("BULK BANK CHARGES"))).onSection("A").notOnCategory(["200", "275"]),
              warning("nrexn7", 270, "ExceptionName of BULK PENSIONS may only be used for category 400 or 407",
                notEmpty.and(hasValue("BULK PENSIONS"))).onSection("A").notOnCategory(["400", "407"]),
              //warning("nrexn8", 437, "If value is IHQ, the Subject under MonetaryDetails must be IHQnnn",
              //        notEmpty.and(hasValue("IHQ")).and(notTransactionFieldValue("MonetaryAmount.AdHocRequirement.Subject", "IHQnnn"))).onSection("A"),
              warning("nrexn9", 270, "ExceptionName of STRATE may only be used for category 601/01 or 603/01",
                notEmpty.and(hasValue("STRATE"))).onSection("A").notOnCategory(["601/01", "603/01"]),
              warning("nrexn10", 269, "May not be used. MUTUAL PARTY is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("MUTUAL PARTY"))).onSection("BCDEFG"),
              warning("nrexn11", 269, "May not be used. BULK INTEREST is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK INTEREST"))).onSection("BCDEFG"),
              warning("nrexn12", 269, "May not be used. BULK VAT REFUNDS is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK VAT REFUNDS"))).onSection("BCDEFG"),
              warning("nrexn13", 269, "May not be used. BULK BANK CHARGES is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK BANK CHARGES"))).onSection("BCDEFG"),
              warning("nrexn14", 269, "May not be used. BULK PENSIONS is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK PENSIONS"))).onSection("BCDEFG"),
              warning("nrexn15", 269, "May not be used. STRATE is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("STRATE"))).onSection("BCDEFG"),
              warning("nrexn16", 269, "May not be used. FCA RESIDENT NON REPORTABLE is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("FCA RESIDENT NON REPORTABLE"))).onSection("ABDEFG"),
              warning("nrexn17", 269, "May not be used. CFC RESIDENT NON REPORTABLE is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("CFC RESIDENT NON REPORTABLE"))).onSection("ABDEFG"),
              warning("nrexn18", 269, "May not be used. VOSTRO NON REPORTABLE is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("VOSTRO NON REPORTABLE"))).onSection("ABDEFG"),
              warning("nrexn19", 269, "May not be used. VOSTRO INTERBANK is only applicable for INTERBANK and NON REPORTABLE transactions",
                notEmpty.and(hasValue("VOSTRO INTERBANK"))).onSection("ABEFG"),
              warning("nrexn20", 269, "May not be used. NOSTRO INTERBANK is only applicable for INTERBANK and NON REPORTABLE transactions",
                notEmpty.and(hasValue("NOSTRO INTERBANK"))).onSection("ABEFG"),
              warning("nrexn21", 269, "May not be used. NOSTRO NON REPORTABLE is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("NOSTRO NON REPORTABLE"))).onSection("ABDEFG"),
              warning("nrexn22", 269, "May not be used. RTGS NON REPORTABLE is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("RTGS NON REPORTABLE"))).onSection("ABDEFG"),
              warning("nrexn23", 269, "If the Subject under the MonetaryDetails is REMITTANCE DISPENSATION and category 256 then MUTUAL PARTY may not be used",
                hasValue("MUTUAL PARTY").and(hasAnyMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A").onCategory("256"),
              warning("nrexn24", 437, "If value is IHQ, the Subject under MonetaryDetails must be IHQnnn",
                notEmpty.and(hasValue("IHQ").and(evalMoneyField("AdHocRequirement.Subject", notPattern(/^IHQ\d{3}$/))))).onSection("A"),
              warning("nrexn25", 269, "May not be used. 'IHQ' is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("IHQ"))).onSection("BCDEFG"),
              warning("nrexn26", 270, "If the MoneyTransferAgentIndicator is TRAVEL CARD or TRAVELLERS CHEQUE, the category can only be 252, 255, 256 or 530/05",
                hasValue("MUTUAL PARTY").and(hasAnyMoneyFieldValue("MoneyTransferAgentIndicator", ["TRAVEL CARD", "TRAVELLERS CHEQUE"]))).onSection("A").notOnCategory(["252", "255", "256", "530/05"]),
              warning("nrexn27", 446, "If value is IHQ, the RegistrationNumber under EntityCustomer of the particular IHQnnn must not be equal to the registered IHQ registration number",
                notEmpty.and(hasValue("IHQ")).and(evalTransactionField("Resident.Entity.RegistrationNumber", isInLookup("ihqCompanies", "registrationNumber")))).onSection("A"),
              warning("nrexn28", 447, "If value is IHQ, the Resident LegalEntityName of the particular IHQnnn must not be equal to the registered IHQ name",
                  notEmpty.and(hasValue("IHQ")).and(evalTransactionField("Resident.Entity.EntityName",isInLookup("ihqCompanies", "companyName")))).onSection("A")
  
            ]
          },
          {
            field: "NonResident.Exception.AccountIdentifier",
            rules: [
              deprecated("nrexai", "S03", "This fields is not used for finsurv submissions",
                notEmpty)
            ]
          },
          {
            field: "NonResident.Exception.AccountNumber",
            rules: [
              deprecated("nrexan", "S04", "This fields is not used for finsurv submissions",
                notEmpty)
            ]
          },
          {
            field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("nriaid1", 272, "If the Flow is OUT must contain a value of NON RESIDENT OTHER, NON RESIDENT RAND, NON RESIDENT FCA, CASH, FCA RESIDENT, RES FOREIGN BANK ACCOUNT, VOSTRO, VISA NET or MASTER SEND",
                notValueIn(["NON RESIDENT OTHER", "NON RESIDENT RAND", "NON RESIDENT FCA", "CASH", "FCA RESIDENT", "RES FOREIGN BANK ACCOUNT", "VOSTRO", "VISA NET", "MASTER SEND"])).onOutflow().onSection("ACDG"),
              warning("nriaid2", 273, "Must contain a value of NON RESIDENT RAND, VISA NET or MASTER SEND", notValueIn(["NON RESIDENT RAND", "VISA NET", "MASTER SEND"])).onSection("B"),
              warning("nriaid3", 272, "Must contain a value VISA NET or MASTER SEND", notEmpty.and(notValueIn(["VISA NET", "MASTER SEND", "CARD DIRECT"]))).onSection("E"),
              warning("nriaid4", 252, "If the AccountIdentifier is NON RESIDENT OTHER or NON RESIDENT RAND or NON RESIDENT FCA or FCA RESIDENT or RES FOREIGN BANK ACCOUNT, NonResident Exception may not be completed",
                hasValueIn(["NON RESIDENT OTHER", "NON RESIDENT RAND", "NON RESIDENT FCA", "FCA RESIDENT", "RES FOREIGN BANK ACCOUNT"]).and(hasTransactionField("NonResident.Exception"))).onSection("ABEG"),

              warning("nriaid5", 274, "If the AccountIdentifier is RES FOREIGN BANK ACCOUNT and the Flow is IN and the category is 255 or 256 or 810 or 416, the Non Resident Individual element must be completed.",
                hasValue("RES FOREIGN BANK ACCOUNT").and(notTransactionField("NonResident.Individual"))).onInflow().onCategory(["255", "256", "810", "416"]).onSection("A"),

              warning("nriaid6", 274, "If the AccountIdentifier is RES FOREIGN BANK ACCOUNT and the Flow is OUT and the category is 255 or 256 or 810, the non resident Individual element must be completed.",
                hasValue("RES FOREIGN BANK ACCOUNT").and(notTransactionField("NonResident.Individual"))).onOutflow().onCategory(["255", "256", "810"]).onSection("A"),
              ignore("nriaid7", 272, "If the Flow is OUT must contain a value of NON RESIDENT OTHER, NON RESIDENT RAND, NON RESIDENT FCA, CASH, FCA RESIDENT, RES FOREIGN BANK ACCOUNT, VOSTRO, VISA NET or MASTER SEND", // duplicate of nriaid1
                notValueIn(["NON RESIDENT OTHER", "NON RESIDENT RAND", "NON RESIDENT FCA", "CASH", "FCA RESIDENT", "RES FOREIGN BANK ACCOUNT", "VOSTRO", "VISA NET", "MASTER SEND"])).onOutflow().onSection("ACDG"),
              warning("nriaid9", 276, "If AccountIdentifier is FCA RESIDENT and the Flow is OUT the category 513 must be completed",
                hasValue("FCA RESIDENT")).onOutflow().onSection("A").notOnCategory("513"),
              warning("nriaid10", 276, "If AccountIdentifier is FCA RESIDENT and the Flow is IN the category 517 must be completed",
                hasValue("FCA RESIDENT")).onInflow().onSection("A").notOnCategory("517"),
              ignore("nriaid11"), // 219 deleted in 21040123
              warning("nriaid12", 272, "The value CARD DIRECT is being deprecated (VISA NET and MASTER SEND to be implemented by 2016-05-15)", hasValue("CARD DIRECT")).onSection("E"),
              warning("nriaid13", 252, "May not be completed",
                notEmpty).onSection("F"),
              warning("nriaid14", 274, "If the AccountIdentifier is FCA RESIDENT and the Flow is OUT and the category is 513, Non Resident Individual must be completed",
                hasValue("FCA RESIDENT").and(hasTransactionField("NonResident.Entity"))).onOutflow().onSection("A").onCategory("513")
            ]
          },
          {
            field: ["NonResident.Individual.AccountNumber", "NonResident.Entity.AccountNumber"],
            minLen: 2,
            maxLen: 40,
            rules: [
              warning("nrian1", 279, "If the Flow is OUT must be completed if the AccountIdentifier is not CASH or NON RESIDENT RAND",
                isEmpty.and(notNonResidentFieldValue("AccountIdentifier", ["CASH", "NON RESIDENT RAND"]))).onOutflow().onSection("ABCDG"),
              warning("nrian2", 280, "Must not be equal to AccountNumber under Resident element",
                notEmpty.and(matchesResidentField("AccountNumber"))).onSection("ABCDG"),
              warning("nrian3", 533, "May not contain invalid characters like ' or &",
                notEmpty.and(hasPattern(/['&]/))).onSection("ABCDG"),
              warning("nrian4", 252, "Must not be completed",
                notEmpty).onSection("F")
            ]
          },
          {
            field: ["NonResident.Individual.Address.AddressLine1", "NonResident.Entity.Address.AddressLine1"],
            minLen: 2,
            maxLen: 50,
            rules: [
              // NOTE: Removed section F
              warning("nrial11", 281, "Must not be completed", notEmpty).onSection("E")
            ]
          },
          {
            field: ["NonResident.Individual.Address.AddressLine2", "NonResident.Entity.Address.AddressLine2"],
            minLen: 2,
            maxLen: 50,
            rules: [
              // NOTE: Removed section F
              warning("nrial21", 281, "Must not be completed", notEmpty).onSection("E")
            ]
          },
          {
            field: ["NonResident.Individual.Address.Suburb", "NonResident.Entity.Address.Suburb"],
            minLen: 2,
            maxLen: 50,
            rules: [
              // NOTE: Removed section F
              warning("nrial31", 281, "Must not be completed", notEmpty).onSection("E")
            ]
          },
          {
            field: ["NonResident.Individual.Address.City", "NonResident.Entity.Address.City"],
            minLen: 2,
            maxLen: 35,
            rules: [
              ignore("nric1").onSection("ABCDG"),
              // NOTE: Removed section F
              warning("nric2", 284, "Must not be completed", notEmpty).onSection("E")
            ]
          },
          {
            field: ["NonResident.Individual.Address.State", "NonResident.Entity.Address.State"],
            minLen: 2,
            maxLen: 35,
            rules: [
              ignore("nris1").onSection("ABCDG"),
              warning("nris2", 286, "Must not be completed",
                notEmpty).onSection("E")
            ]
          },
          {
            field: ["NonResident.Individual.Address.PostalCode", "NonResident.Entity.Address.PostalCode"],
            minLen: 2,
            maxLen: 10,
            rules: [
              ignore("nriz1").onSection("ABCDG"),
              warning("nriz2", 288, "Must not be completed",
                notEmpty).onSection("E"),
              warning("nriz3", "L04", "Length is too long",
                notEmpty.and(isTooLong(10))).onSection("ABCDG")
            ]
          },
          {
            field: ["NonResident.Individual.Address.Country", "NonResident.Entity.Address.Country"],
            len: 2,
            rules: [
              warning("nrictry1", 289, "Must be completed",
                isEmpty).onSection("ABCDEG"),
              warning("nrictry2", 238, "Invalid SWIFT country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("ABCDEG"),
              warning("nrictry3", 290, "SWIFT country code may not be {{Locale}}",
                notEmpty.and(hasValue(map("Locale")))).onSection("ABCDG"),
              warning("nrictry4", 238, "SWIFT country code must not be {{Locale}} except if the ForeignIDCountry under IndividualCustomer is NA, LS or SZ",
                notEmpty.and(hasValue(map("Locale")).and(notTransactionFieldValue("Resident.Individual.ForeignIDCountry", ["NA", "LS", "SZ"])))).onSection("E"),
              warning("nrictry5", 238, "EU is not a valid country and may not be used",
                notEmpty.and(hasValue("EU"))).onOutflow().onSection("A").notOnCategory("513"),
              warning("nrictry6", 238, "EU is not a valid country and may not be used",
                notEmpty.and(hasValue("EU"))).onInflow().onSection("A").notOnCategory("517"),
              warning("nrictry7", 238, "For Outflow category 513 the country must be linked to the currency (EU must be used for EUR payments)",
                notEmpty.and(notMatchToCurrency)).onOutflow().onSection("A").onCategory("513"),
              warning("nrictry8", 238, "For Inflow category 517 the country must be linked to the currency (EU must be used for EUR payments)",
                notEmpty.and(notMatchToCurrency)).onInflow().onSection("A").onCategory("517"),
              warning("nrictry9", 252, "Must not be completed",
                notEmpty).onSection("F")
            ]
          },


          //Resident
          {
            field: "Resident",
            rules: [
              warning("rg1", 277, "Must contain one of IndividualCustomer or EntityCustomer or Exception elements",
                notTransactionField("Resident.Individual").and(notTransactionField("Resident.Entity").and(notTransactionField("Resident.Exception")))).onSection("ABCEG"),
              warning("rg2", 291, "If the Flow is OUT and category 255 is used, Resident EntityCustomer element must be completed",
                notTransactionField("Resident.Entity")).onOutflow().onCategory("255").onSection("AB"),
              warning("rg5", 548, "Resident CustomerAccountHolder and AdditionalCustomer Data elements must not be completed",
                notEmpty).onSection("F"),
              ignore("rg6")
            ]
          },
          {
            field: "Resident.Individual.Surname",
            minLen: 1,
            maxLen: 35,
            rules: [
              warning("risn1", 254, "If Resident Individual is completed, Surname must be completed", isEmpty).onSection("ABCEG"),
              // NOTE: Removed section G and changed text
              warning("risn2", 255, "The words specified under NonResident or Resident ExceptionName, excluding STRATE, must not be used.",
                isExceptionName).onSection("ABCE"),
              warning("risn3", 254, "If the Flow is OUT and the Subject under the MonetaryDetails element is REMITTANCE DISPENSATION, surname must have a value.",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A"),
              warning('risn4', 254, 'If the Flow is IN and the category is 400 and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION, surname must be completed.',
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onCategory('400').onSection("A"),
              ]
          },
          {
            field: "Resident.Individual.Name",
            minLen: 1,
            maxLen: 50,
            rules: [
              warning("rin1", 256, "If Resident Individual is completed, must be completed", 
                isEmpty.and(hasTransactionField("Resident.Individual"))).onSection("ABCEG"),
              warning("rin2", 256, "If the Flow is OUT and the Subject under the MonetaryDetails element is REMITTANCE DISPENSATION, name must have a value.",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A"),
              warning('rin3', 256, 'If the Flow is IN and the category is 400 and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION, name must be completed.',
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onCategory('400').onSection("A"),
              warning("rin4", 255, "The words specified under NonResident or Resident ExceptionName, excluding STRATE, must not be used.",
                isExceptionName).onSection("ABCE"),
            ]
          },
          {
            field: "Resident.Individual.Gender",
            len: 1,
            rules: [
              warning("rig1", 257, "Must be completed", isEmpty).onSection("ABEG"),
              warning("rig2", 295, "Invalid Gender value", notEmpty.and(notValueIn(["M", "F"]))).onSection("ABEG"),
              warning("rig3", "S11", "The gender should match the ID Number",
                notEmpty.and(evalTransactionField("Resident.Individual.IDNumber",notEmpty.and(isValidRSAID)).and(notMatchesGenderToIDNumber("Resident.Individual.IDNumber")))).onSection("ABEG")
            ]
          },
          {
            field: "Resident.Individual.DateOfBirth",
            rules: [
              warning("ridob1", 296, "Must be completed",
                isEmpty).onSection("ABEG"),
              warning("ridob2", 215, "Date format incorrect (Date format is CCYY-MM-DD)",
                notEmpty.and(notPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/).or(isDaysInFuture(0)))).onSection("ABEG"),
              warning("ridob3", "S11", "The date of birth should match the ID Number",
                notEmpty.and(hasTransactionField("Resident.Individual.IDNumber").and(notMatchResidentDateToIDNumber))).onSection("ABEG")
            ]
          },
          {
            field: "Resident.Individual.IDNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              warning("riidn1", 298, "If BoPCategory and SubBoPCategory is 511/01 to 511/08 or 512/01 to 512/07 or 513 is used, must be completed",
                isEmpty).onSection("AB").onCategory(["511", "512", "513"]),
              warning("riidn2", 298, "If the Flow is OUT and category 401 is used, must be completed",
                isEmpty).onOutflow().onSection("AB").onCategory("401"),
              warning("riidn3", 297, "Invalid ID number if completed. (Note, if the ID number does not comply to the algorithm, the Subject must be INVALIDIDNUMBER to accept an invalid ID number)",
                notEmpty.and(notValidRSAID).and(notMoneyFieldValue("AdHocRequirement.Subject", "INVALIDIDNUMBER"))).onSection("ABEG"),
              warning("riidn4", 294, "If IndividualCustomer is selected, at least one of IDNumber or TempResPermitNumber or ForeignIDNumber must be completed",
                isEmpty.and(notTransactionField("Resident.Individual.TempResPermitNumber").and(notTransactionField("Resident.Individual.ForeignIDNumber")))).onSection("ABE"),
              warning("riidn5", 563, "If the Subject is SDA, IDNumber must be completed",
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).onSection("AB")
            ]
          },
          {
            field: "Resident.Individual.TempResPermitNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              warning("ritrpn1", 299, "If CategoryCode 511, 512 or 513 is used, TempResPermitNumber may not be completed",
                notEmpty).onSection("AB").onCategory(["511", "512", "513"]),
              warning("ritrpn2", 299, "If the Flow is OUT and category 401 is used, may not be completed",
                notEmpty).onOutflow().onSection("AB").onCategory("401"),
              warning("ritrpn3", 294, "If IndividualCustomer is selected, at least one of IDNumber or TempResPermitNumber or ForeignIDNumber must be completed",
                isEmpty.and(notTransactionField("Resident.Individual.IDNumber").and(notTransactionField("Resident.Individual.ForeignIDNumber")))).onSection("ABE"),
              warning("ritrpn4", 562, "If the Subject is SDA, TempRespermitNumber must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).onSection("AB")
            ]
          },
          {
            field: "Resident.Individual.ForeignIDNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              warning("rifidn1", 452, "If category is 511/01 to 511/08 or 512/01 to 512/07 or 513 is used, ForeignIDNumber may not be completed",
                notEmpty).onSection("AB").onCategory(["511", "512", "513"]),
              warning("rifidn2", 452, "If the Flow is OUT and category 401 is used, may not be completed",
                notEmpty).onSection("AB").onCategory("401"),
              warning("rifidn3", 294, "If IndividualCustomer is selected, at least one of IDNumber or TempResPermitNumber or ForeignIDNumber must be completed",
                isEmpty.and(notTransactionField("Resident.Individual.IDNumber").and(notTransactionField("Resident.Individual.TempResPermitNumber")))).onSection("ABE"),
              warning("rifidn4", 204, "If the Non Resident AccountIdentifier is NON RESIDENT OTHER or CASH, the Resident IndividualCustomer is completed and the category is 250 or 251, the ForeignIDNumber must be completed",
                isEmpty.and(hasNonResidentFieldValue("AccountIdentifier", ["NON RESIDENT OTHER", "CASH"]))).onSection("A").onCategory(["250", "251"]),
              warning("rifidn5", 562, "If the Subject is SDA, ForeignIDNumber must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "SDA"))).onSection("AB")
            ]
          },
          {
            field: "Resident.Individual.ForeignIDCountry",
            len: 2,
            rules: [
              // NOTE: Removed section E
              warning("rifidc1", 300, "If ForeignIDNumber is completed, must be completed",
                isEmpty.and(hasTransactionField("Resident.Individual.ForeignIDNumber"))).onSection("AB"),
              warning("rifidc2", 238, "Invalid SWIFT country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("ABE"),
              warning("rifidc3", 391, "The ForeignID Country must not be {{Locale}}",
                notEmpty.and(hasValue(map("Locale")))).onSection("ABE")
            ]
          },
          {
            field: "Resident.Individual.PassportNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              warning("ripn1", 301, "If BoPCategory 255 is used and the Flow is OUT, must not be completed (IndividualThirdPartyPassportNumb er must be completed)",
                notEmpty).onOutflow().onSection("AB").onCategory("255"),
              warning("ripn2", 301, "Must not be completed",
                notEmpty).onSection("G"),
              warning("ripn3", 302, "If BoPCategory 256 is used and the PassportNumber is not completed, (account holder is not traveling) the IndividualThirdPartyPassportNumber must be completed.",
                isEmpty.and(not(hasAllMoneyField("ThirdParty.Individual.PassportNumber")))).onSection("AB").onCategory("256")
            ]
          },
          {
            field: "Resident.Individual.PassportCountry",
            len: 2,
            rules: [
              warning("ripc1", 259, "If PassportNumber is completed, must be completed",
                isEmpty.and(hasTransactionField("Resident.Individual.PassportNumber"))).onSection("AB"),
              warning("ripc2", 238, "Invalid SWIFT country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("AB"),
              warning("ripc3", 302, "If BoPCategory 256 is used and the PassportCountry is not completed, (account holder is not traveling) the IndividualThirdPartyPassportCountry must be completed.",
                isEmpty.and(not(hasAllMoneyField("ThirdParty.Individual.PassportCountry")))).onSection("AB").onCategory("256")
            ]
          },
          {
            field: ["Resident.Individual.BeneficiaryID1", "Resident.Individual.BeneficiaryID2", "Resident.Individual.BeneficiaryID3", "Resident.Individual.BeneficiaryID4"],
            rules: [
              deprecated("ribenid", "S05", "This fields is not used for finsurv submissions",
                notEmpty)
            ]
          },
          {
            field: "Resident.Entity.EntityName",
            minLen: 2,
            maxLen: 70,
            rules: [
              warning("relen1", 304, "If Resident EntityCustomer is used, must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("ABC"),
              warning("relen2", 303, "Must not be completed",
                notEmpty).onSection("F"),
              warning("relen3", 261, "If the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then an Entity must not be completed",
                notEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onSection("A"),
              warning("relen4", 571, "Entity Name must be equal to the registered IHQ name as per the IHQ table if the RegistrationNumber is equal to the IHQ registration number as per the IHQ table",
                notEmpty.and(evalTransactionField("Resident.Entity.RegistrationNumber", isInLookup("ihqCompanies", "registrationNumber"))).and(not(isInLookup("ihqCompanies", "companyName")))).onSection("A"),
              warning("relen5", 447, "If NonResident Exception is IHQ, value must not be same as {{Regulator}}-registered IHQ name",
                notEmpty.and(evalTransactionField("NonResident.Exception.ExceptionName", hasValue("IHQ"))).and(isInLookup("ihqCompanies", "companyName"))).onSection("A"),
              warning("relen6", 255, "The words specified under Non-resident or Resident ExceptionName, excluding STRATE, must not be used.",
                notEmpty.and(notValue('STRATE').and(isExceptionName))).onSection("ABCDEG"),
              // NOTE: Added this rule
              warning("relen7", 303, "If the Reporting Entity Code is 304 or 305, it must not be completed",
                notEmpty).onSection("G").onCategory(["304","305"]),
            ]
          },
          {
            field: "Resident.Entity.TradingName",
            minLen: 2,
            maxLen: 70,
            rules: [
              warning("retn1", 304, "If Resident EntityCustomer is used, must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("ABC")
            ]
          },
          {
            field: "Resident.Entity.RegistrationNumber",
            minLen: 2,
            maxLen: 30,
            rules: [
              warning("rern1", 306, "If Resident EntityCustomer is used, must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("ABEG"),
              warning("rern2", 403, "If Subject is AIRPORT the RegistrationNumber must be GOVERNMENT",
                notValue("GOVERNMENT").and(hasMoneyFieldValue("AdHocRequirement.Subject", "AIRPORT"))).onSection("AB").onCategory("830"),
              warning("rern3", 446, "If NonResident Exception is IHQ, RegistrationNumber must not be same as SARB-registered IHQ registration number",
                notEmpty.and(evalTransactionField("NonResident.Exception.ExceptionName", hasValue("IHQ"))).and(isInLookup("ihqCompanies", "registrationNumber"))).onSection("A"),
              warning("rern4", 219, "Additional spaces identified in data content",
                notEmpty.and(hasAdditionalSpaces)).onSection("ABEG")
            ]
          },
          {
            field: "Resident.Entity.InstitutionalSector",
            len: 2,
            rules: [
              warning("reis1", 308, "Must be completed", isEmpty).onSection("ABE"),
              warning("reis2", 518, "Must not be completed", notEmpty).onSection("F"),
              warning("reis3", 310, "If InstitutionalSectorCode is completed, must be valid", notEmpty.and(notValidInstitutionalSector)).onSection("ABCDEG")
            ]
          },
          {
            field: "Resident.Entity.IndustrialClassification",
            len: 2,
            rules: [
              warning("reic1", 311, "Must be completed", isEmpty).onSection("ABE"),
              warning("reic2", 519, "Must not be completed", notEmpty).onSection("F"),
              // NOTE: Wrong error code, changed 311 to 313
              warning("reic3", 313, "If IndustrialClassification is completed, must be valid", notEmpty.and(notValidIndustrialClassification)).onSection("ABCDEG")
            ]
          },
          {
            field: "Resident.Exception",
            rules: [
              // NOTE: Removed section B
              warning("re1", 315, "Must not contain a value", notEmpty).onSection("EF")
            ]
          },
          {
            field: "Resident.Exception.ExceptionName",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("ren1", 267, "May only contain one of the specified values",
                notResExceptionName).onSection("ACD"),
              warning("ren2", 316, "If category 250 or 251 is used, may only contain the value MUTUAL PARTY",
                notEmpty.and(notValue("MUTUAL PARTY"))).onSection("A").onCategory(["250", "251"]),
              warning("ren3", 316, "For any BoPCategory other than 200, 250, 251 the value MUTUAL PARTY must not be completed.",
                notEmpty.and(hasValue("MUTUAL PARTY"))).onSection("A").notOnCategory(["200", "250", "251"]),
              warning("ren4", 315, "May not be used. MUTUAL PARTY is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("MUTUAL PARTY"))).onSection("BCDEFG"),
              warning("ren5", 316, "May not be used. BULK PENSIONS is only valid for category 407 or 400",
                notEmpty.and(hasValue("BULK PENSIONS"))).onSection("A").notOnCategory(["400", "407"]),
              warning("ren6", 316, "May only be completed if the category is 100 or 200 or 300 or 400 or 500 or 600 or 700 or 800",
                notEmpty.and(hasValue("UNCLAIMED DRAFTS"))).onSection("A").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              warning("ren7", 315, "May not be completed. FCA NON RESIDENT NON REPORTABLE is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("FCA NON RESIDENT NON REPORTABLE"))).onSection("ABEFG"),
              warning("ren8", 315, "May not be completed. VOSTRO NON REPORTABLE is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("VOSTRO NON REPORTABLE"))).onSection("ABEFG"),
              warning("ren9", 315, "May not be completed. VOSTRO INTERBANK is only applicable for NON REPORTABLE transactions.",
                notEmpty.and(hasValue("VOSTRO INTERBANK"))).onSection("ABEFG"),
              warning("ren10", 316, "May not be used. BULK INTEREST is only valid for category 309/08 or 300",
                notEmpty.and(hasValue("BULK INTEREST"))).onSection("A").notOnCategory(["309/08", "300"]),
              warning("ren11", 316, "Category may only be 301 or 300",
                notEmpty.and(hasValue("BULK DIVIDENDS"))).onSection("A").notOnCategory(["301", "300"]),
              warning("ren12", 316, "Category may only be 275 or 200",
                notEmpty.and(hasValue("BULK BANK CHARGES"))).onSection("A").notOnCategory(["275", "200"]),
              warning("ren13", 316, "May only be completed if the category is 601/01 or 603/01 or 600",
                notEmpty.and(hasValue("STRATE"))).onSection("A").notOnCategory(["601/01", "603/01", "600"]),
              warning("ren14", 315, "May not be used. RAND CHEQUE is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("RAND CHEQUE"))).onSection("BCDEFG"),
              warning("ren15", 315, "May not be used. BULK PENSIONS is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK PENSIONS"))).onSection("BCDEFG"),
              warning("ren16", 315, "May not be completed except if the NonResident AccountIdentifier is NON RESIDENT RAND, VISA NET or MASTER SEND",
                notEmpty.and(hasValue("NON RESIDENT RAND").and(notNonResidentFieldValue("AccountIdentifier", ["NON RESIDENT RAND", "VISA NET", "MASTER SEND"])))).onSection("ABC"),
              warning("ren17", 315, "May not be used. NON RESIDENT RAND is only applicable for BOPCUS transactions",
                notEmpty.and(hasValue("NON RESIDENT RAND"))).onSection("CDEFG"),
              warning("ren18", 315, "May not be used. UNCLAIMED DRAFTS is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("UNCLAIMED DRAFTS"))).onSection("BCDEFG"),
              warning("ren19", 315, "May not be used. BULK INTEREST is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK INTEREST"))).onSection("BCDEFG"),
              warning("ren20", 315, "May not be used. BULK DIVIDENDS is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK DIVIDENDS"))).onSection("BCDEFG"),
              warning("ren21", 315, "May not be used. BULK BANK CHARGES is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("BULK BANK CHARGES"))).onSection("BCDEFG"),
              warning("ren22", 315, "May not be used. NOSTRO INTERBANK is only applicable for INTERBANK and NON REPORTABLE transactions",
                notEmpty.and(hasValue("NOSTRO INTERBANK"))).onSection("ABEFG"),
              warning("ren23", 315, "May not be used. NOSTRO NON REPORTABLE is only applicable for INTERBANK and NON REPORTABLE transactions",
                notEmpty.and(hasValue("NOSTRO NON REPORTABLE"))).onSection("ABEFG"),
              warning("ren24", 315, "May not be used. RTGS NON REPORTABLE is only applicable for INTERBANK and NON REPORTABLE transactions",
                notEmpty.and(hasValue("RTGS NON REPORTABLE"))).onSection("ABEFG"),
              warning("ren25", 315, "May not be used. STRATE is only applicable for BOPCUS transactions.",
                notEmpty.and(hasValue("STRATE"))).onSection("BCDEFG"),
              warning("ren26", 315, "For categories 511/01 to 511/08 or 512/01 to 512/07 or 513 the Exception must not be completed",
                notEmpty).onSection("AB").onCategory(["511", "512", "513"]),
              warning("ren27", 315, "If the Subject under the MonetaryDetails is REMITTANCE DISPENSATION and category 251 then MUTUAL PARTY may not be used",
                hasValue("MUTUAL PARTY").and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A").onCategory("251"),
              warning("ren28", 267, "Exception name is case-sensitive and must contain one of the specified values",
                notEmpty.and(notPattern(/^[A-Z][A-Z\s]+[A-Z]$/))),
            ]
          },
          {
            field: "Resident.Exception.Country",
            len: 2,
            rules: [
              warning("rec1", 318, "Must be completed",
                isEmpty).onSection("CD"),
              warning("rec2", 290, "If Resident ExceptionName is VOSTRO NON REPORTABLE or VOSTRO INTERBANK, the Country must not be {{Locale}}",
                notEmpty.and(hasValue(map("Locale"))).and(hasTransactionFieldValue("Resident.Exception.ExceptionName", ["VOSTRO NON REPORTABLE", "VOSTRO INTERBANK"]))).onSection("CD"),
              warning("rec3", 238, "Invalid country code",
                notEmpty.and(hasInvalidSWIFTCountry)).onSection("CD"),
              warning("rec4", 314, "May not be completed",
                notEmpty).onSection("ABEFG")
            ]
          },
          {
            field: "Resident.Exception.AccountIdentifier",
            rules: [
              deprecated("rexai", "S06", "This field is not used for finsurv submissions",
                notEmpty)
            ]
          },
          {
            field: "Resident.Exception.AccountNumber",
            rules: [
              deprecated("rexan", "S07", "This field is not used for finsurv submissions",
                notEmpty)
            ]
          },
          {
            field: ["Resident.Individual", "Resident.Entity"],
            rules: [
              warning("g1", 340, "Must contain at least one of Email, Fax or Telephone",
                notEmpty.and(notResidentField("ContactDetails.Email").and(notResidentField("ContactDetails.Fax").and(notResidentField("ContactDetails.Telephone"))))).onSection("ABEG")
            ]
          },
          {
            field: ["Resident.Individual.AccountName", "Resident.Entity.AccountName"],
            minLen: 2,
            maxLen: 70,
            rules: [
              warning("an1", 319, "Must be completed except if the AccountIdentifier is CASH or EFT or CARD PAYMENT",
                isEmpty.and(notResidentFieldValue("AccountIdentifier", ["CASH", "EFT", "CARD PAYMENT"]))).onSection("ABEG")
            ]
          },
          {
            field: ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("accid1", 272, "Must contain a value of RESIDENT OTHER or CFC RESIDENT or FCA RESIDENT or CASH or EFT or CARD PAYMENT",
                notValueIn(["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "EFT", "CARD PAYMENT"])).onSection("ABG"),
              warning("accid2", 272, "Must contain a value of RESIDENT OTHER or CFC RESIDENT or FCA RESIDENT, CASH or VOSTRO or EFT or CARD PAYMENT",
                notValueIn(["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "VOSTRO", "EFT", "CARD PAYMENT"])).onSection("CD"),
              warning("accid3", 520, "May not be completed", notEmpty).onSection("F"),
              warning("accid4", 272, "Must contain a value DEBIT CARD or CREDIT CARD",
                notValueIn(["DEBIT CARD", "CREDIT CARD"])).onSection("E"),
              warning("accid5", 272, "Invalid AccountIdentifier",
                notValueIn(["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "VOSTRO", "DEBIT CARD", "CREDIT CARD", "EFT", "CARD PAYMENT"])).onSection("ABCDEG"),
              warning("accid6", 355, "If CFC RESIDENT, ForeignValue must be completed",
                hasValue("CFC RESIDENT").and(isCurrencyIn(map("LocalCurrency")))).onSection("ABCD"),
              warning("accid7", 356, "If VOSTRO, {{LocalValue}} must be completed",
                hasValue("VOSTRO").and(notCurrencyIn(map("LocalCurrency")))).onSection("ABCD")
//              warning("accid8", 272, "The Account Identifier cannot be 'FCA RESIDENT' for {{LocalCurrency}} transactions",
//                hasValue("FCA RESIDENT").and(isCurrencyIn(map("LocalCurrency")))).onSection("A")
              //warning("accid9", 568, "If Flow is OUT and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION then account identifier must be CASH",
              //    notValue("CASH").and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A")
            ]
          },
          {
            field: "Resident.Entity.AccountIdentifier",
            rules: [
              warning("eaccid1", 521, "If the RegistrationNumber under Resident EntityCustomer is equal to the IHQ registration number as per the IHQ table, the Resident AccountIdentifier must be FCA RESIDENT",
                evalTransactionField("Resident.Entity.RegistrationNumber", isInLookup('ihqCompanies', 'registrationNumber')).
                and(notNonResException("IHQ")).
                and(not(hasValue("FCA RESIDENT")))).onSection("A")
            ]
          },
          {
            field: ["Resident.Individual.AccountNumber", "Resident.Entity.AccountNumber"],

            minLen: 2,
            maxLen: 40,
            rules: [
              warning('accno1', 279, "Must be completed",
                isEmpty).onSection('E'),
              warning('accno2', 522, "Must not be completed",
                notEmpty).onSection('F'),
              warning('accno3', 280, "Must not be equal to AccountNumber under the NonResidentData element",
                notEmpty.and(matchesNonResidentField('AccountNumber'))).onSection('ABCDG'),
              warning('accno4', 279, "Must be completed if the Flow is OUT and the AccountIdentifier under AdditionalCustomerData is not CASH or EFT or CARD PAYMENT",
                isEmpty.and(notResidentFieldValue("AccountIdentifier", ["CASH", "EFT", "CARD PAYMENT"]))).onOutflow().onSection("ABCDG")
            ]
          },
          {
            field: ["Resident.Individual.CustomsClientNumber"],
            minLen: 2,
            maxLen: 15,
            rules: [
              failure('ccn1', 320, 'Must be completed if Flow is IN and category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106',
                isEmpty.and(not(hasTransactionField("Resident.Entity.CustomsClientNumber")))).onInflow().onSection("AB").notOnCategory(['101/11', '103/11']).onCategory(['101', '103', '105', '106']),
              failure('ccn2', 320, 'Must be completed if Flow is OUT and category is 101/01 to 101/10 or 102/01 to 102/10 or 103/01 to 103/10 or 104/01 to 104/10 or 105 or 106',
                isEmpty.and(not(hasTransactionField("Resident.Entity.CustomsClientNumber")))).onOutflow().onSection("AB").notOnCategory(['101/11', '102/11', '103/11', '104/11']).onCategory(['101', '102', '103', '104', '105', '106']),
              failure('ccn3', 322, 'CustomsClientNumber must be numeric and contain between 8 and 13 digits',
                notEmpty.and(notPattern(/^\d{8,13}$/))).onInflow().onSection("AB").notOnCategory(['101/11', '103/11']).onCategory(['101', '103', '105', '106']),
              failure('ccn4', 322, 'CustomsClientNumber must be numeric and contain between 8 and 13 digits',
                notEmpty.and(notPattern(/^\d{8,13}$/))).onOutflow().onSection("AB").notOnCategory(['101/11', '102/11', '103/11', '104/11']).onCategory(['101', '102', '103', '104', '105', '106']),
              failure('ccn5', 322, 'CustomsClientNumber 70707070 should not be regularly used for transactions more than R50,000.00',
                notEmpty.and(hasPattern(/^70707070$/)).and(hasSumLocalValue('>', "50000"))).onSection("AB"),
              failure('ccn6', 321, 'May not be completed',
                notEmpty).onSection("DEF"),
              failure("ccn7", "S09", "Unless the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, this need not be provided and if invalid will cause the SARB to reject transaction",
                notEmpty.and(not(hasAnyMoneyFieldValue("CategoryCode",["101", "103", "105", "106"])))).onInflow().onSection("AB"),
              failure('ccn8', "S15", 'Check that CustomsClientNumber 70707070 is only used if this transaction is smaller than R50,000.00',
                notEmpty.and(hasPattern(/^70707070$/)).and(hasSumLocalValue('<=', 50000))).onSection("AB"),
              failure("ccn9", "S09", "Unless the category is 101/01 to 101/10 or 102/01 to 102/10 or 103/01 to 103/10 or 104/01 to 104/10 or 105 or 106, this need not be provided and if invalid will cause the SARB to reject transaction",
                notEmpty.and(not(hasAnyMoneyFieldValue("CategoryCode",['101', '102', '103', '104', '105', '106'])))).onOutflow().onSection("AB"),
              failure('ccn10', 322, 'CustomsClientNumber should pass one of the validations for either CCN, ID Number or Tax Number',
                notEmpty.and(hasPattern(/^\d{8,13}$/)).and(not(isValidCCN.or(isValidRSAID.or(isValidZATaxNumber))))).onInflow().onSection("AB").notOnCategory(['101/11', '103/11']).onCategory(['101', '103', '105', '106']),
              failure('ccn11', 322, 'CustomsClientNumber should pass one of the validations for either CCN, ID Number or Tax Number',
                notEmpty.and(hasPattern(/^\d{8,13}$/)).and(not(isValidCCN.or(isValidRSAID.or(isValidZATaxNumber))))).onOutflow().onSection("AB").notOnCategory(['101/11', '102/11', '103/11', '104/11']).onCategory(['101', '102', '103', '104', '105', '106']),
              failure('ccn12', 322,'CustomsClientNumber is 70707070, which implies CCN is not known',
                notEmpty.and(hasPattern(/^70707070$/)))
            ]
          },
          {
            field: ["Resident.Entity.CustomsClientNumber"],
            minLen: 2,
            maxLen: 15,
            rules: [
              failure('ccn1', 320, 'Must be completed if Flow is IN and category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106',
                isEmpty.and(not(hasTransactionField("Resident.Individual.CustomsClientNumber")))).onInflow().onSection("AB").notOnCategory(['101/11', '103/11']).onCategory(['101', '103', '105', '106']),
              failure('ccn2', 320, 'Must be completed if Flow is OUT and category is 101/01 to 101/10 or 102/01 to 102/10 or 103/01 to 103/10 or 104/01 to 104/10 or 105 or 106',
                isEmpty.and(not(hasTransactionField("Resident.Individual.CustomsClientNumber")))).onOutflow().onSection("AB").notOnCategory(['101/11', '102/11', '103/11', '104/11']).onCategory(['101', '102', '103', '104', '105', '106']),
              failure('ccn3', 322, 'CustomsClientNumber must be numeric and contain between 8 and 13 digits',
                notEmpty.and(notPattern(/^\d{8,13}$/))).onInflow().onSection("AB").notOnCategory(['101/11', '103/11']).onCategory(['101', '103', '105', '106']),
              failure('ccn4', 322, 'CustomsClientNumber must be numeric and contain between 8 and 13 digits',
                notEmpty.and(notPattern(/^\d{8,13}$/))).onOutflow().onSection("AB").notOnCategory(['101/11', '102/11', '103/11', '104/11']).onCategory(['101', '102', '103', '104', '105', '106']),
              failure('ccn5', 322, 'CustomsClientNumber 70707070 should not be regularly used for transactions more than R50,000.00',
                notEmpty.and(hasPattern(/^70707070$/)).and(hasSumLocalValue('>', "50000"))).onSection("AB"),
              failure('ccn6', 321, 'May not be completed',
                notEmpty).onSection("DEF"),
              failure("ccn7", "S09", "Unless the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, this need not be provided and if invalid will cause the SARB to reject transaction",
                notEmpty.and(not(hasAnyMoneyFieldValue("CategoryCode",["101", "103", "105", "106"])))).onInflow().onSection("AB"),
              failure('ccn8', "S15", 'Check that CustomsClientNumber 70707070 is only used if this transaction is smaller than R50,000.00',
                notEmpty.and(hasPattern(/^70707070$/)).and(hasSumLocalValue('<=', 50000))).onSection("AB"),
              failure("ccn9", "S09", "Unless the category is 101/01 to 101/10 or 102/01 to 102/10 or 103/01 to 103/10 or 104/01 to 104/10 or 105 or 106, this need not be provided and if invalid will cause the SARB to reject transaction",
                notEmpty.and(not(hasAnyMoneyFieldValue("CategoryCode",['101', '102', '103', '104', '105', '106'])))).onOutflow().onSection("AB"),
              failure('ccn10', 322, 'CustomsClientNumber should pass one of the validations for either CCN, ID Number or Tax Number',
                notEmpty.and(hasPattern(/^\d{8,13}$/)).and(not(isValidCCN.or(isValidRSAID.or(isValidZATaxNumber))))).onInflow().onSection("AB").notOnCategory(['101/11', '103/11']).onCategory(['101', '103', '105', '106']),
              failure('ccn11', 322, 'CustomsClientNumber should pass one of the validations for either CCN, ID Number or Tax Number',
                notEmpty.and(hasPattern(/^\d{8,13}$/)).and(not(isValidCCN.or(isValidRSAID.or(isValidZATaxNumber))))).onOutflow().onSection("AB").notOnCategory(['101/11', '102/11', '103/11', '104/11']).onCategory(['101', '102', '103', '104', '105', '106']),
              failure('ccn12', 322,'CustomsClientNumber is 70707070, which implies CCN is not known',
                notEmpty.and(hasPattern(/^70707070$/)))
            ]
          },
          {
            field: "Resident.Individual.TaxNumber",
            minLen: 2,
            maxLen: 30,
            rules: [
              failure('tni1', 324, 'Must be completed',
                isEmpty.and(not(hasMoneyFieldValue("CategoryCode", "")))).onSection("AB"),
              warning('tni2', 523, 'Must not be completed.',
                notEmpty).onSection("F")
            ]
          },
          {
            field: "Resident.Entity.TaxNumber",
            minLen: 2,
            maxLen: 30,
            rules: [
              failure('tne1', 324, 'Must be completed',
                isEmpty.and(not(hasMoneyFieldValue("CategoryCode", "")))).onSection("AB"),
              failure('tne2', 523, 'Must not be completed.',
                notEmpty).onSection("F")
            ]
          },
          {
            field: ["Resident.Individual.TaxNumber", "Resident.Entity.TaxNumber"],
            rules: [
              failure('tn1', 325, 'Incorrect format: Invalid checksum',
                notEmpty.and(not(isValidZATaxNumber)).and(hasResidentField("CustomsClientNumber").and(hasMoneyField("CategoryCode")))).onSection("AB")
            ]
          },
          {
            field: "Resident.Individual.VATNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              ignore('vni1'), // ignored vni1 so that rules are in line with Resident.Entity.VATNumber
              failure('vni2', 524, 'Must not be completed.',
                notEmpty).onSection("F"),
              failure('vni3', 326, 'Must be completed',
                isEmpty.and(not(hasMoneyFieldValue("CategoryCode", "")))).onSection("AB"), // hasMoneyField doesn't work in transaction scope
            ]
          },
          {
            field: "Resident.Entity.VATNumber",
            minLen: 2,
            maxLen: 20,
            rules: [
              failure('vne1', 326, 'Must be completed',
                isEmpty.and(not(hasMoneyFieldValue("CategoryCode", "")))).onSection("AB"),
              failure('vne2', 524, 'Must not be completed.',
                notEmpty).onSection("F")
            ]
          },
          {
            field: ["Resident.Individual.VATNumber", "Resident.Entity.VATNumber"],
            rules: [
              failure('vn1', 326, 'Incorrect format: Should be "NO VAT NUMBER" or has invalid checksum',
                notEmpty.and(not(hasValue("NO VAT NUMBER").or(isValidZAVATNumber))).and(hasResidentField("CustomsClientNumber").and(hasMoneyField("CategoryCode")))).onSection("AB")
            ]
          },
          {
            // TODO: Does not contain all cases as per the spec
            field: ["Resident.Individual.TaxClearanceCertificateIndicator", "Resident.Entity.TaxClearanceCertificateIndicator"],
            len: 1,
            rules: [
              warning('tcci1', 327, 'Must be "Y" or "N" if Flow is OUT and category is 512/01 to 512/07 or 513',
                notValueIn(['Y', 'N'])).onOutflow().onSection("AB").onCategory(['512', '513']),
                // TODO: Should not be on section F              
                warning('tcci2', 525, 'Must not be completed',
                notEmpty).onSection("CDEF")
            ]
          },
          {
            field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
            minLen: 2,
            maxLen: 30,
            rules: [
              warning('tccr1', 249, 'TaxClearanceCertificateIndicator is "Y", so needs to be completed',
                isEmpty.and(hasResidentFieldValue('TaxClearanceCertificateIndicator', 'Y'))).onSection("AB"),
            // TODO: Should not be on section E and F  
              warning('tccr2', 526, 'Must not be completed', notEmpty).onSection("CDEF")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1", "Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1"],
            minLen: 2,
            maxLen: 70,
            rules: [
              warning('a1_1', 332, 'Must be completed', isEmpty).onSection("BEG"),
              warning('a1_2', 527, 'Must not be completed', notEmpty).onSection("DF"),
              warning("a1_3", 332, 'Must be completed (unless REMITTANCE DISPENSATION on a reversal)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onSection("A"),
              warning("a1_4", 332, 'Must be completed (unless REMITTANCE DISPENSATION)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.AddressLine2", "Resident.Entity.StreetAddress.AddressLine2", "Resident.Individual.PostalAddress.AddressLine2", "Resident.Entity.PostalAddress.AddressLine2"],
            minLen: 2,
            maxLen: 70,
            rules: [
              warning('a2_1', 527, 'Must not be completed', notEmpty).onSection("DF")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb", "Resident.Individual.PostalAddress.Suburb", "Resident.Entity.PostalAddress.Suburb"],
            minLen: 2,
            maxLen: 35,
            rules: [
              warning('s1', 333, 'Must be completed', isEmpty).onSection("BEG"),
              warning('s2', 527, 'May not be completed', notEmpty).onSection("DF"),
              warning('s3', 333, 'Must be completed (unless REMITTANCE DISPENSATION on a reversal)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onSection("A"),
              warning('s4', 333, 'Must be completed (unless REMITTANCE DISPENSATION)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City", "Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City"],
            minLen: 2,
            maxLen: 35,
            rules: [
              warning('c1', 334, 'Must be completed', isEmpty).onSection("BEG"),
              warning('c2', 527, 'Must not be completed', notEmpty).onSection("DF"),
              warning('c3', 334, 'Must be completed (unless REMITTANCE DISPENSATION on a reversal)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onSection("A"),
              warning('c4', 334, 'Must be completed (unless REMITTANCE DISPENSATION)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.PostalCode", "Resident.Entity.StreetAddress.PostalCode"],
            minLen: 2,
            maxLen: 10,
            rules: [
              warning('spc1', 338, 'Invalid postal code',
                notEmpty.and(notValidPostalCode)).onSection("ABEG"),
              warning('spc2', 527, 'May not be completed',
                notEmpty).onSection("DF"),
              warning('spc3', 338, 'If the Street Province is NAMIBIA or LESOTHO or SWAZILAND then the PostalCode must be 9999',
                hasResidentFieldValue("StreetAddress.Province", ["NAMIBIA", "LESOTHO", "SWAZILAND"]).and(notValue("9999"))).onSection("E"),
              warning('spc4', 338, 'A Postal code of 9999 may not be used for South African province',
                notEmpty.and(hasValue("9999").and(notResidentFieldValue("StreetAddress.Province", ["NAMIBIA", "LESOTHO", "SWAZILAND"])))).onSection("ABEG")
            ]
          },
          {
            field: ["Resident.Individual.PostalAddress.PostalCode", "Resident.Entity.PostalAddress.PostalCode"],
            minLen: 2,
            maxLen: 10,
            rules: [
              warning('pc1', 338, 'Invalid postal code',
                notEmpty.and(notValidPostalCode)).onSection("ABEG"),
              warning('pc2', 337, 'Must be completed',
                isEmpty).onSection("BEG"),
              warning('pc3', 527, 'Must not be completed',
                notEmpty).onSection("DF"),
              warning('pc4', 338, 'If the Street Province is NAMIBIA or LESOTHO or SWAZILAND then the PostalCode must be 9999',
                hasResidentFieldValue("PostalAddress.Province", ["NAMIBIA", "LESOTHO", "SWAZILAND"]).and(notValue("9999"))).onSection("E"),
              warning('pc5', 338, 'A Postal code of 9999 may not be used for South African province',
                notEmpty.and(hasValue("9999").and(notResidentFieldValue("PostalAddress.Province", ["NAMIBIA", "LESOTHO", "SWAZILAND"])))).onSection("ABEG"),
              warning('pc6', 337, 'Must be completed (unless REMITTANCE DISPENSATION on a reversal)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onSection("A"),
              warning('pc7', 337, 'Must be completed (unless REMITTANCE DISPENSATION)',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A")
            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.ContactSurname", "Resident.Entity.ContactDetails.ContactSurname", "Resident.Individual.ContactDetails.ContactName", "Resident.Entity.ContactDetails.ContactName"],
            minLen: 2,
            maxLen: 35,
            rules: [
              // NOTE: Removed section EG
              warning('cn1', 339, 'Must be completed',
                isEmpty).onSection("AB"),
              warning('cn2', 528, 'Must not be completed',
                notEmpty).onSection("DF")
            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.Email",
              "Resident.Entity.ContactDetails.Email"],
            minLen: 2,
            maxLen: 120,
            rules: [
              // NOTE: Changed this message
              warning('cnte1', 340, 'Must contain at least one of Email, Fax or Telephone',
                notResidentField('ContactDetails.Email').and(notResidentField('ContactDetails.Fax')).and(notResidentField('ContactDetails.Telephone'))).onSection("ABEG"),
              warning('cnte2', 528, 'Must not be completed', notEmpty).onSection("DF"),
              warning("cnte3", "E01", "This is not a valid email address",
                  notEmpty.and(notValidEmail))

            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.Fax",
              "Resident.Entity.ContactDetails.Fax",
              "Resident.Individual.ContactDetails.Telephone",
              "Resident.Entity.ContactDetails.Telephone"],
            minLen: 2,
            maxLen: 15,
            rules: [
              warning('cntft1', 340, 'Must contain at least one of Email, Fax or Telephone',
                notResidentField('ContactDetails.Email').and(notResidentField('ContactDetails.Fax')).and(notResidentField('ContactDetails.Telephone'))).onSection("ABEG"),
              warning('cntft2', 528, 'Must not be completed',
                notEmpty).onSection("DF"),
              // NOTE: Changed this message
              warning('cntft3', 341, 'Must be in a 10 to 15 digit format',
                notEmpty.and(notPattern(/^\d{10,15}$/))).onSection("ABEG")
            ]
          },
          {
            field: ["Resident.Individual.CardNumber", "Resident.Entity.CardNumber"],
            minLen: 2,
            maxLen: 20,
            rules: [
              warning('crd1', 342, 'Must be completed', isEmpty).onSection("E"),
              warning('crd2', 344, 'Must not be completed', notEmpty).onSection("ABCDFG")
            ]
          },
          {
            field: ["Resident.Individual.SupplementaryCardIndicator", "Resident.Entity.SupplementaryCardIndicator"],
            len: 1,
            rules: [
              // NOTE: Left more detailed message unchanged
              warning('crdi1', 345, 'Must be set to be either Y or N (blank assumed as N)',
                notEmpty.and(notValueIn(['Y', 'N']))).onSection("E"),
              warning('crdi2', 347, 'Must not be completed',
                notEmpty).onSection("ABCDFG")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province", "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
            minLen: 2,
            maxLen: 35,
            rules: [
              warning('p1', 336, 'Must be valid South African province',
                notValidProvince).onSection("BG"),
              warning('p2', 527, 'May not be completed',
                notEmpty).onSection("DF"),
              ignore('p3'),
              warning('p4', 336, 'Must be valid province (including NAMIBIA, LESOTHO or SWAZILAND)',
                notValidProvince.and(notValueIn(["NAMIBIA", "LESOTHO", "SWAZILAND"]))).onSection("E"),
              warning('p5', 287, 'Must be set to NAMIBIA because ForeignIDCountry is NA',
                hasTransactionFieldValue("Resident.Individual.ForeignIDCountry", "NA").and(notValueIn("NAMIBIA"))).onSection("E"),
              warning('p6', 287, 'Must be set to LESOTHO because ForeignIDCountry is LS',
                hasTransactionFieldValue("Resident.Individual.ForeignIDCountry", "LS").and(notValueIn("LESOTHO"))).onSection("E"),
              warning('p7', 287, 'Must be set to SWAZILAND because ForeignIDCountry is SZ',
                hasTransactionFieldValue("Resident.Individual.ForeignIDCountry", "SZ").and(notValueIn("SWAZILAND"))).onSection("E"),
              warning('p8', 336, 'Must be valid South African province',
                notValidProvince).onInflow().onSection("A"),
              warning('p9', 336, 'Must be valid South African province',
                notEmpty.and(notValidProvince)).onOutflow().onSection("A"),
              warning('p10', 336, 'If the flow is OUT and the Subject is REMITTANCE DISPENSATION, this must be valid South African province',
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onOutflow().onSection("A"),
              warning('p11', 336, 'If the Flow is IN and the category is 400 and the Subject under the MonetaryDetails is REMITTANCE DISPENSATION, this must be valid South African province',
                isEmpty.and(hasMoneyFieldValue("AdHocRequirement.Subject", "REMITTANCE DISPENSATION"))).onInflow().onCategory('400').onSection("A"),

            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.Email",
              "Resident.Entity.ContactDetails.Email"],
            rules: [
              warning("cnte3", "E01", "This is not a valid email address",
                  notEmpty.and(notValidEmail))
            ]
          },
          {
            field: "",
            rules: [
              // Ignoring external CCN validations for Standard Bank
              ignore('ext_ccn1'),
              ignore('ext_ccn2'),
              // Applying the messages as per the BOTTs channel (sbIBR1)
              message("tfv1",null, "The category amounts do not add up to the total amount"),
              message("tfv2",null, "The category amounts do not add up to the total amount"),
              message("tn1",null,"Please enter a valid Tax number"),
              message("vn1",null,"Please enter a valid VAT number"),
              message("a2_1", null, "Not required"),
              message("accid3", null, "Not required"),
              message("accno1", null, "Please enter the required information"),
              message("accno2", null, "Not required"),
              message("accno3", null, "Please enter a valid Account number"),
              message("accno4", null, "Please enter the required information"),
              message("c2", null, "Not required"),
              message("cbank2", null, "Not required"),
              message("ccn2", null, "Please enter the required information"),
              message("ccn5", null, "Your importers code (CCN) exceeds your total transaction cost of R50 000.00"),
              message("ccn6", null, "Not required"),
              message("ccntry1", null, "Please enter the required information"),
              message("ccntry2", null, "Not required"),
              message("ccntry3", null, "Please enter a valid SWIFT code"),
              message("cn1", null, "Please enter the required information"),
              message("cn2", null, "Not required"),
              message("cnte1", null, "Please enter either an email, fax or telephone number"),
              message("cnte2", null, "Not required"),
              message("cnte4", null, "Please enter a valid email address"),
              message("cntft1", null, "Please enter either an email, fax or telephone number"),
              message("cntft2", null, "Not required"),
              message("cntft3", null, "Please enter a phone number in 10 or 15 digits format"),
              message("crd1", null, "Please enter the required information"),
              message("crd2", null, "Not required"),
              message("crdi1", null, "Please enter the required information"),
              message("crdi2", null, "Not required"),
              message("eaccid1", null, "Your registration number is invalid"),
              message("fcurr1", null, "Please enter the required information"),
              message("fcurr2", null, "Please enter a Valid SWIFT currency code"),
              message("flow1", null, "Please enter the required information"),
              message("flw_a1_1", null, "Please enter the required information"),
              message("flw_a1_2", null, "Address shouldn't exceed 35 characters"),
              message("flw_a1_4", null, "Please enter the required information"),
              message("flw_a2_1", null, "Address shouldn't exceed 35 characters"),
              message("flw_addr_l1", null, "This field shouldn't exceed 35 characters"),
              message("flw_ahs1", null, "Please enter the required information"),
              message("flw_ahs2", null, "Please use BOP 250 or 251 for travel"),
              message("flw_bba1", null, "Please enter the required information"),
              message("flw_bbbc1", null, "Please enter the required information"),
              message("flw_bbc", null, "Please enter the required information"),
              message("flw_bbc1", null, "Please enter the required information"),
              message("flw_bbnm1", null, "Please enter the required information"),
              message("flw_c1", null, "Please enter the required information"),
              message("flw_c2", null, "City shouldn't exceed 35 characters"),
              message("flw_c4", null, "Please enter the required information"),
              message("flw_cbc1", null, "Please enter the required information"),
              message("flw_cn1", null, "Contact name shouldn't exceed 50 characters"),
              message("flw_cnte1", null, "Please provide a valid email address"),
              message("flw_cps1", null, "Please enter the required information"),
              message("flw_csn1", null, "Surname shouldn't exceed 35 characters"),
              message("flw_iisa1", null, "Please enter the required information"),
              message("flw_iisa2", null, "Not required for ZAR payments"),
              message("flw_isa1", null, "Please enter the required information"),
              message("flw_isa2", null, "Foreign Currency Account (FCA) is not required, please remove"),
              message("flw_isa3", null, "Foreign Currency Account and Account to be Debitted must not match"),
              message("flw_lc1", null, "Please enter the required information"),
              message("flw_lc5", null, "Not required"),
              message("flw_masad2", null, "Please enter a valid date format: YYYY-MM-DD"),
              message("flw_masan1", null, "Please enter the required information"),
              message("flw_masan2", null, "Please enter the required information"),
              message("flw_mcc1", null, "You must be a South African Resident to use codes 511, 512, 513, or 516"),
              message("flw_mexc1", null, "Please enter the required information for BOP category 830"),
              message("flw_mexc2", null, "The required information shouldn't exceed 100 characters"),
              message("flw_mlibr1", null, "Please provide the base rate for this BOP category"),
              message("flw_mlipm1", null, "Please enter the required information for loans"),
              message("flw_mlipm12", null, "Only provide the required information if using a Interest Base Rate"),
              message("flw_mlir1", null, "Please provide the base rate for this BOP category"),
              message("flw_mlir3", null, "Please provide the base rate for this BOP category"),
              message("flw_mlir7", null, "Please enter the required information"),
              message("flw_mlit2", null, "Only provide the required information if using a Interest Base Rate (and this rate needs a term)"),
              message("flw_mlrn1", null, "Reference number shouldn't exceed 20 characters"),
              message("flw_mlt1", null, "Please provide a Loan Tenor for this BOP category"),
              message("flw_mtpcd_l1", null, "Surname shouldn't exceed 35 characters"),
              message("flw_mtpcdn_l1", null, "Contact name shouldn't exceed 50 characters"),
              message("flw_mtpibd3", null, "The date of birth should match the first 6 letters of your ID number"),
              message("flw_mtpibd4", null, "Please enter the required information"),
              message("flw_mtpibd5", null, "The date of birth does not match the first 6 letters of your ID number"),
              message("flw_mtpiid3", null, "Please enter a valid ID number"),
              message("flw_mtpinm_l1", null, "Name shouldn't exceed 50 characters"),
              message("flw_mtpinm0", null, "Please enter the required information"),
              message("flw_mtpinm3", null, "Please enter the required information"),
              message("flw_mtpipe1", null, "Please enter the required information"),
              message("flw_mtpipe2", null, "Please enter a valid date format: YYYY-MM-DD"),
              message("flw_mtpipe5", null, "Your passport has expired"),
              message("flw_mtpipn_l1", null, "Passport number shouldn't exceed 15 characters"),
              message("flw_mtpisn_l1", null, "Surname shouldn't exceed 35 characters"),
              message("flw_mtpisn3", null, "Not required"),
              message("flw_mtpisn4", null, "Please enter the required information"),
              message("flw_mtpite1", null, "Please enter the required information"),
              message("flw_mtpite2", null, "Please enter a valid date format: YYYY-MM-DD"),
              message("flw_mtpitp_l1", null, "Permit number is limited to 35 characters"),
              message("flw_nrian1", null, "Please enter the required information"),
              message("flw_nrian4", null, "Account number shouldn't exceed 34 characters"),
              message("flw_nrian5", null, "Not required"),
              message("flw_nrlen_l1", null, "Company name shouldn't exceed 70 characters"),
              message("flw_nrnm_l1", null, "Name shouldn't exceed 50 characters"),
              message("flw_nrnm1", null, "Please enter the required information"),
              message("flw_nrsn_l1", null, "Surname shouldn't exceed 35 characters"),
              message("flw_p10", null, "Please enter a valid South African province"),
              message("flw_p2", null, "Please enter the required information"),
              message("flw_p3", null, "Province shouldn't exceed 35 characters"),
              message("flw_relen1", null, "Company name shouldn't exceed 70 characters"),
              message("flw_resap1", null, "Please enter a valid South African province"),
              message("flw_ridob1", null, "Please enter a valid Date of birth"),
              message("flw_riidn1", null, "Not required"),
              message("flw_rimn1", null, "Middle name shouldn't exceed 50 characters"),
              message("flw_rin1", null, "Name shouldn't exceed 50 characters"),
              message("flw_ripc1", null, "Please provide the required information for BOP category 250 or 251"),
              message("flw_ripc2", null, "Please enter the required information"),
              message("flw_ripc3", null, "Please enter the required information"),
              message("flw_ripe1", null, "Please enter the required information"),
              message("flw_ripe2", null, "Please enter the required information"),
              message("flw_ripe3", null, "Please enter the required information"),
              message("flw_ripe4", null, "Please enter the required information"),
              message("flw_ripe5", null, "Please enter a valid date format: YYYY-MM-DD"),
              message("flw_ripe6", null, "Your passport has expired"),
              message("flw_ripn1", null, "Please enter the required information"),
              message("flw_ripn2", null, "Please enter the required information"),
              message("flw_ripn3", null, "Please enter the required information"),
              message("flw_ripn4", null, "Passport number shouldn't exceed 15 characters"),
              message("flw_risap1", null, "Please enter a valid South African province"),
              message("flw_risn1", null, "Surname shouldn't exceed 35 characters"),
              message("flw_ritrpe1", null, "Please enter the required information"),
              message("flw_ritrpe3", null, "Please enter a valid date format: YYYY-MM-DD"),
              message("flw_ritrpe4", null, "Your temporary resident permit number has expired"),
              message("flw_ritrpn2", null, "Permit number shouldn't exceed 35 characters"),
              message("flw_ritrpn3o", null, "Please enter the required information"),
              message("flw_rsac1", null, "Please enter the required information"),
              message("flw_rsam1", null, "Please enter the required information"),
              message("flw_rsam3", null, "not required"),
              message("flw_s1", null, "Please enter the required information"),
              message("flw_s2", null, "Suburb shouldn't exceed 35 characters"),
              message("flw_s4", null, "Please enter the required information"),
              message("flw_spc_l1", null, "Postal code shouldn't exceed 10 characters"),
              message("flw_spc1", null, "Please enter the required information"),
              message("flw_tccr1", null, "Not required"),
              message("flw_tccr3", null, "Please enter the required information"),
              message("flw_tn1", null, "Tax number shouldn't exceed 30 characters"),
              message("flw_tpits1", null, "Please enter the required information"),
              message("flw_tpits2", null, "Please enter the required information for BOP category 256"),
              message("flw_tpits3", null, "not required"),
              message("flw_tpits5", null, "not required"),
              message("flw_tvdc1", null, "Please enter the required information for BOP cateogry 225 and 256"),
              message("flw_tvdc2", null, "not required"),
              message("flw_tvlbp1", null, "Please enter the required information when traveling by road"),
              message("flw_tvlbp2", null, "not required"),
              message("flw_tvlbp3", null, "not required"),
              message("flw_tvldd1", null, "Please enter the required information"),
              message("flw_tvldd2", null, "Please enter a valid date format: YYYY-MM-DD"),
              message("flw_tvldd3", null, "Not required"),
              message("flw_tvlm1", null, "Please enter the required information"),
              message("flw_tvlm2", null, "Not required"),
              message("flw_tvltn1", null, "Please enter the required information"),
              message("flw_tvltn2", null, "Not required"),
              message("g1", null, "Please enter the required information"),
              message("ieicn1", null, "Please enter the required information"),
              message("ieicn2", null, "Not required"),
              message("ieicn3", null, "Please enter a valid Import control number: INV followed by number, minimum 4 characters"),
              message("ieicn4", null, "Please enter the format as follows: AAACCYYMMDD0000000 where AAA is a valid customs office code in alpha format; CC is the century of import, YY is the year of import, MM is the month of import, DD is the day of import, and 0000000 is the 7 digit unique bill of entry number allocated by SARS as part of the MRN"),
              message("ieicn5", null, "No additional spaces or comma's (,) allowed"),
              message("ieicn6", null, "Please enter the required information"),
              message("iepcc1", null, "Please enter the required information"),
              message("iepcc2", null, "The currency code should match the transaction currency"),
              message("iepv1", null, "Please enter the required information"),
              message("ietdn1", null, "Please enter the required information"),
              message("ietdn2", null, "Not required"),
              message("ietdn3", null, "Please enter the required information"),
              message("ietdn4", null, "Not required"),
              message("ieucr4", null, "Not required"),
              message("madhd1", null, "Please enter the required information"),
              message("madhd2", null, "Not required"),
              message("madhd3", null, "Not required"),
              message("madhs12", null, "Amount shouldn't exceed R3 000"),
              message("madhs13", null, "Not allowed for Remittance dispensation"),
              message("madhs17", null, "Please enter a valid IHQ number"),
              message("madhs2", null, "Not required"),
              message("maiad2", null, "Not required"),
              message("maiad3", null, "Please enter a valid date format: CCYY-MM-DD"),
              message("maian2", null, "Not required"),
              message("mars2", null, "Not required"),
              message("masan3", null, "Not required"),
              message("masan4", null, "Please enter the required information"),
              message("masar3", null, "Not required"),
              message("mcc1", null, "Please enter the required information"),
              message("mcc2", null, "Not required"),
              message("mcc3", null, "Please enter a valid Finsurv category"),
              message("mcc6", null, "Please enter the required information"),
              message("mcc8", null, "Only required for Import undertaking customers"),
              message("mcc9", null, "Only required for Import undertaking customers"),
              message("mcrdcb1", null, "Not required"),
              message("mcrdci1", null, "Not required"),
              message("mcrdec1", null, "Please enter the required information"),
              message("mcrdec2", null, "Not required"),
              message("mcrdem1", null, "Not required"),
              message("mcrdem2", null, "Please enter the required information"),
              message("mcrdfp1", null, "Not required"),
              message("mcrdfp2", null, "Please enter the required information"),
              message("mcrdfp3", null, "This field shouldn't contain a negative value"),
              message("mcrdft1", null, "Not required"),
              message("mcrdfw1", null, "Not required"),
              message("mcrdfw2", null, "Please enter the required information"),
              message("mcrdfw3", null, "This field shouldn't contain a negative value"),
              message("mdircd1", null, "Not required"),
              message("mdirtr1", null, "Not required"),
              message("mexc1", null, "Please enter the required information"),
              message("mexc2", null, "Not required"),
              message("mfv2", null, "This field shouldn't contain a negative value"),
              message("mfv3", null, "Please enter the required information"),
              message("mfv4", null, "Not required"),
              message("mfv5", null, "Foreign value shouldn't exceed 20 digits"),
              message("mfv6", null, "Please enter the required information"),
              message("mlc5", null, "Not required"),
              message("mlc6", null, "Please enter the required information"),
              message("mlc7", null, "Please enter the required information"),
              message("mlir2", null, "Not required"),
              message("mlir4", null, "Please enter the required information in the format 0.00"),
              message("mlir5", null, "Interest rate cannot be greater than 100%"),
              message("mlir7", null, "Not required"),
              message("mlrn1", null, "Please enter the required information"),
              message("mlrn10", null, "Not required"),
              message("mlrn11", null, "Not required"),
              message("mlrn2", null, "Loan reference number must be 99012301230123"),
              message("mlrn3", null, "Loan reference number must be 99456745674567"),
              message("mlrn4", null, "Loan reference number must be 99789078907890"),
              message("mlrn5", null, "Loan reference number must be 99012301230123"),
              message("mlrn6", null, "Loan reference number must be 99456745674567"),
              message("mlrn7", null, "Loan reference number must be 99789078907890"),
              message("mlrn8", null, "Please enter the required information"),
              message("mlrn9", null, "Not required"),
              message("mlt1", null, "Please enter a valid date format: YYYY-MM-DD"),
              message("mlt2", null, "Please enter a future date in the format: CCYY-MM-DD"),
              message("mlt3", null, "Not required"),
              message("mrtrn1", null, "Please enter the required information"),
              message("mrtrn3", null, "Not required"),
              message("mrtrn5", null, "Not required"),
              message("mrtrs1", null, "Please enter the required information"),
              message("mrtrs2", null, "Please enter the required information"),
              message("mrtrs3", null, "Not required"),
              message("mrv5", null, "Not required"),
              message("mrv6", null, "The amount is too high for the BOP cateogry selected"),
              message("mrv7", null, "This field shouldn't exceed 1 000"),
              message("mrv8", null, "This field shouldn't exceed 20 digits"),
              message("mrv9", null, "Please enter the required information"),
              message("msc1", null, "Please enter a valid sub category code"),
              message("msc2", null, "Not required"),
              message("mseq1", null, "Please enter the required information"),
              message("msrn3", null, "Not required"),
              message("mswd1", null, "Not required"),
              message("mta5", null, "Please enter the required information"),
              message("mtie1", null, "Please enter the required information"),
              message("mtie2", null, "Not required"),
              message("mtie5", null, "Not required"),
              message("mtpccn1", null, "Customs Client Number must be numeric and contain between 8 and 13 digits. If your number is 5 digits, please add 000 before your number i.e. 00012345"),
              message("mtpccn2", null, "Please enter a valid Customs client number"),
              message("mtpccn3", null, "Not required"),
              message("mtpccn4", null, "Not required"),
              message("mtpcde1", null, "Please enter the required information"),
              message("mtpcde2", null, "Not required"),
              message("mtpcde3", null, "Not required"),
              message("mtpcdf1", null, "Please enter the required information"),
              message("mtpcdf2", null, "Not required"),
              message("mtpcdf3", null, "Not required"),
              message("mtpcdn1", null, "Not required"),
              message("mtpcdn2", null, "Not required"),
              message("mtpcdn3", null, "Not required"),
              message("mtpcds1", null, "Please enter the required information"),
              message("mtpcds2", null, "Not required"),
              message("mtpcds3", null, "Not required"),
              message("mtpcdt1", null, "Please enter the required information"),
              message("mtpcdt2", null, "Not required"),
              message("mtpcdt3", null, "Not required"),
              message("mtpenm2", null, "Not required"),
              message("mtpenm3", null, "Not required"),
              message("mtpern1", null, "Not required"),
              message("mtpern2", null, "Registration number shouldn't be the same as the companies registration number"),
              message("mtpern3", null, "Not required"),
              message("mtpi1", null, "Please enter the required information"),
              message("mtpibd1", null, "Please enter the required information"),
              message("mtpibd2", null, "Please enter the required information"),
              message("mtpibd3", null, "Please enter the required information"),
              message("mtpibd4", null, "Not required"),
              message("mtpibd5", null, "Please enter the required information"),
              message("mtpibd6", null, "Please enter a valid date format: YYYY-MM-DD"),
              message("mtpibd7", null, "Not required"),
              message("mtpig1", null, "Please enter the required information"),
              message("mtpig2", null, "Please enter valid gender"),
              message("mtpig3", null, "Please enter the required information"),
              message("mtpig4", null, "Please enter the required information"),
              message("mtpig5", null, "Not required"),
              message("mtpig6", null, "Please enter the required information"),
              message("mtpig7", null, "Please enter the required information"),
              message("mtpiid1", null, "Please enter the required information"),
              message("mtpiid2", null, "Please enter the required information"),
              message("mtpiid3", null, "Please enter a valid ID number"),
              message("mtpiid4", null, "Not required"),
              message("mtpiid5", null, "Please enter the required information"),
              message("mtpiid6", null, "ID number shouldn't be the same as the applicant"),
              message("mtpiid7", null, "Not required"),
              message("mtpiid8", null, "Please enter the required information"),
              message("mtpinm1", null, "Please enter the required information"),
              message("mtpinm2", null, "Please enter the required information"),
              message("mtpinm3", null, "Please enter the required information"),
              message("mtpinm4", null, "Name shouldn't be the same as the applicant"),
              message("mtpinm5", null, "Not required"),
              message("mtpinm6", null, "Please enter the required information"),
              message("mtpinm7", null, "Not required"),
              message("mtpipc1", null, "Please enter the required information"),
              message("mtpipc2", null, "Not required"),
              message("mtpipc3", null, "Please enter a valid SWIFT code"),
              message("mtpipc4", null, "Not required"),
              message("mtpipn1", null, "Please enter the required information"),
              message("mtpipn3", null, "Not required"),
              message("mtpipn4", null, "Passport number shouldn't be the same as the applicant"),
              message("mtpipn5", null, "Not required"),
              message("mtpipn6", null, "Please enter the required information"),
              message("mtpisn1", null, "Please enter the required information"),
              message("mtpisn10", null, "Not required"),
              message("mtpisn2", null, "Not required"),
              message("mtpisn3", null, "Please enter the required information"),
              message("mtpisn4", null, "Please enter the required information"),
              message("mtpisn5", null, "Surname shouldn't be the same as the applicant"),
              message("mtpisn6", null, "Please enter the required information"),
              message("mtpisn7", null, "Not required"),
              message("mtpitp1", null, "Please enter the required information"),
              message("mtpitp2", null, "Not required"),
              message("mtpitp3", null, "Please enter the required information"),
              message("mtpitp4", null, "not required"),
              message("mtpitp5", null, "Temporary resident permit number shouldn't be the same as the applicants"),
              message("mtpitp6", null, "Not required"),
              message("mtpitp7", null, "Please enter the required information"),
              message("mtppac1", null, "Please enter the required information"),
              message("mtppac2", null, "Not required"),
              message("mtppac3", null, "Not required"),
              message("mtppal11", null, "Please enter the required information"),
              message("fe_mtppal11", null, "Please enter the required information"),
              message("mtppal12", null, "Not required"),
              message("mtppal14", null, "Not required"),
              message("mtppal21", null, "Not required"),
              message("mtppal22", null, "Please enter the required information"),
              message("mtppap1", null, "Please enter the required information"),
              message("mtppap2", null, "Not required"),
              message("mtppap3", null, "Please enter a valid province"),
              message("mtppap4", null, "Not required"),
              message("mtppas1", null, "Please enter the required information"),
              message("mtppas2", null, "not required"),
              message("mtppas3", null, "not required"),
              message("mtppaz1", null, "Please enter the required information"),
              message("mtppaz2", null, "not required"),
              message("mtppaz3", null, "Please enter a valid postal code"),
              message("mtppaz4", null, "not required"),
              message("mtpsac1", null, "Please enter the required information"),
              message("mtpsac2", null, "not required"),
              message("mtpsac3", null, "not required"),
              message("mtpsal11", null, "Please enter the required information"),
              message("fe_mtpsal11", null, "Please enter the required information"),
              message("mtpsal12", null, "not required"),
              message("mtpsal14", null, "not required"),
              message("mtpsal21", null, "not required"),
              message("mtpsal22", null, "not required"),
              message("mtpsap1", null, "Please enter the required information"),
              message("mtpsap2", null, "not required"),
              message("mtpsap3", null, "Please enter a valid province"),
              message("mtpsap4", null, "not required"),
              message("mtpsas1", null, "Please enter the required information"),
              message("mtpsas2", null, "not required"),
              message("mtpsas3", null, "not required"),
              message("mtpsaz1", null, "Please enter the required information"),
              message("mtpsaz2", null, "not required"),
              message("mtpsaz3", null, "Please enter a valid postal code"),
              message("mtpsaz4", null, "not required"),
              message("mtptx2", null, "not required"),
              message("mtptx3", null, "Tax number shouldn't be the same as the applicant"),
              message("mtptx5", null, "not required"),
              message("mtpvn1", null, "not required"),
              message("mtpvn2", null, "VAT number shouldn't be the same as the applicants"),
              message("mtpvn3", null, "Not required"),
              message("mtvl1", null, "Please add third party details for new Finsurv"),
              message("NonResident.Entity.AccountNumber.minLen", null, "Number entered is too short"),
              message("NonResident.Entity.Address.AddressLine1.minLen", null, "Address entered is too short"),
              message("NonResident.Entity.Address.AddressLine2.minLen", null, "Address entered is too short"),
              message("NonResident.Entity.Address.City.minLen", null, "City entered is too short"),
              message("NonResident.Entity.Address.Country.minLen", null, "Country code entered is too short"),
              message("NonResident.Entity.Address.PostalCode.minLen", null, "Code entered is too short"),
              message("NonResident.Entity.Address.State.minLen", null, "State entered is too short"),
              message("NonResident.Entity.Address.Suburb.minLen", null, "Suburb entered is too short"),
              message("NonResident.Entity.EntityName.minLen", null, "Name entered is too short"),
              message("NonResident.Individual.AccountNumber.minLen", null, "Number entered is too short"),
              message("NonResident.Individual.Address.AddressLine1.minLen", null, "Address entered is too short"),
              message("NonResident.Individual.Address.AddressLine2.minLen", null, "Address entered is too short"),
              message("NonResident.Individual.Address.City.minLen", null, "City entered is too short"),
              message("NonResident.Individual.Address.Country.minLen", null, "Country code entered is too short"),
              message("NonResident.Individual.Address.PostalCode.minLen", null, "Code entered is too short"),
              message("NonResident.Individual.Address.State.minLen", null, "State entered is too short"),
              message("NonResident.Individual.Address.Suburb.minLen", null, "Suburb entered is too short"),
              message("NonResident.Individual.PassportNumber.minLen", null, "Passport number entered is too short"),
              message("nr3", null, "Please enter the required information"),
              message("nr4", null, "Please enter the required information"),
              message("nr5", null, "Not required"),
              message("nr6", null, "This category is only allowed for individuals."),
              message("flw_CCnr6", null, "This category is only allowed for individuals."),
              message("nrcmc1", null, "Please enter the required information"),
              message("nrcmc2", null, "Not required"),
              message("nrcmn1", null, "Please enter the required information"),
              message("nrcmn2", null, "Not required"),
              message("nrcmn3", null, "No additional spaces allowed"),
              message("nrcmn4", null, "Please enter the required information"),
              message("nrex1", null, "Not required"),
              message("nrex2", null, "Not required"),
              message("nrgn1", null, "Please enter a valid gender"),
              message("nriaid13", null, "Please enter the required information"),
              message("nriaid14", null, "Please enter the required information"),
              message("nrial11", null, "Not required"),
              message("nrial21", null, "Not required"),
              message("nrial31", null, "Not required"),
              message("nrian2", null, "Company account number shouldn't be the same as match the beneficiaries"),
              message("nrian3", null, "Special characters ' or & are not alllowed"),
              message("nrian4", null, "Not required"),
              message("nric2", null, "Not required"),
              message("nrictry1", null, "Please enter the required information"),
              message("nrictry2", null, "Please enter a valid SWIFT country code"),
              message("nrictry5", null, "EU is not a valid county"),
              message("nrictry9", null, "Not required"),
              message("nris2", null, "Not required"),
              message("nriz2", null, "Not required"),
              message("nriz3", null, "Postal code length is too long"),
              message("nrlen1", null, "Please enter the required information"),
              message("nrlen2", null, "Not required"),
              message("nrlen4", null, "Not required"),
              message("nrlen5", null, "Please enter the required information"),
              message("nrnm1", null, "Please enter the required information"),
              message("nrpc2", null, "Please enter a valid country code"),
              message("nrpn2", null, "No additional spaces allowed"),
              message("nrsn1", null, "Please enter the required information"),
              message("nrsn3", null, "Please enter the required information"),
              message("obank2", null, "Not required"),
              message("ocntry1", null, "Please enter the required information"),
              message("ocntry2", null, "Not required"),
              message("ocntry5", null, "Please enter a valid SWIFT country code"),
              message("OriginatingBank.minLen", null, "Bank name entered is too short"),
              message("p2", null, "Not required"),
              message("p4", null, "Please enter a valid province"),
              message("p5", null, "Province is Namibia"),
              message("p6", null, "Province is Lesotho"),
              message("p7", null, "Province is Swaziland"),
              message("PaymentDetail.BeneficiaryBank.Address.minLen", null, "Address entered is too short"),
              message("PaymentDetail.BeneficiaryBank.BankName.minLen", null, "Bank name entered is too short"),
              message("PaymentDetail.BeneficiaryBank.BranchCode.minLen", null, "Branch code entered is too short"),
              message("PaymentDetail.BeneficiaryBank.City.minLen", null, "City entered is too short"),
              message("PaymentDetail.BeneficiaryBank.SWIFTBIC.len", null, "SWIFT BIC should be 11 characters"),
              message("PaymentDetail.CorrespondentBank.Address.minLen", null, "Address entered is too short"),
              message("PaymentDetail.CorrespondentBank.BankName.minLen", null, "Bank name entered is too short"),
              message("PaymentDetail.CorrespondentBank.BranchCode.minLen", null, "Branch code entered is too short"),
              message("PaymentDetail.CorrespondentBank.City.minLen", null, "City entered is too short"),
              message("PaymentDetail.CorrespondentBank.SWIFTBIC.minLen", null, "SWIFT BIC should be 11 characters"),
              message("pc1", null, "Please enter a valid postal code"),
              message("pc3", null, "Not required"),
              message("pc4", null, "The postal code for this province is 9999"),
              message("rbank2", null, "Not required"),
              message("rbank3", null, "Please enter the required information"),
              message("rcntry1", null, "Please enter the required information"),
              message("rcntry2", null, "Not required"),
              message("rcntry3", null, "Please enter a valid SWIFT code"),
              message("rcntry5", null, "Not required"),
              message("rcntry6", null, "Not required"),
              message("rcntry7", null, "Not required"),
              message("rec1", null, "Please enter the required information"),
              message("rec2", null, "Country cannot be a local country"),
              message("rec3", null, "Please enter a valid country code"),
              message("rec4", null, "Not required"),
              message("reic2", null, "Not required"),
              message("reic3", null, "Please enter a valid industrial classification code"),
              message("reis2", null, "Not required"),
              message("reis3", null, "Please enter a valid sector code"),
              message("relen1", null, "Please enter the required information"),
              message("relen2", null, "Not required"),
              message("relen3", null, "Not required"),
              message("ren10", null, "Not required"),
              message("ren13", null, "Not required"),
              message("ren14", null, "Not required"),
              message("ren15", null, "Not required"),
              message("ren17", null, "Not required"),
              message("ren18", null, "Not required"),
              message("ren19", null, "Not required"),
              message("ren20", null, "Not required"),
              message("ren21", null, "Not required"),
              message("ren22", null, "Not required"),
              message("ren23", null, "Not required"),
              message("ren24", null, "Not required"),
              message("ren25", null, "Not required"),
              message("ren26", null, "Not required"),
              message("ren27", null, "Not required"),
              message("ren3", null, "Not required"),
              message("ren4", null, "Not required"),
              message("ren5", null, "Not required"),
              message("ren6", null, "Please enter the required information"),
              message("ren7", null, "Not required"),
              message("ren8", null, "Not required"),
              message("ren9", null, "Not required"),
              message("repor1", null, "Please enter the required information"),
              message("repor2", null, "Please enter the required information"),
              message("repor3", null, "No additional spaces allowed"),
              message("repq1", null, "Please enter the required information"),
              message("repyn1", null, "Please enter the required information"),
              message("rern1", null, "Please enter the required information"),
              message("rern3", null, "Registration number shouldn't be the same as SARB registration number"),
              message("rern4", null, "No additional spaces allowed"),
              message("Resident.Entity.AccountName.minLen", null, "Name entered is too short"),
              message("Resident.Entity.AccountNumber.minLen", null, "Account number entered is too short"),
              message("Resident.Entity.ContactDetails.ContactName.minLen", null, "Name entered is too short"),
              message("Resident.Entity.ContactDetails.ContactSurname.minLen", null, "Surname must not match the applicant"),
              message("Resident.Entity.ContactDetails.Email.minLen", null, "Email entered is too short"),
              message("Resident.Entity.ContactDetails.Fax.minLen", null, "Fax number entered is too short"),
              message("Resident.Entity.ContactDetails.Telephone.minLen", null, "A minimum amount of 10 numbers is required"),
              message("Resident.Individual.ForeignIDCountry.minLen", null, "Country entered is too short"),
              message("Resident.Individual.ForeignIDNumber.minLen", null, "ID number entered is too short"),
              message("Resident.Individual.IDNumber.minLen", null, "ID number entered is too short"),
              message("Resident.Individual.PassportCountry.minLen", null, "Country entered is too short"),
              message("Resident.Individual.PassportNumber.minLen", null, "Passport number entered is too short"),
              message("Resident.Entity.StreetAddress.AddressLine1.minLen", null, "Address entered is too short"),
              message("Resident.Entity.StreetAddress.AddressLine2.minLen", null, "Address entered is too short"),
              message("Resident.Entity.StreetAddress.City.minLen", null, "City entered is too short"),
              message("Resident.Entity.StreetAddress.PostalCode.minLen", null, "Code entered is too short"),
              message("Resident.Entity.StreetAddress.State.minLen", null, "State entered is too short"),
              message("Resident.Entity.StreetAddress.Suburb.minLen", null, "Suburb entered is too short"),
              message("Resident.Entity.TaxClearanceCertificateReference.minLen", null, "Reference entered is too short"),
              message("Resident.Entity.TaxNumber.minLen", null, "Tax number entered is too short"),
              message("Resident.Entity.TempResPermitNumber.minLen", null, "Permit number is too short"),
              message("Resident.Entity.VATNumber.minLen", null, "VAT number entered is too short"),
              message("rg2", null, "Please enter the required information"),
              message("rg5", null, "Not required"),
              message("rg7", null, "Not required"),
              message("ridob1", null, "Please enter the required information"),
              message("ridob2", null, "Please enter a valid date of birth"),
              message("rifidc1", null, "Please enter the required information"), 
              message('tni1', null, "Please enter the required information"), 
              message('tne1', null, "Please enter the required information"),
              message('nrpn1', null, "Please enter the required information"),
              message('nrpc1', null, "Please enter the required information"),
              message('vne1', null, "Please enter the required information"),
              message('tne1', null, "Please enter the required information")  
            ]
          },
          {
            field: "OriginatingBank",
            rules: [
              ignore("obank1"),
              ignore("obank3"),
              ignore("obank4"),
              ignore("obank5"),
              ignore("obank6"),
              ignore("obank7"),
              ignore("obank8"),
              ignore("obank9")
            ]
          },
          {
            field: "ReceivingBank",
            rules: [
              ignore("rbank1"),
              ignore("rbank3"),
              ignore("rbank4"),
              ignore("rbank5"),
              ignore("rbank6"),
              ignore("rbank7"),
              ignore("rbank8"),
              ignore("rbank9")
            ]
          },
          {
            field: "CorrespondentBank",
            rules: [
              ignore("cbank1")
            ]
          },
          {
            field: "ReceivingCountry",
            rules: [
              ignore("rcntry4")
            ]
          },
          {
            field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
            rules: [
              ignore("nriaid1"),
              ignore("nriaid4"),
              ignore("nriaid5"),
              ignore("nriaid6"),
              ignore("nriaid7"),
              ignore("nriaid9"),
              ignore("nriaid10"),
              ignore("nriaid11")
            ]
          },
          {
            field: "Total{{LocalValue}}",
            rules: [
              ignore("tlv1")
            ]
          }
        ]
      }
    };
    return transaction;
  }
});

