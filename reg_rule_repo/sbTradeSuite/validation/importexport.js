define(function () {
  return function (predef) {
    var stdImportExport;
    with (predef) {
      stdImportExport = {
        ruleset: "Standard Import/Export Rules",
        scope: "importexport",
        validations: [
          {
            field: "ImportControlNumber",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("ieicn1", 495, "Please enter the required information",
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", ["SDA", "REMITTANCE DISPENSATION"]))).onOutflow().onSection("A").notOnCategory("103/11").onCategory(["101", "103", "105", "106"]),
              warning("ieicn2", 496, "Not required",
                notEmpty).onSection("ABG").notOnCategory(["101", "103", "105", "106"]),
              // also included in iecn3 (497: If the Flow is OUT and the BoPCategory and SubBopCategory is or 101/01 to 101/10 the first 3 characters must be INV)
              warning("ieicn3", 499, "Please enter a valid Import control number: INV followed by number, minimum 4 characters",
                notPattern(/^INV.+$/)).onOutflow().onSection("ABG").onCategory("101").notOnCategory("101/11"),
              // also included in iecn3 (498: If the Flow is OUT and the category is 103/01 to 103/10 or 105 or 106, the first 3 characters must be a valid customs office code)
              warning("ieicn4", 499, "Please enter the format as follows: AAACCYYMMDD0000000 where AAA is a valid customs office code in alpha format; CC is the century of import, YY is the year of import, MM is the month of import, DD is the day of import, and 0000000 is the 7 digit unique bill of entry number allocated by SARS as part of the MRN",
                notValidICN).onOutflow().onSection("ABG").notOnCategory("103/11").onCategory(["103", "105", "106"]),
              //500: If the Flow is OUT and the category is 103/01 to 103/10or 105 or 106, the number supplied must be a valid MRN stored in the IVS.
              warning("ieicn5", 219, "No additional spaces or comma's (,) allowed",
                notEmpty.and(hasAdditionalSpaces.or(hasPattern(/,/)))).onSection("ABG"),
              warning("ieicn6", 495, "Please enter the required information",
                isEmpty).onOutflow().onSection("BG").notOnCategory("103/11").onCategory(["101", "103", "105", "106"])
            ]
          },
          {
            field: "TransportDocumentNumber",
            minLen: 2,
            maxLen: 35,
            rules: [
              warning("ietdn1", 502, "Please enter the required information",
                isEmpty.and(notMoneyFieldValue("AdHocRequirement.Subject", ["SDA", "REMITTANCE DISPENSATION"]))).onOutflow().notOnCategory("103/11").onCategory("103").onSection("ABG"),
              warning("ietdn2", 503, "Not required",
                notEmpty).onSection("ABG").notOnCategory(["103", "105", "106"]),
              warning("ietdn3", 502, "Please enter the required information",
                isEmpty).onOutflow().notOnCategory("103/11").onCategory("103").onSection("BG"),
              warning("ietdn4", 503, "Not required",
                notEmpty.and(importUndertakingClient)).onCategory(["105", "106"]).onSection("ABG")
            ]
          },
          {
            field: "UCR",
            minLen: 2,
            maxLen: 35,
            rules: [
              // TODO: A format check is needed here
              warning("ieucr1", 504, "If UCR contains a value and the Flow is IN, the minimum characters is 12 but not exceeding 35 characters",
                notEmpty.and(isTooShort(12).or(isTooLong(35)))).onInflow().onSection("ABG"),
              warning("ieucr2", 505, "Must be completed if the Flow is IN and the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106",
                isEmpty).onSection("ABG").onInflow().notOnCategory(['101/11', '103/11']).onCategory(['101', '103', '105', '106']),
              // NOTE: Updated the message. Original -> UCR must not be completed
              warning("ieucr3", 506, "For any BoPCategory and SubBopCategory other than 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, the UCR must not be completed.",
                notEmpty).onSection("ABG").onInflow().notOnCategory(["101/01", "101/02", "101/03", "101/04", "101/05", "101/06", "101/07", "101/08", "101/09", "101/10", "103/01", "103/02", "103/03", "103/04", "103/05", "103/06", "103/07", "103/08", "103/09", "103/10", "105", "106"]),
              // NOTE: Couldn't find more than one 506 in the spec. Is this correct?
              warning("ieucr4", 506, "Not required",
                notEmpty).onSection("ABG").onOutflow()
              // 514: Invalid customs client number completed in UCR
              // NOTE: There can be invalid-Valid CCNs, therefore this should be handled by an external call
              //warning('ieucr5', 514, "Invalid customs client number completed in UCR",
              //  notEmpty.and(notValidCCNinUCR)).onSection("ABG").onInflow(),
              //warning('ieucr6', "UC1", "The CCN in the UCR should either match the Resident's CCN or the 3rd Party's CCN",
              // notEmpty.and(notMatchCCNinUCR)).onSection("ABG").onInflow()
              //warning('ieucr7', "UC1","The CCN captured against the Account Holder or the CNN on the Third Party Details must match the CCN in the UCR",
              //  notEmpty.and(notMatchCCNinUCR)).onSection("ABG").onInflow()
            ]
          },
          {
            field: "PaymentCurrencyCode",
            len: 3,
            rules: [
              warning("iepcc1", 530, "Please enter the required information",
                isEmpty).onSection("ABG"),
              warning("iepcc2", 531, "The currency code should match the transaction currency",
                notEmpty.and(notValidImportExportCurrency)).onSection("ABG")
            ]
          },
          {
            field: "PaymentValue",
            rules: [
              // NOTE: Updated the message, but there is no check for 0.00 or 0
              warning("iepv1", 507, "Please enter the required information",
                isEmpty).onSection("ABG")
            ]
          },
          {
           field: "MRNNotOnIVS",
           rules: [
             warning("iemrn1", 203, "The value must only be Y or N",
               notEmpty.and(notValueIn(["Y", "N"]))).onSection("ABG")
           ]
          }
        ]
      };
    }

    return stdImportExport;
  }
});
