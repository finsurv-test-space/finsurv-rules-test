define(function () {

  return function (predef) {

   var display;

   with (predef) {
     display = {
       detailTrans: {
         ruleset: "Flow Transaction Display Rules",
         scope: "transaction",
         fields: [
           {
             field: "TrnReference",
             display: [
               setValue(function() {
                  // from https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
                 var cryptoObj = window.crypto || window.msCrypto;
                 //[1e7]+-1e3+-4e3+-8e3+-1e11
                 return ([1e7]+-1e3+-4e3+-8e3+-1e5).replace(/[018]/g, function(c) {
                     return (c ^ cryptoObj.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
                   });
               }, isEmpty)
             ]
           },
           {
            field: "TotalForeignValue",
            display: [
              disable(),
              hide(not(hasResidentField("CustomsClientNumber")))
            ]
          },
          {
            field: "ReportingQualifier",
            display: [
              disable(),
              setValue('%s', function (context, value) {
                context.section = "A";
                context.buildCategories();
                return "BOPCUS";
              }).notOnCategory(['ZZ1', 'ZZ2']).onSection("C"),
              setValue('%s', function (context, value) {
                context.section = "C";
                context.buildCategories();
                return "NON REPORTABLE";
              }).onCategory(['ZZ1']).onSection("ABDEFG")
            ]
          },
          {
            field: "Flow",
            display: [
              disable()
            ]
          },
          {
            field: "ValueDate",
            display: [
              show(),
              disable()
            ]
          },
          {
            field: "FlowCurrency",
            display: [
              show(),
              disable()
            ]
          },
        {
          field  : "TotalValue",
          display: [
            hide(),
            show(isDefined),
            disable()
          ]
        },
          {
            field: "BranchCode",
            display: [
            show()
            ]
          },
          {
            field: "BranchName",
            display: [
            show()
            ]
          },
          {
            field: "HubCode",
            display: [
            show()
            ]
          },
          {
            field: "HubName",
            display: [
            show()
            ]
          },
          {
            field: "OriginatingBank",
            display: [
            hide(),
              show().onSection("ABCDG"),
              show(notNonResidentFieldValue("AccountIdentifier", "CASH")).onInflow().onSection("ABCDG"),
              show(hasTransactionField("OriginatingCountry")).onInflow().onSection("ABCDG")
            ]
          },
          {
            field: "OriginatingCountry",
            display: [
            hide(),
              show().onSection("ABCDG"),
              show(notNonResidentFieldValue("AccountIdentifier", "CASH")).onInflow().onSection("ABCDG"),
              show(hasTransactionField("OriginatingBank")).onInflow().onSection("ABCDG")
            ]
          },
          {
            field: "CorrespondentBank",
            display: [
              show().onSection("ABCDG"),
              hide().onSection("EF")
            ]
          },
          {
            field: "CorrespondentCountry",
            display: [
              show().onSection("ABCDG"),
              hide().onSection("EF")
            ]
          },
          {
            field: "ReceivingBank",
            display: [
            hide().onSection("EF"),
              hide(hasResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["250", "251"]),
              hide(hasNonResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["255", "256"]),
            ]
          },
          {
            field: "ReceivingCountry",
            display: [
            hide().onSection("EF"),
              hide(hasResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["250", "251"]),
              hide(hasNonResException("MUTUAL PARTY")).onOutflow().onSection("AB").onCategory(["255", "256"]),
              show(hasTransactionField("ReceivingBank")).onOutflow().onSection("ABCDG")
            ]
          },
          {
            field: "NonResident",
            display: [
              setValue(undefined).onSection("F")
            ]
          },
          {
            field: "Resident",
            display: [
              setValue(undefined).onSection("F")
            ]
          },
          {
            field: "NonResident.Individual.Surname",
            display: [
              show().onSection("ABCDG")
            ]
          },
          {
            field: "NonResident.Individual.Name",
            display: [
              show().onSection("ABCDG")
            ]
          },
          {
            field: "NonResident.Individual.Gender",
            display: [
              show().onSection("ABCDG")
            ]
          },
          {
            field: "NonResident.Individual.PassportNumber",
            display: [
              show().onSection("AB").onCategory(["250", "251"]),
              hide().onSection("CEF")
            ]
          },
          {
            field: "NonResident.Individual.PassportCountry",
            display: [
              show().onSection("AB").onCategory(["250", "251"]),
              hide().onSection("CEF")
            ]
          },
          {
            field: "NonResident.Entity.EntityName",
            display: [
              show().onSection("ABCDG"),
              hide().onSection("E")
            ]
          },
          {
            field: "NonResident.Entity.CardMerchantName",
            display: [
              show().onSection("E"),
              hide().onSection("ABCDG")
            ]
          },
          {
            field: "NonResident.Entity.CardMerchantCode",
            display: [
              show().onSection("E"),
              hide().onSection("ABCDG")
            ]
          },
          {
            field: "NonResident.Exception",
            display: [
              hide(hasTransactionField("Resident.Exception")).onSection("A"),
              hide().onSection("BEG")
            ]
          },
          {
            field: "NonResident.Exception.ExceptionName",
            display: [
              hide(hasTransactionField("Resident.Exception")).onSection("A"),
              hide().onSection("BEG"),
              setValue('MUTUAL PARTY', null, isEmpty).onSection('A').onInflow().onCategory("252")
            ]
          },
          {
            field: "NonResident.Individual.AccountIdentifier",
            display: [
              setValue("NON RESIDENT RAND", null, isEmpty).onSection("B")
            ]
          },
          {
            field: ["NonResident.Individual.AccountNumber", "NonResident.Entity.AccountNumber"],
            display: [
              show()
            ]
          },
          {
            field: [

              "NonResident.Individual.Address.AddressLine1",
              "NonResident.Individual.Address.AddressLine2",
              "NonResident.Individual.Address.Suburb",
              "NonResident.Individual.Address.City",
              "NonResident.Individual.Address.State",
              "NonResident.Individual.Address.PostalCode",
              "NonResident.Entity.Address.AddressLine1",
              "NonResident.Entity.Address.AddressLine2",
              "NonResident.Entity.Address.Suburb",
              "NonResident.Entity.Address.City",
              "NonResident.Entity.Address.State",
              "NonResident.Entity.Address.PostalCode"
            ],
            display: [
              hide().onSection("E")
            ]
          },
          {
            field: ["NonResident.Individual.Address.Country", "NonResident.Entity.Address.Country"],
            display: [
              show().onSection("ABCDEG")
            ]
          },
          {
            field: "Resident.Individual.Surname",
            display: [
              show().onSection("ABCEG")
            ]
          },
          {
            field: "Resident.Individual.Name",
            display: [
              show().onSection("ABCEG")
            ]
          },
          {
            field: "Resident.Individual.Gender",
            display: [
              show().onSection("ABEG")
            ]
          },
          {
            field: "Resident.Individual.DateOfBirth",
            display: [
              show().onSection("ABEG")
            ]
          },
          {
            field: "Resident.Individual.IDNumber",
            display: [
              show().onSection("ABEG")
            ]
          },
          {
            field: "Resident.Individual.BeneficiaryID1",
            display: [
              hide()
            ]
          },
          {
            field: "Resident.Individual.BeneficiaryID2",
            display: [
              hide()
            ]
          },
          {
            field: "Resident.Individual.BeneficiaryID3",
            display: [
              hide()
            ]
          },
          {
            field: "Resident.Individual.BeneficiaryID4",
            display: [
              hide()
            ]
          },
          {
            field: "Resident.Individual.TempResPermitNumber",
            display: [
              show().onSection("ABEG"),
              hide().onSection("AB").onCategory(["511", "512", "513", "514", "515"]),
              hide().onOutflow().onSection("AB").onCategory("401")
            ]
          },
          {
            field: "Resident.Individual.ForeignIDNumber",
            display: [
              show().onSection("ABEG"),
              hide().onSection("AB").onCategory(["511", "512", "513", "514", "515"]),
              hide().onOutflow().onSection("AB").onCategory("401")
            ]
          },
          {
            field: "Resident.Individual.ForeignIDCountry",
            display: [
              show().onSection("ABEG"),
              hide().onSection("AB").onCategory(["511", "512", "513", "514", "515"]),
              hide().onOutflow().onSection("AB").onCategory("401")
            ]
          },
          {
            field: "Resident.Individual.PassportNumber",
            display: [
              show().onSection("AB"),
              hide().onSection("CDEFG"),
              hide().onSection("AB").onCategory("255")
            ]
          },
          {
            field: "Resident.Individual.PassportCountry",
            display: [
              show().onSection("AB"),
              hide().onSection("CDEFG"),
              hide().onSection("AB").onCategory("255")
            ]
          },
          {
            field: "Resident.Entity.EntityName",
            display: [
              hide().onSection("F")
            ]
          },
          {
            field: "Resident.Entity.TradingName",
            display: [
              hide().onSection("F")
            ]
          },
          {
            field: "Resident.Entity.RegistrationNumber",
            display: [
              hide().onSection("F")
            ]
          },
          {
            field: "Resident.Entity.InstitutionalSector",
            display: [
              hide().onSection("F")
            ]
          },
          {
            field: "Resident.Entity.IndustrialClassification",
            display: [
              hide().onSection("F")
            ]
          },
          {
            field: "Resident.Exception",
            display: [
              hide().onSection("BEF")
            ]
          },
          {
            field: "Resident.Exception.ExceptionName",
            display: [
              hide().onSection("BEF")
            ]
          },
          {
            field: "Resident.Exception.Country",
            display: [
              hide().onSection("ABEFG")
            ]
          },
          {
            field: ["Resident.Individual", "Resident.Entity"],
            display: [
              show()
            ]
          },
          {
            field: ["Resident.Individual.AccountName", "Resident.Entity.AccountName"],
            display: [
              show()
            ]
          },
          {
            field: ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
            display: [
              hide().onSection("F")
            ]
          },
          {
            field: ["Resident.Individual.AccountNumber", "Resident.Entity.AccountNumber"],
            display: [
              hide().onSection("F")
            ]
          },
          {
            field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],
            display: [
              show()
              // setValue(undefined, null, isEmpty)
            ]
          },
          {
            field: ["Resident.Individual.TaxNumber", "Resident.Entity.TaxNumber"],
            display: [
              hide().onSection("F"),
            ]
          },
          {
            field: ["Resident.Individual.VATNumber", "Resident.Entity.VATNumber"],
            display: [
              hide().onSection("F"),   
			      ]
          },
          {
            field: ["Resident.Individual.TaxClearanceCertificateIndicator", "Resident.Entity.TaxClearanceCertificateIndicator"],
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(['512', '513'])
            ]
          },
          {
            field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(['512', '513']),
              show(hasResidentFieldValue('TaxClearanceCertificateIndicator', 'Y')).onSection("AB")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.AddressLine1", "Resident.Entity.StreetAddress.AddressLine1", "Resident.Individual.PostalAddress.AddressLine1", "Resident.Entity.PostalAddress.AddressLine1"],
            display: [
              hide().onSection("DF")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.AddressLine2", "Resident.Entity.StreetAddress.AddressLine2", "Resident.Individual.PostalAddress.AddressLine2", "Resident.Entity.PostalAddress.AddressLine2"],
            display: [
              hide().onSection("DF")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.Suburb", "Resident.Entity.StreetAddress.Suburb", "Resident.Individual.PostalAddress.Suburb", "Resident.Entity.PostalAddress.Suburb"],
            display: [
              hide().onSection("DF")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.City", "Resident.Entity.StreetAddress.City", "Resident.Individual.PostalAddress.City", "Resident.Entity.PostalAddress.City"],
            display: [
              hide().onSection("DF")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province", "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
            display: [
              hide().onSection("DF")
            ]
          },
          {
            field: ["Resident.Individual.StreetAddress.PostalCode", "Resident.Entity.StreetAddress.PostalCode"],
            display: [
              hide().onSection("DF")
            ]
          },
          {
            field: ["Resident.Individual.PostalAddress.PostalCode", "Resident.Entity.PostalAddress.PostalCode"],
            display: [
              hide().onSection("DF")
            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.ContactSurname", "Resident.Entity.ContactDetails.ContactSurname", "Resident.Individual.ContactDetails.ContactName", "Resident.Entity.ContactDetails.ContactName"],
            display: [
              hide().onSection("DF")
            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.Email", "Resident.Entity.ContactDetails.Email"],
            display: [
              hide().onSection("DF")
            ]
          },
          {
            field: ["Resident.Individual.ContactDetails.Fax",
              "Resident.Entity.ContactDetails.Fax",
              "Resident.Individual.ContactDetails.Telephone",
              "Resident.Entity.ContactDetails.Telephone"],
            display: [
              hide().onSection("DF")
            ]
          },
          {
            field: ["Resident.Individual.CardNumber", "Resident.Entity.CardNumber"],
            display: [
              hide().onSection("ABCDFG")
            ]
          },
          {
            field: ["Resident.Individual.SupplementaryCardIndicator", "Resident.Entity.SupplementaryCardIndicator"],
            display: [
              hide().onSection("ABCDFG")
            ]
          }
         ]
       },
       detailMoney: {
         ruleset: "Flow Money Display Rules",
         scope: "money",
         fields: [
          {
            field: "SequenceNumber",
            display: [
              show(),
              setValue(function(context, moneyInd){
                return Number(moneyInd)+1;
              }, isEmpty)
            ]
          },
          {
            field: "MoneyTransferAgentIndicator",
            display: [
              show()
            ]
          },
          {
            field: "{{LocalValue}}",
            display: [
            show().onSection("ABCDEG"),
            disable(isCurrencyIn(map('LocalCurrency'))).onSection("ABCDEG"),
            setValue('%s','ForeignValue',isCurrencyIn(map('LocalCurrency'))).onSection("ABCDEG"),
              hide().onSection("F")
            ]
          },
          {
            field: "ForeignValue",
            display: [
              hide().onSection("F"),
              setValue('%s', 'money::{{LocalValue}}',isEmpty.and(hasMoneyField(map('LocalValue'))).and(isCurrencyIn(map('LocalCurrency')))).onSection("ABCDEG")
            ]
          },
        {
         field  : "DomesticCurrencyCode",
         display: [
           hide(),
           setValue(map('LocalCurrency')).onSection('ABCDEG'),
           setValue(undefined).onSection('F')
         ]
        },
        {
          field: "CompoundCategoryCode",
          display: [
            show(),
            hide(not(hasResidentField("CustomsClientNumber")))
          ]
        },
          {
            field: "CategoryCode",
            display: [
              show(),
              // hide(not(hasResidentField("CustomsClientNumber")))
              hide(not(evalTransactionField("Resident.Individual.CustomsClientNumber", hasPattern(/^\d{8,13}$/))
                .or(evalTransactionField("Resident.Entity.CustomsClientNumber", hasPattern(/^\d{8,13}$/)))))
            ]
          },
          {
            field: "CategorySubCode",
            display: [
              show(),
              //hide(not(hasResidentField("CustomsClientNumber")))
              hide(not(evalTransactionField("Resident.Individual.CustomsClientNumber", hasPattern(/^\d{8,13}$/))
                .or(evalTransactionField("Resident.Entity.CustomsClientNumber", hasPattern(/^\d{8,13}$/)))))
            ]
          },
          {
            field: "CategoryDescription",
            display : [
              hide(not(evalTransactionField("Resident.Individual.CustomsClientNumber", hasPattern(/^\d{8,13}$/))
                .or(evalTransactionField("Resident.Entity.CustomsClientNumber", hasPattern(/^\d{8,13}$/))))),
              setValue(function (data, ind) {
                var money = data.MonetaryAmount[ind];
                var cat = money.CategoryCode +
                  (money.CategorySubCode ? '/' + money.CategorySubCode : '');
                if (cat) {
                  cat = cat.toUpperCase();
                  if (cat == 'ZZ1' || cat == 'ZZ2') {
                    // special case
                    return 'Transfer';
                  }
                  var categories = data.getLookups().categories;
                  var thisCat = _.find(categories, function (itm) {
                    return itm.code == cat && itm.flow == data.Flow;
                  })
                  if (thisCat) {
                    //TODO: Check the meta section for custom category description (IE: variable assignment: altDesc = $document.customDesc[thisCat]
                    //  ... OR Use meta section with custom function lookup...)
                    return thisCat.description + ' (' + thisCat.section + (thisCat.subsection ? ', ' + thisCat.subsection : '') + ')';
                  }
                }
              })
            ]
          },
          {
            field: "SWIFTDetails",
            display: [
              show()
            ]
          },
          {
            field: "StrateRefNumber",
            display: [
              hide(),
              show(hasResException("STRATE")).onSection("AB"),
              show().onSection("AB").onCategory(["601/01", "603/01"])
            ]
          },
          {
            field: "Travel",
            display: [
              hide()
            ]
          },
          {
            field: "Travel.Surname",
            display: [
              hide()
            ]
          },
          {
            field: "Travel.Name",
            display: [
              hide()
            ]
          },
          {
            field: "Travel.IDNumber",
            display: [
              hide()
            ]
          },
          {
            field: "Travel.DateOfBirth",
            display: [
              hide()
            ]
          },
          {
            field: "Travel.TempResPermitNumber",
            display: [
              hide()
            ]
          },
          {
            field: "LoanRefNumber",
            display: [
              hide(),
              show().onSection("AB").onCategory(["801", "802", "803", "804"]),
              show().onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              setValue("99012301230123", null,
                isEmpty.and(hasMoneyFieldValue("LocationCountry", "LS"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
              setValue("99456745674567", null,
                isEmpty.and(hasMoneyFieldValue("LocationCountry", "SZ"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
              setValue("99789078907890", null,
                isEmpty.and(hasMoneyFieldValue("LocationCountry", "NA"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
              setValue("99012301230123", null,
                isEmpty.and(hasMoneyFieldValue("LocationCountry", "LS"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              setValue("99456745674567", null,
                isEmpty.and(hasMoneyFieldValue("LocationCountry", "SZ"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"]),
              setValue("99789078907890", null,
                isEmpty.and(hasMoneyFieldValue("LocationCountry", "NA"))).onOutflow().onSection("AB").onCategory(["106", "309/04", "309/05", "309/06", "309/07"])
            ]
          },
          {
            field: "LoanTenor",
            display: [
            hide(),
            show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"])//,
           // hide().onSection("CDEF")
            ]
          },
          {
            field: "LoanInterestRate",
            display: [
              hide(),
              show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
              show().onOutflow().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
              show().onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"])
            ]
          },
          {
            field: "{{Regulator}}Auth.RulingsSection",
            display: [
              hide().onSection("CDEF"),
            hide().onSection("ABG").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"])
            ]
          },
          {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
            display: [
              hide().onSection("CDEF"),
              hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              show(hasMoneyField(map("{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate")).and(hasMoneyField(map('CategoryCode')))).onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              hide(not(hasResidentField("CustomsClientNumber")).or(not(hasMoneyField(map('CategoryCode'))))),
              setValue(undefined, hasMoneyField(map("{{Regulator}}Auth.AuthIssuer")).and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "SARB")))
            ]
          },
          {
            field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumberDate",
            display: [
              hide().onSection("CDEF"),
              hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              show(hasMoneyField(map("{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber"))).onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              hide(not(hasResidentField("CustomsClientNumber")).or(not(hasMoneyField(map('CategoryCode'))))),
              setValue(undefined, hasMoneyField(map("{{Regulator}}Auth.AuthIssuer")).and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "SARB")))
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
            display: [
              hide().onSection("CDEF"),
              hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              hide(not(hasResidentField("CustomsClientNumber")).or(not(hasMoneyField(map('CategoryCode'))))),
              setValue(undefined, hasMoneyField(map("{{Regulator}}Auth.AuthIssuer")).and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "Bank")))
            ]
          },
          {
            field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
            display: [
              hide().onSection("CDEF"),
              hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              hide(not(hasResidentField("CustomsClientNumber")).or(not(hasMoneyField(map('CategoryCode'))))),
              setValue(undefined, hasMoneyField(map("{{Regulator}}Auth.AuthIssuer")).and(hasMoneyFieldValue(map("{{Regulator}}Auth.AuthIssuer"), "Bank")))
            ]
          },
          {
            field: "{{Regulator}}Auth.AuthIssuer",
            display: [
              hide(not(hasMoneyField('RegulatorAuth.AuthFacilitator')).or(hasMoneyFieldValue("RegulatorAuth.AuthFacilitator", "-- None --"))),
              setValue(undefined, not(hasMoneyField(map('{{Regulator}}Auth.AuthFacilitator'))).or(hasMoneyFieldValue(map('{{Regulator}}Auth.AuthFacilitator'), '-- None --'))),
              setValue(undefined, hasMoneyFieldValue(map('{{Regulator}}Auth.AuthFacilitator'), 'Standard Bank')
                .and(hasMoneyFieldValue(map('{{Regulator}}Auth.AuthIssuer'), 'Another Bank'))),
              setValue(undefined, hasMoneyFieldValue(map('{{Regulator}}Auth.AuthFacilitator'), 'Another Bank')
                .and(hasMoneyFieldValue(map('{{Regulator}}Auth.AuthIssuer'), 'Standard Bank')))
            ]
          },
          {
            field: "CannotCategorize",
            display: [
              hide(),
              show().onSection("AB").onCategory("830")
            ]
          },
          {
            field: "AdHocRequirement.Subject",
            display: [
              hide().onSection("CDEF")
            ]
          },
          {
            field: "AdHocRequirement.Description",
            display: [
              hide().onSection("CDEF")
            ]
          },
          {
            field: "LocationCountry",
            display: [
              hide().onSection("CDE")
            ]
          },
          {
            field: "ReversalTrnRefNumber",
            display: [
              hide(),
              show().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              show(hasMoneyFieldValue("CardChargeBack", "Y")).onSection("E")
            ]
          },
          {
            field: "ReversalTrnSeqNumber",
            display: [
              hide(),
              show().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
              show(hasMoneyField('ReversalTrnRefNumber')).onSection("E")
            ]
          },
          {
            field: "BOPDIRTrnReference",
            display: [
              hide().onSection("ABCDEF")
            ]
          },
          {
            field: "BOPDIR{{DealerPrefix}}Code",
            display: [
              hide().onSection("ABCDEF")
            ]
          },
          {
            field: "ThirdParty.Individual.Surname",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.Individual.Name",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.Individual.Gender",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.Individual.IDNumber",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.Individual.DateOfBirth",
            display: [
              show()
            ]
          },
          {
            field: ["ThirdParty.Individual.TempResPermitNumber","ThirdParty.Individual.TempResExpiryDate"],
            display: [
              show(),
              hide().onSection("AB").onCategory(["511", "512", "513", "516"]),
            ]
          },
          {
            field: "ThirdParty.Individual.PassportNumber",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.Individual.PassportCountry",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.Entity.Name",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.Entity.RegistrationNumber",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.CustomsClientNumber",
            display: [
              hide().onSection("CDEF")
            ]
          },
          {
            field: "ThirdParty.TaxNumber",
            display: [
              hide().onSection("CDEF")
            ]
          },
          {
            field: "ThirdParty.VATNumber",
            display: [
              hide().onSection("CDEF")
            ]
          },
          {
            field: "ThirdParty.StreetAddress.AddressLine1",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.StreetAddress.AddressLine2",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.StreetAddress.Suburb",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.StreetAddress.City",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.StreetAddress.Province",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.StreetAddress.PostalCode",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.PostalAddress.AddressLine1",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.PostalAddress.AddressLine2",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.PostalAddress.Suburb",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.PostalAddress.City",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.PostalAddress.Province",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.PostalAddress.PostalCode",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.ContactDetails.ContactSurname",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.ContactDetails.ContactName",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.ContactDetails.Email",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.ContactDetails.Fax",
            display: [
              show()
            ]
          },
          {
            field: "ThirdParty.ContactDetails.Telephone",
            display: [
              show()
            ]
          },
          {
            field: "CardChargeBack",
            display: [
              hide().onSection("ABCDFG")
            ]
          },
          {
            field: "CardIndicator",
            display: [
              hide().onSection("ABCDG")
            ]
          },
          {
            field: "ElectronicCommerceIndicator",
            display: [
              hide().onSection("ABCDFG")
            ]
          },
          {
            field: "POSEntryMode",
            display: [
              hide().onSection("ABCDFG")
            ]
          },
          {
            field: "CardFraudulentTransactionIndicator",
            display: [
              hide().onSection("ABCDFG")
            ]
          },
          {
            field: "ForeignCardHoldersPurchases{{LocalValue}}",
            display: [
              hide().onSection("ABCDEG")
            ]
          },
          {
            field: "ForeignCardHoldersCashWithdrawals{{LocalValue}}",
            display: [
              hide().onSection("ABCDEG")
            ]
          },
          {
            field: "ImportExport",
            display: [
            hide(emptyImportExport),
              show().onSection("AB").onCategory(["101", "103", "105", "106"])
            ]
          },
          {
            field: "IECurrencyCode",
            display: [
              setValue('%s','transaction::FlowCurrency',not(isDefined))
            ]
          },
          // The four heading fields below don't actually exist as fields but rather are used
          //  for checking if the headings in the import/export table should be displayed or hidden.
          {
            field: "IE_ICNHeading",
            display: [
              hide(),
              show().onOutflow().onSection("ABG").onCategory(["101", "103", "105", "106"]),
              setLabel("Import Control Number (or MRN)"),
              setLabel("Invoice Number (or MRN)").onOutflow().onSection("ABG").onCategory("101"),
              setLabel("Movement Reference Number (MRN)").onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
            ]
          },
          {
            field: "IE_TDNHeading",
            display: [
              hide(),
              show().onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
            ]
          },
          {
            field: "IE_UCRHeading",
            display: [
              hide(),
              show().onInflow().onSection("ABG").onCategory(["101", "103", "105", "106"])
            ]
          },
          {
            field: "IE_MRNNotOnIVSHeading",
            display: [
              hide(),
              show().onOutflow().onSection("ABG").onCategory(["101", "103", "105", "106"])
            ]
          }
         ]
       },
       detailImportExport: {
         ruleset: "Flow Import/Export Display Rules",
         scope: "importexport",
         fields: [
          {
            field: "ImportControlNumber",
            display: [
              hide(isEmpty),
              show().onOutflow().onSection("ABG").onCategory(["101", "103", "105", "106"]),
              setValue("INV", null, notPattern(/^INV.+$/)).onOutflow().onSection("ABG").onCategory("101"),
              setValue("", null, hasValue("INV")).onOutflow().onCategory(["102", "103", "104", "105", "106"])
            ]
          },
          {
            field: "ImportControlNumberLabel",
            display: [
              setValue("Import control number", null, isEmpty),
              setValue("Invoice number", null, isEmpty).onOutflow().onSection("ABG").onCategory("101"),
              setValue("Movement Reference Number", null, isEmpty).onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
            ]
          },
          {
            field: "TransportDocumentNumber",
            display: [
            hide(isEmpty),
            show().onOutflow().onSection("ABG").onCategory(["103", "105", "106"]),
            ]
          },
          {
            field: "MRNNotOnIVS",
            display: [
            hide(isEmpty),
              show().onOutflow().onSection("ABG").onCategory(["101", "103", "105", "106"])
            ]
          },
          {
            field: "UCR",
            display: [
            hide(isEmpty),
              show().onInflow().onSection("ABG").onCategory(["101", "103", "105", "106"])
            ]
          },
          {
            field: "PaymentCurrencyCode",
            display: [
              show(),
              setValue('%s','money::IECurrencyCode',returnTrue)
            ]
          },
          {
            field: "PaymentValue",
            display: [
              show()
            ]
          }
        ]
       }
     };
   }

   return display;
 }
});