define(function () {
  return function (predef) {
    var filterLookupRules;
    with (predef) {
      filterLookupRules = {
        filterLookupTrans: {
          ruleset: "Reporting Transaction Lookup Filter Rules",
          scope: "transaction",
          fields: []
        },

        filterLookupMoney: {
          ruleset: "Reporting Monetary Lookup Filter Rules",
          scope: "money",
          fields: [
            {
              field: "CompoundCategoryCode",
              display: [
                limitValue(['102'], evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')).and(hasCustomValue("PaymentType", "ADVANCE_PAYMENT"))),
                limitValue(['101'], not(evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn'))).and(hasCustomValue("PaymentType", "ADVANCE_PAYMENT"))),
                limitValue(['104'], evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')).and(hasCustomValue("PaymentType", "BALANCE_PAYMENT"))),
                limitValue(['104'], evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')).and(hasCustomValue("PaymentType", "OPEN_ACCOUNT"))),
                limitValue(['103'], not(evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn'))).and(hasCustomValue("PaymentType", "BALANCE_PAYMENT"))),
                limitValue(['103'], not(evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn'))).and(hasCustomValue("PaymentType", "OPEN_ACCOUNT"))),
                limitValue(['101', '103'], not(evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))),
                limitValue(['102', '104'], evalResidentField("CustomsClientNumber", isInLookup('luClientCCNs', 'ccn')))
              ]
            },
            {
              field: "{{Regulator}}Auth.AuthIssuer",
              display: [
                excludeValue(["Another Bank"], hasMoneyFieldValue(map("{{Regulator}}Auth.AuthFacilitator"), "Standard Bank")),
                excludeValue(["Standard Bank"], hasMoneyFieldValue(map("{{Regulator}}Auth.AuthFacilitator"), "Another Bank")),
              ]
            }
          ]
        },

        filterLookupImportExport: {
          ruleset: "Reporting Import Export Lookup Filter Rules",
          scope: "importexport",
          fields: []
        }
      }
    }

    return filterLookupRules;
  }
});


