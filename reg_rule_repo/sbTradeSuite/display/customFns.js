define(function (require) {
  return function (app, config) {

    var superInit = function () { };

    app.run(["$locale", function ($locale) {
      $locale.NUMBER_FORMATS.GROUP_SEP = " ";
      $locale.NUMBER_FORMATS.DECIMAL_SEP = ".";
    }]);

    if (config.initializationFn) superInit = config.initializationFn;

    var _getData = app.getData;

    app.getData = function () {
      var data = JSON.parse(JSON.stringify(_getData.apply(this, arguments)));
      if (!data.customData) data.customData = {};
      if (data.customData.snapShot) delete data.customData.snapShot;
      // data.customData.AuthIssuers = [];
      // data.transaction.MonetaryAmount.forEach(function (m) {
      //   data.customData.AuthIssuers.push(m.RegulatorAuth ? m.RegulatorAuth.AuthIssuer : undefined)
      // })
      return data;
    }

    var _config = {
      initializationFn: function (model, scope) {
        superInit(model, scope);

        scope.getResidentEntityType = function () {
          var data = app.getData();
          return data.transaction.Resident ? (data.transaction.Resident.Entity ? 'Entity' : data.transaction.Resident.Individual ? 'Individual' : 'Unknown') : undefined;
        }

        scope.getNonResidentEntityType = function(){
          var data = app.getData();
          return data.transaction.NonResident ? (data.transaction.NonResident.Entity ? 'Entity' : data.transaction.NonResident.Individual ? 'Individual' : 'Unknown') : undefined;
        }

        scope.shouldDisplayTradeSuiteFields = function(){
          var data = app.getData();
          
          var pattern = "^\\d{8,13}$";

          if (data.transaction.Resident.Individual) {
            if ((data.transaction.Resident.Individual.CustomsClientNumber) && (data.transaction.MonetaryAmount[0].CategoryCode)) {
              if((data.transaction.Resident.Individual.CustomsClientNumber).match(pattern)) {
                return true;
              }
            }

            return false;
          }
          else if (data.transaction.Resident.Entity) {
            if ((data.transaction.Resident.Entity.CustomsClientNumber) && (data.transaction.MonetaryAmount[0].CategoryCode)) {
              if((data.transaction.Resident.Entity.CustomsClientNumber).match(pattern)) {
                return true;
              }
            }

            return false;
          }
          else {
            return false;
          }
        }

        scope.shouldDisplayDescription = function() {
          var data = app.getData();
          var pattern = "^\\d{8,13}$";

          if (data.transaction.Resident.Individual) {
            if (data.transaction.Resident.Individual.CustomsClientNumber && data.transaction.MonetaryAmount[0].CategoryCode) {
              if((data.transaction.Resident.Individual.CustomsClientNumber).match(pattern)) {
                return true;
              }
            }
          }
          else if (data.transaction.Resident.Entity) {
            if (data.transaction.Resident.Entity.CustomsClientNumber && data.transaction.MonetaryAmount[0].CategoryCode) {
              if((data.transaction.Resident.Entity.CustomsClientNumber).match(pattern)) {
                return true;
              }
            }
          }
          
          return false;
        }

        scope.shouldDisplayRegulator = function(){
          var data = app.getData();

          var AuthIssuer = data.transaction.MonetaryAmount[0].RegulatorAuth ? data.transaction.MonetaryAmount[0].RegulatorAuth.AuthIssuer : undefined;

          if (AuthIssuer){
            
            return AuthIssuer;
          }

          return "";
        }

        scope.getDescription = function() {
          var data = app.getData();

          var desc = data.transaction.MonetaryAmount[0].CategoryDescription ? data.transaction.MonetaryAmount[0].CategoryDescription : undefined;

          if (desc) {
            return desc;
          }

          return "";

        }
      },

      clearHidden: false
    }

    Object.assign(config, _config);

    return config;

  }
})