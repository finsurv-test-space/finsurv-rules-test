define(function () {
  return function (predef) {
    var ie;

    with (predef) {
      ie = {
        ruleset: "Import/Export Rules for Flow Form",
        scope: "importexport",
        validations: [
          // {
          //   field: "",
          //   rules: [
          //     //ignore("field.maxLen"),
          //     message("ieicn1", null, "Required field for this category"),
          //     message("ieicn2", null, "Not required for category chosen"),
          //     //message("ieicn3", null, "The first 3 characters must be INV followed by the invoice number. "),
          //     message("ieicn4", null, "The format is AAACCYYMMDD0000000 where AAA is a valid customs office code in; CC is the century of import, YY is the year of import, MM is the month of import, DD is the day of import, and 0000000 is the 7 digit unique bill of entry number allocated by SARS as part of the MRN"),
          //     message("ietdn1", null, "Must be completed for the category selected"),
          //     message("ietdn2", null, "Not required for category chosen"),
          //     message("ieucr4", null, "UCR is not required"),
          //     message("iepcc2", null, "Currency Code of all Import/Export entries is not ZAR or does not match transaction Currency"),
          //     message("ImportControlNumber.minLen", null, "Number is too short"),
          //     message("UCR.minLen", null, "UCR is too short"),
          //     message("ImportControlNumber.maxLen", null, "Number is too long"),
          //     message("UCR.maxLen", null, "UCR is too long"),
          //     message("TransportDocumentNumber.maxLen", null, "Number is too long"),
          //     message("TransportDocumentNumber.minLen", null, "Number is too short")
          //   ]
          // }
        ]
      }
    };

    return ie;
  }
});