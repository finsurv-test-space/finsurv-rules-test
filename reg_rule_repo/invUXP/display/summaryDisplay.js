define(function () {
  return function (predef) {

    var summaryTrans, summaryMoney, summaryImportExport;
    with (predef) {
      summaryTrans = {
        ruleset: "Summary invUXP Transaction Display Rules",
        scope: "transaction",
        fields: [
          {
            // UXP only has individuals, and ReferenceName should exists on all reports
          field: "Resident.Description",
          display: [
            appendValue("%s", "Resident.Individual.ReferenceName", hasTransactionField("Resident.Individual")),
          ]
        }, 
        {
          field: "NonResident.Description",
          display: [
            appendValue("%s", "NonResident.Individual.ReferenceName", hasTransactionField("NonResident.Individual")),
          ]
        }
        ]
      };

      summaryMoney = {
        ruleset: "Summary StdBank Money Display Rules",
        scope: "money",
        fields: []
      };

      summaryImportExport = {
        ruleset: "Summary StdBank Import Export Display Rules",
        scope: "importexport",
        fields: []
      };
    }

    return {
      summaryTrans: summaryTrans,
      summaryMoney: summaryMoney,
      summaryImportExport: summaryImportExport
    }
  }
});