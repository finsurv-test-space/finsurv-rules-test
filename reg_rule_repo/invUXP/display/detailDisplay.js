define(function() {
  return function(predef) {
    var display;

    with (predef) {
      display = {
        detailTrans: {
          ruleset: "UXP Transaction Display Rules",
          scope: "transaction",
          fields: [
            {
              field: ["Resident.Individual.StreetAddress.Mandate"],
              display: [
                hide(),
                setValue('ACCEPT', not(hasResidentFieldValue("StreetAddress.Country", "ZA").or(hasTransactionFieldValue("AccountHolderStatus", ["Non Resident"]))))
               
              ]
            },
            {
              field: ["Resident.Individual.StreetAddress.Province"],
              display: [
                hide(),
                setValue('GAUTENG', hasResidentFieldValue("StreetAddress.Mandate","ACCEPT"))
              ]
            },
            {
              field: "NonResident.Individual.Address.Country",
              display: [
                setValue("%s", "Resident.Individual.StreetAddress.Country", notMatchesTransactionField("Resident.Individual.StreetAddress.Country").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA")))),
                setValue(function(context){
                  var curr = context.getTransactionField("FlowCurrency");
                  return curr?curr.substr(0,2):"";
                }).onCategory(['513']),

                setValue(function(context){
                  var curr = context.getTransactionField("FlowCurrency");
                  curr = curr?curr.substr(0,2):"";
                  return curr=="EU"?"DE":curr;
                }).notOnCategory(['513']),
                setValue("%s", "LocationCountry", isEmpty.and(notMatchesTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(hasResidentFieldValue("StreetAddress.Country", "ZA"))))),
                setValue("%s", "LocationCountry", isEmpty.and(hasTransactionField("LocationCountry").and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N")))),
                disable(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "Y").and(notResidentFieldValue("StreetAddress.Country", "ZA"))),
                enable(not(isTransactionFieldProvided("NonResident.Individual.Address.Country")).and(hasTransactionFieldValue("NonResident.Individual.IsMutualParty", "N").or(hasResidentFieldValue("StreetAddress.Country", "ZA"))))
              ]
            },
            {
              field: "TransactionCurrency",
              display: [
                setValue("%s", "FlowCurrency", isEmpty)
              ]
            },
            {
              field: "TotalForeignValue",
              display: [
                show(),
                setValue(undefined, null,
                  notEmpty
                    .and(
                    hasTransactionFieldValue("TransactionCurrency","ZAR")
                  )
                )
              ]
            },
            {
              field: "ZAREquivalent",
              display: [
                show(),
                //hide(hasTransactionFieldValue("FlowCurrency", "ZAR")),
                setValue(undefined, null,
                  notEmpty
                    .and(
                    notTransactionFieldValue("TransactionCurrency","ZAR")
                  )
                )

              ]
            },
            {
              field: "FeesIncluded",
              display: [
                hide(),
                //show(isCurrencyIn("ZAR").or(hasTransactionField("ZAREquivalent")))
                //  .onOutflow().onSection("A"),
                setValue(undefined, null, not(isCurrencyIn("ZAR").or(hasTransactionField("ZAREquivalent"))).and(notEmpty))
                  .onOutflow().onSection("A"),
                setValue('N', null, isEmpty.and(isCurrencyIn("ZAR").or(hasTransactionField("ZAREquivalent"))))
                  .onOutflow().onSection("A")
                //show(hasTransactionField("ZAREquivalent")).onOutflow().onSection("A")
              ]
            },
            {
              field: [ "Resident.Individual.TempResExpiryDate",  "Resident.Entity.TempResExpiryDate"],
              display: [
                disable()
              ]
            },
            {
              field: "LocationCountry",
              display : [
                
                setValue(undefined).onSection("C")
              ]
            },
            {
              field  : "IsInvestecFCA",
              display: [
                clearValue(hasTransactionField("IsInvestecFCA").and(isCurrencyIn("ZAR"))),
                show(),
                hide(notTransactionField("FlowCurrency").or(isCurrencyIn("ZAR"))),
                disable(isTransactionFieldProvided("IsInvestecFCA")).onOutflow()
              ]
            },
            {
              field  : "InvestecFCA",
              display: [
                //hide(),
                setValue("%s","NonResident.Individual.AccountNumber", isEmpty.and(hasTransactionFieldValue("IsInvestecFCA", "Y"))),
                show(notCurrencyIn("ZAR").and(hasTransactionFieldValue("IsInvestecFCA", "Y"))),
                disable(isTransactionFieldProvided("InvestecFCA")).onOutflow()
              ]
            },
            {
              field: "NonResident.Individual.AccountNumber",
              display: [
                //show()
                hide(),
                show(hasTransactionFieldValue("IsInvestecFCA", "N").or(notTransactionField("IsInvestecFCA"))).onOutflow()
                //,disable(isTransactionFieldProvided("NonResident.Individual.AccountNumber"))
              ]
            },
            {
              field: "RateConfirmation",
              display: [
                show(),
                setValue("N", null, notTransactionField("RateConfirmation")),
                hide(function(context){
                  var curr1 = context.getCustomValue("AccountHolderCurrency");
                  var curr2 = context.getCustomValue("CounterPartyCurrency");
                  if(!(curr1&&curr2)) return false;
                  return curr1 == curr2;
                })
              ]
            },
            {
              field: "PaymentDetail.Charges",
              display: [
                hide()
              ]
            },
            {
              field: "Resident.Individual.PassportExpiryDate",
              display: [
                show(),
                // hide(),
                // show(hasTransactionFieldValue("AccountHolderStatus", ["Non Resident","Foreign Temporary Resident"])
                //   .and(hasTransactionField("Resident.Individual"))),
                // disable(isTransactionFieldProvided("Resident.Individual.PassportExpiryDate"))
                disable()
              ]
            }
          ]
        },
        detailMoney: {
          ruleset: "UXP Money Display Rules",
          scope: "money",
          fields: [
            {
              field: "CompoundCategoryCode",
              display : [
                setValue(function (data, ind) {
                  var money = data.MonetaryAmount[ind];
                  var cat = money.CategoryCode +
                    (money.CategorySubCode ? '/' + money.CategorySubCode : '');
                  return cat;
                })
              ]
            },
            {
              field: "CategoryDescription",
              display : [
                setValue(function (data, ind) {
                  var money = data.MonetaryAmount[ind];
                  var cat = money.CategoryCode +
                    (money.CategorySubCode ? '/' + money.CategorySubCode : '');
                  if (cat) {
                    cat = cat.toUpperCase();
                    if (cat == 'ZZ1' || cat == 'ZZ2') {
                      // special case
                      return 'Transfer';
                    }
                    var categories = data.getLookups().categories;
                    var thisCat = _.find(categories, function (itm) {
                      return itm.code == cat;
                    })
                    if (thisCat) {
                      return thisCat.description;
                    }
                  }
                })
              ]
            },
            {
              field: "ForeignValue",
              display : [
                hide(),
                setValue("%s","transaction::TotalForeignValue")
              ]
            },
            {
              field: "RandValue",
              display : [
                hide(),
                setValue("%s","transaction::ZAREquivalent")
              ]
            }
          ]
        },
        detailImportExport: {
          ruleset: "UXP Import/Export Display Rules",
          scope: "importexport",
          fields: []
        }
      };
    }

    return display;
  };
});
