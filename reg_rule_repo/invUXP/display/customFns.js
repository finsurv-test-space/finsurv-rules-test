define(function(require) {
  return function(app, config) {

    /** ResidencyStatus */
    var RS = {
      PSASTA: "PSA/STA",
      NOR: "NOR",
      FTR: "FTR"
    };

    /** PaymentFlow */
    var PF = {
      PBAtoFCA: "PBA to FCA",
      FCAtoPBA: "FCA to PBA",
      OffshoreToFCA: "Offshore to FCA",
      FCAtoFCA: "FCA to FCA"
    };

    /** Category map (Table provided by Tammy Slacks, Digital Online, Investec) */
    var Cat = {};

    Cat[PF.PBAtoFCA] = {};
    Cat[PF.PBAtoFCA][RS.PSASTA] = "513";
    Cat[PF.PBAtoFCA][RS.NOR] = "510/02";
    Cat[PF.PBAtoFCA][RS.FTR] = "417";

    Cat[PF.FCAtoPBA] = {};
    Cat[PF.FCAtoPBA][RS.PSASTA] = "ZZ1";
    Cat[PF.FCAtoPBA][RS.NOR] = "510/02";
    Cat[PF.FCAtoPBA][RS.FTR] = "417";

    Cat[PF.OffshoreToFCA] = {};
    Cat[PF.OffshoreToFCA][RS.PSASTA] = "511/04";
    Cat[PF.OffshoreToFCA][RS.NOR] = "ZZ1";
    Cat[PF.OffshoreToFCA][RS.FTR] = "417";

    Cat[PF.FCAtoFCA] = {};
    Cat[PF.FCAtoFCA][RS.PSASTA] = "ZZ1";
    Cat[PF.FCAtoFCA][RS.NOR] = "ZZ1";
    Cat[PF.FCAtoFCA][RS.FTR] = "ZZ1";

    /** Flow map */

    var Flow = {};
    Flow[PF.PBAtoFCA] = ["OUT"];
    Flow[PF.FCAtoPBA] = ["IN"];
    Flow[PF.OffshoreToFCA] = ["IN"];
    Flow[PF.FCAtoFCA] = ["IN","OUT"];

    app.getUxpCategory = function(residencyStatus,paymentFlow) {
      return Cat[paymentFlow][residencyStatus];
    };

    app.getUxpFlow = function(paymentFlow) {
      return Flow[paymentFlow];
    };



    var superInit = function () { };

    if (config.dataPreTransformFn) superInit = config.dataPreTransformFn;

    var _config = {
        dataPreTransformFn: function (data) {
            superInit(data);
            //custom transform functions go here...

            var d = data.transaction;

            var customData = data.customData;

            if(customData && customData.residencyStatus && customData.paymentFlow){
              var flow = app.getUxpFlow(customData.paymentFlow);
              d.Flow = flow[0];
  
              var cat = app.getUxpCategory(customData.residencyStatus, customData.paymentFlow);
              cat = cat.split("/");

              if(!d.MonetaryAmount||!d.MonetaryAmount.length){
                d.MonetaryAmount = [{}];
              }
              
              d.MonetaryAmount[0].CategoryCode = cat[0];
              d.MonetaryAmount[0].CategorySubCode = cat[1];
  
              if(cat[0]==='ZZ1'){
                d.ReportingQualifier = "NON REPORTABLE";
              }
            }
            //scope.$digest();
        }
    }

    Object.assign(config, _config);

    return config;

  };
});
