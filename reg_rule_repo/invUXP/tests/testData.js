var ResidentIndividual = {
  Individual: {
    ReferenceName: "Bar Acc",
    StreetAddress: {
      AddressLine1: "MNI Towers 2",
      AddressLine2: "11 Jalan Pinang",
      Suburb: "Kuala Lumpur",
      City: "Johannesburg",
      PostalCode: "2196",
      Province: "Foo",
      Country: "DE"
    },
    PostalAddress: {
      AddressLine1: "50200",
      AddressLine2: "Kuala Lumpur",
      Suburb: "POSTALSUBURB",
      City: "Johannesburg",
      PostalCode: "2196",
      Province: "Foo",
      Country: "US"
    },
    ContactDetails: {
      ContactName: "CONTACTNAME",
      ContactSurname: "CONTACTSURNAME",
      Email: "CONTACTEMAIL@GMAIL.COM",
      Fax: "2761360578",
      Telephone: "2761305978"
    },
    Name: "John",
    Surname: "Johnson",
    AccountIdentifier: "RESIDENT OTHER",
    AccountName: "Account_NT 1 p",
    AccountNumber: "221495398",
    TaxNumber: "123456",
    VATNumber: "66666",
    Gender: "M",
    DateOfBirth: "1955-02-29",
    //IDNumber: "5502290001080",
    //TempResPermitNumber: "123123213",
    //TempResExpiryDate: "2018-07-17",
    TaxNumber: "123412342"
  }
};

var ResidentEntity = {
  Entity: {
    PostalAddress: {
      AddressLine2: "ACME Bank",
      AddressLine1: "Ground Floor",
      Suburb: "Johannesburg",
      PostalCode: "2001",
      City: "Johannesburg",
      Province: "GAUTENG",
      Country: "ZA"
    },
    StreetAddress: {
      AddressLine2: "ACME Bank",
      AddressLine1: "Ground Floor",
      Suburb: "Johannesburg",
      PostalCode: "2001",
      City: "Johannesburg",
      Province: "GAUTENG",
      Country: "ZA"
    },
    EntityName: "ACME BK OF ZA LTD",
    ContactDetails: {
      ContactSurname: "Fudd",
      Email: "elma.fudd@acme.co.za",
      Telephone: "27116365094",
      ContactName: "Elma"
    },
    AccountIdentifier: "RESIDENT OTHER",
    IndustrialClassification: "08",
    InstitutionalSector: "01",
    TradingName: "ACME BK OF ZA LTD",
    RegistrationNumber: "196200073806",
    AccountName: "CONTRA MTSS TREAS DIV",
    AccountNumber: "11009864660",
    TaxNumber: "123412342"
  }
};

var NonResidentIndividual = {
  Individual: {
    IsMutualParty: "N",
    ReferenceName: "Foo Acc",
    Address: {
      AddressLine1: "addressline1",
      AddressLine2: "addressline2",
      Suburb: "suburb",
      City: "townname",
      State: "stateName",
      PostalCode: "1234",
      Country: "DE"
    },
    AccountIdentifier: "NON RESIDENT OTHER",
    AccountNumber: "1100243812587",
    Name: "Fred",
    Surname: "Georgeson"
  },
  Type: "Individual"
};

var NonResidentEntity = {
  Entity: {
    IsMutualParty: "N",
    EntityName: "Foo Inc.",
    Address: {
      AddressLine1: "5 Way",
      AddressLine2: "",
      City: "city",
      PostalCode: "4001",
      Country: "US"
    },
    AccountIdentifier: "FCA RESIDENT",
    AccountNumber: "000040452"
  }
};

function genData() {
  var testData = [];

  /** ResidencyStatus */
  var RS = ["PSA/STA", "NOR", "FTR"];

  var RSMapping = {
    "PSA/STA": "South African Resident",
    NOR: "Non Resident",
    FTR: "Foreign Temporary Resident"
  };

  /** PaymentFlow */
  var PF = ["PBA to FCA", "FCA to PBA", /*"Offshore to FCA",*/ "FCA to FCA"];

  ["Individual" /*, "Entity"*/].forEach(function(i) {
    ["Individual" /*, "Entity"*/].forEach(function(j) {
      RS.forEach(function(rs) {
        PF.forEach(function(pf) {
          var d = {
            name: "Res" + i + ", NonRes" + j + ", rs:" + rs + ", pf:" + pf,
            data: {
              customData: {
                residencyStatus: rs,
                paymentFlow: pf
              },
              transaction: {
                ValueDate: new Date().toISOString().substr(0, 10),
                AccountHolderStatus: RSMapping[rs],
                CounterpartyStatus: "Non Resident",
                ReportingQualifier: "BOPCUS",
                FlowCurrency: "USD",
                LocationCountry: "US",
                ReceivingCountry: "US",
                IsInvestecFCA: "Y",
                //InvestecFCA: "0011221495398",
                Resident: JSON.parse(JSON.stringify(window["Resident" + i])),
                NonResident: JSON.parse(
                  JSON.stringify(window["NonResident" + j])
                ),
                PaymentDetail: {
                  IsCorrespondentBank: "N",
                  BeneficiaryBank: {
                    BankName: "Foo",
                    BranchCode: "bar",
                    Address: "baz",
                    City: "foo"
                  }
                }
              }
            }
          };
          d.data.transaction.ReceivingCountry = rs == "PSA/STA" ? "ZA" : "US";
          d.data.transaction.Resident.Individual.StreetAddress.Country =
            rs == "PSA/STA" ? "ZA" : "US";
          d.data.transaction.NonResident.Individual.Address.Country =
            rs == "PSA/STA" ? "ZA" : "US";
          testData.push(d);
        });
      });
    });
  });
  return testData;
}
var testData = [
  {
    name: "Empty transaction",
    data: {}
  },

  {
    name: "Minimal individuals",
    data: {
      Flow: "OUT",
      Resident: {
        Individual: {}
      },
      NonResident: {
        Individual: {}
      }
    }
  },

  {
    name: "Minimal information INV",

    data: {
      transaction: {
        TrnReference: "InfoProvidedInv2",
        Flow: "OUT",
        ValueDate: "2017-05-10",
        ReportingQualifier: "BOPCUS",
        OriginatingBank: "IVESZAJJ0",
        OriginatingCountry: "ZA",
        FlowCurrency: "USD",
        TotalForeignValue: "1000.00",
        ZAREquivalent: "1234",
        Resident: {
          Entity: {
            PostalAddress: {
              AddressLine2: "ACME Bank",
              AddressLine1: "Ground Floor",
              Suburb: "Johannesburg",
              PostalCode: "2001",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            StreetAddress: {
              AddressLine2: "ACME Bank",
              AddressLine1: "Ground Floor",
              Suburb: "Johannesburg",
              PostalCode: "2001",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            EntityName: "ACME BK OF ZA LTD",
            ContactDetails: {
              ContactSurname: "Fudd",
              Email: "elma.fudd@acme.co.za",
              Telephone: "27116365094",
              ContactName: "Elma"
            },
            AccountIdentifier: "RESIDENT OTHER",
            IndustrialClassification: "08",
            InstitutionalSector: "01",
            TradingName: "ACME BK OF ZA LTD",
            RegistrationNumber: "196200073806",
            AccountName: "CONTRA MTSS TREAS DIV",
            AccountNumber: "9864660"
          }
        }
      }
    }
  },

  {
    name: "Minimal entities",
    data: {
      Flow: "OUT",
      Resident: {
        Entity: {}
      },
      NonResident: {
        Entity: {}
      }
    }
  },

  {
    name: "invUXP1",
    data: {
      ValueDate: "2018-02-12",
      TotalForeignValue: "7206.00",
      TrnReference: "090f4a1180766248",
      OriginatingCountry: "ZA",
      LocationCountry: "DE",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      FlowCurrency: "USD",
      OriginatingBank: "IVESZAJJ0",
      Version: "FINSURV",
      PaymentDetail: {
        BeneficiaryBank: {
          BankName: "RRR",
          Address: "RRR",
          City: "RR",
          BranchCode: "FRR"
        },
        IsCorrespondentBank: "N"
      },
      Resident: {
        Entity: {
          VATNumber: "4310183340",
          StreetAddress: {
            AddressLine2: "ouuhxsstqi",
            AddressLine1: "pmapbvlotg",
            Suburb: "Springfield Park",
            PostalCode: "4051",
            Country: "South Africa",
            City: "Durban North",
            Province: "Durban North"
          },
          EntityName: "Moore Sound and Electronics",

          TradingName: "",
          RegistrationNumber: "1999/041061/23",
          TaxNumber: "9140325201",
          AccountNumber: "1100203024196"
        },
        Type: "Entity"
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "addressline1",
            AddressLine2: "addressline2",
            Suburb: "suurb",
            City: "townname",
            PostalCode: "postalcode",
            Country: "US"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "9032243812587",
          Name: "Fred",
          Surname: "Georgeson"
        },
        Type: "Individual"
      }
    }
  },
  {
    name: "invUXP2",
    data: {
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Entity: {
          VATNumber: "4310183340",
          StreetAddress: {
            AddressLine2: "ouuhxsstqi",
            AddressLine1: "pmapbvlotg",
            Suburb: "Springfield Park",
            PostalCode: "4051",
            Country: "ZA",
            City: "Durban North",
            Province: "LIMPOPO"
          },
          EntityName: "Moore Sound and Electronics",
          TradingName: "",
          RegistrationNumber: "1999/041061/23",
          TaxNumber: "9140325201",
          AccountNumber: "1100203024196",
          ContactDetails: {
            Fax: "",
            Email: "asd@asd.asd",
            ContactSurname: "asda",
            ContactName: "asd"
          }
        },
        Type: "Entity"
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "addressline1",
            AddressLine2: "addressline2",
            Suburb: "suurb",
            City: "townname",
            PostalCode: "postalcode",
            Country: "US"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "9032243812587",
          Name: "Fred",
          Surname: "Georgeson"
        },
        Type: "Individual"
      },
      ValueDate: "2018-02-12",
      TrnReference: "090f4a1180766248",
      OriginatingCountry: "ZA",
      LocationCountry: "DE",
      FlowCurrency: "USD",
      OriginatingBank: "IVESZAJJ0",
      PaymentDetail: {
        BeneficiaryBank: {
          BankName: "RRR",
          Address: "RRR",
          City: "RR",
          BranchCode: "FRR"
        },
        IsCorrespondentBank: "N",
        SWIFTDetails: "124124124124"
      },
      TransactionCurrency: "USD",
      RateConfirmation: "N",
      IsInvestecFCA: "N",
      CounterpartyStatus: "Non Resident",
      ReceivingCountry: "AD",
      AccountHolderStatus: "South African Resident"
    }
  },
  {
    name: "invUXP3",
    data: {
      customData: {
        paymentFlow: "PBA to FCA"
      },
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            IsEntity: false,
            IsIndividual: true,
            Dob: "/Date(6213600000)/",
            DateOfBirth: "1970-03-14",
            AccountNumber: "10010493997",
            Surname: "Onororr",
            Name: "AlizizIz",
            Gender: "F",
            IDNumber: "7003140133080",
            AccountHolderStatus: "South African Resident",
            EntityName: null,
            VATNumber: null,
            RegistrationNumber: null,
            TaxNumber: "6398290712",
            TempResPermitNumber: "",
            TempResExpiryDate: "",
            PassportNumber: "",
            PassportExpiryDate: "1999-08-08",
            PassportCountry: "",
            TradingName: null,
            StreetAddress: {
              AddressLine1: "277 Xcyzn Str",
              AddressLine2: "",
              Suburb: "Bubcdw",
              City: "Rcbzkw",
              Province: "GAUTENG",
              PostalCode: "5226",
              Country: "ZA"
            },
            ContactDetails: {
              ContactSurname: "Onororr",
              ContactName: "AlizizIz",
              Email: "ZkpYM@someweb.co.za",
              Telephone: "0111234101",
              Fax: ""
            },
            ErrorDescription: null,
            ErrorNumber: null,
            ErrorMnemonic: null,
            AccountIdentifier: "RESIDENT OTHER",
            TaxClearanceCertificateIndicator: "N"
          }
        },
        NonResident: {
          Individual: {
            IsEntity: false,
            IsIndividual: true,
            Dob: "/Date(6213600000)/",
            DateOfBirth: "1970-03-14",
            AccountNumber: "1100448531501",
            Surname: "Onororr",
            Name: "AlizizIz",
            Gender: "F",
            IDNumber: "7003140133080",
            AccountHolderStatus: "South African Resident",
            EntityName: null,
            VATNumber: null,
            RegistrationNumber: null,
            TaxNumber: "6398290712",
            TempResPermitNumber: "",
            TempResExpiryDate: "",
            PassportNumber: "",
            PassportExpiryDate: "",
            PassportCountry: "",
            TradingName: null,
            StreetAddress: {
              AddressLine1: "277 Xcyzn Str",
              AddressLine2: "",
              Suburb: "Bubcdw",
              City: "Rcbzkw",
              Province: "GAUTENG",
              PostalCode: "5226",
              Country: "ZA"
            },
            ContactDetails: {
              ContactSurname: "Onororr",
              ContactName: "AlizizIz",
              Email: "ZkpYM@someweb.co.za",
              Telephone: "0111234101",
              Fax: ""
            },
            ErrorDescription: null,
            ErrorNumber: null,
            ErrorMnemonic: null,
            AccountIdentifier: "FCA NON RESIDENT",
            Address: { Country: "US" },
            IsMutualParty: "Y"
          }
        },
        MonetaryAmount: [
          {
            ImportExport: [],
            SARBAuth: {},
            TravelMode: {},
            ThirdParty: {},
            CategoryCode: "513",
            CategoryDescription:
              "Investment by a resident individual originating from a local source into an account conducted in foreign currency held at an Authorised Dealer in South Africa",
            CompoundCategoryCode: "513",
            RandValue: "11.00"
          }
        ],
        AccountHolderStatus: "South African Resident",
        CounterpartyStatus: "South African Resident",
        BranchCode: "99030100",
        BranchName: "SANDTON",
        CorrespondentBank: "Investec Bank Limited",
        CorrespondentCountry: "ZA",
        ReceivingBank: "Investec Bank Limited",
        AccountHolderType: "Individual",
        IsAgent: "N",
        AgentThirdPartyType: "Individual",
        AgentThirdPartyStatus: "South African Resident",
        ReceivingCountry: "SA",
        LocationCountry: "US",
        ValueDate: "2018-07-20",
        FlowCurrency: "USD",
        PaymentDetail: {
          BeneficiaryBank: {
            BankName: "Investec Bank Limited",
            BranchCode: "99030100",
            Address: "100 Grayston Drive",
            City: "Johannesburg",
            SWIFTDetails: "IVESZAJJXXX"
          },
          IsCorrespondentBank: "N"
        },
        RateConfirmation: "N",
        TransactionCurrency: "ZAR",
        IsInvestecFCA: "N",
        ZAREquivalent: "11.00",
        FeesIncluded: "N"
      }
    }
  },
  {
    name: "Duncan's issue",
    data: {
      transaction: {
        NonResident: {
          Individual: {
            IsEntity: false,
            IsIndividual: true,
            Dob: "/Date(11397600000)/",
            DateOfBirth: "1970-05-13",
            AccountNumber: "1100531861510",
            Surname: "AsekekEk",
            Name: "IlroroAo",
            Gender: "M",
            IDNumber: "",
            AccountHolderStatus: "Foreign Temporary Resident",
            EntityName: null,
            VATNumber: null,
            RegistrationNumber: null,
            TaxNumber: "",
            TempResPermitNumber: "175/16",
            TempResExpiryDate: "2020-12-31",
            PassportNumber: "944471506",
            PassportExpiryDate: "2020-09-21",
            PassportCountry: "SE",
            TradingName: null,
            StreetAddress: {
              AddressLine1: "153 Fqbpp Str",
              AddressLine2: "",
              Suburb: "Vudipx",
              City: "Yfdreu",
              Province: "WESTERN CAPE",
              PostalCode: "3969",
              Country: "ZA"
            },
            ContactDetails: {
              ContactSurname: "AsekekEk",
              ContactName: "IlroroAo",
              Email: "WsvWf@someweb.co.za",
              Telephone: "0112867000",
              Fax: ""
            },
            ErrorDescription: null,
            ErrorNumber: null,
            ErrorMnemonic: null,
            Address: {
              Country: "US"
            },
            IsMutualParty: "Y",
            AccountIdentifier: "FCA NON RESIDENT",
            ReferenceName: "Mr RQ AsekekEk"
          }
        },
        Resident: {
          Individual: {
            IsEntity: false,
            IsIndividual: true,
            Dob: "/Date(11397600000)/",
            DateOfBirth: "1970-05-13",
            AccountNumber: "10011819313",
            Surname: "AsekekEk",
            Name: "IlroroAo",
            Gender: "M",
            IDNumber: "",
            AccountHolderStatus: "Foreign Temporary Resident",
            EntityName: null,
            VATNumber: null,
            RegistrationNumber: null,
            TaxNumber: "",
            TempResPermitNumber: "175/16",
            TempResExpiryDate: "2020-12-31",
            PassportNumber: "944471506",
            PassportExpiryDate: "2020-09-21",
            PassportCountry: "SE",
            TradingName: null,
            StreetAddress: {
              AddressLine1: "153 Fqbpp Str",
              AddressLine2: "",
              Suburb: "Vudipx",
              City: "Yfdreu",
              Province: "GAUTENG",
              PostalCode: "3969",
              Country: "ZA"
            },
            ContactDetails: {
              ContactSurname: "AsekekEk",
              ContactName: "IlroroAo",
              Email: "WsvWf@someweb.co.za",
              Telephone: "0112867000",
              Fax: ""
            },
            ErrorDescription: null,
            ErrorNumber: null,
            ErrorMnemonic: null,
            AccountIdentifier: "RESIDENT OTHER",
            ReferenceName: "NTDKMr  eke Matsek"
          }
        },
        AccountHolderStatus: "Foreign Temporary Resident",
        CounterpartyStatus: "Foreign Temporary Resident",
        BranchCode: "99030100",
        BranchName: "SANDTON",
        CorrespondentBank: "Investec Bank Limited",
        CorrespondentCountry: "ZA",
        ReceivingBank: "Investec Bank Limited",
        AccountHolderType: "Individual",
        IsAgent: "N",
        AgentThirdPartyType: "Individual",
        AgentThirdPartyStatus: "South African Resident",
        ValueDate: "2018-08-13",
        IsInvestecFCA: "Y",
        PaymentDetail: {
          BeneficiaryBank: {
            BankName: "Investec Bank Limited",
            BranchCode: "99030100",
            Address: "100 Grayston Drive",
            City: "Johannesburg",
            SWIFTBIC: "IVESZAJJXXX"
          }
        },
        LocationCountry: "US",
        ReceivingCountry: "ZA",
        FlowCurrency: "USD",
        Flow: "OUT",
        MonetaryAmount: [
          {
            CategoryCode: "417"
          }
        ]
      },
      customData: {
        msgHide: true,
        paymentFlow: "PBA to FCA",
        AccountHolderCurrency: "ZAR",
        CounterPartyCurrency: "USD",
        residencyStatus: "FTR"
      }
    }
  },
  {
    name: "Vikash transaction currency issue",
    data: {
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Individual: {
          IsEntity: false,
          IsIndividual: true,
          Dob: "/Date(358466400000)/",
          DateOfBirth: "1981-05-12",
          AccountNumber: "10010607732",
          Surname: "Ototott",
          Name: "MticicRc",
          Gender: "M",
          IDNumber: "8105125512086",
          AccountHolderStatus: "South African Resident",
          EntityName: null,
          VATNumber: null,
          RegistrationNumber: null,
          TaxNumber: "2497642384",
          TempResPermitNumber: "",
          TempResExpiryDate: "",
          PassportNumber: "",
          PassportExpiryDate: "",
          PassportCountry: "",
          TradingName: null,
          StreetAddress: {
            AddressLine1: "335 Sqanl Str",
            AddressLine2: "dabwg",
            Suburb: "Oaofjs",
            City: "Iywful",
            Province: "GAUTENG",
            PostalCode: "6003",
            Country: "GB",
            Mandate: "ACCEPT"
          },
          ContactDetails: {
            ContactSurname: "Ototott",
            ContactName: "MticicRc",
            Email: "sOdfm@someweb.co.za",
            Telephone: "0111234742",
            Fax: ""
          },
          ErrorDescription: null,
          ErrorNumber: null,
          ErrorMnemonic: null,
          AccountIdentifier: "RESIDENT OTHER",
          ReferenceName: "DV GMr  ott DV Got",
          TaxClearanceCertificateIndicator: "N"
        }
      },
      NonResident: {
        Individual: {
          IsEntity: false,
          IsIndividual: true,
          Dob: "/Date(358466400000)/",
          DateOfBirth: "1981-05-12",
          AccountNumber: "1100402590502",
          Surname: "Ototott",
          Name: "MticicRc",
          Gender: "M",
          IDNumber: "8105125512086",
          AccountHolderStatus: "South African Resident",
          EntityName: null,
          VATNumber: null,
          RegistrationNumber: null,
          TaxNumber: "2497642384",
          TempResPermitNumber: "",
          TempResExpiryDate: "",
          PassportNumber: "",
          PassportExpiryDate: "",
          PassportCountry: "",
          TradingName: null,
          StreetAddress: {
            AddressLine1: "335 Sqanl Str",
            AddressLine2: "dabwg",
            Suburb: "Oaofjs",
            City: "Iywful",
            Province: "",
            PostalCode: "6003",
            Country: "GB"
          },
          ContactDetails: {
            ContactSurname: "Ototott",
            ContactName: "MticicRc",
            Email: "sOdfm@someweb.co.za",
            Telephone: "0111234742",
            Fax: ""
          },
          ErrorDescription: null,
          ErrorNumber: null,
          ErrorMnemonic: null,
          Address: {
            Country: "GB",
            PostalCode: "6003",
            City: "Iywful",
            Suburb: "Oaofjs",
            AddressLine2: "dabwg",
            AddressLine1: "335 Sqanl Str",
            State: "GAUTENG"
          },
          IsMutualParty: "Y",
          AccountIdentifier: "FCA NON RESIDENT",
          ReferenceName: "Mr FX Ototott"
        }
      },
      MonetaryAmount: [
        {
          SARBAuth: {},
          TravelMode: {},
          ThirdParty: {},
          CategoryCode: "513",
          CategoryDescription:
            "Investment by a resident individual originating from a local source into an account conducted in foreign currency held at an Authorised Dealer in South Africa",
          CompoundCategoryCode: "513",
          ForeignValue: "222",
          TaxClearanceCertificateIndicator: "N"
        }
      ],
      AccountHolderStatus: "South African Resident",
      CounterpartyStatus: "South African Resident",
      BranchCode: "99030100",
      BranchName: "SANDTON",
      CorrespondentBank: "Investec Bank Limited",
      CorrespondentCountry: "ZA",
      ReceivingBank: "Investec Bank Limited",
      AccountHolderType: "Individual",
      IsAgent: "N",
      AgentThirdPartyType: "Individual",
      AgentThirdPartyStatus: "South African Resident",
      ValueDate: "2018-08-15",
      IsInvestecFCA: "Y",
      PaymentDetail: {
        BeneficiaryBank: {
          BankName: "Investec Bank Limited",
          BranchCode: "99030100",
          Address: "100 Grayston Drive",
          City: "Johannesburg",
          SWIFTBIC: "IVESZAJJXXX"
        },
        IsCorrespondentBank: "N"
      },
      LocationCountry: "EU",
      ReceivingCountry: "ZA",
      FlowCurrency: "EUR",
      RateConfirmation: "N",
      InvestecFCA: "1100402590502",
      TransactionCurrency: "EUR",
      TotalForeignValue: "222"
    }
  },
  {
    name: "Niranjan's issue 2019 06 07",
      data: {
        "Version": "FINSURV",
        "ReportingQualifier": "BOPCUS",
        "Flow": "IN",
        "Resident": {
          "Individual": {
            "IsEntity": false,
            "IsIndividual": true,
            "Dob": "/Date(1318975200000)/",
            "DateOfBirth": "2011-10-19",
            "AccountNumber": "10011814650",
            "Surname": "HbbaxbaBa",
            "Name": "AafaxfaAa",
            "Gender": "F",
            "IDNumber": "",
            "EntityName": null,
            "VATNumber": null,
            "RegistrationNumber": null,
            "TaxNumber": "",
            "TempResPermitNumber": "",
            "TempResExpiryDate": "",
            "PassportNumber": "111884676",
            "PassportExpiryDate": "2022-10-24",
            "PassportCountry": "SZ",
            "TradingName": null,
            "StreetAddress": {
              "AddressLine1": "386 Dkslz Str",
              "AddressLine2": "bxwhq",
              "Suburb": "Mmfmap",
              "City": "Bylyhp",
              "Province": "GAUTENG",
              "PostalCode": "NG3",
              "Country": "CD"
            },
            "ContactDetails": {
              "ContactSurname": "HbbaxbaBa",
              "ContactName": "AafaxfaAa",
              "Email": "ovIdQ@someweb.co.za",
              "Telephone": "0111234768",
              "Fax": "0112869999"
            },
            "ErrorDescription": null,
            "ErrorNumber": null,
            "ErrorMnemonic": null,
            "IsInvestecSourceAcc": "N",
            "IsInvestecFCA": "N",
            "PostalAddress": {
              "Province": "GAUTENG"
            },
            "TaxClearanceCertificateIndicator": "N"
          }
        },
        "NonResident": {
          "Individual": {
            "IsMutualParty": "Y",
            "AccountNumber": "",
            "Address": {
              "AddressLine1": "386 Dkslz Str",
              "AddressLine2": "bxwhq",
              "Suburb": "Mmfmap",
              "City": "Bylyhp",
              "State": "GAUTENG",
              "PostalCode": "NG3",
              "Country": "CD"
            },
            "Surname": "HbbaxbaBa",
            "Name": "AafaxfaAa",
            "Gender": "F"
          }
        },
        MonetaryAmount: [
          {
            SARBAuth: {},
            TravelMode: {},
            ThirdParty: {},
            CategoryCode: "513",
            CategoryDescription:
              "Investment by a resident individual originating from a local source into an account conducted in foreign currency held at an Authorised Dealer in South Africa",
            CompoundCategoryCode: "513",
            ForeignValue: "222",
            TaxClearanceCertificateIndicator: "N"
          }
        ],
        "FlowCurrency": "ZAR",
        "ZAREquivalentCurrency": "ZAR",
        "AccountHolderStatus": "Non Resident",
        "AccountHolderType": "Individual",
        "BranchCode": "99030100",
        "BranchName": "SANDTON",
        "PaymentDetail": {
          "IsCorrespondentBank": "N",
          "BeneficiaryBank": {}
        },
        "TotalForeignValue": 19900,
        "ValueDate": "2019-06-07",
        "RateConfirmation": "N",
        "CounterpartyStatus": "Non Resident",
        "IsInvestecFCA": "N",
        "IsInvestecSourceAcc": "N",
        "TransactionCurrency": "ZAR"
      }
  }
];

testData = testData.concat(genData());

// Since this dummy data needs to be tested in the Dev source and the Built source,
// it needs to work with requirejs and without it. So, cannot assume requirejs' 'define' will be
// defined.

if (typeof define !== "undefined")
  define({
    data: testData
  });
