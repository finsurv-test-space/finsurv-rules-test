define(function () {
//THIS IS the Transaciotn rules
  return function (predef) {
      var transaction;

      with (predef) {
          transaction = {
              ruleset: "Transaction Rules for Flow form",
              scope: "transaction",
              validations: [
                {
                  field: ["NonResident.Individual.AccountNumber", "NonResident.Entity.AccountNumber"],
                  rules: [
                    ignore("inv_nrian5")
                  ]
                },
                {
                  field: "",
                  rules: [
                    message("inv_ritrpe4", "I24","We are unable to proceed with your request as it appears your temporary resident permit number has expired. Please contact your Private Banker or the 24/7 global Client Support Centre to update your details.")
                  ]
                },
                {
                      field: "TotalForeignValue",
                      rules: [
                          ignore("tfv1"),
                          ignore("tfv2"),
                          ignore("tfv4"),
                          failure("inv_tfv1", "I37", "A Currency Amount must be provided",
                              isEmpty.or(hasValue("0"))
                                  .or(hasValue("0.00"))
                                  .or(hasValue("0.0"))
                                  .or(hasValue(".00"))
                                  .or(hasValue(".0")).and(notTransactionField("ZAREquivalent"))
                          ).onInflow().onSection("ABCDEG"),
                          failure("inv_tfv2", "I??", "A Currency Amount or a ZAR equivalent amount must be provided",
                              hasCustomValue("paymentFlow","PBA to FCA").and(
                                  isEmpty.or(hasValue("0"))
                                  .or(hasValue("0.00"))
                                  .or(hasValue("0.0"))
                                  .or(hasValue(".00"))
                                  .or(hasValue(".0")).and(notTransactionField("ZAREquivalent")))
                          ).onOutflow().onSection("ABCDEG"),
                          failure("inv_tfv3", "I??", "A Currency Amount must be provided",
                              notCustomValue("paymentFlow","PBA to FCA").and(
                                  isEmpty.or(hasValue("0"))
                                  .or(hasValue("0.00"))
                                  .or(hasValue("0.0"))
                                  .or(hasValue(".00"))
                                  .or(hasValue(".0")).and(notTransactionField("ZAREquivalent")))
                          ).onOutflow().onSection("ABCDEG")
                      ]
                  },
                  {
                      field: "ZAREquivalent",
                      rules: [
                          failure("inv_zar1", "I??", "A Currency Amount or a ZAR equivalent amount must be provided",
                              isEmpty.and(notTransactionField("TotalForeignValue"))).onOutflow().onSection("ABCDEG"),
                          failure("inv_zar2", "I??", "A ZAR equivalent amount cannot be provided if the the Currency Amount is provided",
                              notEmpty.and(hasTransactionField("TotalForeignValue"))).onOutflow().onSection("ABCDEG")
                      ]
                  },
                  {
                    field: ["Resident.Individual.TaxClearanceCertificateReference", "Resident.Entity.TaxClearanceCertificateReference"],
                    rules: [
                        ignore('tccr1'),
                        failure('inv_tccr2', "I??", 'Tax Clearance Certificate Reference number can only contain "/" and alphanumeric characters e.g. 0700/3/2016/A000033069',
                            notEmpty.and(notPattern('^([a-zA-Z0-9]+\\/{0,1}){0,}[a-zA-Z0-9]+$')))
                            .onSection("AB"),
                        failure('inv_tccr3', "I??", 'The Tax Clearance Certificate Reference number is required',
                                 isEmpty.and(hasAnyMoneyFieldValue('TaxClearanceCertificateIndicator', 'Y'))).onSection("AB")
                    ]
                },
                  // ,
                  // {
                  //   field: ["Resident.Individual.TaxClearanceCertificateIndicator", "Resident.Entity.TaxClearanceCertificateIndicator"],
                  //   len: 1,
                  //   rules: [
                  //     failure('inv_tcci1', 327, 'Must be completed',
                  //       notValueIn(['Y', 'N'])).onOutflow().onSection("AB").onCategory(['512', '513']),
                      
                  //   ]
                  // }
              ]
          }
      };

      return transaction;
  }

});

