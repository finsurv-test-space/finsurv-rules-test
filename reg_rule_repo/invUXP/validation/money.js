define(function() {
  return function(predef) {
    var money;

    with (predef) {
      money = {
        ruleset: "Money Rules for Flow Form",
        scope: "money",
        validations: [
          {
              field: "ForeignValue",
              rules: [
              failure("mfv1", 348, "Sequence Amount must be completed",
              isEmpty.and(notCurrencyIn("ZAR")).and(notTransactionField("ZAREquivalent")))
              .onInflow().onSection("ABCDEG"),
              failure("mfv3", 355, "If Currency is not ZAR, Sequence Amount must be completed",
              isEmpty.and(notCurrencyIn("ZAR")).and(notTransactionField("ZAREquivalent")))
              .onSection("ABCDEG"),
              failure("inv_mfv1", 348, "Sequence Amount must be completed",
              isEmpty.and(notCurrencyIn("ZAR")).and(notTransactionField("ZAREquivalent"))).onOutflow().onSection("ABCDEG")
              ]
          },
          {
            field: "TaxClearanceCertificateIndicator",
            rules: [
              message("inv_tcci1", 327, "Must be completed")
            ]
          }
        ]
      };
    }

    return money;
  };
});
