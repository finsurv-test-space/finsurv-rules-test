define(function () {
  return function (predef) {
    var extTrans;
    with (predef) {

      extTrans = {
        ruleset: "External Transaction Rules",
        scope: "transaction",
        validations: [
          {
            field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],
            rules: [
              validate("ext_ccn1", "Validate_ImportUndertakingCCN", //323, "Not a registered import undertaking client (the Flow is OUT and category is 102/01 to 102/10 or 104/01 to 104/10 is used)"
                notEmpty.and(hasPattern(/^\d{8}$/))),
              validate("ext_ccn2", "Validate_ValidCCN", //322, "Not a registered customs client number"
                notEmpty.and(hasPattern(/^\d{8}$/))).onOutflow().onSection("AB").notOnCategory(['102/11', '104/11']).onCategory(['102', '104', '106']),
            ]
          },
          {
            field: "ReplacementOriginalReference",
            minLen: 1,
            maxLen: 30,
            rules: [
              validate("ext_repot1", "Validate_ReplacementTrnReference",
                notEmpty.and(hasTransactionFieldValue("ReplacementTransaction", "Y"))).onSection("ABCDEFG")
            ]
          }
        ]
      };

    }
    return extTrans;
  }
});

