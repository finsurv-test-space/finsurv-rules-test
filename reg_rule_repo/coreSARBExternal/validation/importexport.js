define(function () {
  return function (predef) {
    var extImportExport;
    with (predef) {

      extImportExport = {
        ruleset: "External ImportExport Rules",
        scope: "importexport",
        validations: [
          {
            field: "UCR",
            rules: [
              validate("ext_ieucr1", "Validate_ValidCCNinUCR", //514, "Invalid customs client number completed in UCR"
                notEmpty.and(hasPattern(/^[0-9]ZA[0-9]{8}.+$/))).onInflow().onSection("ABG")
            ]
          }
          //,
          //{
          //  field: "MRNNotOnIVS",
          //  rules: [
          //    validate("ext_ieivs1", "Validate_MRNonIVS", //221, "If outflow and the category 103/01 to 103/10 or 105 or 106, and no MRN can be found on the IVS, must contain the value Y"
          //      //232, "If the MRN is registered on the IVS, it must not be completed"
          //      hasImportExportField("ImportControlNumber")).onOutflow().onSection("ABG").onCategory(["103", "105", "106"]).notOnCategory("103/11")
          //  ]
          //}
        ]
      };

    }
    return extImportExport;
  }
});
