define(function () {
  return function (predef) {

    var summaryTrans, summaryMoney, summaryImportExport;
    with (predef) {
      summaryTrans = {
        ruleset: "Summary Diplay feature test Transaction Display Rules",
        scope: "transaction",
        fields: []
      };

      summaryMoney = {
        ruleset: "Summary Diplay feature test Money Display Rules",
        scope: "money",
        fields: []
      };

      summaryImportExport = {
        ruleset: "Summary Diplay feature test Import Export Display Rules",
        scope: "importexport",
        fields: []
      };
    }

    return {
      summaryTrans: summaryTrans,
      summaryMoney: summaryMoney,
      summaryImportExport: summaryImportExport
    }
  }
});