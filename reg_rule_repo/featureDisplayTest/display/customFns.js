define(function(require) {
  return function(app, config) {



    var superInit = function () { };

    if (config.dataPreTransformFn) superInit = config.dataPreTransformFn;

    var _config = {
        dataPreTransformFn: function (data) {
            superInit(data);
            //custom transform functions go here...

            alert("So much display!")
            //scope.$digest();
        }
    }

    Object.assign(config, _config);

    return config;

  };
});
