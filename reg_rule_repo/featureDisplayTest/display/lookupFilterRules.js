/**
 * Created by petruspretorius on 06/01/2016.
 */
define(function () {
  return function (predef) {

    var filterLookupRules;
    with (predef) {
      filterLookupRules = {
        filterLookupTrans: {
          ruleset: "Diplay feature test Transaction Lookup Filter Rules (NBOL)",
          scope: "transaction",
          fields: []
        },

        filterLookupMoney: {
          ruleset: "Diplay feature test Monetary Lookup Filter Rules",
          scope: "money",
          fields: [
            {
              field: "ThirdPartyKind",
              display: [
                // "If CategoryCode 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onOutflow().onSection("AB").onCategory(["512", "513"]),

                // "If the category is 256 and the PassportNumber under Resident Individual contains no value, Individual must be completed",
                limitValue(["Individual"], notTransactionField("Resident.Individual.PassportNumber")).onSection("AB").onCategory("256"),

                // "If the category is 255 or 256 and the Resident Entity element is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onSection("AB").onCategory(["255", "256"]),

                // "If the SupplementaryCardIndicator is Y, Individual must be completed"
                limitValue(["Individual"], hasResidentFieldValue("SupplementaryCardIndicator", "Y")).onSection("E"),

                // "If the Flow is IN and category 303, 304, 305, 306, 416 or 417 is used and Resident Entity is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onInflow().onSection("AB").onCategory(["303", "304", "305", "306", "416", "417"]),

                // "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, Individual must be completed",
                limitValue(["Individual"], hasTransactionField("Resident.Entity")).onInflow().onSection("A").onCategory(["511", "516"])
              ]
            }
          ]
        },

        filterLookupImportExport: {
          ruleset: "Diplay feature test Import Export Lookup Filter Rules",
          scope: "importexport",
          fields: []
        }
      }
    }

    return filterLookupRules;
  }
});


