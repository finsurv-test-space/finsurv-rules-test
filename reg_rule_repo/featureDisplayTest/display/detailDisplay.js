define(function() {
  return function(predef) {
    var display;

    with (predef) {
      display = {
        detailTrans: {
          ruleset: "Diplay feature test Transaction Display Rules",
          scope: "transaction",
          fields: [
            {
              field: ["TotalForeignValue"],
              display: [
                setValue(function(){
                  alert("So much setting of data!");
                })
               
              ]
            }
          ]
        },
        detailMoney: {
          ruleset: "Diplay feature test Money Display Rules",
          scope: "money",
          fields: []
        },
        detailImportExport: {
          ruleset: "Diplay feature test Import/Export Display Rules",
          scope: "importexport",
          fields: []
        }
      };
    }

    return display;
  };
});
