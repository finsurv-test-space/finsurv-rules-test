define(function () {
    return function (predef) {
      var extTrans;
      with (predef) {
  
        extTrans = {
          ruleset: "Standard Transaction Rules for Standard Bank",
          scope: "transaction",
          validations: [
            {
              field: "",
              rules: [
                // Ignoring external CCN validations for Standard Bank
                ignore('ext_ccn1'),
                ignore('ext_ccn2')

              ]
            }
          ]
        };
  
      }
      return extTrans;
    }
  });