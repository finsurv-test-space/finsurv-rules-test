define(function () {
    return function (predef) {
      var extTrans;
      with (predef) {
  
        extTrans = {
          ruleset: "Standard Money Rules for Standard Bank",
          scope: "money",
          validations: [
            {
                field: "",
                rules: [
                  // Ignoring external CCN validations for Standard Bank
                  ignore('ext_tpccn1')
                ]
            },
            {
              field: "CategoryCode",
              rules: [
                failure("mcc7", 416, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN or OUT in cases where the Resident Entity element is completed, the third party individual and address details must be completed",
                hasTransactionField("Resident.Entity").and(
                  notMoneyField("ThirdParty.Individual.Surname").or(notMoneyField("ThirdParty.StreetAddress.AddressLine1")).or(notMoneyField("ThirdParty.PostalAddress.AddressLine1"))
                )).onSection("A").onCategory(["511", "516"]),
                //Note: This differs from the SARB spec. LIBRA-2200
                failure("mcc10", 425, "If Bop Category 511/01 to 511/07 or 516 is used and flow is IN or OUT in cases where the EntityCustomer element is completed, IndividualThirdPartyIDNumber must be completed",
                hasTransactionField("Resident.Entity").and(notMoneyField("ThirdParty.Individual.IDNumber"))).onSection("A").onCategory(["511", "516"]),
              ]
            },
            {
              field: "ThirdParty.Entity.Name",
              rules: [
                failure("mtpenm4", "S416", "This must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Entity"))).onSection("AB"),
              ]
            },
            {
              field: "ThirdParty.Individual.Surname",
              rules: [
                failure("mtpisn9", 416, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN or OUT in cases where the Resident Entity element is completed, this must be completed",
                  isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("A").onCategory(["511", "516"]),
                failure("mtpisn12", "S416", "This must be completed",
                  isEmpty.and(hasMoneyField("ThirdParty.Individual"))).onSection("AB"),
              ]
            },
            {
              field: "ThirdParty.TaxNumber",
              rules: [
                failure("mtptx4", 439, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed",
                  isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("AB").onCategory(["511", "516"]),
                failure("mtptx6", 546, "ThirdPartyTaxNumber must be completed for registered agents",
                  isEmpty.and(hasMoneyField("ThirdParty.Individual").or(hasMoneyField("ThirdParty.Entity")))).onOutflow().onSection("AB").onCategory(["101", "103", "105", "106"]).notOnCategory(["101/11", "103/11"]),
                failure('mtptx7', 325, "Invalid tax number",
                  notEmpty.and(not(isValidZATaxNumber))).onCategory("512").onSection("AB"),
              ]
            },
            {
              field: "ThirdParty.CustomsClientNumber",
              rules: [
                failure("mtpccn6", 396, "ThirdPartyCustomsClientNumber must be completed for registered agents",
                  isEmpty.and(hasMoneyField("ThirdParty.Individual").or(hasMoneyField("ThirdParty.Entity")))).onOutflow().onSection("AB").onCategory(["101", "103", "105", "106"]).notOnCategory(["101/11", "103/11"]),
              ]
            },
            {
              field: "ThirdParty.StreetAddress.AddressLine1",
              rules: [
                failure("mtpsal13", 456, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN or OUT in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("A").onCategory(["511", "516"])
              ]
            },
            {
              field: "ThirdParty.PostalAddress.AddressLine1",
              rules: [
                failure("mtppal13", 512, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN or OUT in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("A").onCategory(["511", "516"])
              ]
            }
          ]
        };
  
      }
      return extTrans;
    }
  });