define(function () {
  return function (predef) {

    var display;

    with (predef) {
      display = {
        detailTrans: {
          ruleset: "Std Bank Transaction Display Rules",
          scope: "transaction",
          fields: [
           
            {
              field: "BranchCode",
              display: [
                show()
              ]
            },
            {
              field: "BranchName",
              display: [
                show()
              ]
            },
            {
              field: "HubCode",
              display: [
                show()
              ]
            },
            {
              field: "HubName",
              display: [
                show()
              ]
            },
            {
              field: ["Resident.Individual.CustomsClientNumber", "Resident.Entity.CustomsClientNumber"],
              display: [
                show()
              ]
            },
            {
              field: "ValueDate",
              display: [
                show(),
                disable(isTransactionFieldProvided("ValueDate"))
              ]
            },
            {
              field: "FlowCurrency",
              display: [
                disable(isTransactionFieldProvided("FlowCurrency"))
              ]
            },
            {
              field: "TotalForeignValue",
              display: [
                disable()
              ]
            }
          ]
        },
        detailMoney: {
          ruleset: "Std Bank Money Display Rules",
          scope: "money",
          fields: [
            {
              field: "{{LocalValue}}",
              display: [
                show().onSection("ABCDEG"),
                disable(isCurrencyIn("ZAR")).onSection("ABCDEG"),
                hide().onSection("F"),
                hide(isCurrencyIn(["ZAR"])),
                // setValue("%s", "ForeignValue", hasTransactionFieldValue("FlowCurrency", "ZAR")).onSection("ABCDEG"),
                // disable(hasCustomField('TotalDomesticAmount')).onSection("ABCDEG"),
                // setValue(function (context, ind) {
                //   var exchangeRatio = Number(context.getCustomValue("TotalDomesticAmount")) /
                //     Number(context.getTransactionField("TotalForeignValue"));
                //   var result = Number(context.getMoneyField(ind, "ForeignValue")) * exchangeRatio;
                //   return result.toFixed(2);
                // }, hasCustomField('TotalDomesticAmount').and(notCurrencyIn(["ZAR"]))).onSection("ABCDEG"),
                setValue(function (context, ind) {
                  var result = Number(context.getMoneyField(ind, "ForeignValue"));
                  return result.toFixed(2);
                }, hasCustomField('TotalDomesticAmount').and(isCurrencyIn(["ZAR"]))).onSection("ABCDEG")
              ]
            },
            {
              /*mtaAccounts item
               {
               accountNumber : "12345678",
               MTA : "GLOBAL",
               rulingSection : "baz",
               ADLALevel : "2",
               isADLA : true
               }
               */
              field: "MoneyTransferAgentIndicator",
              display: [
                setValue(function (context, ind) {
                  var AccountNumber =
                    context.getTransactionField('Resident.Individual.AccountNumber') |
                    context.getTransactionField('Resident.Entity.AccountNumber');

                  var mtaAccount =
                    context.getLookups().mtaAccounts.find(function (item) {
                      return item.accountNumber == AccountNumber;
                    });

                  return (!!mtaAccount ? mtaAccount.MTA : "");

                }, evalTransactionField("Resident.Individual.AccountNumber", isInLookup('mtaAccounts', 'accountNumber'))
                  .or(evalTransactionField("Resident.Entity.AccountNumber", isInLookup('mtaAccounts', 'accountNumber')))
                ).onCategory(['833']).onSection("A"),
                setValue('AD', null, isEmpty).onSection("C")
              ]
            },
            {
              field: "{{Regulator}}Auth.RulingsSection",
              display: [
                hide().onSection("CDEF"),
                hide().onSection("AB").onCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
                setValue(function (context, ind) {
                  var AccountNumber =
                    context.getTransactionField('Resident.Individual.AccountNumber') |
                    context.getTransactionField('Resident.Entity.AccountNumber');

                  var mtaAccount =
                    context.getLookups().mtaAccounts.find(function (item) {
                      return item.accountNumber == AccountNumber;
                    });

                  // return ((!!mtaAccount && (mtaAccount.isADLA)) ? mtaAccount.rulingSection : "");
                  return (!!mtaAccount ? mtaAccount.rulingSection : "");
                }, 
                evalTransactionField("Resident.Individual.AccountNumber", isInLookup('mtaAccounts', 'accountNumber'))
                  .or(evalTransactionField("Resident.Entity.AccountNumber", isInLookup('mtaAccounts', 'accountNumber')))
                ).onCategory(['833']).onSection("A")
              ]
            },
            {
              field: "SWIFTDetails",
              display: [

                show()
              ]
            },
            {
              field: "LoanTenorType",
              display: [
                hide(),
                show().onSection("AB").onCategory(["801", "802", "803", "804"]),
                show().onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"])
              ]
            },
            {
              field: "LoanTenorMaturityDate",
              display: [
                hide(),
                show(hasMoneyFieldValue("LoanTenorType", ["MATURITY DATE"])).onSection("AB").onCategory(["801", "802", "803", "804"]),
                show(hasMoneyFieldValue("LoanTenorType", ["MATURITY DATE"])).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"])
              ]
            },
            {
              field: "AdHocRequirement.Description",
              display: [
                hide(),
                show(hasMoneyField("AdHocRequirement.Subject")).onSection("ABG"),
                setValue("NONE", null,
                  hasMoneyFieldValue("AdHocRequirement.Subject", "NO")).onSection("A")
              ]
            },
            {
              field: "AdHocRequirement.Subject",
              display: [
                hide().onSection("CDEF")

              ]
            },
            //================================================================================
            // THIRD PARTY
            //================================================================================

            {
              field: "ThirdParty",
              display: [
                hide().onCategory(['ZZ1'])
              ]
            },
            //--------------------
            // for sameAsPhysical
            {
              field: "ThirdParty.PostalAddress.AddressLine1",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.AddressLine1", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false))
              ]
            },
            {
              field: "ThirdParty.PostalAddress.AddressLine1",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.AddressLine1", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false))
              ]
            },
            {
              field: "ThirdParty.PostalAddress.AddressLine2",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.AddressLine2", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false))
              ]
            },
            {
              field: "ThirdParty.PostalAddress.Suburb",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.Suburb", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false))
              ]
            },
            {
              field: "ThirdParty.PostalAddress.City",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.City", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false))
              ]
            },
            {
              field: "ThirdParty.PostalAddress.Province",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.Province", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false))
              ]
            },
            {
              field: "ThirdParty.PostalAddress.PostalCode",
              display: [
                setValue("%s", "ThirdParty.StreetAddress.PostalCode", hasMoneyFieldValue("ThirdParty.sameAsPhysical", true)),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false))
              ]
            },
            {
              field: "ThirdPartyKind",
              display: [
                show(),
                disable(hasMoneyFieldValue('allowAdhocBOPThirdParty', false)),
                setValue('Individual', null, isEmpty.and(hasMoneyField('ThirdParty.Individual'))),
                setValue('Entity', null, isEmpty.and(hasMoneyField('ThirdParty.Entity')))
              ]
            },
            {
              field: "SWIFTDetails",
              display: [
                hide()
              ]
            },
            {
              field: "advancedpayment",
              display: [
                setValue(true, null, isEmpty.or(hasValue(false))).onOutflow().onSection("ABG").onCategory("101"),
                setValue(false, null, isEmpty.or(hasValue(true))).onOutflow().onSection("ABG").notOnCategory("103/11").onCategory(["103", "105", "106"])
              ]
            }
          ]
        },
        detailImportExport: {
          ruleset: "Std Bank Import/Export Display Rules",
          scope: "importexport",
          fields: [

            {
              field: "TransportDocumentNumber",
              display: [
                hide(isEmpty),
                show().onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
              ]
            },
            {
              field: "TransportDocumentNumber",
              display: [
                hide(isEmpty),
                show().onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
              ]
            },
            {
              field: "PaymentValue",
              display: [
                setLabel("Allocated Amount")
              ]
            }
          ]
        }

      }
    }
    return display;
  }
});