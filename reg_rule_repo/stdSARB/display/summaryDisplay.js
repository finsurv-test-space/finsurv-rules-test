define(function () {
  return function (predef) {

    var summaryTrans, summaryMoney, summaryImportExport;
    with (predef) {
      summaryTrans = {
        ruleset: "Summary StdBank Transaction Display Rules",
        scope: "transaction",
        fields: [
          {
            field: [
              "Resident.Individual.StreetAddress.AddressLine1",
              "Resident.Entity.StreetAddress.AddressLine1",
              "Resident.Individual.PostalAddress.AddressLine1",
              "Resident.Entity.PostalAddress.AddressLine1",
              "Resident.Individual.StreetAddress.AddressLine2",
              "Resident.Entity.StreetAddress.AddressLine2",
              "Resident.Individual.PostalAddress.AddressLine2",
              "Resident.Entity.PostalAddress.AddressLine2",
              "Resident.Individual.StreetAddress.Suburb",
              "Resident.Entity.StreetAddress.Suburb",
              "Resident.Individual.PostalAddress.Suburb",
              "Resident.Entity.PostalAddress.Suburb",
              "Resident.Individual.StreetAddress.City",
              "Resident.Entity.StreetAddress.City",
              "Resident.Individual.PostalAddress.City",
              "Resident.Entity.PostalAddress.City",
              "Resident.Individual.StreetAddress.Province",
              "Resident.Entity.StreetAddress.Province",
              "Resident.Individual.PostalAddress.Province",
              "Resident.Entity.PostalAddress.Province",
              "Resident.Individual.StreetAddress.PostalCode",
              "Resident.Entity.StreetAddress.PostalCode",
              "Resident.Individual.PostalAddress.PostalCode",
              "Resident.Entity.PostalAddress.PostalCode",
              "Resident.Individual.ContactDetails.ContactSurname",
              "Resident.Entity.ContactDetails.ContactSurname",
              "Resident.Individual.ContactDetails.ContactName",
              "Resident.Entity.ContactDetails.ContactName",
              "Resident.Individual.ContactDetails.Email",
              "Resident.Entity.ContactDetails.Email",
              "Resident.Individual.ContactDetails.Fax",
              "Resident.Entity.ContactDetails.Fax",
              "Resident.Individual.ContactDetails.Telephone",
              "Resident.Entity.ContactDetails.Telephone"
            ],
            display: [
              setValue("", null, notEmpty).onSection("DF")
            ]
          }

        ]
      };

      summaryMoney = {
        ruleset: "Summary StdBank Money Display Rules",
        scope: "money",
        fields: [
          {
            field: "Description",
            displayOnly: true,
            display: [
              appendValue("%s", categoryDescription)
            ]
          },
          {
            field: "Category",
            display: [
              setValue(),
              appendValue("%s", function (context) {
                var cat = context.lookups.getCategory(context.flow,
                  context.getMoneyField(context.currentMoneyInstance, 'CategoryCode'),
                  context.getMoneyField(context.currentMoneyInstance, 'CategorySubCode'));
                if (cat)
                  return cat.code;
                return "";
              })
            ]
          },
          {
            field: "CategoryCode",
            display: [
              setValue('ZZ1', null, isEmpty).onSection("C")
            ]
          }
        ]
      };

      summaryImportExport = {
        ruleset: "Summary StdBank Import Export Display Rules",
        scope: "importexport",
        fields: []
      };


    }

    return {
      summaryTrans: summaryTrans,
      summaryMoney: summaryMoney,
      summaryImportExport: summaryImportExport
    }
  }
})
  ;
