define(function() {
  return function(testBase) {
      with (testBase) {

        var test_cases = [
            // Money: ThirdParty.TaxNumber
            //If category 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed
            assertSuccess("mtptx1", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '512', CategorySubCode: '04', ThirdParty: {TaxNumber: '123'}}]
            }),
            assertSuccess("mtptx1", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {}},
                MonetaryAmount: [{CategoryCode: '512', CategorySubCode: '04', ThirdParty: {TaxNumber: ''}}]
            }),
            assertFailure("mtptx1", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '512', CategorySubCode: '04', ThirdParty: {TaxNumber: ''}}]
            }),

            //If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed
            assertSuccess("mtptx4", {
                ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '511', CategorySubCode: '04', ThirdParty: {TaxNumber: '123'}}]
            }),
            assertSuccess("mtptx4", {
                ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {}},
                MonetaryAmount: [{CategoryCode: '511', CategorySubCode: '04', ThirdParty: {TaxNumber: ''}}]
            }),
            assertFailure("mtptx4", {
                ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '511', CategorySubCode: '04', ThirdParty: {TaxNumber: ''}}]
            }),

            //If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed
            assertSuccess("mtptx4", {
                ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '516', ThirdParty: {TaxNumber: '123'}}]
            }),
            assertSuccess("mtptx4", {
                ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {}},
                MonetaryAmount: [{CategoryCode: '516', ThirdParty: {TaxNumber: ''}}]
            }),
            assertFailure("mtptx4", {
                ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '516', ThirdParty: {TaxNumber: ''}}]
            }),

            //FIN-1750 / LIBRA-XXXX -- TaxNumber
            assertSuccess("mtptx6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '01', ThirdParty: {TaxNumber: '123'}}]
            }),
            assertSuccess("mtptx6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '01', ThirdParty: {TaxNumber: '123', Entity:{}}}]
            }),
            assertSuccess("mtptx6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '01', ThirdParty: {TaxNumber: '123', Individual:{}}}]
            }),
            assertNoRule("mtptx6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11', ThirdParty: {TaxNumber: '123', Individual:{}}}]
            }),
            assertNoRule("mtptx6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11', ThirdParty: {TaxNumber: '', Individual:{}}}]
            }),
            assertSuccess("mtptx6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '01', ThirdParty: {TaxNumber: ''}}]
            }),
            assertFailure("mtptx6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '01', ThirdParty: {TaxNumber: '', Entity:{}}}]
            }),
            assertFailure("mtptx6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '01', ThirdParty: {TaxNumber: '', Individual:{}}}]
            }),

            assertSuccess("mtptx7", {
              ReportingQualifier: 'BOPCUS',
              MonetaryAmount: [{
                CategoryCode : "512",
                ThirdParty: { TaxNumber: '1125519171' }
              }]
            }),
            assertFailure("mtptx7", {
              ReportingQualifier: 'BOPCUS',
              MonetaryAmount: [{
                CategoryCode : "512",
                ThirdParty: { TaxNumber: '123' }
              }]
            }),

            //FIN-1750 / LIBRA-XXXX -- CCN
            assertSuccess("mtpccn6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '01', ThirdParty: {CustomsClientNumber: '123'}}]
            }),
            assertSuccess("mtpccn6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '01', ThirdParty: {CustomsClientNumber: '123', Entity:{}}}]
            }),
            assertSuccess("mtpccn6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '01', ThirdParty: {CustomsClientNumber: '123', Individual:{}}}]
            }),
            assertNoRule("mtpccn6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11', ThirdParty: {CustomsClientNumber: '123', Individual:{}}}]
            }),
            assertNoRule("mtpccn6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '11', ThirdParty: {CustomsClientNumber: '', Individual:{}}}]
            }),
            assertSuccess("mtpccn6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '01', ThirdParty: {CustomsClientNumber: ''}}]
            }),
            assertFailure("mtpccn6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '01', ThirdParty: {CustomsClientNumber: '', Entity:{}}}]
            }),
            assertFailure("mtpccn6", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                MonetaryAmount: [{CategoryCode: '101', CategorySubCode: '01', ThirdParty: {CustomsClientNumber: '', Individual:{}}}]
            }),

            //FIN-1750 / LIBRA-XXXX -- Name/Surname
            assertNoRule("mtpenm4", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', 
                MonetaryAmount: [{ThirdParty: {}}]
            }),
            assertSuccess("mtpenm4", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', 
                MonetaryAmount: [{ThirdParty: {Entity:{Name:'SomeName'}}}]
            }),
            assertFailure("mtpenm4", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', 
                MonetaryAmount: [{ThirdParty: {Entity:{}}}]
            }),
            assertWarning("ThirdParty.Entity.Name.maxLen", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT',
                MonetaryAmount: [{ThirdParty: {Entity:{Name:'123456789_123456789_123456789_123456789_123456789_1'}}}]
            }),
            assertSuccess("ThirdParty.Entity.Name.maxLen", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT',
                MonetaryAmount: [{ThirdParty: {Entity:{Name:'123456789_123456789_123456789_123456789_123456789'}}}]
            }),
            assertNoRule("mtpisn12", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', 
                MonetaryAmount: [{ThirdParty: {}}]
            }),
            assertSuccess("mtpisn12", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT',
                MonetaryAmount: [{ThirdParty: {Individual:{Surname:'SomeName'}}}]
            }),
            assertFailure("mtpisn12", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT',
                MonetaryAmount: [{ThirdParty: {Individual:{}}}]
            }),

            assertSuccess("mcc10", {
                ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "bla" } },
                MonetaryAmount: [{
                  CategoryCode: "516",
                  ThirdParty: { Individual: { IDNumber: '1234' }}
                }]
            }),
            assertSuccess("mcc10", {
                ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "bla" } },
                MonetaryAmount: [{
                    CategoryCode: "511",
                    CategorySubCode: "01",
                    ThirdParty: { Individual: { IDNumber: '1234' }}
                }]
            }),
            assertFailure("mcc10", {
                ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "bla" } },
                MonetaryAmount: [{
                    CategoryCode: "516",
                    ThirdParty: {}
                }]
            }),
            assertFailure("mcc10", {
                ReportingQualifier: "BOPCUS", Resident: { Entity: { EntityName: "bla" } },
                MonetaryAmount: [{
                    CategoryCode: "511",
                    CategorySubCode: "01",
                    ThirdParty: { Individual: { Surname: 'XYZ' }, PostalAddress: { AddressLine1: 'Postal Home' } }
                }]
            })
        ]

      }
    return testBase;
  }
})
