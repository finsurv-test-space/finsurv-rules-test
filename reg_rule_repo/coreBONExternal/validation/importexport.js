define(function () {
  return function (predef) {
    var extImportExport;
    with (predef) {

      extImportExport = {
        ruleset: "External ImportExport Rules",
        scope: "importexport",
        validations: [
          // {
          //   field: "UCR",
          //   rules: [
          //     validate("ext_ieucr1", "Validate_ValidCCNinUCR", //421, "Invalid customs client number completed in UCR"
          //       notEmpty.and(hasPattern(/^[0-9]ZA[0-9]{8}.+$/))).onInflow().onSection("A")
          //   ]
          // }
        ]
      };

    }
    return extImportExport;
  }
});
