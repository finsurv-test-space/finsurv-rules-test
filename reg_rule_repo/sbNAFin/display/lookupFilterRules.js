define(function () {
  return function (predef) {

    var filterLookupRules;
    with (predef) {
      filterLookupRules = {
        filterLookupTrans: {
          ruleset: "Reporting Transaction Lookup Filter Rules",
          scope: "transaction",
          fields: []
        },

        filterLookupMoney: {
          ruleset: "Reporting Monetary Lookup Filter Rules",
          scope: "money",
          fields: [
            {
              field: "MoneyTransferAgentIndicator",
              display: [
                limitValue(["CARD"]).onSection("EF"),
                limitValue(["BOPDIR"]).onSection("G"),
                // Since dealerType is not being set in the Meta for StandardBank, hard-fiter it out here... 
                excludeValue(['ADLA'])
                // TODO: I don't think the above is correct, we should revisit this and set the dealerType Meta property.
                //limitValue(["ADLA"], dealerTypeADLA).onSection("AB"),
                //excludeValue(["AD", "ADLA", "CARD", "BOPDIR"], dealerTypeAD).onSection("ABCD").onCategory("833"),
                //excludeValue(["ADLA", "CARD", "BOPDIR"], dealerTypeAD).onSection("ABCD").notOnCategory("833")
              ]
            }
           
          ]
        },

        filterLookupImportExport: {
          ruleset: "Reporting Import Export Lookup Filter Rules",
          scope: "importexport",
          fields: []
        }
      }
    }

    return filterLookupRules;
  }
});


