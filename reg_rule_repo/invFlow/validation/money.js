define(function () {
  return function (predef) {
    var money;

    with (predef) {
      money = {
        ruleset: "Money Rules for Flow Form",
        scope: "money",
        validations: [
        {
          field: "",
          rules: [
          //ignore("field.maxLen"),
          message("mrv3", null, "ZAR equivalent and Foreign Currency Amount cannot be the same"),
          message("mrv4", null, "The reported Rand value must be within a certain variance to the Foreign value"),
          message("mrv6", null, "Rand value is too high for the category selected"),
          message("mrv7", null, "Rand value is too high for the category selected"),
          message("mrv8", null, "Rand value is too high for the category selected"),
          message("mfv2", null, "Cannot be negative"),
          message("mfv5", null, "Foreign value is too high"),
          message("mcc1", null, "Code is required").onOutflow(),
          message("mcc1", null, "Please select a reason for payment").onInflow(),
          //
          message("mcc3", null, "Invalid category"),
          message("msc1", null, "Code is invalid"),
          message("mlrn1", null, "Loan Ref number required due to category selected"),
          message("mlrn5", null, "Loan Reference Number must be 99012301230123 for category selected"),
          message("mlrn6", null, "Loan Reference Number must be 99456745674567 for category selected"),
          message("mlrn7", null, "Loan Referefence Number must be 99789078907890 for category selected"),
          message("mlrn8", null, "Loan Reference Number must be completed for the category selected"),
          message("mlrn9", null, "Loan Reference Number must be completed for the category selected"),
          message("mlrn11", null, "Loan Reference Number is not required for the category selected"),
          message("mlt1", null, "The date of maturity in the format CCYY-MM-DD or 'ON DEMAND' if no date is applicable must be completed"),
          message("mlt2", null, "Must be a date in the future if not 'ON DEMAND'"),
          message("mlir4", null, "If category 309/01 to 309/07 is used, this must be completed in format 0.00"),
          message("mlir5", null, "Rate cannot be greater than 100%"),
          message("mexc1", null, "Required field for this category"),
          message("mtpisn1", null, "Required field for this category"),
          message("mtpig1", null, "Gender must be completed"),
          message("mtpig3", null, "Gender must be completed"),
          message("mtpiid2", null, "Required field for this category"),
          message("mtpibd2", null, "Date of Birth must be completed"),
          message("mtpitp2", null, "Not required for category chosen"),
          message("mtpccn1", null, "Customs Client Number must be between 8 and 13 digits"),
          message("mtptx3", null, "May not be the same as Applicant Tax number"),
          message("mtpvn2", null, "May not be the same as Applicant VAT number"),
          message("mtie1", null, "Required field for this category"),
          message("mtie3", null, "Total Sub Sequence Amount may not exceed a 1% variance with the Sequence Amount"),
          message("mtie4", null, "If outflow and the category is 101/01 to 101/10 or 103/01 to 103/10 or 105 or 106, the import/export element must be completed unless a) the Subject is SDA or REMITTANCE DISPENSATION or b) category 106 and import undertaking client"),
          message("SWIFTDetails.minLen", null, "SWIFT details are too short"),
          message("{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber.minLen", null, "Number is too short"),
          message("{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber.minLen", null, "Number is too short"),
          message("ThirdParty.Individual.Surname.minLen", null, "Surname is too short"),
          message("ThirdParty.Individual.Name.minLen", null, "Name is too short"),
          message("ThirdParty.Individual.IDNumber.minLen", null, "ID number is too short"),
          message("ThirdParty.Individual.TempResPermitNumber.minLen", null, "Permit number is too short"),
          message("ThirdParty.Individual.PassportNumber.minLen", null, "Passport number is too short"),
          message("ThirdParty.Entity.RegistrationNumber.minLen", null, "Number is too short"),
          message("ThirdParty.TaxNumber.minLen", null, "Tax number is too short"),
          message("ThirdParty.ContactDetails.ContactSurname.minLen", null, "Surname is too short"),
          message("ThirdParty.ContactDetails.ContactName.minLen", null, "Name is too short"),
          message("ThirdParty.ContactDetails.Email.minLen", null, "Email address is too short"),
          message("ThirdParty.ContactDetails.Fax.minLen", null, "Fax is too short"),
          message("ThirdParty.ContactDetails.Telephone.minLen", null, "Phone number is too short")
          ]
        }, {
          field: "SequenceNumber",
          rules: [
          ignore("mseq1")
          ]
        },
        {
          field: "MoneyTransferAgentIndicator",
          rules: [
          ignore("mta5")
          ]
        },
        // {
        //   field: "RandValue",
        //   rules: [
        //   ignore("mrv1")//,
        //   // failure("inv_mrv1", "I??", "Sequence Amount must be completed",
        //   // isEmpty.and(hasTransactionField("ZAREquivalent").or(isCurrencyIn("ZAR")))).onOutflow().onSection("ABCDEG")
        //   ]
        // },
        // {
        //   field: "ForeignValue",
        //   rules: [
        //   failure("mfv1", 348, "Sequence Amount must be completed",
        //   isEmpty.and(notCurrencyIn("ZAR")).and(notTransactionField("ZAREquivalent")))
        //   .onInflow().onSection("ABCDEG"),
        //   failure("mfv3", 355, "If Currency is not ZAR, Sequence Amount must be completed",
        //   isEmpty.and(notCurrencyIn("ZAR")).and(notTransactionField("ZAREquivalent")))
        //   .onSection("ABCDEG"),
        //   failure("inv_mfv1", 348, "Sequence Amount must be completed",
        //   isEmpty.and(notCurrencyIn("ZAR")).and(notTransactionField("ZAREquivalent"))).onOutflow().onSection("ABCDEG")
        //   ]
        // },
        {
          field: "CategoryCode",
          rules: [
          failure('inv_mcc1', 294, "Must be South African Resident to use codes 511, 512, 513, or 516",
          notTransactionFieldValue("AccountHolderStatus", "South African Resident")
          ).onSection("AB")
          .onCategory(["511", "512", "513", "516"]),
          ignore("mcc6")
          //,failure("inv_mcc6", 416, "If CategoryCode 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, the third party individual and address details must be completed",
          //hasTransactionField("Resident.Entity").and(
          //  notMoneyField("ThirdParty.Individual.Surname").or(notMoneyField("ThirdParty.StreetAddress.AddressLine1")).or(notMoneyField("ThirdParty.PostalAddress.AddressLine1"))
          //)).onOutflow().onSection("A")
          //    .onCategory(["512", "513"])
          //    .notOnCategory(["512/04"])
          ]
        },
        {
          field: "ReversalTrnRefNumber",
          rules: [
          ignore("mrtrn1")
          ]
        },
        {
          field: "ReversalTrnSeqNumber",
          rules: [
          ignore("mrtrs1")
          ]
        },
        {
          field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthRefNumber",
          rules: [
          ignore("masar1")
          ]
        },
        {
          field: "TaxClearanceCertificateIndicator",
          rules: [
          failure("inv_tcci1", "I??", "If category 512 or 513 is used, must be completed",
          isEmpty).onSection("AB").onOutflow().onCategory(["512", "513"]),
          failure("inv_tcci2", "I??", "Unless category 512 or 513 is used, this must not be completed",
          notEmpty).onSection("AB").onOutflow().notOnCategory(["512", "513"])
          ]
        },
        {
          field: "CannotCategorize",
          rules: [
          failure("inv_mexc1", "I32", "If category 830 is used, must be completed",
          notEmpty).onSection("AB").notOnCategory("830"),
          failure("inv_mexc2", "I60", "This field is too long and must be shortened to 100 characters", notEmpty.and(isTooLong(100))).onSection("A")
          ]
        },
        {
          field: "AdHocRequirement.Subject",
          rules: [
          ignore("madhs8")
          ]
        },
        {
          field: "LoanRefNumber",
          rules: [
          failure("mlrn2", 374, "If category is 801, or 802, or 803 or 804 and 'Country where service was requested from or where the merchandise was exported to' is LS, the Loan Reference Number must be 99012301230123",
          notValue("99012301230123").and(hasTransactionFieldValue("LocationCountry", "LS"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
          failure("mlrn3", 374, "If category is 801, or 802, or 803 or 804 and 'Country where service was requested from or where the merchandise was exported to' is SZ, the Loan Reference Number must be 99456745674567",
          notValue("99456745674567").and(hasTransactionFieldValue("LocationCountry", "SZ"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
          failure("mlrn4", 374, "If category is 801, or 802, or 803 or 804 and 'Country where service was requested from or where the merchandise was exported to' is NA, the Loan Reference Number must be 99789078907890",
          notValue("99789078907890").and(hasTransactionFieldValue("LocationCountry", "NA"))).onSection("ABG").onCategory(["801", "802", "803", "804"]),
          failure("inv_mlrn1", "I??", "This field is too long and must be shortened to 20 characters",
          notEmpty.and(isTooLong(20))).onSection("A")
          ]
        },
        {
          field: "LoanInterestRate",
          rules: [
          ignore("mlir1"),
          ignore("mlir3"),
          ignore("mlir4")
          ]
        },
        {
          field: "LoanTenor",
          rules: [
          failure("inv_mlt1", "I??", "Loan Tenor required due to category selected",
          notEmpty).onOutflow().onSection("AB").notOnCategory(["810", "815", "816", "817", "818", "819"])
          ]
        },
        {
          field: "LoanInterest.BaseRate",
          rules: [
          failure("inv_mlibr1", "I??", "Base rate required due to category selected",
          notEmpty).onOutflow().onSection("AB").notOnCategory(["810", "815", "816", "817", "818", "819"])
          ]
        },
        {
          field: "LoanInterest.Term",
          rules: [
          failure("inv_mlit1", "I??", "Term required as Base Rate is not Prime Rate",
          isEmpty.and(hasMoneyField("LoanInterest.BaseRate").and(notMoneyFieldValue("LoanInterest.BaseRate", ["FIXED", "PRIME"])))),
          failure("inv_mlit2", "I??", "Unless the Interest Base Rate is used (and this rate needs a term) this must not be provided",
          notEmpty.and(notMoneyField("LoanInterest.BaseRate").or(hasMoneyFieldValue("LoanInterest.BaseRate", ["FIXED", "PRIME"]))))
          ]
        },
        {
          field: "LoanInterest.PlusMinus",
          rules: [
          failure("inv_mlipm1", "I??", "Required field for loans",
          isEmpty.and(hasMoneyField("LoanInterest.BaseRate")).and(notMoneyFieldValue("LoanInterest.BaseRate", ["FIXED", "PRIME"]))),
          failure("inv_mlipm12", "I??", "Unless the Interest Base Rate is used this must not be provided",
          notEmpty.and(notMoneyField("LoanInterest.BaseRate").or(hasMoneyFieldValue("LoanInterest.BaseRate", ["FIXED", "PRIME"]))))
          ]
        },
        {
          field: "LoanInterest.Rate",
          rules: [
          failure("inv_mlir1", 378, "Rate required due to category selected",
          notPattern("^\\d+(\\.\\d{2})$")).onOutflow().onSection("AB").onCategory(["810", "815", "816", "817", "818", "819"]),
          failure("inv_mlir3", 380, "Rate required due to category selected",
          notPattern("^\\d{1,3}\\.\\d{2}?$")).onOutflow().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
          failure("inv_mlir4", 380, "If the Flow is In and CategoryCode 309/01 to 309/07 is used, must be completed reflecting the percentage interest paid e.g. 7.20",
          notPattern("^\\d{1,3}\\.\\d{2}?$")).onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
          warning("inv_mlir5", "S12", "Rate should not be greater than 100%",
          hasPattern("^\\d{3}\\.\\d{2}?$")).onOutflow().onSection("ABG").onCategory(["309/04", "309/05", "309/06", "309/07"]),
          warning("inv_mlir6", "S12", "It is unlikely that an interest rate greater than 100% is being charged",
          hasPattern("^\\d{3}\\.\\d{2}?$")).onInflow().onSection("ABG").onCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"]),
          failure("inv_mlir7", "I??", "Unless the category 810, 815, 816, 817, 818, 819, 309/04, 309/05, 309/06 or 309/07 is used is used this must not be provided",
          notEmpty).onOutflow().onSection("AB").notOnCategory(["810", "815", "816", "817", "818", "819", "309/04", "309/05", "309/06", "309/07"]),
          failure("inv_mlir8", "I??", "Unless the category 309/01, 309/02, 309/03, 309/04, 309/05, 309/06 or 309/07 is used is used this must not be provided",
          notEmpty).onInflow().onSection("ABG").notOnCategory(["309/01", "309/02", "309/03", "309/04", "309/05", "309/06", "309/07"])
          ]
        },
        {
          field: "LocationCountry",
          rules: [
          ignore("mlc1"),
          ignore("mlc2"),
          ignore("mlc3"),
          ignore("mlc4")
          ]
        }, {
          field: "{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber",
          rules: [
          ignore("masan1"),
          failure("inv_masan1", "I33", "If the SARB Auth Date is captured then SARB Auth Number must be provided",
          isEmpty.and(hasMoneyField("{{Regulator}}Auth.SARBAuthDate"))).onSection("ABG"),
          failure("inv_masan2", "I??", "The SARB Auth Number must be provided for category 105 and 106",
          isEmpty).onOutflow().onSection("ABG").onCategory(["105", "106"])
          ]
        },
        {
          field: "{{Regulator}}Auth.SARBAuthDate",
          rules: [
          failure("inv_masad1", "I12", "If the SARB Auth Number is captured then SARB Auth Date must be provided",
          isEmpty.and(hasMoneyField("{{Regulator}}Auth.{{RegulatorPrefix}}AuthAppNumber"))).onSection("ABG"),
          failure("inv_masad2", "I26", "Must be in a valid date format: YYYY-MM-DD",
          notEmpty.and(notPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/))).onSection("ABG"),
          warning("inv_masad3", "I27", "This SARB Auth Date is in the future",
          notEmpty.and(isDaysInFuture(0))).onSection("ABG")
          ]
        },
        {
          field: "{{Regulator}}Auth.RulingsSection",
          rules: [
          ignore("mars1")
          ]
        },
        {
          field: "{{Regulator}}Auth.{{DealerPrefix}}InternalAuthNumber",
          rules: [
          ignore("maian1")
          ]
        },
        {
          field: "ThirdParty.TaxNumber",
          rules: [
          ignore("mtptx1")
          //,failure("inv_mtptx1", 439, "If category 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed",
          //    isEmpty.and(hasTransactionField("Resident.Entity")))
          //    .onOutflow().onSection("AB").onCategory(["512", "513"])
          //    .notOnCategory(["512/04"])
          ]
        },
        {
          field: "TravelMode.TravellerStatus",
          rules: [
          failure("inv_tpits1", "I08", "Traveller Status must be completed",
          isEmpty).onSection("AB").onCategory(["255", "256"]),
          failure("inv_tpits2", "I09", "If category 256 is used, and the Applicant is the traveller then the Applicant Passport Number must be completed",
          hasValue("Account Holder").and(notTransactionField("Resident.Individual.PassportNumber"))).onSection("AB").onCategory("256"),
          failure("inv_tpits3", "I10", "If category 255 or 256 is used, Applicant information cannot be prepopulated because the Applicant Type is an Entity",
          hasValue("Account Holder").and(hasTransactionField("Resident.Entity"))).onSection("AB").onCategory(["255", "256"]),
          failure("inv_tpits4", "I11", "If category 303, 304, 305, 306, 416 or 417 is used, Applicant information cannot be prepopulated because the Applicant Type is an Entity",
          isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("AB").onCategory(["303", "304", "305", "306", "416", "417"]),
          failure("inv_tpits5", "I08", "Not required for category chosen",
          notEmpty).onSection("AB").notOnCategory(["255", "256"])
          ]
        },
        {
          field: "ThirdParty.Individual.Surname",
          rules: [
          ignore("mtpisn1"),
          //failure("inv_mtpisn1", 416, "If CategoryCode 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, it must be completed",
          //    isEmpty.and(hasTransactionField("Resident.Entity")))
          //    .onOutflow().onSection("AB").onCategory(["512", "513"])
          //    .notOnCategory(["512/04"]),
          ignore("mtpisn2"),
          failure("inv_mtpisn2", 416, "This must be completed",
          isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", [
          'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
          ]))).onSection("AB")
          .onCategory(["255", "256"]),
          ignore("mtpisn3"),
          ignore("mtpisn4"),
          ignore("mtpisn5"),
          ignore("mtpisn8"),
          failure("inv_mtpisn0", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onOutflow()
          .notOnCategory(["255", "256", "512", "513"]),
          failure("inv_mtpisn2", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onInflow().notOnCategory(["255", "256", "303", "304", "305", "306", "416", "417"]),
          failure("inv_mtpisn3", "I??", "Not required if Applicant is selected",
          notEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", ["Account Holder"]))).onSection("AB").onCategory(["255", "256"]),
          failure("inv_mtpisn_l1", "I84", "Surname cannot be longer than 35 characters", notEmpty.and(isTooLong(35)))
          .onSection("AB")
          //, failure("inv_mtpisn4", 416, "This must be completed", isEmpty)
          //  .onOutflow().onSection("AB")
          //    .onCategory(["512", "513"])
          //  .notOnCategory(["512/04"])
          ]
        },
        //{
        //    field: "ThirdParty.Individual.MiddleNames",
        //    rules: [
        //      ignore("mtpcdn1"),
        //      failure("inv_mtpcdn1", "I??", "Not required for category chosen",
        //        notEmpty).onSection("AB").onOutflow().notOnCategory(["255", "256", "512", "513"]),
        //      failure("inv_mtpcdn2", "I??", "Not required for category chosen",
        //        notEmpty).onSection("AB").onInflow().notOnCategory(["255", "256", "303", "304", "305", "306", "416", "417"]),
        //      failure("inv_mtpimn_l1", "I85", "Name cannot be longer than 50 characters", notEmpty.and(isTooLong(50))).onSection("AB")
        //    ]
        //},
        {
          field: "ThirdParty.Individual.Name",
          rules: [
          ignore("mtpinm1"),
          ignore("mtpinm2"),
          ignore("mtpinm3"),
          ignore("mtpinm4"),
          failure("inv_mtpinm0", 419, "This must be completed",
          isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", [
          'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
          ]))).onSection("AB").onCategory(["255", "256"]),
          failure("inv_mtpinm_l1", "I83", "Name cannot be longer than 50 characters",
          notEmpty.and(isTooLong(50))).onSection("AB"),
          failure("inv_mtpinm2", "I??", "Not required for category chosen",
          notEmpty)
          .onSection("AB").onInflow().notOnCategory(["255", "256", "303", "304", "305", "306", "416", "417"]),
          failure("inv_mtpinm1", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onOutflow()
          .notOnCategory(["255", "256", "512", "513"])
          //, failure("inv_mtpinm3", "I??", "This must be completed", isEmpty)
          //  .onOutflow().onSection("AB")
          //    .onCategory(["512/01", "512/02", "512/03", "512/05", "512/06", "512/07", "513"])
          ]
        },
        {
          field: "ThirdParty.Individual.Gender",
          rules: [
          ignore("mtpig4"),
          failure("inv_mtpig1", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onOutflow().notOnCategory(["255", "256", "512/01", "512/02", "512/03", "512/05", "512/06", "512/07", "513"]),
          failure("inv_mtpig2", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onInflow().notOnCategory(["255", "256", "303", "304", "305", "306", "416", "417", "511", "516"])
          ]
        },
        {
          field: "ThirdParty.Individual.IDNumber",
          rules: [
          failure("mtpiid1", 424, "ID number is required",
          isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", "South African Resident"))).onSection("AB").onCategory(["255", "256"]),
          ignore("mtpiid2"),
          //failure("mtpiid2", 425, "If category 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed",
          //    isEmpty.and(hasTransactionField("Resident.Entity")))
          //      .onOutflow().onSection("AB")
          //         .onCategory(["512", "513"])
          //         .notOnCategory(["512/04"]),
          failure("mtpiid3", 426, "This is an invalid ID number",
          notEmpty.and(notValidRSAID)).onSection("ABEG"),
          failure("mtpiid6", 553, "Cannot be the same as Applicant ID number",
          notEmpty.and(matchesTransactionField("Resident.Individual.IDNumber"))).onSection("ABEG"),
          failure("inv_mtpiid1", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onOutflow().notOnCategory(["255", "256", "512", "513"]),
          failure("inv_mtpiid2", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onInflow().notOnCategory(["255", "256", "303", "304", "305", "306", "416", "417", "511", "516"])
          //, failure("inv_mtpiid3", 424, "ID number is required", isEmpty)
          //  .onOutflow().onSection("AB")
          //          .onCategory(["512", "513"])
          //          .notOnCategory(["512/04"])
          ]
        },
        {
          field: "ThirdParty.Individual.DateOfBirth",
          rules: [
          failure("mtpibd1", 428, "Date of birth is required",
          isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", [
          'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
          ]))).onSection("AB").onCategory(["255", "256"]),
          failure("mtpibd3", 428, "Date of birth is required",
          isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", [
          'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
          ])))
          .onSection("AB").onCategory("255"),
          failure("inv_mtpibd1", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onOutflow().notOnCategory(["255", "256", "512", "513"]),
          failure("inv_mtpibd2", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onInflow().notOnCategory(["255", "256", "303", "304", "305", "306", "416", "417"]),
          failure("inv_mtpibd3", "I??", "The date of birth should match the ID Number",
          notEmpty.and(hasMoneyField("ThirdParty.Individual.IDNumber")
          .and(notMatchThirdPartyDateToIDNumber)))
          .onSection("AB")
          .onCategory(["255", "256"])
          //, failure("inv_mtpibd4", 428, "Date of birth is required", isEmpty)
          //  .onOutflow().onSection("AB").onCategory(["512", "513"]).notOnCategory(["512/04"])
          //, failure("inv_mtpibd5", "I??", "Date of birth does not match ID",
          //    hasMoneyField("ThirdParty.Individual.IDNumber")
          //        .and(notValue(dateOfBirthFromSAID("ThirdParty.Individual.IDNumber")))
          //  ).onCategory(["255", "256", "512", "513"])
          ]
        },
        {
          field: "ThirdParty.Individual.TempResPermitNumber",
          rules: [
          failure("mtpitp1", 424, "Temporary resident permit number is required",
          isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", "Foreign Temporary Resident"))).onSection("AB").onCategory(["255", "256"]),
          failure("mtpitp5", 554, "Cannot be the same as Applicant Temp Resident Permit Number",
          notEmpty.and(matchesTransactionField("Resident.Individual.TempResPermitNumber"))).onSection("ABEG"),
          failure("inv_mtpitp1", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onOutflow().notOnCategory(["255", "256", "512", "513"]),
          failure("inv_mtpitp2", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onInflow().notOnCategory(["255", "256", "303", "304", "305", "306", "416", "417"]),
          failure("inv_mtpitp_l1", "I82", "Permit number limited to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
          ]
        },
        {
          field: "ThirdParty.Individual.TempResExpiryDate",
          rules: [
          failure("inv_mtpite1", 424, "This must be completed",
          isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", "Foreign Temporary Resident"))).onSection("AB").onCategory(["255", "256"]),
          failure("inv_mtpite2", "I28", "Must be in a valid date format: YYYY-MM-DD",
          notEmpty.and(notPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/))).onSection("ABG"),
          warning("inv_mtpite3", "I29", "Traveller Temporary Resident Permit has expired",
          notEmpty.and(isDaysInPast(0))).onSection("ABG"),
          failure("inv_mtpite4", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onOutflow().notOnCategory(["255", "256", "512", "513"]),
          failure("inv_mtpite5", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onInflow().notOnCategory(["255", "256", "303", "304", "305", "306", "416", "417"])
          ]
        },
        {
          field: "ThirdParty.Individual.PassportNumber",
          rules: [
          failure("mtpipn1", 431, "Passport number is required",
          isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", [
          'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
          ]))).onSection("AB").onCategory(["255", "256"]),
          ignore("mtpipn2"),
          failure("mtpipn4", 555, "Cannot be the same as Applicant Passport Number",
          notEmpty.and(matchesTransactionField("Resident.Individual.PassportNumber"))).onSection("ABEG"),
          failure("inv_mtpipn1", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onOutflow().notOnCategory(["255", "256", "512", "513"]),
          failure("inv_mtpipn2", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onInflow().notOnCategory(["255", "256", "303", "304", "305", "306", "416", "417"]),
          failure("inv_mtpipn_l1", "I51", "This field is too long and must be shortened to 15 characters", notEmpty.and(isTooLong(15))).onSection("A")
          ]
        },
        {
          field: "ThirdParty.Individual.PassportCountry",
          rules: [
          failure("mtpipc1", 433, "If category 255 or 256 is used this must be completed",
          isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", [
          'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
          ]))).onSection("AB").onCategory(["255", "256"]),
          failure("inv_mtpipc1", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onOutflow().notOnCategory(["255", "256", "512", "513"]),
          failure("inv_mtpipc2", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onInflow().notOnCategory(["255", "256", "303", "304", "305", "306", "416", "417"])
          ]
        },
        {
          field: "ThirdParty.Individual.PassportExpiryDate",
          rules: [
          failure("inv_mtpipe1", 433, "This must be completed",
          isEmpty.and(hasMoneyFieldValue("TravelMode.TravellerStatus", [
          'South African Resident', 'Non Resident', 'Foreign Temporary Resident'
          ]))).onSection("AB").onCategory(["255", "256"]),
          failure("inv_mtpipe2", "I30", "Must be in a valid date format: YYYY-MM-DD",
          notEmpty.and(notPattern(/^(19|20)\d{2}-(0\d|10|11|12)-(0[1-9]|1\d|2\d|3[01])$/))).onSection("ABG"),
          failure("inv_mtpipe3", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onOutflow().notOnCategory(["255", "256", "512", "513"]),
          failure("inv_mtpipe4", "I??", "Not required for category chosen",
          notEmpty).onSection("AB").onInflow().notOnCategory(["255", "256", "303", "304", "305", "306", "416", "417"]),
          warning("inv_mtpipe5", "I31", "Passport has expired",
          notEmpty.and(isDaysInPast(0))).onSection("ABG")
          ]
        },
        {
          field: "ThirdParty.ContactDetails.ContactSurname",
          rules: [
          ignore("mtpcds1"),
          failure("inv_mtpcd_l1", "I81", "This field is too long and must be shortened to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
          ]
        },
        {
          field: "ThirdParty.ContactDetails.ContactName",
          rules: [
          ignore("mtpcdn1"),
          failure("inv_mtpcdn_l1", "I80", "This field is too long and must be shortened to 50 characters", notEmpty.and(isTooLong(50))).onSection("A")
          ]
        },
        {
          field: "ThirdParty.ContactDetails.Email",
          rules: [
          ignore("mtpcde1")
          ]
        },
        {
          field: "ThirdParty.ContactDetails.Fax",
          rules: [
          ignore("mtpcdf1")
          ]
        },
        {
          field: "ThirdParty.ContactDetails.Telephone",
          rules: [
          ignore("mtpcdt1")
          ]
        },
        {
          field: [
          "ThirdParty.StreetAddress.AddressLine1", "ThirdParty.StreetAddress.AddressLine2", "ThirdParty.StreetAddress.Suburb",
          "ThirdParty.StreetAddress.City", "ThirdParty.StreetAddress.Province", "ThirdParty.PostalAddress.AddressLine1",
          "ThirdParty.PostalAddress.AddressLine2", "ThirdParty.PostalAddress.Suburb", "ThirdParty.PostalAddress.City",
          "ThirdParty.PostalAddress.Province"
          ],
          rules: [
          failure("inv_addr_l1", "I99", "This field is too long and must be shortened to 35 characters", notEmpty.and(isTooLong(35))).onSection("A")
          ]
        },
        {
          field: "ThirdParty.StreetAddress.AddressLine1",
          rules: [
          ignore("mtpsal11")
          ]
        },
        {
          field: "ThirdParty.PostalAddress.AddressLine1",
          rules: [
          ignore("mtppal11")
          ]
        },
        {
          field: "TravelMode.Mode",
          rules: [
          failure("inv_tvlm1", "I??", "If category 255 or 256 is used this must be completed",
          isEmpty).onOutflow().onSection("A").onCategory(["255", "256"]),
          failure("inv_tvlm2", "I??", "Unless category 255 or 256 is used this cannot be specified",
          notEmpty).onOutflow().onSection("A").notOnCategory(["255", "256"])
          ]
        },
        {
          field: "TravelMode.BorderPost",
          rules: [
          failure("inv_tvlbp1", "I??", "Required field for this category when traveling by ROAD",
          isEmpty.and(hasMoneyFieldValue("TravelMode.Mode", "ROAD"))).onOutflow().onSection("A").onCategory(["255", "256"]),
          failure("inv_tvlbp2", "I??", "Not required for category chosen",
          notEmpty).onOutflow().onSection("A").notOnCategory(["255", "256"]),
          failure("inv_tvlbp3", "I??", "Not required when traveling by AIR, SEA or RAIL",
          notEmpty.and(hasMoneyFieldValue("TravelMode.Mode", ["AIR", "SEA", "RAIL"]))).onOutflow().onSection("A").onCategory(["255", "256"])
          ]
        },
        {
          field: "TravelMode.TicketNumber",
          rules: [
          failure("inv_tvltn1", "I??", "Required field for this category when traveling by AIR, SEA or RAIL",
          isEmpty.and(hasMoneyFieldValue("TravelMode.Mode", ["AIR", "SEA", "RAIL"]))).onOutflow().onSection("A").onCategory(["255", "256"]),
          failure("inv_tvltn2", "I??", "Unless category 255 or 256 is used this cannot be specified",
          notEmpty).onOutflow().onSection("A").notOnCategory(["255", "256"])
          ]
        },
        {
          field: "TravelMode.DepartureDate",
          rules: [
          failure("inv_tvldd1", "I??", "If category 255 or 256 is used this must be completed", isEmpty).onOutflow().onSection("A").onCategory(["255", "256"]),
          failure("inv_tvldd2", "I??", "Must be a valid date in the future in the format: YYYY-MM-DD",
          notEmpty.and(notDatePattern.or(isDaysInPast(1)))).onSection("ABG"),
          failure("inv_tvldd3", "I??", "Unless category 255 or 256 is used this cannot be specified",
          notEmpty).onOutflow().onSection("A").notOnCategory(["255", "256"])
          ]
        },
        {
          field: "TravelMode.DestinationCountry",
          rules: [
          failure("inv_tvdc1", "I??", "If category 255 or 256 is used this must be completed", isEmpty).onOutflow().onSection("A").onCategory(["255", "256"]),
          failure("inv_tvdc2", "I??", "Unless category 255 or 256 is used this cannot be specified",
          notEmpty).onOutflow().onSection("A").notOnCategory(["255", "256"])
          ]
        }
        ]
      }
    };

    return money;
  }
});
