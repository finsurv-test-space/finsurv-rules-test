define(function (require) {
    return function (app, config) {

        var superInit = function () { };

        if (config.initializationFn) superInit = config.initializationFn;

        var _config = {
            initializationFn: function (model, scope) {
                superInit(model, scope);
                model.MonetaryAmount[0].isAccGrpOpen = true;
            },

            useTabs: true,
            //noCss: true,
            clearHidden: true,
            clearHiddenOnErrorOnly: true,
            snapShotValidate: true
        }

        Object.assign(config, _config);






        return config;
    }
})