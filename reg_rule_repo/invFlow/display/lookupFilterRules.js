define(function () {
  return function (predef) {
    var filterLookupRules;
    with (predef) {
      filterLookupRules = {
        filterLookupTrans: {
          ruleset: "Reporting Transaction Lookup Filter Rules",
          scope: "transaction",
          fields: [
            {
              field: "OriginatingCountry",
              display: [
                limitValue(["ZA"]).onOutflow().onSection("ABG"),
                excludeValue(["ZA"]).onInflow().onSection("ABG")
              ]
            },
            {
              field: "ReceivingCountry",
              display: [
                limitValue(["ZA"]).onInflow().onSection("ABG"),
                excludeValue(["ZA"]).onOutflow().onSection("ABG")
              ]
            },
            {
              field: "LocationCountry",
              display: [
                excludeValue(['ZA']).onSection("ABG"),
                excludeValue(['EU']).onSection("ABG").notOnCategory("513")
              ]
            },
            {
              field: "NonResident.Exception.ExceptionName",
              display: [
                limitValue(["MUTUAL PARTY"]).onInflow().onSection("A").onCategory("252"),
                excludeValue(["MUTUAL PARTY"]).onSection("A").notOnCategory(["200", "252", "255", "256", "530/05"]),
                excludeValue(["BULK INTEREST"]).onSection("A").notOnCategory(["300", "309/08"]),
                excludeValue(["BULK VAT REFUNDS"]).onSection("A").notOnCategory(["400", "411/02"]),
                excludeValue(["BULK BANK CHARGES"]).onSection("A").notOnCategory(["200", "275"]),
                excludeValue(["BULK PENSIONS"]).onSection("A").notOnCategory(["400", "407"]),
                excludeValue(["STRATE"]).onSection("A").notOnCategory(["601/01", "603/01"]),
                excludeValue(["MUTUAL PARTY", "BULK INTEREST", "BULK VAT REFUNDS", "BULK BANK CHARGES", "BULK PENSIONS", "STRATE"]).onSection("BCDEFG"),
                excludeValue(["FCA RESIDENT NON REPORTABLE", "CFC RESIDENT NON REPORTABLE", "VOSTRO NON REPORTABLE", "NOSTRO NON REPORTABLE", "RTGS NON REPORTABLE"]).onSection("ABDEFG"),
                excludeValue(["VOSTRO INTERBANK", "NOSTRO INTERBANK"]).onSection("ABEFG")
              ]
            },
            {
              field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
              display: [
                excludeValue(["CARD DIRECT"]).onOutflow().onSection("ACDG"),
                limitValue(["NON RESIDENT RAND"]).onSection("B"),
                limitValue(["CARD DIRECT"]).onSection("E")
              ]
            },
            {
              field: ["NonResident.Individual.Address.Country", "NonResident.Entity.Address.Country"],
              display: [
                excludeValue(["ZA"]).onInflow().onSection("ABCDG"),
                excludeValue(["ZA"], notTransactionFieldValue("Resident.Individual.ForeignIDCountry", ["NA", "LS", "SZ"])).onSection("E"),
                excludeValue(["EU"]).onOutflow().onSection("A").notOnCategory("513"),
                excludeValue(["EU"]).onInflow().onSection("A").notOnCategory("517")
              ]
            },
            {
              field: "Resident.Exception.ExceptionName",
              display: [
                limitValue(["MUTUAL PARTY"]).onSection("A").onCategory(["250", "251"]),
                excludeValue(["MUTUAL PARTY"]).onSection("A").notOnCategory(["250", "251"]),
                excludeValue(["MUTUAL PARTY", "NON RESIDENT RAND", "RAND CHEQUE", "BULK PENSIONS", "UNCLAIMED DRAFTS", "BULK INTEREST", "BULK DIVIDENDS", "BULK BANK CHARGES", "STRATE"]).onSection("BCDEFG"),
                excludeValue(["BULK PENSIONS"]).onSection("A").notOnCategory(["400", "407"]),
                excludeValue(["UNCLAIMED DRAFTS"]).onSection("A").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
                excludeValue(["FCA NON RESIDENT NON REPORTABLE", "VOSTRO NON REPORTABLE", "VOSTRO INTERBANK", "NOSTRO INTERBANK", "NOSTRO NON REPORTABLE", "RTGS NON REPORTABLE"]).onSection("ABEFG"),
                excludeValue(["BULK INTEREST"]).onSection("A").notOnCategory(["309/08", "300"]),
                excludeValue(["BULK DIVIDENDS"]).onSection("A").notOnCategory(["301", "300"]),
                excludeValue(["BULK BANK CHARGES"]).onSection("A").notOnCategory(["275", "200"]),
                excludeValue(["STRATE"]).onSection("A").notOnCategory(["601/01", "603/01", "600"])
              ]
            },
            {
              field: "Resident.Exception.Country",
              display: [
                excludeValue(["ZA"], hasTransactionFieldValue("Resident.Exception.ExceptionName", ['VOSTRO NON REPORTABLE', 'VOSTRO INTERBANK'])).onSection("CD")
              ]
            },
            {
              field: ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
              display: [
                limitValue(["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH"]).onSection("ABG"),
                limitValue(["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "VOSTRO"]).onSection("CD"),
                limitValue(["DEBIT CARD", "CREDIT CARD"]).onSection("E")
              ]
            },
            {
              field: ["Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province", "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
              display: [
                limitValue(["GAUTENG", "LIMPOPO", "NORTH WEST", "WESTERN CAPE", "EASTERN CAPE", "NORTHERN CAPE", "FREE STATE", "MPUMALANGA", "KWAZULU NATAL"]).onSection("ABG"),
                limitValue(["GAUTENG", "LIMPOPO", "NORTH WEST", "WESTERN CAPE", "EASTERN CAPE", "NORTHERN CAPE", "FREE STATE", "MPUMALANGA", "KWAZULU NATAL"], notTransactionField("Resident.Individual")).onSection("E")
              ]
            },
            {
              field: ["AccountHolderStatus"],
              display : [
                excludeValue(['Foreign Temporary Resident'], hasTransactionField("Resident.Entity"))
              ]
            },
            {
              field: ["CounterpartyStatus"],
              display : [
                excludeValue(['Foreign Temporary Resident'], hasTransactionField("NonResident.Entity"))
              ]
            }
          ]
        },

        filterLookupMoney: {
          ruleset: "Reporting Monetary Lookup Filter Rules",
          scope: "money",
          fields: [
            {
              field: "MoneyTransferAgentIndicator",
              display: [
                limitValue(["CARD"]).onSection("EF"),
                limitValue(["BOPDIR"]).onSection("G"),
                limitValue(["ADLA"], dealerTypeADLA).onSection("AB"),
                excludeValue(["AD", "ADLA", "CARD", "BOPDIR"], dealerTypeAD).onSection("ABCD").onCategory("833"),
                excludeValue(["ADLA", "CARD", "BOPDIR"], dealerTypeAD).onSection("ABCD").notOnCategory("833")
              ]
            },
            {
              field: "CompoundCategoryCode",
              display: [
                //#dummyData 18
                excludeValue(['102,104'],
                  notImportUndertakingClient
                    .and(notCustomValue("ignoreLUClient", true)))
                  .onOutflow().onSection("ABG"),

                //#dummyData 19
                limitValue(['102,104'], importUndertakingClient).onOutflow().onSection("ABG"),

                //#dummyData 14
                limitValue(['101,102,103,104,105,106'], hasCustomValue("tradeOnly", true)).onOutflow().onSection("ABG"),
                limitValue(['ZZ1']).onSection("C"),
                excludeValue(['ZZ1', 'ZZ2']).onSection("ABDEF"),
                excludeValue(["511", "512", "513"],
                hasTransactionFieldValue("AccountHolderStatus", "Foreign Temporary Resident")).onSection("AB"),
                excludeValue(["401"],
                hasTransactionFieldValue("AccountHolderStatus", "Foreign Temporary Resident")).onOutflow().onSection("AB"),
                excludeValue(["256"],hasTransactionField("Resident.Entity")).onSection("AB"),
                excludeValue(["255"],hasTransactionField("Resident.Individual")).onSection("AB")
              ]
            }
          ]
        },

        filterLookupImportExport: {
          ruleset: "Reporting Import Export Lookup Filter Rules",
          scope: "importexport",
          fields: []
        }
      }
    }

    return filterLookupRules;
  }
});


