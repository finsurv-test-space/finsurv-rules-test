define(function () {
  return function (predef) {

    var filterLookupRules;
    with (predef) {
      filterLookupRules = {
        filterLookupTrans: {
          ruleset: "Reporting Transaction Lookup Filter Rules",
          scope: "transaction",
          fields: [
            {
              field: "OriginatingCountry",
              display: [
                limitValue(["ZA"]).onOutflow().onSection("ABG"),
                excludeValue(["ZA"]).onInflow().onSection("ABG")
              ]
            },
            {
              field: "ReceivingCountry",
              display: [
                limitValue(["ZA"]).onInflow().onSection("ABG"),
                excludeValue(["ZA"]).onOutflow().onSection("ABG")
              ]
            },
            {
              field: "LocationCountry",
              display: [
                excludeValue(['ZA'], not(hasTransactionFieldValue("NonResident.Exception.ExceptionName", "IHQ"))).onSection("ABG"),
                excludeValue(['EU']).onSection("ABG").notOnCategory("513")
              ]
            },
            {
              field: ["NonResident.Individual.AccountIdentifier", "NonResident.Entity.AccountIdentifier"],
              display: [
                // excludeValue(["CARD DIRECT"]).onOutflow().onSection("ACDG"),
                // limitValue(["NON RESIDENT RAND"]).onSection("B"),
                // limitValue(["CARD DIRECT"]).onSection("E")
              ]
            },
            {
              field: ["NonResident.Individual.Address.Country", "NonResident.Entity.Address.Country"],
              display: [
                excludeValue(["ZA"]).onInflow().onSection("ABCDG"),
                excludeValue(["ZA"], notTransactionFieldValue("Resident.Individual.ForeignIDCountry", ["NA", "LS", "SZ"])).onSection("E"),
                excludeValue(["EU"]).onOutflow().onSection("A").notOnCategory("513"),
                excludeValue(["EU"]).onInflow().onSection("A").notOnCategory("517")
              ]
            },
            {
              field: "Resident.Exception.ExceptionName",
              display: [
                limitValue(["MUTUAL PARTY"]).onSection("A").onCategory(["250", "251"]),
                excludeValue(["MUTUAL PARTY"]).onSection("A").notOnCategory(["250", "251"]),
                excludeValue(["MUTUAL PARTY", "NON RESIDENT RAND", "RAND CHEQUE", "BULK PENSIONS", "UNCLAIMED DRAFTS", "BULK INTEREST", "BULK DIVIDENDS", "BULK BANK CHARGES", "STRATE"]).onSection("BCDEFG"),
                excludeValue(["BULK PENSIONS"]).onSection("A").notOnCategory(["400", "407"]),
                excludeValue(["UNCLAIMED DRAFTS"]).onSection("A").notOnCategory(["100", "200", "300", "400", "500", "600", "700", "800"]),
                excludeValue(["FCA NON RESIDENT NON REPORTABLE", "VOSTRO NON REPORTABLE", "VOSTRO INTERBANK", "NOSTRO INTERBANK", "NOSTRO NON REPORTABLE", "RTGS NON REPORTABLE"]).onSection("ABEFG"),
                excludeValue(["BULK INTEREST"]).onSection("A").notOnCategory(["309/08", "300"]),
                excludeValue(["BULK DIVIDENDS"]).onSection("A").notOnCategory(["301", "300"]),
                excludeValue(["BULK BANK CHARGES"]).onSection("A").notOnCategory(["275", "200"]),
                excludeValue(["STRATE"]).onSection("A").notOnCategory(["601/01", "603/01", "600"])
              ]
            },
            {
              field: "Resident.Exception.Country",
              display: [
                excludeValue(["ZA"], hasTransactionFieldValue("Resident.Exception.ExceptionName", ['VOSTRO NON REPORTABLE', 'VOSTRO INTERBANK'])).onSection("CD")
              ]
            },
            {
              field: ["Resident.Individual.AccountIdentifier", "Resident.Entity.AccountIdentifier"],
              display: [
                // limitValue(["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "EFT", "CARD PAYMENT"]).onSection("ABG"),
                // limitValue(["RESIDENT OTHER", "CFC RESIDENT", "FCA RESIDENT", "CASH", "EFT", "CARD PAYMENT", "VOSTRO"]).onSection("CD"),
                // limitValue(["DEBIT CARD", "CREDIT CARD"]).onSection("E")
              ]
            },
            {
              field: ["Resident.Individual.StreetAddress.Province", "Resident.Entity.StreetAddress.Province", "Resident.Individual.PostalAddress.Province", "Resident.Entity.PostalAddress.Province"],
              display: [
                limitValue(["GAUTENG", "LIMPOPO", "NORTH WEST", "WESTERN CAPE", "EASTERN CAPE", "NORTHERN CAPE", "FREE STATE", "MPUMALANGA", "KWAZULU NATAL"]).onSection("ABG"),
                limitValue(["GAUTENG", "LIMPOPO", "NORTH WEST", "WESTERN CAPE", "EASTERN CAPE", "NORTHERN CAPE", "FREE STATE", "MPUMALANGA", "KWAZULU NATAL"], notTransactionField("Resident.Individual")).onSection("E")
              ]
            }
          ]
        },

        filterLookupMoney: {
          ruleset: "Reporting Monetary Lookup Filter Rules",
          scope: "money",
          fields: [
            {
              field: "MoneyTransferAgentIndicator",
              display: [
                limitValue(["CARD"]).onSection("EF"),
                limitValue(["BOPDIR"]).onSection("G"),
                excludeValue(["AD", "ADLA", "CARD", "BOPDIR"], dealerTypeAD).onSection("ABCD").onCategory("833"),
                excludeValue(["ADLA", "CARD", "BOPDIR"], dealerTypeAD).onSection("ABCD").notOnCategory("833")
              ]
            }
          ]
        },

        filterLookupImportExport: {
          ruleset: "Reporting Import Export Lookup Filter Rules",
          scope: "importexport",
          fields: []
        }
      }
    }

    return filterLookupRules;
  }
});


