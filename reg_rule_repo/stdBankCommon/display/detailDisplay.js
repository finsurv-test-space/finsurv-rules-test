define(function () {
  return function (predef) {

    var display;

    with (predef) {
      display = {
        detailTrans: {
          ruleset: "Std Bank Transaction Display Rules",
          scope: "transaction",
          fields: [
            {
              field: "ValueDate",
              display: [
                show(),
                disable(isTransactionFieldProvided("ValueDate"))
              ]
            },
            {
              field: "FlowCurrency",
              display: [
                disable(isTransactionFieldProvided("FlowCurrency"))
              ]
            },
            {
              field: "TotalForeignValue",
              display: [
                disable()
              ]
            }
          ]
        },
        detailMoney: {
          ruleset: "Std Bank Money Display Rules",
          scope: "money",
          fields: [

            {
              field: "SWIFTDetails",
              display: [
                hide()
              ]
            },
            {
              field: "advancedpayment",
              display: [
                setValue(true, null, isEmpty.or(hasValue(false))).onOutflow().onSection("ABG").onCategory("101"),
                setValue(false, null, isEmpty.or(hasValue(true))).onOutflow().onSection("ABG").notOnCategory("103/11").onCategory(["103", "105", "106"])
              ]
            }
          ]
        },
        detailImportExport: {
          ruleset: "Std Bank Import/Export Display Rules",
          scope: "importexport",
          fields: [

            {
              field: "TransportDocumentNumber",
              display: [
                hide(isEmpty),
                show().onOutflow().onSection("ABG").onCategory(["103", "105", "106"])
              ]
            },
            {
              field: "PaymentValue",
              display: [
                setLabel("Allocated Amount")
              ]
            },
            {
              field: "PaymentCurrencyCode",
              display: [
                setValue("%s", "transaction::FlowCurrency", isEmpty)
              ]
            }
          ]
        }

      }
    }
    return display;
  }
});