/**
 * Created by petruspretorius on 04/05/2017.
 */


define(function () {
  return function (predef) {
    var extTrans;
    with (predef) {

      extTrans = {
        ruleset: "Common Transaction Rules for Standard Bank",
        scope: "transaction",
        validations: [
          {
            field: "OriginatingCountry",
            rules: [
              warning("std_ocntry1", "SBO", "For category 830, Originating Countries other than LS, NA or SZ need to be authorised",
                notEmpty.and(notValueIn(['LS','NA','SZ']))).onInflow().onCategory(['830'])

            ]
          }
        ]
      };

    }
    return extTrans;
  }
});