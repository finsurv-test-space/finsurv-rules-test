define(function () {
  return function (predef) {
    var extTrans;
    with (predef) {

      extTrans = {
        ruleset: "Common Money Rules for Standard Bank",
        scope: "money",
        validations: [
          {
            field: "ReversalTrnRefNumber",
            rules: [
              //Warning W01, "Reversal currency is different from the original transaction. Ensure reversal amount is reasonable"
              //Error E01, "Reveral amount is greater than the amount on the original transaction sequence"
              validate("sb_mrtrn1", "SBValidate_ReversalTrnRef",
                notEmpty.and(hasMoneyField('ReversalTrnSeqNumber'))).onSection("ABG")
            ]
          },
          {
            field: "ThirdParty.CustomsClientNumber",
            rules: [
              //ignore("mtpccn2"),
              failure("mtpccn6", 396, "ThirdPartyCustomsClientNumber must be completed for registered agents",
                isEmpty.and(hasMoneyField("ThirdParty.Individual").or(hasMoneyField("ThirdParty.Entity")))).onOutflow().onSection("AB").onCategory(["101", "103", "105", "106"]).notOnCategory(["101/11", "103/11"]),
            ]
          },
          {
            field: "CategoryCode",
            len: 3,
            rules: [
              failure("mcc7", 416, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN or OUT in cases where the Resident Entity element is completed, the third party individual and address details must be completed",
              hasTransactionField("Resident.Entity").and(
                notMoneyField("ThirdParty.Individual.Surname").or(notMoneyField("ThirdParty.StreetAddress.AddressLine1")).or(notMoneyField("ThirdParty.PostalAddress.AddressLine1"))
              )).onSection("A").onCategory(["511", "516"]),
            ]
          },
          {
            field: "ThirdParty.Individual.Surname",
            minLen: 1,
            maxLen: 35,
            rules: [
              failure("mtpisn9", 416, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN or OUT in cases where the Resident Entity element is completed, this must be completed",
              isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("A").onCategory(["511", "516"]),
              warning("mtpisn12", "S416", "This must be completed",
                isEmpty.and(hasMoneyField("ThirdParty.Individual"))).onSection("AB"),
            ]
          },
          {
            field: "ThirdParty.TaxNumber",
            minLen: 2,
            maxLen: 15,
            rules: [
              failure("mtptx4", 439, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed",
                  isEmpty.and(hasTransactionField("Resident.Entity"))).onInflow().onSection("AB").onCategory(["511", "516"]),
              failure("mtptx6", 546, "ThirdPartyTaxNumber must be completed for registered agents",
                isEmpty.and(hasMoneyField("ThirdParty.Individual").or(hasMoneyField("ThirdParty.Entity")))).onOutflow().onSection("AB").onCategory(["101", "103", "105", "106"]).notOnCategory(["101/11", "103/11"]),
              failure('mtptx7', 325, "Invalid tax number",
                notEmpty.and(not(isValidZATaxNumber))).onCategory("512").onSection("AB"),
            ]
          },
          {
            field: "ThirdParty.StreetAddress.AddressLine1",
            minLen: 2,
            maxLen: 70,
            rules: [
              failure("mtpsal13", 456, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN or OUT in cases where the Resident Entity element is completed, this must be completed",
              isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("A").onCategory(["511", "516"])
            ]
          },
          {
            field: "ThirdParty.PostalAddress.AddressLine1",
            minLen: 2,
            maxLen: 70,
            rules: [
              failure("mtppal13", 512, "If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN or OUT in cases where the Resident Entity element is completed, this must be completed",
              isEmpty.and(hasTransactionField("Resident.Entity"))).onSection("A").onCategory(["511", "516"])
            ]
          },
          {
            field: "ThirdParty.Entity.Name",
            minLen: 1,
            maxLen: 50,
            rules: [
              failure("mtpenm4", "S416", "This must be completed",
              isEmpty.and(hasMoneyField("ThirdParty.Entity"))).onSection("AB"),
            ]
          }
        ]
      };

    }
    return extTrans;
  }
});
