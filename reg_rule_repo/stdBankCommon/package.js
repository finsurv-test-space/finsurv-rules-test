define({
  engine: {major: "1", minor: "0"},
  dependsOn: "coreSARBExternal@201511",
  features: ["featureBranchHub", "featureHOLDCO", "featureMTAAccounts", "featureSARBManB4", "featureEntity511", "featureSchema", "featureLimit"],
  mappings: {
    _minLenErrorType: "ERROR", // SUCCESS, ERROR, WARNING
    _maxLenErrorType: "ERROR",
    _lenErrorType: "ERROR"
  }
})