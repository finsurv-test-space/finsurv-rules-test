define(function() {
  return function(testBase) {
      with (testBase) {

      var test_cases = [
              // Money: ThirdParty.TaxNumber
              //If category 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed
              assertSuccess("mtptx1", {
                  ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                  MonetaryAmount: [{CategoryCode: '512', CategorySubCode: '04', ThirdParty: {TaxNumber: '123'}}]
              }),
              assertSuccess("mtptx1", {
                  ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {}},
                  MonetaryAmount: [{CategoryCode: '512', CategorySubCode: '04', ThirdParty: {TaxNumber: ''}}]
              }),
              assertFailure("mtptx1", {
                  ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
                  MonetaryAmount: [{CategoryCode: '512', CategorySubCode: '04', ThirdParty: {TaxNumber: ''}}]
              }),

              //If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed
              assertSuccess("mtptx4", {
                  ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {}},
                  MonetaryAmount: [{CategoryCode: '511', CategorySubCode: '04', ThirdParty: {TaxNumber: '123'}}]
              }),
              assertSuccess("mtptx4", {
                  ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {}},
                  MonetaryAmount: [{CategoryCode: '511', CategorySubCode: '04', ThirdParty: {TaxNumber: ''}}]
              }),
              assertFailure("mtptx4", {
                  ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {}},
                  MonetaryAmount: [{CategoryCode: '511', CategorySubCode: '04', ThirdParty: {TaxNumber: ''}}]
              }),

              //If CategoryCode 511/01 to 511/07 or 516 is used and Flow is IN in cases where the Resident Entity element is completed, this must be completed
              assertSuccess("mtptx4", {
                  ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {}},
                  MonetaryAmount: [{CategoryCode: '516', ThirdParty: {TaxNumber: '123'}}]
              }),
              assertSuccess("mtptx4", {
                  ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Individual: {}},
                  MonetaryAmount: [{CategoryCode: '516', ThirdParty: {TaxNumber: ''}}]
              }),
              assertFailure("mtptx4", {
                  ReportingQualifier: 'BOPCUS', Flow: 'IN', Resident: {Entity: {}},
                  MonetaryAmount: [{CategoryCode: '516', ThirdParty: {TaxNumber: ''}}]
              }),

              assertSuccess("mtptx7", {
                ReportingQualifier: 'BOPCUS',
                MonetaryAmount: [{
                  CategoryCode : "512",
                  ThirdParty: { TaxNumber: '1125519171' }
                }]
              }),
              assertFailure("mtptx7", {
                ReportingQualifier: 'BOPCUS',
                MonetaryAmount: [{
                  CategoryCode : "512",
                  ThirdParty: { TaxNumber: '123' }
                }]
              }),

              // ThirdParty.Individual.Surname: This must be completed
              assertNoRule("mtpisn12", {
                ReportingQualifier: 'BOPCUS', Flow: 'OUT', 
                MonetaryAmount: [{ThirdParty: {}}]
              }),
                assertSuccess("mtpisn12", {
                    ReportingQualifier: 'BOPCUS', Flow: 'OUT',
                    MonetaryAmount: [{ThirdParty: {Individual:{Surname:'SomeName'}}}]
              }),
                assertWarning("mtpisn12", {
                    ReportingQualifier: 'BOPCUS', Flow: 'OUT',
                    MonetaryAmount: [{ThirdParty: {Individual:{}}}]
              }),
          ]

      }
    return testBase;
  }
})
