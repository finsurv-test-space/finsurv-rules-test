


define(function () {
  //THIS IS the Transaciotn rules
    return function (predef) {
        var transaction;
  
        with (predef) {
            transaction = {
                ruleset: "Transaction Rules for Flow form",
                scope: "transaction",
                validations: [
                  {
                    field: "PaymentDetail.BeneficiaryBank.BankName",
                    minLen: 2,
                    maxLen: 50,
                    rules: [
                        failure("inv_bbnm1", "I61", "Either Beneficiary bank name or SWIFTBIC must be provided",
                            isEmpty.and(notTransactionField("PaymentDetail.BeneficiaryBank.SWIFTBIC"))).onOutflow().onSection("A")
                    ]
                  },
                  {
                    field: "PaymentDetail.BeneficiaryBank.BranchCode",
                    minLen: 2,
                    maxLen: 30,
                    rules: [
                      failure("inv_bbbc1", "I61", "If Beneficiary Bank Name is provided, Beneficiary Bank Branch Code must be provided",
                        isEmpty.and(hasTransactionField("PaymentDetail.BeneficiaryBank.BankName"))).onOutflow().onSection("A")
                    ]
                  },
                  {
                    field: "PaymentDetail.BeneficiaryBank.Address",
                    minLen: 2,
                    maxLen: 200,
                    rules: [
                      failure("inv_bba1", "I61", "If Beneficiary Bank Name is provided, Beneficiary Bank Address must be provided",
                        isEmpty.and(hasTransactionField("PaymentDetail.BeneficiaryBank.BankName"))).onOutflow().onSection("A")
                    ]
                  },
                  {
                    field: "PaymentDetail.BeneficiaryBank.City",
                    minLen: 2,
                    maxLen: 100,
                    rules: [
                      failure("inv_bbc1", "I61", "If Beneficiary Bank Name is provided, Beneficiary Bank City must be provided",
                        isEmpty.and(hasTransactionField("PaymentDetail.BeneficiaryBank.BankName"))).onOutflow().onSection("A")
                    ]
                  },
                  {
                    field: "ReceivingCountry",
                    rules: [
                        failure("inv_bbc", "I62", "The country must be provided for the beneficiary bank",
                            isEmpty.and(hasTransactionField("PaymentDetail.BeneficiaryBank.SWIFTBIC").
                                or(hasTransactionField("PaymentDetail.BeneficiaryBank.BankName")).
                                or(hasTransactionField("PaymentDetail.BeneficiaryBank.BranchCode")).
                                or(hasTransactionField("PaymentDetail.BeneficiaryBank.Address")).
                                or(hasTransactionField("PaymentDetail.BeneficiaryBank.City")))).onOutflow().onSection("A")
                    ]
                  },
                  {
                    field: "CorrespondentCountry",
                    rules: [
                        failure("inv_cbc1", "I65", "The country must be provided for the beneficiary bank",
                            isEmpty.and(hasTransactionField("PaymentDetail.CorrespondentBank.SWIFTBIC").
                                or(hasTransactionField("PaymentDetail.CorrespondentBank.BankName")).
                                or(hasTransactionField("PaymentDetail.CorrespondentBank.BranchCode")).
                                or(hasTransactionField("PaymentDetail.CorrespondentBank.Address")).
                                or(hasTransactionField("PaymentDetail.CorrespondentBank.City")))).onOutflow().onSection("A")
                    ]
                  }
                ]
            }
        };
  
        return transaction;
    }
  
  });
  
  