define({
  engine: {major: "1", minor: "0"},
  dependsOn: "coreSARB",
  features: ["featureNoEditResident", "featureSchema"],
  mappings: {
    LocalCurrencySymbol: "R",
    LocalCurrencyName: "Rand",
    LocalCurrency: "ZAR",
    LocalValue: "RandValue",
    Regulator: "SARB",
    DealerPrefix: "AD",
    RegulatorPrefix: "SARB"
  }
})