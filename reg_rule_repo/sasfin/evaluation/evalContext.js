define(function () {
  return function (evaluationEx) {
    return {
      localCountryRegex: "[Zz][Aa]",
      localCurrencyRegex: "[Zz][Aa][Rr]",
      whoAmIRegex: "SASFZA.*",
      onshoreRegex: "....ZA.*",
      onshoreADRegex: [
        "ABSAZAJJ.*", // ABSA BANK
        "ALBRZAJJ.*", // ALBARAKA BANK
        "BKCHZAJJ.*", // BANK OF CHINA
        "BIDBZAJJ.*", // BIDVEST
        "BNPAZAJJ.*", // BNP PARIBAS JHB
        "CABLZAJJ.*", // CAPITEC
        "CITIZAJX.*", // CITIBANK JHB
        "FIRNZAJJ.*", // FIRSTRAND BANK
        "HOBLZAJJ.*", // HABIB OVERSEAS BANK JHB
        "HBZHZAJJ.*", // HBZ BANK
        "IVESZAJJ.*", // INVESTEC
        "MGTCZAJJ.*", // JP MORGAN JHB
        "LISAZAJJ.*", // MERCANTILE BANK
        "NEDSZAJJ.*", // NEDBANK
        "SASFZAJJ.*", // SASFIN
        "SOGEZAJJ.*", // SOCIETY GENERAL JHB
        "SCBLZAJJ.*", // STANCHART BANK JHB
        "SBINZAJJ.*", // STATE BANK OF INDIA
        "BATHZAJJ.*", // BANK OF ATHENS
        "SBSAZAJJ.*"] // STANDARD BANK
    };
  }
})
