define(function () {
  return function (predef) {
    var stdTrans;
    with (predef) {

      stdTrans = {
        ruleset: "Standard Transaction Rules",
        scope: "transaction",
        validations: [
        
          {
            field: "ValueDate",
            rules: [
              warning("vd2", 216, "Future dated transaction exceeds today's date plus 10 days", notEmpty.and(isDaysInFuture(10))),
            ]
          }
        ]
      };


    }
    return stdTrans;
  }
});
