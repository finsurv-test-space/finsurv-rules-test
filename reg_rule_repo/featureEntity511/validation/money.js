define(function () {
  return function (predef) {
    var extMoney;
    with (predef) {

      extMoney = {
        ruleset: "Money Rules for 511 Entities",
        scope: "money",
        validations: [
          {
            field: "CategoryCode",
            rules: [
              failure("mcc6", 416, "If CategoryCode 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, the third party individual and address details must be completed",
                hasTransactionField("Resident.Entity").and(
                  notMoneyField("ThirdParty.Individual.Surname").or(notMoneyField("ThirdParty.StreetAddress.AddressLine1")).or(notMoneyField("ThirdParty.PostalAddress.AddressLine1"))
                ).and(hasMoneyFieldValue("AdHocRequirement.Subject","SDA"))).onOutflow().onSection("A").onCategory(["511", "512", "513"])
            ]
          },
          {
            field: "AdHocRequirement.Subject",
            rules: [
              failure("madhs20", 292, "If Flow is OUT and category is 511/01 to 511/07 and Resident Entity is used and Third Party Individual details are provided then Subject must be SDA",
                notValue("SDA").and(hasTransactionField("Resident.Entity")).and(hasMoneyField("ThirdParty.Individual"))).onOutflow().onCategory("511").onSection("A")
            ]
          },
          {
            field: "ThirdParty.Individual.Surname",
            rules: [
              failure("mtpisn1", 416, "If CategoryCode 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, it must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject","SDA"))).onOutflow().onSection("AB").onCategory(["511", "512", "513"])
            ]
          },
          {
            field: "ThirdParty.Individual.IDNumber",
            rules: [
              failure("mtpiid2", 425, "If category 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject","SDA"))).onOutflow().onSection("AB").onCategory(["511", "512", "513"])
            ]
          },
//          {
//            field: "ThirdParty.TaxNumber",
//            rules: [
//              failure("mtptx1", 439, "If category 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed",
//                isEmpty.and(hasTransactionField("Resident.Entity"))).onOutflow().onSection("AB").onCategory(["511", "512", "513"])
//            ]
//          },
          {
            field: "ThirdParty.StreetAddress.AddressLine1",
            rules: [
              failure("fe_mtpsal11", 456, "If category 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject","SDA"))).onOutflow().onSection("A").onCategory(["511", "512", "513"])
            ]
          },
          {
            field: "ThirdParty.PostalAddress.AddressLine1",
            rules: [
              failure("fe_mtppal11", 512, "If category 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed",
                isEmpty.and(hasTransactionField("Resident.Entity")).and(hasMoneyFieldValue("AdHocRequirement.Subject","SDA"))).onOutflow().onSection("A").onCategory(["511", "512", "513"])
            ]
          }
        ]
      };

    }
    return extMoney;
  }
});
