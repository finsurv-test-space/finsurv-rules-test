define(function() {
  return function(testBase) {
      with (testBase) {
        var mappings = {
          LocalCurrencySymbol: "R",
          LocalCurrencyName: "Rand",
          LocalCurrency: "ZAR",
          LocalValue: "RandValue",
          Regulator: "SARB",
          DealerPrefix: "AD",
          RegulatorPrefix: "SARB"
        };

        setMappings(mappings, true);

        var test_cases = [
          //Ignored rule! If Flow is OUT and category is 511/01 to 511/07 the Resident Entity may not be used
          assertNoRule("rg7", {
            ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: {Individual: {Name: "John"}},
            MonetaryAmount: [{CategoryCode: "511", CategorySubCode: "04"}]
          }),
          assertNoRule("rg7", {
            ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: {Exception: {ExceptionName: "x1"}},
            MonetaryAmount: [{CategoryCode: "511", CategorySubCode: "04"}]
          }),
          assertNoRule("rg7", {
            ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: {Entity: {EntityName: "bla"}},
            MonetaryAmount: [{CategoryCode: "511", CategorySubCode: "04"}]
          }),


          // If Flow is OUT and category is 511/01 to 511/07 and Resident Entity is used and Third Party Individual details are provided then Subject must be SDA
          assertSuccess("madhs20", {
            ReportingQualifier: 'BOPCUS', Flow: "OUT",
            Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '511', CategorySubCode: '04',
              ThirdParty: {Individual: {Surname: ''}},
              AdHocRequirement: {Subject: 'SDA'}
            }]
          }),
          assertSuccess("madhs20", {
            ReportingQualifier: 'BOPCUS', Flow: "OUT",
            Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '511', CategorySubCode: '04',
              AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}
            }]
          }),
          assertSuccess("madhs20", {
            ReportingQualifier: 'BOPCUS', Flow: "OUT",
            Resident: {Individual: {}},
            MonetaryAmount: [{
              CategoryCode: '511', CategorySubCode: '04',
              ThirdParty: {Individual: {Surname: ''}},
              AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}
            }]
          }),
          assertFailure("madhs20", {
            ReportingQualifier: 'BOPCUS', Flow: "OUT",
            Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '511', CategorySubCode: '04',
              ThirdParty: {Individual: {Surname: ''}},
              AdHocRequirement: {Subject: 'REMITTANCE DISPENSATION'}
            }]
          }),

          //If CategoryCode 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, the third party individual details and subject must be completed
          assertSuccess("mcc6", {
            ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: {Entity: {EntityName: "bla"}},
            MonetaryAmount: [{
              CategoryCode: "513",
              AdHocRequirement: {Subject: 'SDA'},
              ThirdParty: {
                Individual: {Surname: 'XYZ'},
                StreetAddress: {AddressLine1: 'Street Home'},
                PostalAddress: {AddressLine1: 'Postal Home'}
              }
            }]
          }),
          assertFailure("mcc6", {
            ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: {Entity: {EntityName: "bla"}},
            MonetaryAmount: [{
              CategoryCode: "513",
              AdHocRequirement: {Subject: 'SDA'},
              ThirdParty: {
                StreetAddress: {AddressLine1: 'Street Home'},
                PostalAddress: {AddressLine1: 'Postal Home'}
              }
            }]
          }),
          assertFailure("mcc6", {
            ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: {Entity: {EntityName: "bla"}},
            MonetaryAmount: [{
              CategoryCode: "513",
              AdHocRequirement: {Subject: 'SDA'},
              ThirdParty: {Individual: {Surname: 'XYZ'}, PostalAddress: {AddressLine1: 'Postal Home'}}
            }]
          }),
          assertFailure("mcc6", {
            ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: {Entity: {EntityName: "bla"}},
            MonetaryAmount: [{
              CategoryCode: "513",
              AdHocRequirement: {Subject: 'SDA'},
              ThirdParty: {Individual: {Surname: 'XYZ'}, StreetAddress: {AddressLine1: 'Street Home'}}
            }]
          }),

          assertSuccess("mcc6", {
            ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: {Entity: {EntityName: "bla"}},
            MonetaryAmount: [{
              CategoryCode: "511",
              CategorySubCode: "01",
              AdHocRequirement: {Subject: 'SDA'},
              ThirdParty: {
                Individual: {Surname: 'XYZ'},
                StreetAddress: {AddressLine1: 'Street Home'},
                PostalAddress: {AddressLine1: 'Postal Home'}
              }
            }]
          }),
          //#6 If CategoryCode 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, the third party individual and address details must be completed
          assertFailure("mcc6", {
            ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: {Entity: {EntityName: "bla"}},
            MonetaryAmount: [{
              CategoryCode: "511",
              CategorySubCode: "01",
              AdHocRequirement: {Subject: 'SDA'},
              ThirdParty: {
                StreetAddress: {AddressLine1: 'Street Home'},
                PostalAddress: {AddressLine1: 'Postal Home'}
              }
            }]
          }),
          assertFailure("mcc6", {
            ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: {Entity: {EntityName: "bla"}},
            MonetaryAmount: [{
              CategoryCode: "511",
              CategorySubCode: "01",
              AdHocRequirement: {Subject: 'SDA'},
              ThirdParty: {Individual: {Surname: 'XYZ'}, PostalAddress: {AddressLine1: 'Postal Home'}}
            }]
          }),
          assertFailure("mcc6", {
            ReportingQualifier: "BOPCUS", Flow: "OUT", Resident: {Entity: {EntityName: "bla"}},
            MonetaryAmount: [{
              CategoryCode: "511",
              CategorySubCode: "01",
              AdHocRequirement: {Subject: 'SDA'},
              ThirdParty: {Individual: {Surname: 'XYZ'}, StreetAddress: {AddressLine1: 'Street Home'}}
            }]
          }),

          //If the Flow is OUT and the CategoryCode is 511/01 to 511/07 or 512/01 to 512/07 or 513 and the Resident Entity element is used, the Subject must be YES or NO
/*          assertSuccess("madhs8", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{CategoryCode: '512', CategorySubCode: '03', AdHocRequirement: {Subject: 'NO'}}]
          }),
          assertSuccess("madhs8", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {}},
            MonetaryAmount: [{CategoryCode: '512', CategorySubCode: '03', AdHocRequirement: {Subject: 'SETOFF'}}]
          }),
          assertFailure("madhs8", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{CategoryCode: '512', CategorySubCode: '03', AdHocRequirement: {Subject: 'SETOFF'}}]
          }),

          assertSuccess("madhs8", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{CategoryCode: '511', CategorySubCode: '03', AdHocRequirement: {Subject: 'NO'}}]
          }),
          assertSuccess("madhs8", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {}},
            MonetaryAmount: [{CategoryCode: '511', CategorySubCode: '03', AdHocRequirement: {Subject: 'SETOFF'}}]
          }),
          assertFailure("madhs8", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{CategoryCode: '511', CategorySubCode: '03', AdHocRequirement: {Subject: 'SETOFF'}}]
          }),
*/
          // Money: ThirdParty.Individual.Surname
          //If CategoryCode 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and Flow is OUT in cases where the Resident Entity element is completed, it must be completed
          assertSuccess("mtpisn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {EntityName: 'ACME'}},
            MonetaryAmount: [{CategoryCode: '512', CategorySubCode: '04', AdHocRequirement: {Subject: 'SDA'}, ThirdParty: {Individual: {Surname: 'XYZ'}}}]
          }),
          assertFailure("mtpisn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {EntityName: 'ACME'}},
            MonetaryAmount: [{CategoryCode: '512', CategorySubCode: '04', AdHocRequirement: {Subject: 'SDA'}, ThirdParty: {Individual: {Surname: ''}}}]
          }),
          assertFailure("mtpisn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {EntityName: 'ACME'}},
            MonetaryAmount: [{CategoryCode: '512', CategorySubCode: '04', AdHocRequirement: {Subject: 'SDA'}, ThirdParty: {Entity: {}}}]
          }),
          assertFailure("mtpisn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {EntityName: 'ACME'}},
            MonetaryAmount: [{CategoryCode: '512', CategorySubCode: '04', AdHocRequirement: {Subject: 'SDA'}}]
          }),

          assertSuccess("mtpisn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {EntityName: 'ACME'}},
            MonetaryAmount: [{CategoryCode: '511', CategorySubCode: '04', AdHocRequirement: {Subject: 'SDA'}, ThirdParty: {Individual: {Surname: 'XYZ'}}}]
          }),
          assertFailure("mtpisn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {EntityName: 'ACME'}},
            MonetaryAmount: [{CategoryCode: '511', CategorySubCode: '04', AdHocRequirement: {Subject: 'SDA'}, ThirdParty: {Individual: {Surname: ''}}}]
          }),
          assertFailure("mtpisn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {EntityName: 'ACME'}},
            MonetaryAmount: [{CategoryCode: '511', CategorySubCode: '04', AdHocRequirement: {Subject: 'SDA'}, ThirdParty: {Entity: {}}}]
          }),
          assertFailure("mtpisn1", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {EntityName: 'ACME'}},
            MonetaryAmount: [{CategoryCode: '511', CategorySubCode: '04', AdHocRequirement: {Subject: 'SDA'}}]
          }),

          // Money: ThirdParty.Individual.IDNumber
          //If category 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed
          assertSuccess("mtpiid2", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '512',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {Individual: {IDNumber: '6811035039084'}}
            }]
          }),
          assertFailure("mtpiid2", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '512', CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {Individual: {IDNumber: ''}}}]
          }),

          assertSuccess("mtpiid2", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '511',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {Individual: {IDNumber: '6811035039084'}}
            }]
          }),
          assertFailure("mtpiid2", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '511',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {Individual: {IDNumber: ''}}}]
          }),
          assertSuccess("fe_mtpsal11", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '512',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {StreetAddress: {AddressLine1: 'ADDR'}}
            }]
          }),
          assertSuccess("fe_mtpsal11", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {}},
            MonetaryAmount: [{
              CategoryCode: '512',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {StreetAddress: {AddressLine1: ''}}
            }]
          }),
          assertFailure("fe_mtpsal11", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '512',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {StreetAddress: {AddressLine1: ''}}
            }]
          }),

          assertSuccess("fe_mtpsal11", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '511',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {StreetAddress: {AddressLine1: 'ADDR'}}
            }]
          }),
          assertSuccess("fe_mtpsal11", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {}},
            MonetaryAmount: [{
              CategoryCode: '511',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {StreetAddress: {AddressLine1: ''}}
            }]
          }),
          assertFailure("fe_mtpsal11", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '511',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {StreetAddress: {AddressLine1: ''}}
            }]
          }),

          // Money: ThirdParty.PostalAddress.AddressLine1
          //If category 511/01 to 511/07 or 512/01 to 512/07 or 513 is used and flow is OUT in cases where the Resident Entity element is completed, this must be completed
          assertSuccess("fe_mtppal11", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '512',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {PostalAddress: {AddressLine1: 'ADDR'}}
            }]
          }),
          assertSuccess("fe_mtppal11", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {}},
            MonetaryAmount: [{
              CategoryCode: '512',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {PostalAddress: {AddressLine1: ''}}
            }]
          }),
          assertFailure("fe_mtppal11", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '512',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {PostalAddress: {AddressLine1: ''}}
            }]
          }),

          assertSuccess("fe_mtppal11", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '511',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {PostalAddress: {AddressLine1: 'ADDR'}}
            }]
          }),
          assertSuccess("fe_mtppal11", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Individual: {}},
            MonetaryAmount: [{
              CategoryCode: '511',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {PostalAddress: {AddressLine1: ''}}
            }]
          }),
          assertFailure("fe_mtppal11", {
            ReportingQualifier: 'BOPCUS', Flow: 'OUT', Resident: {Entity: {}},
            MonetaryAmount: [{
              CategoryCode: '511',
              CategorySubCode: '04',
              AdHocRequirement: {Subject:"SDA"},
              ThirdParty: {PostalAddress: {AddressLine1: ''}}
            }]
          })
        ]

      }
    return testBase;
  }
})
