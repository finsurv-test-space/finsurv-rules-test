#!/bin/bash

#* To build with test: 
#*  - bash shell/build.sh yes
#* or 
#*  - npm run build yes
#*
#* To build without test: 
#*  - bash shell/build.sh
#* or 
#*  - npm run build

ROOT=$PWD

run_build(){
    bash shell/buildEvalRules.sh
    errExitFunc $?
    
    bash shell/buildRules.sh
    errExitFunc $?

    bash shell/buildChannels.sh
    errExitFunc $?
}


write_metadata(){
   echo "{
        	\"artefact\":
                []
        }" > ${ROOT}/build/metadata.json
}

# Useage: exitFunc $?
errExitFunc() {
    if [ $1 == 1 ]
    then
        echo -e ${RED}"ERROR: Build error"${NC}
        exit 1
    fi
}

#* Install npm#
npm install

#* Cleanup build folder#
if [ -d build ]; then
    rm -r build
fi 
mkdir build

#* build process#
SIGNAL="fail"
if [[ "$1" == "yes" ]]; then
    #* Run tests before build# 
    echo "Running tests for rules before build..."
    ERR=$(npm run test)
    FLAG="${ERR##*$'\n'}"

    #* Run all build scripts if no test returns no error#
    if [[ "$FLAG" == "0" ]]; then
        echo "No error during testing, start building rules..."
        run_build
        write_metadata
        SIGNAL="pass"
    else
        echo "$FLAG errors found during testing, abort build."
    fi
else
    #* Run build without running tests#
    echo "Running build without running tests..."
    run_build
    write_metadata
    SIGNAL="pass"
fi

if [ "${SIGNAL}" == "pass" ]; then
    exit 0
else
    exit 1
fi