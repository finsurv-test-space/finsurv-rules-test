#!/bin/bash

result=$(npm run test)
errors="${result##*$'\n'}"

echo "$result"

if [[ $errors -eq 0 ]]; then
  exit 0
else
  printf "\nFAILED TESTS..." && exit 1
fi