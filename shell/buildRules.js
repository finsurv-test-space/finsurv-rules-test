/**
 * Created by petruspretorius on 22/04/2015.
 */
({
  optimize: 'none',
  baseUrl : '../src/',
  name : "coreRules",
  out     : "../build/coreRules/coreRules.min.js",
  paths : {
    'evaluationEx' : "./evaluation/evaluationEx"
  }
})

