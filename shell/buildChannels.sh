#!/bin/bash

# This script should only be invoked from the root project via `npm run buildChannels` or `npm run build`

# --- build stuff
mkdir -p build
node tools/compileChannelPackages.js $1

if [ "$1" ]; then
    # compile one package
    node node_modules/requirejs/bin/r.js -o baseUrl=./build/crules/$1 name=pack out=./build/crules/$1/pack.min.js optimize=none paths.data/lookups=empty:
else 
    # compile all packages
    for d in ./build/crules/*/ ; do (
        echo 'building package:' $d
        node node_modules/requirejs/bin/r.js -o baseUrl=$d/ name=pack out=$d/pack.min.js optimize=none paths.data/lookups=empty:
    ); 
    done;
fi