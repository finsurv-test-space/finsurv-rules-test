
#!/bin/bash
# --- Publish the artefacts...
aws s3 cp evalExampleInteractive.html s3://txstream-test1/
aws s3 cp node_modules/requirejs/require.js s3://txstream-test1/node_modules/requirejs/
aws s3 cp node_modules/lodash/lodash.min.js s3://txstream-test1/node_modules/lodash/
aws s3 cp tools/bg.js s3://txstream-test1/tools/
aws s3 cp build/evaluationEx/evaluationEx.min.js s3://txstream-test1/src/evaluation/evaluationEx.js
# --- publish the built rules
aws s3 sync --delete build/crules s3://txstream-test1/build/crules
