# FinSurv Base rules #

This repo contains the base FinSurv validation/evaluation rules to be used in the Electronic BOP form and the Java validation engine.

## Repo Structure ##

* reg_rule_repo
    * This is the location of all the channel rules
* shell
    * This contains all the shell scripts for building and pushing artefacts to AWS
* src
    * This contain the source for the evaluation, validation and tests
* tools
    * Miscelaneous tools used for channel building, generating rule CSVs, etc.

## Usage ##

To Build:
```bash
npm run build
```
To Test:
```bash
npm run test
```

## Demos ##

You can open `evalExampleInteractive.html` to test the evaluation rules.
