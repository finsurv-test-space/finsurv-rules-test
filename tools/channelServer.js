'use strict';

var express = require('express');
var swaggerJSDoc = require('swagger-jsdoc');
var routes = require('./api/controllers/routes.js');
var bodyParser = require('body-parser');

//Creating server
var app = express();

app.use(bodyParser.json()); // To support JSON-encoded bodies
app.use(bodyParser.urlencoded({ // To support URL-encoded bodies
  extended: true,
}));


var swaggerDefinition = {
  swagger: "2.0",
  info: {
    title: "tXstream Report Data REST API",
    description: "The tXstream Report API provides access to all report data and related functionality",
    version: "3.0",
    contact: {
      name: "Synthesis",
      url: "http://www.synthesis.co.za",
      email: "admin@synthesis.co.za"
    },
    license: {
      name: "API License"
    }
  },
  host: "localhost:8000",
  basePath: "/",
  schemes: ["http", "https"],
  consumes: ["application/json"],
  produces: ["application/json","text/html","text/plain"]
};

var options = {
  swaggerDefinition: swaggerDefinition,
  apis: ["./api/controllers/routes.js"]  
};

var swaggerSpec = swaggerJSDoc(options);

app.get('/swagger',function(req, res){
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});




var port = process.env.PORT || 8000;

app.use('/swagger-ui',express.static(__dirname + '/swagger'))

routes.setup(app);

app.listen(port);

exports = module.exports = app;