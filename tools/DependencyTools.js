var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
define(["require", "exports"], function (require, exports) {
    "use strict";
    exports.__esModule = true;
    function getDependencies(packageId, repoDir) {
        return __awaiter(this, void 0, void 0, function () {
            var packageName, packages, info, parents, features, i, i, i;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        packageName = packageId.split("@")[0];
                        packages = [packageName];
                        return [4 /*yield*/, promiseLoadModule(repoDir + "/" + packageName + "/package.js")];
                    case 1:
                        info = _a.sent();
                        if (info.dependsOn) {
                            parents = getDependencies(info.dependsOn, repoDir);
                        }
                        if (info.features) {
                            features = [];
                            for (i in info.features) {
                                features = features.concat(getDependencies(info.features[i], repoDir));
                            }
                        }
                        for (i in parents) {
                            if (packages.indexOf(parents[i]) == -1) {
                                packages.push(parents[i]);
                            }
                        }
                        for (i in features) {
                            if (packages.indexOf(features[i]) == -1) {
                                packages.push(features[i]);
                            }
                        }
                        return [2 /*return*/, packages];
                }
            });
        });
    }
    function promiseLoadModule(path) {
        return new Promise(function (resolve, reject) {
            try {
                requirejs([path], function (_module) {
                    resolve(_module);
                }, function () {
                    // treating fails as empty resolves.
                    resolve(undefined);
                });
            }
            catch (er) {
                //This is weird, but it gets Node.js to not break when requirejs can't find the file.
                resolve(undefined);
            }
        });
    }
    function dependencyWalker(packageId, repoDir, fn, memo) {
        return __awaiter(this, void 0, void 0, function () {
            var packages;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, getDependencies(packageId, repoDir)];
                    case 1:
                        packages = _a.sent();
                        return [2 /*return*/, packages.reduce(fn, memo)];
                }
            });
        });
    }
    function loadDependentResources(resourcePath, packageId, repoDir) {
        var _this = this;
        return dependencyWalker(packageId, repoDir, function (memo, packageName) { return __awaiter(_this, void 0, void 0, function () {
            var _a, _b;
            return __generator(this, function (_c) {
                switch (_c.label) {
                    case 0:
                        _b = (_a = memo).push;
                        return [4 /*yield*/, promiseLoadModule(repoDir + "/" + packageName + "/" + resourcePath)];
                    case 1:
                        _b.apply(_a, [_c.sent()]);
                        return [2 /*return*/, memo];
                }
            });
        }); }, []);
    }
    exports["default"] = {
        getDependencies: getDependencies,
        dependencyWalker: dependencyWalker,
        loadDependentResources: loadDependentResources
    };
});
