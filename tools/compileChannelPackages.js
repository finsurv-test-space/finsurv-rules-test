/*
    This tool traverses reg_rule_repo and produces a compiled set of rules in build/crules/ that can be served as a single .js file.

    run with
    ```
        node compileRules.js
    ```

    TODO: Package.js string-maps need extending.
*/
var storedChannelPackages = {};
var UglifyJS = require("uglify-js");
var fs = require('fs');
var ruleParser = require('./ruleParser');
var glob = require('glob');
var less = require('less');
var minify = require('html-minifier').minify;
var exec = require('child_process').exec;
var path = require('path');
var escapeStr = require('js-string-escape');

var mime = require('mime');

// work with requirejs style define() and require();
var requirejs = require('requirejs');
//require('amdefine/intercept');

requirejs.config({
    baseUrl: path.join(__dirname, "..")
})





/**
 * The configuration object should be run from the project root using `npm run buildChannels`
 */
var config = {
    repoDir: 'reg_rule_repo',
    outDir: 'build/crules',
    debug: false
}

function log() {
    if (config.debug) {
        console.log.apply(this, arguments);
    }
}

function compilePartials(path) {
    var files = glob.sync(`${path}/partials/**/*.html`);
    var partials = files.reduce((memo, file) => {
        // need to clean the filename so the bopform only needs to know about the root templates.
        // It also helps to perform template over-writes in the merge step...
        var cleanFileName = file.substr(file.indexOf('/partials/') + 10);//10 == '/partials/'.length
        memo[cleanFileName] = escapeStr(minify(fs.readFileSync(file, 'utf8'), {
            collapseWhitespace: true,
            removeComments: true,
            quoteCharacter: '"',
            ignoreCustomFragments: [/\{\{[\s\S]*?\}\}/],
            keepClosingSlash: true
        }))
        //.replace(/\n/g, ' ').replace(/\'/g, "\\\'");
        return memo;
    }, {})
    return (Object.getOwnPropertyNames(partials).length == 0) ? undefined : partials;
}

/*
    Less does not have a syncronous version of the compiler, so we use promises.
*/
function lessPromise(file) {
    return new Promise((resolve, reject) => {
        fs.readFile(file, 'utf8', (e, str) => {
            if (e) { reject(e); return; }
            less.render(str, { compress: true }, (e, output) => {
                if (e) reject(e)
                else {
                    resolve(output.css);
                }
            });
        })
    });
}

function compileCssPromise(packPath) {
    return new Promise((resolve, reject) => {
        log('looking for less files in ' + `${packPath}/css/**/*.less`);
        glob(`${packPath}/css/**/*.less`, (er, files) => {
            log('attempting to resolve:' + JSON.stringify(files))
            if (files == null) resolve([])
            else Promise.all(files.map(lessPromise))
                .then(css => {
                    //resolve(css);
                    resolve(inlineStyleAssets({ css: css, baseDir: packPath + "/css" }));

                })
                .catch(e => reject(e));
        });
    })
}

/**
 * Addapted from https://github.com/G33kLabs/gulp-inline-base64
 */
function inlineStyleAssets(opts) {
    log('attempting to inline styles');

    var csses = opts.css;

    return csses.map((css) => {
        log("have css " + css.substr(0, 100) + "...");

        if (!opts.baseDir) opts.baseDir = path.dirname(module.filename);

        var app_path = opts.baseDir;
        var reg_exp = /url\([ '"]?(.*?)[ '"]?(|\,(.*?))\)/g;
        // if (opts.useRelativePath) {
        //     app_path = file.path.replace(/\/[^/]+$/, '/');
        // }

        var matches = [],
            found,
            force;
        while (found = reg_exp.exec(css)) {
            matches.push({
                'txt': found[0],
                'url': found[1],
                'force': found[2].replace(/(\,|\ )/g, '') == 'true' ? true : false
            });
        }
        log('matches:', matches);
        for (var i = 0, len = matches.length; i < len; i++) {
            if (matches[i].url.indexOf('data:image') === -1) { //if find -> image already decoded
                var filepath = path.join(app_path, path.normalize(matches[i].url));
                if (fs.existsSync(filepath)) {
                    var size = fs.statSync(filepath).size;

                    // File will not be included because of its size
                    if (opts.maxSize && size > opts.maxSize && !matches[i].force) {
                        log('inlining styles issue:', ('file is greater than ' + Math.round(size / 1024) + 'kb > ' + Math.round(opts.maxSize / 1024) + 'kb => skip') + (' (' + filepath + ')'));
                        css = css.replace(matches[i].txt, 'url(' + matches[i].url + ')');
                    }

                    // Else replace by inline base64 version
                    else {
                        log(`inlining:(${filepath})`);
                        var b = fs.readFileSync(filepath, 'base64');
                        css = css.replace(matches[i].txt, 'url(' + ('data:' + mime.getType(filepath) + ';base64,' + b.toString('base64')) + ')');
                    }

                } else {
                    log('inlining styles issue:', ('file not found => skip') + (' (' + filepath + ')'));
                }
            }
        }

        return css;
    })


}


/**
 * Helper to extract the rule text. Uses Uglify's parser
 * TODO: maybe consider using the typescript compiler's parser??
 */
function extractRules(filename, type) {
    type = type ? type : 'validation';
    log(`Parsing ${filename}.`);

    var toplevel = UglifyJS.parse(readFile(filename));

    var returnSet = [];
    var currentRuleSet;

    var objectWalker = new UglifyJS.TreeWalker(function (node) {

        if (node instanceof UglifyJS.AST_Object) {
            currentRuleSet.validations.push(node.print_to_string());
            return true;
        }
    });

    var evalObjectWalker = new UglifyJS.TreeWalker(function (node) {

        if (node instanceof UglifyJS.AST_Object) {
            returnSet.push(node.print_to_string());
            return true;
        }
    });

    var validationArrayWalker = new UglifyJS.TreeWalker(function (node) { //walk AST

        if (node.key == 'ruleset') {
            // re-init the currentRuleSet...
            currentRuleSet = { ruleset: node.value.getValue() };

        }
        if (node.key == 'scope') {
            currentRuleSet.scope = node.value.getValue();
        }

        if (node instanceof UglifyJS.AST_Array) {
            currentRuleSet.validations = [];
            returnSet.push(currentRuleSet);
            node.walk(objectWalker);
            return true;
        }
    });

    var evaltionArrayWalker = new UglifyJS.TreeWalker(function (node) { //walk AST
        if (node instanceof UglifyJS.AST_Array) {
            //currentRuleSet = [];
            //returnSet.push(currentRuleSet);
            node.walk(evalObjectWalker);
            return true;
        }
    });

    var assignmentWalker = new UglifyJS.TreeWalker(function (node) { //walk AST
        if (node instanceof UglifyJS.AST_Assign) {
            node.walk(type == 'validation' ? validationArrayWalker : evaltionArrayWalker)
            return true;
        }
    });

    var withWalker = new UglifyJS.TreeWalker(function (node) { //walk AST

        if (node instanceof UglifyJS.AST_With) {
            node.walk(assignmentWalker)
            return true;
        }
    });

    toplevel.walk(withWalker);

    return returnSet;
}


function checkNRequire(path) {

    return fs.existsSync(path + ".js") ? requirejs(path) : {};
}

function checkNParse(path, type) {

    return fs.existsSync(path + ".js") ? extractRules(path + ".js", type) : [];
}

function readFile(filename) {

    try {
        fs.accessSync(filename, fs.F_OK);
        var ret = String(fs.readFileSync(filename), 'utf8');
        return ret;
    }
    catch (e) {
        if (e.code == "ENOENT") {
            log(`No such file ${filename} yo...`);
            return;
        }
        throw e;
    }
}

function readMinimalJSFile(filename) {
    var fileStr = readFile(filename);

    if (fileStr) {
        var toplevel = UglifyJS.parse(fileStr);
        return toplevel.print_to_string();
    }
    //implicit return undefined;
}

function extractPredef(str) {
    if (str) {
        var matcher = /(?:function\s*\(\s*predef\s*\)\s*{)([\s\S]+?)(?:return predef)/;
        var match = str.match(matcher);
        return match[1];
    }
}

function extractCustomScopeFns(str) {
    if (str) {
        var matcher = /(function\s*\(\s*app,\s*config\s*\)\s*{[\s\S]+?return config\s*})/;
        var match = str.match(matcher);
        return match[1];
    }
}

/**
 * Loads a package into memory
 * @param {*} path
 */
function readChannelPackage(path) {
    return compileCssPromise(path)
        .then((css) => {
          var evalContext = checkNRequire(`${path}/evaluation/evalContext`);
          if(typeof evalContext === 'function') evalContext = evalContext();
            return {
                css: css,
                data: {
                  lookups: checkNRequire(`${path}/data/lookups`)
                },
                display: {
                  customFns: [extractCustomScopeFns(readMinimalJSFile(`${path}/display/customFns.js`))],
                  detailDisplay: checkNParse(`${path}/display/detailDisplay`),
                  lookupFilterRules: checkNParse(`${path}/display/lookupFilterRules`),
                  summaryDisplay: checkNParse(`${path}/display/summaryDisplay`)
                },
                document: {
                  importexport: checkNParse(`${path}/document/importexport`),
                  money: checkNParse(`${path}/document/money`),
                  transaction: checkNParse(`${path}/document/transaction`),
                },
                evaluation: {
                  evalScenarios: checkNParse(`${path}/evaluation/evalScenarios`, 'evaluation'),
                  evalContext: evalContext,
                  evalRules: checkNParse(`${path}/evaluation/evalRules`, 'evaluation')
                },
                formDef: checkNRequire(`${path}/formDef/default`),
                partials: compilePartials(path),
                predef: extractPredef(readMinimalJSFile(`${path}/predef/predef.js`)),
                validation: {
                    importexport: checkNParse(`${path}/validation/importexport`),
                    money: checkNParse(`${path}/validation/money`),
                    transaction: checkNParse(`${path}/validation/transaction`),
                },
                info: requirejs(`${path}/package.js`)//this SHOULD exist...
            }
        })



}

/**
 *  go through the list of directories and load the channel package for each one.
 */
function loadChannelPackages(dirs) {
    return dirs
        .map(dir => {
            return readChannelPackage(`${config.repoDir}/${dir}`)
                .then((channelPackage) => {
                    return { name: dir, pack: channelPackage }
                })
        });

}

/**
 * Helper
 */
function getChannelPackageByName(channelPackages, name) {
    let splitName = name.split("@");
    var pack = channelPackages.find(channelPackage => channelPackage.name == splitName[0]);
    if(!pack) throw new Error(`Trying to load package ${name} but it does not exist!`);
    return pack;
}

function mergeFormDefs(src, dest) {

  var formDef = Object.assign({}, src, dest);

  for (var prop in formDef.detail) {
    if (src.detail[prop]) {
      var diff = src.detail[prop].fields.filter(function (itm) {
        return formDef.detail[prop].fields.indexOf(itm) == -1;
      })
      if (diff && diff.length) {
        formDef.detail[prop].fields = formDef.detail[prop].fields.concat(diff);
      }
    }
  }

  return formDef;
}


/*
    Non-Destructively, recursively merge the source package into the destination package...
*/
function mergePackages(channelPackages, packageD, packageS, featureBranch = false) {
    let info = Object.assign({}, packageS.pack.info);

    if (info.dependsOn) {
        log(`Channel package-source ${packageS.name} depends on ${info.dependsOn}. Resolving...`)
        var dependsOn = info.dependsOn;
        if (!storedChannelPackages[packageS.name]) {
            packageS = mergePackages(channelPackages, packageS, getChannelPackageByName(channelPackages, info.dependsOn))

        } else {
            packageS = storedChannelPackages[packageS.name];
        }
        // packageS = mergePackages(channelPackages, packageS, getChannelPackageByName(channelPackages, info.dependsOn))
    }
    if (info.features) {
        if (!storedChannelPackages[packageS.name]) {
            info.features.forEach(feature => {
                log(`Channel package-source ${packageS.name} has feature ${feature}. Resolving...`)
                packageS = mergePackages(channelPackages, packageS, getChannelPackageByName(channelPackages, feature), true)
            })
        } else {
            packageS = storedChannelPackages[packageS.name];
        }
    }
    if (!(info.dependsOn || info.features))
        log("Reached root package...")


    var mergedMap = {};

    if (!packageD.pack.info.mappings) packageD.pack.info.mappings = {};
    if (!packageS.pack.info.mappings) packageS.pack.info.mappings = {};

    Object.assign(mergedMap, packageD.pack.info.mappings, Object.assign({}, packageS.pack.info.mappings, packageD.pack.info.mappings));


    let mPackage = {
        name: packageD.name,
        pack: {
            css: packageD.pack.css,
            //css: packageS.pack.css.concat(packageD.pack.css),
            data: {
                lookups: Object.assign({}, packageS.pack.data.lookups, packageD.pack.data.lookups)
            },
            display: {
                customFns: packageD.pack.display.customFns.concat(packageS.pack.display.customFns),
                detailDisplay: packageD.pack.display.detailDisplay.concat(packageS.pack.display.detailDisplay),
                lookupFilterRules: packageD.pack.display.lookupFilterRules.concat(packageS.pack.display.lookupFilterRules),
                summaryDisplay: packageD.pack.display.summaryDisplay.concat(packageS.pack.display.summaryDisplay)
            },
            document: {
              importexport: packageD.pack.document.importexport.concat(packageS.pack.document.importexport),
              money: packageD.pack.document.money.concat(packageS.pack.document.money),
              transaction: packageD.pack.document.transaction.concat(packageS.pack.document.transaction)
            },
            evaluation: {
              evalScenarios: packageD.pack.evaluation.evalScenarios.concat(packageS.pack.evaluation.evalScenarios),
              evalContext: Object.assign({},packageS.pack.evaluation.evalContext,packageD.pack.evaluation.evalContext),
              evalRules: packageD.pack.evaluation.evalRules.concat(packageS.pack.evaluation.evalRules)
            },
            formDef: Object.assign({}, packageS.pack.formDef, packageD.pack.formDef),//mergeFormDefs(packageS.pack.formDef, packageD.pack.formDef),//
            partials: Object.assign({}, packageS.pack.partials, packageD.pack.partials),
            predef: conditionalConcat(packageD.pack.predef, packageS.pack.predef),
            validation: {
                importexport: packageD.pack.validation.importexport.concat(packageS.pack.validation.importexport),
                money: packageD.pack.validation.money.concat(packageS.pack.validation.money),
                transaction: packageD.pack.validation.transaction.concat(packageS.pack.validation.transaction)
            },
            info: Object.assign({}, packageD.pack.info, packageS.pack.info, { mappings: mergedMap })

        }
    }


    // packageS should now be in a merged state.
    // if(!storedChannelPackages[packageS.name])
    //     storedChannelPackages[packageS.name] = packageS;
    if(!featureBranch)
        storedChannelPackages[packageD.name] = mPackage;
    return mPackage;

}


function conditionalConcat(D, S, delimeter) {
    delimeter = delimeter ? delimeter : '\n';
    if (D && S)
        return D + delimeter + S;
    if (D)
        return D
    return S;
}



function mergeChannelPackages(channelPackages) {
    return channelPackages
        .map(channelPackage => {
            let info = channelPackage.pack.info;
            if (!storedChannelPackages[channelPackage.name]) {
                if (info.features) {
                    info.features.forEach(feature => {
                        log(`Channel package ${channelPackage.name} has feature ${feature}. Resolving...`)
                        channelPackage = mergePackages(channelPackages, channelPackage, getChannelPackageByName(channelPackages, feature), true)
                    })
                }
                if (info.dependsOn) {
                    log(`Channel package ${channelPackage.name} depends on ${info.dependsOn}. Resolving...`)
                    channelPackage = mergePackages(channelPackages, channelPackage, getChannelPackageByName(channelPackages, info.dependsOn))
                }
            }
            else {
                channelPackage = storedChannelPackages[channelPackage.name]
            }
            return channelPackage;
        })
}

function ruleWrapper(rulesStr) {
    return `
define(function(){
    return function(predef){
        var rules;
        with(predef){
            rules = [${rulesStr}];
        }
    return rules;
    }
});`
}

var CssWrapper = (css) => `
define(function(){
    return function(container){
    container = container? (container.parentElement?container.parentElement:container) : document.getElementsByTagName("head")[0]
    var a="${escapeStr(css)}",
    b=document.createElement("style");
    b.type="text/css",
    b.styleSheet?b.styleSheet.cssText=a:b.appendChild(document.createTextNode(a)),
    container.appendChild(b)
    }
});`;

var sectionTemplates = {
    css: (css) => {
        return CssWrapper(css.join(' '));
    },
    customFns: (content) => {
      content = content.reduce(function(m,i){if(i)m.push(i);return m},[]);
        return `
define(function (require) {
  return [
    ${content.join(',')}
  ]
})
`;
    },
    display: (rules) => {
        var rulesStr = rules.map((rule) => {
            return `
{
    ruleset : "${rule.ruleset}", 
    scope : "${rule.scope}",
    fields : [${rule.validations.join(",\n")}]
}
 `;
        }).join(", ");
        return ruleWrapper(rulesStr);
    },
    evaluation: (rules) => {
        var rulesStr = rules.join(",\n");
        return `
define(function(){
    return function(evaluationEx){
        var rules;
        with(evaluationEx){
            rules = [${rulesStr}];
        }
    return rules;
    }
});`
    },
    evalContext: (def) =>{
      return `
define(function () {
  return function (evaluationEx) {
    return ${JSON.stringify(def)}
    }
});`
    },
    partials: (partialObj) => {
        var outStr = "";
        for (var file in partialObj) {
            outStr += `$templateCache.put('${file}','${partialObj[file]}');\n`
        }
        return outStr ?
            `define(function(){return function(){angular.module("bopForm").run(["$templateCache", function($templateCache) {${outStr};}])}});`
            : "";
    },
    predef: (content) => {
        // content needs to have "predef = _.extend(predef, {/*add your functions here*/}" ...
        //     This is to allow a reference to predef in your own predefs. This may change by convention later...
        return `
define(function () {
  return function (predef) {
    ${content}
    return predef;
  }
})     
        `;
    },
    validation: (rules) => {
        var rulesStr = rules.map((rule) => {
            return `
{
    ruleset : "${rule.ruleset}", 
    scope : "${rule.scope}",
    validations : [${rule.validations.join(",\n")}]
}
 `   ;
        }).join(", ");
        return ruleWrapper(rulesStr);
    }
}

function writeStr(path, str) {
    return new Promise((resolve, reject) => {
        fs.open(path, 'w', (err, fd) => {
            if (err) throw err;
            fs.write(
                fd,
                str,
                () => {
                    fs.close(fd, (err) => {
                        if (err) reject(err);
                        else resolve();
                    });
                })
        });
    })

}

function writeSubPackages(strFn) {
    return (channelPackage, section) => {
        for (var subsection in channelPackage.pack[section]) {
            (function (subsection) {
                writeStr(`${config.outDir}/${channelPackage.name}/${section}/${subsection}.js`,
                    strFn(channelPackage, section, subsection));
            })(subsection)
        }
    }
}

var sectionWrite = {
  css: (channelPackage, section) => {
    //won't always have css so check first...
    if (channelPackage.pack[section])
      writeStr(
        `${config.outDir}/${channelPackage.name}/${section}/css.js`,
        sectionTemplates.css(channelPackage.pack[section])
      );
  },
  data: writeSubPackages(
    (channelPackage, section, subsection) =>
      `define(${JSON.stringify(channelPackage.pack[section][subsection])});`
  ),
  display: writeSubPackages((channelPackage, section, subsection) => {
    return subsection == "customFns"
      ? sectionTemplates.customFns(channelPackage.pack[section][subsection])
      : sectionTemplates.display(channelPackage.pack[section][subsection]);
  }),
  document: writeSubPackages((channelPackage, section, subsection) =>{
    // can re-use the validation template...
    return sectionTemplates.validation(channelPackage.pack[section][subsection]);
  }),
  evaluation: writeSubPackages((channelPackage, section, subsection) => {
    if(subsection!="evalContext")
      return sectionTemplates.evaluation(channelPackage.pack[section][subsection]);
    else
      return sectionTemplates.evalContext(channelPackage.pack[section][subsection]);
  }),
  formDef: (channelPackage, section) => {
    writeStr(
      `${config.outDir}/${channelPackage.name}/${section}/default.js`,
      `define(${JSON.stringify(channelPackage.pack[section])});`
    );
  },
  partials: (channelPackage, section) => {
    //won't always have a partials so check first...
    if (channelPackage.pack[section])
      writeStr(
        `${config.outDir}/${channelPackage.name}/${section}/partials.js`,
        sectionTemplates.partials(channelPackage.pack[section])
      );
  },
  predef: (channelPackage, section) => {
    //won't always have a predef so check first...
    if (channelPackage.pack[section])
      writeStr(
        `${config.outDir}/${channelPackage.name}/${section}/predef.js`,
        sectionTemplates.predef(channelPackage.pack[section])
      );
  },
  validation: writeSubPackages((channelPackage, section, subsection) =>
    sectionTemplates.validation(channelPackage.pack[section][subsection])
  ),
  info: (channelPackage, section) => {
    writeStr(
      `${config.outDir}/${channelPackage.name}/package.js`,
      `define(${JSON.stringify(channelPackage.pack[section])});`
    );
  }
};

function writeSection(channelPackage, section) {
    return new Promise((resolve, reject) => {
        fs.mkdir(`${config.outDir}/${channelPackage.name}/${section}`, () => {
            sectionWrite[section](channelPackage, section);
            resolve();
        });
    })

}

function writePackageLoader(channelPackage) {

    return writeStr(`${config.outDir}/${channelPackage.name}/pack.js`,
        `
define([
    ${channelPackage.pack.partials ? "'partials/partials'," : ''}
    ${channelPackage.pack.predef ? "'predef/predef'," : ''}
    'package',
    //'data/lookups',//lookups are injected by the server... 
    'display/customFns',
    'display/detailDisplay',
    'display/lookupFilterRules',
    'display/summaryDisplay',
    //'evaluation/evalScenarios',//eval-rules are injected by the server... 
    //'evaluation/evalContext',
    //'evaluation/evalRules',
    'formDef/default',
    //'validation/importexport',// validation-rules are injected by the server... 
    //'validation/money',
    //'validation/transaction',
    //'document/importexport',// document-rules are injected by the server... 
    //'document/money',
    //'document/transaction',
    'css/css' 
    ],function(
        ${channelPackage.pack.partials ? "partials," : ''}
        ${channelPackage.pack.predef ? "predef," : ''}
        package,
        //lookups,
        customFns,
        detailDisplay,
        lookupFilterRules,
        summaryDisplay,
        //evalScenarios,
        //evalContext,
        //evalRules,
        formDef,
        //importExport,
        //money,
        //transaction,
        //docIE,
        //docMoney,
        //docTrans,
        css
    ){
        return function(lookups){
            return {
                data: {
                    lookups: lookups
                },
                documentRules : {
                //  trans: docTrans,
                //  money: docMoney,
                //  ie: docIE
                },
                display: {
                    customFns: customFns,
                    detailDisplay: detailDisplay,
                    lookupFilterRules: lookupFilterRules,
                    summaryDisplay: summaryDisplay,
                },
                evaluation: {
                  //     evalScenarios: evalScenarios,
                  //     evalContext: evalContext,
                //     evalRules: evalRules,
                },
                formDefinition: formDef,
                ${channelPackage.pack.partials ? "partials: partials," : ''}
                ${channelPackage.pack.predef ? "predef: predef," : ''}
                validation: {
                    // importexport: importExport,
                    // money: money,
                    // transaction: transaction,
                },
                info: package,
                css: css
            }   
        }
        
    })            
            `);
}

function writeMergedPackages(mergedChannelPackages) {
    return Promise.all(
        mergedChannelPackages
            .map(channelPackage => {
                return new Promise((resolve, reject) => {

                    fs.mkdir(`${config.outDir}/${channelPackage.name}`, (err) => {
                        if (err && err.code != "EEXIST") reject(err)
                        else {
                            resolve();
                        };
                    });

                })
                    .then(async () => {
                        for (var section in channelPackage.pack) {
                            await writeSection(channelPackage, section);
                        }
                    })
                    .then(() => {
                        return writePackageLoader(channelPackage);
                    })

            }))
        .then(() => {
            return mergedChannelPackages;
        })
}

// var execPromise = async function (cmd) {
//     return await new Promise((resolve, reject) => {
//         exec(cmd, (err, stdout, stderr) => {
//             if (err) {
//                 console.log(stderr);
//                 reject(err);
//             } else {
//                 console.log(stdout);
//                 resolve();
//             }
//         })
//     })
// }

function getDependencies(package){
    packageName = package.split("@")[0];
    var packages = [packageName];
    var info = requirejs(`${config.repoDir}/${packageName}/package.js`);
    if(info.dependsOn){
        var parents = getDependencies(info.dependsOn);
    }

    if(info.features){
        var features=[];
        for(var i in info.features){
            features= features.concat(getDependencies(info.features[i]));
        }
    }

    for( var i in parents){
        if(packages.indexOf(parents[i])==-1){
            packages.push(parents[i]);
        }
    }

    for( var i in features){
        if(packages.indexOf(features[i])==-1){
            packages.push(features[i]);
        }
    }

    return packages;
}


/*
    Staring point
*/
fs.readdir(config.repoDir, (err, files) => {

    if (!err) {
       
        var dirs = process.argv[2]?
        // argument provided, only process that channel and it's dependencies
        getDependencies(process.argv[2]): 
        // all channels, filtering out the directories only...
        files.filter((filePath) => fs.statSync(`${config.repoDir}/${filePath}`).isDirectory());


       
        log(`Packaging the following directories: ${dirs}`);
        Promise.all(loadChannelPackages(dirs))
            .then(channelPackages => {
                log('\n\nMerging....\n\n');
                // now we need to go through the list of channel Packages and merge as required.
                let mergedChannelPackages = mergeChannelPackages(channelPackages);
                return new Promise((resolve, reject) => {
                    fs.mkdir(`${config.outDir}`, (err) => {
                        if (err && err.code != "EEXIST") reject(err)
                        else {
                            resolve(mergedChannelPackages);
                        };
                    })
                })
            })
            .then(mergedChannelPackages => {
                return writeMergedPackages(mergedChannelPackages);
            })
            .then(mergedChannelPackages => {
                writeStr(`${config.outDir}/channels.js`, `define({data:${JSON.stringify(dirs)}})`);


                // execute r.js to minify the package
                // return Promise.all(mergedChannelPackages
                //     .map(channelPackage => {
                //         var cmd = `node node_modules/requirejs/bin/r.js -o baseUrl=${config.outDir}/${channelPackage.name}/ name=pack out=${config.outDir}/${channelPackage.name}/pack.min.js paths.data/lookups=empty:`;
                //         return execPromise(cmd);
                //         //log(cmd);
                //         //var child = exec(cmd);
                //         //child.stdout.pipe(process.stdout);
                //     }))
            })
            .catch(err => {
                console.log(`
ERROR
${err.message}
-----
${err.extract ? err.extract.join('\n') : ""}
-----
stack:
----- 
${err.stack}
                `);
            })


    }
})
