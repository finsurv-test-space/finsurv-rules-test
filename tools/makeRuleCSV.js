/**
 * Created by petruspretorius on 10/02/2016.
 */
var fs = require("fs");
var argv = process.argv.slice(2);

var coreSARB = "../reg_rule_repo/coreSARB/validation/";
var coreSARBExternal = "../reg_rule_repo/coreSARBExternal/validation/";
var stdBankCommon = "../reg_rule_repo/stdBankCommon/validation/";
var featureBranchHub = "../reg_rule_repo/featureBranchHub/validation/"
var featureMTAAccounts = "../reg_rule_repo/featureMTAAccounts/validation/";

var _ = require("lodash");
// var filenames = argv.length ? argv : ['detailDisplay.js', 'summaryDisplay.js', 'transaction.js', 'money.js', 'importexport.js'];
var filenames =  [
                   coreSARB + 'transaction.js',  coreSARB + 'money.js',  coreSARB + 'importexport.js',
                   coreSARBExternal + 'transaction.js',  coreSARBExternal + 'money.js',  coreSARBExternal + 'importexport.js',
                   stdBankCommon + 'transaction.js',  stdBankCommon + 'money.js',  stdBankCommon + 'importexport.js',
                   featureBranchHub + 'transaction.js',  featureBranchHub + 'money.js',  featureBranchHub + 'importexport.js',
                   featureMTAAccounts + 'transaction.js',  featureMTAAccounts + 'money.js',  featureMTAAccounts + 'importexport.js',
                 ];

// work with requirejs style define() and require();
require('amdefine/intercept');
// if (typeof define !== 'function') { var define = require('amdefine')(module) };
var testCases = require("../testCases");

var ruleParser = require('./ruleParser');
var testParser = require('./testParser');

ruleParser.parse(filenames);

var rulesRuleId = ruleParser.getRuleIDs();
var testsRuleId = testParser.getRuleIDs(testCases);
var needTests = _.difference(rulesRuleId, testsRuleId);
var needRules = _.difference(testsRuleId, rulesRuleId);

console.log("Rule ID's in Rules");
// console.log(rulesRuleId);
console.log("Total: "+rulesRuleId.length);
console.log("");

console.log("Rule ID's in Tests");
// console.log(testsRuleId);
console.log("Total: "+testsRuleId.length);
console.log("");

console.log("Rule ID's that exist in Rules but not the Tests");
console.log(needTests);
console.log("Total: "+needTests.length);
console.log("");

console.log("Rule ID's that exist in Tests but not the Rules");
// console.log(needRules);
console.log("Total: "+needRules.length);
console.log("");



//Uncomment below to generate CSV's
 fs.writeFileSync("rules.csv",ruleParser.getRulesCSV());
// fs.writeFileSync("tests.csv", testParser.getTestsCSV(testCases));
// fs.writeFileSync("displayRules.csv",ruleParser.getDisplayRulesCSV());


process.exit(0);
