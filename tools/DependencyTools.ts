interface engineVer {
  major: string;
  minor: string;
}

interface packageInfo {
  engine: engineVer;
  dependsOn: string;
  features: string[];
}

async function getDependencies(packageId: string, repoDir: string) {
  var packageName = packageId.split("@")[0];
  var packages = [packageName];
  var info: packageInfo = await promiseLoadModule(
    `${repoDir}/${packageName}/package.js`
  );
  if (info.dependsOn) {
    var parents = getDependencies(info.dependsOn, repoDir);
  }

  if (info.features) {
    var features = [];
    for (var i in info.features) {
      features = features.concat(getDependencies(info.features[i], repoDir));
    }
  }

  for (var i in parents) {
    if (packages.indexOf(parents[i]) == -1) {
      packages.push(parents[i]);
    }
  }

  for (var i in features) {
    if (packages.indexOf(features[i]) == -1) {
      packages.push(features[i]);
    }
  }

  return packages;
}

function promiseLoadModule(path): Promise<any> {
  return new Promise(function(resolve, reject) {
    try {
      requirejs(
        [path],
        function(_module) {
          resolve(_module);
        },
        function() {
          // treating fails as empty resolves.
          resolve(undefined);
        }
      );
    } catch (er) {
      //This is weird, but it gets Node.js to not break when requirejs can't find the file.
      resolve(undefined);
    }
  });
}

async function dependencyWalker(
  packageId: string,
  repoDir: string,
  fn: (memo: any, currentValue: string) => any,
  memo: any
) {
  var packages = await getDependencies(packageId, repoDir);
  return packages.reduce(fn, memo);
}

function loadDependentResources(
  resourcePath: String,
  packageId: string,
  repoDir: string
) {
  return dependencyWalker(
    packageId,
    repoDir,
    async (memo, packageName) => {
      memo.push(
        await promiseLoadModule(`${repoDir}/${packageName}/${resourcePath}`)
      );
      return memo;
    },
    []
  );
}

export default {
  getDependencies,
  dependencyWalker,
  loadDependentResources
};
