const http = require("http");
const express = require("express");
const hostname = "127.0.0.1";
const port = 3000;
const fs = require("fs");
const path = require("path");

var app = express();

app.use(/^\/go.js$/, (req, res) => {
  res.sendFile(
    path.join(__dirname, "..", "node_modules", "gojs", "release", "go.js")
  );
});
app.use(/^\/$/, (req, res) => {
  res.statusCode = 200;
  res.setHeader("Content-Type", "text/html");
  Promise.all([getChannelHeirarchy(), getHTML()])
    .then(([data, html]) => {
      output = html.replace(/\$\{data\}/g, JSON.stringify(data));
      res.send(output);
    })
    .catch(err => {
      throw err;
    });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error("Not Found");
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get("env") === "development" ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.send("error");
});

app.set("port", port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on("error", onError);
server.on("listening", onListening);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== "listen") {
    throw error;
  }

  var bind = typeof port === "string" ? "Pipe " + port : "Port " + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case "EACCES":
      console.error(bind + " requires elevated privileges");
      process.exit(1);
      break;
    case "EADDRINUSE":
      console.error(bind + " is already in use");
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === "string" ? "pipe " + addr : "port " + addr.port;
  //debug('Listening on ' + bind);
}

/*
* heirarchy stuff
*/

// work with requirejs style define() and require();
const requirejs = require("requirejs");
//require('amdefine/intercept');

requirejs.config({
  baseUrl: path.join(__dirname, "..")
});

function getHTML() {
  return new Promise((resolve, reject) => {
    //if (getHTML.cache) resolve(getHTML.cache);
    //else
    fs.readFile(
      path.join(__dirname, "nodeDependencyViewer/index.html"),
      "utf8",
      (err, data) => {
        if (!err) {
          //getHTML.cache = data;
          resolve(data);
        }
        reject(err);
      }
    );
  });
}
//getHTML.cache = undefined;

function getChannelNames() {
  return new Promise((resolve, reject) => {
    fs.readdir("reg_rule_repo", (err, files) => {
      if (!err) {
        var dirs = files.filter((
          filePath // all channels, filtering out the directories only...
        ) => fs.statSync(`reg_rule_repo/${filePath}`).isDirectory());

        console.log(`Getting info on the following directories: ${dirs}`);

        resolve(dirs);
      }
      reject(err);
    });
  });
}

function mapPackageInfo(dirs) {
  return dirs.map(dir => {
    let info = requirejs(`reg_rule_repo/${dir}/package.js`);
    var ret = {
      key: dir,
      name: dir
    };
    if (info.dependsOn) ret.parent = info.dependsOn.split("@")[0];
    if (info.features) ret.features = info.features;
    return ret;
  });
}

function getChannelHeirarchy() {
  return new Promise((resolve, reject) => {
    getChannelNames()
      .then(mapPackageInfo)
      .then(packageInfos => {
        resolve(packageInfos);
      })
      .catch(err => {
        reject(err);
      });
  });
}


console.log('visit http://localhost:3000/');