/**
 * Created by nishaal on 2/22/16.
 */
/**
 * Created by petruspretorius on 10/02/2016.
 */
var UglifyJS = require("uglify-js"); //Please install node first
var fs = require('fs');
var _ = require('lodash');
//var filenames = argv.length ? argv : ['detailDisplay.js', 'summaryDisplay.js', 'transaction.js', 'money.js', 'importexport.js'];
//var filenames = ['detailDisplay.js', 'transaction.js'];


var delimiter = '\t';
var columns = {
  display: [
    "SourceFile",
    "field",
    "txt",
    "src",
    "condition",
    "assertion",
    "onInflow",
    "onOutflow",
    "onSection",
    "notOnSection",
    "onCategory",
    "notOnCategory"
  ],
  rules: [
    "SourceFile",
    "field",
    "len",
    "minLen",
    "maxLen",
    "ruleId",
    "code",
    "message",
    "condition",
    "assertion",
    "onInflow",
    "onOutflow",
    "onSection",
    "notOnSection",
    "onCategory",
    "notOnCategory"
  ]
};

function quoteWrap(txt) {
  return "\"" + txt + "\"";
}

function quoteWrapEscape(txt){
  return "\"" + txt.replace(/"/g, '""') + "\"";
}

var arrayMatcher = /\[(.+)\]/;

function mapColumns(columns, rule) {
  return columns.map(function(columnName) {
    return (rule[columnName] ? String(rule[columnName]).indexOf('"') !== -1 || String(rule[columnName]).indexOf(',') !== -1 ? quoteWrapEscape(String(rule[columnName])): rule[columnName] : "");
  }).join(delimiter) + "\n";
}

function getRuleStr(type, rule) {
  var out = "";
  rule[type].forEach(function(_rule) {
    var tempRule = _.cloneDeep(rule);
    delete tempRule[type];
    tempRule = _.extend(tempRule, _rule);
    out += mapColumns(columns[type], tempRule);
  })
  return out;
}

function makeRulesCSV(srcRules, type) {
  var out = columns[type].join(delimiter) + "\n";
  srcRules.forEach(function(ruleSet) {
    ruleSet.fields.forEach(function(rule){
      //check if we have an array field
    if (rule.field.indexOf('[') != -1) {
      var fields = arrayMatcher.exec(rule.field)[1].split(',');

      var tempRule = _.cloneDeep(rule);
      fields.forEach(function(fieldName) {
        tempRule.field = fieldName;
        out += getRuleStr(type, tempRule);
      })
    } else {
      out += getRuleStr(type, rule);
    }
    })

  })
  return out;
}


var allRules = [];
var allDisplayRules = [];


function parse(filenames) {


  allRules = [];
  allDisplayRules = [];

  filenames.forEach(function(filename) {

    try{
      fs.accessSync(filename, fs.F_OK);
      var toplevel = UglifyJS.parse(String(fs.readFileSync(filename)));
    }
    catch(e){
      if (e.code == "ENOENT"){
        return;
      }
      throw e;
    }



    var rules = [];
    var displayRules = [];

    var ruleNodeWalker = function(ruleObj, type) {

      return new UglifyJS.TreeWalker(function(node) {

        if (node instanceof UglifyJS.AST_Call) {

          if (node.expression.name) {
            if (type == 'rules') {
              ruleObj.ruleId = node.args[0].getValue();
              if (node.expression.name != 'ignore') {
                if (node.expression.name == 'validate') {
                  ruleObj.code = node.args[1].getValue();
                  ruleObj.assertion = node.args[2].print_to_string();
                } else {
                  ruleObj.code = node.args[1].getValue();
                  ruleObj.message = node.args[2].getValue();
                  if (typeof node.args[3] != 'undefined')
                    ruleObj.assertion = node.args[3].print_to_string();
                }

              }

            } else {
              switch (node.expression.name) {
                case 'setLabel':
                case 'setValue':
                case 'appendValue':
                  if (typeof node.args[0] != 'undefined')
                    ruleObj.txt = node.args[0].getValue();
                  if (typeof node.args[1] != 'undefined')
                    ruleObj.src = node.args[1].getValue();
                  if (typeof node.args[2] != 'undefined')
                    ruleObj.assertion = node.args[2].print_to_string();
                  break;
                case 'excludeValue':
                case 'limitValue':
                  ruleObj.txt = node.args[0].getValue();
                  if (typeof node.args[1] != 'undefined')
                    ruleObj.assertion = node.args[1].print_to_string();
                  break;
                default:
                  if (typeof node.args[0] != 'undefined')
                    ruleObj.assertion = node.args[0].print_to_string();

              }

            }

            // console.log(fieldObj);
            return true;
          } else {
            var args = node.args ? node.args.filter(function(item) {
              return typeof item.value !== 'undefined' || typeof item.elements !== 'undefined';
            }) : [];

            args = args.map(function(item) {
              // console.log(item);
              return item.value || item.elements.map(function(item) {
                return item.value;
              }) || [];
            })
            ruleObj[node.expression.property] = args[0] ? args[0] : "X";

          }

          //console.log(node.expression.property || node.expression.name, args);
          //
          //console.log(node.print_to_string());

        }
      });
    }


    var ruleWalker = function(ruleSet, fieldObj, type) {

      return new UglifyJS.TreeWalker(function(node) { //walk AST
        if (typeof fieldObj[type] === 'undefined') {
          fieldObj[type] = [];
          ruleSet.fields.push(fieldObj);
        };

        if (node instanceof UglifyJS.AST_Call) {

          var ruleObj = {
            condition: node.start.value
          };

          fieldObj[type].push(ruleObj);
          node.walk(ruleNodeWalker(ruleObj, type));
          return true;
        }
      });

    }

    var currentField;
    var rulesWalker = function(ruleSet){
      return new UglifyJS.TreeWalker(function(node) { //walk AST

      if (node instanceof UglifyJS.AST_Array && this.parent() && this.parent().key == 'rules') {
        if(rules.indexOf(ruleSet)==-1)rules.push(ruleSet);
        node.walk(ruleWalker(ruleSet, currentField, 'rules'))
        return true;
      }
      if (node instanceof UglifyJS.AST_Array && this.parent() && this.parent().key == 'display') {
        if(displayRules.indexOf(ruleSet)==-1)displayRules.push(ruleSet);
        node.walk(ruleWalker(ruleSet, currentField, 'display'))
        return true;
      }
      if (this.parent() && this.parent().key == 'field') {
        currentField = {
          field: node.getValue ? [node.getValue()] : node.elements.map(function(item) {
            return item.getValue()
          }),
          SourceFile: filename
        };

        return true;
      }
      if (this.parent() && this.parent().key == 'len') {
        currentField.len = node.getValue();
        return true;
      }
      if (this.parent() && this.parent().key == 'minLen') {
        currentField.minLen = node.getValue();
        return true;
      }
      if (this.parent() && this.parent().key == 'maxLen') {
        currentField.maxLen = node.getValue();
        return true;
      }
    })
    };

    var currentRuleSet;
    var arrayWalker = new UglifyJS.TreeWalker(function(node) { //walk AST

      if(node.key == 'ruleset'){
        currentRuleSet = { ruleset: node.value.getValue(), fields: []};

      }
      if(node.key == 'scope'){
        currentRuleSet['scope'] = node.value.getValue();
      }

      if (node instanceof UglifyJS.AST_Array) {
        node.walk(rulesWalker(currentRuleSet))
        return true;
      }
    });

    var assignmentWalker = new UglifyJS.TreeWalker(function(node) { //walk AST
      if (node instanceof UglifyJS.AST_Assign) {
        node.walk(arrayWalker)
        return true;
      }
    });

    var withWalker = new UglifyJS.TreeWalker(function(node) { //walk AST

      if (node instanceof UglifyJS.AST_With) {
        rules = [];
        node.walk(assignmentWalker)
        return true;
      }
    });
    toplevel.walk(withWalker);


    if (rules.length > 0) {
      allRules = allRules.concat(rules);
    } else { //display rules
      allDisplayRules = allDisplayRules.concat(displayRules);
    }
  });

}


function getRuleIDs(){
  return allRules.reduce(function(memo,item){
    item.fields.map(function(ruleObj){
      ruleObj.rules.map(function(rule){
        memo.push(rule.ruleId)
      });
    });
    return memo;
  }, []);
}

function getRulesCSV() {
  return makeRulesCSV(allRules, 'rules');
}

function getDisplayRulesCSV() {
  return makeRulesCSV(allDisplayRules, 'display');
}

function getRules() {
  return allRules;
}

function getDisplayRules() {
  return allDisplayRules;
}


module.exports = {
  parse: parse,
  getRules: getRules,
  getDisplayRules: getDisplayRules,
  getRulesCSV: getRulesCSV,
  getDisplayRulesCSV: getDisplayRulesCSV,
  getRuleIDs: getRuleIDs
}
