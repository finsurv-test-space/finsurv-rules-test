/**
 * Created by nishaal on 05/18/16.
 */

var UglifyJS = require("uglify-js"); //Please install node first
var fs = require('fs');
var _ = require('lodash');


var allEvalRules = [];
function parse(filenames){
                          
    allEvalRules= [];          
    
    filenames.forEach(function(filename) {
        try{
            fs.accessSync(filename, fs.F_OK);
            var toplevel = UglifyJS.parse(String(fs.readFileSync(filename)));
        }
        catch(e){
            if (e.code == "ENOENT"){
                return;
            };
            throw e;
        };
        
        var evalRules = [];
        
        var arrayWalker = new UglifyJS.TreeWalker(function(node) { //walk AST

        if (node instanceof UglifyJS.AST_Array) {
            evalRules.push(node.print_to_string());
            return true;
        }
        });

        var assignmentWalker = new UglifyJS.TreeWalker(function(node) { //walk AST
        if (node instanceof UglifyJS.AST_Assign) {
            node.walk(arrayWalker)
            return true;
        }
        });

        var withWalker = new UglifyJS.TreeWalker(function(node) { //walk AST

        if (node instanceof UglifyJS.AST_With) {
            node.walk(assignmentWalker)
            return true;
        }
        });
        toplevel.walk(withWalker);


        if (evalRules.length > 0) {
            allEvalRules = allEvalRules.concat(evalRules);
        } 
        
           
    });
    
};


function getEvalRules(){
    return allEvalRules;    
};

module.exports = {
    parse: parse,
    getEvalRules: getEvalRules
};
