/**
 * Created by petruspretorius on 10/02/2016.
 */
var fs = require("fs");
var argv = process.argv.slice(2);

var coreSARB =            "../reg_rule_repo/coreSARB/validation/";
var stdBankCustomCommon = "../../../../custom/common/rules/stdbank";
var stdBankCustomNBOL =   "../../../../custom/NBOL/rules/stdbankNBOL";

var _ = require("lodash");
// var filenames = argv.length ? argv : ['detailDisplay.js', 'summaryDisplay.js', 'transaction.js', 'money.js', 'importexport.js'];
var filenames =  [
      coreSARB + 'transaction.js',  coreSARB + 'money.js',  coreSARB + 'importexport.js',
      stdBankCustomCommon + 'Trans.js', stdBankCustomCommon + 'Money.js', stdBankCustomCommon + 'IE.js',
      stdBankCustomNBOL + 'Trans.js', stdBankCustomNBOL + 'Money.js', stdBankCustomNBOL + 'IE.js'
];


var testCases = require("../testCases");

var ruleParser = require('./ruleParser');
var testParser = require('./testParser');

ruleParser.parse(filenames);

var rulesRuleId = ruleParser.getRuleIDs();
var testsRuleId = testParser.getRuleIDs(testCases);
var needTests = _.difference(rulesRuleId, testsRuleId);
var needRules = _.difference(testsRuleId, rulesRuleId);

// console.log("Rule ID's in Rules");
// // console.log(rulesRuleId);
// console.log("Total: "+rulesRuleId.length);
// console.log("");
//
// console.log("Rule ID's in Tests");
// // console.log(testsRuleId);
// console.log("Total: "+testsRuleId.length);
// console.log("");
//
// console.log("Rule ID's that exist in Rules but not the Tests");
// // console.log(needTests);
// console.log("Total: "+needTests.length);
// console.log("");
//
// console.log("Rule ID's that exist in Tests but not the Rules");
// // console.log(needRules);
// console.log("Total: "+needRules.length);
// console.log("");



//Uncomment below to generate CSV's
fs.writeFileSync("rules.csv",ruleParser.getRulesCSV());
//fs.writeFileSync("tests.csv", testParser.getTestsCSV(testCases));
// fs.writeFileSync("displayRules.csv",ruleParser.getDisplayRulesCSV());


process.exit(0);
