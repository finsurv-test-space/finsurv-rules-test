/**
 * Created by nishaal on 10/24/16.
 */
var fs = require('fs');
var _ = require('lodash');
// var testCases = require('./../testCases');

function quoteWrapEscape(txt){
  return "\"" + txt.replace(/"/g, '""') + "\"";
}

var outStr='assertion,ruleId,context\n';
buildStr = function(type,id,context){
  outStr += [type,id,quoteWrapEscape(JSON.stringify(context))].join(",")+"\n";
}

var ruleIds = [];
addRuleId = function(id){
  if (id.indexOf(":")!==-1){
    id = id.substr(0, id.indexOf(":"));
  };
  if (id.indexOf(".len")==-1 && id.indexOf(".minLen")==-1 && id.indexOf(".maxLen")==-1){
    if (ruleIds.indexOf(id)==-1){
      ruleIds.push(id);
    };
  };
};


var testBase = {

  assertFailure: function (id, context) {
    buildStr("assertFailure",id,context);
    addRuleId(id);
  },

  assert2Failures: function (id, context) {
    buildStr("assert2Failures",id,context);
    addRuleId(id);
  },

  assertWarning: function (id, context) {
    buildStr("assertWarning",id,context);
    addRuleId(id);
  },

  assertDeprecated: function (id, context) {
    buildStr("assertDeprecated",id,context);
    addRuleId(id);
  },

  assertSuccess: function (id, context) {
    buildStr("assertSuccess",id,context);
    addRuleId(id);
  },

  assertNoRule: function (id, context) {
    buildStr("assertNoRule",id,context);
    addRuleId(id);
  },

  assertFailureCustom: function (id, context) {
    buildStr("assertFailureCustom",id,context);
    addRuleId(id);
  },

  assert2FailuresCustom: function (id, context) {
    buildStr("assert2FailuresCustom",id,context);
    addRuleId(id);
  },

  assertWarningCustom: function (id, context) {
    buildStr("assertWarningCustom",id,context);
    addRuleId(id);
  },

  assertDeprecatedCustom: function (id, context) {
    buildStr("assertDeprecatedCustom",id,context);
    addRuleId(id);
  },

  assertSuccessCustom: function (id, context) {
    buildStr("assertSuccessCustom",id,context);
    addRuleId(id);
  },

  assertNoRuleCustom: function (id, context) {
    buildStr("assertNoRuleCustom",id,context);
    addRuleId(id);
  },

  assertValue: function (id, context) {
    buildStr("assertValue",id,context);
    addRuleId(id);
  },

  assertShow: function (id, context) {
    buildStr("assertShow",id,context);
    addRuleId(id);
  },

  assertHide: function (id, context) {
    buildStr("assertHide",id,context);
    addRuleId(id);
  },

  assertNoEvent: function (id, context) {
    buildStr("assertNoEvent",id,context);
    addRuleId(id);
  },

  assertDisabled: function (id, context) {
    buildStr("assertDisabled",id,context);
    addRuleId(id);
  },

  assertEnabled: function (id, context) {
    buildStr("assertEnabled",id,context);
    addRuleId(id);
  },

}


var getTestsCSV = function(testCases){
  testCases(testBase);
  return outStr;
}

var getRuleIDs = function(testCases){
  testCases(testBase)
  return ruleIds;
}



module.exports = {
  getTestsCSV: getTestsCSV,
  getRuleIDs: getRuleIDs
}
